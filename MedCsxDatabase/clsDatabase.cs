﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using MedCsxDatabase;
using System.Configuration;
using MedJames.Database.SQL;

namespace MedCsxDatabase
{
    public class clsDatabase
    {
    #region Class Variables
        //Database connection object
        private DBRoutines db = null;


        //Database name
        private const string DatabaseName = "Claims";

        //Database timeout
        private const Int32 TimeoutLength = 15;

        //Database command timeout
        public const Int32 CommandTimeoutLength = 300; //5 mins

    #endregion

    #region Database Connection
<<<<<<< HEAD
        public clsDatabase() : this("data source=medntsql1;initial catalog=claims;persist security info=False;Trusted_Connection=Yes") { }

=======
<<<<<<< HEAD
        public clsDatabase() : this("data source=medntsql1;initial catalog=Claims;persist security info=False;Trusted_Connection=Yes") { }

=======
        public clsDatabase() : this("data source=qasql4;initial catalog=Claims;persist security info=False;Trusted_Connection=Yes") { }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        
        public clsDatabase(string connectionString)
            : base()
        {

            frmDatabaseConnect f = null;
            try
            {
                //Display a message to let the user know they are connecting to DB
                //Display the DB connect form
                f = new frmDatabaseConnect();
                //f.Show();
                //f.Refresh();

                //Connect to the DB if the connection is closed
                this.db = new MedJames.Database.SQL.DBRoutines(connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //Close the DB connect form
                if (f!=null)
                {
                    //f.Close();
                }
             }
        }
    #endregion

    #region Database Routines
        
        #region Parameters
        protected string MakeParmList(object parm1 = null,
            object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
            object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
            object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
            object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
            object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
            object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
            object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
            object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
            object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
<<<<<<< HEAD
            object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
            object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null)
=======
            object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        {
            string parms = "";
            string parmName = "";
            object parmValue;

            this.CreateParameter(parm1, ref parms);
            this.CreateParameter(parm2, ref parms);
            this.CreateParameter(parm3, ref parms);
            this.CreateParameter(parm4, ref parms);
            this.CreateParameter(parm5, ref parms);
            this.CreateParameter(parm6, ref parms);
            this.CreateParameter(parm7, ref parms);
            this.CreateParameter(parm8, ref parms);
            this.CreateParameter(parm9, ref parms);
            this.CreateParameter(parm10, ref parms);
            this.CreateParameter(parm11, ref parms);
            this.CreateParameter(parm12, ref parms);
            this.CreateParameter(parm13, ref parms);
            this.CreateParameter(parm14, ref parms);
            this.CreateParameter(parm15, ref parms);
            this.CreateParameter(parm16, ref parms);
            this.CreateParameter(parm17, ref parms);
            this.CreateParameter(parm18, ref parms);
            this.CreateParameter(parm19, ref parms);
            this.CreateParameter(parm20, ref parms);
            this.CreateParameter(parm21, ref parms);
            this.CreateParameter(parm22, ref parms);
            this.CreateParameter(parm23, ref parms);
            this.CreateParameter(parm24, ref parms);
            this.CreateParameter(parm25, ref parms);
            this.CreateParameter(parm26, ref parms);
            this.CreateParameter(parm27, ref parms);
            this.CreateParameter(parm28, ref parms);
            this.CreateParameter(parm29, ref parms);
            this.CreateParameter(parm30, ref parms);
            this.CreateParameter(parm31, ref parms);
            this.CreateParameter(parm32, ref parms);
            this.CreateParameter(parm33, ref parms);
            this.CreateParameter(parm34, ref parms);
            this.CreateParameter(parm35, ref parms);
            this.CreateParameter(parm36, ref parms);
            this.CreateParameter(parm37, ref parms);
            this.CreateParameter(parm38, ref parms);
            this.CreateParameter(parm39, ref parms);
            this.CreateParameter(parm40, ref parms);
            this.CreateParameter(parm41, ref parms);
<<<<<<< HEAD
            this.CreateParameter(parm42, ref parms);
            this.CreateParameter(parm43, ref parms);
            this.CreateParameter(parm44, ref parms);
            this.CreateParameter(parm45, ref parms);
            this.CreateParameter(parm46, ref parms);
=======
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            return " " + parms.Trim(',');
        }

            private void CreateParameter(object Parameter, ref string Parameters)
            {
                if (Parameter!=null)
                {
                    if ((Parameter.GetType() == typeof(DateTime)) || (Parameter.GetType() == typeof(string)))
                    {
                        if ((Parameter.GetType()==typeof(string)) && (Parameter.ToString().Length > 2) && 
                            (((Parameter.ToString().Substring(0,1) == "'") && (Parameter.ToString().Substring(Parameter.ToString().Length - 1, 1) == "'")) ||
                            ((Parameter.ToString().Substring(0,1) == "[") && (Parameter.ToString().Substring(Parameter.ToString().Length - 1, 1) == "]"))))
                        {
                            Parameters += ", " + Parameter.ToString();
                        }
                        else 
                        {
                            Parameters += ", " + "'" + Parameter.ToString().Replace("'", "") + "'";
                        }
                    }
                    else if (Parameter.GetType() == typeof(bool))
                    {
                        if ((bool)Parameter)
                        {
                            Parameters += ", " + "1";
                        }
                        else
                        {
                            Parameters += ", " + "0";
                        }
                    }
                    else
                    {
                        Parameters += ", " + Parameter;
                    }
                }
            }
        #endregion

        #region SQL String
            public DataTable ExecuteSelect(string sql)
            {
                //Executes select statement and returns datatable
                DateTime startTime = DateTime.Now;
                DataSet Results = db.ExecSQLReturnDS(sql, CommandTimeoutLength, "");
                DataTable Table;
                if (Results.Tables.Count > 0)
                {
                    Table = Results.Tables[0];
                }
                else
                {
                    Table = new DataTable();
                }
                Log(sql, "", DateTime.Now.Subtract(startTime));
                return Table;
            }

        public List<Hashtable> ExecuteSelectReturnHashList(string sql)
        {
            //Executes select statement and returns data table
            DateTime startTime = DateTime.Now;
            DataSet Results = db.ExecSQLReturnDS(sql, CommandTimeoutLength, "");
            DataTable Table;
            if (Results.Tables.Count > 0)
            {
                Table = Results.Tables[0];
            }
            else
            {
                Table = new DataTable();
            }
            Log(sql, "", DateTime.Now.Subtract(startTime));
            return DataTableToHashtableList(Table);
        }

        public DataTable ExecuteSelectReturnDataTable(string sql, int Distinction)
        {
            //Executes select statement and returns data table
            DateTime startTime = DateTime.Now;
            DataSet Results = db.ExecSQLReturnDS(sql, CommandTimeoutLength, "");
            DataTable Table;
            if (Results.Tables.Count > 0)
            {
                Table = Results.Tables[0];
            }
            else
            {
                Table = new DataTable();
            }
            Log(sql, "", DateTime.Now.Subtract(startTime));
            return Table;
        }

        public DataTable ExecuteSelectReturnDataTable(string sql)
        {
            //Executes select statement and returns data table
            DateTime startTime = DateTime.Now;
            DataSet Results = db.ExecSQLReturnDS(sql, CommandTimeoutLength, "");
            DataTable Table;
            if (Results.Tables.Count > 0)
            {
                Table = Results.Tables[0];
            }
            else
            {
                Table = new DataTable();
            }
            Log(sql, "", DateTime.Now.Subtract(startTime));
            return Table;
        }

        public object ExecuteSelectReturnValue(string sql)
            {
                //Executes select statement and returns value
                DateTime startTime = DateTime.Now;
                object Value = db.ExecSQLReturnValue(sql);
                Log(sql, "", DateTime.Now.Subtract(startTime));
                return Value;                
            }

            public void ExecuteSelectNoReturnValue(string sql)
            {
                //Executes select statement and doesn't returns value
                DateTime startTime = DateTime.Now;
                db.ExecSQLUpdateStmt(sql);
                Log(sql, "", DateTime.Now.Subtract(startTime));
            }

            public string GetString(string sql)
            {
                //Executes select statement and returns string
                DateTime startTime = DateTime.Now;
                string Value = db.ExecSQLReturnValue(sql).ToString();
                Log(sql, "", DateTime.Now.Subtract(startTime));
                return Value;
            }

            public int GetInteger(string sql)
            {
                //Executes select statement and returns integer
                DateTime startTime = DateTime.Now;
                int  Value = (int)db.ExecSQLReturnValue(sql);
                Log(sql, "", DateTime.Now.Subtract(startTime));
                return Value;
            }

            public Hashtable GetRow(string tableName, int id)
            {
                //Gets row from table and returns data row
                DateTime startTime = DateTime.Now;
                DataTable Table = this.ExecuteSelect("select * from [" + tableName + "] where " + tableName + "_Id = " + id.ToString());
                Log("GetRow: " + tableName + " " + id, "", DateTime.Now.Subtract(startTime));
                if (Table.Rows.Count > 0)
                {
                    return DataTableRowToHashtable(Table, Table.Rows[0]);
                }
                else
                {
                    return new Hashtable();
                }
            }
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


        public Hashtable GetPasswordENCR(string tableName, int id)
        {
            //Gets row from table and returns data row
            DateTime startTime = DateTime.Now;
            DataTable Table = this.ExecuteSelect("select  CONVERT(varchar, DecryptByKey(Password_Enc)) AS 'Password' from [User] where user_id = " + id.ToString());
            Log("GetRow: " + tableName + " " + id, "", DateTime.Now.Subtract(startTime));
            if (Table.Rows.Count > 0)
            {
                return DataTableRowToHashtable(Table, Table.Rows[0]);
            }
            else
            {
                return new Hashtable();
            }
        }
        #endregion

        #region Stored Procedures
        public DataTable ExecuteStoredProcedure(string spName, object parm1 = null,
<<<<<<< HEAD
=======
=======
        #endregion

        #region Stored Procedures
            public DataTable ExecuteStoredProcedure(string spName, object parm1 = null,
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
                  object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
                  object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
                  object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
                  object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
                  object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
                  object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
                  object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
<<<<<<< HEAD
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
                  object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null)
=======
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            {
                string parms = MakeParmList(parm1, parm2, parm3, parm4, parm5, parm6, parm7,
                    parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16, parm17,
                    parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27,
                    parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37,
<<<<<<< HEAD
                    parm38, parm39, parm40, parm41, parm42, parm43, parm44, parm45, parm46);
=======
                    parm38, parm39, parm40, parm41);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                return ExecuteStoredProcedure(spName, parms);
            }

            public List<Hashtable> ExecuteStoredProcedureReturnHashList(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
                  object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
                  object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
                  object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
                  object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
                  object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
                  object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
                  object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
<<<<<<< HEAD
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
                  object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null)
=======
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            {
                DataTable Data = ExecuteStoredProcedure(spName, parm1, parm2, parm3, parm4, parm5,
                    parm6, parm7, parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16,
                    parm17, parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27,
                    parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37,
<<<<<<< HEAD
                    parm38, parm39, parm40, parm41, parm42, parm43, parm44, parm45, parm46);
=======
                    parm38, parm39, parm40, parm41);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                return DataTableToHashtableList(Data);
            }

        public DataTable ExecuteStoredProcedureReturnDataTable (string spName, object parm1 = null,
                 object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                 object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
                 object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
                 object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
                 object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
                 object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
                 object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
                 object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
                 object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
<<<<<<< HEAD
                 object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
                  object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null)
=======
                 object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        {
            DataTable Data = ExecuteStoredProcedure(spName, parm1, parm2, parm3, parm4, parm5,
                parm6, parm7, parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16,
                parm17, parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27,
                parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37,
<<<<<<< HEAD
                parm38, parm39, parm40, parm41, parm42, parm43, parm44, parm45, parm46);
=======
                parm38, parm39, parm40, parm41);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            return Data;
        }

        public void ExecuteStoredProcedureNoReturnValue(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
                  object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
                  object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
                  object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
                  object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
                  object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
                  object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
                  object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
<<<<<<< HEAD
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
                  object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null)
=======
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            {
                string parms = MakeParmList(parm1, parm2, parm3, parm4, parm5, parm6, parm7,
                    parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16, parm17,
                    parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27,
                    parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37,
<<<<<<< HEAD
                    parm38, parm39, parm40, parm41, parm42, parm43, parm44, parm45, parm46);
=======
                    parm38, parm39, parm40, parm41);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                this.ExecuteSelectNoReturnValue("exec " + spName + parms);
            }

            public DataTable ExecuteStoredProcedure(string spName, List<SqlParameter> parameters)
            {
                //Executes stored procedure and returns results
                DateTime startTime = DateTime.Now;
                DataSet Results = db.ExecSPReturnDS(spName, parameters.ToArray(), CommandTimeoutLength);
                DataTable Table;
                if (Results.Tables.Count > 0)
                {
                    Table = Results.Tables[0];
                }
                else
                {
                    Table = new DataTable();
                }
                Log(spName, parameters, DateTime.Now.Subtract(startTime));
                return Table;
            }

            private DataTable ExecuteStoredProcedure(string spName, string parameters)
            {
                //Executes stored procedure and returns results
                string sql = "exec " + spName + parameters;
                return this.ExecuteSelect(sql);
            }

            public DateTime CurrentDate()
            {
                //Returns the current date from the DB
                return GetDateFromStoredProcedure("usp_Get_Current_Date");
                //getdate()
            }

            private object ExecuteStoredProcedureValuesOnly(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
                  object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
                  object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
                  object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
                  object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
                  object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
                  object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
                  object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
<<<<<<< HEAD
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
                  object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null)
=======
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            {
                string parms = MakeParmList(parm1, parm2, parm3, parm4, parm5, parm6, parm7,
                    parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16, parm17,
                    parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27,
                    parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37,
<<<<<<< HEAD
                    parm38, parm39, parm40, parm41, parm42, parm43, parm44, parm45, parm46);
=======
                    parm38, parm39, parm40, parm41);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                string sql = "exec " + spName + parms;
                return this.ExecuteSelectReturnValue(sql);
            }

            public bool GetBoolFromStoredProcedure(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null)
            {
                object Value = ExecuteStoredProcedureValuesOnly(spName, parm1, parm2, parm3, parm4, parm5, parm6);
                if (Value==null)
                {
                    Value = 0;
                }

                int parse = 0;
                if (int.TryParse(Value.ToString().Replace("$", ""), out parse))
                {
                    if ((int)Value == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    switch (Value.ToString().ToUpper())
                    {
                        case "TRUE":
                        case "YES":
                        case "1":
                            return true;
                        default:
                            return false;
                    }
                }
            }

            public DateTime GetDateFromStoredProcedure(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null)
            {
                return (DateTime)ExecuteStoredProcedureValuesOnly(spName, parm1, parm2, parm3, parm4, parm5, parm6);
            }

            public string GetStringFromStoredProcedure(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null)
            {
                object Value = ExecuteStoredProcedureValuesOnly(spName, parm1, parm2, parm3, parm4, parm5, parm6);
                if (Value==null)
                {
                    Value = "";
                }
                return Value.ToString();
            }

            public int GetIntFromStoredProcedure(string spName, object parm1 = null,
                  object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                  object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
                  object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
                  object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
                  object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
                  object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
<<<<<<< HEAD
                  object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
                  object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
                  object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
                  object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null,
                  object parm42 = null, object parm43 = null, object parm44 = null, object parm45 = null,
                  object parm46 = null) 
=======
                  object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null, object parm30 = null) 
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            {
                object Value = ExecuteStoredProcedureValuesOnly(spName, parm1, parm2, parm3, parm4, parm5, 
                    parm6, parm7, parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16, 
                    parm17, parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27, 
<<<<<<< HEAD
                    parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37, parm38,
                    parm39, parm40, parm41, parm42, parm43, parm44, parm45, parm46);
=======
                    parm28, parm29, parm30);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                int parse = 0;
                if ((Value!=null) && int.TryParse(Value.ToString().Replace("$", ""), out parse))
                {
                    return (int)Value;
                }
                return 0;
            }

            public double GetDblFromStoredProcedure(string spName, object parm1 = null,
                object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
                object parm6 = null)
            {
                object Value = ExecuteStoredProcedureValuesOnly(spName, parm1, parm2, parm3, parm4, parm5,
                    parm6);
            try
            {
                double parse = 0;
                if ((Value != null) && double.TryParse(Value.ToString().Replace("$", ""), out parse))
                {
                    //return (double)(decimal)Value;
                    return Convert.ToDouble(Value);
                    //return (double)((decimal)Value);
                }
            }
            catch (Exception ex)
            {

            }
                return 0;
            }
        #endregion

        #region Non-Query Procedures
            public int InsertRow(Hashtable row, string table, int userID)
            {
                /*Inserts row into table and returns id of row inserted.
                 * Row is a dictionary whose keys are column names and values are column values.
                 * If an insert event is registered in teh data dictionary for the table, raise
                 * the event.
                 */

                string insertSQL, values, value;

                if (!row.Contains("Modified_By"))
                {
                    row.Add("Modified_By", userID);
                }
                else
                {
                    row["Modified_By"] = userID;
                }

                if (!row.Contains("Created_By"))
                {
                    row.Add("Created_By", userID);
                }
                else
                {
                    row["Created_By"] = userID;
                }

                insertSQL = "insert into [" + table + "] (";
                values = " values (";

                foreach (string key in row.Keys)
                {
                    value = row[key].ToString().Trim();
                    if (value != "")
                    {
                        insertSQL += key + ",";
                        if (isStringValue(value) || forceString(key))
                        {
                            switch (value)
                            {
                                case "True":
                                    value = "1";
                                    break;
                                case "False":
                                    value = "0";
                                    break;
                                default:
                                    value = "'" + value.Trim().Replace("'", "''") + "'";
                                    break;
                            }
                        }
                        values += value + ",";
                    }
                }
                insertSQL = insertSQL.Substring(0, insertSQL.Length - 1) + ")";
                values = values.Substring(0, values.Length - 1) + ")";
                insertSQL += values;

                return (int)(decimal)this.ExecuteStoredProcedureValuesOnly("usp_Insert_Row", "'" + insertSQL.Replace("'", "''") + "'");
            }

            public void UpdateRow(string table, Hashtable newRow, Hashtable oldRow,
                Hashtable parms, int userID)
            {
                string selectSQL, updateSQL, value;

                //update row
                updateSQL = "update [" + table + "] set Modified_By=" + userID + ",Modified_Date='" + DateTime.Now.ToString()+"',";

                if (newRow.Contains("Modified_By"))
                {
                    newRow.Remove("Modified_By");
                }

                if (newRow.Contains("Modified_Date"))
                {
                    newRow.Remove("Modified_Date");
                }

                if (oldRow != null)
                {
                    if (oldRow.Contains("Modified_By"))
                    {
                        oldRow.Remove("Modified_By");
                    }

                    if (oldRow.Contains("Modified_Date"))
                    {
                        oldRow.Remove("Modified_Date");
                    }
                }
              
                foreach (string key in newRow.Keys)
                {
                    if (key.ToUpper() != (table + "_Id").ToUpper()) //updating primary key causes errors
                    {
                        value = newRow[key].ToString().Trim();
                        if (ShouldUpdateColumn(newRow, key, oldRow))
                        {
                            if (isStringValue(value) || forceString(key))
                            {
                                switch (value)
                                {
                                    case "True":
                                        value = "1";
                                        break;
                                    case "False":
                                        value = "0";
                                        break;
                                    default:
                                        value = "'" + value.Trim().Replace("'", "''") + "'";
                                        break;
                                }
                             }
                            updateSQL += key + "=" + value + ",";
                        }
                    }
                }

                updateSQL = updateSQL.Substring(0, updateSQL.Length - 1) + " ";
                updateSQL += "where " + table + "_Id=" + newRow[table+"_Id"];

                DateTime startTime = DateTime.Now;
                db.ExecSQLUpdateStmt(updateSQL);
                Log(updateSQL,"", DateTime.Now.Subtract(startTime));
            }

      

        public void DeleteAllRows(string table)
            {
                //Deletes all rows from a table
                string sql = "delete from " + table;
                DateTime startTime = DateTime.Now;
                db.ExecSQLDeleteStmt(sql);
                Log(sql, "", DateTime.Now.Subtract(startTime));
            }

            public void DeleteRows(ref string table, ref ArrayList ids)
            {
                foreach (int id in ids)
                {
                    this.DeleteRow(table, id);
                }
            }

            public void DeleteRow(string table, int id)
            {
                string sql = "delete from [" + table + "] where " + table + "_Id=" + id;
                DateTime startTime = DateTime.Now;
                db.ExecSQLDeleteStmt(sql);
                Log(sql, "", DateTime.Now.Subtract(startTime));
            }
        #endregion
    #endregion

    #region Log
            private const string LOG_FILE_NAME = "sql.log";
            private bool m_SqlLogging = false;

        public bool SqlLogging
        {
            get { return m_SqlLogging; }
            set { m_SqlLogging = value; }
        }

        protected void Log(string sql, List<SqlParameter> parms, TimeSpan elapsedTime)
        {
            //Writes to log

            //Displays the query executed in the debug window
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("T") + " " + sql);

            if (!SqlLogging)
            {
                return;
            }

            StreamWriter sw = new StreamWriter(LOG_FILE_NAME, true);
            string s=DateTime.Now.ToLongTimeString() + "\t" + elapsedTime.TotalMilliseconds.ToString() + "\t" + sql;
        
            if (parms != null)
            {
                if (parms.Count > 0)
                {
                    s += " " + parms[0].Value.ToString();
                    for (int i=1; i<parms.Count; i++)
                    {
                        s += ", " + parms[i].Value.ToString();
                    }
                }
            }

            sw.WriteLine(s);
            sw.Close();
        }

        protected void Log(string sql, string parms, TimeSpan elapsedTime)
        {
            //Writes to log

            //Displays the query executed in the debug window
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("T") + " " + sql);

            if (!SqlLogging)
            {
                return;
            }

            StreamWriter sw = new StreamWriter(LOG_FILE_NAME, true);
            string s = DateTime.Now.ToLongTimeString() + "\t" + elapsedTime.TotalMilliseconds.ToString() + "\t" + sql;

            if ((parms != null) && (parms.Length > 1))
            {
                string[] parameters = parms.Split(',');
                s += " " + parameters[0];
                for (int i = 1; i < parameters.Length; i++)
                {
                    s += ", " + parameters[i];
                }
            }

            sw.WriteLine(s);
            sw.Close();
        }
    #endregion

    #region Misc Functions
        public Hashtable DataTableRowToHashtable(DataTable Table, DataRow Row)
        {
            //Changes datarow into hashtable
            Hashtable HashRow = new Hashtable();
            string ColumnName = "";

            for(int i= 0; i<Row.ItemArray.Length; i++)
            {
                if (!Row.IsNull(i))
                {
                    ColumnName = Table.Columns[i].ColumnName;
                    if (!HashRow.ContainsKey(ColumnName))
                    {
                        if (Row[i].GetType() == typeof(string))
                        {
                            HashRow.Add(Table.Columns[i].ColumnName, Row[i].ToString().Trim());
                        }
                        else
                        {
                            HashRow.Add(Table.Columns[i].ColumnName, Row[i]);
                        }
                    }

                }
            }
            return HashRow;
        }

        public static List<Hashtable> DataTableToHashtableList(DataTable Table)
        {
            //Converts a data table into a list of hashtables

            List<Hashtable> HashList = new List<Hashtable>();
            foreach (DataRow Row in Table.Rows)
            {
                Hashtable HashRow = new Hashtable();
                string ColumnName = "";

                for (int i = 0; i < Row.ItemArray.Length; i++)
                {
                    if (!Row.IsNull(i))
                    {
                        ColumnName = Table.Columns[i].ColumnName;
                        if (!HashRow.ContainsKey(ColumnName))
                        {
                            if (Row[i].GetType() == typeof(string))
                            {
                                HashRow.Add(Table.Columns[i].ColumnName, Row[i].ToString().Trim());
                            }
                            else
                            {
                                HashRow.Add(Table.Columns[i].ColumnName, Row[i]);
                            }
                        }

                    }
                }
                HashList.Add(HashRow);
            }
            return HashList;
        }

        private static bool isStringValue(string value)
        {
            char[] c = value.ToCharArray();
            Int32 periods = 0;

            if ((value == "") || (value == null))
                return true;

            for (int i=0;i<c.Length;i++)
            {
                if ((!isDigit(c[i])) && (c[i] != '.') && (c[i] != '-'))
                    return true;

                if (c[i] == '.')
                    periods++;

                if ((c[i]=='-') && (i!=0))
                {
                    /* If a dash is not in the first positition, it's a string.
                     * We have had problems with phone numbers (555-5555) being interpreted
                     * as a number - isnumeric() returns true.
                    */
                    return true;
                }
            }

            if (periods >= 2)
                return true;
            
            return false;
        }

        private static bool isDigit(char m)
        {
            if (m == '0') { return true; }
            if (m == '1') { return true; }
            if (m == '2') { return true; }
            if (m == '3') { return true; }
            if (m == '4') { return true; }
            if (m == '5') { return true; }
            if (m == '6') { return true; }
            if (m == '7') { return true; }
            if (m == '8') { return true; }
            if (m == '9') { return true; }
            return false;
        }

        private bool forceString(string columnName)
        {
            /*Returns true if the value should be interpreted as a string,
             * regardless of its value.
            */

            if ((columnName == "Explanation_Of_Proceeds") ||
                (columnName == "Tax_Id") ||
                (columnName == "Police_Report_Number") ||
                (columnName == "First_Name") ||
                (columnName == "Last_Name") ||
                (columnName == "Zipcode") ||
                (columnName == "Report_Type") ||
                (columnName == "Police_Report_Date_Of_Loss") ||
                (columnName == "Insured_Date_Of_Birth") ||
                (columnName == "File_Note_Text") ||
                (columnName == "Other_Insurance_Policy_No") ||
                (columnName == "Other_Insurance_Claim_No"))
                return true;
            return false;
        }

        private bool ShouldUpdateColumn(Hashtable newRow, string key, Hashtable oldRow = null )
        {
            try
            {
                if (oldRow == null)
                    return true;

                if (!oldRow.ContainsKey(key))
                    return true;

                if (!newRow.ContainsKey(key))
                    return false;

                if (oldRow[key].ToString() == newRow[key].ToString())
                    return false;
                return true;
            }
            catch (Exception e)
            {
                return true;
            }
        }
    #endregion
    }
}

Key Insurance Company
P.O. Box 201
Overland Park, KS 66201

#DATE#

#APPRAISER_FIRST_NAME#,

Please complete the assignment for the following claim.  When you are done, reply to this email, attaching all related photos and documents.  You must submit this assignment to us by replying to this email. You must not change the return email address or the subject line in any way. 

Thank you,

#ADJUSTER_NAME#, Adjuster
Phone: (913) 663-5500 x#ADJUSTER_EXT#

 
Assignment  Info
----------------------------------------------------------------
 
Appraiser: #APPRAISER_NAME#
Insured or Claimant: #INSURED_OR_CLAIMANT#
Task Description: #TASK_DESCRIPTION#

Special Instructions: #SPECIAL_INSTRUCTIONS#


Vehicle Info
----------------------------------------------------------------
 
Vehicle: #VEHICLE_NAME#
VIN: #VIN#
Color: #VEHICLE_COLOR#
License Plate State: #LICENSE_PLATE_STATE#
License Plate: #LICENSE_PLATE#
Location Contact: 
	#VEHICLE_LOCATION_CONTACT#

Location: 
	#VEHICLE_LOCATION#
 
Damage Description: #DAMAGE_DESCRIPTION#
Where Seen: #WHERE_SEEN#
Estimated Damage Amount: #ESTIMATED_DAMAGE_AMOUNT#


Claim Info
----------------------------------------------------------------
 
Claim Number: #CLAIM_NUMBER#
Policy Number: #POLICY_NUMBER#
Date of Loss: #DATE_OF_LOSS#
Deductible: #DEDUCTIBLE#
Type of Claim: #TYPE_OF_CLAIM#
Loss Description: #LOSS_DESCRIPTION#

Insured: #INSURED_INFO

Insured Vehicle: #INSURED_VEHICLE#

Claimant: #CLAIMANT_INFO#


ImageRight Info - Do Not Change
----------------------------------------------------------------
DRAWER: CLMS
FILENO: #CLAIM_NUMBER#
PACKAGETYPE: 30800
DOCTYPE: APPR
FLOW: 20
STEP: 67
TASKPRIORITY: 5

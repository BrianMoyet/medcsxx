USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_BI_Diagnostic_Report]    Script Date: 10/15/2020 11:35:41 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[usp_Get_BI_Diagnostic_Report] 
@bi_evaluation_id int
AS

SELECT        d.BI_Diagnostic_Id, d.Diagnostic_Date, d.Best_Case_Cost, d.Worst_Case_Cost, v.Name AS Ordered_By, dt.Description AS Diagnostic_Type, d.BI_Diagnostic_Type_Id, d.Ordered_By_Id
FROM            BI_Diagnostic AS d INNER JOIN
                         BI_Diagnostic_Type AS dt ON d.BI_Diagnostic_Type_Id = dt.BI_Diagnostic_Type_Id INNER JOIN
                         Vendor AS v ON d.Ordered_By_Id = v.Vendor_Id
WHERE        (d.BI_Evaluation_Id = @bi_evaluation_id)
ORDER BY d.Diagnostic_Date
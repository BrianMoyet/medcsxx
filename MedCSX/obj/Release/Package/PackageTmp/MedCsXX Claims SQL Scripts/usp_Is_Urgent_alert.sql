USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Is_Urgent_alert]    Script Date: 11/16/2020 11:38:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Is_Urgent_alert]
	@file_note_id int,
	@sent_to_user_id int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT       alert_id
                        
FROM            Alert
WHERE        (Show_Alert = 1) AND (File_Note_Id = @file_note_id) AND (Sent_To_User_Id = @sent_to_user_id)
END

USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Fraud_Claims_For_Id]    Script Date: 8/20/2020 9:34:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		James Willoughby
-- Create date: 3/23/2018
-- Description:	Returns fraud claims associated with vendor, person, or claim for provided 
--				vendor_id, person_id, and/or claim_id.  There will always be a claim_id
--				associatiated with the fraud claim.				
-- =============================================
Alter PROCEDURE [dbo].[usp_Get_Fraud_Claims_For_Id]
	-- Add the parameters for the stored procedure here
	@claimId int,
	@vendorId int = 0,
	@personId int = 0,
	@vehicleId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	if @vendorId <> 0
		begin
		SELECT        NICB.NICB_Id AS Id, NICB.Vendor_Id, NICB.Person_Id, NICB.Vehicle_Id, NICB.Claim_Id, NICB.Person_Type_Id, NICB.NICB_Type_Id, NICB.Description, NICB_Type.Description AS NICBDescription, 
                         Person.First_Name + ' ' + Person.Last_Name AS PersonName, Vehicle.Year_Made + ' ' + Vehicle.Manufacturer + ' ' + Vehicle.Model + ' ' + Vehicle.VIN AS VehicleName
FROM            NICB INNER JOIN
                         NICB_Type ON NICB.NICB_Type_Id = NICB_Type.NICB_Type_Id INNER JOIN
                         Person ON NICB.Person_Id = Person.Person_Id INNER JOIN
                         Vehicle ON NICB.Vehicle_Id = Vehicle.Vehicle_Id where nicb.Vendor_Id = @vendorId 
		end
	else if @personId <> 0
		begin
		SELECT        NICB.NICB_Id AS Id, NICB.Vendor_Id, NICB.Person_Id, NICB.Vehicle_Id, NICB.Claim_Id, NICB.Person_Type_Id, NICB.NICB_Type_Id, NICB.Description, NICB_Type.Description AS NICBDescription, 
                         Person.First_Name + ' ' + Person.Last_Name AS PersonName, Vehicle.Year_Made + ' ' + Vehicle.Manufacturer + ' ' + Vehicle.Model + ' ' + Vehicle.VIN AS VehicleName
FROM            NICB INNER JOIN
                         NICB_Type ON NICB.NICB_Type_Id = NICB_Type.NICB_Type_Id INNER JOIN
                         Person ON NICB.Person_Id = Person.Person_Id INNER JOIN
                         Vehicle ON NICB.Vehicle_Id = Vehicle.Vehicle_Id where nicb.Person_Id = @personId
		end
	else if @vehicleId <> 0
		begin
		SELECT        NICB.NICB_Id AS Id, NICB.Vendor_Id, NICB.Person_Id, NICB.Vehicle_Id, NICB.Claim_Id, NICB.Person_Type_Id, NICB.NICB_Type_Id, NICB.Description, NICB_Type.Description AS NICBDescription, 
                         Person.First_Name + ' ' + Person.Last_Name AS PersonName, Vehicle.Year_Made + ' ' + Vehicle.Manufacturer + ' ' + Vehicle.Model + ' ' + Vehicle.VIN AS VehicleName
FROM            NICB INNER JOIN
                         NICB_Type ON NICB.NICB_Type_Id = NICB_Type.NICB_Type_Id INNER JOIN
                         Person ON NICB.Person_Id = Person.Person_Id INNER JOIN
                         Vehicle ON NICB.Vehicle_Id = Vehicle.Vehicle_Id where nicb.Vehicle_Id = @vehicleId 
		end
	else
		begin
		SELECT        NICB.NICB_Id AS Id, NICB.Vendor_Id, NICB.Person_Id, NICB.Vehicle_Id, NICB.Claim_Id, NICB.Person_Type_Id, NICB.NICB_Type_Id, NICB.Description, NICB_Type.Description AS NICBDescription, 
                         Person.First_Name + ' ' + Person.Last_Name AS PersonName, Vehicle.Year_Made + ' ' + Vehicle.Manufacturer + ' ' + Vehicle.Model + ' ' + Vehicle.VIN AS VehicleName
FROM            NICB INNER JOIN
                         NICB_Type ON NICB.NICB_Type_Id = NICB_Type.NICB_Type_Id INNER JOIN
                         Person ON NICB.Person_Id = Person.Person_Id INNER JOIN
                         Vehicle ON NICB.Vehicle_Id = Vehicle.Vehicle_Id where nicb.Claim_Id = @claimId
		end
END
GO


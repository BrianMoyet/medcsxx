USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_My_Reserves]    Script Date: 7/10/2020 2:08:08 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



ALTER  PROCEDURE [dbo].[usp_Get_My_Reserves] 
@user_id foreign_key
AS

select r.Reserve_Id, c.Display_Claim_id, dbo.get_person_name(cl.person_id) as [Claimant],
rt.description as [Reserve_Type], rs.description as [Reserve_Status],
case when r.is_case_reserve=1 then 'Case' else 'Average' end as Reserving,
c.Date_Reported as Date_Open, r.Gross_Loss_Reserve, r.Net_Loss_Reserve, r.Paid_Loss, r.Gross_Expense_Reserve, 
r.Net_Expense_Reserve, r.Paid_Expense, c.Policy_No, dbo.get_person_name(c.insured_person_id) as Insured,
c.Date_Of_Loss, rt.Image_Index,
ug.description as User_Group,
case when r.assigned_to_user_group_id = 0 then dbo.get_user_display_name(@user_id) else ug.description end as Assigned_To,
r.Reserve_Id as Id,
dbo.Reserve_Locked(r.Reserve_Id) as [Reserve_Locked], dbo.Claim_Locked(c.claim_id) as [Claim_Locked],
dbo.Reserve_Closed(r.Reserve_Id) as [Reserve_Closed], dbo.Reserve_Closed_Pending_Salvage(r.Reserve_Id) as [Reserve_Closed_Pending_Salvage], 
dbo.Reserve_Closed_Pending_Subro(r.Reserve_Id) as [Reserve_Closed_Pending_Subro], c.Claim_Id
from reserve r, claimant cl, claim c, reserve_type rt, reserve_status rs, user_group ug
where r.claimant_id = cl.claimant_id
and cl.claim_id = c.claim_id
and (r.adjuster_id=@user_id) -- or dbo.is_user_in_user_group(@user_id, r.assigned_to_user_group_id) = 1)  COMMENTED OUT FOR PERFORMANCE - PUT BACK IN IF USER GROUPS ARE EVER USED FOR REAL
and rt.reserve_type_id = r.reserve_type_id
and rs.reserve_status_id = r.reserve_status_id
and r.reserve_status_id not in (5,7) --closed, void
and r.assigned_to_user_group_id = ug.user_group_id
order by is_case_reserve, r.date_open
GO



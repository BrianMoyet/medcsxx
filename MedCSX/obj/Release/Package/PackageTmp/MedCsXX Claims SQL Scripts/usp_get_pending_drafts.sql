USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Pending_Drafts]    Script Date: 8/26/2020 1:15:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Get_Pending_Drafts]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT    Pending_Draft.Pending_Draft_Id, Pending_Draft.Pending_Draft_Status_Id, Pending_Draft.Pending_Draft_Sent_To, Pending_Draft.Reserve_Id, Pending_Draft.Transaction_Id, Pending_Draft.Transaction_Type_Id, 
                         Pending_Draft.Draft_Print_Id, Pending_Draft.Draft_Id, Pending_Draft.Created_By, Pending_Draft.Modified_By, Pending_Draft.Created_Date, Pending_Draft.Modified_Date, Pending_Draft_Status.Description AS draft_status, 
                         Draft.Payee, Draft.Draft_Amount
FROM            Pending_Draft INNER JOIN
                         Pending_Draft_Status ON Pending_Draft.Pending_Draft_Status_Id = Pending_Draft_Status.Pending_Draft_Status_Id INNER JOIN
                         [Transaction] ON Pending_Draft.Transaction_Id = [Transaction].Transaction_Id INNER JOIN
                         Draft ON [Transaction].Draft_Id = Draft.Draft_Id
END
GO


USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_BI_Treatment_Report]    Script Date: 10/19/2020 2:45:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Get_BI_Treatment_Report] 
@bi_evaluation_id int
AS
SELECT        t.BI_Treatment_Id, v.Name AS Treating_Doctor, t.Treatment_Start_Date, t.Treatment_End_Date, t.Nbr_Treatments, dbo.Yes_No(t.Chronic) AS Chronic, t.Cost_Best_Case, t.Cost_Worst_Case, t.Report_Fees, v.Vendor_Id, 
                         t.Chronic AS Expr1, t.BI_Diagnosis_Type_Id, dt.Description

FROM            BI_Treatment AS t INNER JOIN
                         BI_Diagnosis_Type AS dt ON t.BI_Diagnosis_Type_Id = dt.BI_Diagnosis_Type_Id INNER JOIN
                         Vendor AS v ON t.Treating_Doctor_Id = v.Vendor_Id
WHERE        (t.BI_Evaluation_Id = @bi_evaluation_id)
ORDER BY t.Treatment_Start_Date

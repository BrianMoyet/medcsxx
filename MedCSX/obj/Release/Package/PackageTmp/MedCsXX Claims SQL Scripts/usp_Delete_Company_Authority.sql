USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Delete_Company_Authority]    Script Date: 10/8/2020 2:53:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Delete_Company_Authority] 
@User_Id int
AS

DELETE 
FROM            User_Company_Authority
WHERE        (User_Id = @User_Id)

GO


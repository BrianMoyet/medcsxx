USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Diagnostic_Types]    Script Date: 10/9/2020 2:05:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Get_Diagnostic_Types] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [BI_Diagnostic_Type_Id], [Description] FROM [BI_Diagnostic_Type] ORDER BY [Description]
END
GO


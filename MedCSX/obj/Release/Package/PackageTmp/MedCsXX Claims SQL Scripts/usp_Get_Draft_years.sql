-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE usp_Get_Draft_years

AS
BEGIN
SELECT DISTINCT LTRIM(STR(YEAR(t.Created_Date))) AS Years
FROM            Draft AS d INNER JOIN
                         [Transaction] AS t ON d.Draft_Id = t.Draft_Id
WHERE        (d.Draft_Id <> 0)
GROUP BY LTRIM(STR(MONTH(t.Created_Date))) + '/' + LTRIM(STR(DAY(t.Created_Date))) + '/' + LTRIM(STR(YEAR(t.Created_Date))), YEAR(t.Created_Date), MONTH(t.Created_Date), DAY(t.Created_Date)
ORDER BY Years
END
GO

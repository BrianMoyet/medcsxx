/*
   Thursday, March 4, 20213:43:43 PM
   User: 
   Server: qasql4
   Database: Claims_Encrypt
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Person ADD
	Address1_Enc varbinary(MAX) NULL,
	Address2_Enc varbinary(MAX) NULL,
	City_Enc varbinary(MAX) NULL,
	State_Id_Enc varbinary(MAX) NULL,
	Zipcode_Enc varbinary(MAX) NULL,
	Home_Phone_Enc varbinary(MAX) NULL,
	Work_Phone_Enc varbinary(MAX) NULL,
	Cell_Phone_Enc varbinary(MAX) NULL,
	Other_Contact_Phone_Enc varbinary(MAX) NULL,
	Fax_Enc varbinary(MAX) NULL,
	Email_Enc varbinary(MAX) NULL,
	Date_Of_Birth_Enc varbinary(MAX) NULL,
	SSN_Enc varbinary(MAX) NULL,
	Drivers_License_No_Enc varbinary(MAX) NULL,
	Sex_Enc varbinary(MAX) NULL,
	Employer_Enc varbinary(MAX) NULL,
	Language_Id_Enc varbinary(MAX) NULL,
	Comments_Enc varbinary(MAX) NULL,
	Other_Insurance_Policy_No_Enc varbinary(MAX) NULL,
	Other_Insurance_Company_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_Name_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_Address_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_City_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_State_Id_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_ZipCode_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_Phone_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_Fax_Enc varbinary(MAX) NULL,
	Other_Insurance_Agent_Email_Enc varbinary(MAX) NULL,
	Other_Insurance_Claim_No_Enc varbinary(MAX) NULL,
	Salutation_Enc varbinary(MAX) NULL
GO
ALTER TABLE dbo.Person SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

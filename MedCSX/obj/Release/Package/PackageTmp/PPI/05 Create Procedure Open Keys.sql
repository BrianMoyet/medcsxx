CREATE PROCEDURE OpenKeys
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;
    END TRY
    BEGIN CATCH
        -- Handle non-existant key here
    END CATCH
END
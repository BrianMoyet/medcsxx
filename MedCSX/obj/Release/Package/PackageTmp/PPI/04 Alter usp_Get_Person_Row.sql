USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Person_Row]    Script Date: 3/18/2021 11:20:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Get_Person_Row]
	
	@person_id	int,
	@claimType	int = 0
	
AS


--DECLARE @open nvarchar(200), @close nvarchar(200)
--SET @open = 'OPEN SYMMETRIC KEY SymKey_Encrypt DECRYPTION BY CERTIFICATE Certificate_Encrypt;';
--SET @close = 'CLOSE SYMMETRIC KEY SymKey_Encrypt;';

--EXEC sp_executesql @open

OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;


	If @claimType = 0




SELECT       Person_Id, Claim_Id, Person_Type_Id, Policy_Individual_Id, 
dbo.fn_Decrypt(Salutation) as Salutation, 
Last_Name, 
Middle_Initial, 
First_Name, 
Suffix, 
dbo.fn_Decrypt(Address1) as Address1, 
dbo.fn_Decrypt(Address2) as Address2, 
dbo.fn_Decrypt(City) as City, 
Convert(int, CONVERT(varchar, dbo.fn_Decrypt(State_Id))) as State_Id, 
dbo.fn_Decrypt(Zipcode) as Zipcode, 
dbo.fn_Decrypt(Home_Phone) as Home_Phone, 
dbo.fn_Decrypt(Work_Phone) as Work_Phone, 
dbo.fn_Decrypt(Cell_Phone) as Cell_Phone, 
dbo.fn_Decrypt(Other_Contact_Phone) as Other_Contact_Phone, 
dbo.fn_Decrypt(Fax) as Fax, 
dbo.fn_Decrypt(Email) as Email, 
Convert(datetime ,CONVERT(varchar, DecryptByKey(Date_Of_Birth))) AS 'Date_Of_Birth', 
dbo.fn_Decrypt(SSN) AS 'SSN', 
dbo.fn_Decrypt(Drivers_License_No) as Drivers_License_No, 
dbo.fn_Decrypt(Sex) as Sex, 
dbo.fn_Decrypt(Employer) as Employer, 
Convert(int, CONVERT(varchar, dbo.fn_Decrypt(Language_Id)))  as Language_Id, 
Created_By, Modified_By, Created_Date, Modified_Date, Is_Company,
dbo.fn_Decrypt(Comments) as Comments, 
Flag_Fraud, 
dbo.fn_Decrypt(Other_Insurance_Policy_No) as Other_Insurance_Policy_No, 
dbo.fn_Decrypt(Other_Insurance_Company) as Other_Insurance_Company, 
dbo.fn_Decrypt(Other_Insurance_Agent_Name) as Other_Insurance_Agent_Name, 
dbo.fn_Decrypt(Other_Insurance_Agent_Address) as Other_Insurance_Agent_Address, 
dbo.fn_Decrypt(Other_Insurance_Agent_City) as Other_Insurance_Agent_City, 
Convert(int, CONVERT(varchar, dbo.fn_Decrypt(Other_Insurance_Agent_State_Id))) as Other_Insurance_Agent_State_Id, 
dbo.fn_Decrypt(Other_Insurance_Agent_ZipCode) as Other_Insurance_Agent_ZipCode, 
dbo.fn_Decrypt(Other_Insurance_Agent_Phone) as Other_Insurance_Agent_Phone, 
dbo.fn_Decrypt(Other_Insurance_Agent_Fax) as Other_Insurance_Agent_Fax, 
dbo.fn_Decrypt(Other_Insurance_Agent_Email) as Other_Insurance_Agent_Email, 
dbo.fn_Decrypt(Other_Insurance_Claim_No) as Other_Insurance_Claim_No, 
Has_Other_Insurance, 
'person' AS PersonSource
FROM            Person
WHERE        (Person_Id = @person_id)
ORDER BY Person_Id DESC
	Else
		SELECT *, [Name] As First_Name, Addr1 As Address1, Addr2 As Address2, 
			dbo.fn_State_Id_For_State_Abbrv(State) As State_Id, Zip As Zipcode, Email_Address As Email,
			Phone As Home_Phone, Company As Is_Company, Insured_dob As Date_Of_Birth, 'insured' As PersonSource
		FROM GAAS01.dbo.Insured 
		WHERE Insured_Key = @person_id --London

		--EXEC sp_executesql @close

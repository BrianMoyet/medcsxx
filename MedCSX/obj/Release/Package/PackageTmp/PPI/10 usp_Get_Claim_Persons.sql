USE [Claims_Encrypt]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Claim_Persons]    Script Date: 3/17/2021 10:37:42 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/* Get all persons related to a claim (insured, owner, driver, prop damage owner & driver, etc. */

ALTER PROCEDURE [dbo].[usp_Get_Claim_Persons] 
@claim_id foreign_key
AS

/* return person rows */
select distinct p.* , s.abbreviation as [State], pt.Image_Index, dbo.get_person_type(person_id) as Type, dbo.get_person_name(p.person_id) as Name
from person p, state s, person_type pt
where p.person_id <> 0 and s.state_id = dbo.fn_decrypt(p.state_id) and
p.person_type_id = pt.person_type_id and 
--dbo.get_person_name(p.person_id) <> 'Parked and Unoccupied' and
claim_id = @claim_id
GO


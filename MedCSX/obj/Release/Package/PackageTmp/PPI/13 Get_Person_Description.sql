USE [Claims_Encrypt]
GO
/****** Object:  UserDefinedFunction [dbo].[Get_Person_Description]    Script Date: 3/17/2021 3:58:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/* Return a Person Row Formatted as a Displayable Name, such as "Mr David A Smith Jr"  */

ALTER  FUNCTION [dbo].[Get_Person_Description] (@person_id foreign_key)
RETURNS varchar(200)  AS  
BEGIN 
	declare @person_description person_description

	if @person_id = 0 
	begin
		set @person_description = ''
	end
	else
	begin
		/* Build Displayable Name */
		select @person_description = dbo.Get_Person_Name(@person_id) + ', ' + dbo.trim(dbo.fn_Decrypt(address1)) + ' ' + dbo.trim(dbo.fn_Decrypt(address2)) + ', ' +
			dbo.trim(dbo.fn_Decrypt(city)) + ', ' + dbo.trim(s.abbreviation) + ' ' + 
			dbo.trim(dbo.fn_Decrypt(zipcode))
		from person p, state s
		where person_id = @person_id and dbo.fn_Decrypt(p.state_id) = s.state_id

		/* Remove spaces before commas */
		set @person_description = replace (@person_description, ' ,', ',')
		set @person_description = replace (@person_description, ' ,', ',')

		/* Eliminate excessive embedded spaces */
		set @person_description = replace (@person_description, '  ', ' ')
		set @person_description = replace (@person_description, '  ', ' ')		
		set @person_description = replace (@person_description, '  ', ' ')
		set @person_description = replace (@person_description, '  ', ' ')
		set @person_description = replace (@person_description, '  ', ' ')

		/* Remove redundant commas (happens with 'Unknown') */
		set @person_description = replace (@person_description, ',,,', '')

		set @person_description = dbo.trim(@person_description)
	end

	return @person_description
END


































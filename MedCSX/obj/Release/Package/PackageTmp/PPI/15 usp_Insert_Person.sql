USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Insert_Person]    Script Date: 3/18/2021 11:50:41 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_Insert_Person] 
@claim_id int,
@person_type_id int,
@policy_individual_id int,
@salutation salutation,
@last_name last_name,
@middle_initial middle_initial,
@first_name first_name,
@suffix suffix,
@address1 address,
@address2 address,
@city city,
@state_id int,
@zipcode zipcode,
@home_phone phone_number,
@work_phone phone_number,
@cell_phone phone_number,
@other_contact_phone phone_number,
@fax phone_number,
@email email,
@date_of_birth date_with_default,
@ssn ssn,
@drivers_license_no drivers_license_no,
@sex sex,
@employer employer,
@language_id int,
@is_company bit,
@user_id created_by,
@comments varchar(max) = '',
@fraudInd bit = 0,
@hasOtherIns bit = 0,
@OtherInsCompany varchar(64) = '',
@OtherInsPolicyNo varchar(32) = '',
@OtherInsClaimNo varchar(32) = '',
@OtherInsAgent varchar(64) = '',
@OtherInsAgentAddress address = '',
@OtherInsAgentCity city = '',
@OtherInsAgentStateId int = 0,
@OtherInsAgentZip zipcode = '',
@OtherInsAgentPhone phone_number = '',
@OtherInsAgentFax phone_number = '',
@OtherInsAgentEmail email = ''
AS

declare @person_id int

OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;

INSERT INTO Person (claim_id, person_type_id, policy_individual_id, salutation, last_name, middle_initial, first_name, suffix,
address1, address2, city, state_id, zipcode, home_phone, work_phone, cell_phone, other_contact_phone, fax,
email, date_of_birth, SSN, drivers_license_no, sex, employer, language_id, is_company, created_by, modified_by, Comments,
Flag_Fraud, Has_Other_Insurance, Other_Insurance_Company, Other_Insurance_Policy_No, Other_Insurance_Claim_No,
Other_Insurance_Agent_Name, Other_Insurance_Agent_Address, Other_Insurance_Agent_City, Other_Insurance_Agent_State_Id,
Other_Insurance_Agent_ZipCode, Other_Insurance_Agent_Phone, Other_Insurance_Agent_Fax, Other_Insurance_Agent_Email)
VALUES (@claim_id, @person_type_id, @policy_individual_id,  dbo.fn_Encrypt(@salutation), @last_name, @middle_initial, @first_name,
@suffix,  dbo.fn_Encrypt(@address1),  dbo.fn_Encrypt(@address2),  dbo.fn_Encrypt(@city),  dbo.fn_Encrypt(@state_id),  dbo.fn_Encrypt(@zipcode),  dbo.fn_Encrypt(@home_phone),  dbo.fn_Encrypt(@work_phone),  dbo.fn_Encrypt(@cell_phone),  dbo.fn_Encrypt(@other_contact_phone),  dbo.fn_Encrypt(@fax), 
 dbo.fn_Encrypt(@email),  dbo.fn_Encrypt(@date_of_birth),  dbo.fn_Encrypt(@ssn),  dbo.fn_Encrypt(@drivers_license_no),  dbo.fn_Encrypt(@sex),  dbo.fn_Encrypt(@employer),  dbo.fn_Encrypt(@language_id), @is_company, @user_id, @user_id,  dbo.fn_Encrypt(@comments),
@fraudInd, @hasOtherIns,  dbo.fn_Encrypt(@OtherInsCompany),  dbo.fn_Encrypt(@OtherInsPolicyNo),  dbo.fn_Encrypt(@OtherInsClaimNo),  dbo.fn_Encrypt(@OtherInsAgent),  dbo.fn_Encrypt(@OtherInsAgentAddress), 
 dbo.fn_Encrypt(@OtherInsAgentCity),  dbo.fn_Encrypt(@OtherInsAgentStateId),  dbo.fn_Encrypt(@OtherInsAgentZip),  dbo.fn_Encrypt(@OtherInsAgentPhone),  dbo.fn_Encrypt(@OtherInsAgentFax),  dbo.fn_Encrypt(@OtherInsAgentEmail))

SELECT TOP 1 @person_id = person_id FROM Person ORDER BY Person_Id Desc
SELECT @person_id AS 'Person_Id'

	CLOSE SYMMETRIC KEY SymKey_Encrypt;


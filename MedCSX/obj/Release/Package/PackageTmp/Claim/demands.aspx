﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master"  CodeBehind="demands.aspx.cs" Inherits="MedCSX.Claim.demands" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-2">
        Demand Amount:
    </div>
    <div class="col-md-10">
        <asp:TextBox ID="txtDemandAmount" runat="server"></asp:TextBox>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Demand Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtDemandDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Received Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtReceivedDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Due Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtDueDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvDemandHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="demand_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   AllowSorting="True" AllowPaging="True"  width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                    <asp:BoundField DataField="reserve_type" HeaderText="Demand / Offer"  SortExpression="reserve_type"/>
                    <asp:BoundField DataField="claimant" HeaderText="Date"  SortExpression="claimant"/>
                    <asp:BoundField DataField="vendor" HeaderText="Received Date"  SortExpression="vendor"/>
                    <asp:BoundField DataField="claimant" HeaderText="Due Date"  SortExpression="claimant"/>
                    <asp:BoundField DataField="claimant" HeaderText="Amount"  SortExpression="claimant"/>
                

            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
   </div>
</div>
</asp:Content>

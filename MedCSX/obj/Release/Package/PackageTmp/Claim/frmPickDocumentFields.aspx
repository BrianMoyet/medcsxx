﻿<%@ Page Title="Pick Document Fields" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPickDocumentFields.aspx.cs" Inherits="MedCSX.Claim.frmPickDocumentFields" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Document:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtDocumentType" ForeColor="Blue" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Author:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtAdjuster"  ForeColor="Blue" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtClaimId"  ForeColor="Blue" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Claimant:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboClaimant"   runat="server" Enabled="False"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Vehicle:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboVehicle" runat="server" EnableTheming="False"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Draft:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboDraft" runat="server" Enabled="False"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click"  />
           
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

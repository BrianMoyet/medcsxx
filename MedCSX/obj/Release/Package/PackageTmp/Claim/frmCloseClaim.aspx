﻿<%@ Page Title="Close Claim" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmCloseClaim.aspx.cs" Inherits="MedCSX.Claim.frmCloseClaim" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
       
    </div>
      <div class="row">
        <div class="col-md-12">
            <h4>The claim has the following uncompleted items:</h4>
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            
             <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="Close Claim and send notifications." OnClick="btnOK_Click" TabIndex="24" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
            
        </div>
    </div>
</asp:Content>

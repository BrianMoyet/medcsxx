﻿<%@ Page Title="Add Demand" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmDemand.aspx.cs" Inherits="MedCSX.Claim.frmDemand" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
    <div class="col-md-2">
        Demand Amount:
    </div>
    <div class="col-md-10">
        <asp:TextBox ID="txtDemandAmount" runat="server"></asp:TextBox>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Demand Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="dtpDemandDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Received Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="dtpReceivedDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Due Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="dtpDueDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>

    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"  />
                             <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
               <asp:GridView ID="grDemandHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ShowFooter="true"
               ForeColor="#333333"   width="100%">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
                  <asp:BoundField DataField="reserveType" HeaderText="Demand/Offer"/>
                  <asp:BoundField DataField="dtOfLoss" HeaderText="Date"/>
                  <asp:BoundField DataField="dtReported" HeaderText="Received Date" />
                  <asp:BoundField DataField="dtDue" HeaderText="Due Date"  />
                  <asp:BoundField DataField="netLossReserve" HeaderText="Amount" DataFormatString="{0:c}" />
                 
               </Columns>
             
               <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
               <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>

    </div>
</asp:Content>

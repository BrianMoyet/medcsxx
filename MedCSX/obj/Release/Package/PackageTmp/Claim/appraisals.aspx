﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master"  CodeBehind="appraisals.aspx.cs" Inherits="MedCSX.Claim.appraisals" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="row">
            <div class="col-md-2">
                <asp:ListView ID="lvTasks" runat="server"></asp:ListView>
                
            </div>
            <div class="col-md-10">
                  <div class="row">
                    <div class="col-md-12">
                        <b>General</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Claim: <asp:Label ID="lblClaim1" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;&nbspPolicy: <asp:Label ID="lblPolicy" runat="server" Text="Label"></asp:Label>
                        <div class="row">
                            <div class="col-md-3">
                                    Type of Claim: 
                            </div>
                            <div class="col-md-9">
                                    <asp:TextBox ID="txtTypeOfClaim" runat="server"></asp:TextBox>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-3">
                                   Investigation Type: 
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList ID="ddlInvestigationType" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                   Appraiser: 
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList ID="ddlAppraiser" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12">
                                   Task Description:
                                <br />
                                <asp:TextBox ID="txtTaskDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6"> <%--"Third" Column--%>
                        DOL: <asp:Label ID="lblDOL" runat="server" Text="Label"></asp:Label>&nbps;&nbsp;&nbsp;Deductible: <asp:Label ID="lblDeductible" runat="server" Text="Label"></asp:Label>
                        <br />Special Instructions:
                        <br /><asp:TextBox ID="txtSpecialInstructions" runat="server" TextMode="MultiLine"></asp:TextBox>
                        <br />Point of Impact / Loss Description
                        <br /><asp:TextBox ID="txtPointOfImpact" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12">
                            Initial Estimate: <asp:TextBox ID="txtInitialEstimate" runat="server"></asp:TextBox>&nbsp;<asp:TextBox ID="txtDate" runat="server" TextMode="Date"></asp:TextBox>&nbsp;<asp:Button ID="btnAddSupplemntal" runat="server" Text="Add Supplemental" OnClick="btnAddSupplemntal_Click" />&nbsp;Total Estimate: <asp:Label ID="lblTotalEstimate" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-4">
                        <b>Insurance Company</b>
                        <br /><asp:Label ID="lblInsuranceCompany" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <b>Insured</b>
                        <br /><asp:Label ID="lblInsured" runat="server" Text="Label"></asp:Label>
                        <br /><asp:HyperLink ID="hprEditInsured"  class="pull-right" runat="server">Edit</asp:HyperLink>
                    </div>
                    <div class="col-md-4">
                        <b>Claimant</b>
                        <br /><asp:Label ID="lblClaimant" runat="server" Text="Label"></asp:Label>
                         <br /><asp:HyperLink ID="hprEditClaimant"  class="pull-right" runat="server">Edit</asp:HyperLink>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-4">
                        <b>Vehicle</b>
                        <br /><asp:Label ID="lblVehicle" runat="server" Text="Label"></asp:Label>
                         <br /><asp:HyperLink ID="hprEditVehicle"  class="pull-right" runat="server">Edit</asp:HyperLink>
                    </div>
                    <div class="col-md-4">
                        <b>Location of Vehicle</b>
                        <br /><asp:Label ID="lblLocationOfVehicle" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="col-md-4">
                   
                    </div>
                </div>
            </div>
        </div>
     <div class="row">
         <div class="col-md-12">
               
               <asp:Button ID="btnSubmitRequest" runat="server" class="btn btn-info btn-xs"  Text="Submit Request" OnClick="btnSubmitRequest_Click"  />
               <asp:Button ID="btnResendRequest" runat="server" class="btn btn-info btn-xs"  Text="btnResendRequest" onClick="btnResendRequest_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
        </div>
    </div>
  </asp:Content>
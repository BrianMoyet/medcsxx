﻿<%@  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmRequestPoliceReport.aspx.cs" Inherits="MedCSX.Claim.frmRequestPoliceReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-2">
            Vehicle
        </div>
    </div>
     <div class="row">
        <div class="col-md-2">
            <b>Vehicle</b>
        </div>
             <div class="col-md-2">
                 <asp:DropDownList ID="cboInsuredVehicle" runat="server"></asp:DropDownList>
        </div>
    </div>
      <div class="row">
        <div class="col-md-4">  <%--LOSS LOCATION--%>
           <div class="row">
               <div class="col-md-12">
                   Loss Location
               </div>
           </div>
             <div class="row">
               <div class="col-md-2">
                   Street:
               </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtLocStreet" runat="server"></asp:TextBox>
               </div>
           </div>
             <div class="row">
               <div class="col-md-2">
                   City:
               </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtLocCity" runat="server"></asp:TextBox>
               </div>
           </div>
             <div class="row">
               <div class="col-md-2">
                   County:
               </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtLocCounty" runat="server"></asp:TextBox>
               </div>
           </div>
             <div class="row">
               <div class="col-md-2">
                   State:
               </div>
                  <div class="col-md-10">
                      <asp:DropDownList ID="cboLocState" runat="server"></asp:DropDownList>
               </div>
           </div>
        </div>
             <div class="col-md-8"> <%--People involved--%>
                <div class="row">
                    <div class="col-md-12">
                        <b>People Involved</b>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-2">
                       
                    </div>
                       <div class="col-md-5">
                        First Name
                    </div>
                       <div class="col-md-5">
                       Last Name
                    </div>
                </div>
                   <div class="row">
                    <div class="col-md-2">
                       Primary
                    </div>
                       <div class="col-md-5">
                           <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </div>
                       <div class="col-md-5">
                           <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-2">
                       2nd Party
                    </div>
                       <div class="col-md-5">
                           <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    </div>
                       <div class="col-md-5">
                           <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-2">
                       3rd Party
                    </div>
                       <div class="col-md-5">
                           <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                    </div>
                       <div class="col-md-5">
                           <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <b>Investigating Agency</b>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        Investigating Agency Name:
    </div>
         <div class="col-md-3">
             <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
    </div>
         <div class="col-md-4">
        
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        Agency Type:
    </div>
         <div class="col-md-3">
             <asp:DropDownList ID="cboAgencyType" runat="server"></asp:DropDownList>
    </div>
         <div class="col-md-4">
        
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
        Precinct, Troop, or Officer:
    </div>
         <div class="col-md-3">
             <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
    </div>
         <div class="col-md-4">
        
    </div>
</div>
   <div class="row">
    <div class="col-md-3">
        Type of Report  Requested:
    </div>
         <div class="col-md-3">
             <asp:DropDownList ID="cboReportType" runat="server"></asp:DropDownList>
    </div>
         <div class="col-md-4">
            Other: <asp:TextBox ID="txtReportTypeOther" runat="server"></asp:TextBox>
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
       Report Number:
    </div>
         <div class="col-md-3">
             <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
    </div>
         <div class="col-md-4">
        
    </div>
</div>
    <div class="row">
    <div class="col-md-12">
        Comments: <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
    </div>
</div>
      <div class="row">
         <div class="col-md-12">
               <asp:Button ID="btnOk" runat="server"  class="btn btn-info btn-xs"  Text="OK"/>
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
        </div>
</asp:Content>

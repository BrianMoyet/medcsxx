﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmReopenReserve.aspx.cs" Inherits="MedCSX.Claim.frmReopenReserve" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            Your Financial Authority (Loss and Expense Compbined):  <asp:Label ID="txtFinancialAuthority" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Loss Reserve: <asp:TextBox ID="txtChangeLoss" runat="server" AutoPostBack="True" OnTextChanged="txtChangeLoss_TextChanged"></asp:TextBox> Expense Reserve: <asp:TextBox ID="txtChangeExpense" runat="server" AutoPostBack="True" OnTextChanged="txtChangeExpense_TextChanged"></asp:TextBox>
        </div>
    </div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnsOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnsOK_Click" />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"   OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

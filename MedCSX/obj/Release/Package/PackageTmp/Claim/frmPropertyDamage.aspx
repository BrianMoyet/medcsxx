﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPropertyDamage.aspx.cs" Inherits="MedCSX.Claim.frmPropertyDamage" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <%--   <style>
        .MyTabStyle .ajax__tab_tab /*inactive tab*/
        { 
         outline: 0; 
        }

        .MyTabStyle .ajax__tab_active .ajax__tab_tab  /*active tab*/
        { 
         color: #ff0000; 
         outline: 0; 
        }
        .MyTabStyle .ajax__tab_header .ajax__tab_inner
         {
             border-color: #ff0000;
             color: #ff0000;
     
         }
        input[type=radio] {
            float: left;
        }

        label {
            margin-left: 2px;
            //display: block;
        }

    </style>--%>


      <div class="row">
            <div class="col-md-2">
              Reserve Type:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="cboReserveType" runat="server">
                       <asp:ListItem Value="11">Property Damage</asp:ListItem>
                   </asp:DropDownList>
            </div>
          
       </div>
    
     <div class="row">
            <div class="col-md-2">
              Property Type:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="cboPropertyType" runat="server" OnSelectedIndexChanged="cboPropertyType_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            </div>
       </div>
   
      <div class="row">
            <div class="col-md-2">
              Where Seen:
                </div>
            <div class="col-md-10">
                <asp:TextBox ID="tbLocation" runat="server" Width="550px"></asp:TextBox>
            
                 (Required for Appraisals)
            </div>
       </div>
    <div class="row">
            <div class="col-md-2">
             Describe Damage:
                </div>
            <div class="col-md-10">
                <asp:TextBox ID="tbDescription" runat="server" Width="550px"></asp:TextBox>
            
             (Required for Appraisals)
            </div>
       </div>
      <div class="row">
            <div class="col-md-2">
              Estimated Amount:
                
            </div>
            <div class="col-md-5">
                <asp:TextBox ID="tbEstimate" runat="server" AutoPostBack="True" OnTextChanged="tbEstimate_TextChanged"></asp:TextBox>
            </div>
       </div>
     <div id="AutoPanel" runat="server">
     <div class="row">
            <div class="col-md-2">
              Auto:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="cboVehicle" runat="server" OnSelectedIndexChanged="cboVehicle_SelectedIndexChanged"></asp:DropDownList>
                  <asp:Button  ID="btnVehicleNew" runat="server" class="btn btn-info btn-xs"  Text="A"   CausesValidation="False" OnClick="btnVehicleNew_Click"  />
  <asp:Button ID="btnVehicleEdit" runat="server" CausesValidation="False" class="btn btn-info btn-xs"  Text="U" OnClick="btnVehicleEdit_Click"   />
           
            </div>
       </div>
        </div>
    <div id="PropertyPanel" runat="server" visible="false">
      <div class="row">
        <div class="col-md-2">
              <b>Property Description:</b>
            </div>
        <div class="col-md-5">
            <asp:TextBox ID="tbPropertyDescription" runat="server"></asp:TextBox>
        </div>
    </div>
     </div>
    <div class="row">
            <div class="col-md-2">
              Owner:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="cboOwner" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboOwner_SelectedIndexChanged"></asp:DropDownList>
                  <asp:Button  ID="btnOwnerNew" runat="server" class="btn btn-info btn-xs"  Text="A"   CausesValidation="False" OnClick="btnOwnerNew_Click"    />
                <asp:Button ID="btnOwnerEdit" runat="server" CausesValidation="False" class="btn btn-info btn-xs"  Text="U" OnClick="btnOwnerEdit_Click"   />
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-5">
                   <asp:Label ID="txtOwner" runat="server" Height="75px" Width="275px"></asp:Label>

            </div>
       </div>
     <div id="DriverPanel" runat="server">
        <div class="row">
            <div class="col-md-2">
              Driver:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="cboDriver" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboDriver_SelectedIndexChanged"></asp:DropDownList>
                  <asp:Button  ID="btnDriverNew" runat="server" class="btn btn-info btn-xs"  Text="A"   CausesValidation="False" OnClick="btnDriverNew_Click"    />
                  <asp:Button ID="btnDriverEdit" runat="server" CausesValidation="False" class="btn btn-info btn-xs"  Text="U" OnClick="btnDriverEdit_Click"  />
            </div>
       </div>
        <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-5">
                   <asp:Label ID="txtDriver" runat="server" Height="76px" Width="275px"></asp:Label>
            </div>
       </div>
     </div>
   
       <div class="row">
            <div class="col-md-12">
                <b>Other Insurance</b>
            </div>
       </div>
    <div class="row">
        <div class="col-md-1">
            Company:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="tbOICompany" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            Policy No:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="tbOIPolicyNo" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            Claim No:
        </div>
       <div class="col-md-3">
        <asp:TextBox ID="tbOIClaimNo" runat="server"></asp:TextBox>
            </div>
     </div>

     <div class="row">
        <div class="col-md-1">
            Adjuster:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="tbOIAdjuster" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            Phone:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="tbOIPhoneNo" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="tbOIPhoneNo_TextChanged"></asp:TextBox>
            </div>
         <div class="col-md-1">
           Fax:
        </div>
         <div class="col-md-3">
        <asp:TextBox ID="tbOIFax"  TextMode="Phone" runat="server" AutoPostBack="True" OnTextChanged="tbOIFax_TextChanged"></asp:TextBox>
            </div>
     </div>
    
    <div class="row">
        <div class="col-md-1">
            Address:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="tbOIAddress" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            City:
        </div>
         <div class="col-md-3">
        <asp:TextBox ID="tbOICity" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-4">
           State: <asp:DropDownList ID="cboOIState" runat="server"></asp:DropDownList> &nbsp;Zip: <asp:TextBox ID="tbOIZip" runat="server" Width="60px" OnTextChanged="tbOIZip_TextChanged" AutoPostBack="true"></asp:TextBox>
        </div>
     </div>
     <div class="row">
          <div class="col-md-12">
                  <asp:DropDownList Visible="false" ID="cboUser" runat="server"></asp:DropDownList><asp:DropDownList Visible="false" ID="cboUserGroup" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="propDamageOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="propDamageOK_Click" />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
     <div class="row">
           <div class="col-md-12">
               <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
           </div>
    </div>
</asp:Content>

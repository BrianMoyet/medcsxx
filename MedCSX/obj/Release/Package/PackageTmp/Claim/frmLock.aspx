﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmLock.aspx.cs" Inherits="MedCSX.Claim.frmLock1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:Label ID="txtPrompt" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            Required Text:<br />
            <asp:Label ID="txtRequiredText" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            Optional Text:<br />
            <asp:Label ID="txtFileNoteText" runat="server" Text=""></asp:Label>
        </div>
    </div>
       <div class="row">
             
         <div class="col-md-12">
           <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click"/>
           <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

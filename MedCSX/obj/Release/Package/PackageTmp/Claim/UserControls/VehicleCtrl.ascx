﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleCtrl.ascx.cs" Inherits="MedCSX.Claim.UserControls.VehicleCtrl" %>
 <meta charset="utf-8" />
  
   

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 

      

     
  
<div class="row">
    <div class="col-md-5">
         <div class="row">
               <div class="col-md-12">
                <b>Vehicle Details</b>
            </div>
         </div>
        <div class="row">
            <div class="col-md-3">
                Year:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbVehicleYear" runat="server" Enabled="False"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Make:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbVehicleMake" runat="server" Enabled="False"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Model:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbVehicleModel" runat="server" Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                License Plate No:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbVehicleLicNo" runat="server" Width="75px"></asp:TextBox> State: <asp:DropDownList ID="cboVehicleState" runat="server"></asp:DropDownList>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                VIN:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbVehicleVIN" runat="server" AutoPostBack="True" OnTextChanged="tbVehicleVIN_TextChanged" Enabled="False"></asp:TextBox>
            &nbsp;<asp:Label ID="lblVinMessage" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                Color:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbVehicleColor" runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:CheckBox ID="chkVehicleDrivable" Text="This is Drivable" runat="server" />
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnCarfax"  class="btn btn-info btn-xs"  runat="server" Text="View Carfax" OnClick="btnCarfax_Click" />
             
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:HyperLink ID="hprViewCarfax" runat="server">HyperLink</asp:HyperLink>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
               <hr />
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
              <b>Loss Payee (From Personal Lines)</b>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                Name:</div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeName" runat="server" Enabled="False"></asp:TextBox> 
            </div>
        </div>
        
         <div class="row">
            <div class="col-md-3">
                Address 1:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeAddress1" runat="server" Enabled="False"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Address 2:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeAddress2" runat="server" Enabled="False"></asp:TextBox>
            </div>
        </div>
           <div class="row">
            <div class="col-md-3">
               City:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeCity" runat="server" Enabled="False" ></asp:TextBox>
            </div>
        </div>
                  <div class="row">
            <div class="col-md-3">
               State:
            </div>
             <div class="col-md-9">
                 <asp:DropDownList ID="tbPayeeState" runat="server" Enabled="False">
                 </asp:DropDownList>
                 Zip:   <asp:TextBox ID="tbPayeeZip" runat="server"  Width="60px" Enabled="False"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Loan No:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeLoanNo" runat="server" Enabled="False" ></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Phone:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeePhone" runat="server" TextMode="Phone" Enabled="False"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Fax:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeFax" runat="server" TextMode="Phone" Enabled="False"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Corrected Phone:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="tbPayeeCorrectedPhone" runat="server" TextMode="Phone"></asp:TextBox>
            </div>
        </div>
    </div>
     <div class="col-md-7">
         <div class="row">
               <div class="col-md-12">
                <b>Current Location of Vehicle (for Scene Access)</b>
            </div>
         </div>
          
         <div class="row">
            <div class="col-md-2">
                First Name:
            </div>
          <div class="col-md-10">
              <asp:TextBox ID="tbLocationFName" runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="row">
             <div class="col-md-2">
                  Last Name: 
             </div>
              <div class="col-md-10">
                  <asp:TextBox ID="tbLocationLName" runat="server"></asp:TextBox>
                  </div>
         </div>
          <div class="row">
            <div class="col-md-2">
                Contact Email:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="tbLocationEmail" runat="server" TextMode="Email"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-2">
                Address 1:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="tbLocationAddress1" runat="server"></asp:TextBox>
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
                Address 2:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="tbLocationAddress2" runat="server"></asp:TextBox>
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
               City:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="tbLocationCity" runat="server"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-2">
               State:
            </div>
            <div class="col-md-10">
                <asp:DropDownList ID="cboLocationState" runat="server" Enabled="False"></asp:DropDownList>
                Zip: <asp:TextBox ID="tbLocationZipcode" runat="server" Width="60px" Enabled="False"></asp:TextBox>
                <br /> Zipcode is Required for Scene Access
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
               Phone:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="tbLocationPhone1" runat="server"></asp:TextBox> 
            </div>
        </div>
           <div class="row">
                <div class="col-md-2">
                    Fax: </div>
                <div class="col-md-10">
                  <asp:TextBox ID="tbLocationPhone2" runat="server"></asp:TextBox> 
                </div>
           </div>
       <div class="row">
            <div class="col-md-2">
                </div>
            <div class="col-md-10">
                <asp:Button ID="btnSetToDriversAddress"  class="btn btn-info btn-xs"  runat="server" Text="Set to Driver's Address" OnClick="btnSetToDriversAddress_Click" />
                <br /><asp:Button ID="txtSetToOwnersAddress"  class="btn btn-info btn-xs"  runat="server" Text="Set to Owner's Address" OnClick="txtSetToOwnersAddress_Click" />
            </div>
    </div>

</div>
      <div class="row">
           <div class="col-md-12">
               <asp:Button ID="btnVehicleOK" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update" CausesValidation="false" OnClick="btnVehicleOK_Click"/>
               
               <asp:Button ID="btnAddEditSIU" runat="server" class="btn btn-info btn-xs"  Text="Add/Edit SIU"     OnClick="btnAddEditSIU_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right" CausesValidation="false"  Text="Cancel/Close" OnClick="btnCancel_Click" />
        </div>
          </div>
    <div class="row">
         <div class="col-md-2">
        <asp:Label ID="lblAlert" runat="server" visible="False" Text="" ForeColor="Red"></asp:Label>
   </div>
    </div>
</div>

﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmBIEvaluation.aspx.cs" Inherits="MedCSX.Claim.frmBIEvaluation"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row"    style="background-color:#f0f0f0;">
        <div class="col-md-1">
          Claim:
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtClaimNo" runat="server" Text="Label"></asp:Label>
        </div>
         <div class="col-md-1">
           Unadjusted
        </div>
         <div class="col-md-1">
            
        </div>
         <div class="col-md-1">
           Adjusted for Liability:
        </div>
         <div class="col-md-1">
            
        </div>
          <div class="col-md-1">
<<<<<<< HEAD
              <asp:LinkButton ID="btnPrintVersion" runat="server"  class="btn btn-info btn-xs"  OnClick="btnPrintVersion_Click" CausesValidation="false" >Printable Version</asp:LinkButton>
=======
<<<<<<< HEAD
              <asp:LinkButton ID="btnPrintVersion" runat="server"  class="btn btn-info btn-xs"  OnClick="btnPrintVersion_Click" CausesValidation="false" >Printable Version</asp:LinkButton>
=======
              <asp:LinkButton ID="btnPrintVersion" runat="server" OnClick="btnPrintVersion_Click" CausesValidation="false" >Printable Version</asp:LinkButton>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        </div>

    </div>
     <div class="row"    style="background-color:#f0f0f0;">
        <div class="col-md-1">
          Claimant
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtClaimantId" runat="server" Text="Label"></asp:Label>
        </div>
         <div class="col-md-1">
           Loss Range:
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtUnadjLossRange" runat="server" Text="Label"></asp:Label>
        </div>
         <div class="col-md-1">
            Loss Range:
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtAdjLossRange" runat="server" Text="Label"></asp:Label>
        </div>
           <div class="col-md-1">
<<<<<<< HEAD
              <asp:LinkButton ID="btnCalcExplanation" runat="server"  class="btn btn-info btn-xs"  CausesValidation="false" OnClick="btnCalcExplanation_Click">Explain Calculations</asp:LinkButton>
=======
<<<<<<< HEAD
              <asp:LinkButton ID="btnCalcExplanation" runat="server"  class="btn btn-info btn-xs"  CausesValidation="false" OnClick="btnCalcExplanation_Click">Explain Calculations</asp:LinkButton>
=======
              <asp:LinkButton ID="btnCalcExplanation" runat="server" CausesValidation="false" OnClick="btnCalcExplanation_Click">Explain Calculations</asp:LinkButton>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        </div>
    </div>
         <div class="row"    style="background-color:#f0f0f0;">
        <div class="col-md-1">
          Date of Loss
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtDtLoss" runat="server" Text="Label"></asp:Label>
        </div>
         <div class="col-md-1">
           Expected Value:
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtUnadjExpectValue" runat="server" Text="Label"></asp:Label>
        </div>
         <div class="col-md-1">
           Exected Value:
        </div>
         <div class="col-md-1">
             <asp:Label ID="txtAdjExpectValue" runat="server" Text="Label"></asp:Label>
        </div>
           <div class="col-md-1">
              
        </div>
    </div>
         
    <div class="row"    style="background-color:#f0f0f0;">
        <div class="col-md-1">
            Injury Type
        </div>
        <div class="col-md-11">
            <asp:DropDownList ID="cboInjuryType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboInjuryType_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>
       <div class="row">
        <div class="col-md-1">
            <hr />
        </div>
            </div>
    <div class="row">
           <div class="col-md-12">
               <b>General</b><br />
              <div class="row"    style="background-color:#f0f0f0;">
                 <div class="col-md-9">
                    Description
                 </div>
                 <div class="col-md-3">
                    Percent
                 </div>
             </div>
             <div class="row"   style="background-color:#fffacd;">
                 <div class="col-md-9" >
                     Probability of Outcome - Damage Evaluation
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtProbablityDamage"  runat="server" AutoPostBack="True" OnTextChanged="txtProbablityDamage_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-9">
                     Probability of Outcome - Liability Evaluation
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtProbablityLiability"  runat="server" AutoPostBack="True" OnTextChanged="txtProbablityLiability_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
             <div class="row"   style="background-color:#fffacd;">
                 <div class="col-md-9">
                      Best Case Comparative Negligence Outcome
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtProbablityNegligenceBest" runat="server" AutoPostBack="True" OnTextChanged="txtProbablityNegligenceBest_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-9">
                     Worst Case Comparative Negligence Outcome
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtProbablityNegligenceWorst"   runat="server" AutoPostBack="True" OnTextChanged="txtProbablityNegligenceWorst_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
            <%--  <asp:GridView ID="grGeneral" runat="server" AutoGenerateSelectButton="false" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="biEvalId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   AllowPaging="True" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    
                    <Columns>
                      
                        <asp:BoundField DataField="Description" HeaderText="Description"  SortExpression=""  />
                        <asp:TemplateField HeaderText="Percent">
                          <ItemTemplate>
                              <asp:TextBox ID="percent" runat="server"></asp:TextBox>
                          </ItemTemplate>
                        
                    </asp:TemplateField>
                      
                    </Columns>
                  <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>--%>
           
        </div>
    </div>
        <div class="row">
        <div class="col-md-1">
            <hr />
        </div>
            </div>
     <div class="row">
      <div class="col-md-12">
               <b>Damages</b><br />
              <div class="row"    style="background-color:#f0f0f0;">
                 <div class="col-md-6">
                     Description
                 </div>
                 <div class="col-md-3">
                     Best Case Cost
                 </div>
                  <div class="col-md-3">
                     Worst Case Cost
                 </div>
             </div>
              <div class="row"   style="background-color:#fffacd;">
                 <div class="col-md-6">
                     General Damages
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtGeneralbest" Step=".01"  runat="server" AutoPostBack="True" TextMode="Number" OnTextChanged="txtGeneralbest_TextChanged"></asp:TextBox>
                 </div>
                  <div class="col-md-3">
                     <asp:TextBox ID="txtGeneralWorst"  Step=".01" runat="server" Height="19px" AutoPostBack="True" OnTextChanged="txtGeneralWorst_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
              <div class="row">
                 <div class="col-md-6">
                     Wage Loss
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtWageBest"  Step=".01" runat="server" AutoPostBack="True" OnTextChanged="txtWageBest_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
                  <div class="col-md-3">
                     <asp:TextBox ID="txtWageWorst"  Step=".01" runat="server" AutoPostBack="True" OnTextChanged="txtWageWorst_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
              <div class="row"   style="background-color:#fffacd;">
                 <div class="col-md-6">
                     Ambulance, Emergency Room, and Initial Hospital Case Other than Diagnostic Costs
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtAmbulanceBest" Step=".01"  runat="server" AutoPostBack="True" OnTextChanged="txtAmbulanceBest_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
                  <div class="col-md-3">
                     <asp:TextBox ID="txtAmbulanceWorst" Step=".01"  runat="server" AutoPostBack="True" OnTextChanged="txtAmbulanceWorst_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
              <div class="row">
                 <div class="col-md-6">
                     Estimated Future Medical Amount
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtMedicalBest"  Step=".01" runat="server" AutoPostBack="True" OnTextChanged="txtMedicalBest_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
                  <div class="col-md-3">
                     <asp:TextBox ID="txtMedicalWorst" Step=".01"  runat="server" AutoPostBack="True" OnTextChanged="txtMedicalWorst_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
              <div class="row"   style="background-color:#fffacd;">
                 <div class="col-md-6">
                     <b>Diagnostic and Treatments Total</b>
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtDiagTreatBest" Step=".01"  runat="server" AutoPostBack="True" OnTextChanged="txtDiagTreatBest_TextChanged" TextMode="Number"></asp:TextBox>
                 </div>
                  <div class="col-md-3">
                     <asp:TextBox ID="txtDiagTreatWorst" Step=".01"  runat="server" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
              <div class="row">
                 <div class="col-md-6">
                     <b>Grand Total</b>
                 </div>
                 <div class="col-md-3">
                     <asp:TextBox ID="txtGrandTotalBest" Step=".01"  runat="server" TextMode="Number"></asp:TextBox>
                 </div>
                  <div class="col-md-3">
                     <asp:TextBox ID="txtGrandTotalWorst" Step=".01"  runat="server" TextMode="Number"></asp:TextBox>
                 </div>
             </div>
            <%--  <asp:GridView ID="grDamages" runat="server" AutoGenerateSelectButton="false" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="biEvalId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   AllowPaging="True" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    
                    <Columns>
                      
                        <asp:BoundField DataField="Description" HeaderText="Description"  SortExpression=""  />
                        <asp:TemplateField HeaderText="Best Case Cost">
                          <ItemTemplate>
                              <asp:TextBox ID="bestCaseCost" runat="server"></asp:TextBox>
                          </ItemTemplate>
                        
                    </asp:TemplateField>
                          <asp:TemplateField HeaderText="Worst Case Cost">
                          <ItemTemplate>
                              <asp:TextBox ID="worstCaseCost" runat="server"></asp:TextBox>
                          </ItemTemplate>
                        
                    </asp:TemplateField>
                      
                    </Columns>
                  <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>--%>
            
        </div>
    </div>
     <div class="row">
        <div class="col-md-1">
            <hr />
        </div>
            </div>
     <div class="row">
        <div class="col-md-12">

               <b>Diagnostics</b><br />
             <div style="width: 100%; height: 100px; overflow: scroll">
              <asp:GridView ID="grDiagnostics" runat="server" OnRowEditing="grDiagnostics_RowEditing" OnRowDataBound="grDiagnostics_RowDataBound"   AutoGenerateColumns="False" CellPadding="4" DataKeyNames="BI_Diagnostic_Id"   ShowHeaderWhenEmpty="True"
                    ForeColor="#333333" OnRowDeleting="grDiagnostics_RowDeleting" OnRowUpdating="grDiagnostics_RowUpdating" ShowFooterWhenEmpty="true" OnRowCancelingEdit="grDiagnostics_RowCancelingEdit" Width="100%" ShowFooter="True"  OnRowDeleted="grDiagnostics_RowDeleted" OnRowUpdated="grDiagnostics_RowUpdated"  >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnostic Type">
                            <EditItemTemplate>
                                <%--<asp:Label ID="hdnBI_Diagnostic_Type_Id" runat="server" Text="" Visible="false" EnableViewState="false"></asp:Label>--%>
                              
                                <asp:DropDownList ID="DiagnosticEditDropDownList" runat="server" 
                                      DataSourceID="sqldsDiagnosticType" 
                                DataTextField="Description" 
                                   SelectedValue='<%# Bind("BI_Diagnostic_Type_Id") %>'
                                DataValueField="BI_Diagnostic_Type_Id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                 <asp:DropDownList ID="DiagnosticAddDropDownList" runat="server" 
                                DataSourceID="sqldsDiagnosticType" 
                                DataTextField="Description" 
                                DataValueField="BI_Diagnostic_Type_Id">
                                </asp:DropDownList>
                            </FooterTemplate>
                          <ItemTemplate>
                               <asp:Label ID="diagType" runat="server" Text='<%# Bind("Diagnostic_Type") %>'></asp:Label>
       
                          </ItemTemplate>
                           
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnostic Date">
                          <ItemTemplate>
                              <asp:Label ID="dtDiagnostic" runat="server" Text='<%# Bind("diagnostic_date") %>'></asp:Label>
                             
                          </ItemTemplate>
                           <EditItemTemplate>
                    <asp:TextBox ID="dtDiagnosticEdit" TextMode="DateTime" runat="server" Width="105px" Text='<%# Bind("diagnostic_date") %>' ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="dtDiagnosticAdd" TextMode="DateTime" runat="server" Width="105px" Text='<%# Bind("diagnostic_date") %>'></asp:TextBox>
                </FooterTemplate>
                    </asp:TemplateField>
                         <asp:TemplateField HeaderText="Best Case Cost">
                          <ItemTemplate>
<<<<<<< HEAD
                              <asp:Label ID="bestCaseCost" runat="server" Text='<%# String.Format("{0:C}", Eval("best_Case_Cost"))  %>'></asp:Label>
=======
<<<<<<< HEAD
                              <asp:Label ID="bestCaseCost" runat="server" Text='<%# String.Format("{0:C}", Eval("best_Case_Cost"))  %>'></asp:Label>
=======
                              <asp:Label ID="bestCaseCost" runat="server" Text='<%# Bind("best_Case_Cost") %>'></asp:Label>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                          </ItemTemplate>
                            <EditItemTemplate>
                    <asp:TextBox ID="bestCaseCostEdit" runat="server" Width="105px" Text='<%# Bind("best_Case_Cost") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="bestCaseCostAdd" runat="server" Width="120px" Text='<%# Bind("best_Case_Cost") %>'></asp:TextBox>
                </FooterTemplate>
                    </asp:TemplateField>
                          <asp:TemplateField HeaderText="Worst Case Cost">
                       
                                <EditItemTemplate>
                    <asp:TextBox ID="worstCaseCostEdit" runat="server" Width="105px" Text='<%# Bind("worst_Case_Cost") %>' ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="worstCaseCostAdd" runat="server" Width="120px" Text='<%# Bind("worst_Case_Cost") %>'></asp:TextBox>
                     
                </FooterTemplate>
                           <ItemTemplate>
<<<<<<< HEAD
                               <asp:Label ID="worstCaseCost" runat="server" Text='<%#  String.Format("{0:C}", Eval("worst_Case_Cost"))  %>'></asp:Label>
=======
<<<<<<< HEAD
                               <asp:Label ID="worstCaseCost" runat="server" Text='<%#  String.Format("{0:C}", Eval("worst_Case_Cost"))  %>'></asp:Label>
=======
                               <asp:Label ID="worstCaseCost" runat="server" Text='<%# Bind("worst_Case_Cost") %>'></asp:Label>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="Ordered By">
                            <EditItemTemplate>
                                <asp:DropDownList ID="OrderedDREditDropDownList" runat="server" Width="150px" 
                                DataSourceID="sqldsDoctors" 
                                DataTextField="Description" 
                                DataValueField="ID"
                                SelectedValue='<%# Bind("Ordered_By_Id") %>'
                              >
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                               
                                 <asp:DropDownList ID="OrderedDRAddDropDownList" runat="server"  Width="150px"
                                DataSourceID="sqldsDoctors" 
                                DataTextField="Description" 
                                DataValueField="ID">
                                </asp:DropDownList>
                               
                               
                            </FooterTemplate>
                          <ItemTemplate>
                               <asp:Label ID="OrderedDoctorId" runat="server" Text='<%# Bind("ordered_by") %>'></asp:Label>
                         </ItemTemplate>
                       </asp:TemplateField>
                     <asp:TemplateField>
                         
                         <EditItemTemplate></EditItemTemplate>
                         <FooterTemplate>
                              <asp:Button ID="AddDiagnosticButton" runat="server"  class="btn btn-info btn-xs"  OnClick="AddDiagnosticButton_Click" CommandName="InsertDiag"  Text="A" />
                         </FooterTemplate>
                         <ItemTemplate></ItemTemplate>
                     </asp:TemplateField>
                        <asp:BoundField DataField="BI_Diagnostic_Type_Id" HeaderText="" Visible="false" />
                        <asp:BoundField DataField="Ordered_By_Id" HeaderText="" Visible="false" />
                        
                    </Columns>
                  <EmptyDataTemplate>
                      <asp:LinkButton ID="AddEmptyRow" CausesValidation="false" class="btn btn-info btn-xs"  OnClick="AddEmptyRow_Click" runat="server">A</asp:LinkButton>
                  </EmptyDataTemplate>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                <EditRowStyle BackColor="LightGray" ForeColor="Black" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
            </div>
        </div>
     
    </div>
     <div class="row">
        <div class="col-md-1">
            <hr />
        </div>
            </div>
     <div class="row">
       <div class="col-md-12">
               <b>Treatments</b><br />
             <asp:GridView ID="grTreatments" runat="server"    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="BI_Treatment_Id"   ShowHeaderWhenEmpty="True"
                    ForeColor="#333333" OnRowDeleting="grTreatments_RowDeleting"  OnRowEditing="grTreatments_RowEditing"  OnRowUpdating="grTreatments_RowUpdating" ShowFooterWhenEmpty="true" OnRowCancelingEdit="grTreatments_RowCancelingEdit" Width="100%" ShowFooter="True"  >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    
                   <Columns>
                       <asp:TemplateField ShowHeader="False">
                           <EditItemTemplate>
                               <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                               &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                           </EditItemTemplate>
                           <ItemTemplate>
                               <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                               &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"  OnClientClick="return confirm('Are you sure you want to delete?');"></asp:LinkButton>
                           </ItemTemplate>
                       </asp:TemplateField>
                    <asp:TemplateField HeaderText="Treating Doctor">
                        <ItemTemplate>
                            <asp:Label ID="TreatingDoctorId" runat="server" Text='<%# Bind("Treating_Doctor") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="TreatmentsDREditDropDownList" runat="server"  Width="150px"
                                DataSourceID="sqldsDoctors" 
                                DataTextField="Description" 
                                DataValueField="ID"
                                SelectedValue='<%# Bind("vendor_id") %>'>
                                </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                                 <asp:DropDownList ID="TreatmentsDRAddDropDownList" runat="server"  Width="150px"
                                DataSourceID="sqldsDoctors" 
                                DataTextField="Description" 
                                DataValueField="ID">
                                </asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date">
                        <ItemTemplate>
                            <asp:Label ID="dtStart" runat="server" Text='<%# Bind("treatment_start_date") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="dtStartEdit" runat="server" TextMode="DateTime" Width="75px" Text='<%# Bind("treatment_start_date") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtStartAdd" runat="server" TextMode="DateTime" Width="75px" Text='<%# Bind("treatment_start_date") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date">
                        <ItemTemplate>
                            <asp:Label ID="dtEnd" runat="server" Text='<%# Bind("treatment_end_date") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="dtEndEdit" runat="server" TextMode="DateTime" Width="75px" Text='<%# Bind("treatment_end_date") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtEndAdd" runat="server" TextMode="DateTime" Width="75px" Text='<%# Bind("treatment_end_date") %>'></asp:TextBox>
                        </FooterTemplate>   
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nbr Treatments">
                        <ItemTemplate>
                            <asp:Label ID="nbrTreatments" runat="server"  Width="45px" Text='<%# Bind("nbr_treatments") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="nbrTreatmentsEdit" runat="server" TextMode="Number" Width="45px" Text='<%# Bind("nbr_treatments") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="nbrTreatmentsAdd" runat="server" TextMode="Number" Width="45px" Text='<%# Bind("nbr_treatments") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Diagnosis">
                        <ItemTemplate>
                            <asp:Label ID="diagnosis" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="diagnosisEdit" runat="server" 
                                    DataSourceID="sqldsDiagnosis" 
                                    DataTextField="Description" 
                                    DataValueField="BI_Diagnosis_Type_Id"
                                    SelectedValue='<%# Bind("BI_Diagnosis_Type_Id") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                             <asp:DropDownList ID="diagnosisAdd" runat="server" 
                                          DataSourceID="sqldsDiagnosis" 
                                        DataTextField="Description" 
                                 
                                        DataValueField="BI_Diagnosis_Type_Id"
                                         SelectedValue='<%# Bind("BI_Diagnosis_Type_Id") %>'></asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Chronic">
                         <ItemTemplate>
                            <asp:Label ID="chronic" runat="server" Text='<%# Bind("chronic") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                             <asp:DropDownList ID="ChronicEdit" runat="server"  Width="50px"   SelectedValue='<%# Bind("Expr1") %>'>
                                 <asp:ListItem>False</asp:ListItem>
                                 <asp:ListItem>True</asp:ListItem>
                             </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                             <asp:DropDownList ID="ChronicAdd" runat="server"  Width="50px"  >
                                 <asp:ListItem>False</asp:ListItem>
                                 <asp:ListItem>True</asp:ListItem>
                                        </asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Best Case Cost">
                        <ItemTemplate>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                            <asp:Label ID="bestCaseCost" runat="server" Text='<%#  String.Format("{0:C}", Eval("cost_best_case"))  %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="bestCaseCostEdit" runat="server" TextMode="Number" Width="105px" Text='<%#  Bind("cost_best_case") %>'></asp:TextBox>
<<<<<<< HEAD
=======
=======
                            <asp:Label ID="bestCaseCost" runat="server" Text='<%# Bind("cost_best_case") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="bestCaseCostEdit" runat="server" TextMode="Number" Width="105px" Text='<%# Bind("cost_best_case") %>'></asp:TextBox>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="bestCaseCostAdd" runat="server" TextMode="Number" Width="120px" Text='<%# Bind("cost_best_case") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Worst Worst Cost">
                        <ItemTemplate>
<<<<<<< HEAD
                            <asp:Label ID="worstCaseCost" runat="server" Text='<%#  String.Format("{0:C}", Eval("cost_worst_case"))  %>'></asp:Label>
=======
<<<<<<< HEAD
                            <asp:Label ID="worstCaseCost" runat="server" Text='<%#  String.Format("{0:C}", Eval("cost_worst_case"))  %>'></asp:Label>
=======
                            <asp:Label ID="worstCaseCost" runat="server" Text='<%# Bind("cost_worst_case") %>'></asp:Label>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="worstCaseCostEdit" runat="server" TextMode="Number" Width="105px" Text='<%# Bind("cost_worst_case") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="worstCaseCostAdd" runat="server" TextMode="Number" Width="120px" Text='<%# Bind("cost_worst_case") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Report Fees">
                        <ItemTemplate>
<<<<<<< HEAD
                            <asp:Label ID="reportFees" runat="server" Text='<%#  String.Format("{0:C}", Eval("report_fees"))  %>'></asp:Label>
=======
<<<<<<< HEAD
                            <asp:Label ID="reportFees" runat="server" Text='<%#  String.Format("{0:C}", Eval("report_fees"))  %>'></asp:Label>
=======
                            <asp:Label ID="reportFees" runat="server" Text='<%# Bind("report_fees") %>'></asp:Label>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="reportFeesEdit" runat="server" TextMode="Number" Width="105px" Text='<%# Bind("report_fees") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="reportFeesAdd" runat="server" TextMode="Number" Width="120px" Text='<%# Bind("report_fees") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate></ItemTemplate>
                        <EditItemTemplate></EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="AddTreatmentButton" runat="server"  class="btn btn-info btn-xs"  OnClick="AddTreatmentButton_Click" CommandName="InsertDiag"  Text="A" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Vendor_Id" HeaderText="" Visible="false" />
                    <asp:BoundField DataField="BI_Diagnosis_Type_Id" HeaderText="" Visible="false" />
                    <asp:BoundField DataField="Chronic" HeaderText="" Visible="false" />
                      
                    </Columns>
                  <EmptyDataTemplate>
                      <asp:LinkButton ID="AddEmptyRowTreatment" CausesValidation="false" class="btn btn-info btn-xs"  OnClick="AddEmptyRowTreatment_Click" runat="server">A</asp:LinkButton>
                  </EmptyDataTemplate>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                   <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                  <EditRowStyle BackColor="LightGray" ForeColor="Black" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
             
        </div>
    </div>
     <div class="row">
        <div class="col-md-1">
            <hr />
        </div>
            </div>
     <div class="row">
        <div class="col-md-1">
            Notes
        </div>
         <div class="col-md-11">
             <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Width="100%" Height="50px" AutoPostBack="True" OnTextChanged="txtNotes_TextChanged"></asp:TextBox>
        </div>
    </div>
      <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="Close" OnClick="btnOK_Click"  />
           
             
    </div>
</div>
    <asp:SqlDataSource ID="sqldsDiagnosticType" runat="server" ConnectionString="<%$ ConnectionStrings:ClaimsConnectionString %>" SelectCommand="SELECT [BI_Diagnostic_Type_Id], [Description] FROM [BI_Diagnostic_Type] ORDER BY [Description]"></asp:SqlDataSource>
     <asp:SqlDataSource ID="sqldsDiagnosis" runat="server" ConnectionString="<%$ ConnectionStrings:ClaimsConnectionString %>" SelectCommand="SELECT  BI_Diagnosis_Type_Id, Description, Enum_Name, Image_Index, Created_By, Modified_By, Created_Date, Modified_Date FROM            BI_Diagnosis_Type"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsDoctors" runat="server" ConnectionString="<%$ ConnectionStrings:ClaimsConnectionString %>" SelectCommand="SELECT dbo.Vendor.Vendor_Id AS ID, dbo.Vendor.Name + ' ' + dbo.Vendor.City + ', ' + dbo.State.Abbreviation AS Description FROM dbo.Vendor INNER JOIN dbo.State ON dbo.Vendor.State_Id = dbo.State.State_Id WHERE        (Vendor.State_Id = @stateid) AND (Vendor.Vendor_Sub_Type_Id IN (16, 17, 18))  OR                          (Vendor.Vendor_Id = 0)  order by description" OnSelecting="sqldsDoctors_Selecting">
        <SelectParameters>
                        <asp:Parameter Name="stateid" />
        </SelectParameters>

    </asp:SqlDataSource>
</asp:Content>

﻿<%@ Page Title="Litigation" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmLitigation.aspx.cs" Inherits="MedCSX.Claim.frmLitigation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
    <div class="col-md-3">
            Status:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="cboLitigationStatus" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Defense Attorney:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="cboLitigationDefenseAttorney" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Plantiff Attorney:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="cboLitigationPlaintiffAttorney" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Court Location:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="cboLitigationCourtLocation" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Date Filed:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="dpFileDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Date Served:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="dpServeDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Resolution Amount:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>        
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
          Comments:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="250px"></asp:TextBox>        
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
          Reserves:                   
    </div>
    <div class="col-md-9">
         <asp:GridView ID="grReserves" OnSelectedIndexChanged="grReserves_SelectedIndexChanged" AutoGenerateSelectButton="false" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="reserveId"  
                    ForeColor="#333333"    CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" ShowHeader="true">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                       <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:BoundField DataField="reserveType" HeaderText="Reserve Type"/>
                        <asp:BoundField DataField="claimant" HeaderText="Claimant"/>  
            </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
             <EditRowStyle BackColor="#999999" />
             <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
        </asp:GridView>   
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
          <br />
        <asp:Label ID="lblDefendents" runat="server" Text="Defendants:"></asp:Label>                   
    </div>
    <div class="col-md-9">
          <br />
        <asp:Label ID="lblAddDefendantsMessage" runat="server" Font-Bold="true" Text="Please save the Litigation to add Defendants." EnableViewState="false"></asp:Label>
        <asp:GridView ID="grDefendants" Visible="true" runat="server" AutoGenerateDeleteButton="true" OnRowDeleting="grDefendants_RowDeleting" AutoGenerateSelectButton="true" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"  ShowHeaderWhenEmpty="true" EmptyDataText="No Defendants Found"
                    ForeColor="#333333"   AllowPaging="True" CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" PageSize="15" ShowHeader="true" ShowFooter="true">
                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Name" >
                        <ItemTemplate>
                            <asp:Label runat="server" Text=<%# Eval("description")%>></asp:Label>
                        </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                      <FooterTemplate>
          <asp:Button  ID="btnDefendantsAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"  data-toggle="modal" data-target="#theModal"  CausesValidation="false" OnClick="btnDefendantsAdd2_Click" /> 
    </FooterTemplate>
                        </asp:TemplateField>
                    
                      
                       
                        
            </Columns>
              <EmptyDataTemplate>
                   <asp:Button  ID="btnDefendantsAdd2" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"  data-toggle="modal" data-target="#theModal" CausesValidation="false" OnClick="btnDefendantsAdd2_Click" /> 
              </EmptyDataTemplate>
             <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
             <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
        </asp:GridView>   
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click"  />
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

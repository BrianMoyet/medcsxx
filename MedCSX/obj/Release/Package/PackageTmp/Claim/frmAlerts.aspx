﻿<%@ Page Title="Alerts" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAlerts.aspx.cs" Inherits="MedCSX.Claim.frmAlerts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="pnlUrgent" visible="false"   style="background-color:#fa8072;">
    <div class="row">
        <div class="col-md-12" >
            <h3>&nbsp; <b>Urgent Alert!</b></h3>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                        <asp:GridView ID="grAlerts" runat="server" AutoGenerateSelectButton="false"  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="alert_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
               ForeColor="#333333" AllowSorting="True" AllowPaging="False" OnSorting="grAlerts_Sorting"   OnRowDataBound="grAlerts_RowDataBound" width="100%" ShowFooter="True" PageSize="500">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
              <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "=window.open('ViewClaim.aspx?&Ptabindex=4&tabindex=18&claim_id=" + DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString()  %> Runat="Server">
                            <%# Eval("display_claim_id") %></asp:Hyperlink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" />
                        </asp:TemplateField>
                  <asp:BoundField DataField="Urgent" HeaderText="Urgent"  SortExpression="Urgent"  />
                  <asp:BoundField DataField="Sent_From" HeaderText="Sent From"  SortExpression="Sent_From">
                     <ItemStyle Width="150px" />
                  </asp:BoundField>
                  <asp:BoundField DataField="date_sent" HeaderText="Date Sent" SortExpression="date_sent">
                     <ItemStyle Width="175px" />
                  </asp:BoundField>
                
                  <asp:BoundField DataField="file_note_type" HeaderText="Alert Text" SortExpression="file_note_type" />
                   
               </Columns>
              
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
               <EditRowStyle BackColor="#999999" />
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>
    </div>
</asp:Content>

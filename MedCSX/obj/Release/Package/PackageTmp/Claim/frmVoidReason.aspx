﻿<%@ Page  MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmVoidReason.aspx.cs" Inherits="MedCSX.Claim.frmVoidReason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Void Reason:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboVoidReason" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboVoidReason_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>

</asp:Content>

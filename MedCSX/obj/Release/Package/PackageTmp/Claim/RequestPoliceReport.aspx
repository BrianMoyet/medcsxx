﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="RequestPoliceReport.aspx.cs" Inherits="MedCSX.Claim.RequestPoliceReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-3">
        Vehicle:
    </div>
     <div class="col-md-3">
         <asp:DropDownList ID="ddlVehicle" runat="server"></asp:DropDownList>
         &nbsp;&nbsp;<asp:HyperLink ID="HyperLink1" runat="server">New</asp:HyperLink>&nbsp;&nbsp;<asp:HyperLink ID="HyperLink2" runat="server">Edit</asp:HyperLink>
    </div>
</div>
 <div class="row">
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12"><b>Loss Location</b></div>
        </div>
        <div class="row">
             <div class="col-md-3">
                Investigating Agency Name:
             </div>
            <div class="col-md-9">
                <asp:TextBox ID="txtInvestigatingAgencyName" runat="server"></asp:TextBox>
             </div>
        </div>
         <div class="row">
             <div class="col-md-3">
                City:
             </div>
            <div class="col-md-9">
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
             </div>
        </div>
        <div class="row">
             <div class="col-md-3">
                County:
             </div>
            <div class="col-md-9">
                <asp:TextBox ID="txtCounty" runat="server"></asp:TextBox>
             </div>
        </div>
         <div class="row">
             <div class="col-md-3">
                State:
             </div>
            <div class="col-md-9">
                <asp:DropDownList runat="server" ID="ddlState"></asp:DropDownList>
             </div>
        </div>
    </div>
      <div class="col-md-7">
         <div class="row">
            <div class="col-md-12"><b>People Involved</b></div>
        </div>
          <div class="row">
             <div class="col-md-4">
              
             </div>
            <div class="col-md-4">
                First Name
             </div>
          <div class="col-md-4">
                Last Name
             </div>
        </div>
     <div class="row">
             <div class="col-md-4">
               Primary:
             </div>
             <div class="col-md-4">
                <asp:TextBox ID="txtPrimaryFirstName" runat="server"></asp:TextBox>
             </div>
             <div class="col-md-4">
                <asp:TextBox ID="txtPrimaryLastName" runat="server"></asp:TextBox>
             </div>
        </div>
           <div class="row">
             <div class="col-md-4">
               2nd Party:
             </div>
             <div class="col-md-4">
                <asp:TextBox ID="txt2ndPartyFirst" runat="server"></asp:TextBox>
             </div>
             <div class="col-md-4">
                <asp:TextBox ID="txt2ndPartyLast" runat="server"></asp:TextBox>
             </div>
        </div>
           <div class="row">
             <div class="col-md-4">
               3rd Party:
             </div>
             <div class="col-md-4">
                <asp:TextBox ID="txt3rdPartyFirst" runat="server"></asp:TextBox>
             </div>
             <div class="col-md-4">
                <asp:TextBox ID="txt3rdPartyLast" runat="server"></asp:TextBox>
             </div>
        </div>
    </div>
</div>
<div class="row">
      <div class="col-md-12"><b>Investigating Agency</b></div>
</div>
<div class="row">
    <div class="col-md-4">Investigating Agency Name</div>
    <div class="col-md-8"><asp:TextBox ID="txtAgencyName" runat="server"></asp:TextBox></div>
</div>
<div class="row">
    <div class="col-md-4">Agency Type</div>
    <div class="col-md-8"><asp:DropDownList ID="ddlAgencyType" runat="server"></asp:DropDownList></div>
</div>
</asp:Content>

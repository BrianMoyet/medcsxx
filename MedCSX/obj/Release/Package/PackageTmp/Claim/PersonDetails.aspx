﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="PersonDetails.aspx.cs" Inherits="MedCSX.Claim.PersonDetails" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:RadioButtonList ID="rblClaimantType" runat="server"  RepeatDirection="Horizontal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblClaimantType_SelectedIndexChanged">
                    <asp:ListItem Value="0" Text="Person" Selected="True" />
                    <asp:ListItem Value="1" Text="Company" />
            </asp:RadioButtonList>
       
        </div>
</div>
<div runat="server" id="Person">
    <div class="row">
            <div class="col-md-12">
                <b>Person</b>
              
            </div>
    </div>
     <div class="row">
            <div class="col-md-6">
                Prefix: <asp:TextBox ID="txtPrefix" runat="server"></asp:TextBox>
            </div>
          <div class="col-md-6">
                Suffix: <asp:TextBox ID="txtSuffix" runat="server"></asp:TextBox>
            </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                First: <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="**" ControlToValidate="txtFirstName" ForeColor="Red">**</asp:RequiredFieldValidator><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox> Mid: <asp:TextBox ID="txtMiddleInitial" runat="server" Width="15"></asp:TextBox> Last: <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="**" ControlToValidate="txtLast" ForeColor="Red">**</asp:RequiredFieldValidator><asp:TextBox ID="txtLast" runat="server"></asp:TextBox>
                
            </div>
        </div>
    </div>
    <div runat="server" id="Company" visible="false">
        <div class="row">
                <div class="col-md-12">
                    <b>Company:</b>  
                   
                </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    Company Name: <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="**" ControlToValidate="txtCompanyName" ForeColor="Red">**</asp:RequiredFieldValidator>   <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                   
                </div>
        </div>
    </div>
   
        <div class="row">
             <div class="col-md-10">
                    <b>Address</b>    
       
                </div>
            <div class="col-md-2">
                <asp:Button ID="btnPickAddress" runat="server" Text="Pick Address" />
            </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                    Address 1:         
                </div>
            <div class="col-md-11">
                   <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox>           
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                    Address 2:         
                </div>
            <div class="col-md-11">
                   <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>           
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                   City:         
                </div>
            <div class="col-md-11">
                   <asp:TextBox ID="txtCity" runat="server"></asp:TextBox> State: <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList> Zip: <asp:TextBox ID="txtZip" runat="server" Width="60px"></asp:TextBox>          
                </div>
        </div>
      <div class="row">
             <div class="col-md-12">
                    <b>Phone</b>    
             
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                Home Phone:
             </div>
              <div class="col-md-5">
                      <asp:TextBox ID="txtHomePhone" runat="server" TextMode="Phone"></asp:TextBox>            
             </div>
             <div class="col-md-1">
                Work Phone:       
             </div>
              <div class="col-md-5">
                <asp:TextBox ID="txtWorkPhone" runat="server" TextMode="Phone"></asp:TextBox>                    
             </div>
       </div>
     <div class="row">
             <div class="col-md-1">
                Cell Phone:
             </div>
              <div class="col-md-5">
                      <asp:TextBox ID="txtCellPhone" runat="server" TextMode="Phone"></asp:TextBox>            
             </div>
             <div class="col-md-1">
               Fax:       
             </div>
              <div class="col-md-5">
                <asp:TextBox ID="txtFax" runat="server" TextMode="Phone"></asp:TextBox>                    
             </div>
       </div>
     <div class="row">
             <div class="col-md-1">
                Other Contact:
             </div>
              <div class="col-md-5">
                      <asp:TextBox ID="txtOtherPhone" runat="server" TextMode="Phone"></asp:TextBox>            
             </div>
            
       </div>
     <div class="row">
             <div class="col-md-12">
                    <b>Other</b>    
          
                </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Email:
             </div>
         <div class="col-md-5">
             <asp:TextBox ID="txtEmail" runat="server" TextMode="Email"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 DOB: <asp:CompareValidator ID="CompareValidator1" runat="server"
ControlToValidate="txtDOB" ErrorMessage="**"
Operator="DataTypeCheck" Type="Date" ForeColor="Red">**</asp:CompareValidator>
             </div>
         <div class="col-md-5">
                 <asp:TextBox ID="txtDOB" runat="server" TextMode="Date"></asp:TextBox>
             </div>

     </div>
    <div class="row">
             <div class="col-md-1">
                 SSN:
             </div>
         <div class="col-md-5">
             <asp:TextBox ID="txtSSn" runat="server"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 Drivers Lic No:
             </div>
         <div class="col-md-5">
                 <asp:TextBox ID="txtDriversLicenseNo" runat="server"></asp:TextBox>
             </div>
     </div>
    <div class="row">
             <div class="col-md-1">
                 Employer:
             </div>
         <div class="col-md-5">
             <asp:TextBox ID="txtEmployer" runat="server"></asp:TextBox>
             </div>
         <div class="col-md-6">
                DL State: <asp:DropDownList ID="ddlDLState" runat="server"></asp:DropDownList> DL CLass: <asp:DropDownList ID="ddlClass" runat="server"></asp:DropDownList>
             </div>
     </div>
     <div class="row">
             <div class="col-md-1">
                 Language:
             </div>
         <div class="col-md-5">
             <asp:DropDownList ID="ddlLanguage" runat="server"></asp:DropDownList>
             </div>
         <div class="col-md-1">
                Sex:
             </div>
         <div class="col-md-5">
             <asp:RadioButtonList ID="rblSex" runat="server"  RepeatDirection="Horizontal"> <asp:ListItem Value="M" Text="Male"/>
                    <asp:ListItem Value="F" Text="Female" /></asp:RadioButtonList>
             <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="rblSex" ErrorMessage="**" ForeColor="Red" MaximumValue="2" MinimumValue="1">**</asp:RangeValidator>
             </div>
     </div>
     <div class="row">
             <div class="col-md-1">
                 Comments:
             </div>
         <div class="col-md-11">
             <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" OnTextChanged="txtComments_TextChanged" AutoPostBack="True"></asp:TextBox>
             <br />
             <asp:Label ID="lblComments" runat="server" ForeColor="Red" Text="You have entered comments in the contact information of this person.  This information DOES NOT REPLACE a file note associated with the claim.  Nor will the information entered as a comment convert to a file note.  It is information that pertains to the PERSON not the CLAIM." Visible="False"></asp:Label>
             </div>
     </div>
    <div id="otherInsurance" runat="server" visible="false">
      <div class="row">
             <div class="col-md-12">
                    <b>Other Insurance</b>    
                 <hr />
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                 Company:
             </div>
            <div class="col-md-11">
                <asp:TextBox ID="txtOtherInsCompany" runat="server"></asp:TextBox>
             </div>
        </div>
    <div class="row">
             <div class="col-md-1">
                 Policy #:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtOtherInsPolicyNumber" runat="server"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 Claim #:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtOtherInsClaimNo" runat="server"></asp:TextBox>
             </div>
        </div>
      <div class="row">
             <div class="col-md-1">
                 Adjuster:
             </div>
            <div class="col-md-11">
                <asp:TextBox ID="txtOtherInsAdjuster" runat="server"></asp:TextBox>
             </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Address:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtOtherInsAddress" runat="server"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 City:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtOtherInsCity" runat="server"></asp:TextBox> State: <asp:DropDownList ID="ddlOtherState" runat="server"></asp:DropDownList>
             </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Phone:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtOtherInsPhone" runat="server" TextMode="Phone"></asp:TextBox>
             </div>
          <div class="col-md-1">
                 Fax:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtOtherInsFax"  TextMode="Phone" runat="server"></asp:TextBox> ZIP: <asp:TextBox ID="txtOtherInsZip" runat="server"></asp:TextBox>
             </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Email:
             </div>
            <div class="col-md-11">
                <asp:TextBox ID="txtOtherInsEmail" runat="server" TextMode="Email"></asp:TextBox>
             </div>
        </div>
</div>
       <div class="row">
           <div class="col-md-12">
                   <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />&nbsp;
               <asp:Button ID="btnAddInsurance" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Add Insurance"  causesValidation="false"   OnClick="btnAddInsurance_Click"  />&nbsp;
               <asp:Button ID="btnSIU" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Add/Edit SIU"  causesValidation="false"   OnClick="btnSIU_Click"  />&nbsp;
                   <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
                   <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
            </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/SiteModal.Master" CodeBehind="frmClaimantDetails.aspx.cs" Inherits="MedCSX.Claim.frmClaimantDetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div class="row">
      <div class="col-md-1">
          <asp:Label ID="lblPersonType" runat="server" Text="Type"></asp:Label>
      </div>
     
      <div class="col-md-9">
          <asp:DropDownList ID="cboPersonType" runat="server" AutoPostBack="true"></asp:DropDownList>
     </div>
</div>
 
     
    

     <div class="row">
      <div class="col-md-1">
           <asp:Label ID="lblPerson" runat="server" Text="Person"></asp:Label>
           
      </div>
     
      <div class="col-md-9">
          <asp:DropDownList ID="cboPersonSelect" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboPersonSelect_SelectedIndexChanged"></asp:DropDownList> 
          <asp:Button ID="btnPersonNew" runat="server"  class="btn btn-info btn-xs" CausesValidation="false"  Text="A" OnClick="btnPersonNew_Click" />
          <asp:Button ID="btnPersonEdit"  class="btn btn-info btn-xs" runat="server" CausesValidation="false" Text="U" OnClick="btnPersonEdit_Click" />
          <br />
          <asp:Label ID="txtPersonDetails" runat="server" Text=""></asp:Label>

      </div>
     
    
</div>

 <div class="row">
     <div class="col-md-12">
               
               <asp:Button ID="btnPersonOK" runat="server" class="btn btn-info btn-xs" Text="OK" OnClick="btnPersonOK_Click"/>
              <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
</div>
<%--     <div class="row">
     <div class="col-md-12">
         <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        </div>
</div>--%>
</asp:Content>

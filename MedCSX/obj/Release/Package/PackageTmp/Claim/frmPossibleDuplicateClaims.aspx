﻿<%@ Page Title="Possible Duplicate Claims" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPossibleDuplicateClaims.aspx.cs" Inherits="MedCSX.Claim.frmPossibleDuplicateClaims" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <div class="row">
        <div class="col-md-12">
              <h4>Possible Duplicate Claims!</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
               <asp:GridView ID="grPossibleDuplicates"  runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" 
                    DataKeyNames="claimId"   
                    ShowHeaderWhenEmpty="True" 
                    EmptyDataText="No records Found"
                    ForeColor="#333333"  
                    AllowPaging="False"  Width="100%" PageSize="100" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                    
                      
                        <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"claimId").ToString() + "=window.open('ViewClaim.aspx?tabindex=18&claim_id=" + DataBinder.Eval(Container.DataItem,"claimId").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"claimId").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=900, copyhistory=no, left=300, top=100').focus();" %> Runat="Server">
                            <%# Eval("claimId") %></asp:Hyperlink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" />
                        </asp:TemplateField>
                      
                      
                    
                        <asp:BoundField DataField="policyNo" HeaderText="Policy"  />
                        <asp:BoundField DataField="dtLoss" HeaderText="Date of Loss"  ><ItemStyle Width="150px" /></asp:BoundField>

                        <asp:BoundField DataField="matchDescrip" HeaderText="Match Description"></asp:BoundField>
                        
                       
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                   <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                    <EditRowStyle BackColor="#999999" />
                     <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" CssClass="HeaderFreez"  />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
        </div>
    </div>
</asp:Content>

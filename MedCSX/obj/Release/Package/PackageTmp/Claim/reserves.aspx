﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteModal.Master"  CodeBehind="reserves.aspx.cs" Inherits="MedCSX.Claim.reserves" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div  style="background-color: #CCCCCC">
    <div class="row">
          <div class="col-md-2">
                Claimant:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblClaimant" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Coverage Type:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblCoverageType" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Coverage Limit:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblCoverageLimit" runat="server" Text="Label"></asp:Label>
          </div>
      </div>
    
     <div class="row">
          <div class="col-md-2">
                Loss Paid:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblLossPaid" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Net Loss Reserve:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblNetLossReserve" runat="server" Text="Label"></asp:Label>
          </div>
      </div>
      <div class="row">
          <div class="col-md-2">
                Expense Paid:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblExpensePaid" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Net Expense Reserve:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblNetExpenseReserve" runat="server" Text="Label"></asp:Label>
          </div>
      </div>
</div>    
     <div class="row">
          <div class="col-md-12">
              <asp:HyperLink ID="hprShowLoss" runat="server">Show Loss</asp:HyperLink>
              &nbsp;&nbsp;<asp:HyperLink ID="hprShowExpenses" runat="server">Show Expenses</asp:HyperLink>
              &nbsp;&nbsp;<asp:HyperLink ID="hprShowAll" runat="server">Show All</asp:HyperLink>
              &nbsp;&nbsp;<asp:HyperLink ID="hprTransactionReport" runat="server">Transaction Report</asp:HyperLink>
          </div>
     </div>
     <div class="row">
          <div class="col-md-9">
                        <asp:GridView ID="gvTransactions" runat="server" AutoGenerateColumns="False" CellPadding="4"  DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  OnSorting="gvTransactions_Sorting" AllowSorting="True" AllowPaging="True"  OnPageIndexChanging="gvTransactions_PageIndexChanging"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                 
                         <asp:BoundField DataField="transaction_date" HeaderText="Date"  SortExpression="transaction_date"  />
                         <asp:BoundField DataField="transaction_type" HeaderText="Transaction Type"  SortExpression="transaction_type"  />
                         <asp:BoundField DataField="amount" HeaderText="Amount"  SortExpression="amount" DataFormatString="{0:c}"  />
                         <asp:BoundField DataField="expense_loss" HeaderText="Loss/Expense"  SortExpression=""  />
                         <asp:BoundField DataField="draft_no" HeaderText="Dradt No"  SortExpression="draft_no"  />
                         <asp:BoundField DataField="void" HeaderText="Void"  SortExpression="void"  />
                         <asp:BoundField DataField="void_reason" HeaderText="Void Reason"  SortExpression="void_reason"  />
                          
                     
                    
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
          </div>
           <div class="col-md-3">
               <asp:HyperLink ID="hprIssueDraft" runat="server">Issue Draft</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink1" runat="server">Print Draft</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink2" runat="server">View Draft</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink3" runat="server">Receive Salvage</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink4" runat="server">Receive Subro</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink5" runat="server">Reissue Draft</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink6" runat="server">Void</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink7" runat="server">Unvoid Draft</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink8" runat="server">Wire Transfer</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink9" runat="server">Wire Transfer Overpayment</asp:HyperLink>
               <br /><asp:HyperLink ID="HyperLink10" runat="server">Issue Pending Draft</asp:HyperLink>
           </div>
     </div>
     <div class="row">
         <div class="col-md-12">
             <asp:Label ID="lblTransactionLocked" runat="server" Text="Transaction Not Locked" Font-Bold="True"></asp:Label>
         </div>
    </div>
    <div class="row">
         <div class="col-md-8">
             Lock Reason:
             <br />Lock Date:
         </div>
        <div class="col-md-4">
            <asp:HyperLink ID="HyperLink11" runat="server" Enabled="false">Unlock</asp:HyperLink>&nbsp;&nbsp;<asp:HyperLink ID="HyperLink12" runat="server">Lock Info</asp:HyperLink>
         </div>
    </div>


     <div class="row">
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
        </div>
</asp:Content>

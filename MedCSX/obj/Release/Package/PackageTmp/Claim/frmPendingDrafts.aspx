﻿<%@ Page Title="Pending Drafts" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPendingDrafts.aspx.cs" Inherits="MedCSX.Claim.frmPendingDrafts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="tsbNew" runat="server" OnClick="tsbNew_Click">New</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="tsbFinalize" runat="server" OnClick="tsbFinalize_Click">Finalize</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="tsbCancel" runat="server" OnClick="tsbCancel_Click">Cancel</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="tsbOpen" runat="server" OnClick="tsbOpen_Click">Open</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="sbStatus" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:RadioButton ID="rbPending" Text="Pending" runat="server" AutoPostBack="true" GroupName="status" OnCheckedChanged="rbPending_CheckedChanged" />
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rbCompleted" Text="Completed" AutoPostBack="true"  GroupName="status" OnCheckedChanged="rbCompleted_CheckedChanged" runat="server" />
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rbCancelled" Text="Cancelled" AutoPostBack="true"  GroupName="status" runat="server" OnCheckedChanged="rbCancelled_CheckedChanged" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="grPendingDrafts" AutoGenerateSelectButton="true" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="pending_draft_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"  CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top"  ShowFooter="True" FooterStyle-BackColor="White">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
                
                  <asp:BoundField DataField="draft_Status" HeaderText="Status" />
                  <asp:BoundField DataField="created_Date" HeaderText="Pending Date" />
                   <asp:BoundField DataField="Payee" HeaderText="Payee" />
                   <asp:BoundField DataField="draft_Amount" HeaderText="Amount" />

                
               </Columns>
                 <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>
    </div>
</asp:Content>

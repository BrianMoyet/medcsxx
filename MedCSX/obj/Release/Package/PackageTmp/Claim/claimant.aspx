﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/SiteModal.Master" CodeBehind="claimant.aspx.cs" Inherits="MedCSX.Claim.claimant" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <%-- <div class="row">
      <div class="col-md-1">
           Type:
      </div>
     
      <div class="col-md-9">
          <asp:DropDownList ID="ddlClaimantType" runat="server" AutoPostBack="true"></asp:DropDownList>
     </div>
</div>--%>
 <div class="row">
      <div class="col-md-1">
           Person:
      </div>
     
      <div class="col-md-9">
          <asp:DropDownList ID="ddlClaimant" runat="server" OnSelectedIndexChanged="ddlClaimant_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> <asp:HyperLink ID="HyperLink1" runat="server">New</asp:HyperLink>&nbsp;<asp:HyperLink ID="HyperLink2" runat="server">Edit</asp:HyperLink><br />
          <asp:Label ID="lblClaimantDetails" runat="server" Text="" Width="300px" Height="200px"></asp:Label>

      </div>
     
    
</div>

 <div class="row">
     <div class="col-md-12">
               
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
        </div>
</div>
</asp:Content>

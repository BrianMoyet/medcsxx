﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="PossibleDuplicateClaimsFound.aspx.cs" Inherits="MedCSX.Claim.PossibleDuplicateClaimsFound" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-12">
         <h4>Possilbe Duplicate Claims Found - Please Investigate</h4>
    </div>
</div>
    <div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvPossibleDuplicateClaims" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="possible_duplicate_claim_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  AllowPaging="True" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                       <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + "=window.open('claim.aspx?tabindex=18&claim_id=" + DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" +  DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + ".focus()" %> Runat="Server">
                            View</asp:Hyperlink>

                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="claim_id" HeaderText="Claim" />
                        <asp:BoundField DataField="policy_no" HeaderText="Policy" />
                         <asp:BoundField DataField="date_of_loss" HeaderText="Date of Loss"  />
                         <asp:BoundField DataField="description" HeaderText="Match Description" />
              
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnCreateNewClaimAnyways" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Create New Claim Anyway"     OnClick="btnCreate_Click"  />
        <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" OnClick="btnCancel_Click" />
    </div>
</div>
</asp:Content>

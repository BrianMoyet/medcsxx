﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmWitnessPassengerDetails.aspx.cs" Inherits="MedCSX.Claim.frmWitnessPassengerDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   
        <div class="row">
            <div class="col-md-2">
              Type:
            </div>
            <div class="col-md-10">
                   <asp:DropDownList ID="cboPersonType" runat="server"></asp:DropDownList>
            </div>
        </div>
    <div class="row">
            <div class="col-md-2">
              Person:
            </div>
            <div class="col-md-10">
                   <asp:DropDownList ID="cboPersonSelect" runat="server" OnSelectedIndexChanged="cboPersonSelect_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnPersonNew" runat="server"  class="btn btn-info btn-xs"  CausesValidation="false" OnClick="btnPersonNew_Click">A</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnPersonEdit" runat="server"  class="btn btn-info btn-xs"  CausesValidation="false" OnClick="btnPersonEdit_Click">U</asp:LinkButton>
                <p>
                    <asp:Label ID="txtPersonDetails" runat="server" Text=""></asp:Label></p>
            </div>
        </div>
     <div class="row">
            <div class="col-md-2">
              Location:
            </div>
            <div class="col-md-10">
                   <asp:DropDownList ID="cboVehicleSelect" runat="server" width="75px"  AutoPostBack="true" OnSelectedIndexChanged="cboVehicleSelect_SelectedIndexChanged">
                       <asp:ListItem Value="VehCar" Text="Car" Selected="True" />
                       <asp:ListItem Value="VehVan" Text="Van" />
                       <asp:ListItem Value="VehTruck" Text="Truck"/>

                   </asp:DropDownList>
            </div>
        </div>
     <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-10">
               <table id="tblCar" runat="server" GridLines="Both" BorderStyle="Solid" cellborder="1" runat="server" css="background-size: 300px 100px;" background="../content/images/icoredcar2.png"  width="350px" height="200px">
                   <tr valign="top">
                       <td width="150px"><asp:RadioButton runat="server"  ID="rbWitnessFrontRight" ToolTip="Witness Front Right" Tag="9" GroupName="WitnessLocation"  /></td>
                       <td align="center"><asp:RadioButton runat="server"  ID="rbWitnessMiddleRight" ToolTip="Witness Middle Right" Tag="11" GroupName="WitnessLocation"  /></td>
                       <td align="right"><asp:RadioButton runat="server"  ID="rbWitnessBackRight"  ToolTip="Witness Back Right" Tag="13"  GroupName="WitnessLocation" /></td>
                   </tr>
                    <tr valign="middle">
                        <td><asp:RadioButton runat="server"  ID="rbWitnessFront"  Tag="8"  /></td>
                        <td>
                            <table width="100%" height="100%">
                                <tr>
                                    <td><asp:RadioButton runat="server"  ID="rbWitnessPassenger" Tag="0" ToolTip="Witness Passenger" GroupName="WitnessLocation"/></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitnessBackPassenger"  Tag="2" ToolTip="Witness Back Passenger" GroupName="WitnessLocation" /></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitness3rdRowPassenger"  Tag="5" ToolTip="Witness 3rd Row Passenger" GroupName="WitnessLocation" /></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitness4thRowPassenger"  Tag="23" ToolTip="Witness 4th Row Passenger" GroupName="WitnessLocation" /></td>
                                </tr>
                                <tr>
                                    <td><asp:RadioButton runat="server"  ID="rbWitnessFrontCenter" ToolTip="Witness Front Center" Tag="7"  GroupName="WitnessLocation" /></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitnessBackCenter"  Tag="3" ToolTip="Withness Back Center" GroupName="WitnessLocation" /></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitness3rdRowCenter"  Tag="6" ToolTip="Witness 3rd Row Center" GroupName="WitnessLocation" /></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitness4thRowCenter"  Tag="24" ToolTip="Witness 4th Row Center" GroupName="WitnessLocation" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitnessBackDrivers" Tag="1" ToolTip="Witness Back Driver"  GroupName="WitnessLocation" /></td>
                                    <td><asp:RadioButton runat="server"  ID="rbWitness3rdRowDrivers"   Tag="4" ToolTip="Witness 3rd Row Driver" GroupName="WitnessLocation" /></td>
                                    <td> <asp:RadioButton runat="server"  ID="rbWitness4thRowDrivers"  Tag="22" ToolTip="Witness 4th Row Driver" GroupName="WitnessLocation" /></td>
                                 
                                </tr>
                            </table>
                        </td>
                        <td align="right"><asp:RadioButton runat="server"  ID="rbWitnessBack"  Tag="15" TextAlign="Left"  GroupName="WitnessLocation" /></td>
                    </tr>
                    <tr valign="bottom">
                        <td><asp:RadioButton runat="server"  ID="rbWitnessFrontLeft"  Tag="10"  GroupName="WitnessLocation" /></td>
                        <td align="center"><asp:RadioButton runat="server"  ID="rbWitnessMiddleLeft"  Tag="12"  GroupName="WitnessLocation" /></td>
                        <td align="right" ><asp:RadioButton runat="server"  ID="rbWitnessBackLeft"  Tag="14"  GroupName="WitnessLocation" /></td>
                    </tr>
               </table>
                  
            
             
            
           
            
               
            </div>

        </div>
     <div class="row">
         <div class="col-md-12">
               
               <asp:Button ID="btnPersonOK" runat="server"  class="btn btn-info btn-xs"  Text="OK" OnClick="btnPersonOK_Click"  />
               
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master" CodeBehind="injured.aspx.cs" Inherits="MedCSX.Claim.injured" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <div class="row">
        <div class="col-md-7">
            Injured Person<br />
            <asp:DropDownList ID="ddlInjuredPerson" runat="server" OnSelectedIndexChanged="ddlInjuredPerson_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><br />
            <asp:Label ID="lblClaimantDetails" runat="server" Text="" Width="200px" Height="200px"></asp:Label> 
        </div>
        <div class="col-md-5">
            Liens<br />
             <asp:GridView ID="gvLiens" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="injured_lien_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  AllowPaging="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                            <asp:Button  ID="btnLiensUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="View"    OnClientClick=<%# FormatPopupLiens(Eval("injured_lien_id").ToString(), "r") %>  /> 
                                
                            </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                                   <FooterTemplate>
                                      <asp:Button  ID="btnLiensAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"    OnClientClick=<%# FormatPopupLiens("0", "c") %>  /> 
                                </FooterTemplate>
                            </asp:TemplateField>
                        <asp:BoundField DataField="" HeaderText="Vendor Name"  SortExpression=""  />
                        <asp:BoundField DataField="" HeaderText="Type"  SortExpression=""  />
                        <asp:BoundField DataField="" HeaderText="Lien Amount"  SortExpression=""  />
                        <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                              <asp:Button  ID="btnLiensUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"    OnClientClick=<%# FormatPopupLiens(Eval("injured_lien_id").ToString(), "u") %>  />
                     
                           
                             <asp:Button  ID="btnLiensDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnLiensDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
        </div>
    </div>
   <div class="row">
        <div class="col-md-7">
            Reserves<br />
              <asp:GridView ID="gvReserves" runat="server" AutoGenerateColumns="False" CellPadding="4"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                      <%--  <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                            <asp:Button  ID="btnReserveUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="View"    OnClientClick=<%# FormatPopupReserve(Eval("Reserve_id").ToString(), "r") %>  /> 
                                
                            </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                                   <FooterTemplate>
                                      <asp:Button  ID="btnReserveAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"    OnClientClick=<%# FormatPopupReserve("0", "c") %>  /> 
                                </FooterTemplate>
                            </asp:TemplateField>--%>
                      <%--  <asp:BoundField DataField="reserve_id" HeaderText="Reserve"  SortExpression="reserve_id"  />
                        <asp:BoundField DataField="adjuster_id" HeaderText="Assigned To"  SortExpression="adjuster_id"  />--%>
                        <asp:BoundField DataField="coverage" HeaderText="Reserve"  />
                        <asp:BoundField DataField="" HeaderText="Assigned To"  />
                       <%-- <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                              <asp:Button  ID="btnReserveUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"    OnClientClick=<%# FormatPopupReserve(Eval("Reserve_id").ToString(), "u") %>  />
                     
                           
                             <asp:Button  ID="btnReserveDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnReserveDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>--%>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
              <div class="row">
                  <div class="col-md-12">
                          Assign To User: <asp:DropDownList ID="ddlAssignToUser" runat="server"></asp:DropDownList> or Group: <asp:DropDownList ID="ddlAssignToGroup" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
            <div class="col-md-5">
                <b>Injury Details</b><br />
                Injury Cause: 
                <asp:DropDownList ID="ddlInjuryCause" runat="server">
                </asp:DropDownList>
                <br />
                Injury Type:
                <asp:DropDownList ID="ddlInjuryType" runat="server">   </asp:DropDownList>
                <br />
                Injured
                 <asp:GridView ID="gvInjuredBodyPart" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  AllowPaging="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                            <asp:Button  ID="btnInjuredBodyPartUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="View"    OnClientClick=<%# FormatPopupInjuredBodyPart(Eval("id").ToString(), "r") %>  /> 
                                
                            </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                                   <FooterTemplate>
                                      <asp:Button  ID="btnInjuredBodyPartAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"    OnClientClick=<%# FormatPopupInjuredBodyPart("0", "c") %>  /> 
                                </FooterTemplate>
                            </asp:TemplateField>
                        <asp:BoundField DataField="" HeaderText="Body Part"  SortExpression=""  />
                      
                       
                        <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                              <asp:Button  ID="btnInjuredBodyPartUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"    OnClientClick=<%# FormatPopupInjuredBodyPart(Eval("id").ToString(), "u") %>  />
                     
                           
                             <asp:Button  ID="btnInjuredBodyPartDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnInjuredBodyPartDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
                Extent of Injury<br />
                <asp:TextBox ID="txtExtentofInjury" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>  
   </div>
<div class="row">
       <div class="col-md-12">
               
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" OnClick="btnCancel_Click" />
        </div>
</div>
</asp:Content>
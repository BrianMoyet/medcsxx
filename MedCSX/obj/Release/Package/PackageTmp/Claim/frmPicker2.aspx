﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPicker2.aspx.cs" Inherits="MedCSX.Claim.frmPicker2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:Label ID="txtHeaderLabel" runat="server" Text="This Reserve has Drafts Which Have Not Been Printed.  The Reserve Cannont be Closed."></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-9">
            <asp:GridView ID="grSelections"  runat="server" AutoGenerateColumns="False" CellPadding="4"  DataKeyNames="claimantId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" AutoGenerateSelectButton="true" PageSize="500">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                 
                         <asp:BoundField DataField="fullName" HeaderText="Claimant" />
                         <asp:BoundField DataField="personType" HeaderText="Reserve" />
                         <asp:BoundField DataField="draftNo" HeaderText="Draft No" />
                         <asp:BoundField DataField="draftAmount" HeaderText="Amount"  />
                         <asp:BoundField DataField="draftNo" HeaderText="Draft No" />
                         <asp:BoundField DataField="payee" HeaderText="Payee" />
                         <asp:BoundField DataField="streetAddress" HeaderText="Address 1"  />
                         <asp:BoundField DataField="Address2" HeaderText="Address 2"  />
                         <asp:BoundField DataField="city" HeaderText="City"  />
                         <asp:BoundField DataField="state" HeaderText="State"  />
                         <asp:BoundField DataField="postalCode" HeaderText="Zip"  />
                          
                     
                    
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                   <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                    <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
        </div>
          <div class="col-md-3">
              <asp:LinkButton ID="btnPickOpen" OnClick="btnPickOpen_Click" runat="server">Open</asp:LinkButton>
              <br />
              <asp:LinkButton ID="btnPickRefresh" OnClick="btnPickRefresh_Click" runat="server">Refresh</asp:LinkButton>
        </div>
    </div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnPickOK" runat="server" class="btn btn-info btn-xs"  Text="OK" />
               <asp:Button ID="btnPickCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"   OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="New Task" Language="C#" AutoEventWireup="true"   MasterPageFile="~/SiteModal.Master"  CodeBehind="frmNewTask.aspx.cs" Inherits="MedCSX.Claim.frmNewTask" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="row">
      <div class="col-md-1">
            Task Type:
      </div>
      <div class="col-md-11">
         <asp:DropDownList ID="cboTaskType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboTaskType_SelectedIndexChanged"></asp:DropDownList>
      </div>
</div>
     <div class="row">
      <div class="col-md-1">
            Task Description:
      </div>
      <div class="col-md-11">
          <asp:TextBox ID="tbDescription" runat="server" TextMode="MultiLine" Height="125px" Width="500px"></asp:TextBox>
      </div>
</div>
       <div class="row">
      <div class="col-md-1">
            Claim:
      </div>
      <div class="col-md-11">
          <asp:TextBox ID="tbClaim" runat="server"></asp:TextBox>
      </div>
</div>
       <div class="row">
      <div class="col-md-1">
            Claimant:
      </div>
      <div class="col-md-11">
         <asp:DropDownList ID="cboClaimants" runat="server" OnSelectedIndexChanged="cboClaimants_SelectedIndexChanged" CausesValidation="false" AutoPostBack="true"></asp:DropDownList>
        
      </div>
</div>
       <div class="row">
      <div class="col-md-1">
            Reserve:
      </div>
      <div class="col-md-11">
         <asp:DropDownList ID="cboReserves" runat="server"></asp:DropDownList>
      </div>
</div>
    <div class="row">
         <div class="col-md-12">
               <asp:Button ID="btnOk" runat="server"  class="btn btn-info btn-xs"  Text="OK" OnClick="btnOk_Click" />
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
        </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="FileNote.aspx.cs" Inherits="MedCSX.Claim.FileNote" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

      <div class="row">
          <div class="col-md-3">
              Type:
              <br /><asp:ListBox ID="lbFileNoteType" runat="server" Rows="6"  width="225px" Height="120px"></asp:ListBox>
                 <br /><asp:CheckBox ID="chkCleared" Text="Cleared" runat="server" />
          </div>
           <div class="col-md-3">
                File Note Text:<br />
               <asp:TextBox ID="txtFileNoteText" runat="server" TextMode="MultiLine"  width="225px" Height="120px"></asp:TextBox>
               <br /><asp:CheckBox ID="chkSummary" Text="Summary" runat="server" /> <asp:CheckBox ID="chkUrgent" Text="Urgent" runat="server" />
          </div>
           <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        Claimant: <asp:DropDownList ID="ddlClaimant" AutoPostBack="true" OnSelectedIndexChanged="ddlClaimant_SelectedIndexChanged" runat="server"></asp:DropDownList><br />
                        Reserve: &nbsp;&nbsp;<asp:DropDownList ID="ddlReserve" runat="server"></asp:DropDownList>
                     </div>
                    <div class="row">
                         <div class="col-md-6">
                             Send Alert To:
                             <br /><asp:CheckBox ID="chkAlertAssignedAdjuster" Text="Assigned Adjuster" runat="server" />
                             <br /><asp:CheckBox ID="chkAlertTheirSupervisor" Text="Their Supervisor" runat="server" />
                             <br /><asp:CheckBox ID="chkDeferAlertsUntil" Text="Defer Alerts Until" runat="server" />
                             <br /><asp:TextBox ID="txtDeferAlertsUntil" runat="server" TextMode="DateTimeLocal"></asp:TextBox>
                         </div>
                         <div class="col-md-6">
                             Set Diary For:
                                <br /><asp:CheckBox ID="chkDiaryAssignedAdjuster" Text="Assigned Adjuster" runat="server" />
                             <br /><asp:CheckBox ID="chkDiaryTheirSupervisor" Text="Their Supervisor" runat="server" />
                             <br /><asp:DropDownList ID="ddlDiaryType" runat="server" Width="190px"></asp:DropDownList>
                             <br />Nbr of Days <asp:TextBox ID="txtNumberOfDays" runat="server"></asp:TextBox>
                         </div>
                    </div>
               </div>
          </div>
      </div>
     <div class="row">
                    
                    <div class="row">
                         <div class="col-md-4">
                             Event Type:<asp:DropDownList ID="ddlEventType" runat="server"></asp:DropDownList>
                         </div>
                         <div class="col-md-4">
                              Assigned To: <asp:Label ID="lblAssignedTo" runat="server" Text=""></asp:Label>
                             <br />Created By: <asp:Label ID="lblCreatedBy" runat="server" Text=""></asp:Label> <asp:Label ID="lblCreatedDate" runat="server" Text="Label"></asp:Label>
                             <br />Updated By;  <asp:Label ID="lblUpdatedBy" runat="server" Text=""></asp:Label> <asp:Label ID="lblUpdatedDate" runat="server" Text="Label"></asp:Label>
                         </div>
                    </div>
               </div>
       <div class="row">
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
        </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/SiteModal.Master" CodeBehind="frmManualPhoneNumber.aspx.cs" Inherits="MedCSX.Claim.frmManualPhoneNumber" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
          <h4>Add Phone Number</h4>
        </div>
</div>
<div class="row">
        <div class="col-md-4">
          Name:
        </div>
         <div class="col-md-4">
             <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        </div>
</div>
<div class="row">
    <div class="col-md-4">
          Home Phone:
        </div>
         <div class="col-md-4">
             <asp:TextBox ID="txtHomePhone" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="txtHomePhone_TextChanged"></asp:TextBox>
        </div>
</div>
<div class="row">
    <div class="col-md-4">
          Work Phone:
        </div>
         <div class="col-md-4">
             <asp:TextBox ID="txtWorkPhone" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="txtWorkPhone_TextChanged"></asp:TextBox>
        </div>
</div>
<div class="row">
    <div class="col-md-4">
          Cell Phone:
        </div>
         <div class="col-md-4">
             <asp:TextBox ID="txtCellPhone" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="txtCellPhone_TextChanged"></asp:TextBox>
        </div>
</div>
<div class="row">
    <div class="col-md-4">
          Fax Phone:
        </div>
         <div class="col-md-4">
             <asp:TextBox ID="txtFax" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="txtFax_TextChanged"></asp:TextBox>
        </div>
</div>
    <div class="row">
    <div class="col-md-4">
          Email:
        </div>
        <div class="col-md-4">
             <asp:TextBox ID="txtEmail" runat="server" TextMode="Phone"></asp:TextBox>
        </div>
</div>
     <div class="row">
           <div class="col-md-12">
               &nbsp;
                </div>
    </div>
       <div class="row">
           <div class="col-md-12">
                   <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click" />&nbsp;
                   <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
                   <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
            </div>
    </div>
</asp:content>

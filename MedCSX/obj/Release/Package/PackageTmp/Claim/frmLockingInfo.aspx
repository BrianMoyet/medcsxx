﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmLockingInfo.aspx.cs" Inherits="MedCSX.Claim.frmLockingInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-9">
            <asp:GridView ID="grSelections" runat="server" AutoGenerateColumns="False" CellPadding="4" OnSelectedIndexChanged="grSelections_SelectedIndexChanged"  DataKeyNames="lockId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" AutoGenerateSelectButton="true" PageSize="500">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />  <Columns>
                                 
                         <asp:BoundField DataField="isLocked" HeaderText="Locked" />
                         <asp:BoundField DataField="lockReason" HeaderText="Lock Reason" />
                         <asp:BoundField DataField="dtLocked" HeaderText="Lock Date" />
                         <asp:BoundField DataField="userId" HeaderText="User"  />
                         <asp:BoundField DataField="dtUnlocked" HeaderText="Unlock Date" />
                        
                          
                     
                    
                    </Columns>
                <EmptyDataTemplate>
        <asp:Label ID="lblEmptyTxt" runat="server" Text="No Locking Info found."></asp:Label>
      </EmptyDataTemplate>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                 <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                    <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
            <asp:Label ID="lblLocking" runat="server" Text="No Locking Info found for this claim." Visible="false"></asp:Label>
        </div>
        <div class="col-md-3">
            
            File Note  <asp:RequiredFieldValidator ID="rfvFileNote" runat="server" ErrorMessage="Required" ControlToValidate="txtFileNote" ForeColor="#FF3300"> </asp:RequiredFieldValidator> 
            <br />
            <asp:TextBox ID="txtFileNote" TextMode="MultiLine" runat="server"></asp:TextBox>
           <br />
            <asp:LinkButton ID="btnUnLock"  class="btn btn-info btn-xs" runat="server" OnClick="btnUnLock_Click" CausesValidation="true">Unlock</asp:LinkButton><asp:Label ID="lblLockPermissions" runat="server" Text=""></asp:Label>
            <br />
            <asp:LinkButton ID="btnManLock"  class="btn btn-info btn-xs" runat="server" OnClick="btnManLock_Click" CausesValidation="true"  >Manually Lock Claim</asp:LinkButton>
          
        </div>
    </div>
     <div class="row">
        <div class="col-md-9">
            <b>Lock Details</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    Locked:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lblLocked" runat="server" Text=""></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-3">
                    Lock Reason:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lblLockReason" runat="server" Text=""></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-3">
                    Lock Date:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lbldtLock" runat="server" Text=""></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-3">
                    LockFile Note Text:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lblLockTxt" runat="server" Text="" Height="100px"></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-3">
                    Unlocked By:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lblUnLock" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Unlock Date:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lbldtUnLock" runat="server" Text=""></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-3">
                    Unlock File Note Text:
                </div>
                 <div class="col-md-9">
                     <asp:Label ID="lblUnLockTxt" runat="server" Text=""  Height="100px"></asp:Label>
                </div>
            </div>
        </div>
    </div>
      <div class="row">
          <div class="col-md-12">
               
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close" CausesValidation="false"   OnClick="btnCancel_Click" />
              </div>
        </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="propertyDamage.aspx.cs" Inherits="MedCSX.Claim.propertyDamage1" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <%--   <style>
        .MyTabStyle .ajax__tab_tab /*inactive tab*/
        { 
         outline: 0; 
        }

        .MyTabStyle .ajax__tab_active .ajax__tab_tab  /*active tab*/
        { 
         color: #ff0000; 
         outline: 0; 
        }
        .MyTabStyle .ajax__tab_header .ajax__tab_inner
         {
             border-color: #ff0000;
             color: #ff0000;
     
         }
        input[type=radio] {
            float: left;
        }

        label {
            margin-left: 2px;
            //display: block;
        }

    </style>--%>


      <div class="row">
            <div class="col-md-2">
              Reserve Type:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="ddlReserveType" runat="server"></asp:DropDownList>
            </div>
          
       </div>
    
     <div class="row">
            <div class="col-md-2">
              Property Type:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="ddlPropertyType" runat="server"></asp:DropDownList>
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              Auto:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="ddlAuto" runat="server"></asp:DropDownList>
            </div>
       </div>
    <div class="row">
            <div class="col-md-2">
              Owner:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="ddlOwner" runat="server"></asp:DropDownList>
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-5">
                   write out
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              Driver:
            </div>
            <div class="col-md-5">
                   <asp:DropDownList ID="ddlDriver" runat="server"></asp:DropDownList>
            </div>
       </div>
      <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-5">
                   write out
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              Where Seen:
            </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtWhereSeen" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-5">
                 (Required for Appraisals)
            </div>
       </div>
    <div class="row">
            <div class="col-md-2">
             Describe Damage:
            </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtDescribeDamage" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-5">
             (Required for Appraisals)
            </div>
       </div>
      <div class="row">
            <div class="col-md-2">
              Estimated Amount:
            </div>
            <div class="col-md-5">
                <asp:TextBox ID="txtEstimatedAmount" runat="server"></asp:TextBox>
            </div>
       </div>
       <div class="row">
            <div class="col-md-12">
                Other Insurance <hr style='display:inline-block; width:100px;' />
            </div>
       </div>
    <div class="row">
        <div class="col-md-1">
            Company:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            Policy No:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="txtPolicyNo" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            Claim No:
        </div>
       <div class="col-md-3">
        <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
            </div>
     </div>

     <div class="row">
        <div class="col-md-1">
            Adjuster:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="txtAdjuster" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            Phone:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="txtPhone" runat="server" TextMode="Phone"></asp:TextBox>
            </div>
         <div class="col-md-1">
           Fax:
        </div>
         <div class="col-md-3">
        <asp:TextBox ID="txtFax"  TextMode="Phone" runat="server"></asp:TextBox>
            </div>
     </div>
    
    <div class="row">
        <div class="col-md-1">
            Address:
        </div>
        <div class="col-md-3">
        <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-1">
            City:
        </div>
         <div class="col-md-3">
        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </div>
         <div class="col-md-4">
           State: <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList> &nbsp;Zip: <asp:TextBox ID="txtZip" runat="server" Width="60px"></asp:TextBox>
        </div>
     </div>
     <div class="row">
          <div class="col-md-12">
                  Assign To User: <asp:DropDownList ID="ddlAssignToUser" runat="server"></asp:DropDownList> or Group: <asp:DropDownList ID="ddlAssignToGroup" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

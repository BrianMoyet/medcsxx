﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmDraft.aspx.cs" Inherits="MedCSX.Claim.frmDraft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="reason" runat="server" visible="false">
        <div class="row">
               <div class="col-md-12">
             Reissue Reason: 
              
                 <asp:DropDownList ID="cboVoidReason" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboVoidReason_SelectedIndexChanged"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div runat="server" id="tiDraft" visible="false">

<div class="row">
    <div class="col-md-5">
                <div class="row">
             <div class="col-md-2">
             Tax Id: 
             </div>    
             <div class="col-md-10">
                 <asp:TextBox ID="txtTaxId" runat="server" OnTextChanged="txtTaxId_TextChanged" AutoPostBack="true" CausesValidation="false" TabIndex="3"></asp:TextBox>
             &nbsp;<asp:Label ID="lblTaxID" runat="server" ForeColor="Red" Visible="False" TabIndex="900"></asp:Label>
                 <asp:TextBox ID="txtVendorId" runat="server" OnTextChanged="txtVendorId_TextChanged" AutoPostBack="true" CausesValidation="false" Visible="false" ></asp:TextBox>
             </div>  
        </div>
        
<div class="row">
    <div class="col-md-12">
        Don't Know Tax Id? <asp:LinkButton ID="btnVendorLookup" runat="server" OnClick="btnVendorLookup_Click" CausesValidation="false" TabIndex="4">Lookup Vendor...</asp:LinkButton>
 <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                        Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false"  OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
                </div>
            </div>
        </div>
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkClaimantAttorney" runat="server" Text="Claimant Attorney" TabIndex="5"/>
    </div>
</div>
        <div class="row">
            <div class="col-md-12">
                  <asp:RadioButton ID="rbTransLoss" runat="server" Text="Loss" OnCheckedChanged="rbTransLoss_CheckedChanged"  AutoPostBack="true" CausesValidation="false" GroupName="Trans"  />&nbsp;
                <asp:RadioButton ID="rbTransExpense" runat="server" Text="Expense" OnCheckedChanged="rbTransExpense_CheckedChanged" AutoPostBack="true" CausesValidation="false" GroupName="Trans" TabIndex="1" />
                <br /><asp:Label ID="lblTransactionTypes" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>
        </div>
         <div class="row">
             <div class="col-md-2">
             Transaction  Type:
             </div>    
             <div class="col-md-10">
                 <asp:DropDownList ID="cboTransactionType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboTransactionType_SelectedIndexChanged" TabIndex="2"></asp:DropDownList>
             </div>  
        </div>

        <div class="row">
            <div class="col-md-2">
            Payee:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPayee" runat="server" TextMode="MultiLine" Height="55px" Width="300px" TabIndex="6"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                <asp:Label ID="lblTransAmount" runat="server" Text="Amount:"></asp:Label>
                
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtTransAmount" runat="server" Width="75px" TabIndex="7" TextMode="Number" step=".01" ></asp:TextBox> &nbsp;<asp:ImageButton ID="btnTransAmountInfo" ImageUrl="../content/Images/mnuInformation.png" runat="server" ToolTip="Enter the amount in Legal Fees for that part of an attorney bill that is for an attorney, paralegal or legal assistant�s time. Do not enter that part of a bill that deals with other expenses, such as vendors, court fees, postage, copies, documents and/or exhibits, etc." />
                &nbsp:&nbsp;&nbsp;<asp:Label ID="lblTransExpense" runat="server" Text="Expense Amount:"   ></asp:Label> 
                <asp:TextBox ID="txtTransExpenseAmount" runat="server"  Width="75px" TabIndex="8" TextMode="Number" step=".01" >0</asp:TextBox> &nbsp;<asp:ImageButton ID="btnTransExpensInfo" ImageUrl="../content/Images/mnuInformation.png" runat="server" ToolTip="Enter the amount in Legal Expenses for that part of an attorney bill that relates to other costs, such as copies, court fees, documents, exhibits, other expenses or payment of a vendor bill. If this is a bill from a vendor other than an attorney, such as a court reporter, enter the entire amount as a Legal Expense." />
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                <asp:Label ID="lblPIPfrom" runat="server" Text="PIP Dates:"></asp:Label>
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPIPfrom" TextMode="Date" runat="server" TabIndex="9"></asp:TextBox>&nbsp;&nbsp; <asp:Label ID="lblPIPto" runat="server" Text="To:"></asp:Label>  <asp:TextBox ID="txtPIPto" TextMode="Date" runat="server" TabIndex="10"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
            Explanation of Proceeds:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtExplanation" runat="server" TextMode="MultiLine" Height="55px" Width="300px" TabIndex="11"></asp:TextBox>
            </div>
        </div>
        <div runat="server" id="gbMailTo">
         <div class="row">
              <div class="col-md-2">
              
            </div>
            <div class="col-md-10">
                  <b>Mail To:</b></div>
        </div>
         <div class="row">
              <div class="col-md-2">
                Name:
                  
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtMailToName" runat="server" widht="340px" Width="300px" TabIndex="12"></asp:TextBox>
            </div>
        </div>
         <div class="row">
              <div class="col-md-2">
                Address 1:
                 
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtMailToAddress1" runat="server" widht="340px" Width="300px" TabIndex="13"></asp:TextBox>
            </div>
        </div>
         <div class="row">
              <div class="col-md-2">
                Address 2:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtMailToAddress2" runat="server" widht="340px" Width="300px" TabIndex="14"></asp:TextBox>
            </div>
        </div>
          <div class="row">
              <div class="col-md-2">
                City:
                  
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtMailToCity" runat="server" widht="340px" TabIndex="15"></asp:TextBox>
                &nbsp;&nbsp;&nbsp; State <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="cboMailToState" ErrorMessage="**" ForeColor="Red" MaximumValue="100" MinimumValue="1" Enabled="false">**</asp:RangeValidator>
                : <asp:DropDownList ID="cboMailToState" runat="server" TabIndex="16"></asp:DropDownList>
                &nbsp;&nbsp;&nbsp; <br />Zip: 
             &nbsp;<asp:TextBox ID="txtMailToZip" runat="server" Width="54px" AutoPostBack="True" OnTextChanged="txtMailToZip_TextChanged" TabIndex="17"></asp:TextBox>
            </div>
        </div>
        </div>  <%-- close gb medical lien--%>
    </div>
    <div class="col-md-7">
        <div class="row">
           
            <div class="col-md-12">
                 
                 <asp:Image ID="imgCheck"  ImageUrl="../content/Images/imgCheck.png" runat="server" Width="150px" />
            </div>
        </div>
        <div runat="server" id="gbLossPayee">
            <div class="row">
                <div class="col-md-12">
                    <b>Loss Payee Reminder</b>
                </div>
            </div>
             <div class="row">
                <div class="col-md-10"  style="background-color:lightgoldenrodyellow;">
                    <asp:CheckBox ID="chkLossPayee" runat="server" Text="I Have Reviewed Loss Payee:" TabIndex="18"/>
                   <br /><asp:Label ID="txtLossPayee" runat="server" Text="Loss payee Info here" Width="175px" Height="100" ForColor="blue"></asp:Label>
                </div>
                   <div class="col-md-2">
                               &nbsp;</div>
            </div>
             <div class="row">
                <div class="col-md-12">
                   <hr />
                </div>
            </div>
        </div> <%-- Close tiDraft--%>
         <div runat="server" id="gbDeductible">
            <div class="row">
                <div class="col-md-12">
                    <b>Deductible Reminder</b>
                </div>
            </div>
              <div class="row">
                      
                <div class="col-md-10"  style="background-color:lightgoldenrodyellow;">
                    <asp:CheckBox ID="chkDeductible" runat="server" Text="I Have Reviewed Deductible:" TabIndex="19" /><br />
                   <br />Deductible Amount: <asp:Label ID="txtDeductible" runat="server" Text="$$"  Width="78px" ForeColor="Blue"></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-12">
                   <hr />
                </div>
            </div>
        </div>     <%--close gbpendmailto--%>
        <div runat="server" id="gbMedicalLien" visible="false">
            <div class="row">
            <div class="col-md-12">
               <b>Medical Lien Reminder</b>
            </div>
        </div>
            <div class="row">
                      
            <div class="col-md-10"  style="background-color:lightgoldenrodyellow;">
                <asp:CheckBox ID="chkMedicalLien" runat="server" Text="I Have Reviewed Medical Liens:" TabIndex="20" /><br />
                <br /><asp:Label ID="txtMedicalLien" runat="server"  Width="175px" Height="100px"></asp:Label>
            </div>
             <div class="col-md-2">&nbsp;</div>
        </div>
        </div> <%-- close gb medical lien--%>
    </div>
</div>
</div> <%-- Close tiDraft--%>

<div runat="server" id="tiPending" visible="false">
<div class="row">
    <div class="col-md-5">
         <div class="row">
             <div class="col-md-2">
            Sent To:
             </div>    
             <div class="col-md-10">
                 <asp:TextBox ID="txtPendSentTo" runat="server" TabIndex="21"></asp:TextBox>
             </div>  
        </div>
         <div class="row">
             <div class="col-md-2">
             Transaction  Type:
             </div>    
             <div class="col-md-10">
                 <asp:DropDownList ID="cboPendTransactionType" runat="server" OnSelectedIndexChanged="cboPendTransactionType_SelectedIndexChanged" AutoPostBack="true" TabIndex="22"></asp:DropDownList>
             </div>  
        </div>
         <div class="row">
             <div class="col-md-2">
             
             </div>    
             <div class="col-md-10">
                 <asp:RadioButton ID="rbPendTransLoss" runat="server" Text="Loss" OnCheckedChanged="rbTransLoss_CheckedChanged"  AutoPostBack="true" CausesValidation="false" GroupName="Pend" TabIndex="24" />
                <br /><asp:RadioButton ID="rbPendTransExpense" runat="server" Text="Expense" OnCheckedChanged="rbTransExpense_CheckedChanged"  AutoPostBack="true" CausesValidation="false" GroupName="Pend" TabIndex="25" />
             </div>  
        </div>
        <div class="row">
             <div class="col-md-2">
             Tax Id:
                 
             </div>    
             <div class="col-md-10">
                 <asp:TextBox ID="txtPendTaxId" runat="server" OnTextChanged="txtTaxId_TextChanged" TabIndex="26"></asp:TextBox>
             </div>  
        </div>
        
<div class="row">
    <div class="col-md-12">
        Don't Know Tax Id? <asp:LinkButton ID="btnPendVendorLookup" runat="server" OnClick="btnVendorLookup_Click" CausesValidation="false">Lookup Vendor...</asp:LinkButton>
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkPendClaimantAttorney" runat="server" Text="Claimant Attorney" TabIndex="27"/>
    </div>
</div>
        <div class="row">
            <div class="col-md-2">
            Payee:
                
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPendPayee" runat="server" TextMode="MultiLine" Height="55px" Width="300px" TabIndex="28"></asp:TextBox>
            </div>
        </div>
      
         <div class="row">
            <div class="col-md-2">
            Explanation of Proceeds:
                
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPendExplanation" runat="server" TextMode="MultiLine" Height="55px" Width="300px" TabIndex="29"></asp:TextBox>
            </div>
        </div>
        <div runat="server" id="gbPendMailTo">
         <div class="row">
              <div class="col-md-2">
              
            </div>
            <div class="col-md-10">
                  <b>Mail TO:</b>
            </div>
        </div>
         <div class="row">
              <div class="col-md-2">
                Name:
                 
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPendMailToName" runat="server" widht="340px" Width="300px" OnTextChanged="txtPendMailToName_TextChanged" AutoPostBack="true" CausesValidation="false" TabIndex="30"></asp:TextBox>
            </div>
        </div>
         <div class="row">
              <div class="col-md-2">
                Address 1:
                 
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPendMailToAddress1" runat="server" widht="340px" Width="300px"  OnTextChanged="txtPendMailToName_TextChanged" AutoPostBack="true" CausesValidation="false" TabIndex="31"></asp:TextBox>
            </div>
        </div>
         <div class="row">
              <div class="col-md-2">
                Address 2:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPendMailToAddress2" runat="server" widht="340px" Width="300px"  OnTextChanged="txtPendMailToName_TextChanged" AutoPostBack="true" CausesValidation="false" TabIndex="32"></asp:TextBox>
            </div>
        </div>
          <div class="row">
              <div class="col-md-2">
                City:
                 
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtPendMailToCity" runat="server" widht="340px" TabIndex="33"></asp:TextBox>
                &nbsp;&nbsp;&nbsp; State: 
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="cboPendMailToState" ErrorMessage="**" Enabled="false" ForeColor="Red" MaximumValue="100" MinimumValue="1">**</asp:RangeValidator>
&nbsp;<asp:DropDownList ID="cboPendMailToState" runat="server" TabIndex="34"></asp:DropDownList>
                &nbsp;&nbsp;&nbsp; Zip:
             &nbsp;<asp:TextBox ID="txtPendMailToZip" runat="server" Width="54px" AutoPostBack="True" OnTextChanged="txtPendMailToZip_TextChanged" TabIndex="35"></asp:TextBox>
            </div>
        </div>  
        </div> <%--close gbpendmailto--%>
    </div>
</div>
</div>
    <div id="totalLoss" runat="server">
        <div class="row">
            <div class="col-md-12">
        <asp:CheckBox ID="chkTotalLoss" Text="Total Loss?" runat="server" />
    </div>
</div>
        </div>
<div class="row">
    <div class="col-md-12">
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" class="btn btn-info btn-xs"  runat="server" Text="OK" OnClick="btnOK_Click" TabIndex="37" />  <asp:Button ID="btnPrint" class="btn btn-info btn-xs"  runat="server" Text="Print Draft..." OnClick="btnPrint_Click" TabIndex="36" />    <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Label ID="lblDraftQueueWarning" runat="server" Text="Draft Printing is Currently Turned Off. Draft Will Be Placed in Queue and Printed Later." BackColor="#FFC0C0" widht="100%"></asp:Label>
    </div>
</div>
</asp:Content>

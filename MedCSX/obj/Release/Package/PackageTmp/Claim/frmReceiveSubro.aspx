﻿<%@ Page Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmReceiveSubro.aspx.cs" Inherits="MedCSX.Claim.frmReceiveSubro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Recovery Source:
        </div>
         <div class="col-md-7">
             <asp:TextBox ID="txtSubroId" runat="server"></asp:TextBox>
        </div>
  </div>
    <div class="row">
         <div class="col-md-3">
            Check No:
        </div>
         <div class="col-md-7">
             <asp:TextBox ID="txtSubroDraftNo" runat="server"></asp:TextBox>
        </div>
</div>
      <div class="row">
         <div class="col-md-3">
            Subrogation Amount:
        </div>
         <div class="col-md-7">
             <asp:TextBox ID="txtSubroAmount" runat="server"></asp:TextBox>
        </div>
</div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"/>
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

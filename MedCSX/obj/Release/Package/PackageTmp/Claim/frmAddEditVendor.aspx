﻿<%@ Page Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAddEditVendor.aspx.cs" Inherits="MedCSX.Claim.frmAddEditVendor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-12">
        <b>Vendor Details</b>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        Vendor Name:
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVendorName" ForeColor="Red" ErrorMessage="**">**</asp:RequiredFieldValidator>
    </div>
    <div class="col-md-10">
        <asp:TextBox ID="txtVendorName" runat="server"></asp:TextBox>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        Type:
    </div>
    <div class="col-md-4">
        <asp:DropDownList ID="cboVendorType" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="cboVendorType_SelectedIndexChanged"></asp:DropDownList>
    </div>
    <div class="col-md-2">
        Sub Type:
    </div>
    <div class="col-md-4">
        <asp:DropDownList ID="cboVendorSubType" runat="server"></asp:DropDownList>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        Address 1:
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  ForeColor="Red" ControlToValidate="txtVendorAddress1" ErrorMessage="**">**</asp:RequiredFieldValidator>
    </div>
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorAddress1" runat="server"></asp:TextBox>
    </div>
    <div class="col-md-2">
        Address 2:
    </div>
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorAddress2" runat="server"></asp:TextBox>
    </div>
</div>
    <div class="row">
    <div class="col-md-2">
        City:
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  ForeColor="Red" ControlToValidate="txtVendorCity" ErrorMessage="**">**</asp:RequiredFieldValidator>
    </div>
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorCity" runat="server"></asp:TextBox>
    </div>
    <div class="col-md-2">
        State: 
        </div>
        <div class="col=md-4">
        <asp:DropDownList ID="cboVendorState" runat="server"></asp:DropDownList> Zip: 
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtVendorZipcode"  ForeColor="Red" EnableViewState="False" ErrorMessage="**">**</asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtVendorZipcode"  ForeColor="Red" ErrorMessage="**" ValidationExpression="\d{5}(-\d{4})?">**</asp:RegularExpressionValidator>
&nbsp;<asp:TextBox ID="txtVendorZipcode" runat="server" CausesValidation="false" AutoPostBack="true" OnTextChanged="txtVendorZipcode_TextChanged"></asp:TextBox>
    </div>
</div>
    <div class="row">
    <div class="col-md-2">
       PhonPhone:
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtVendorPhone" ErrorMessage="**" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">**</asp:RegularExpressionValidator>
    </div>
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorPhone" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="txtVendorPhone_TextChanged"></asp:TextBox>
    </div>
    <div class="col-md-2">
        Fax:Fax:
       </div> 
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorFax" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="txtVendorFax_TextChanged"></asp:TextBox>
    </div>
</div>
       <div class="row">
    <div class="col-md-2">
       Email:
     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtVendorEmail" ErrorMessage="**" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">**</asp:RegularExpressionValidator>
    </div>
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorEmail" runat="server" TextMode="Email"></asp:TextBox>
    </div>
    <div class="col-md-2">
        Tax Id:
    </div>
    <div class="col-md-4">
        <asp:TextBox ID="txtVendorTaxId" runat="server" TextMode="Phone" AutoPostBack="true" CausesValidation="false" OnTextChanged="txtVendorTaxId_TextChanged"></asp:TextBox>
        <br /><asp:Label ID="lblvendorTaxIDMessage" runat="server" ForeColor="Red" Text="Tax Id not in the correct format (xx-xxxxxxx)" Visible="False"></asp:Label>
    </div>
</div>
 <div class="row">
    <div class="col-md-2">
      
    </div>
    <div class="col-md-4">
        <asp:CheckBox ID="chkVendorTaxIdRequired" runat="server" Text="Tax Id Required" />
       
    </div>
    <div class="col-md-2">
      
    </div>
    <div class="col-md-4">
        <asp:CheckBox ID="chkVendor1099Required" runat="server" Text="1099 Required" />
    </div>
</div>
        <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"/> <asp:Label ID="lblMessageBox" runat="server" Text=""></asp:Label>
               <asp:Button ID="btnInjuredCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>

</asp:Content>

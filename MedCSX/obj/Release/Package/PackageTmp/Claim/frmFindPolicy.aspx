﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmFindPolicy.aspx.cs" Inherits="MedCSX.Claim.frmFindPolicy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="DivSearch" runat="server">
    <div class="row">
        <div class="col-md-12">
            Date of Loss:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbPolicyDOL" ErrorMessage="**" Font-Bold="True" ForeColor="Red" EnableViewState="false" Text="**"></asp:RequiredFieldValidator>
            &nbsp;<asp:TextBox ID="tbPolicyDOL" runat="server" TextMode="Date"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;Policy No: <asp:TextBox ID="tbPolicyNum" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
           First Name:&nbsp;&nbsp;&nbsp; <asp:TextBox ID="tbPolicyFName" runat="server"></asp:TextBox>
            &nbsp; &nbsp; &nbsp;Last Name: <asp:TextBox ID="tbPolicyLName" runat="server"></asp:TextBox>
            SSN: <asp:TextBox ID="tbPolicySSN" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
           Address: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="tbPolicyAddress" runat="server"></asp:TextBox>
            &nbsp;
             &nbsp; &nbsp;City: <asp:TextBox ID="tbPolicyCity" runat="server"></asp:TextBox>
            State: <asp:DropDownList ID="cboPolicyState" runat="server"></asp:DropDownList> &nbsp;Zip: <asp:TextBox ID="tbPolicyZipCode" runat="server" Width="60px"></asp:TextBox>

            :
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 bg-light">
            Vehicle 
            </div>
    </div>
     <div class="row">
     <div class="col-md-12">
           Auto Year:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="tbPolicyVehicleYear" runat="server"></asp:TextBox>
            &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Make: <asp:TextBox ID="tbPolicyVehicleMake" runat="server"></asp:TextBox>
            Model: <asp:TextBox ID="tbPolicyVehicleModel" runat="server"></asp:TextBox>
            VIN: <asp:TextBox ID="tbPolicyVehicleVIN" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
             
         <div class="col-md-12">
               <asp:Button ID="btnPolicySearch" runat="server" class="btn btn-info btn-xs"  Text="Search" CausesValidation="False" OnClick="btnPolicySearch_Click"  />&nbsp;<asp:Label ID="lblInvalidSearch" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
               <br /><br />
        </div>
    </div>
      <div class="row">
        <div class="col-md-12 bg-info ">
            <b>Policies Found</b> 
           
           
            </div>
    </div>
         <div class="row">
         <div class="col-md-12">
                   <asp:LinkButton ID="showActivePolicies" runat="server" Text="Show Active Policies" OnClick="showActivePolicies_Click"></asp:LinkButton> 
             &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="showExpiredPolicies" runat="server" Text="Show Expired Policies" OnClick="showExpiredPolicies_Click"></asp:LinkButton>
      
           
            </div>
             </div>
   
    <div class="row">
         <div class="col-md-12">
          <asp:GridView ID="grPolicySearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" OnSelectedIndexChanged="grPolicySearch_SelectedIndexChanged" CellPadding="4" DataKeyNames=""   ShowHeaderWhenEmpty="True" EmptyDataText="No Results Found." 
                    ForeColor="#333333"  Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
            
             
                        <asp:BoundField DataField="policyNo" HeaderText="Policy"  SortExpression="policyNo"/>
                        <asp:BoundField DataField="versionNo" HeaderText="Version No"  SortExpression="versionNo"/>
                        <asp:BoundField DataField="dtEffective" HeaderText="Eff Date" SortExpression="dtEffective"/>
                        <asp:BoundField DataField="dtExpire" HeaderText="Exp Date" SortExpression="dtExpire"/>
                        <asp:BoundField DataField="fName" HeaderText="First Name" SortExpression="fName"/>
                        <asp:BoundField DataField="lName" HeaderText="Last Name" SortExpression="lName"/>
                        <asp:BoundField DataField="vehYear" HeaderText="Year" SortExpression="vehYear"/>
                        <asp:BoundField DataField="vehMake" HeaderText="Make" SortExpression="vehMake"/>
                        <asp:BoundField DataField="vehModel" HeaderText="Model" SortExpression="vehModel"/>
                        <asp:BoundField DataField="vehVIN" HeaderText="VIN" SortExpression="vehVIN"/>
                      
                       
            </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
             <EditRowStyle BackColor="#999999" />
                     <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
             </div>
    </div>
       <div class="row">
        <div class="col-md-12  text-center ">
            <b>Policy Details </b>
            </div>
    </div>
     <div class="row">
        <div class="col-md-12 text-center">
         <b>Individuals on Policy</b>
            </div>
    </div>
      <div class="row">
         <div class="col-md-12">
          <asp:GridView ID="grPolicyIndividuals" runat="server"   AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No Results Found." 
                    ForeColor="#333333"   Width="100%" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
            
                        
                        <asp:BoundField DataField="fullName" HeaderText="Name"  SortExpression=""/>
                        <asp:BoundField DataField="personType" HeaderText="Type"  SortExpression=""/>
                        <asp:BoundField DataField="streetAddress" HeaderText="Address" SortExpression=""/>
                        <asp:BoundField DataField="phone" HeaderText="Home Phone" SortExpression=""/>
                        <asp:BoundField DataField="cDOB" HeaderText="Date of Birth" SortExpression=""/>
                        <asp:BoundField DataField="cAge" HeaderText="Age" SortExpression=""/>
                        <asp:BoundField DataField="cSex" HeaderText="Sex" SortExpression=""/>
                        <asp:BoundField DataField="MaritalStatus" HeaderText="Marital Status" SortExpression=""/>
                        
                      
                       
            </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
           <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
            <EditRowStyle BackColor="#999999" />
                     <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
             </div>
    </div>
     <div class="row">
        <div class="col-md-12 text-center">
         <b>Vehicles on Policy</b>
            </div>
    </div>
      <div class="row">
         <div class="col-md-12">
          <asp:GridView ID="grPolicyVehicles" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="vehicleId"   ShowHeaderWhenEmpty="True" EmptyDataText="No Results Found." 
                    ForeColor="#333333"   Width="100%" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
            
             
                        <asp:BoundField DataField="vehicleId" HeaderText="Vehicle No"  SortExpression=""/>
                        <asp:BoundField DataField="level" HeaderText="Level"  SortExpression=""/>
                        <asp:BoundField DataField="vYear" HeaderText="Year" SortExpression=""/>
                        <asp:BoundField DataField="vMake" HeaderText="Nake" SortExpression=""/>
                        <asp:BoundField DataField="vModel" HeaderText="Model" SortExpression=""/>
                        <asp:BoundField DataField="vVIN" HeaderText="VIN" SortExpression=""/>
                        <asp:BoundField DataField="vAge" HeaderText="Age" SortExpression=""/>
                        
                      
                       
            </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
          <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
            <EditRowStyle BackColor="#999999" />
                     <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
             </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Company: <asp:DropDownList ID="cboPolicyCompany" runat="server" OnSelectedIndexChanged="cboPolicyCompany_SelectedIndexChanged"  AutoPostBack="True"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboPolicyCompany" ErrorMessage="*" ForeColor="Red">*</asp:RequiredFieldValidator>
        </div>
         <div class="col-md-3">
            Location: <asp:DropDownList ID="cboPolicyCoLocation" runat="server"></asp:DropDownList>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboPolicyCoLocation" ErrorMessage="*" ForeColor="Red">*</asp:RequiredFieldValidator>
        </div>
         <div class="col-md-3">
            Cov Type: <asp:DropDownList ID="ddlCovType" runat="server"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
         <div class="col-md-12">
             <asp:Button ID="btnPolicyOK" runat="server" Text="OK" class="btn btn-info btn-xs" OnClick="btnPolicyOK_Click" /> 
             <asp:Button ID="btnUsePolicyOK" runat="server" class="btn btn-info btn-xs"  Text="Use This Policy and Continue" OnClick="btnUsePolicyOK_Click" /> 
             <asp:Button ID="btnNoPolicyOK" runat="server" class="btn btn-warning btn-xs"  Text="Continue Without Policy"   OnClick="btnNoPolicyOK_Click"   />
              <asp:Button ID="btnPolicyCancel" runat="server" class="btn btn-danger pull-right"  Text="Cancel"    OnClientClick="window.close(); return false;" CausesValidation="False" />
              
         </div>
     </div>
</div>
    <div runat="server" id="DivPossibleDuplicatesFound" visible="false">
        <div class="row">
    <div class="col-md-12">
<<<<<<< HEAD
         <h4><font color=red>Possilbe Duplicate Claims Found - Please Investigate</font></h4>
=======
<<<<<<< HEAD
         <h4><font color=red>Possilbe Duplicate Claims Found - Please Investigate</font></h4>
=======
         <h4>Possilbe Duplicate Claims Found - Please Investigate</h4>
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
    </div>
</div>
    <div class="row">
    <div class="col-md-12">
        <asp:GridView ID="grPossibleDuplicates" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="claimId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  AllowPaging="True" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                       <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"claimId").ToString() + "=window.open('viewclaim.aspx?tabindex=18&claim_id=" + DataBinder.Eval(Container.DataItem,"claimId").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"claimId").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" +  DataBinder.Eval(Container.DataItem,"claimId").ToString() + ".focus()" %> Runat="Server">
                            View</asp:Hyperlink>

                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="duplicateClaim" HeaderText="Claim" />
                        <asp:BoundField DataField="policyNo" HeaderText="Policy" />
                         <asp:BoundField DataField="dtLoss" HeaderText="Date of Loss"  />
                         <asp:BoundField DataField="matchDescrip" HeaderText="Match Description" />
              
                    </Columns>
                <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                     <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnCreateWithDuplicate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="OK This Policy and Continue" OnClick="btnCreateWithDuplicate_Click"/>
        <asp:Button ID="btnCancelDuplicates" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>

    </div>

</asp:Content>

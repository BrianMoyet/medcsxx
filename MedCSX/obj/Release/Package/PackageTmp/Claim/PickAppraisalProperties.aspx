﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="PickAppraisalProperties.aspx.cs" Inherits="MedCSX.Claim.PickAppraisalProperties" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvAppraisalproperties" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="vehicle_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                    <asp:TemplateField HeaderText="">  
                    
                    <ItemTemplate>  
                        <asp:CheckBox ID="chkRequest" runat="server" />  
                    </ItemTemplate>  
                </asp:TemplateField> 
                    <asp:BoundField DataField="vehicle_id" HeaderText=""/>
                    <asp:BoundField DataField="Vehicle_short_name" HeaderText="Property"/>
                    <asp:BoundField DataField="" HeaderText="Appraisal Requested"/>
                   
                

            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
   </div>
</div>
    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmSetAverageReserves.aspx.cs" Inherits="MedCSX.Admin.frmSetAverageReserves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
              <asp:GridView ID="grAverageReserves" runat="server"  OnRowEditing="grAverageReserves_RowEditing"  OnRowDataBound="grAverageReserves_RowDataBound"   AutoGenerateColumns="False" CellPadding="4" DataKeyNames="state_reserve_type_id"   ShowHeaderWhenEmpty="True"
                    ForeColor="#333333"  OnRowDeleting="grAverageReserves_RowDeleting"  OnRowUpdating="grAverageReserves_RowUpdating" ShowFooterWhenEmpty="true"  OnRowCancelingEdit="grAverageReserves_RowCancelingEdit" Width="100%" ShowFooter="True"  OnRowDeleted="grAverageReserves_RowDeleted" OnRowUpdated="grAverageReserves_RowUpdated"  >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="State">
                            <EditItemTemplate>
                                <%# Eval("state") %>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblstateitem" runat="server" Text='<%# Bind("state") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reserve Type">
                            <EditItemTemplate>
                                <%# Eval("reserve_type") %>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblreserveitem" runat="server" Text='<%# Bind("reserve_type") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Average Reserve">
                       
                                <EditItemTemplate>
                                <asp:TextBox ID="arEdit" runat="server"  Text='<%# Bind("average_reserve") %>' ></asp:TextBox> 
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="arEdit" ValidationExpression="^\d{1,9}(\.\d{1,9})?$" ForeColor="Red" ErrorMessage="Dollar amounts only"></asp:RegularExpressionValidator>
                </EditItemTemplate>
              
                           <ItemTemplate>
                               <asp:Label ID="ar" runat="server" Text='<%# Bind("average_reserve") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                        
                    </Columns>
                  
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                <EditRowStyle BackColor="LightGray" ForeColor="Black" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
        </div>
    </div>
</asp:Content>

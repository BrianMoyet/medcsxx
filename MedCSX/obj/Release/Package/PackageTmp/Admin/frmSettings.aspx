﻿<%@ Page Title="Settings" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmSettings.aspx.cs" Inherits="MedCSX.Admin.frmSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  
    <div class="row">
        <div class="col-md-12">
              <asp:Panel ID="mainForm" runat="server">
   <cc1:tabcontainer id="TabContainer1" runat="server" activetabindex="0" AutoPostBack="True"  OnActiveTabChanged="TabContainer1_ActiveTabChanged" >
      <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
         <HeaderTemplate>General</HeaderTemplate>
         <ContentTemplate>
            <div class="row">
                <div class="col-md-4">Max Supplemental Payments:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMaxSupplementalPayments" runat="server" Width="40px" TextMode="Number"></asp:TextBox></div>
            </div>
         <div class="row">
                <div class="col-md-4">Minutes Until Locked:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMinutesUntilLocked" runat="server" Width="40px"  TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Minutes Locked Until Shutdown:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMinutesLockedUntilShutdown" runat="server"  Width="40px" TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                 <div class="col-md-12">
                     <asp:Button ID="btnSaveGeneral" runat="server" Text="Save General" class="btn btn-info btn-xs"  OnClick="btnSaveGeneral_Click" /></div>
             </div> 
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2">
         <HeaderTemplate>Adjuster Assignment</HeaderTemplate>
         <ContentTemplate>
        
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel3" ID="TabPanel3">
         <HeaderTemplate>Password</HeaderTemplate>
         <ContentTemplate>
              <div class="row">
                <div class="col-md-4">Weeks Between Changes:</div>
                <div class="col-md-8"><asp:TextBox ID="txtWeeksBetweenChanges" runat="server" Width="40px"  TextMode="Number"></asp:TextBox></div>
            </div>
            <div class="row">
                <div class="col-md-4">Minimum Characters:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMinCharacters" runat="server" Width="40px"  TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Maximum Characters:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMaxCharacters" runat="server"  Width="40px" TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Minimum Uppercase Characters:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMinUppercase" runat="server"  Width="40px" TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Minimum Special Characters:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMinSpecial" runat="server"  Width="40px" TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Minimum Numberic Characters:</div>
                <div class="col-md-8"><asp:TextBox ID="txtMinNumeric" runat="server" Width="40px"  TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Changes Between Reuse:</div>
                <div class="col-md-8"><asp:TextBox ID="txtChangesBeforeReuse" runat="server" Width="40px"  TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Days To Alert Change:</div>
                <div class="col-md-8"><asp:TextBox ID="txtDaysToAlertChange" runat="server" Width="40px"  TextMode="Number"></asp:TextBox></div>
            </div>
             <div class="row">
                 <div class="col-md-12"><asp:Button ID="btnSavePassword" class="btn btn-info btn-xs"  OnClick="btnSavePassword_Click" runat="server" Text="Save Password Section" /></div>
             </div>
            
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel4" ID="TabPanel4">
         <HeaderTemplate>Appraisals</HeaderTemplate>
         <ContentTemplate>
               <div class="row">
                <div class="col-md-4">Appraisal Request File:</div>
                <div class="col-md-8"><asp:Label ID="lblARFileName" runat="server" Text="" ForeColor="Blue"></asp:Label>
                    <%--<asp:TextBox ID="txtARFileName" runat="server"></asp:TextBox> <asp:FileUpload ID="txtAppraisalRequestFileName" runat="server" />--%></div>
            </div>
              <div class="row">
                <div class="col-md-4">Appraisal Email Address:</div>
                <div class="col-md-8"><asp:TextBox ID="txtAppraisalEmail" runat="server" TextMode="Email"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">Appraisal Subject Line:</div>
                <div class="col-md-8"><asp:TextBox ID="txtAppraisalSubject" runat="server"></asp:TextBox></div>
            </div>
             <div class="row">
                <div class="col-md-4">SMTP Server:</div>
                <div class="col-md-8"><asp:TextBox ID="txtSMTPServer" runat="server"></asp:TextBox></div>
            </div>
             <div class="row">
                 <div class="col-md-12">
                    <br /><asp:Button ID="btnSaveAppraisal" OnClick="btnSaveAppraisal_Click" runat="server" class="btn btn-info btn-xs"  Text="Save Appraisal Section" />
                 </div>
             </div>
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel5" ID="TabPanel5">
         <HeaderTemplate>Forced Diary</HeaderTemplate>
         <ContentTemplate>
              <div class="row">
                  <div class="col-md-4">
                        <div style="width: 100%; height: 650px; overflow: scroll">
                  <asp:GridView ID="grUserTypes" AutoGenerateSelectButton="true" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="User_Type_Id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False" OnSelectedIndexChanged="grUserTypes_SelectedIndexChanged"   width="100%" ShowFooter="true"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="Description" HeaderText="User Type"/>
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                 <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
                   </div>
                  </div>
                  <div class="col-md-5">
                       <div style="width: 100%; height: 650px; overflow: scroll">
                  <asp:GridView ID="grUserTypeDiary" AutoGenerateSelectButton="true" DataKeyNames="user_type_diary_id" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False"  OnSelectedIndexChanged="grUserTypeDiary_SelectedIndexChanged"  width="100%" ShowFooter="true"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="from_reserve_day" HeaderText="From Reserve Day"/>
                           <asp:BoundField DataField="to_reserve_day" HeaderText="To Reserve Day"/>
                           <asp:BoundField DataField="max_diary_days" HeaderText="Max Diary Days"/>
                           <asp:TemplateField HeaderText="" >
                     <ItemTemplate>
                        <asp:Button  ID="btnUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update" CausesValidation="false"  OnClick="btnUpdate_Click" />
                       
                     </ItemTemplate>
                     
                  </asp:TemplateField>
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                   <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
                   </div>
                  </div>
                  <div class="col-md-3">
                       <div id="pnlSubroMaxDiaryDays" runat="server">
                    Subro Max Diary Days<br />
                      <asp:TextBox ID="txtSubroMaxDiaryDays" TextMode="Number"  Width="40px" runat="server"></asp:TextBox> days
                        <hr />
                           </div>
                      <div id="pnlSalvageMaxDiaryDays" runat="server">
                      Salvage Max Diary Days<br />
                      <asp:TextBox ID="txtSalvageMaxDiaryDays" TextMode="Number"  Width="40px" runat="server"></asp:TextBox> days
                        <hr />
                          </div>
                      <div id="pnlGracePeriod" runat="server">
                        Grace period before locking:<br />
                      <asp:TextBox ID="txtDiaryGracePeriod" TextMode="Number"  Width="40px" runat="server"></asp:TextBox> days
                        <hr />
                          </div>
                   
                   <asp:Button ID="Button2" OnClick="btnSaveForcedDiary_Click" runat="server" class="btn btn-info btn-xs"  Text="Save Forced Diary" />
                 <br /><br /><br />
                      <asp:Button ID="btnEnableForcedDiary"  class="btn btn-info btn-xs" CausesValidation="false" runat="server" Text="Enable Forced Diary" OnClick="btnEnableForcedDiary_Click" /><br />
                      <br /><asp:Button ID="btnDisableForcedEntry"  class="btn btn-info btn-xs" CausesValidation="false" runat="server" Text="Disable Forced Diary" OnClick="btnDisableForcedEntry_Click" />
                  </div>
              </div>
              <div class="row">
                 <div class="col-md-12">
                    <br /><asp:Button ID="btnSaveForcedDiary" OnClick="btnSaveForcedDiary_Click" runat="server" class="btn btn-info btn-xs"  Text="Save Forced Diary" />
                 </div>
             </div>
         </ContentTemplate>
      </cc1:TabPanel>
 
      <cc1:TabPanel runat="server" HeaderText="TabPanel6" ID="TabPanel6">
         <HeaderTemplate>Draft Printing</HeaderTemplate>
         <ContentTemplate>
            <div class="row">
                <div class="col-md-12"><b>Draft Printing Settings</b></div>  
            </div>
              <div class="row">
                   <div class="col-md-12">Redirect All Draft Printing To:<asp:DropDownList ID="ddlPrintingLocation" runat="server"></asp:DropDownList></div>  
             </div>
            <div class="row">
                <div class="col-md-12">Draft Printing Permissions: <asp:DropDownList ID="cboDraftPrintingPermission" runat="server"></asp:DropDownList> </div>  
            </div>
             
             <div class="row">
                   <div class="col-md-12">Allow Draft Printing From: <asp:TextBox ID="txtDraftPrintingFrom" Width="50px" runat="server"></asp:TextBox> Until: <asp:TextBox ID="txtDraftPrintingTo" Width="50px" runat="server"></asp:TextBox></div>  
             </div>
             <div class="row">
                   <div class="col-md-12">
                       <asp:CheckBox ID="chkSunday" runat="server" Text="Sunday" />
                       <asp:CheckBox ID="chkMonday" runat="server" Text="Monday" />
                       <asp:CheckBox ID="chkTuesday" runat="server" Text="Tuesday" />
                       <asp:CheckBox ID="chkWednesday" runat="server" Text="Wednesday" />
                       <asp:CheckBox ID="chkThursday" runat="server" Text="Thursday" />
                       <asp:CheckBox ID="chkFriday" runat="server" Text="Friday" />
                       <asp:CheckBox ID="chkSaturday" runat="server" Text="Saturday" />
                   </div>  
             </div>
              <div class="row">
                   <div class="col-md-12">Draft Printing is Currently: <asp:Label ID="lblCurrentDraftPrintingStatus" ForeColor="Blue" runat="server" Text=""></asp:Label></div>  
             </div>
             <div class="row">
                   <div class="col-md-12">Drafts in Queue: <asp:Label ID="lblDraftsInQueue" ForeColor="Blue" runat="server" Text=""></asp:Label></div>  
             </div>
             <div class="row">
                 <div class="col-md-12">
                    <br /><asp:Button ID="btnSaveDraftPrinting" runat="server" class="btn btn-info btn-xs"  Text="Save Draft Printing" OnClick="btnSaveDraftPrinting_Click"/>
                 </div>
             </div>
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel7" ID="TabPanel7">
         <HeaderTemplate>Reserving</HeaderTemplate>
         <ContentTemplate>
             <div class="row">
                <div class="col-md-4">Require case reservers after</div>
                <div class="col-md-8"><asp:TextBox ID="txtReserveRequirementDays" TextMode="Number" Width="40px"  runat="server"></asp:TextBox> days</div>
            </div>
             <div class="row">
                <div class="col-md-4">Warn about case reservers after</div>
                <div class="col-md-8"><asp:TextBox ID="txtWarnAboutReserves" TextMode="Number" Width="40px"  runat="server"></asp:TextBox> days</div>
            </div>
             <div class="row">
                <div class="col-md-4">Require case reservers after</div>
                <div class="col-md-8"><asp:CheckBox ID="chkReservesRequiredOnLockedFiles"   Text="Reserves Required on Locked Files" runat="server" /></div>
            </div>
             <div class="row">
                 <div class="col-md-12">
                    <br /><asp:Button ID="btnSaveReserving" runat="server" class="btn btn-info btn-xs"  Text="Save Reserving" OnClick="btnSaveReserving_Click"/>
                 </div>
             </div>
           
            
         </ContentTemplate>

      </cc1:TabPanel>
         <cc1:TabPanel runat="server" HeaderText="TabPanel8" ID="TabPanel8">
         <HeaderTemplate>Commercial</HeaderTemplate>
         <ContentTemplate>
         <div class="row">
                <div class="col-md-4">Alert user if over</div>
                <div class="col-md-8"><asp:TextBox ID="txtMaxAggregateBeforeAlert"  Width="40px" TextMode="Number" runat="server"></asp:TextBox> % of policy aggregate has been used.</div>
            </div>
             <div class="row">
                <div class="col-md-4">Alert user if over</div>
                <div class="col-md-8"><asp:TextBox ID="TextBox2" TextMode="Number"  Width="40px" runat="server"></asp:TextBox> % of policy SIR has been used.</div>
            </div>
            
             <div class="row">
                 <div class="col-md-12">
                    <br /><asp:Button ID="Button1" runat="server" class="btn btn-info btn-xs"  Text="Save Reserving" OnClick="btnSaveReserving_Click"/>
                 </div>
             </div>

           
            
         </ContentTemplate>

      </cc1:TabPanel>
    
   </cc1:tabcontainer>
</asp:Panel>
        
        </div>
    </div>
         
         <div class="col-md-12">
               
            
               <asp:Button ID="btnInjuredCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
</asp:Content>

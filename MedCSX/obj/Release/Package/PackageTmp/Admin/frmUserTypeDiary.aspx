﻿<%@ Page Title="Reserve Day Range" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmUserTypeDiary.aspx.cs" Inherits="MedCSX.Admin.frmUserTypeDiary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-4">
            From Reserve Day:
        </div>
        <div class="col-md-6">
            <asp:TextBox ID="txtFromDay" TextMode="Number" runat="server"></asp:TextBox>
        </div>
        </div>
        <div class="row">
         <div class="col-md-4">
            To Reserve Day:
        </div>
        <div class="col-md-6">
            <asp:TextBox ID="txtToDay" TextMode="Number" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
         <div class="col-md-4">
            Max Diary Days:
        </div>
        <div class="col-md-6">
            <asp:TextBox ID="txtMaxDiaryDays" TextMode="Number" runat="server"></asp:TextBox>
        </div>
    </div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs" OnClick="btnOK_Click"  Text="OK" />
               <asp:Button ID="btnPickCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"   OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmSceneAccessAppraiser.aspx.cs" Inherits="MedCSX.Admin.frmSceneAccessAppraiser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="row">
                <div class="col-md-12">
                    <div id="tsbPickVendor" runat="server">
                    Vendor Tax Id: <asp:TextBox ID="txtVendorTaxID" CausesValidation="false"  OnTextChanged="txtVendorTaxID_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox><br />
              Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> <br />
              <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false"  OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
           </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"><b>Appraiser Details</b></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <asp:Label ID="lblVendor" runat="server" ForeColor="Blue" Height="112px" Width="182px"></asp:Label>
            <p>
                <asp:CheckBox ID="chkActive" runat="server" Text="Active" /></p>
        </div>
        <div class="col-md-8">
            Can Appraise in the following states:<br /><asp:Label ID="lblAddFirst" runat="server" ForeColor="Red" Text="Create a vendor before adding states." Visible="false"></asp:Label>
            <br />
             <asp:GridView ID="lstStates" AutoGenerateSelectButton="false" DataKeyNames="Id" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False"  ShowFooter="false"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="Description" HeaderText="State"/>
                       
                         
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
            <br />
             <asp:LinkButton ID="tsbAddState" runat="server" class="btn btn-info btn-xs" CausesValidation="false" OnClick="tsbAddState_Click" >Add State</asp:LinkButton> 
            <asp:Button ID="tsbDeleteState" runat="server" class="btn btn-danger btn-xs"  Text="Delete States"    OnClientClick="return confirm('Are you sure you want to delete all States?');" OnClick="tsbDeleteState_Click"  />
       <br /><br />
            </div>
    </div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"/> 
               <asp:Button ID="btnInjuredCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

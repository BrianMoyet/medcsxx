﻿<%@ Page Title="Configure Documents" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmConfigureDocuments.aspx.cs" Inherits="MedCSX.Admin.frmConfigureDocuments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="tsbAdd" OnClick="tsbAdd_Click" runat="server">Add Document</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton OnClick="tsbEdit_Click" ID="tsbEdit" runat="server">Edit Document</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton OnClick="tsbDelete_Click" Visible="false" Enabled="false" ID="tsbDelete"  OnClientClick="return confirm('Are you sure you want to delete this record?');" runat="server">Delete Document Type</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
              <div style="width: 100%; height: 600px; overflow: scroll">
              <asp:GridView ID="gvConfigureDocuments" DataKeyNames="Id"  AutoGenerateColumns="false" Width="100%"  AutoGenerateSelectButton="true" runat="server">
                 <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                       <asp:BoundField DataField="Description" HeaderText="Description"  />
                    <asp:BoundField DataField="File_Name" HeaderText="Word Template"  />
                    <asp:BoundField DataField="Event_Type" HeaderText="Event Type"  />
                    <asp:BoundField DataField="Stored_Procedure_Name" HeaderText="Stored Procedure"  />
                    <asp:BoundField DataField="Claim_Type" HeaderText="Claim Type"  />
                     <asp:BoundField DataField="Active" HeaderText="Active"  />
                    
                </Columns>
                 <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>
    </div>
    </div>
</asp:Content>

﻿<%@ Page Title="Edit users" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmUsers.aspx.cs" Inherits="MedCSX.Admin.frmUsers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
         input[type=checkbox] {
      float: left;
      }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div style="width: 100%; height: 300px; overflow: scroll">
              <asp:GridView ID="grUsers" runat="server" AutoGenerateSelectButton="true" OnSelectedIndexChanged="grUsers_SelectedIndexChanged"  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="user_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
               ForeColor="#333333" AllowSorting="True" AllowPaging="False" OnSorting="grUsers_Sorting"   width="100%" ShowFooter="True" PageSize="500">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
             <asp:BoundField DataField="user_id" HeaderText="User Id" SortExpression="user_id" />
                  <asp:BoundField DataField="first_name" HeaderText="First Name" SortExpression="first_name" />
                  <asp:BoundField DataField="last_name" HeaderText="Last Name" SortExpression="last_name" />
                  <asp:BoundField DataField="user_type" HeaderText="User Type" SortExpression="user_type" />
                  <asp:BoundField DataField="user_status" HeaderText="Status" SortExpression="user_status" />
                    <asp:TemplateField HeaderText="" >
                       <HeaderTemplate>
                                <asp:Button  ID="btnUsersAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD" OnClick="btnUsersAdd_Click" data-toggle="modal" data-target="#theModal"  CausesValidation="false"  />
                       </HeaderTemplate>
                    
                        <FooterTemplate>
                        <asp:Button  ID="btnUsersAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD" OnClick="btnUsersAdd_Click" data-toggle="modal" data-target="#theModal"    CausesValidation="false"  />
                     </FooterTemplate>
                  </asp:TemplateField>
               </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
              
               <EditRowStyle BackColor="#999999" />
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center"  style="background-color:salmon">
            <asp:Label runat="server" ForeColor="White" Font-Size="Large" Font-Bold="true" ID="lblUserName"></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
              <asp:Panel ID="mainForm" runat="server">
   <cc1:tabcontainer id="TabContainer1" runat="server" activetabindex="0" AutoPostBack="True" OnActiveTabChanged="TabContainer1_ActiveTabChanged" >
      <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
         <HeaderTemplate>General</HeaderTemplate>
         <ContentTemplate>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">User Id:</div>
                        <div class="col-md-9"><asp:Label ID="txtUserId" runat="server"></asp:Label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">First Name:</div>
                        <div class="col-md-9"><asp:TextBox ID="txtUserFName" runat="server"></asp:TextBox></div>
                    </div>
                      <div class="row">
                        <div class="col-md-3">User Status:</div>
                        <div class="col-md-9"><asp:DropDownList ID="cboUserStatus" runat="server"></asp:DropDownList></div>
                    </div>
                     <div class="row">
                        <div class="col-md-3">User Type:</div>
                        <div class="col-md-9"><asp:DropDownList ID="cboUserType" runat="server"></asp:DropDownList></div>
                    </div>
                     <div class="row">
                        <div class="col-md-3">Supervisor:</div>
                        <div class="col-md-9"><asp:DropDownList ID="cboUserSupervisor" runat="server"></asp:DropDownList></div>
                    </div>
                     <div class="row">
                        <div class="col-md-3">Claim Exp. Started:</div>
                        <div class="col-md-9"><asp:TextBox ID="txtUserExperience" runat="server"></asp:TextBox></div>
                    </div>
                     <div class="row">
                        <div class="col-md-3">Adj Classification:</div>
                        <div class="col-md-9"><asp:DropDownList ID="cboUserClassification" runat="server"></asp:DropDownList></div>
                    </div>
                      <div class="row">
                        <div class="col-md-3">Phone Extension:</div>
                        <div class="col-md-9"><asp:TextBox ID="txtUserPhone" runat="server"></asp:TextBox></div>
                    </div>
                     <div class="row">
                        <div class="col-md-3">Default Draft Printer:</div>
                        <div class="col-md-9"><asp:DropDownList ID="cboDefaultPrinter" runat="server"></asp:DropDownList></div>
                    </div>
                     <div class="row">
                        <div class="col-md-12"><asp:CheckBox ID="chkUserAssignedClaims" Text="Can Be Assigned Claims" runat="server" /></div>
                        
                    </div>
                </div>
                  <div class="col-md-6">  
                       <div class="row">
                        <div class="col-md-3">Logon Id:</div>
                        <div class="col-md-9"><asp:TextBox ID="txtUserLogon" runat="server"></asp:TextBox></div>
                    </div>
                       <div class="row">
                        <div class="col-md-3">Last Name:</div>
                        <div class="col-md-9"><asp:TextBox ID="txtUserLName" runat="server"></asp:TextBox></div>
                    </div>
                       <div class="row">
                        <div class="col-md-3">Languages:</div>
                        <div class="col-md-9">
                             <div style="height: 80px; overflow: scroll">
                            <asp:GridView ID="grUserLanguage" DataKeyNames="id" runat="server" AutoGenerateColumns="False" Width="75px">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                      <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>      
                                   <asp:BoundField DataField="description" HeaderText="Language" SortExpression="description" />
                                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                                  <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                                <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                            </asp:GridView>
                                 </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">Direct Fax:</div>
                        <div class="col-md-9"><asp:TextBox ID="txtUserFax" runat="server"></asp:TextBox></div>
                    </div>
                      <div class="row">
                        <div class="col-md-3">Email:</div>
                        <div class="col-md-9">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="**" EnableViewState="false" ControlToValidate="txtUserEmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">**</asp:RegularExpressionValidator><asp:TextBox ID="txtUserEmail"  runat="server"></asp:TextBox></div>
                    </div>
                         <div class="row">
                        <div class="col-md-12">
                            <asp:LinkButton ID="PswrdReset" CausesValidation="False" OnClick="PswrdReset_Click" runat="server">Reset Password</asp:LinkButton>
                            <asp:LinkButton ID="btnSaveUser" class="btn btn-info btn-xs" OnClick="btnSaveUser_Click" runat="server">Save</asp:LinkButton>
                        </div>
                    </div>
                      <div class="row">
                          <div class="col-md-12">
                              <asp:Label ID="lblpwchanged" ForeColor="Red" Visible="false" runat="server" Text=""></asp:Label>
                          </div>
                      </div>
                </div>
            </div>
         
              
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2">
         <HeaderTemplate>Alerts and Diaries</HeaderTemplate>
         <ContentTemplate>
          <div class="row">
              <div class="col-md-4">
                  <asp:Calendar ID="userCalendar" OnSelectionChanged="userCalendar_SelectionChanged" runat="server" ></asp:Calendar>
              </div>
              <div class="col-md-8">
                  <div class="row">
                      <div class="col-md-12">
                          <asp:Label ID="lblUserActiveAlerts" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<asp:Label ID="userActiveAlerts" runat="server" Text=""></asp:Label>
                          &nbsp;&nbsp;<asp:LinkButton ID="btnUserClearAlerts" CausesValidation="false" OnClick="btnUserClearAlerts_Click" runat="server">Clear</asp:LinkButton>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <asp:Label ID="lblUserActiveDiaries" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<asp:Label ID="userActiveDiaries" runat="server" Text=""></asp:Label>
                          &nbsp;&nbsp;<asp:LinkButton ID="btnUserClearDiaries" CausesValidation="false" OnClick="btnUserClearDiaries_Click" runat="server">Clear</asp:LinkButton>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                          Alert Preference:
                      </div>
                       <div class="col-md-9">
                           <asp:DropDownList ID="cboAlertPreference" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="cboAlertPreference_SelectedIndexChanged" runat="server"></asp:DropDownList>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                          Outgoing Alerts:
                      </div>
                       <div class="col-md-9">
                           <asp:DropDownList ID="cboOutgoingAlert" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="cboOutgoingAlert_SelectedIndexChanged" runat="server"></asp:DropDownList>
                      </div>
                  </div>
                   <div class="row">
                      <div class="col-md-12">
                          <asp:CheckBox ID="chkForceDiaryByUserType" CausesValidation="false" AutoPostBack="true" OnCheckedChanged="chkForceDiaryByUserType_CheckedChanged" Text="Use Force Diary from User Type" runat="server" />
                          
                      </div>
                  </div>
                   <div class="row">
                      <div class="col-md-12">
                          <asp:CheckBox ID="chkForceDiary" CausesValidation="false" AutoPostBack="true" OnCheckedChanged="chkForceDiary_CheckedChanged" Text="Force Diary for this User" runat="server" />
                      </div>
                  </div>
                  <div runat="server" id="pnlDiaryGracePeriod">
                   <div class="row">
                      <div class="col-md-12">
                          <asp:CheckBox ID="chkGracePeriodByUserType" CausesValidation="false" AutoPostBack="true" OnCheckedChanged="chkGracePeriodByUserType_CheckedChanged" Text="Use Diary Grace Period from User Type" runat="server" />
                      </div>
                  </div>
                   <div class="row">
                      <div class="col-md-12">
                          Diary Grace period: <asp:TextBox ID="txtDiaryGracePeriod" Width="25px" runat="server" CausesValidation="false" AutoPostBack="true" OnTextChanged="txtDiaryGracePeriod_TextChanged"></asp:TextBox> days
                      </div>
                  </div>
                      </div>
              </div>
          </div>
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel3" ID="TabPanel3">
         <HeaderTemplate>Signature</HeaderTemplate>
         <ContentTemplate>
            <div class="row">
                 <div class="col-md-12">
                        Signature File: <asp:TextBox ID="txtSignatureFile" OnTextChanged="txtSignatureFile_TextChanged" CausesValidation="false" AutoPostBack="true" runat="server"></asp:TextBox>
                     <asp:FileUpload ID="FileUpload1"  runat="server" /> <asp:Button ID="btnChangeSignature" CausesValidation="false" OnClick="btnChangeSignature_Click" runat="server" Text="Complete Signature Update" />
                 </div>
             </div>
               <div class="row">
                 <div class="col-md-12">
                     <asp:Image ID="txtSignatureFileName"  Width="500px" runat="server" /> 
                 </div>
             </div>
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel4" ID="TabPanel4">
         <HeaderTemplate>Groups</HeaderTemplate>
         <ContentTemplate>
              <div class="row">
                 <div class="col-md-12">

                     <asp:LinkButton ID="btnSelectAllGroups" runat="server" CausesValidation="false" OnClick="btnSelectAllGroups_Click">Check  All</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSelectNoGroups" runat="server" CausesValidation="false" OnClick="btnSelectNoGroups_Click">Uncheck All</asp:LinkButton>
                     <br />
                    
                        <div style="width: 100%; height: 300px; overflow: scroll">
                     <asp:GridView ID="grUserGroup" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333" width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>      
                  
                        <asp:BoundField DataField="description" HeaderText="User Groups"  />
                       
                    
                    </Columns>
                     <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
                    
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                      
                </asp:GridView>

                             
                            </div>
                     <asp:LinkButton ID="btnSaveGroups" OnClick="btnSaveGroups_Click" CausesValidation="false" class="btn btn-info btn-xs" runat="server">Save</asp:LinkButton>
                 </div>
             </div>
            
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel5" ID="TabPanel5">
         <HeaderTemplate>Authority</HeaderTemplate>
         <ContentTemplate>
                <div style="width: 100%; height: 300px; overflow: scroll">
               <asp:GridView ID="grAuthority"  runat="server" OnSelectedIndexChanged="grAuthority_SelectedIndexChanged" AutoGenerateSelectButton="true" AutoGenerateColumns="False" CellPadding="4"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333" width="100%" DataKeyNames="id">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        
                  
                        <asp:BoundField DataField="Coverage Type" HeaderText="Coverage Type"  />
                         <asp:BoundField DataField="Authority Amount" HeaderText="Authority Amount" DataFormatString="{0:c}"  />
                         <asp:BoundField DataField="Technical Supervisor" HeaderText="Supervisor"  />
                          <asp:BoundField DataField="supervisor_id" />
                        
                       
                    
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                     <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
           </div>
             <div class="row">
                 <div class="col-md-1">Amount:</div>
                  <div class="col-md-4"><asp:TextBox ID="txtFinancialAuthority" runat="server"></asp:TextBox></div>
                  <div class="col-md-2">Supervisor:</div>
                  <div class="col-md-4"><asp:DropDownList ID="cboUserReserveSupervisor" runat="server"></asp:DropDownList></div>
                 <div class="col-md-1">  <asp:LinkButton ID="btnSaveAuthority" CausesValidation="false" OnClick="btnSaveAuthority_Click" class="btn btn-info btn-xs" runat="server">Save</asp:LinkButton></div>
             </div>
         </ContentTemplate>
      </cc1:TabPanel>
   <%--   <cc1:TabPanel runat="server" HeaderText="TabPanel6" ID="TabPanel6">
         <HeaderTemplate>Attorney</HeaderTemplate>
         <ContentTemplate>
           
         </ContentTemplate>
      </cc1:TabPanel>--%>
      <cc1:TabPanel runat="server" HeaderText="TabPanel7" ID="TabPanel7">
         <HeaderTemplate>Company Authority</HeaderTemplate>
         <ContentTemplate>
          <div class="row">
                 <div class="col-md-12">

                     <asp:LinkButton ID="btnSelectAllCo" runat="server" CausesValidation="false" OnClick="btnSelectAllCo_Click" >Check  All</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSelectNoCo" runat="server" CausesValidation="false"  OnClick="btnSelectNoCo_Click">Uncheck All</asp:LinkButton>
                     <br />
                    
                        <div style="width: 100%; height: 300px; overflow: scroll">
                     <asp:GridView ID="grUserCompany" DataKeyNames="company_id" runat="server" AutoGenerateColumns="False" CellPadding="4"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333" width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>      
                  
                        <asp:BoundField DataField="description" HeaderText="User Groups"  />
                      <%--  <asp:BoundField DataField="isChecked" HeaderText="checked"  />--%>
                       
                    
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                     <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
                    
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                      
                </asp:GridView>

                             
                            </div>
                     <asp:LinkButton ID="btnSaveCompany" OnClick="btnSaveCompany_Click" CausesValidation="false" class="btn btn-info btn-xs" runat="server">Save</asp:LinkButton>
                 </div>
             </div>
         </ContentTemplate>
      </cc1:TabPanel>
      <cc1:TabPanel runat="server" HeaderText="TabPanel8" ID="TabPanel8">
         <HeaderTemplate>Allowed Authority</HeaderTemplate>
         <ContentTemplate>
             <div class="row">
                 <div class="col-md-12">
                     <b>Designate what user is authorized to do:</b>
                 </div>
             </div>
            <div class="row">
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditUser" OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Edit Users" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkAddReserve"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Add Reserve" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkModifyClaim"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Modify Claim Data" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkManLock"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Manually Lock Claim" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkOpenDoc"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Open Document" /></div>
            </div>
             <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkCloseClaim"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Close Claim" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkChgLossRes"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Change Loss Reserve" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkVoidRes"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Void Reserve" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkSetAvgRes"  OnCheckedChanged="chkEditUser_CheckedChanged" AutoPostBack="true" CausesValidation="false"  Text="Set Average Reserves" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkPrintQueue"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Print Draft Queue" /></div>
             </div>
             <div class="row">
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditTypeTable"  OnCheckedChanged="chkEditUser_CheckedChanged" AutoPostBack="true" CausesValidation="false"  Text="Edit Type Tables" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkChgExpRes"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Change Expense Reserve" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkReissDraft"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Reissue Draft" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkSetIcon"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Set Type Table Icons" /></div>
                <div class="col-md-2"><asp:CheckBox runat="server" ID="chkViewAppr"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="View Appraiser Data" /></div>
                 
             </div>
             <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditVendor"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Edit Vendors" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkReOpenRes"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Reopen Reserve" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkReissSubro"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Reissue Subrogation" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkSQLLog"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="SQL Logging" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkWireTrans"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Wire Transfer" /></div>
             </div>
             <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditUserGroup"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Edit User Groups" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkCloseRes"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Close Reserve" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkReissSalv"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Reissue Salvage" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkBulkAssign"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Bulk Assign Claims" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkPendDraft"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Pending Drafts" /></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkConfigNotes"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Configure Alerts/Diary/File Notes" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkClsPendSalv"  OnCheckedChanged="chkEditUser_CheckedChanged" AutoPostBack="true" CausesValidation="false" Text="Close Pending Salvage" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkReopenClaim"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Reopen Claim" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkUnVoidDraft"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Unvoid Draft" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkResendEmail"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Resend Appraisal Emails" /></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditSettings"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Edit Global Settings" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkClsPendSub"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Close Pending Subrogation" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkChgVendTax"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Change Vendor Tax-Id Req" /></div>
                 <div class="col-md-2"> <asp:CheckBox runat="server" ID="chkAuditClaim"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Audit Claims" /></div>
                 <div class="col-md-2"><asp:CheckBox Font-Bold="true" runat="server" ID="chkSuper" OnCheckedChanged="chkSuper_CheckedChanged" AutoPostBack="true" CausesValidation="false"  Text="Super User" /></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkOpenLog"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Open Error Log" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkIssueDraft"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Issue Draft" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkSendMsg"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Send Message" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkLogInAs"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Log In As" /></div>
                 <div class="col-md-2"></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditDictionary"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Edit Data Dictionary" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkRecSubro"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Receive Subrogation" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkStaffMetrix"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Staffing Metrics" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkResendAppr"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Resend Appraisal Request" /></div>
                 <div class="col-md-2"></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkVendorAssoc"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Vendor Type/Sub-Type Associations" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkRecSal"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Receive Salvage" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkRecoverRpt"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Recovery Amounts Report" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkCompleteAppr"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Mark Appraisal As Complete" /></div>
                 <div class="col-md-2"></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkLogUser"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Log In Users" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkVoidDrft"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Void Draft" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEditTrans"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Edit Transactions" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkCancelAppr"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false"  Text="Cancel Appraisal" /></div>
                 <div class="col-md-2"></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkKillUser"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Boot Off Users" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkVdSubro"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Void Subrogation" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkPrintFoxPro"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Print Foxpro Drafts" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkChgDocStatus"  OnCheckedChanged="chkEditUser_CheckedChanged" AutoPostBack="true" CausesValidation="false"  Text="Change Document Status" /></div>
                 <div class="col-md-2"></div>
             </div>
              <div class="row">
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkEventView"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Open Event Viewer" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkVdSal"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Void Salvage" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkAcctRpt"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Accounting Reports" /></div>
                 <div class="col-md-2"><asp:CheckBox runat="server" ID="chkNewDoc"  OnCheckedChanged="chkEditUser_CheckedChanged"  AutoPostBack="true" CausesValidation="false" Text="Create New Document" /></div>
                 <div class="col-md-2"></div>
             </div>

             <div class="row">
                 <div class="col-md-12">
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    


                 </div>
             </div>
            
         </ContentTemplate>

      </cc1:TabPanel>
    
   </cc1:tabcontainer>
</asp:Panel>
        
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="Vendor Type/Sub-Type Association" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmVendorTypeSubTypeAssociations.aspx.cs" Inherits="MedCSX.Admin.frmVendorTypeSubTypeAssociations" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Vendor Types
        </div>
        <div class="col-md-5">
            Vendor Sub-Types
        </div>
        <div class="col-md-4">

        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
              <div style="width: 200px; height: 500px; overflow: scroll">
                  <asp:GridView ID="lstVendorTypes" AutoGenerateSelectButton="true" DataKeyNames="Vendor_Type_Id" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False"  OnSelectedIndexChanged="lstVendorTypes_SelectedIndexChanged"  ShowFooter="true"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="Description" HeaderText="Vendor Type"/>
                         
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
               <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
                   </div>
        </div>
        <div class="col-md-5">
              <div style="width: 300px;  height: 500px; overflow: scroll">
                  <asp:GridView ID="lstVendorSubTypes" AutoGenerateSelectButton="true" DataKeyNames="Vendor_Sub_Type_Id" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False"  OnSelectedIndexChanged="lstVendorSubTypes_SelectedIndexChanged"   ShowFooter="true"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="Description" HeaderText="Vendor Sub Type"/>
                     </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                    
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
                   </div>
        </div>
        <div class="col-md-4">
            <asp:LinkButton ID="llAssociate" class="btn btn-info btn-xs" OnClick="llAssociate_Click"  runat="server">Associate with a new Category</asp:LinkButton>
            <p>To add vendor types and sub-types go to "Edit Type Tables"</p>
        </div>
    </div>
</asp:Content>

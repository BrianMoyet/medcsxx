USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_get_draft_Print_creator]    Script Date: 2/5/2021 1:32:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_get_draft_Print_creator] 
	@Draft_Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        Created_By
FROM            Draft
WHERE        (Draft_Id = @Draft_Id)
END
GO


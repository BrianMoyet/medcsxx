USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Draft_No_Ordered]    Script Date: 2/10/2021 11:47:46 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		James Willoughby
-- Create date: 10/04/2017
-- Description:	Searches for specified draft number and orders by scalar
-- =============================================
ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Draft_No_Ordered] 
@draft_no varchar(25),
@orderBy varchar(255),
@subClaimTypeId int = NULL 
AS
-- Return Claims whose Display Claim Id is like @claim_no

BEGIN
select claim_id, display_claim_id, policy_no, date_of_loss, 
case dbo.get_user_display_name(adjuster_id) when '' then 'Unassigned' else dbo.get_user_display_name(adjuster_id)  end 
as adjuster, 
last_activity_date, cs.description,
dbo.get_person_name(c.insured_person_id) as insured, loss_description, dbo.claim_locked(claim_id) as is_locked,
 c.sub_claim_type_id, sct.[description] as 'Sub_Claim_Type', c.Company_Location_Id,
case @orderBy when 'Company_Location_Id' then
(select co.[Description] + ', ' + col.Abbreviation
from Company_Location col
inner join Company co on co.Company_Id=col.Company_Id
where col.Company_Location_Id=c.Company_Location_Id)
--insert additional parameters here 
else 'Unfiltered'
end as Ordered, c.Date_Reported

from claim c, claim_status cs, draft d, sub_claim_type sct

where dbo.get_claim_id_for_draft_id(d.draft_id) = c.claim_id
and c.claim_id <> 0
and c.claim_status_id = cs.claim_status_id
and rtrim(ltrim(str(draft_no))) like @draft_no + '%'
and c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
and c.sub_claim_type_id = sct.sub_claim_type_id

order by Ordered, c.Last_Activity_Date desc
END

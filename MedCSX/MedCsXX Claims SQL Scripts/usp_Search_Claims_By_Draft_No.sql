USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Draft_No]    Script Date: 2/10/2021 11:46:55 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Draft_No] 
@draft_no varchar(25),
@subClaimTypeId int = NULL 
AS
-- Return Claims whose Display Claim Id is like @claim_no

-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.

select claim_id, display_claim_id, policy_no, date_of_loss, 
case dbo.get_user_display_name(adjuster_id) when '' then 'Unassigned' else dbo.get_user_display_name(adjuster_id)  end 
as adjuster, 
last_activity_date, cs.description,
dbo.get_person_name(c.insured_person_id) as insured, loss_description, dbo.claim_locked(claim_id) as is_locked,
 c.sub_claim_type_id, sct.[description] as 'Sub_Claim_Type', c.Date_Reported

from claim c, claim_status cs, draft d, sub_claim_type sct

where dbo.get_claim_id_for_draft_id(d.draft_id) = c.claim_id
and c.claim_id <> 0
and c.claim_status_id = cs.claim_status_id
and rtrim(ltrim(str(draft_no))) like @draft_no + '%'
and c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
and c.sub_claim_type_id = sct.sub_claim_type_id

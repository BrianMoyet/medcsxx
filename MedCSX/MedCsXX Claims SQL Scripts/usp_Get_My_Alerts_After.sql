USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_My_Alerts_After]    Script Date: 10/14/2020 11:09:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER  PROCEDURE [dbo].[usp_Get_My_Alerts_After]
@user_id foreign_key,
@alert_start_date datetime
AS

-- Get Non-Deferred Alerts
select alert_id, dbo.yes_no(is_urgent) as Urgent, 
Sent_From = case dbo.get_user_display_name(sent_from_user_id) 
	when '' then 'System' 
	else dbo.get_user_display_name(sent_from_user_id) 
	end, 
date_sent, c.display_claim_id, f.file_note_text as file_note_text,
ft.Image_Index, a.Is_Deferred, a.Deferred_Date, ft.description as file_note_type, alert_id as Id,
c.Policy_No, dbo.Get_Person_Name(c.Insured_Person_Id) AS 'Insured_Person'

from alert a WITH(NOLOCK), file_note f WITH(NOLOCK), claim c WITH(NOLOCK), file_note_type ft WITH(NOLOCK)

where f.claim_id = c.claim_id and sent_to_user_id = @user_id
and a.file_note_id = f.file_note_id and a.show_alert = 1
and f.file_note_type_id = ft.file_note_type_id
and ((a.date_sent >= @alert_start_date and is_deferred = 0) OR 
(is_deferred = 1 and a.deferred_date >= @alert_start_date and getdate() >= a.deferred_date))
order by dbo.yes_no(is_urgent) DESC, date_sent DESC
--	
--union
--
---- Get Deferred Alerts
--select alert_id, dbo.yes_no(is_urgent) as Urgent, 
--Sent_From = case dbo.get_user_display_name(sent_from_user_id) 
--	when '' then 'System' 
--	else dbo.get_user_display_name(sent_from_user_id) 
--	end, 
--date_sent, c.display_claim_id, convert(varchar(8000), f.file_note_text),
--ft.Image_Index, a.Is_Deferred, a.Deferred_Date, ft.description as file_note_type, alert_id as Id,
--c.Policy_No, dbo.Get_Person_Name(c.Insured_Person_Id) AS 'Insured_Person'
--
--from alert a, file_note f, claim c, file_note_type ft
--
--where f.claim_id = c.claim_id and sent_to_user_id = @user_id
--and a.file_note_id = f.file_note_id and a.show_alert = 1
--and f.file_note_type_id = ft.file_note_type_id
--and is_deferred = 1 and a.deferred_date >= @alert_start_date and getdate() >= a.deferred_date
--
--order by a.date_sent desc
/* select alert_id, dbo.yes_no(is_urgent) as Urgent, 
Sent_From = case dbo.get_user_display_name(sent_from_user_id) 
	when '' then 'System' 
	else dbo.get_user_display_name(sent_from_user_id) 
	end, 
date_sent, c.display_claim_id, f.file_note_text,
ft.Image_Index, a.Is_Deferred, a.Deferred_Date

from alert a, file_note f, claim c, file_note_type ft

where f.claim_id = c.claim_id and sent_to_user_id = @user_id
and a.file_note_id = f.file_note_id and a.show_alert = 1
and f.file_note_type_id = ft.file_note_type_id
and ((a.date_sent >= @alert_start_date and is_deferred = 0) 
	or (is_deferred = 1 and a.deferred_date >= @alert_start_date and getdate() >= a.deferred_date) )

order by a.date_sent desc */



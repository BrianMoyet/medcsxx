USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Daily_Draft_Report_By_Year]    Script Date: 9/30/2020 10:55:07 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Get_Daily_Draft_Report_By_Year] 
 @years int



 AS

select
ltrim(str(month(t.created_date))) + '/' + ltrim(str(day(t.created_date))) + '/' + ltrim(str(year(t.created_date))) as Date, 
sum(-1.0 * t.transaction_amount) as Amount
 into #temp
from draft d, [transaction] t
where d.draft_id <> 0 and t.draft_id = d.draft_id
group by ltrim(str(month(t.created_date))) + '/' + ltrim(str(day(t.created_date))) + '/' + 
	ltrim(str(year(t.created_date))), year(t.created_date), month(t.created_date), day(t.created_date)


--insert into #temp select * from v_daily_draft_report_dates_2

insert into #temp select ltrim(str(month(t.created_date))) + '/' + ltrim(str(day(t.created_date))) + '/' + ltrim(str(year(t.created_date))) as Date,
sum(-1.0 * t.transaction_amount) as Amount
from draft d, [transaction] t
where d.draft_id <> 0 and  t.void_draft_id = d.draft_id
group by ltrim(str(month(t.created_date))) + '/' + ltrim(str(day(t.created_date))) + '/' + 
	ltrim(str(year(t.created_date))), year(t.created_date), month(t.created_date), day(t.created_date)


select month([date]) as Months, sum(Amount) as Amount
from #temp
where (year([date]) = @years) 
group by  month([date]), year([date])
order by year([date]), month([date])

drop table #temp
GO


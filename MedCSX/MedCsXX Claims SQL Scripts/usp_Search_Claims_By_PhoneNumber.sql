USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_PhoneNumber]    Script Date: 2/10/2021 11:53:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_PhoneNumber]
	
	@phoneNumber		varchar(16),
	@subClaimTypeId	int = NULL
	
AS
	-- Return Claims whose phone number  = @phoneNuber
	
	-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.
	SELECT c.claim_id, display_claim_id, policy_no, date_of_loss, 
		Case dbo.get_user_display_name(adjuster_id) 
			WHEN '' THEN 'Unassigned' 
			ELSE dbo.get_user_display_name(adjuster_id)  
		END As adjuster, 
		last_activity_date, cs.description,
		dbo.get_person_name(c.insured_person_id) As insured, 
		loss_description, dbo.claim_locked(c.claim_id) As is_locked,
	 	c.sub_claim_type_id, sct.[description] As 'Sub_Claim_Type',
		p.First_Name, p.Last_Name, cell_phone, Home_Phone, Fax, Other_Contact_Phone, Work_Phone, c.Date_Reported 
	FROM claim c, claim_status cs, sub_claim_type sct, Person p
	WHERE p.Claim_Id = c.Claim_Id 
		and	(p.Cell_Phone = @phoneNumber 
			or p.Home_Phone = @phoneNumber
			or p.Fax = @phoneNumber
			or p.Other_Contact_Phone = @phoneNumber
			or p.Work_Phone = @phoneNumber)
			And c.claim_id <> 0
				And c.claim_status_id = cs.claim_status_id
					And c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
						And c.sub_claim_type_id = sct.sub_claim_type_id

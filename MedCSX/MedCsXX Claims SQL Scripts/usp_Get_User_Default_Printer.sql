USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_User_Default_Printer]    Script Date: 12/16/2020 1:56:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[usp_Get_User_Default_Printer]
	@userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   [User].User_Default_Draft_Printer_Id
FROM            [User] 
WHERE        ([User].User_Id = @userid)
END

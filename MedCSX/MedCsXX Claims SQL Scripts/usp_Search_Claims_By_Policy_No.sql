USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Policy_No]    Script Date: 2/10/2021 11:25:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Policy_No]
	
	@policy_no		varchar(50),
	@subClaimTypeId	int = NULL
		
AS
	-- Return Claims whose Policy_No is like @policy_no
	
	-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.
	
	SELECT claim_id, display_claim_id, policy_no, date_of_loss, date_reported,
		CASE dbo.get_user_display_name(adjuster_id)
			WHEN '' THEN 'Unassigned' ELSE dbo.get_user_display_name(adjuster_id)  
		END As adjuster, 
		last_activity_date, cs.description,
		dbo.get_person_name(c.insured_person_id) As insured, loss_description, dbo.claim_locked(claim_id) As is_locked,
		 c.sub_claim_type_id, sct.[description] As 'Sub_Claim_Type'
	FROM claim c, claim_status cs, sub_claim_type sct
	WHERE c.policy_no LIKE '%' + @policy_no + '%'
		And c.claim_id <> 0
			And c.claim_status_id = cs.claim_status_id
				And c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
					And c.sub_claim_type_id = sct.sub_claim_type_id

	/*
	IF len(@policy_no) > 2 And left(@policy_no, 3) IN ('CBB', 'MBA', 'MBR')
	BEGIN
		SELECT policy_number
		FROM GAAS01.dbo.Policy p
		WHERE p.policy_number LIKE '%' + @policy_no + '%'
	END
	ELSE
	BEGIN
		SELECT claim_id, display_claim_id, policy_no, date_of_loss, 
			CASE dbo.get_user_display_name(adjuster_id)
				WHEN '' THEN 'Unassigned' ELSE dbo.get_user_display_name(adjuster_id)  
			END As adjuster, 
			last_activity_date, cs.description,
			dbo.get_person_name(c.insured_person_id) As insured, loss_description, dbo.claim_locked(claim_id) As is_locked,
			 c.sub_claim_type_id, sct.[description] As 'Sub_Claim_Type'
		FROM claim c, claim_status cs, sub_claim_type sct
		WHERE c.policy_no LIKE '%' + @policy_no + '%'
			And c.claim_id <> 0
				And c.claim_status_id = cs.claim_status_id
					And c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
						And c.sub_claim_type_id = sct.sub_claim_type_id
	END
	*/

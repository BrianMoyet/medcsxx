USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Open_Appraisal_Totals]    Script Date: 10/30/2020 9:59:09 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[usp_Get_Open_Appraisal_Totals] AS

SELECT        s.Scene_Access_Appraiser_Id AS id, v.Name, COUNT(*) AS c, 2 AS ImageIndex
FROM            Appraisal_Request AS ar INNER JOIN
                         Appraisal_Task AS a ON ar.Appraisal_Request_Id = a.Appraisal_Request_Id INNER JOIN
                         Vendor AS v INNER JOIN
                         Scene_Access_Appraiser AS s ON v.Vendor_Id = s.Vendor_Id ON a.Scene_Access_Appraiser_Id = s.Scene_Access_Appraiser_Id INNER JOIN
                         Claim AS c ON ar.Claim_Id = c.Claim_Id
WHERE        (a.Appraisal_Task_Status_Id NOT IN (3, 5)) AND (c.Claim_Status_Id <> 6) AND (a.Scene_Access_Appraiser_Id > 0)
GROUP BY v.Name, s.Scene_Access_Appraiser_Id
GO


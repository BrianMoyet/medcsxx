USE [Claims]
GO
/****** Object:  Table [dbo].[Draft_Printing_Location]    Script Date: 12/28/2020 11:28:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draft_Printing_Location](
	[Draft_Printing_Location_Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nchar](255) NOT NULL,
	[Image_Index] [int] NOT NULL,
 CONSTRAINT [PK_Draft_Printing_Location] PRIMARY KEY CLUSTERED 
(
	[Draft_Printing_Location_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Draft_Printing_Location] ON 
GO
INSERT [dbo].[Draft_Printing_Location] ([Draft_Printing_Location_Id], [Description], [Image_Index]) VALUES (1, N'User Default                                                                                                                                                                                                                                                   ', 0)
GO
INSERT [dbo].[Draft_Printing_Location] ([Draft_Printing_Location_Id], [Description], [Image_Index]) VALUES (2, N'Corporate (KC)                                                                                                                                                                                                                                                 ', 0)
GO
INSERT [dbo].[Draft_Printing_Location] ([Draft_Printing_Location_Id], [Description], [Image_Index]) VALUES (3, N'Las Vegas                                                                                                                                                                                                                                                      ', 0)
GO
SET IDENTITY_INSERT [dbo].[Draft_Printing_Location] OFF
GO
ALTER TABLE [dbo].[Draft_Printing_Location] ADD  CONSTRAINT [DF_Draft_Printing_Location_Image_Index]  DEFAULT ((0)) FOR [Image_Index]
GO

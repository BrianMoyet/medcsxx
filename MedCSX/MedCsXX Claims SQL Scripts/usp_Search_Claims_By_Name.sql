USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Name]    Script Date: 2/10/2021 11:32:02 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Name] 
@first_name first_name,
@last_name last_name,
@subClaimTypeId int = NULL 
AS
-- Search Claims by First and Last Names Using Like 

-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.

if @first_name = ''
begin
	set @first_name = '%'
end

if @last_name = ''
begin
	set @last_name = '%'
end

select distinct c.claim_id, display_claim_id, policy_no, date_of_loss, date_reported,
case dbo.get_user_display_name(adjuster_id) when '' then 'Unassigned' else dbo.get_user_display_name(adjuster_id)  end 
as adjuster, 
last_activity_date, cs.description,
dbo.get_person_name(p.person_id) as insured, dbo.get_person_type(p.person_id) as loss_description , dbo.claim_locked(c.claim_id) as is_locked,
 c.sub_claim_type_id, sct.[description] as 'Sub_Claim_Type'

from claim c, claim_status cs, person p, sub_claim_type sct

where c.claim_status_id = cs.claim_status_id
and p.claim_id = c.claim_id
and c.claim_id <> 0
and p.first_name like @first_name
and p.last_name like  @last_name
and c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
and c.sub_claim_type_id = sct.sub_claim_type_id

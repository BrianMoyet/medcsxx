USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Delete_Litigation_Reserves]    Script Date: 8/12/2020 2:14:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Delete_Litigation_Reserves] 
@litigation_id int
AS

DELETE FROM Litigation_Reserve WHERE Litigation_Id = @litigation_id
GO


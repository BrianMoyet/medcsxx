USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Drafts_In_Queue_By_Default_Printer]    Script Date: 2/5/2021 1:49:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[usp_Get_Drafts_In_Queue_By_Default_Printer] 

		 @default_Printer_ID INT

		 AS

SELECT        d.Draft_Id, d.Reissued_From_Draft_Id, d.Is_Void, d.Payee, d.Vendor_Id, d.Draft_Amount, d.Explanation_Of_Proceeds, d.Mail_To_Name, d.Address1, d.Address2, d.City, d.State_Id, d.Zipcode, d.Draft_No, d.Bank_Routing_No, 
                         d.Bank_Account_No, d.Draft_Void_Reason_Id, d.No_Periods, d.Is_Printed, d.Print_Date, d.Pip_Date_From, d.Pip_Date_To, d.In_Draft_Queue, d.Actual_Print_Date, d.Released_From_Queue_By, d.Created_By, d.Modified_By, 
                         d.Created_Date, d.Modified_Date, c.Claim_Id, c.Claim_Status_Id, c.Display_Claim_Id, c.Policy_No, c.Version_No, c.Is_Expired_Policy, c.Company_Location_Id, c.Date_Of_Loss, c.Adjuster_Id, c.Adjuster_Assignment_Date, 
                         c.Insured_Person_Id, c.Has_Coverage_Issue, c.Has_Disputed_Liability, c.Insured_Vehicle_Id, c.Owner_Person_Id, c.Driver_Person_Id, c.Is_Comp, c.Is_Collision, c.Vehicle_Damage, c.Where_Seen, c.Permission_To_Use, 
                         c.Last_Activity_Date, c.Loss_Location, c.Loss_City, c.Loss_State_Id, c.Loss_Description, c.Authority_Contacted, c.Police_Report_Number, c.Reported_By, c.Reported_By_Type_Id, c.Close_Date, c.Reopen_Date, c.Comments, 
                         c.No_Supplemental_Payments, c.Accident_Type_Id, c.Date_Reported, c.Adjuster_Entered_First_File_Note, c.Is_Foxpro_Conversion, c.Foxpro_Claim_No, c.Setup_Complete, c.Date_First_Contact_Insured, 
                         c.Date_Attempt_Contact_Insured, c.At_Fault_Type_Id, c.At_Fault_Date_Set, c.Created_By AS Expr1, c.Modified_By AS Expr2, c.Created_Date AS Expr3, c.Modified_Date AS Expr4, c.Large_Loss, c.Sub_Claim_Type_Id, 
                         c.Loss_Location_Nbr, c.Loss_Location_Id, c.Notification_Date, c.Deductible, c.Limit, c.IsLondon, c.HasSR22, c.No_Loss_Sent, dbo.Get_User_Display_Name(d.Created_By) AS Adjuster, c.Display_Claim_Id AS Claim, 
                         d.Draft_Id AS Id, [User].First_Name + ' ' + [User].Last_Name AS Created_By_Name
FROM            Draft AS d INNER JOIN
                         Claim AS c ON dbo.Claim_Id_For_Draft_Id(d.Draft_Id) = c.Claim_Id INNER JOIN
                         [User] ON d.Created_By = [User].User_Id
WHERE        (d.In_Draft_Queue <> 0) AND ([User].User_Default_Draft_Printer_Id = @default_Printer_ID)
ORDER BY d.Print_Date

GO


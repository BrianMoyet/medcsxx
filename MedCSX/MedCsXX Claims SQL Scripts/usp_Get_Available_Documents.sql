USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Available_Documents]    Script Date: 11/9/2020 2:58:20 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



ALTER  PROCEDURE [dbo].[usp_Get_Available_Documents] 
@ClaimTypeId int = NULL 
AS

-- Return the available documents

select dt.*
from document_type dt
where dt.Active = 1 and (dt.claim_type_id = 0 OR dt.claim_type_id = coalesce(@ClaimTypeId, dt.claim_type_id))
order by dt.[description]



GO


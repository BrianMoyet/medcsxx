USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Date]    Script Date: 2/10/2021 11:48:36 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Date] 
@no_days integer,
@subClaimTypeId int = NULL 
AS
-- Return Open Claims Not Older than @no_days 

-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.

select claim_id, display_claim_id, policy_no, date_of_loss, 
case dbo.get_user_display_name(adjuster_id) when '' then 'Unassigned' else dbo.get_user_display_name(adjuster_id)  end 
as adjuster, 
last_activity_date, cs.description,
dbo.get_person_name(c.insured_person_id) as insured, loss_description, dbo.claim_locked(claim_id) as is_locked,
 c.sub_claim_type_id, sct.[description] as 'Sub_Claim_Type', c.Date_Reported

from claim c, claim_status cs, sub_claim_type sct

where c.created_date >= dateadd(day, -1 * @no_days, getdate())
and c.claim_status_id = cs.claim_status_id
and c.claim_status_id <> 6  /* claim closed */
and c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
and c.sub_claim_type_id = sct.sub_claim_type_id

order by c.last_activity_date desc

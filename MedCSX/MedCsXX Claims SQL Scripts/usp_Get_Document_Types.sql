USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Document_Types]    Script Date: 10/27/2020 2:07:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


ALTER  procedure [dbo].[usp_Get_Document_Types] as

SELECT        d.Document_Type_Id AS Id, d.Description, d.File_Name, e.Description AS Event_Type, d.Stored_Procedure_Name, d.Image_Index, ct.Description AS Claim_Type, d.Active
FROM            Document_Type AS d INNER JOIN
                         Event_Type AS e ON d.Event_Type_Id = e.Event_Type_Id INNER JOIN
                         Claim_Type AS ct ON d.Claim_Type_Id = ct.Claim_Type_Id
ORDER BY d.Description


GO


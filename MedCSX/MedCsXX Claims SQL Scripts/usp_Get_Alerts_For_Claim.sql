USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Alerts_For_Claim]    Script Date: 10/14/2020 11:02:27 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Get_Alerts_For_Claim] 
@claim_id foreign_key
AS
/* Return all alerts for a claim */

select a.Alert_Id, ft.Image_Index, dbo.Yes_No(a.Is_Urgent) as Urgent, 
Sent_From = case a.Sent_From_User_Id
	when 0 then 'System'
	else dbo.Get_User_Display_Name(a.Sent_From_User_Id)
end, 
Sent_To = case a.Sent_To_User_Id
	when 0 then 'System'
	else dbo.Get_User_Display_Name(a.Sent_To_User_Id)
end,
a.Date_Sent, a.Modified_Date, a.Date_Responded_To, a.Deferred_Date, ft.Description as File_Note_Type, f.File_Note_Text 
from Alert a, File_Note f, claim c, File_Note_Type ft
where f.File_Note_Id = a.File_Note_Id
and f.Claim_Id = c.Claim_Id 
and ft.File_Note_Type_Id = f.File_Note_Type_Id 
and c.Claim_Id = @claim_id order by dbo.Yes_No(a.Is_Urgent) DESC, a.Date_Sent DESC
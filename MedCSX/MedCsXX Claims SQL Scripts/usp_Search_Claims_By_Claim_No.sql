USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Claim_No]    Script Date: 2/10/2021 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[usp_Search_Claims_By_Claim_No]
	
	@claim_no display_claim_id,
	@subClaimTypeId int = NULL
	
AS
	-- Return Claims whose Display Claim Id is like @claim_no
	
	-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.
	
	DECLARE
		@b		int,
		@searchString	varchar(20)
	
	SELECT @b = isnumeric(@claim_no)
	IF @b = 1
		SET @searchString =  '%' + @claim_no + '%'
	ELSE
		SET @searchString =  @claim_no + '%'
	
	SELECT claim_id, display_claim_id, policy_no, date_of_loss, c.sub_claim_type_id, sct.[description] As 'Sub_Claim_Type',
		CASE dbo.get_user_display_name(adjuster_id)
			WHEN '' THEN 'Unassigned' ELSE dbo.get_user_display_name(adjuster_id) 
		END As adjuster, 
		last_activity_date, cs.description,
		dbo.get_person_name(c.insured_person_id) As insured, loss_description, 
		dbo.claim_locked(claim_id) As is_locked, Date_Reported
	FROM claim c, claim_status cs, sub_claim_type sct
	WHERE c.display_claim_id LIKE @searchString
		And c.claim_id <> 0
			And c.claim_status_id = cs.claim_status_id
				And c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
					And c.sub_claim_type_id = sct.sub_claim_type_id

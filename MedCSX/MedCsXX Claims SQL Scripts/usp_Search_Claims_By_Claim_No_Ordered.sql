USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Claim_No_Ordered]    Script Date: 2/10/2021 12:01:09 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		James Willoughby
-- Create date: 10/04/2017
-- Description:	Search for specified policy and order by scalar
-- =============================================
ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Claim_No_Ordered] 
	@policy_no		varchar(50),
	@orderBy		varchar(255),
	@subClaimTypeId	int = NULL
		
AS
	-- Return Claims whose Policy_No is like @policy_no
BEGIN	
	SELECT claim_id, display_claim_id, policy_no, date_of_loss, 
		CASE dbo.get_user_display_name(adjuster_id)
			WHEN '' THEN 'Unassigned' ELSE dbo.get_user_display_name(adjuster_id)  
		END As adjuster, 
		last_activity_date, cs.description,
		dbo.get_person_name(c.insured_person_id) As insured, loss_description, dbo.claim_locked(claim_id) As is_locked,
		 c.sub_claim_type_id, sct.[description] As 'Sub_Claim_Type', c.Company_Location_Id, 
		CASE @orderBy WHEN 'Company_Location_Id' THEN
		(SELECT co.[Description] + ', ' + col.Abbreviation
		FROM Company_Location col
		inner join Company co on co.Company_Id=col.Company_Id
		WHERE col.Company_Location_Id=c.Company_Location_Id)
		--insert additional parameters here 
		ELSE 'Unfiltered'
		END as Ordered, c.Date_Reported

	FROM claim c, claim_status cs, sub_claim_type sct
	WHERE c.policy_no LIKE '%' + @policy_no + '%'
		And c.claim_id <> 0
			And c.claim_status_id = cs.claim_status_id
				And c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
					And c.sub_claim_type_id = sct.sub_claim_type_id

	ORDER BY Ordered, c.Last_Activity_Date desc
END

USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Date_Ordered]    Script Date: 2/10/2021 11:52:28 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		James Willoughby
-- Create date: 10/04/2017
-- Description:	Searches for claims not older than specified days and orders by scalar
-- =============================================
ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Date_Ordered] 
@no_days integer,
@orderBy varchar(255),
@subClaimTypeId int = NULL 
AS
-- Return Open Claims Not Older than @no_days 

BEGIN
select claim_id, display_claim_id, policy_no, date_of_loss, date_reported,
case dbo.get_user_display_name(adjuster_id) when '' then 'Unassigned' else dbo.get_user_display_name(adjuster_id)  end 
as adjuster, 
last_activity_date, cs.description,
dbo.get_person_name(c.insured_person_id) as insured, loss_description, dbo.claim_locked(claim_id) as is_locked,
 c.sub_claim_type_id, sct.[description] as 'Sub_Claim_Type', c.Company_Location_Id,
case @orderBy when 'Company_Location_Id' then
(select co.[Description] + ', ' + col.Abbreviation
from Company_Location col
inner join Company co on co.Company_Id=col.Company_Id
where col.Company_Location_Id=c.Company_Location_Id)
--insert additional parameters here 
else 'Unfiltered'
end as Ordered

from claim c, claim_status cs, sub_claim_type sct

where c.created_date >= dateadd(day, -1 * @no_days, getdate())
and c.claim_status_id = cs.claim_status_id
and c.claim_status_id <> 6  /* claim closed */
and c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
and c.sub_claim_type_id = sct.sub_claim_type_id

order by Ordered, c.last_activity_date desc

END

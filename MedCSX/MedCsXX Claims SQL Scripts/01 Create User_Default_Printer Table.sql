USE [Claims]
GO
/****** Object:  Table [dbo].[User_Default_Printer]    Script Date: 12/28/2020 11:22:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Default_Printer](
	[User_Default_Printer_Id] [int] IDENTITY(1,1) NOT NULL,
	[Printer_Name] [nchar](255) NOT NULL,
	[Printer_Location] [nchar](255) NOT NULL,
 CONSTRAINT [PK_User_Default_Printer] PRIMARY KEY CLUSTERED 
(
	[User_Default_Printer_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[User_Default_Printer] ON 
GO
INSERT [dbo].[User_Default_Printer] ([User_Default_Printer_Id], [Printer_Name], [Printer_Location]) VALUES (1, N'\\\\moose\\HP Draft Key Home Office                                                                                                                                                                                                                            ', N'Corporate                                                                                                                                                                                                                                                      ')
GO
INSERT [dbo].[User_Default_Printer] ([User_Default_Printer_Id], [Printer_Name], [Printer_Location]) VALUES (2, N'\\\\moose\\HP Draft Key LV                                                                                                                                                                                                                                     ', N'Las Vegas                                                                                                                                                                                                                                                      ')
GO
SET IDENTITY_INSERT [dbo].[User_Default_Printer] OFF
GO

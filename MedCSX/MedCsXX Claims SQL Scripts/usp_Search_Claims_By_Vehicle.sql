USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Vehicle]    Script Date: 2/10/2021 11:41:13 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Vehicle] 
@year year,
@make manufacturer,
@model model,
@vin vin,
@subClaimTypeId int = NULL 
AS
-- Search Claims by Vehicle Year, Make, Model, and Vins Using Like 

-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.

select distinct c.claim_id, display_claim_id, policy_no, date_of_loss, 
case dbo.get_user_display_name(adjuster_id) when '' then 'Unassigned' else dbo.get_user_display_name(adjuster_id)  end 
as adjuster, 
last_activity_date, cs.description,
dbo.get_person_name(c.insured_person_id) as insured, cast(loss_description as  varchar(500)), dbo.claim_locked(c.claim_id) as is_locked,
 c.sub_claim_type_id, sct.[description] as 'Sub_Claim_Type', c.Date_Reported

from claim c, claim_status cs, vehicle v, sub_claim_type sct

where c.claim_status_id = cs.claim_status_id
and c.claim_id <> 0
and v.claim_id = c.claim_id
and v.year_made like @year + '%'
and v.manufacturer like  @make + '%'
and v.model like @model + '%'
and v.vin like @vin + '%'
and c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
and c.sub_claim_type_id = sct.sub_claim_type_id

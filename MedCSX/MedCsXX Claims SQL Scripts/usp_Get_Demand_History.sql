USE [Claims]
GO

/****** Object:  StoredProcedure [dbo].[usp_Get_Demand_History]    Script Date: 11/11/2020 3:09:20 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



ALTER  PROCEDURE [dbo].[usp_Get_Demand_History]
@vendor_id int,
@reserve_id int
AS

SELECT D.Demand_Id as 'ID', 'Demand' as 'Type', D.Demand_Date as 'Date', D.Demand_Amount as 'Amount', D.Received_Date, D.Due_Date
FROM Demand D
WHERE D.Vendor_Id = @vendor_id
and D.Reserve_Id = @reserve_id

UNION

SELECT O.Offer_Id as 'ID', 'Offer' as 'Type', O.Offer_Date as 'Date', O.Offer_Amount as 'Amount', '' as 'Received_Date', '' as 'Due_Date'
FROM Offer O, Demand D
WHERE O.Demand_Id = D.Demand_Id
and D.Vendor_Id = @vendor_id
and D.Reserve_Id = @reserve_id

ORDER BY 'Date', 'Type'


GO


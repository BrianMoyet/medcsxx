USE [Claims]
GO
/****** Object:  StoredProcedure [dbo].[usp_Search_Claims_By_Date_Of_Loss]    Script Date: 2/10/2021 11:37:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_Search_Claims_By_Date_Of_Loss]
	
	@date_of_loss		datetime,
	@subClaimTypeId	int = NULL
	
AS
	-- Return Claims whose Date of Loss = @date_of_loss
	
	-- 7/4/2007 Dave Crook - added @subClaimTypeId as optional parameter.  If not specified, all Sub_Claim_Types are returned.
	
	SELECT claim_id, display_claim_id, policy_no, date_of_loss, date_reported,
		Case dbo.get_user_display_name(adjuster_id) 
			WHEN '' THEN 'Unassigned' 
			ELSE dbo.get_user_display_name(adjuster_id)  
		END As adjuster, 
		last_activity_date, cs.description,
		dbo.get_person_name(c.insured_person_id) As insured, 
		loss_description, dbo.claim_locked(claim_id) As is_locked,
	 	c.sub_claim_type_id, sct.[description] As 'Sub_Claim_Type'
	FROM claim c, claim_status cs, sub_claim_type sct
	WHERE year(c.date_of_loss) = year(@date_of_loss) And month(c.date_of_loss) = month(@date_of_loss)
		And datepart(dd, c.date_of_loss) = datepart(dd, @date_of_loss)
		-- (c.date_of_loss between dbo.make_beginning_date(@date_of_loss) And dbo.make_end_date(@date_of_loss) )
			And c.claim_id <> 0
				And c.claim_status_id = cs.claim_status_id
					And c.sub_claim_type_id = coalesce(@subClaimTypeId, c.sub_claim_type_id)
						And c.sub_claim_type_id = sct.sub_claim_type_id

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Windows;
using MedCsxLogic;

using System.Runtime.InteropServices;
using PdfPrintingNet;
using System.Security.Principal;
using System.Configuration;
//using PdfEdit.Pdf;

namespace MedCSX
{
    /* General method module
     * All methods are very general and may be reused */

    class Functions
    {
        public const int LOGON32_LOGON_INTERACTIVE = 2;
        public const int LOGON32_PROVIDER_DEFAULT = 0;

        static WindowsImpersonationContext impersonationContext;

        [DllImport("advapi32.dll")]
        public static extern int LogonUserA(String lpszUserName,
        String lpszDomain,
        String lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        ref IntPtr phToken);
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken,
        int impersonationLevel,
        ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);



        private static ModMain mm = new ModMain();
        #region Files
        [Description("Returns the contents of a file.")]
        public string ReadFile(string Path)
        {
            //Open a file and read the file stream
            FileStream myStream = new FileStream(Path, FileMode.Open);
            StreamReader myReader = new StreamReader(myStream);
            string myFileContents = myReader.ReadToEnd();

            //close the stream and reader
            myReader.Close();
            myStream.Close();

            return myFileContents;
        }

        [Description("Writes the file contents to the specified file.")]
        public static void WriteFile(string Path, string fileContents)
        {
            //create file and write to the file stream
            FileStream myStream = new FileStream(Path, FileMode.Create, FileAccess.Write);
            StreamWriter myWriter = new StreamWriter(myStream);
            myWriter.Write(fileContents);

            //close the stream and writer
            myWriter.Close();
            myStream.Close();
        }
        #endregion

        #region Web
        [Description("Gets a web page at the address specified and returns the HTLM code.")]
        public string GetWebPage(string address)
        {
            try
            {
                //get web page
                WebClient myClient = new WebClient();
                Stream myStream = myClient.OpenRead(address);
                StreamReader myReader = new StreamReader(myStream);
                string myReturnString = myReader.ReadToEnd();

                //close the stream and stream reader
                myReader.Close();
                myStream.Close();

                //return the HTML
                return myReturnString;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        [Description("Sends an email message.")]
        public void SendEmail(string SMTPServer, string from_EmailAddress, string to_EmailAddress, string Subject, string Body, bool isBodyHTML = false)
        {
            //create the mail message
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from_EmailAddress, to_EmailAddress, Subject, Body);
            msg.IsBodyHtml = isBodyHTML;

            //send the message
            System.Net.Mail.SmtpClient server = new System.Net.Mail.SmtpClient(SMTPServer);
            server.Send(msg);
        }
        #endregion

        #region Screen Capture
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hOpbject);

        //public Bitmap ScreenCapture()
        //{
        //    Bitmap screenshotBmp;
        //    //get an empty Bitmap to store the screen shot
        //    screenshotBmp = new Bitmap((int)SystemParameters.PrimaryScreenWidth, 
        //        (int)SystemParameters.PrimaryScreenHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

        //    //get a graphics context from the empty bitmap
        //    using (Graphics g = Graphics.FromImage(screenshotBmp))
        //    {
        //        //calls the native BitBlt
        //        //specify the rect that contains the pixels to capture
        //        //screenshot is now stored in the screenshotBmp
        //        g.CopyFromScreen(0, 0, 0, 0, screenshotBmp.Size);
        //    }

        //    IntPtr handle = IntPtr.Zero;
        //    try
        //    {
        //        //get the GDI handle for the Bitmap
        //        handle = screenshotBmp.GetHbitmap();

        //        /*convert from the .NET image format to the WPF image format
        //         * Returns a manged BitmapSource, based on the provided pointer
        //         * to an unmanaged bitmap and palette information*/

        //        //placed in <Image />
        //        //imageCapture.Source = Imaging.CreateBitmapSourceFromHBitmap(handle, IntPtr.Zero, 
        //        //    Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        //    }
        //    finally
        //    {
        //        //we must call DeleteObject to free the handle we received from GetHbitmap()
        //        DeleteObject(handle);
        //    }
        //    return screenshotBmp;
        //}

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern IntPtr GetActiveWindow();

        //public Bitmap WindowCapture()
        //{
        //    IntPtr active = GetActiveWindow();
        //    Window ActiveWindow = Application.Current.Windows.OfType<Window>().FirstOrDefault(window => new WindowInteropHelper(window).Handle == active);
        //   // Rectangle Bounds = ActiveWindow.TransformToVisual(relativeTo).TransformBounds(LayoutInformation.GetLayoutSlot(ActiveWindow))
        //    Bitmap screenshotBmp;
        //    //get an empty Bitmat to store the screen shot
        //    screenshotBmp = new Bitmap((int)ActiveWindow.Width, (int)ActiveWindow.Height, 
        //        System.Drawing.Imaging.PixelFormat.Format32bppArgb);

        //    //get a graphics context from the empty bitmap
        //    using (Graphics g = Graphics.FromImage(screenshotBmp))
        //    {
        //        //calls the native BitBlt
        //        //specify the rect that contains the pixels to capture
        //        //screenshot is now stored in the screenshotBmp
        //        g.CopyFromScreen(0, 0, 0, 0, screenshotBmp.Size);
        //    }

        //    IntPtr handle = IntPtr.Zero;
        //    try
        //    {
        //        //get the GDI handle for the Bitmap
        //        handle = screenshotBmp.GetHbitmap();

        //        /*convert from the .NET image format to the WPF image format
        //         * Returns a manged BitmapSource, based on the provided pointer
        //         * to an unmanaged bitmap and palette information*/

        //        //placed in <Image />
        //        //imageCapture.Source = Imaging.CreateBitmapSourceFromHBitmap(handle, IntPtr.Zero, 
        //        //    Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        //    }
        //    finally
        //    {
        //        //we must call DeleteObject to free the handle we received from GetHbitmap()
        //        DeleteObject(handle);
        //    }
        //    return screenshotBmp;
        //}
        #endregion

        #region Dates/Times
        [Description("Change the total number of seconds to an hh:mm:ss format.")]
        public string ChangeTimeFormat(int numSecs)
        {
            //create the new amount of time
            TimeSpan amtOfTime = new TimeSpan(0, 0, numSecs);

            //create the return string
            string hh = amtOfTime.Hours.ToString();
            string mm = amtOfTime.Minutes.ToString();
            string ss = amtOfTime.Seconds.ToString();
            hh = hh.PadLeft(2, '0');
            mm = mm.PadLeft(2, '0');
            ss = ss.PadLeft(2, '0');

            //return formated time
            return hh + ":" + mm + ":" + ss;
        }

        [Description("Change the time in the format hh:mm:ss to the total number of seconds.")]
        public int GetNumSeconds(string fTime)
        {
            //split the fTime into its components
            string[] timeIncrements = fTime.Split(':');

            //create the timespan
            TimeSpan amtOfTime = new TimeSpan(int.Parse(timeIncrements[0]), int.Parse(timeIncrements[1]), int.Parse(timeIncrements[2]));

            return (int)amtOfTime.TotalSeconds;
        }

        //public DraftPrint InsertDraftPrintForPending(Reserve res)
        //{
        //    DraftPrint dp = new DraftPrint();
        //    int draftPrintId;
        //    MedCsxLogic.Claim claim = res.Claim;

        //    dp.DraftId = this.DraftId;
        //    dp.MailToLine1 = this.MailToName;
        //    dp.MailToLine2 = this.Address1;

        //    if (this.Address2.Trim() != "")
        //    {
        //        dp.MailToLine3 = this.Address2;
        //        dp.MailToLine4 = this.City + ", " + this.State().Abbreviation + " " + this.Zipcode;
        //    }
        //    else
        //    {
        //        dp.MailToLine3 = this.City + ", " + this.State().Abbreviation + " " + this.Zipcode;
        //        dp.MailToLine4 = "";
        //    }

        //    dp.Insured = claim.InsuredPerson.Name;
        //    dp.ClaimNo = claim.DisplayClaimId;
        //    dp.PolicyNo = claim.PolicyNo;

        //    if (this.VendorId != 0)
        //        dp.TaxId = this.Vendor().TaxId;

        //    dp.Employee = ml.CurrentUser.Name;

        //    dp.LossDate = claim.DateOfLoss.ToString("MM/dd/yyyy");
        //    dp.DraftNo = this.DraftNo.ToString().PadLeft(6, '0');
        //    dp.DraftAmount = "";

        //    string cov = res.ReserveType.Description;
        //    cov = cov.Replace("ABI Major", "ABI");
        //    cov = cov.Replace("ABI Minor", "ABI");

        //    dp.DraftLongDate = DateTime.Now.ToString("MM/dd/yyy");
        //    dp.DraftAmountWithStars = "$____________";
        //    dp.DraftAmountDescription = "__________________________________________________________ Dollars";
        //    dp.Payee = this.Payee;
        //    dp.ExplanationOfProceeds = this.ExplanationOfProceeds;
        //    dp.RoutingSymbols = "<" + this.DraftNo.ToString().PadLeft(6, '0') + "< :" + this.BankRoutingNo + ": <" + this.BankAccountNo + "<";
        //    dp.NonNegotiable = "";
        //    draftPrintId = dp.Update();

        //    return dp;
        //}

        [Description("Returns the quarter of the year from the given month.")]
        public static int GetQuarter(int month)
        {
            switch (month)
            {
                case 4:
                case 5:
                case 6:
                    return 2;  //2nd Quarter
                case 7:
                case 8:
                case 9:
                    return 3;  //3rd Quarter
                case 10:
                case 11:
                case 12:
                    return 4;  //4th Quarter
                default:    //case 1,2,3
                    return 1;  //1st Quarter
            }
        }

        public static string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length) + "...";
            }
            return source;
        }

        public static bool IsCurrentQuarter(DateTime myDate)
        {
            //get relevant quarters
            int currQuarter = GetQuarter(DateTime.Now.Month);
            int myQuarter = GetQuarter(myDate.Month);

            return currQuarter == myQuarter;
        }
        #endregion

        #region Formatting
        public string FormatCurrency(double number, int numDigitsInMantissa = -1)
        {
            //create the number format
            System.Globalization.NumberFormatInfo numberFormat = new System.Globalization.CultureInfo("en-US", false).NumberFormat;

            if (numDigitsInMantissa >= 0)
            {       //return the number in the currency format with the number of digits in the mantissa set 
                numberFormat.CurrencyDecimalDigits = numDigitsInMantissa;
                return number.ToString("c", numberFormat);
            }
            else
                return number.ToString("c");  //return the number in currency format
        }

        public string FormatNumber(double number, int numDigitsInMantissa = -1)
        {
            //create the number format
            System.Globalization.NumberFormatInfo numberFormat = new System.Globalization.CultureInfo("en-US", false).NumberFormat;

            if (numDigitsInMantissa >= 0)
            {       //return the number with commas with the number of digits in the mantissa set 
                numberFormat.CurrencyDecimalDigits = numDigitsInMantissa;
                return number.ToString("n", numberFormat);
            }
            else
                return number.ToString("n");  //return the number with commas
        }
        #endregion

        #region Validation
        public bool IsDate(object input, DateTime output = default(DateTime))
        {
            //set the output date to be the result
            if (input.GetType() == typeof(DateTime))
            {
                output = (DateTime)input;
                return true;
            }
            else
                return DateTime.TryParse(input.ToString(), out output);
        }

        public bool IsNumeric(object num, decimal output = 0)
        {
            if (num == null)
                return false;  //num is not initialized

            if ((num.GetType() == typeof(string)) || (num.GetType() == typeof(char)))
                return decimal.TryParse(num.ToString().Replace("$", "").Replace(",", ""), out output);  //num is a string - parse to validate
            else if ((num.GetType() == typeof(Int16)) || (num.GetType() == typeof(Int32)) ||
                (num.GetType() == typeof(Int64)) || (num.GetType() == typeof(Single)) ||
                (num.GetType() == typeof(double)) || (num.GetType() == typeof(decimal)))
            {       //num is numeric
                output = (decimal)num;
                return true;
            }
            else
                return false;  //unknown type, not a number
        }
        #endregion

        //#region Reporting Services
        //[Description("Allows user to download the PDF version of the report.")]
        //public static void DownloadPDFReport(string reportURL, Hashtable reportParameters, string reportName)
        //{
        //    try
        //    {
        //        PrintReportingServices PrintMyReport = new PrintReportingServices();
        //        PrintMyReport.RenderReport(reportURL, reportParameters, reportName);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        //[Description("Allows user to download the PDF version of the report and returns the first page in bytes.")]
        public static Byte[] DownloadPDFFirstPage(string reportURL, Hashtable reportParameters)
        {
            try
            {
                PrintReportingServices printMyReport = new PrintReportingServices();
                return printMyReport.ReturnFirstPage_PDF(reportURL, reportParameters);
                //return printMyReport.RenderReport(reportURL, reportParameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Description("Allows user to print a reporting service report directly.")]
        public static void PrintReport(string reportURL, string printerName, Hashtable reportParameters)
        {
            try
            {
                PrintReportingServices printMyReport = new PrintReportingServices();
                printMyReport.PrintReport(reportURL, printerName, reportParameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

     

        private class PrintReportingServices
        {   //class for printing report service reports
            private com.medjames.reports.ReportingService rs;
            private Byte[][] m_renderedReport;
            private MemoryStream m_currentPageStream;
            private System.Drawing.Image m_docImage = null;
            private int m_numberOfPages;
            private int m_currentPrintingPage,
                m_lastPrintingPage;

            public PrintReportingServices()
            {   //create proxy object and authenicate
                rs = new com.medjames.reports.ReportingService();
                rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //rs.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["printedrpts"], System.Configuration.ConfigurationManager.AppSettings["663.rpts"], System.Configuration.ConfigurationManager.AppSettings["medntrs2"]);
            }

            public Byte[] ReturnFirstPage_PDF(string reportPath, Hashtable reportParameters)
            {
                string deviceInfo = null,
                    format = "PDF",
                    encoding = "",
                    mimeType = "";
                Byte[] firstPage = null;
                com.medjames.reports.Warning[] warnings = null;
                com.medjames.reports.ParameterValue[] reportHistoryParameters = null;
                com.medjames.reports.ParameterValue[] Params = null;
                string[] streamIds = null;

                foreach (string key in reportParameters.Keys)
                {   //add report parameters to parameters object

                    com.medjames.reports.ParameterValue param = new com.medjames.reports.ParameterValue();
                    param.Name = key;
                    if (reportParameters[key].GetType() == typeof(string))
                        param.Value = (string)reportParameters[key];
                    else if (reportParameters[key].GetType() == typeof(int))
                        param.Value = ((int)reportParameters[key]).ToString();

                    //resize parameter array
                    if (Params == null)
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, 1);
                    else
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, Params.Length + 1);

                    Params[Params.Length - 1] = param;  //add parameter to parameter array
                }

                //saving report to PDF file
                deviceInfo = "<DeviceInfo></DeviceInfo>";

                //execute report and get page count
                try
                {
                    //renders the first page of the report and returns stream IDs for subsequent pages
                    firstPage = rs.Render(reportPath, format, null, deviceInfo, Params, null, null,
                        out encoding, out mimeType, out reportHistoryParameters, out warnings, out streamIds);
                    return firstPage;
                }
                catch (SoapException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Byte[][] RenderReport(string reportPath, Hashtable reportParameters, string reportName = "")
            {
                string deviceInfo = null,
                    format = "PDF",
                    encoding = "",
                    mimeType = "";
                Byte[] firstPage = null;
                Byte[][] pages = null;
                com.medjames.reports.Warning[] warnings = null;
                com.medjames.reports.ParameterValue[] reportHistoryParameters = null;
                com.medjames.reports.ParameterValue[] Params = null;
                string[] streamIds = null;

                foreach (string key in reportParameters.Keys)
                {   //add report parameters to parameters object

                    //create parameter from hashtable item
                    com.medjames.reports.ParameterValue param = new com.medjames.reports.ParameterValue();
                    param.Name = key;
                    if (reportParameters[key].GetType() == typeof(string))
                        param.Value = (string)reportParameters[key];
                    else if (reportParameters[key].GetType() == typeof(int))
                        param.Value = ((int)reportParameters[key]).ToString();

                    //resize parameter array
                    if (Params == null)
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, 0);
                    else
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, Params.Length);

                    Params[Params.Length] = param;  //add parameter to parameter array
                }

                if (reportName == "")
                {   //print report as image
                    format = "IMAGE";
                    deviceInfo = "<DeviceInfo><OutputFormat>TIFF</OutputFormat><ColorDepth>32</ColorDepth><PageHeight>11in</PageHeight><PageWidth>8.5in</PageWidth><MarginBottom>0.44in</MarginBottom></DeviceInfo>";
                }
                else
                {   //saving report to PDF file
                    deviceInfo = "<DeviceInfo></DeviceInfo>";
                }

                //execute report and get page count
                try
                {
                    //renders the first page of the report and returns stream IDs for subsequent pages
                    firstPage = rs.Render(reportPath, format, null, deviceInfo, Params, null, null,
                        out encoding, out mimeType, out reportHistoryParameters, out warnings, out streamIds);

                    if (reportName != "")
                    {   //write the document to PDF file
                        FileStream fs = new FileStream(reportName + "-1.pdf", FileMode.Create);
                        fs.Write(firstPage, 0, firstPage.Length);
                        fs.Close();
                    }

                    //total number of pages is 1 + number of stream IDs
                    m_numberOfPages = streamIds.Length + 1;
                    //pages = new Byte[m_numberOfPages-1][] { };
                    pages = new Byte[][] { };

                    //first page has been rendered
                    pages[0] = firstPage;
                    for (int pageIndex = 1; pageIndex < m_numberOfPages; pageIndex++)
                    {
                        if (reportName == "")
                        {   //print report as image
                            deviceInfo = "<DeviceInfo><OutputFormat>TIFF</OutputFormat><ColorDepth>32</ColorDepth><PageHeight>11in</PageHeight><PageWidth>8.5in</PageWidth><MarginBottom>0.44in</MarginBottom><StartPage>" + pageIndex + 1 + "</StartPage></DeviceInfo>";
                        }

                        pages[pageIndex] = rs.Render(reportPath, format, null, deviceInfo, Params, null, null,
                        out encoding, out mimeType, out reportHistoryParameters, out warnings, out streamIds);

                        if (reportName != "")
                        {   //write the document to PDF file
                            FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("DraftPrints\\") + reportName + "-" + pageIndex + 1 + ".pdf", FileMode.Create);
                            fs.Write(pages[pageIndex], 0, pages[pageIndex].Length);
                            fs.Close();
                        }
                    }
                }
                catch (SoapException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return pages;
            }

            public bool PrintReport(string reportURL, string printerName, Hashtable reportParameters)
            {
                this.RenderedReport = this.RenderReport(reportURL, reportParameters);

                try
                {   //wait for the report to completely render
                    if (m_numberOfPages < 1)
                        return false;

                    PrinterSettings printerSettings = new PrinterSettings();
                    printerSettings.MaximumPage = m_numberOfPages;
                    printerSettings.MinimumPage = 1;
                    printerSettings.PrintRange = PrintRange.SomePages;
                    printerSettings.FromPage = 1;
                    printerSettings.ToPage = m_numberOfPages;
                    printerSettings.PrinterName = printerName;
                    printerSettings.PrintToFile = true;

                    PrintDocument pd = new PrintDocument();
                    m_currentPrintingPage = 1;
                    m_lastPrintingPage = m_numberOfPages;
                    pd.PrinterSettings = printerSettings;
                    pd.DefaultPageSettings.Landscape = false;   //always print in portrait

                    pd.PrintPage += pd_PrintPage;
                    pd.Print();
                }
                catch (Exception ex)
                {
                    throw ex; 
                    //MessageBox.Show(ex.ToString());
                }
                return true;
            }

            void pd_PrintPage(object sender, PrintPageEventArgs e)
            {
                e.HasMorePages = false;
                if ((m_currentPrintingPage <= m_lastPrintingPage) && MoveToPage(m_currentPrintingPage))
                {
                    ReportDrawPage(e.Graphics);     //draw page
                    if (System.Threading.Interlocked.Increment(ref m_currentPrintingPage) <= m_lastPrintingPage)
                        e.HasMorePages = true;      //print another page if there are more pages
                }
            }

            private bool MoveToPage(int m_currentPrintingPage)
            {
                if (this.RenderedReport[m_currentPrintingPage - 1] == null)
                    return false;       //current page doesn't exist in array list

                m_currentPageStream = new MemoryStream(this.RenderedReport[m_currentPrintingPage - 1]);     //set current page stream to the rendered page
                m_currentPageStream.Position = 0;   //set position to start

                if (m_docImage != null)
                {
                    m_docImage.Dispose();
                    m_docImage = null;
                }
                m_docImage = System.Drawing.Image.FromStream(m_currentPageStream);
                return true;
            }

            private void ReportDrawPage(Graphics graphics)
            {
                if ((m_currentPageStream == null) || (m_currentPageStream.Length == 0) || (m_docImage == null))
                    return;
                lock (this)
                {
                    System.Drawing.Point destPoint = new System.Drawing.Point(0, 0);
                    graphics.DrawImage(m_docImage, destPoint);
                }
            }

            public Byte[][] RenderedReport
            {
                get { return m_renderedReport; }
                set { m_renderedReport = value; }
            }



        }

        public static bool PrintDraft(string draft_id, Byte[] rendered_report)
        {
            string pdfFileName = HttpContext.Current.Server.MapPath("DraftPrints\\") + "Draft" + draft_id + ".pdf";
            File.WriteAllBytes(pdfFileName, rendered_report);
            bool printDraft = false;

            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = mm.draftPrinterKC;
            ps.DefaultPageSettings.Margins.Bottom = 0;  //testing - doesn't adjust
            
                        
            int PrintLocation = MedCsXSystem.Settings.DraftPrintingLocation;

            switch (PrintLocation)
            {
                case 1:  //User Default  Print to the users default print location
                    int pid = User.DefaultPrinterForUser(Convert.ToInt32(HttpContext.Current.Session["userID"]));

                    if (pid == 1)  //corp
                        ps.PrinterName = mm.draftPrinterKC;
                    else if (pid == 2)  //lv
                        ps.PrinterName = mm.draftPrinterLV;
                    else  //corp
                        ps.PrinterName = mm.draftPrinterKC;
                    break;
                case 2:  //Corporte
                    ps.PrinterName = mm.draftPrinterKC;
                    break;
                case 3:  //las Vegas
                    ps.PrinterName = mm.draftPrinterLV;
                    break;
               default:
                    ps.PrinterName = mm.draftPrinterKC;
                    break;

            }

            PdfPrint pdfPrint = new PdfPrint("Med James, Inc", "g/4JFMjn6KuJU6poHlm/Jhoj99HtdbmaxsQP3/y31LL6ZgDF+AS6ibql/wfepKxZ7GOU1be1TLhmsfUsY77NBjs9ImJjHPsgg/4JFMjn6Ks8SFhkXZQn3Q==");
              
                if (pdfPrint.Print(rendered_report, ps) == PdfPrint.Status.OK)
                    printDraft = true;

            return printDraft;
           
        }

        public static bool PrintDraftQueue(string draft_id, Byte[] rendered_report, bool overlimit, string drftprinter = "")
        {
            string pdfFileName = HttpContext.Current.Server.MapPath("DraftPrints\\") + "Draft" + draft_id + ".pdf";
            File.WriteAllBytes(pdfFileName, rendered_report);
            bool printDraft = false;

            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = mm.draftPrinterKC;
            ps.DefaultPageSettings.Margins.Bottom = 0;  

            //Draft mdp = new Draft(Convert.ToInt32(draft_id));


            int PrintLocation = MedCsXSystem.Settings.DraftPrintingLocation;

           


            switch (PrintLocation)
            {
                case 1:  //User Default  Print to the users default print location

                    int dpCreator = mm.Get_Draft_Print_Creator(Convert.ToInt32(draft_id));
                    int pid = User.DefaultPrinterForUser(dpCreator);
                    //int pid = userprinter;

                    if (pid == 1)  //corp
                    {
                        ps.PrinterName = mm.draftPrinterKC;
                        if (drftprinter != "") // specific printer requested from from another location
                            ps.PrinterName = drftprinter;
                    }
                    else if (pid == 2)  //lv
                    {
                        ps.PrinterName = mm.draftPrinterLV;
                        if (drftprinter != "") // specific printer requested from from another location
                            ps.PrinterName = drftprinter;
                        if (overlimit)  // over Vegas signing limit
                            ps.PrinterName = mm.draftPrinterKC;
                       
                    }
                    else  //corp
                    {
                        ps.PrinterName = mm.draftPrinterKC;
                        if (drftprinter != "") // Print Queue print to other location
                            ps.PrinterName = drftprinter;
                    }
                    break;
                case 2:  //Corporte
                    ps.PrinterName = mm.draftPrinterKC;
                    break;
                case 3:  //las Vegas
                    ps.PrinterName = mm.draftPrinterLV;
                    break;
                default:
                    ps.PrinterName = mm.draftPrinterKC;
                    break;

            }

            

           


            PdfPrint pdfPrint = new PdfPrint("Med James, Inc", "g/4JFMjn6KuJU6poHlm/Jhoj99HtdbmaxsQP3/y31LL6ZgDF+AS6ibql/wfepKxZ7GOU1be1TLhmsfUsY77NBjs9ImJjHPsgg/4JFMjn6Ks8SFhkXZQn3Q==");

            if (pdfPrint.Print(rendered_report, ps) == PdfPrint.Status.OK)
                printDraft = true;

            return printDraft;

        }

        private static bool impersonateValidUser(String userName, String domain, String password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            if (RevertToSelf())
            {
                if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                {
                    if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                    {
                        tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                        impersonationContext = tempWindowsIdentity.Impersonate();
                        if (impersonationContext != null)
                        {
                            CloseHandle(token);
                            CloseHandle(tokenDuplicate);
                            return true;
                        }
                    }
                }
            }
            if (token != IntPtr.Zero)
                CloseHandle(token);
            if (tokenDuplicate != IntPtr.Zero)
                CloseHandle(tokenDuplicate);
            return false;
        }

        private static void undoImpersonation()
        {
            impersonationContext.Undo();
        }


    }


}

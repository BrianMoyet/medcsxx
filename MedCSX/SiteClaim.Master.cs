﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX
{
    public partial class SiteClaimMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null)
            {
                if (Convert.ToInt32(Session["userID"]) < 1)
                    Response.Redirect("NotLoggedIn.html");
            }
            else
            {
                Response.Redirect("NotLoggedIn.html");
            }
        }
    }
}
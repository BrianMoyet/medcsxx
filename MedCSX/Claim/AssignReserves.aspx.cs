﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using MedCSX.App_Code;

namespace MedCSX.Claim
{
    public partial class AssignReserves : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Choose Properties To Appraise";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            DataTable dt = new DataTable();
            Reserve myData = new Reserve();
            dt = myData.Get_Reserve_Assignments_For_Claim(claim_id);

            grReserves.DataSource = dt;
            grReserves.DataBind();
            grReserves.Visible = true;
            //Person myData = new Person();
            //myData.GetPersonByPersonID(id);
            //if (myData.isCompany == "1")
            //    rblClaimantType.SelectedValue = "1";
            //txtSuffix.Text = myData.suffix;
            //txtLast.Text = myData.last_name;
            //txtMiddleInitial.Text = myData.middle_initial;
            //txtFirstName.Text = myData.first_name;
            //txtAddress1.Text = myData.address1;
            //txtAddress2.Text = myData.address2;
            //txtCity.Text = myData.city;
            //ddlState.SelectedValue = myData.state_id;
            //txtZip.Text = myData.zipcode;
            //txtHomePhone.Text = myData.home_phone;
            //txtWorkPhone.Text = myData.work_phone;
            //txtCellPhone.Text = myData.cell_phone;
            //txtOtherPhone.Text = myData.other_contact_phone;
            //txtFax.Text = myData.fax;
            //txtEmail.Text = myData.email;
            //txtDOB.Text = myData.date_of_birth;
            //txtSSn.Text = myData.ssn;
            //txtDriversLicenseNo.Text = myData.drivers_license_no;
            //if (myData.sex.ToUpper() == "M")
            //    rblSex.SelectedValue = "1";
            //else if (myData.sex.ToUpper() == "F")
            //    rblSex.SelectedValue = "2";
            //txtEmployer.Text = myData.employer;
            //ddlLanguage.SelectedValue = myData.language_id;
            //txtComments.Text = myData.comments;


        }

        protected void PopulateControls()
        {
            //DataTable dtState = new DataTable();
            //MedCSX.App_Code.Claim myDataState = new MedCSX.App_Code.Claim();
            //dtState = myDataState.PopulateDDLStates();
            //ddlState.DataSource = dtState;
            //ddlState.DataValueField = dtState.Columns[0].ColumnName;
            //ddlState.DataTextField = dtState.Columns[1].ColumnName;
            //ddlState.DataBind();

            //ddlDLState.DataSource = dtState;
            //ddlDLState.DataValueField = dtState.Columns[0].ColumnName;
            //ddlDLState.DataTextField = dtState.Columns[1].ColumnName;
            //ddlDLState.DataBind();

            ////ddlOtherState.DataSource = dtState;
            ////ddlOtherState.DataValueField = dtState.Columns[0].ColumnName;
            ////ddlOtherState.DataTextField = dtState.Columns[1].ColumnName;
            ////ddlOtherState.DataBind();


            //DataTable dtPerson = new DataTable();
            //Person myData = new Person();
            //dtPerson = myData.GetLanguages();
            //ddlLanguage.DataSource = dtPerson;
            //ddlLanguage.DataValueField = dtPerson.Columns[0].ColumnName;
            //ddlLanguage.DataTextField = dtPerson.Columns[1].ColumnName;
            //ddlLanguage.DataBind();
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
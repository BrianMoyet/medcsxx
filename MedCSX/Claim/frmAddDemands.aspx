﻿<%@ Page Title="Add Demands" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAddDemands.aspx.cs" Inherits="MedCSX.Claim.frmAddDemands" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
    <div class="col-md-2">
        Attorney:
    </div>
    <div class="col-md-10">
        <asp:DropDownList ID="cboDemandAttorney" runat="server"></asp:DropDownList>
    &nbsp;&nbsp;
        <asp:LinkButton ID="llAddAttorney" runat="server" CausesValidation="False" OnClick="llAddAttorney_Click">Add Attorney</asp:LinkButton>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        Demads:
    </div>
</div>
<div class="row">
    <div class="col-md-12">
       <asp:GridView ID="grDemands" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   ShowFooter="true">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                       
                
                    <asp:BoundField DataField="reserveType" HeaderText="Reserve"/>
                    <asp:BoundField DataField="claimant" HeaderText="Claimant"/>
                 <asp:TemplateField HeaderText="Demand Amount">
                          <ItemTemplate>
                              <asp:TextBox ID="adjCode" runat="server"></asp:TextBox>
                          </ItemTemplate>
                        
                    </asp:TemplateField>
<%--                    <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                              <asp:Button  ID="btnDemandsUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"  data-toggle="modal" data-target="#theModal"  OnClientClick=<%# FormatPopupDemands(Eval("demand_id").ToString(), "u") %>  />
                     
                           
                             <asp:Button  ID="btnDemandsDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete"  data-toggle="modal" data-target="#theModal"  OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDemandsDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>--%>
                

            </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
             <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
    </div>
</div>
   
<div class="row">
    <div class="col-md-2">
       Demand Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="dpDemandDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Received Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="dpReceivedDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Due Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="dpDueDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click"  />
           
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>

</asp:Content>

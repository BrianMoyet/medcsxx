﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCSX.App_Code;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmPendingDraft : System.Web.UI.Page
    {

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        private Reserve _reserve;
        private PendingDraft _pendingDraft;
        private int _transactionTypeId;
        private int _vendorId;
        private bool _userChangedPayee;
        private bool _userChangedMailingAddress;
        private bool _doneLoading;
        private int _reserve_id;
        private int _pendingDraft_id;

        protected void Page_Load(object sender, EventArgs e)
        {

            //need to see if reserve or pending draft id passed from pick or pending drafts
            if (Request.QueryString["pending_draft_id"] != null)
            {
                _pendingDraft_id = Convert.ToInt32(Request.QueryString["pending_draft_id"]);
                _pendingDraft = new PendingDraft(_pendingDraft_id);
                _reserve = _pendingDraft.Reserve;

                if (_pendingDraft.TransactionType.isLoss)
                    rbLoss.Checked = true;
                else
                    rbExpense.Checked = true;

                txtAddress1.Text = _pendingDraft.Draft.Address1;
                txtAddress2.Text = _pendingDraft.Draft.Address2;
                txtCity.Text = _pendingDraft.Draft.City;
                txtExplanation.Text = _pendingDraft.Draft.ExplanationOfProceeds;
                txtMailToName.Text = _pendingDraft.Draft.MailToName;
                txtPayee.Text = _pendingDraft.Draft.Payee;
                txtSendTo.Text = _pendingDraft.PendingDraftSentTo;

                if (_pendingDraft.Draft.Vendor().TaxId != "")
                {
                    txtTaxId.Text = _pendingDraft.Draft.Vendor().TaxId;
                    chkClaimantAttorney.Checked = _pendingDraft.Draft.Vendor().isClaimantAttorney;
                }

                txtZip.Text = _pendingDraft.Draft.Zipcode;
                this.cboState.SelectedValue = _pendingDraft.Draft.StateId.ToString();
                cboTransactionType.SelectedValue = _pendingDraft.TransactionTypeId.ToString();

            }
            else
            { 
                _reserve_id = Convert.ToInt32(Request.QueryString["reserve_id"]);
                _reserve = new Reserve(_reserve_id);
            }

            if (!Page.IsPostBack)
            {
                mm.UpdateLastActivityTime();

                DisableControls(false);


                LoadDDLs();
            }
        }

        public void DisableControls(bool status)
        {
            foreach (Control c in Page.Controls)
            {
                foreach (Control ctrl in c.Controls)
                {
                    if (ctrl is TextBox)
                        ((TextBox)ctrl).Enabled = status;
                    else if (ctrl is Button)
                        ((Button)ctrl).Enabled = status;
                    else if (ctrl is RadioButton)
                        ((RadioButton)ctrl).Enabled = status;
                    else if (ctrl is RadioButtonList)
                        ((RadioButtonList)ctrl).Enabled = status;
                    else if (ctrl is ImageButton)
                        ((ImageButton)ctrl).Enabled = status;
                    else if (ctrl is CheckBox)
                        ((CheckBox)ctrl).Enabled = status;
                    else if (ctrl is CheckBoxList)
                        ((CheckBoxList)ctrl).Enabled = status;
                    else if (ctrl is DropDownList)
                        ((DropDownList)ctrl).Enabled = status;
                    else if (ctrl is HyperLink)
                        ((HyperLink)ctrl).Enabled = status;
                }

                btnCancel.Enabled = true;
            }
        }

        protected void LoadDDLs()
        {
          
            mf.LoadStateComboBox(this.cboState);
            LoadTransactionTypes();
            
        }

        private void LoadTransactionTypes()
        {
            string whereClause = "";

            if (_reserve.isOpen)
                whereClause = "Is_Payment_Type = 1 ";
            else
                whereClause += "Is_Supplemental_Payment_Type = 1  ";

            if (rbLoss.Checked)
                whereClause += " and is_loss = 1 ";
            else
                whereClause += " and is_loss = 0 ";
            mf.LoadTypeTableDDL(this.cboTransactionType, "Transaction_Type", true, whereClause);
            cboTransactionType_SelectedIndexChanged(cboTransactionType, null);
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (ValidateDraft() == false)
                return;

            if (_pendingDraft == null)
                CreatePendingDraft();

            string url = "frmPendingDrafts.aspx";
            string win = "window.open('" + url + "','mywindowDraftQue', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=400, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
        }

        private void CreatePendingDraft()
        {
            // insert draft
            Draft d = new Draft();
            {
                var withBlock = d;
                d.Address1 = this.txtAddress1.Text;
                d.Address2 = this.txtAddress2.Text;
                d.BankAccountNo = this._reserve.Claim.CompanyLocation().Company.BankAccountNo;
                d.BankRoutingNo = this._reserve.Claim.CompanyLocation().Company.BankRoutingNo;
                d.City = this.txtCity.Text;
                d.DraftNo = _reserve.Claim.NextDraftNo;
                d.ExplanationOfProceeds = this.txtExplanation.Text;
                d.MailToName = this.txtMailToName.Text;
                d.Payee = this.txtPayee.Text;
                d.PrintDate = DateTime.Now;
                d.StateId = mm.GetStateId(this.cboState.SelectedValue); 
                if (txtTaxId.Text.Trim().Length > 0)
                    d.VendorId = Vendor.getVendorIdForTaxId(this.txtTaxId.Text);
                d.Zipcode = this.txtZip.Text;
                d.Update();
            }

            // insert draft_print row
            DraftPrint dp = d.InsertDraftPrintForPending(this._reserve);
            // Save pending draft
            PendingDraft pd = new PendingDraft();
            {
                var withBlock = pd;
                withBlock.ReserveId = this._reserve.Id;
                withBlock.DraftId = d.Id;
                withBlock.DraftPrintId = dp.Id;
                withBlock.PendingDraftStatusId = 1;
                withBlock.PendingDraftSentTo = this.txtSendTo.Text;
                withBlock.TransactionTypeId = Convert.ToInt32(cboTransactionType.SelectedValue);
                withBlock.Update();
            }

            Dictionary<string, string> rptParms = new Dictionary<string, string>();
            rptParms.Add("Draft_Id", dp.Id.ToString());

            
            ShowReport("Draft", rptParms);

        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            //rptParms = 
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }


        protected void rbLoss_CheckedChanged(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            LoadTransactionTypes();
             
        }

        private bool ValidateDraft()
        {
            if (txtTaxId.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a Tax Id.');</script>");
                //MessageBox.Show("Please enter a Tax Id.", "Tax id is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtTaxId.Focus();
                return false;
            }

            if (txtPayee.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a payee.');</script>");
                //MessageBox.Show("Please enter a payee.", "Payee is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtPayee.Focus();
                return false;
            }

            if (txtExplanation.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('lease enter an Explanation.');</script>");
                //MessageBox.Show("Please enter an Explanation.", "Explanation is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtExplanation.Focus();
                return false;
            }

            if (txtMailToName.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a Mail To Name.');</script>");
                //MessageBox.Show("Please enter a Mail To Name.", "Mail To Name is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtMailToName.Focus();
                return false;
            }

            if (txtAddress1.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter Address 1.');</script>");
                //MessageBox.Show("Please enter Address 1.", "Address 1 is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtAddress1.Focus();
                return false;
            }

            if (txtCity.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a City.');</script>");
                //MessageBox.Show("Please enter a City.", "City is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtCity.Focus();
                return false;
            }

            if (txtZip.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a Zipcode.');</script>");
                //MessageBox.Show("Please enter a Zipcode.", "Zipcode is Required", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                txtZip.Focus();
                return false;
            }



            return true;
        }

        protected void txtTaxId_TextChanged(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            string taxId = txtTaxId.Text.Trim();

            if (taxId == "")
                return;
            _vendorId = Vendor.getVendorIdForTaxId(taxId);

            if (_vendorId == 0)
                return;
           

            PopulateVendorControls();
        }

        protected void PopulateVendorControls()
        {
            Vendor vendor = new Vendor(_vendorId);

            if (_userChangedPayee == false)
                txtPayee.Text = vendor.Name;

            if (_userChangedMailingAddress == false)
            {
                this.txtMailToName.Text = vendor.Name;
                this.txtAddress1.Text = vendor.Address1;
                this.txtAddress2.Text = vendor.Address2;
                this.txtCity.Text = vendor.City.Replace(",", ""); // remove trailing commas on some vendor cities
                this.txtZip.Text = vendor.Zipcode;
                this.cboState.SelectedValue = mm.GetStateForId(vendor.StateId);
                    
            }

        }

        protected void btnPendVendorLookup_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
        }

        protected void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableControls(true);

            _transactionTypeId = Convert.ToInt32(cboTransactionType.SelectedValue);

            if ((_transactionTypeId == (int)modGeneratedEnums.TransactionType.Partial_Loss_Payment)
                || (_transactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment)
                || (_transactionTypeId == (int)modGeneratedEnums.TransactionType.Supplemental_Loss_Payment))
            {
                chkClaimantAttorney.Visible = true;
                chkClaimantAttorney.Checked = true;
            }
            else
            {
                chkClaimantAttorney.Visible = false;
            }
        }

        protected void txtPayee_TextChanged(object sender, EventArgs e)
        {
            _userChangedMailingAddress = true;
        }

        protected void txtMailToName_TextChanged(object sender, EventArgs e)
        {
            _userChangedMailingAddress = true;
        }

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Session["refererVendor"] = Request.Url.ToString();

            string lookupValue = txtVendorSearch.Text;
            if (lookupValue == "")
                return;
            //show vendor search results
            string url = "frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
            string s2 = "window.open('" + url + "', 'popup_window4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAssignReserve : System.Web.UI.Page
    {
        public int selectedUserId = 0,
          selectedUserGroupId = 0;
        public bool okPressed = false;

        private Reserve m_reserve;
        private List<cboUserList> myUserList;
        private List<cboUserList> myUserGroupList;

        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();

        private string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            referer = Session["referer"].ToString();
            this.m_reserve = new Reserve(Convert.ToInt32(Request.QueryString["reserve_id"]));
            this.txtClaimId.Text = m_reserve.Claim.DisplayClaimId;
            this.txtReserveId.Text = m_reserve.Description();

            if (!Page.IsPostBack)
            {
                LoadAssignTo();
                this.cboAssignUser.Focus();
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            this.selectedUserId = Convert.ToInt32(this.cboAssignUser.SelectedValue);
            //this.selectedUserGroupId = Convert.ToInt32(this.cboAssignGroup.SelectedValue);
            this.selectedUserGroupId = 0;

            if ((this.cboAssignUser.SelectedIndex == 0) && (this.cboAssignGroup.SelectedIndex == 0))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Reserve is not Assigned.  You Must Choose Someone to Assign the Reserve.');</script>");
                //MessageBox.Show("The Reserve is not Assigned.  You Must Choose Someone to Assign the Reserve", "Reserve Not Assigned", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.cboAssignUser.Focus();
                return;
            }

            if (this.m_reserve != null)
            {
                if (((this.selectedUserId != 0) && (this.m_reserve.AdjusterId == this.selectedUserId)) || ((this.selectedUserGroupId != 0) && (this.m_reserve.AssignedToUserGroupId == this.selectedUserGroupId)))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This Reserve is Already Assigned to " + this.m_reserve.AssignedToDescription() +"');</script>");
                    //MessageBox.Show("This Reserve is Already Assigned to " + this.m_reserve.AssignedToDescription(), "Already Assigned", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.cboAssignUser.Focus();
                    return;
                }
                this.m_reserve.AssignTo(this.selectedUserId, this.selectedUserGroupId);
            }


            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
        }

        private void LoadAssignTo()
        {
            this.cboAssignUser.DataSource = LoadUsers(mm.GetActiveUsers());
            this.cboAssignUser.DataValueField = "id";
            this.cboAssignUser.DataTextField = "description";
            this.cboAssignUser.DataBind();
            this.cboAssignUser.SelectedIndex = 0;

            this.cboAssignGroup.DataSource = LoadUserGrp(mm.GetActiveUsers(1));
            this.cboAssignUser.DataValueField = "id";
            this.cboAssignUser.DataTextField = "description";
            this.cboAssignUser.DataBind();
            this.cboAssignUser.SelectedIndex = 0;
        }

        private IEnumerable LoadUserGrp(List<Hashtable> list)
        {
            myUserGroupList = new List<cboUserList>();
            myUserGroupList.Add(new cboUserList() { id = 0, description = "" });
            try
            {
                foreach (Hashtable uData in list)
                {
                    cboUserList cbo = new cboUserList();
                    cbo.id = (int)uData["User_Group_Id"];
                    cbo.imageIdx = (int)uData["Image_Index"];
                    cbo.description = (string)uData["Description"];
                    myUserGroupList.Add(cbo);
                }
                return myUserGroupList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myUserGroupList;
            }
        }

        private IEnumerable LoadUsers(List<Hashtable> list)
        {
            myUserList = new List<cboUserList>();
            myUserList.Add(new cboUserList() { id = 0, description = "" });
            try
            {
                foreach (Hashtable uData in list)
                {
                    cboUserList cbo = new cboUserList();
                    cbo.id = (int)uData["User_Id"];
                    cbo.description = (string)uData["Name"];
                    myUserList.Add(cbo);
                }
                return myUserList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myUserList;
            }
        }

        class cboUserList
        {
            public int id { get; set; }
            public int imageIdx { get; set; }
            public string description { get; set; }
        }
    }
}
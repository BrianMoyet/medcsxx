﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Forms;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmRecoverDeductibles : System.Web.UI.Page
    {
        public int reserveId;
        public int transactionId; //'Transaction Id of Transaction row inserted.
        private Reserve MyReserve; //'The reserve object passed to this class
        private string referer = "";
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();

        protected void Page_Load(object sender, EventArgs e)
        {
            referer = Session["referer"].ToString();

            reserveId = Convert.ToInt32(Request.QueryString["reserve_id"]);
            MyReserve = new Reserve(reserveId);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            reserveId = Convert.ToInt32(Request.QueryString["reserve_id"]);
            MyReserve = new Reserve(reserveId);

            try
            {
                //    //'Update the last activity time
                mm.UpdateLastActivityTime();

                //'Make sure user entered correct values
                if (txtAmount.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recovered Amount is a required field.');</script>");
                    //System.Windows.MessageBox.Show("Recovered Amount is a required field.", "Recovery AMount Not Entered", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    txtAmount.Focus();
                    return;
                }
                if (modLogic.isNumeric(txtAmount.Text) == false)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recovered Amount Must Be Numeric.');</script>");
                    //System.Windows.MessageBox.Show("Recovered Amount Must Be Numeric.");
                    txtAmount.Focus();
                    return;
                }
                if (txtAmount.Text.Trim() == "0")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recovered Amount Must Not Be Zero.');</script>");
                    //System.Windows.MessageBox.Show("Recovered Amount Must Not Be Zero.");
                    txtAmount.Focus();
                    return;
                }
                if (txtRecoverySource.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The recovery source is a required field.');</script>");
                    //System.Windows.MessageBox.Show("The recovery source is a required field.");
                    txtRecoverySource.Focus();
                    return;
                }
                if (txtCheckNo.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The check number is a required field.');</script>");
                    //System.Windows.MessageBox.Show("The check number is a required field.");
                    txtCheckNo.Focus();
                    return;
                }
                if (modLogic.isNumeric(txtCheckNo.Text.Trim()) == false)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The check number is invalid.');</script>");
                    //System.Windows.MessageBox.Show("The check number is invalid.");
                    txtCheckNo.Focus();
                    return;
                }
                if (txtCheckNo.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The check number cannot be zero.');</script>");
                    //System.Windows.MessageBox.Show("The check number cannot be zero.");
                    txtCheckNo.Focus();
                    return;
                }

                if (MyReserve.LossAmount < Convert.ToDouble(txtAmount.Text.Trim()))
                {
                    //'The loss amount is less than the recovered amount.  Verify that this is OK.

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Warning - Recovered Amount is Greater than the Total Loss Amount for this Reserve Line.');</script>");
                    //return;
                    //if (System.Windows.MessageBox.Show("Warning - Recovered Amount is Greater than the Total Loss Amount for this Reserve Line.Are You Sure You Want to Proceed ? ", "Recovered Greater than Loss", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                    //{
                    //    txtAmount.Focus();
                    //    return;
                    //}
                }

                //'Create a new deductible recovery
                this.transactionId = MyReserve.InsertDeductibles(txtRecoverySource.Text.Trim(), long.Parse(txtCheckNo.Text.Trim()), double.Parse(txtAmount.Text.Trim()));

                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        
        


              
        }
    }
}
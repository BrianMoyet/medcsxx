﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmReopenReserve : System.Web.UI.Page
    {

        private int reserveId,              //reserve ID to create transaction for.  Input parameter
                                            //salvageId,                      //salvage_Id of salvage row inserted
                                            //subroId,                        //subro_Id of subro row inserted
         reissueFromId,                  //Id to reissue from 
         m_mode,                         //which screen to present? Subro or Salvage
         reserveTypeId = 0,            //Policy_Coverage_Type_Id
         claim_id = 0;

        private Reserve m_reserve;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        private ReserveType m_reserveType;
        private MedCsxLogic.Claim m_Claim;

        private string voidReasonTable,
           msg = string.Empty,
           reasonPart = string.Empty,
           noLockPart = ". Proceeding Will Place the Reserve in a Locked State Until Approved By Your Supervisor.",
           lockedPart = ". this would normally add a lock to the reserve, but since it's already locked, the system will just create a note instead.";

        private List<Tuple<int, Hashtable>> eventTypeIds = new List<Tuple<int, Hashtable>>();

        protected void Page_Load(object sender, EventArgs e)
        {
           

            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            m_reserve = new Reserve(this.reserveId);
            this.m_mode = Convert.ToInt32(Request.QueryString["mode"].ToString());

            this.Page.Title = "Claim " + m_reserve.Claim.DisplayClaimId + " - Reopen " + Reserve.ReserveDescription(ref this.reserveId);

            mm.UpdateLastActivityTime();

            //if (!Page.IsPostBack)
            //{
            if (!Page.IsPostBack)
            { 
                LoadReopen();
            }
            //if (this.voidReasonTable == "Draft_Void_Reason")

            //else if (this.voidReasonTable == "Subro_Salvage_Void_Reason")
            //    this.cboVoidReason.SelectedIndex = setSelection((int)modGeneratedEnums.SubroSalvageVoidReason.NSF_Stop_Payment);
            //}
        }

        protected void txtChangeLoss_TextChanged(object sender, EventArgs e)
        {
            string txtLoss = ((TextBox)sender).Text;
            double dloss;
            if (txtLoss.Trim() == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Reserve Amount Must Be Entered.');</script>");
                //System.Windows.MessageBox.Show("Loss Reserve Amount Must Be Entered", "Loss Reserve Amount Must Be Entered", System.Windows.MessageBoxButton.OK,
                //   System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                //Response.Write("<script>alert('Loss Reserve Amount Must Be Entered');</script>");
                this.txtChangeLoss.Focus();
                return;
            }

            if (!modLogic.isNumeric(txtLoss.Trim()))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Reserve Amount Must Be Numeric.');</script>");
                //System.Windows.MessageBox.Show("Loss Reserve Amount Must Be Numeric", "Loss Reserve Amount Must Be Numeric", System.Windows.MessageBoxButton.OK,
                //  System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                //Response.Write("<script>alert('Loss Reserve Amount Must Be Numeric');</script>");

                this.txtChangeLoss.Focus();
                return;
            }

            dloss = double.Parse(txtLoss.Trim().Replace("$", "").Replace(",", ""));
            if (dloss == 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Reserve Must Not be Zero.');</script>");
                //System.Windows.MessageBox.Show("Loss Reserve Must Not be Zero", "Loss Reserve Must Not be Zero", System.Windows.MessageBoxButton.OK,
                // System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                //Response.Write("<script>alert('Loss Reserve Must Not be Zero');</script>");
                this.txtChangeLoss.Focus();
                return;
            }
            this.txtChangeLoss.Text = dloss.ToString("c");
        }

        protected void txtChangeExpense_TextChanged(object sender, EventArgs e)
        {
            double dexpense;
            string txtExpense = ((TextBox)sender).Text;
            if (txtExpense.Trim() == "")
            {
                dexpense = 0;
            }
            else
            {
                if (!modLogic.isNumeric(txtExpense.Trim()))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Expense Reserve Amount Must Be Numeric.');</script>");
                    //   System.Windows.MessageBox.Show("Expense Reserve Amount Must Be Numeric", "Expense Reserve Amount Must Be Numeric", System.Windows.MessageBoxButton.OK,
                    //System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    //Response.Write("<script>alert('Expense Reserve Amount Must Be Numeric');</script>");
                    this.txtChangeExpense.Focus();
                    return;
                }
                dexpense = double.Parse(txtExpense.Trim().Replace("$", "").Replace(",", ""));
            }
            this.txtChangeExpense.Text = dexpense.ToString("c");
        }

        private void LoadReopen()
        {
            reserveTypeId = m_reserve.ReserveTypeId;
            m_reserveType = new ReserveType(reserveTypeId);

            this.txtFinancialAuthority.Text = m_reserve.ReserveType.UserAuthority().ToString("c");
            this.claim_id = m_reserve.ClaimId;
            this.m_Claim = new MedCsxLogic.Claim(this.claim_id);
        }

        
        protected void btnsOK_Click(object sender, EventArgs e)
        {
            this.m_Claim = new MedCsxLogic.Claim(m_reserve.ClaimId);

            this.btnsOK.Enabled = false;
            //try
            //{
                mm.UpdateLastActivityTime();

                if (ReopenValidation())
                {
                    double expense = 0;
                    double loss = double.Parse(this.txtChangeLoss.Text.Trim().Replace("$", "").Replace(",", ""));
                    if (this.txtChangeExpense.Text.Trim() != "")
                        expense = double.Parse(this.txtChangeExpense.Text.Trim().Replace("$", "").Replace(",", ""));

                    m_reserve.Reopen(loss, expense, eventTypeIds, reserveId);

                // ClientScript.RegisterStartupScript(this.GetType(), "close", "parent.location.reload();", true);
                //ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);      //close form
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='viewClaim.aspx?claim_id=" + m_reserve.ClaimId + "&Ptabindex=7';window.close();", true);

            }
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        private bool ReopenValidation()
        {
            msg = "";
            double expense;
            double loss;
            if (this.txtChangeLoss.Text.Trim() == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Reserve Amount Must Be Entered.');</script>");
                //System.Windows.MessageBox.Show("Loss Reserve Amount Must Be Entered" +
                //      "It Must be Reopened to allow Payments.", "Loss Reserve Not Entered",
                //      System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                //Response.Write("<script>alert('Loss Reserve Amount Must Be Entered');</script>");
                
                this.txtChangeLoss.Focus();
                return false;
            }

            if (!modLogic.isNumeric(this.txtChangeLoss.Text.Trim()))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Reserve Amount Must Be Numeric.');</script>");
                //System.Windows.MessageBox.Show("Loss Reserve Amount Must Be Numeric", "Loss Reserve must be numeric",
                //     System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                //Response.Write("<script>alert('Loss Reserve Amount Must Be Numeric');</script>");
                this.txtChangeLoss.Focus();
                return false;
            }

            loss = double.Parse(this.txtChangeLoss.Text.Trim().Replace("$", "").Replace(",", ""));
            if (loss == 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Reserve Must Not be Zero.');</script>");
                //System.Windows.MessageBox.Show("Loss Reserve Must Not be Zero", "Loss Reserve Must Not be Zero",
                //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                //Response.Write("<script>alert('Loss Reserve Must Not be Zero');</script>");

                this.txtChangeLoss.Focus();
                return false;
            }

            if (this.txtChangeExpense.Text.Trim() == "")
            {
                expense = 0;
            }
            else
            {
                if (!modLogic.isNumeric(this.txtChangeExpense.Text.Trim()))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Expense Reserve Amount Must Be Numeric.');</script>");
                    // System.Windows.MessageBox.Show("Expense Reserve Amount Must Be Numeric", "Expense Reserve Amount Must Be Numeric",
                    //System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    //Response.Write("<script>alert('Expense Reserve Amount Must Be Numeric');</script>");

                    this.txtChangeExpense.Focus();
                    return false;
                }
                expense = double.Parse(this.txtChangeExpense.Text.Trim().Replace("$", "").Replace(",", ""));
            }

            double perPerson = m_reserve.PerPersonCoverage;
            double perAccident = m_reserve.PerAccidentCoverage;
            bool deductible = m_reserve.hasDeductible;

            //amount exceeding authority
            if (loss + expense > m_reserve.ReserveType.UserAuthority())
            {
                msg = "";
                reasonPart = "Amounts Entered Exceeds Your Authority";
                msg = reasonPart + noLockPart;

                if (m_reserve.isLocked)
                    msg = reasonPart + lockedPart;

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                //if (System.Windows.MessageBox.Show(msg, "Exceeds Authority", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                //return false;

                eventTypeIds.Add(new Tuple<int, Hashtable>((int)modGeneratedEnums.EventType.Loss_Reserve_Changed_Above_Adjuster_Authority, null));
               

            }

            if (m_reserve.Claim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Auto)
            {

                if (!deductible)
                {
                    msg = "";
                    //TODO
                    if ((loss > 0) && (perPerson == 0) && (perAccident == 0))
                    {   //no coverage
                        reasonPart = "There is No Coverage for this Reserve";
                        msg = reasonPart + noLockPart;

                        if (m_reserve.isLocked)
                            msg = reasonPart + lockedPart;

                        //if (MessageBox.Show(msg, "No Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                        //    return false;
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                        eventTypeIds.Add(new Tuple<int, Hashtable>((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, null));




                    }

                    if ((perPerson > 0) && (loss > perPerson))
                    {   //loss exceeds per-person coverage
                        msg = "";
                        reasonPart = "Loss Amount Enterd Exceed Per-Person Coverage of " + perPerson.ToString("c");
                        msg = reasonPart + noLockPart;

                        if (m_reserve.isLocked)
                            msg = reasonPart + lockedPart;

                        //if (MessageBox.Show(msg, "Exceeds Per-Person Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                        //    return false;
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");


                        eventTypeIds.Add(new Tuple<int, Hashtable>((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, null));
                    }
                    else if ((perAccident > 0) && (loss + this.m_Claim.NetLossReserveForCoverageType(m_reserve.ReserveTypeId) > perAccident))
                    {   //loss exceeds per-accident coverage
                        msg = "";
                        reasonPart = "Loss Amount Entered Will Cause the Reserve to Exceed Per-Accident Coverage of " + perAccident.ToString("c");
                        msg = reasonPart + noLockPart;

                        if (m_reserve.isLocked)
                            msg = reasonPart + lockedPart;

                        //if (MessageBox.Show(msg, "No Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Time of loss is a required entry.');</script>");
                        return false;


                        eventTypeIds.Add(new Tuple<int, Hashtable>((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, null));
                    }
                }
            }
            else if (m_reserve.Claim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Commercial)
            {
               


            }

            return true;
        }
    }
}
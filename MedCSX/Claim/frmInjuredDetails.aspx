﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master" CodeBehind="frmInjuredDetails.aspx.cs" Inherits="MedCSX.Claim.frmInjuredDetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <style>
        .hiddencol
      {
      display: none;
      }
   </style>
    <div class="row">
        <div class="col-md-7">
            <b>Injured Person</b><br />
            <asp:DropDownList ID="cboPersonSelect" runat="server" OnSelectedIndexChanged="cboPersonSelect_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
             <asp:Button  ID="btnPersonNew" runat="server" class="btn btn-info btn-xs"  Text="A"   CausesValidation="False" OnClick="btnPersonNew_Click"  OnClientClick="return confirm('Information not saved, will be LOST.');"  />
                <asp:Button ID="btnPersonEdit" runat="server" CausesValidation="False" class="btn btn-info btn-xs"  Text="U" OnClick="btnPersonEdit_Click"  OnClientClick="return confirm('Information not saved, will be LOST.');"  />
            <br />

            <asp:Label ID="txtPersonDetails" runat="server" Width="150px" Height="110px"></asp:Label> 
        </div>
        <div class="col-md-5">
            <b>Liens</b><br />
             <asp:GridView ID="grLiens" runat="server" AutoGenerateSelectButton="true" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="lienId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   AllowPaging="True" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    
                    <Columns>
                      
                        <asp:BoundField DataField="vendorName" HeaderText="Vendor"  SortExpression=""  />
                        <asp:BoundField DataField="vendorDescription" HeaderText="Type"  SortExpression=""  />
                        <asp:BoundField DataField="lienAmount" HeaderText="Amount"  SortExpression="" DataFormatString="{0:c}"  />
                        <asp:BoundField DataField="vendorId" HeaderText=""  ItemStyle-CssClass="hiddencol" />
                      
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                 <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                  <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
           
          
        </div>
         <div class="col-md-5">
             <asp:LinkButton ID="btnLienAdd" runat="server"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnLienAdd_Click">Add</asp:LinkButton>
             &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnLienEdit" runat="server"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnLienEdit_Click">Edit</asp:LinkButton>
             &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnLienDelete" runat="server" class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnLienDelete_Click">Delete</asp:LinkButton>
            <br /><br />
             <p><asp:Button ID="btnLienEval" runat="server" Text="BI Evaluation"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnLienEval_Click" /></p>
         </div>
    </div>
   <div class="row">
        <div class="col-md-7">
            <b>Reserves</b><br />
              <asp:GridView ID="grReserves" runat="server" AutoGenerateColumns="False" CellPadding="4"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333" width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>      
                      <%--  <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                            <asp:Button  ID="btnReserveUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="View"    OnClientClick=<%# FormatPopupReserve(Eval("Reserve_id").ToString(), "r") %>  /> 
                                
                            </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                                   <FooterTemplate>
                                      <asp:Button  ID="btnReserveAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"    OnClientClick=<%# FormatPopupReserve("0", "c") %>  /> 
                                </FooterTemplate>
                            </asp:TemplateField>--%>
                      <%--  <asp:BoundField DataField="reserve_id" HeaderText="Reserve"  SortExpression="reserve_id"  />
                        <asp:BoundField DataField="adjuster_id" HeaderText="Assigned To"  SortExpression="adjuster_id"  />--%>
                        <asp:BoundField DataField="coverageType" HeaderText="Reserve"  />
                        <asp:BoundField DataField="assignedUser" HeaderText="Assigned To"  />
                       <%-- <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                              <asp:Button  ID="btnReserveUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"    OnClientClick=<%# FormatPopupReserve(Eval("Reserve_id").ToString(), "u") %>  />
                     
                           
                             <asp:Button  ID="btnReserveDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnReserveDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>--%>
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                     <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
              <div class="row">
                  <div class="col-md-12">
                      <asp:Button ID="btnUpdateReserves"  class="btn btn-info btn-xs"  CausesValidation="false" OnClick="btnUpdateReserves_Click"  Visible="false" runat="server" Text="Update Reserve" />  <asp:DropDownList ID="cboUser" runat="server" Visible="false" Enabled="false"></asp:DropDownList><asp:DropDownList ID="cboUserGroup" runat="server" Visible="false" Enabled="false"></asp:DropDownList>
                </div>
            </div>
           
        </div>
            <div class="col-md-5">
                <div id="gbInjuryDetails" runat="server">
                <b>Injury Details</b><br />
                Injury Cause: 
                <asp:DropDownList ID="cboInjuryCause" runat="server">
                </asp:DropDownList>
                <br />
                Injury Type:
                <asp:DropDownList ID="cboInjuryType" runat="server">   </asp:DropDownList>
                <br />
                Injured
                     <div style="width: 100%; height: 100px; overflow: scroll">
                 <asp:GridView ID="grInjuryPart" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  AllowPaging="False" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                   
                     <Columns>
                          <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>      
                        <asp:BoundField DataField="description" HeaderText="Body Part"  SortExpression=""  />
                     
                       
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
                         </div>
                    <br />
                   </div>
                <p>Extent of Injury</p><br />
                <asp:TextBox ID="txtInjuryDescription" runat="server" TextMode="MultiLine" Width="100%" Height="75px"></asp:TextBox>
        </div>  
   </div>
    <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnInjuredOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnInjuredOK_Click"/>
               <asp:Button ID="btnInjuredCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>
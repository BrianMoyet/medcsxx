﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/SiteModal.Master"  CodeBehind="task.aspx.cs" Inherits="MedCSX.Claim.task" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="row">
      <div class="col-md-1">
            Task Type:
      </div>
      <div class="col-md-11">
         <asp:DropDownList ID="ddlTaskType" runat="server"></asp:DropDownList>
      </div>
</div>
     <div class="row">
      <div class="col-md-1">
            Description:
      </div>
      <div class="col-md-11">
          <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="125px" Width="329px"></asp:TextBox>
      </div>
</div>
       <div class="row">
      <div class="col-md-1">
            Claim:
      </div>
      <div class="col-md-11">
          <asp:TextBox ID="txtClaim" runat="server"></asp:TextBox>
      </div>
</div>
       <div class="row">
      <div class="col-md-1">
            Claimant:
      </div>
      <div class="col-md-11">
         <asp:DropDownList ID="ddlClaimant" runat="server"></asp:DropDownList>
        
      </div>
</div>
       <div class="row">
      <div class="col-md-1">
            Reserve:
      </div>
      <div class="col-md-11">
         <asp:DropDownList ID="ddlReserve" runat="server"></asp:DropDownList>
      </div>
</div>
    <div class="row">
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
        </div>
</asp:Content>

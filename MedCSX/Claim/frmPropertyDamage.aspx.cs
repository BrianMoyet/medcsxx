﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmPropertyDamage : System.Web.UI.Page
    {

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        #region Class Variables
        public bool okPressed = false,
            ViewOnly = false;
        public int propertyDamageId = 0;

        private int claimId = 0,
            vehicleId = 0,
            ownerId = 0,
            driverId = 0,
            propertyTypeId = 0,
            selectedUserId = 0,
            selectedUserGroupId = 0;
        private bool doneLoading = true;
        private MedCsxLogic.Claim myClaim = null;
        private PropertyDamage propertyDamage = null;
        private string mode = "";

        private string[] myAssignedUsers;
        private string[] myAssignedUserGroups;
        private List<ClaimantGrid> myDriverList;
        private List<ClaimantGrid> myOwnerList;
        private List<VehicleGrid> myVehicleList;
        private string referer = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["referer2"] != null)
            {
                referer = Session["referer2"].ToString();
                Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();

            this.claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "")); 
            this.propertyDamageId = Convert.ToInt32(Request.QueryString["property_damage_id"]);
            mode = Request.QueryString["mode"].ToString();
           
            this.ViewOnly = false;
            this.myClaim = new MedCsxLogic.Claim(claimId);
            this.propertyDamage = new PropertyDamage(propertyDamageId);
            //if (this.ViewOnly)
                //SetForViewOnly();
            if (!Page.IsPostBack)
            {
                
                LoadAssignTo();     //load assign to user and group combo boxes

                mf.LoadStateDDL(cboOIState,true);
                this.cboReserveType.Enabled = false;
           

                if (myClaim.ClaimTypeId != (int)modGeneratedEnums.ClaimType.Auto)
                {
                    mf.LoadTypeTableComboBox(this.cboReserveType, "Reserve_Type", true, "Is_Medical_Type = 0 AND Sub_Claim_Type_Id = " + this.myClaim.SubClaimTypeId);

                    if (cboReserveType.Items.Count > 0)
                    {
                        if (propertyDamage.Reserve != null)
                        {
                            if (propertyDamage.Reserve.ReserveTypeId != 0)
                            {
                                cboReserveType.SelectedValue = propertyDamage.Reserve.ReserveTypeId.ToString();
                            }
                            else
                            {
                                cboReserveType.SelectedValue = "0";
                            }
                        }
                        else
                        {
                            cboReserveType.SelectedValue = "0";
                        }
                    }

                }

                if (cboReserveType.Items.Count <= 1)
                    cboReserveType.Enabled = false;
                else
                    cboReserveType.Enabled = true;


                mf.LoadTypeTableDDL(this.cboPropertyType, "Property_Type");
                int cbopropertyIndex = SelectPropertyDamageIdCbo((int)modGeneratedEnums.PropertyType.Auto_Property);
                this.cboPropertyType.SelectedIndex = cbopropertyIndex;

                this.doneLoading = false;
                this.driverId = 0;
                this.ownerId = 0;
                this.vehicleId = 0;

                //set dialog caption
                if (this.propertyDamage.isAdd)
                    this.Title = "Claim " + this.myClaim.DisplayClaimId + " - Add Property Damage";
                else
                {
                    if (ViewOnly)
                        this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - View Property Damage";
                    else
                        this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - Edit Property Damage";
                }

                //if in edit mode, load property damage details
                if (this.propertyDamage.isEdit)
                {
                    this.vehicleId = this.propertyDamage.VehicleId;
                    this.driverId = this.propertyDamage.DriverPersonId;
                    this.ownerId = this.propertyDamage.OwnerPersonId;

                    DisplayPropertyDamage();
                    PopulateAssignedTo();
                }
                else
                    PopulateDefaultAssignedTo();

                RefreshPersonsVehicles();
                this.doneLoading = true;
                updatePersonVehicleButtons();


                if (Convert.ToInt32(Request.QueryString["property_damage_id"]) == 0)
                {
                    this.cboDriver.ClearSelection();
                    this.cboDriver.Items.FindByText("Unknown").Selected = true;   //this.cboDriver.SelectedValue = "Unknown"; 
                    cboOwner_SelectedIndexChanged(null, null);


                    this.cboOwner.ClearSelection();
                    this.cboOwner.Items.FindByText("Unknown").Selected = true;   //this.cboDriver.SelectedValue = "Unknown"; 
                    cboOwner_SelectedIndexChanged(null, null);
                }

<<<<<<< HEAD
                populateFormFromSessionVariables();
=======
<<<<<<< HEAD
                populateFormFromSessionVariables();
=======
                if (Session["cboPropertyType"] != null)
                {
                    this.cboPropertyType.SelectedValue = Session["cboPropertyType"].ToString();
                    this.tbLocation.Text = Session["tbLocation"].ToString();
                    this.tbDescription.Text = Session["tbDescription"].ToString();
                    this.tbEstimate.Text = Session["tbEstimate"].ToString();
                    this.cboVehicle.SelectedValue = Session["cboVehicle"].ToString();
                    this.cboOwner.SelectedValue = Session["cboOwner"].ToString();
                    this.cboDriver.SelectedValue = Session["cboDriver"].ToString();
                    this.tbPropertyDescription.Text = Session["tbPropertyDescription"].ToString();

                    Session["cboPropertyType"] = null;
                    Session["tbLocation"] = null;
                    Session["tbDescription"] = null;
                    Session["tbEstimate"] = null;
                    Session["cboVehicle"] = null;
                    Session["cboOwner"] = null;
                    Session["cboDriver"] = null;
                    Session["tbPropertyDescription"] = null;

                }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                if (Request.QueryString["control"] != null)
                {
                    switch (Request.QueryString["control"].ToString())
                    {
                        case "cboOwner":
                            cboOwner.SelectedValue = Request.QueryString["personid"].ToString();
                            cboOwner_SelectedIndexChanged(null, null);
                            break;
                        case "cboDriver":
                            cboDriver.SelectedValue = Request.QueryString["personid"].ToString();
                            cboDriver_SelectedIndexChanged(null, null);
                            break;
                        case "cboVehicle":
                            cboVehicle.SelectedValue = Request.QueryString["vehicleid"].ToString();
                            cboVehicle_SelectedIndexChanged(null, null);
                            break;
                        default:
                            break;
                    }
                }


               

               
            }

            
        }

        private void SetForViewOnly()
        {
            this.btnDriverEdit.Enabled = false;
            this.btnOwnerEdit.Enabled = false;
            this.btnVehicleEdit.Enabled = false;
            this.btnDriverNew.Enabled = false;
            this.btnOwnerNew.Enabled = false;
            this.btnVehicleNew.Enabled = false;
        }

        private void LoadAssignTo()
        {
            this.cboUser.DataSource = LoadUsers(mm.UsersWhichCanBeAssignedClaims());
            this.cboUser.DataBind();
            this.cboUserGroup.DataSource = LoadUserGroups(mm.UserGroupsWhichCanBeAssignedClaims());
            this.cboUserGroup.DataBind();
        }

        private void PopulateAssignedTo()
        {
            Reserve res = this.propertyDamage.Reserve;

            this.selectedUserGroupId = res.AssignedToUserGroupId;
            this.selectedUserId = res.AdjusterId;

            if (res.AssignedToUserGroupId == 0)
            {
                if (res.AssignedToUser().Id != 0)
                    this.cboUser.SelectedValue = SelectUserIdCbo(res.AssignedToUser().Id).ToString();
            }
            else
                this.cboUserGroup.SelectedValue = SelectUserGroupIdCbo(res.AssignedToUserGroupId).ToString();
        }

        private void PopulateDefaultAssignedTo()
        {
            ReserveType rt = new ReserveType((int)modGeneratedEnums.ReserveType.Property_Damage);

            if (rt.DefaultAssignedUserGroupId != 0)
                this.cboUserGroup.SelectedIndex = SelectUserGroupIdCbo(rt.DefaultAssignedUserGroupId);

            this.selectedUserId = 0;
        }

        private void DisplayPropertyDamage()
        {
            this.tbLocation.Text = propertyDamage.WhereSeen;
            this.tbDescription.Text = propertyDamage.DamageDescription;
            this.tbEstimate.Text = String.Format("{0:c}", Convert.ToDecimal(propertyDamage.EstimatedDamageAmount.ToString() ));
            this.tbOICompany.Text = propertyDamage.OtherInsuranceCompany;
            this.tbOIAddress.Text = propertyDamage.OtherInsuranceAddress;
            this.tbOIAdjuster.Text = propertyDamage.OtherInsuranceAdjuster;
            this.tbOICity.Text = propertyDamage.OtherInsuranceCity;
            this.tbOIClaimNo.Text = propertyDamage.OtherInsuranceClaimNo;
            this.tbOIPolicyNo.Text = propertyDamage.OtherInsurancePolicyNo;
            this.tbOIPhoneNo.Text = propertyDamage.OtherInsurancePhone;
            this.tbOIFax.Text = propertyDamage.OtherInsuranceFax;
            this.tbOIZip.Text = propertyDamage.OtherInsuranceZipcode;
            this.tbPropertyDescription.Text = propertyDamage.PropertyDescription;
            int cbopropertyIndex = SelectPropertyDamageIdCbo(this.propertyDamage.PropertyTypeId);
            this.cboPropertyType.SelectedIndex = cbopropertyIndex;
            this.cboPropertyType_SelectedIndexChanged(null, null);
            this.cboOIState.SelectedValue = this.propertyDamage.OtherInsuranceStateId.ToString();
        }

        private void RefreshPersonsVehicles()
        {
            string defaultPerson,
                defaultVehicle;

            this.myVehicleList = mm.LoadVehicles(this.myClaim.VehicleNames, 1);
            this.cboVehicle.DataSource = myVehicleList;
            this.cboVehicle.DataTextField = "vName";
            this.cboVehicle.DataValueField = "vehicleId";
            this.cboVehicle.DataBind();

            this.myDriverList = mm.LoadClaimants(this.myClaim.PersonNamesAndTypes, 2);//LoadPerson(this.myClaim.PersonNamesAndTypes);
            this.cboDriver.DataSource = myDriverList;
            this.cboDriver.DataTextField = "fullName";
            this.cboDriver.DataValueField = "claimantId";
            this.cboDriver.DataBind();

            this.myOwnerList = mm.LoadClaimants(this.myClaim.PersonNamesAndTypesNotOnPolicy(), 1); //LoadPerson(this.myClaim.PersonNamesAndTypesNotOnPolicy(), 1);
            this.cboOwner.DataSource = myOwnerList;
            this.cboOwner.DataTextField = "fullName";
            this.cboOwner.DataValueField = "claimantId";
            this.cboOwner.DataBind();

            defaultVehicle = "Unknown";
            defaultPerson = "Unknown";

            if (vehicleId == 0)
                vehicleId = this.myClaim.VehicleNamed(defaultVehicle).Id;

            this.cboVehicle.SelectedIndex = SelectVehicleIdCbo(vehicleId);

            if (driverId == 0)
            {
                if (this.cboPropertyType.SelectedValue == SelectPropertyDamageIdCbo((int)modGeneratedEnums.PropertyType.Auto_Property).ToString())
                    driverId = mm.GetPersonIdForDescription(defaultPerson, claimId);
            }
            this.cboDriver.SelectedIndex = SelectClaimantIdCbo(driverId, 2);

            if (ownerId == 0)
                ownerId = mm.GetPersonIdForDescription(defaultPerson, claimId);
            this.cboOwner.SelectedIndex = SelectClaimantIdCbo(ownerId, 1);

            Person p = new Person(ownerId);
            this.txtOwner.Text = p.ExtendedDescription().Replace("\r\n","<br />");

            Person p2 = new Person(driverId);
            this.txtDriver.Text = p2.ExtendedDescription().Replace("\r\n", "<br />");
        }

        private void PopulatePropertyDamage()
        {
            this.propertyDamage.WhereSeen = this.tbLocation.Text;
            this.propertyDamage.DamageDescription = this.tbDescription.Text;

            try
            {
                this.propertyDamage.EstimatedDamageAmount = double.Parse(this.tbEstimate.Text.Replace("$","").Replace(",",""));
            }
            catch
            {
                this.propertyDamage.EstimatedDamageAmount = 0.0;
            }

            this.propertyDamage.OtherInsuranceCompany = this.tbOICompany.Text;
            this.propertyDamage.OtherInsuranceAddress = this.tbOIAddress.Text;
            this.propertyDamage.OtherInsuranceAdjuster = this.tbOIAdjuster.Text;
            this.propertyDamage.OtherInsuranceCity = this.tbOICity.Text;
            this.propertyDamage.OtherInsuranceZipcode = this.tbOIZip.Text;
            this.propertyDamage.OtherInsuranceClaimNo = this.tbOIClaimNo.Text;
            this.propertyDamage.OtherInsurancePhone = this.tbOIPhoneNo.Text;
            this.propertyDamage.OtherInsuranceFax = this.tbOIFax.Text;
            this.propertyDamage.OtherInsurancePolicyNo = this.tbOIPolicyNo.Text;
            if (this.cboOIState.Text == "")
                this.propertyDamage.OtherInsuranceStateId = 0;
            else
                this.propertyDamage.OtherInsuranceStateId = Convert.ToInt32(this.cboOIState.SelectedValue);

            //                this.propertyDamage.OtherInsuranceStateId = mm.GetStateId(this.cboOIState.Text);
            this.propertyDamage.PropertyDescription = this.tbPropertyDescription.Text;

            this.propertyDamage.PropertyTypeId = Convert.ToInt32(cboPropertyType.SelectedValue);
            this.propertyDamage.VehicleId = Convert.ToInt32(this.cboVehicle.SelectedValue);
            this.propertyDamage.DriverPersonId = Convert.ToInt32(this.cboDriver.SelectedValue);
            this.propertyDamage.OwnerPersonId = Convert.ToInt32(cboOwner.SelectedValue);
        }

        private void updatePersonVehicleButtons()
        {
            string cboText = "";

            if (!doneLoading)
                return;

            this.btnDriverEdit.Enabled = true;
            this.btnOwnerEdit.Enabled = true;
            this.btnVehicleEdit.Enabled = true;

            cboText = cboDriver.SelectedItem.ToString();
            if ((cboText.Trim() == "Parked and Unoccupied") || (cboText == ""))
                this.btnDriverEdit.Enabled = false;

            if (cboText.Trim().Length >= 7)
            {
                if (cboText.Substring(0, 7) == "Unknown")
                    this.btnDriverEdit.Enabled = false;
            }

            cboText = cboOwner.SelectedItem.ToString();
            if (cboText == "")
                this.btnOwnerEdit.Enabled = false;

            if (cboText.Trim().Length >= 7)
            {
                if (cboText.Substring(0, 7) == "Unknown")
                    this.btnOwnerEdit.Enabled = false;
            }

            cboText = cboVehicle.SelectedItem.ToString();
            if (cboText == "")
                this.btnVehicleEdit.Enabled = false;

            if (cboText.Trim().Length >= 7)
            {
                if (cboText.Substring(0, 7) == "Unknown")
                    this.btnVehicleEdit.Enabled = false;
            }
        }

        private string[] LoadUsers(List<Hashtable> userData)
        {
            var listOfStrings = new List<string>();
            myAssignedUsers = listOfStrings.ToArray();
            string s;
            try
            {
                foreach (Hashtable uData in userData)
                {
                    s = "";
                    s += ((string)uData["First_Name"]).Trim();
                    s += " " + ((string)uData["Last_Name"]).Trim();
                    s += " " + ((string)uData["User_Type"]).Trim();
                    s += " " + ((string)uData["User_Sub_Type"]).Trim();
                    listOfStrings.Add(s);
                }
                myAssignedUsers = listOfStrings.ToArray();
                return myAssignedUsers;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAssignedUsers;
            }
        }

        private string[] LoadUserGroups(List<Hashtable> userData)
        {
            var listOfStrings = new List<string>();
            myAssignedUserGroups = listOfStrings.ToArray();
            try
            {
                foreach (Hashtable uData in userData)
                {
                    listOfStrings.Add(((string)uData["Description"]).Trim());
                }
                myAssignedUsers = listOfStrings.ToArray();
                return myAssignedUsers;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAssignedUsers;
            }
        }

        private int SelectUserGroupIdCbo(int p)
        {
            int userGrpIdx = 0;
            foreach (Hashtable ugData in mm.UserGroupsWhichCanBeAssignedClaims())
            {
                if (p == (int)ugData["User_Group_Id"])
                    break;
                userGrpIdx++;
            }
            return userGrpIdx;
        }

        protected void propDamageOK_Click(object sender, EventArgs e)
        {

            try
            {
                mm.UpdateLastActivityTime();

                if (mode == "r")
                {   //nothing was edited
                    this.okPressed = true;
                    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                    return;
                }

                if (Convert.ToInt32(cboOwner.SelectedValue) < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Who owns this property?');</script>");
                    //MessageBox.Show("Who owns this property?", "Property Owner not selected", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    this.cboOwner.Focus();
                    return;
                }

                
                //if (propertyTypeId != (int)modGeneratedEnums.PropertyType.Auto_Property)
                //    driverId = 0;


                if (propertyTypeId != (int)modGeneratedEnums.PropertyType.Auto_Property)
                {
                    driverId = 0;   //don't save driver info if it is not an Auto property type
                    if (this.tbLocation.Text.Trim().Length == 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Where Seen is a required field if you want to request an appraisal. ');</script>");
                        return;
                        //if (MessageBox.Show("Where Seen is a required field if you want to request an appraisal.  Do you want to go back and enter Where Seen?", "Where Seen Not Entered", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                        //{
                        //    this.tbLocation.Focus();
                        //    return;
                        //}
                    }

                    if (this.tbDescription.Text.Trim().Length == 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Damage Description is a required field if you want to request an appraisal.');</script>");
                        return;
                        //if (MessageBox.Show("Damage Description is a required field if you want to request an appraisal.  Do you want to go back and enter Damage Description?", "Damage Description Not Entered", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                        //{
                        //    this.tbDescription.Focus();
                        //    return;
                        //}
                    }
                }

                string tempEstimate = this.tbEstimate.Text.Replace("$", "").Replace(",", "").Trim();
                if (!modLogic.isNumeric(tempEstimate) && (tempEstimate != ""))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Estimated Amount must be numeric.');</script>");
                    //MessageBox.Show("Estimated Amount must be numeric.", "Invalid Estimated Amount", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    this.tbEstimate.Focus();
                    return;
                }

                this.propDamageOK.Enabled = false;
                //else
                //    this.tbEstimate.Text = String.Format(tempEstimate, "{0:c}");

                //populate the property damage object
                this.PopulatePropertyDamage();

                if (this.propertyDamage.isAdd)
                {
<<<<<<< HEAD
                    if (this.myClaim.ExistingPropertyDamageForOwner(Convert.ToInt32(cboOwner.SelectedValue)))
=======
<<<<<<< HEAD
                    if (this.myClaim.ExistingPropertyDamageForOwner(Convert.ToInt32(cboOwner.SelectedValue)))
=======
                    if (this.myClaim.ExistingPropertyDamageForOwner(ownerId))
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There is Already an APD reserve line created for " + Person.PersonName(ownerId) + "');</script>");
                        //MessageBox.Show("There is Already an APD reserve line created for " + Person.PersonName(ownerId), "Existing Reserve", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                        return;
                    }
                    propertyDamageId = propertyDamage.Update(claimId);
                }
                else if (this.propertyDamage.isEdit)
                {
<<<<<<< HEAD
                    propertyDamageId = propertyDamage.Update(claimId, Convert.ToInt32(cboReserveType.SelectedValue));
=======
<<<<<<< HEAD
                    propertyDamageId = propertyDamage.Update(claimId, Convert.ToInt32(cboReserveType.SelectedValue));
=======
                   propertyDamageId = propertyDamage.Update(claimId);
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }

                this.propertyDamage.Reserve.AssignTo(selectedUserId, selectedUserGroupId);
                this.okPressed = true;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There was an error: " + ex.ToString() + "');</script>");

            }
        }

        protected void cboOwner_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.ownerId = Convert.ToInt32(cboOwner.SelectedValue);//SelectDriverIdCbo((string)cboDriver.SelectedItem));
                Person p = new Person(this.ownerId);
                this.txtOwner.Text = p.ExtendedDescription().Replace("\r\n", "<br />");
                this.updatePersonVehicleButtons();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboDriver_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.driverId = Convert.ToInt32(cboDriver.SelectedValue);//SelectDriverIdCbo((string)cboDriver.SelectedItem);
                Person p = new Person(this.driverId);
                this.txtDriver.Text = p.ExtendedDescription().Replace("\r\n", "<br />");
                this.updatePersonVehicleButtons();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.vehicleId = Convert.ToInt32(cboVehicle.SelectedValue);
                this.updatePersonVehicleButtons();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboPropertyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //if (!doneLoading)
                 //   return;

                this.propertyTypeId = SelectPropertyDamageIdCbo(cboPropertyType.SelectedItem.ToString());

                if (propertyTypeId == (int)modGeneratedEnums.PropertyType.Auto_Property)
                {
                    this.AutoPanel.Visible =  true;
                    this.cboVehicle.Enabled = true;
                    this.btnVehicleEdit.Enabled = true;
                    this.btnVehicleNew.Enabled = true;
                    this.DriverPanel.Visible = true;
                    this.cboDriver.Enabled = true;
                    this.btnDriverEdit.Enabled = true;
                    this.btnDriverNew.Enabled = true;
                    this.PropertyPanel.Visible = false;
                    this.tbPropertyDescription.Enabled = false;
                }
                else
                {
                    this.AutoPanel.Visible = false;
                    this.cboVehicle.Enabled = false;
                    //this.btnVehicleEdit.Enabled = false;
                    this.btnVehicleNew.Enabled = false;
                    this.DriverPanel.Visible = false;
                    this.cboDriver.Enabled = false;
                    //this.btnDriverEdit.Enabled = false;
                    this.btnDriverNew.Enabled = false;
                    this.PropertyPanel.Visible = true;
                    this.tbPropertyDescription.Enabled = true;
                }
                updatePersonVehicleButtons();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void populateSessionHolders()
        {
<<<<<<< HEAD
            string sessionName = "cboPropertyType" + this.claimId.ToString();
            Session[sessionName] = "set";
=======
<<<<<<< HEAD
            string sessionName = "cboPropertyType" + this.claimId.ToString();
            Session[sessionName] = "set";
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            Session["cboPropertyType"] = this.cboPropertyType.SelectedValue.ToString();
            Session["tbLocation"] = this.tbLocation.Text;
            Session["tbDescription"] = this.tbDescription.Text;
            Session["tbEstimate"] = this.tbEstimate.Text;
            Session["cboVehicle"] = this.cboVehicle.SelectedValue.ToString();
            Session["cboOwner"] = this.cboOwner.SelectedValue.ToString();
            Session["cboDriver"] = this.cboDriver.SelectedValue.ToString();
            Session["tbPropertyDescription"] = tbPropertyDescription.Text;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        public void populateFormFromSessionVariables()
        {
            try
            {
                string sessionName = "cboPropertyType" + this.claimId.ToString();
                if (Session[sessionName] != null)
                {
                    this.cboPropertyType.SelectedValue = Session["cboPropertyType"].ToString();
                    this.cboPropertyType_SelectedIndexChanged(null, null);
                    this.tbLocation.Text = Session["tbLocation"].ToString();
                    this.tbDescription.Text = Session["tbDescription"].ToString();
                    this.tbEstimate.Text = Session["tbEstimate"].ToString();
                    this.cboVehicle.SelectedValue = Session["cboVehicle"].ToString();
                    this.cboOwner.SelectedValue = Session["cboOwner"].ToString();
                    this.cboDriver.SelectedValue = Session["cboDriver"].ToString();
                    this.tbPropertyDescription.Text = Session["tbPropertyDescription"].ToString();

                    Session[sessionName] = null;
                    Session["cboPropertyType"] = null;
                    Session["tbLocation"] = null;
                    Session["tbDescription"] = null;
                    Session["tbEstimate"] = null;
                    Session["cboVehicle"] = null;
                    Session["cboOwner"] = null;
                    Session["cboDriver"] = null;
                    Session["tbPropertyDescription"] = null;

                }
            }
            catch (Exception ex)
            {
                string sessionName = "cboPropertyType" + this.claimId.ToString();
                Session[sessionName] = null;
                Session["cboPropertyType"] = null;
                Session["tbLocation"] = null;
                Session["tbDescription"] = null;
                Session["tbEstimate"] = null;
                Session["cboVehicle"] = null;
                Session["cboOwner"] = null;
                Session["cboDriver"] = null;
                Session["tbPropertyDescription"] = null;

                mf.ProcessError(ex);
            }
<<<<<<< HEAD
=======
=======
            




>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

        }


        protected void btnVehicleNew_Click(object sender, EventArgs e)
        {
            populateSessionHolders();



            Session["referer2"] = Request.Url.ToString().Replace("&control=cboVehicle", "").Replace("&control=cboOwner", "").Replace("&control=cboDriver", "").Replace("&personid", "&pastperson"); 

            string url = "frmVehicles.aspx?mode=c&claim_id=" + claimId + "&vehicle_id=0&control=cboVehicle";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnVehicleEdit_Click(object sender, EventArgs e)
        {
            populateSessionHolders();

            Session["referer2"] = Request.Url.ToString().Replace("&control=cboVehicle", "").Replace("&control=cboOwner", "").Replace("&control=cboDriver", "").Replace("&personid", "&pastperson");

            string url = "frmVehicles.aspx?mode=u&claim_id=" + claimId + "&vehicle_id=" + this.cboVehicle.SelectedValue + "&control=cboVehicle";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnOwnerNew_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
            Session["referer2"] = Request.Url.ToString().Replace("&control=cboVehicle", "").Replace("&control=cboOwner", "").Replace("&control=cboDriver", "").Replace("&personid", "&pastperson");

            string url = "frmPersonDetails.aspx?mode=c&person_type_id=7&claim_id=" + this.claimId + "&person_id=0&control=cboOwner";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnOwnerEdit_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
            Session["referer2"] = Request.Url.ToString().Replace("&control=cboVehicle", "").Replace("&control=cboOwner", "").Replace("&control=cboDriver", "").Replace("&personid", "&pastperson");

            string url = "frmPersonDetails.aspx?mode=c&person_type_id=7&claim_id=" + this.claimId + "&person_id=" + this.cboOwner.SelectedValue + "&control=cboOwner";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnDriverNew_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
            Session["referer2"] = Request.Url.ToString().Replace("&control=cboVehicle", "").Replace("&control=cboOwner", "").Replace("&control=cboDriver", "").Replace("&personid", "&pastperson");

            string url = "frmPersonDetails.aspx?mode=c&person_type_id=8&claim_id=" + this.claimId + "&person_id=0&control=cboDriver";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnDriverEdit_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
            Session["referer2"] = Request.Url.ToString().Replace("&control=cboVehicle", "").Replace("&control=cboOwner", "").Replace("&control=cboDriver", "").Replace("&personid", "&pastperson");

            string url = "frmPersonDetails.aspx?mode=c&person_type_id=8&claim_id=" + this.claimId + "&person_id=" + this.cboDriver.SelectedValue + "&control=cboDriver";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void tbOIZip_TextChanged(object sender, EventArgs e)
        {
            ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
            if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
            {
                if (this.tbOICity.Text.ToUpper() != lookup.zCity)
                    this.tbOICity.Text = lookup.zCity;

                cboOIState.ClearSelection();
                cboOIState.Items.FindByText(lookup.zState).Selected = true;
                this.tbOICity.Focus();
            }
            else
                this.tbOIZip.Focus();
        }

        protected void tbOIPhoneNo_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbOIPhoneNo.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbOIPhoneNo.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbOIPhoneNo.Text.Length; i++)
            {
                if (Char.IsDigit(tbOIPhoneNo.Text[i]))
                    p += tbOIPhoneNo.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbOIPhoneNo.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbOIPhoneNo.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

        }

        protected void tbOIFax_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbOIFax.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbOIFax.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbOIFax.Text.Length; i++)
            {
                if (Char.IsDigit(tbOIFax.Text[i]))
                    p += tbOIFax.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbOIFax.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbOIFax.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }

        protected void tbEstimate_TextChanged(object sender, EventArgs e)
        {
            string tempEstimate = this.tbEstimate.Text.Replace("$", "").Replace(",", "").Trim();
            if (!modLogic.isNumeric(tempEstimate) && (tempEstimate != ""))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Estimated Amount must be numeric.');</script>");
                //MessageBox.Show("Estimated Amount must be numeric.", "Invalid Estimated Amount", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                this.tbEstimate.Focus();
                return;
            }
            else
                this.tbEstimate.Text = String.Format("{0:c}", Convert.ToDecimal(tempEstimate));
        }

        private int SelectUserIdCbo(int p)
        {
            int userIdx = 0;
            foreach (Hashtable uData in mm.UsersWhichCanBeAssignedClaims())
            {
                if (p == (int)uData["User_Id"])
                    break;
                userIdx++;
            }
            return userIdx;
        }

        private int SelectUserGroupIdCbo(string p, int mode = 0)
        {
            int userGrpIdx = 0;
            int userGrpId = 0;
            foreach (Hashtable ugData in mm.UserGroupsWhichCanBeAssignedClaims())
            {
                if (p.Trim() == ((string)ugData["Description"]).Trim())
                {
                    userGrpId = (int)ugData["User_Group_Id"];
                    break;
                }
                userGrpIdx++;
            }
            if (mode == 1)
                return userGrpId;
            return userGrpIdx;
        }

        private int SelectUserIdCbo(string p, int mode = 0)
        {
            int userIdx = 0;
            int usrId = 0;
            foreach (Hashtable uData in mm.UsersWhichCanBeAssignedClaims())
            {
                string s = ((string)uData["First_Name"]).Trim() + " " + ((string)uData["Last_Name"]).Trim();
                if (p.Trim() == s.Trim())
                {
                    usrId = (int)uData["User_Id"];
                    break;
                }
                userIdx++;
            }
            if (mode == 1)
                return usrId;
            return userIdx;
        }

        private int SelectPropertyDamageIdCbo(int p)
        {
            List<Hashtable> myData = mm.GetPropertyTypes();
            int propDmgIdx = 0;
            foreach (Hashtable propType in myData)
            {
                if (p == (int)propType["Property_Type_Id"])
                    break;
                propDmgIdx++;
            }
            return propDmgIdx;
        }

        private int SelectPropertyDamageIdCbo(string p)
        {
            List<Hashtable> myData = mm.GetPropertyTypes();
            int propTypeId = 0;
            foreach (Hashtable propType in myData)
            {
                if (p.Trim() == ((string)propType["Description"]).Trim())
                {
                    propTypeId = (int)propType["Property_Type_Id"];
                    break;
                }
            }
            return propTypeId;
        }

        private int SelectClaimantIdCbo(int p, int mode)
        {
            int driverIdx = 0;
            List<ClaimantGrid> List2Use;
            if (mode == 1)
                List2Use = myOwnerList;
            else if (mode == 2)
                List2Use = myDriverList;
            else
                List2Use = new List<ClaimantGrid>();
            foreach (ClaimantGrid vData in List2Use)
            {
                {
                    if (p == vData.claimantId)
                        break;
                    driverIdx++;
                }
            }
            return driverIdx;
        }

        private int SelectVehicleIdCbo(int p)
        {
            int vehicleIdx = 0;
            foreach (VehicleGrid vData in this.myVehicleList)
            {
                if (p == vData.vehicleId)
                    break;
                vehicleIdx++;
            }
            return vehicleIdx;
        }

        //TODO
        //private void tbOIZip_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        //{
        //    ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
        //    if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
        //    {
        //        if (this.tbOICity.Text.ToUpper() != lookup.zCity)
        //            this.tbOICity.Text = lookup.zCity;
        //        this.cboOIState.Text = lookup.zState;
        //        this.cboUser.Focus();
        //    }
        //    else
        //        this.tbOIZip.Focus();
        //}


    }
}
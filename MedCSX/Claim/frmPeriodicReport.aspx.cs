﻿using MedCSX.App_Code;
using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public enum Periods : int
    {
        Daily = 0,
        Monthly = 1
    };

    public enum Months : int
    {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Sep = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12


    };

    public partial class frmPeriodicReport : System.Web.UI.Page
    {
        private static modLogic ml = new modLogic();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        public Periods Periods = Periods.Daily;
        public string dateStoredProc = "";
        public string reportFileName = "";
        public string title = "";
        private List<Hashtable> dates;
        private string[] monthAbbr = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        private Dictionary<string, int> monthDict;
        private string lastSelectedDat = "";
        

        protected void Page_Load(object sender, EventArgs e)
        {
            string periods = Request.QueryString["period"].ToString();

            if (periods == "d")
            {
                dateStoredProc = "usp_Get_Daily_Draft_Report_Dates";
                Periods = Periods.Daily;
                reportFileName = "PERIODIC_DRAFT_REPORT";
                this.Page.Title = "MedCsX Daily Draft Report";
            }
            else
            {
                Periods = Periods.Monthly;
                grMonths.Visible = false;
                dateStoredProc = "usp_Get_Monthly_Draft_Report_Dates";
                reportFileName = "DRAFT_MONTHLY_REPORT";
                this.Page.Title = "MedCsX Monthly Draft Report";
            }

            dates = db.ExecuteStoredProcedureReturnHashList(dateStoredProc);

            if (dates.Count == 0)
                return;
            if (!Page.IsPostBack)
                LoadYears();
        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            //rptParms = 
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }

        protected void tsbViewReport_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Dictionary<string, string> rptParms = new Dictionary<string, string>();

            rptParms.Add("Date1", grDateAmounts.SelectedRow.Cells[1].Text);
            DateTime enddate = Convert.ToDateTime(grDateAmounts.SelectedRow.Cells[1].Text).AddDays(1);
            rptParms.Add("Date2", enddate.ToString());

            ShowReport("MedCsx Daily Draft Report", rptParms);
        }

        private void LoadYears()
        {
            



            grYears.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Draft_years"); 
            grYears.DataBind();
            grYears.SelectedIndex = (Convert.ToInt32(grYears.Rows.Count) - 1);
            grYears_SelectedIndexChanged(null, null);
            grMonths_SelectedIndexChanged(null, null);
        }

        private void LoadMonths()
        {
            grMonths.DataSource = monthAbbr;
            grMonths.DataBind();
            grMonths.SelectedIndex = 0;
            grMonths_SelectedIndexChanged(null, null);
        }

        private void LoadDateAmounts()
        {

            int years = Convert.ToInt32(grYears.SelectedRow.Cells[1].Text);
            string strMonths = grMonths.SelectedRow.Cells[1].Text;
            int months = (int)(Months)Enum.Parse(typeof(Months), strMonths);

            grDateAmounts.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Daily_Draft_Report_By_Year_Month", years, months);
            grDateAmounts.DataBind();
            grDateAmounts.SelectedIndex = 0;
            
        }

      

        protected void grYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMonths();
        }

        protected void grMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDateAmounts();
        }

        protected void grDateAmounts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
    }
}
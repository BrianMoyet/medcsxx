﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmPickBodyPart : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        public bool okPressed = false;      //was OK pressed?  Output parameter
        public int personId = 0;            //Output parameter
        public User MySelectedUser = null;
        public UserGroup MySelectedGroup = null;
        public string selectedItem = "";
        public int appraisalRequestId = 0,
            appraisalTaskId = 0;
        public IEnumerable myList;

        private State myState = null;       //current state
        private List<PickerGrid> mySelections;      //repository for Selection Grid
        private int _claimId = 0,
            pickMode,
            _claimantId = 0;
        private bool doneLoading = false;
        private MedCsxLogic.Claim m_claim = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            myList = (IEnumerable)Request.QueryString["newParts"];

            grSelections.DataSource = LoadList(myList);
            grSelections.DataBind();
        }

        protected void cmdOk_Click(object sender, EventArgs e)
        {

        }

        private IEnumerable LoadList(IEnumerable list)
        {
            ArrayList myArray = (ArrayList)list;
            mySelections = new List<PickerGrid>();
            foreach (object item in myArray)
            {
                if (item.GetType() == typeof(string))
                {
                    mySelections.Add(new PickerGrid()
                    {
                        description = (string)item,
                        userId = 0
                    });
                }
                else if (item.GetType() == typeof(cboGrid))
                {
                    cboGrid pck = (cboGrid)item;
                    mySelections.Add(new PickerGrid()
                    {
                        description = pck.description,
                        userId = pck.id
                    });
                }
            }
            return mySelections;
        }

        class PickerGrid
        {
            //Reserve Assignment Picker
            public int userId { get; set; }
            public Uri imageIdx { get; set; }
            public string userName { get; set; }
            public string userOrGroup { get; set; }
            public int pdToday { get; set; }
            public int pdWeek { get; set; }
            public int biToday { get; set; }
            public int biWeek { get; set; }
            public int TotalClaims { get; set; }

            //Address Picker
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get { return address2 == null ? address1 : address1 + address2; } }
            public string city { get; set; }
            public string state { get; set; }
            public string zipCode { get; set; }
            public string homePhone { get; set; }

            //Pick From List
            public string description { get; set; }

            //Appraisal Picker
            public int image_Idx { get; set; }
            public bool isSelected { get; set; }
            public string appraisalReq { get; set; }
           
        }


    }
}
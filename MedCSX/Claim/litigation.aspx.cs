﻿using MedCSX.App_Code;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class litigation : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Litigation";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpID = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpID);


            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();


                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            MedCSX.App_Code.Litigation myData = new MedCSX.App_Code.Litigation();
            myData.GetLitigationByID(id);

            ddlStatus.SelectedValue = myData.litigation_status_id;
            ddlDefenseAttorney.SelectedValue = myData.defense_attorney_id;
            ddlPlantiffAttorney.SelectedValue = myData.plaintiff_attorney_id;
            ddlCourtLocation.SelectedValue = myData.court_location_id;
            txtDateFiled.Text = Convert.ToDateTime(myData.date_filed).ToString("yyyy-MM-dd");
            txtDateServed.Text = Convert.ToDateTime(myData.date_served).ToString("yyyy-MM-dd"); 
            txtResolutionAmount.Text = string.Format("{0:c}", myData.reslolution_amount);
            txtComments.Text = myData.comments;



        }

        protected void PopulateDDLs()
        {
            DataTable dt = new DataTable();
            dt = HelperFunctions.getTypeTableRows("Litigation_Status", "Description", "1=1");
            ddlStatus.DataSource = dt;
            ddlStatus.DataValueField = dt.Columns[0].ColumnName;
            ddlStatus.DataTextField = dt.Columns[1].ColumnName;
            ddlStatus.DataBind();

            dt = HelperFunctions.VendorsOfType(Litigation.DefenseAttorneyType);
            ddlDefenseAttorney.DataSource = dt;
            ddlDefenseAttorney.DataValueField = dt.Columns[0].ColumnName;
            ddlDefenseAttorney.DataTextField = dt.Columns[1].ColumnName;
            ddlDefenseAttorney.DataBind();


            dt = HelperFunctions.VendorsOfType(Litigation.PlaintiffAttorneyType);
            ddlPlantiffAttorney.DataSource = dt;
            ddlPlantiffAttorney.DataValueField = dt.Columns[0].ColumnName;
            ddlPlantiffAttorney.DataTextField = dt.Columns[1].ColumnName;
            ddlPlantiffAttorney.DataBind();

            dt = HelperFunctions.VendorsOfType(Litigation.CourtLocationType);
            ddlCourtLocation.DataSource = dt;
            ddlCourtLocation.DataValueField = dt.Columns[0].ColumnName;
            ddlCourtLocation.DataTextField = dt.Columns[1].ColumnName;
            ddlCourtLocation.DataBind();

            DataTable dt2 = new DataTable();

            MedCSX.App_Code.Reserve myData = new MedCSX.App_Code.Reserve();
            dt2 = myData.GetReserves_With_Images_By_ClaimId(claim_id);

            gvReserves.DataSource = dt2;
            gvReserves.DataBind();
            gvReserves.Visible = true;

            MedCSX.App_Code.Litigation myData2 = new MedCSX.App_Code.Litigation();
            dt2 = myData2.usp_Get_Litigation_Defendents(id);

            gvDefendants.DataSource = dt2;
            gvDefendants.DataBind();
            gvDefendants.Visible = true;

        }

        public string FormatPopupDefendants(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('defendants.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0,'my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('defendants.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0,'my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnDefendantsDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
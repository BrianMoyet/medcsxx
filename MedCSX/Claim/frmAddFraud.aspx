﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAddFraud.aspx.cs" Inherits="MedCSX.Claim.frmAddFraud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtClaimId" runat="server" Text="" ForeColor="#0066CC"></asp:Label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            Fraud:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtFraudName" runat="server" Text="" ForeColor="#0066CC"></asp:Label>
          
        </div>
    </div>
    <div id="pnlVendor" runat="server">
        <div class="row">
            <div class="col-md-12">
               Vendor Tax Id: <asp:TextBox ID="txtVendorTaxID" CausesValidation="false" OnTextChanged="txtVendorTaxID_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
            
                <br /><asp:LinkButton ID="txtVendorLookup" OnClick="txtVendorLookup_Click" CausesValidation="false" runat="server">Lookup Vendor</asp:LinkButton>
                <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                        Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
                </div>
            </div>
        </div>
                <br /><asp:Label ID="lblTaxID" runat="server" Visible="false" Text="" ForeColor="#0066CC"></asp:Label>
            </div>
         </div>
            </div>
         <div class="row">
            <div class="col-md-12">
                <asp:RadioButton ID="rbFraud_Vendor" GroupName="PersonType" runat="server" Text="Vendor" Enabled="False" />
                <asp:RadioButton ID="rbFraud_Person" GroupName="PersonType" OnCheckedChanged="rbFraud_Person_CheckedChanged" runat="server" Text="Person" Enabled="False" />
                <asp:RadioButton ID="rbFraud_Both" GroupName="PersonType" OnCheckedChanged="rbFraud_Both_CheckedChanged" runat="server" Text="Both" Enabled="False"/>
                <asp:RadioButton ID="rbFraud_Vehicle" GroupName="PersonType" OnCheckedChanged="rbFraud_Vehicle_CheckedChanged" runat="server" Text="Vehicle" Enabled="False" />
            </div>
         </div>

    <div id="pnlVehicle" runat="server" visible="false">
        <div class="row">
            <div class="col-md-3">
                Vin:
            </div>
            <div class="col-md-9">
                <asp:TextBox ID="txtVIN" runat="server" AutoPostBack="True" OnTextChanged="txtVIN_TextChanged"></asp:TextBox>
                <br />
               
            </div>
        </div>
    </div>
      <div class="row">
      
        <div class="col-md-3">
            Type: 
        </div>
          <div class="col-md-9">
            <asp:DropDownList ID="cboFraudType" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Details:<br />
            <asp:TextBox ID="txtFraudDescription"  TextMode="MultiLine" Height="150px" Width="100%"  runat="server"></asp:TextBox>
        </div>
    </div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"/>
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

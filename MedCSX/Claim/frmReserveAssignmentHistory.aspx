﻿<%@ Page Title="Reserve Assignment History" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmReserveAssignmentHistory.aspx.cs" Inherits="MedCSX.Claim.frmReserveAssignmentHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
         <div class="col-md-9">
             <asp:Label ID="lblClaim" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Reserve:
        </div>
         <div class="col-md-9">
             <asp:Label ID="lblReserve" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Assigned To:
        </div>
         <div class="col-md-9">
             <asp:Label ID="lblAssignedTo" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <asp:GridView ID="grHistory" runat="server" AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
               ForeColor="#333333"  width="100%" ShowFooter="True">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
               
                  <asp:BoundField DataField="assignment_date" HeaderText="Date"/>
                  <asp:BoundField DataField="assigned_to" HeaderText="Assigned To" />
                  <asp:BoundField DataField="date_entered_first_file_note" HeaderText="Entered First File Note"/>
                  <asp:BoundField DataField="date_first_contact_claimant" HeaderText="First Contact Claimant" />
                   <asp:BoundField DataField="date_attempt_contact_claimant" HeaderText="Attempt Contact Claimant" />
                   <asp:BoundField DataField="date_first_loss_payment" HeaderText="First Loss payment" />
                
               </Columns>
             
               <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salvage.aspx.cs" MasterPageFile="~/SiteModal.Master"  Inherits="MedCSX.Claim.salvage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <b>Vehicle</b>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Tax Id:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtTaxID" runat="server"></asp:TextBox>
                <br />Don't Know Tax Id?  <asp:HyperLink ID="hprLookupVendor" runat="server">Lookup Vendor</asp:HyperLink>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Name:
            </div>
            <div class="col-md-10">
                <asp:Label ID="lblname" runat="server" Text="lblName"></asp:Label>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Location:
            </div>
            <div class="col-md-10">
                <asp:Label ID="lblLocation" runat="server" Text="lblLocation"></asp:Label>
            </div>
        </div>
    </div>
    <div class="col-md-6">
       <div class="row">
            <div class="col-md-12">
                <b>Fees</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Staorage Days:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtStorageDays" runat="server"></asp:TextBox>
               
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Cost Per Day:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtCostPerDay" runat="server"></asp:TextBox>
               
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
               Towing:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtTowing" runat="server"></asp:TextBox>
               
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                Other Charges
            </div>
        </div>
         <div class="row">
            <div class="col-md-6">
                <asp:TextBox ID="txtOtherChargesDescription" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-6">
                    <asp:TextBox ID="txtOtherChargesAmount" runat="server"></asp:TextBox>           
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:DropDownList ID="ddlSalvageVehicle" runat="server"></asp:DropDownList>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <b>Dates</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Storage Began:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtStorageBegan" runat="server" TextMode="Date"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Appraiser Assigned:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtAppraiserAssigned" runat="server" TextMode="Date"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
               Completed:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtCompeted" runat="server" TextMode="Date"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <b>Location</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:RadioButtonList ID="rblLocation" runat="server">
                    <asp:ListItem>At Copart</asp:ListItem>
                    <asp:ListItem>Owner Retained</asp:ListItem>
                    <asp:ListItem>Other</asp:ListItem>
                    <asp:ListItem>Abandon</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
</div>
    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>
 

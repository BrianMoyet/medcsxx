﻿<%@ Page Title="Vendors" AspCompat="true" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmVendors.aspx.cs" Inherits="MedCSX.Claim.frmVendors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .HeaderFreez
{
	position:relative ;
	top:expression(this.offsetParent.scrollTop);
	z-index: 10;
}
    </style>
    <div class="row">
        <div class="col-md-12">
            <b>Search By</b>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="btnSearchByTaxId" runat="server" OnClick="btnSearchByTaxId_Click">Tax ID</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSearchByName" runat="server" OnClick="btnSearchByName_Click">Name</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSearchByAddress" runat="server" OnClick="btnSearchByAddress_Click">Address</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSearchByCity" runat="server" OnClick="btnSearchByCity_Click">City</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSearchByState" runat="server" OnClick="btnSearchByState_Click">State</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnSearchByZip" runat="server" OnClick="btnSearchByZip_Click">Zip</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="txtSearchCriteria" runat="server" Text=""></asp:Label>
             <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                    <asp:Label ID="lblVendorSearch" runat="server" Text=""></asp:Label><asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Vendor Type: <asp:DropDownList ID="grVendorType" OnSelectedIndexChanged="grVendorType_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
            &nbsp;&nbsp;&nbsp;Vendor Sub Type: <asp:DropDownList ID="grVendorSubType" OnSelectedIndexChanged="grVendorSubType_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <div style="width: 100%; height: 400px; overflow: scroll">
             <asp:GridView ID="grVendors" runat="server"
                 AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Vendor_Id"
                 ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" 
                 CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" 
                 HeaderStyle-VerticalAlign="Top" ShowFooter="true" 
                 FooterStyle-BackColor="White" Width="100%" AutoGenerateSelectButton="True" OnSelectedIndexChanged="grVendors_SelectedIndexChanged">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
                 <%-- <asp:TemplateField ItemStyle-Width="75px">
                      <ItemTemplate>
                          <asp:LinkButton ID="copyToClipBoard" Text="Copy Tax ID" runat="server" OnClick="copyToClipBoard_Click"></asp:LinkButton>
                      </ItemTemplate>
                  </asp:TemplateField>--%>
                  <asp:BoundField DataField="tax id" HeaderText="Tax ID" ItemStyle-Width="75px" />
                    <asp:BoundField DataField="name" HeaderText="Vendor Name"/>
                    <asp:BoundField DataField="address 1" HeaderText="Address 1"/>
                    <asp:BoundField DataField="Address 2" HeaderText="Address 2"/>
                    <asp:BoundField DataField="city" HeaderText="City"/>
                    <asp:BoundField DataField="state" HeaderText="State"/>
                    <asp:BoundField DataField="zipcode" HeaderText="Zip"/>
                    <asp:BoundField DataField="phone" HeaderText="Phone"/>
                    <asp:BoundField DataField="fax" HeaderText="Fax"/>
                    <asp:BoundField DataField="email" HeaderText="Email"/>
                   <asp:TemplateField HeaderText="" >
                       <HeaderTemplate>
                                <asp:Button  ID="btnVendorsAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD" data-toggle="modal" data-target="#theModal" OnClick="btnVendorsAdd_Click"  CausesValidation="false"  />
                       </HeaderTemplate>
                     <ItemTemplate>
                        <asp:Button  ID="btnVendorsUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"   data-toggle="modal" data-target="#theModal" OnClick="btnVendorsUpdate_Click" CausesValidation="false"   />
                        
                     </ItemTemplate>
                        <FooterTemplate>
                        <asp:Button  ID="btnVendorsAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD" data-toggle="modal" data-target="#theModal"  OnClick="btnVendorsAdd_Click"  CausesValidation="false"  />
                     </FooterTemplate>
                  </asp:TemplateField>
                
               </Columns>
              <HeaderStyle CssClass="HeaderFreez" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
             <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
               <EditRowStyle BackColor="#999999" />
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
                 </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

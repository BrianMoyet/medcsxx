﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxDatabase;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmChangePassword : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        public int uid = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
             uid = Convert.ToInt32(Request.QueryString["uid"]);

            lblUser.Text = Convert.ToInt32(HttpContext.Current.Session["userID"]).ToString();
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            string oldPasswordEntered = "";
            string newPassword = "";
            string newPasswordConfirmed = "";
            string msg = "";
            bool result = false;

            mm.UpdateLastActivityTime();

            oldPasswordEntered = txtOldPassword.Text;
            newPassword = txtNewPassword.Text;
            newPasswordConfirmed = txtConfirmPassword.Text;

            result = UpdatePassword(ref oldPasswordEntered, ref newPassword, ref newPasswordConfirmed, ref msg);

            ClientScript.RegisterStartupScript(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

            if (result == true)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }
        }

        public bool UpdatePassword(ref string oldPasswordEntered, ref string newPassword, ref string newPasswordConfirmed, ref string msg)
        {
            List<Hashtable> c;
            short i;
            string retiredPassword;
            //Hashtable d;
            string oldPassword;

            // get old password
            Hashtable user = ml.getRow("User", uid);
            oldPassword = user["Password"].ToString();

            Hashtable settings = ml.getRow("System_Settings", 1);

            if (oldPassword != oldPasswordEntered)
            {
                msg = "Old Password is Incorrect.";
                return false;
            }

            //'check for matching new passwords
            if (newPassword != newPasswordConfirmed)
            {
                msg = "New Password and Confirmation Password Don't Match.";
                return false;
            }

            //'check for minimum password characters
            if (newPassword.Length < Convert.ToInt32(settings["Minimum_Password_Characters"]))
            {
                msg = "Password Must be at Least " + settings["Minimum_Password_Characters"].ToString() + " Characters Long.";
                return false;
            }

            //'check for minimum uppercase characters
            if (mm.UppercaseCount(newPassword) < Convert.ToUInt32(settings["Minimum_UpperCase_Characters"]))
            { 
                msg = "Password Must Have at Least " + settings["Minimum_UpperCase_Characters"].ToString() + " Uppercase Characters.";
                return false;
            }


            //'check for minimum special characters
            if (Regex.Matches(newPassword, "[~!@#$%^&*()_+{}:\"<>?]").Count < Convert.ToInt32(settings["Minimum_Special_Characters"]))
            {
                msg = "Password Must Have at Least " + settings["Minimum_Special_Characters"].ToString() + " Special Characters.";
                return false;
            }

            //'check for minimum numeric characters
            if (mm.NumericCount(newPassword) < Convert.ToInt32(settings["Minimum_Numeric_Characters"]))
            {
                msg = "Password Must Have at Least " + settings["Minimum_Numeric_Characters"].ToString() + " Numeric Characters.";
                return false;
            }
           
            //'check for maximum password characters
            if (newPassword.Length > Convert.ToInt32(settings["Maximum_Password_Characters"]))
            {
                msg = "Password Must not be Greater than " + settings["Maximum_Password_Characters"] + " Characters Long.";
                return false;
            }

            c = db.ExecuteStoredProcedureReturnHashList("usp_Get_Old_Passwords", uid);
            i = 0;
            if (c.Any())
            {
                foreach (Hashtable d in c)
                {
                    retiredPassword = d["Password"].ToString();
                    if (newPassword == retiredPassword)
                    {
                        msg = "New Password Must not be the Same as the Previous " + settings["Password_Changes_Before_Reuse"].ToString() + " Passwords.";
                        return false;
                    }

                    i = i++;

                    if (i == Convert.ToUInt32(settings["Password_Changes_Before_Reuse"]))
                    {
                        db.ExecuteStoredProcedure("usp_Set_Password", uid, newPassword, uid);
                        msg = "Password Successfully Changed.";
                        return true;
                    }


                }
            }
            else
            {
                db.ExecuteStoredProcedure("usp_Set_Password", uid, newPassword, uid);
                msg = "Password Successfully Changed.";
                return true;
            }

            db.ExecuteStoredProcedure("usp_Set_Password", uid, newPassword, uid);
            msg = "Password Successfully Changed.";
            return true;
        }
    }
}
﻿<%@ Page Title="Kansas City Draft Queue" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmUnprintedDraftsKC.aspx.cs" Inherits="MedCSX.Claim.frmUnprintedDraftsKC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
       <div class="col-md-12">
           <center>
                <h4>Kansas City Draft Queue</h4>
                <asp:Label ID="sbStatus" runat="server" ForeColor="Red"> </asp:Label>
           </center>
       </div>
   </div>
    <div class="row">
   <div class="col-md-3">
            <asp:RadioButtonList ID="rblSelect" runat="server" OnSelectedIndexChanged="rblSelect_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                <asp:ListItem Value="1" Text="Check All    "></asp:ListItem>
                <asp:ListItem Value="0" Text="Uncheck All"></asp:ListItem>
            </asp:RadioButtonList>
            </div>
        <div class="col-md-9">
           <asp:Button ID="btnPrintKC" runat="server" OnClick="btnPrintKC_Click" class="btn btn-info btn-xs pull-right" Text="Print To Kansas City" /> 
        </div>
        </div>
             <div class="row">
        <div class="col-md-12">
             <asp:GridView ID="grDrafts" AutoGenerateSelectButton="false" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="draft_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"   AllowPaging="False"   width="100%"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                     <Columns>
                            <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>      
                    <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "=window.open('ViewClaim.aspx?Ptabindex=7&claim_id=" + DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + ".focus()" %> Runat="Server">
                            <%# Eval("display_claim_id") %></asp:Hyperlink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" />
                        </asp:TemplateField>
                   <asp:BoundField DataField="draft_no" HeaderText="Draft No"  />
                   <asp:BoundField DataField="draft_amount" HeaderText="Amount" DataFormatString="{0:c}" />
                   <asp:BoundField DataField="print_date" HeaderText="Date In Queue" />
                   <asp:BoundField DataField="Adjuster" HeaderText="Adjuster" />
                   <asp:BoundField DataField="Payee" HeaderText="Payee" />
                   <asp:BoundField DataField="Created_By_Name" HeaderText="Created By" />

                      
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                     <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
<<<<<<< HEAD
        <asp:Button ID="btnPrintLV" OnClick="btnPrintLV_Click" class="btn btn-danger btn-xs pull-right" runat="server" Text="Print To Las Vegas"  OnClientClick="return confirm('WARNING! Are you sure you want to Print to LAS VEGAS?');"  />
=======
<<<<<<< HEAD
        <asp:Button ID="btnPrintLV" OnClick="btnPrintLV_Click" class="btn btn-danger btn-xs pull-right" runat="server" Text="Print To Las Vegas"  OnClientClick="return confirm('WARNING! Are you sure you want to Print to LAS VEGAS?');"  />
=======
        <asp:Button ID="btnPrintLV" OnClick="btnPrintLV_Click" class="btn btn-info btn-xs pull-right" runat="server" Text="Print To Las Vegas"  OnClientClick="return confirm('WARNING! Are you sure you want to Print to LAS VEGAS?');"  />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;

namespace MedCSX.Claim
{

    public partial class FileNote : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - File Note";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            FileNotes myFN = new FileNotes();
            myFN.GetFileNoteByID(id);
            lbFileNoteType.SelectedValue = myFN.File_Note_Type_Id;
            //chkCleared.Checked = true;
            txtFileNoteText.Text = myFN.File_Note_Text;
            if (myFN.Is_Summary == "1")
                chkSummary.Checked = true;
            //if (myFN. == "1")
            //    chkUrgent.Checked = true;
            ddlClaimant.SelectedValue = myFN.Claimant_Id;
            ddlClaimant_SelectedIndexChanged(this, EventArgs.Empty);
            ddlReserve.SelectedValue = myFN.ReserveDescription;
            ddlDiaryType.SelectedValue = myFN.Diary_Type_Id;
            ddlEventType.SelectedValue = myFN.Event_Type_Id;
            lblUpdatedBy.Text = myFN.Modified_By_Name;
            lblUpdatedDate.Text = myFN.Modified_Date;
            lblCreatedBy.Text = myFN.Created_By_Name;
            lblCreatedDate.Text = myFN.Created_Date;
            lblAssignedTo.Text = myFN.Assigned_To;
      
        }

        protected void PopulateControls()
        {
            DataTable dt = new DataTable();
            FileNotes myFN = new FileNotes();
            dt = myFN.GetFileNoteUserTypes();
            lbFileNoteType.DataSource = dt;
            lbFileNoteType.DataValueField = dt.Columns[0].ColumnName;
            lbFileNoteType.DataTextField = dt.Columns[1].ColumnName;
            lbFileNoteType.DataBind();

            dt = HelperFunctions.getTypeTableRows("Diary_Type", "Description", "1=1");
            ddlDiaryType.DataSource = dt;
            ddlDiaryType.DataValueField = dt.Columns[0].ColumnName;
            ddlDiaryType.DataTextField = dt.Columns[1].ColumnName;
            ddlDiaryType.DataBind();

            DataTable dtc = new DataTable();
            Claimants myClaimant = new Claimants();
            dtc = myClaimant.GetClaimantsAndIDsForClaim(claim_id);
            DataRow newRowC = dtc.NewRow();
            newRowC["claimant_id"] = 0;
            newRowC["Person"] = "";
            dtc.Rows.InsertAt(newRowC, 0);

            ddlClaimant.DataSource = dtc;
            ddlClaimant.DataValueField = dtc.Columns[0].ColumnName;
            ddlClaimant.DataTextField = dtc.Columns[2].ColumnName;
            ddlClaimant.DataBind();

            dt = HelperFunctions.getTypeTableRows("Event_Type", "Description", "1=1");
            ddlEventType.DataSource = dt;
            ddlEventType.DataValueField = dt.Columns[0].ColumnName;
            ddlEventType.DataTextField = dt.Columns[1].ColumnName;
            ddlEventType.DataBind();
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlClaimant_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            Reserve myReserves = new Reserve();
            dt = myReserves.usp_Get_Reserve_Names_For_Claimant(Convert.ToInt32(ddlClaimant.SelectedValue));
            DataRow newRowR = dt.NewRow();
            newRowR["reserve_name"] = "";

            dt.Rows.InsertAt(newRowR, 0);

            ddlReserve.DataSource = dt;
            ddlReserve.DataValueField = dt.Columns[0].ColumnName;
            ddlReserve.DataTextField = dt.Columns[0].ColumnName;
            ddlReserve.DataBind();
            ddlReserve.Visible = true;


        }
    }
}
﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;




namespace MedCSX.Claim
{
    public partial class frmAddDefendant : System.Web.UI.Page
    {
        public bool okPressed = false;      //was OK button pressed?  Output parameter
        public int transactionId = 0;       //transaction ID of transaction row inserted.  Output parameter
        public int voidReasonId = 0;        //void reason ID selected.  Output parameter
        public int PersonId = 0,        //person ID selected.  Output parameter
          VendorId = 0;        //vendor ID selected.  Output parameter
        public string DefendantName = "";   //defendant name from ID.  Output parameter

        private int reserveId,              //reserve ID to create transaction for.  Input parameter
                                            //salvageId,                      //salvage_Id of salvage row inserted
                                            //subroId,                        //subro_Id of subro row inserted
            reissueFromId,                  //Id to reissue from 
            m_mode,                         //which screen to present? Subro or Salvage
            reserveTypeId = 0,            //Policy_Coverage_Type_Id
            claim_id = 0;                 //claim_id for reserve ID
                                          //private int vendorId;       //vendor ID corresponding to tax ID entered
        private long draftNo;
        private Reserve m_reserve;
        private ReserveType m_reserveType;
        private MedCsxLogic.Claim m_Claim;
        private List<Tuple<int, Hashtable>> eventTypeIds = new List<Tuple<int, Hashtable>>();
        private string voidReasonTable,
            msg = string.Empty,
            reasonPart = string.Empty,
            noLockPart = ". Proceeding Will Place the Reserve in a Locked State Until Approved By Your Supervisor. Do You Wish to Proceed?",
            lockedPart = ". this would normally add a lock to the reserve, but since it's already locked, the system will just create a note instead. Do You Wish to Proceed?";
        private bool doneLoading = false;
        //private List<ReasonList> myReasonList;
        private List<ClaimantGrid> myPersonList;
        private List<ClaimantGrid> myVendorList;

        private static ModForms mf = new ModForms();
        string referer = "";

        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private int m_claim_id = 0;
        private int m_litigation_id = 0;

        protected void btnOK_Click(object sender, EventArgs e)
        {
            int tmpperson = 0;
            int tmpvendor = 0;
            try
            {
                if (DefendantValidation())
                {
                    if (rbDefendantPerson.Checked == true)
                        tmpperson = Convert.ToInt32(cboDefendantPerson.SelectedValue);

                    if (rbDefendantVendor.Checked == true)
                        Convert.ToInt32(cboDefendantVendor.SelectedValue);

                    LitigationDefendant NewDefendent = new LitigationDefendant();
                    NewDefendent.LitigationId = Convert.ToInt32(Request.QueryString["litigation_id"]);
                    NewDefendent.PersonId = tmpperson;
                    NewDefendent.Vendor_Id = tmpvendor;
                    NewDefendent.Update();



                    Session["referer2"] = null;
                    Session["id"] = cboDefendantPerson.SelectedItem;
                    Session["vendorId"] = cboDefendantPerson.SelectedItem;
                    ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool DefendantValidation()
        {
            if ((bool)this.rbDefendantPerson.Checked)
            {
                if (this.cboDefendantPerson.SelectedIndex < 0)
                {
                    this.DefendantName = "";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a person.');</script>");
                    //MessageBox.Show("Please select a person.", "No Person Selected", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.cboDefendantPerson.Focus();
                    return false;
                }
            }
            else
            {
                if (this.cboDefendantVendor.SelectedIndex < 0)
                {
                    this.DefendantName = "";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select an insurance company.');</script>");
                    //MessageBox.Show("Please select an insurance company.", "No Insurance Company Selected", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.cboDefendantVendor.Focus();
                    return false;
                }
            }
            return true;
        }




        protected void Page_Load(object sender, EventArgs e)
        {
            referer = Session["referer2"].ToString();
            this.Page.Title = "Select Defendant";
            m_claim_id = Convert.ToInt32(Request.QueryString["claim_id"]);
            m_litigation_id = Convert.ToInt32(Request.QueryString["litigation_id"]);

            m_Claim = new MedCsxLogic.Claim(m_claim_id);
            //this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuEditUser.png", UriKind.Relative));
            //tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiDefendant);
            if (!Page.IsPostBack)
            { 
                bool insLoaded = LoadInsuranceCo();
                bool peopleLoaded = false;
                if (this.m_Claim.Id != 0)
                    peopleLoaded = LoadPeople();
            }

        }

        private bool LoadInsuranceCo()
        {
            this.myVendorList = mm.LoadClaimants(Vendor.VendorsOfType(LitigationDefendant.InsuranceCompanyType), 4);
            this.cboDefendantVendor.DataSource = this.myVendorList;
            this.cboDefendantVendor.DataTextField = "fullName";
            this.cboDefendantVendor.DataValueField = "claimantId";
            this.cboDefendantVendor.DataBind();
            return true;
        }

        private bool LoadPeople()
        {
            this.myPersonList = mm.LoadClaimants(this.m_Claim.PersonNamesAndTypes, 1);
            this.cboDefendantPerson.DataSource = this.myPersonList;
            this.cboDefendantPerson.DataTextField = "fullName";
            this.cboDefendantPerson.DataValueField = "claimantId";
            this.cboDefendantPerson.DataBind();
            return true;
        }

        protected void rbDefendantPerson_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbDefendantPerson.Checked == true)
            {
                this.cboDefendantVendor.Enabled = false;
                this.cboDefendantPerson.Enabled = true;
            }
            else
            {
                this.cboDefendantVendor.Enabled = true;
                this.cboDefendantPerson.Enabled = false;
            }
           
        }

        protected void rbDefendantVendor_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbDefendantVendor.Checked == true)
            {
                this.cboDefendantVendor.Enabled = true;
                this.cboDefendantPerson.Enabled = false;
            }
            else
            {
                this.cboDefendantVendor.Enabled = false;
                this.cboDefendantPerson.Enabled = true;
            }
        }

        protected void cboDefendantPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
           // ClaimantGrid mySelection = (ClaimantGrid)this.cboDefendantPerson.SelectedItem;
            //if (mySelection == null)
            //    return;

            if (Convert.ToInt32(cboDefendantPerson.SelectedValue) < 0)
                return;

            this.VendorId = 0;
            this.PersonId = Convert.ToInt32(cboDefendantPerson.SelectedValue);
            this.DefendantName = cboDefendantPerson.SelectedItem.Text;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmClaimantDetails : System.Web.UI.Page
    {
        #region Class Variables
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();

        public bool okPressed = false;     //was OK pressed? Output parameter.
        public int personId = 0;      //output parameter

        private int claimId = 0,
            thisPersonId = 0,
            whichMode;
        private WitnessPassenger witnessPassenger;
        private Claimant claimant;
        private ArrayList persons;
        private bool doneLoading = false;
        private MedCsxLogic.Claim m_claim;
        private string titleExt = "";

        private List<witnessType> myWitnessType;
        private List<ClaimantGrid> myPersonList;

        private string referer = "";
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            this.personId = Convert.ToInt32(Request.QueryString["person_id"]);

            m_claim = new MedCsxLogic.Claim(claimId);

            int mode = Convert.ToInt32(Request.QueryString["mode"]);
            whichMode = mode;

            referer = Session["referer"].ToString();


            //set dialog caption
            if (personId == 0)
                this.Page.Title = "Claim " + Claim.DisplayClaimId + " - Add " + titleExt;

                if (mode == 0)
                {
                    witnessPassenger = new WitnessPassenger(personId);
                    thisPersonId = witnessPassenger.PersonId;
                    this.cboPersonType.SelectedValue = SelectTypeCbo(witnessPassenger.WitnessPassengerTypeId).ToString();
                }
                else if (mode == 1)
                {
                    claimant = new Claimant(personId);
                    thisPersonId = claimant.PersonId;
                }
            else
            {
                this.Page.Title = "Claim " + Claim.DisplayClaimId + " - Edit " + titleExt;
                //load property damage details
                if (mode == 0)
                {
                    witnessPassenger = new WitnessPassenger(personId);
                    thisPersonId = witnessPassenger.PersonId;
                    this.cboPersonType.SelectedValue = SelectTypeCbo(witnessPassenger.WitnessPassengerTypeId).ToString();
                }
                else if (mode == 1)
                {
                    claimant = new Claimant(personId);
                    thisPersonId = claimant.PersonId;
                }

            }

            if (!Page.IsPostBack)
            {
               
                //TODO
               
               
                SetTitle(mode);
                LoadForm(mode);

                this.btnPersonOK.Enabled = false;
                cboPersonType.DataSource = LoadTypeTableComboBox(cboPersonType, false, "Claim_Type_Id = " + this.Claim.ClaimTypeId);
                cboPersonType.DataTextField = "witnessDescription";
                cboPersonType.DataValueField = "witnessTypeId";

                cboPersonType.DataBind();
                cboPersonType.SelectedIndex = 0;


                //RefreshPersons();
                doneLoading = true;

            }

            if (Request.QueryString["control"] != null)
            {
                switch (Request.QueryString["control"].ToString())
                {
                    case "cboPersonSelect":
                        cboPersonSelect.SelectedValue = Request.QueryString["personid"].ToString();
                        cboPersonSelect_SelectedIndexChanged(null, null);
                        break;

                   
                    
                    default:
                        break;
                }
            }

            if (this.personId > 0)
            {
                btnPersonNew.Enabled = false;
            }
        }

        private void SetTitle(int mode)
        {
            switch (mode)
            {
                case 0:     //witness/passenger
                    this.lblPersonType.Visible = true;
                    this.cboPersonType.Enabled = true;
                    this.lblPerson.Text = "Person:";
                    this.titleExt = "Witness / Passenger";
                    break;
                case 1:     //claimant
                    this.lblPersonType.Visible  = false;
                    this.cboPersonType.Visible = false;
                    this.lblPerson.Text = "Claimant:";
                    this.titleExt = "Claimant";
                    break;
                default:
                    break;
            }
        }

        private void LoadForm(int mode = 0)
        {
            this.doneLoading = false;
            this.btnPersonOK.Enabled = false;
            cboPersonType.DataSource = LoadTypeTableComboBox(cboPersonType, false, "Claim_Type_Id = " + this.Claim.ClaimTypeId);
            cboPersonType.DataTextField = "witnessDescription";
            cboPersonType.DataValueField = "witnessTypeId";
            cboPersonType.DataBind();
            cboPersonType.SelectedIndex = 0;

            //set dialog caption
            if (personId == 0)
                this.Title = "Claim " + Claim.DisplayClaimId + " - Add " + titleExt;
            else
            {
                this.Title = "Claim " + Claim.DisplayClaimId + " - Edit " + titleExt;
                //load property damage details
                if (mode == 0)
                {
                    witnessPassenger = new WitnessPassenger(personId);
                    thisPersonId = witnessPassenger.PersonId;
                    this.cboPersonType.SelectedIndex = SelectTypeCbo(witnessPassenger.WitnessPassengerTypeId);
                }
                else if (mode == 1)
                {
                    claimant = new Claimant(personId);
                    thisPersonId = claimant.PersonId;
                }

            }

            RefreshPersons();
            doneLoading = true;
        }

       
        private void RefreshPersons()
        {
            myPersonList = mm.LoadClaimants(this.Claim.PersonNamesAndTypes, 1);
            this.cboPersonSelect.DataSource = myPersonList;
            this.cboPersonSelect.DataTextField = "fullName";
            this.cboPersonSelect.DataValueField = "claimantId";
            this.cboPersonSelect.DataBind();
            this.cboPersonSelect_SelectedIndexChanged(null, null);

            if (thisPersonId == 0)
            {
                this.cboPersonSelect.SelectedIndex = 0;
                thisPersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
            }
            else
                this.cboPersonSelect.SelectedValue = this.claimant.PersonId.ToString(); //this.cboPersonSelect.SelectedIndex = SelectPersonCbo(thisPersonId);

            this.txtPersonDetails.Text = new Person(Convert.ToInt32(cboPersonSelect.SelectedValue)).ExtendedDescription().Replace("\r\n", "<br>"); ;

            if (cboPersonSelect.Text.Trim().Split(' ')[0] == "Unknown")
                this.btnPersonEdit.Enabled = false;
            else
                this.btnPersonEdit.Enabled = true;
        }

        private int SelectPersonCbo(int p)
        {
            int cboIdx = 0;
            foreach (ClaimantGrid cg in myPersonList)
            {
                if (p == cg.claimantId)
                    break;
                cboIdx++;
            }
            return cboIdx;
        }

        private List<witnessType> LoadTypeTableComboBox(DropDownList cb, bool includeZeros, string whereClause)
        {
            myWitnessType = new List<witnessType>();
            try
            {
                foreach (Hashtable wData in TypeTable.getTypeTableRows("Witness_Passenger_Type", whereClause))
                {
                    witnessType wt = new witnessType();
                    wt.witnessTypeId = (int)wData["Id"];
                    wt.imageIdx = (int)wData["Image_Index"];
                    wt.witnessDescription = (string)wData["Description"];
                    if (includeZeros || (wt.witnessTypeId != 0))
                        myWitnessType.Add(wt);
                }
                return myWitnessType;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myWitnessType;
            }
        }

        private int SelectTypeCbo(int p)
        {
            int cboIdx = 0;
            foreach (witnessType type in myWitnessType)
            {
                if (type.witnessTypeId == p)
                    break;
                cboIdx++;
            }
            return cboIdx;
        }

        protected void btnPersonOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (whichMode == 0)
                {
                    if (personId == 0)
                    {
                        if (this.Claim.ExistingWitnessPassenger(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.');</script>");
                            //MessageBox.Show(Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                            return;
                        }

                        witnessPassenger = new WitnessPassenger();
                    }
                    else
                    {
                        if (witnessPassenger.PersonId != Convert.ToInt32(cboPersonSelect.SelectedValue))
                        {
                            if (this.Claim.ExistingWitnessPassenger(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                            {
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.');</script>");
                                //MessageBox.Show(Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK); 
                                return;
                            }
                        }
                    }
                    witnessPassenger.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                    witnessPassenger.ClaimId = claimId;
                    witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                    personId = witnessPassenger.Update();

                    Person p = new Person(thisPersonId);
                    p.UpdateUnknownPersons();   //add additional unknown person as necessary
                }
                else if (whichMode == 1)
                {
                    claimant = new Claimant();
                    if (personId == 0)
                    {
                        if (this.Claim.ExistingClaimant(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Claimant for this Claim.');</script>");
                            //MessageBox.Show(Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Claimant for this Claim.", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                            return;
                        }

                       
                    }
                    else
                    {
                        if (personId != Convert.ToInt32(cboPersonSelect.SelectedValue))
                        {
                            if (this.Claim.ExistingClaimant(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                            {
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Claimant for this Claim.');</script>");
                                //MessageBox.Show(Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Claimant for this Claim.", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                                return;
                            }
                        }
                    }
                    claimant.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                    claimant.ClaimId = claimId;
                    claimant.Update();

                    this.claimant.Person.UpdateUnknownPersons();  //add additional unknown person as necessary
                }

                this.okPressed = true;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboPersonSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (cboPersonSelect.SelectedIndex == -1)
                    return;

                btnPersonOK.Enabled = true;
                //TODO
                //ClaimantGrid mySelection = (ClaimantGrid)cboPersonSelect.SelectedItem;
                thisPersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                this.txtPersonDetails.Text = new Person(thisPersonId).ExtendedDescription().Replace("\r\n","<br />");

                //if (mySelection.nameAddressType.Trim().Split(' ')[0] == "Unknown")
                //{
                //    this.btnPersonEdit.Enabled = false;
                   
                //}
                //else
                //{
                //    this.btnPersonEdit.Enabled = true;
                  
                //}
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnPersonNew_Click(object sender, EventArgs e)
        {

            Session["referer2"] = Request.Url.ToString();

            
            string url = "frmPersonDetails.aspx?mode=u&person_type_id=9&claim_id=" + this.claimId + "&person_id=0&control=cboPersonSelect";
            string s = "window.open('" + url + "', 'popup_windowClaimantPerson', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowClaimantPerson", s, true);
        }

        protected void btnPersonEdit_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString();

            string url = "frmPersonDetails.aspx?mode=u&person_type_id=9&claim_id=" + this.claimId + "&person_id=" + this.cboPersonSelect.SelectedValue + "&control=cboPersonSelect";
            string s = "window.open('" + url + "', 'popup_windowClaimantPerson', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowClaimantPerson", s, true);
        }

        private MedCsxLogic.Claim Claim
        {
            get { return m_claim; }
            set { m_claim = value; }
        }

        class witnessType
        {
            public int witnessTypeId { get; set; }
            public int imageIdx { get; set; }
            public string witnessDescription { get; set; }
        }

    }
}
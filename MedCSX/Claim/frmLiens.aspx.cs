﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmLiens : System.Web.UI.Page
    {

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        private int vendorId = 0;
        private int claimId = 0;
        private int injuredId = 0;
        private int lienId = 0;

        private MedCsxLogic.Claim m_claim;
        private Injured m_injured;

        private string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            claimId = Convert.ToInt32(Request.QueryString["claim_id"]);
            injuredId = Convert.ToInt32(Request.QueryString["injured_id"]);
            vendorId = Convert.ToInt32(Request.QueryString["vendor_id"]);
            lienId = Convert.ToInt32(Request.QueryString["injured_lien_id"]);
            

            m_claim = new MedCsxLogic.Claim(claimId);

            if (Session["referer3"] != null)
            {

                referer = Session["referer3"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();


           
                if (injuredId == 0)
                {
                    this.m_injured = new Injured();     //create new injured person object
                  
                }
                else
                    this.m_injured = new Injured(this.injuredId);



            if (!Page.IsPostBack)
            {
                if (injuredId != 0)
                {
                    this.lblInjured.Text = this.m_injured.Claimant().ClaimantName().ToString();

                    Vendor v = new Vendor(vendorId);

                    if (!string.IsNullOrEmpty(v.TaxId))
                        this.txtTaxId.Text = v.TaxId;

                    this.txtVendorName.Text = v.Description.Replace("\r\n", "<br>");
                    this.txtAmount.Text = Request.QueryString["amount"].ToString().Replace("$", "");
                }

                if (Session["vendorID"] != null)
                {
                    vendorId = Convert.ToInt32(Session["vendorID"]);
                    Vendor myVendor = new Vendor(vendorId);
                    txtTaxId.Text = myVendor.TaxId.ToString();
                    txtTaxId_TextChanged(null, null);
                    Session["vendorID"] = null;
                    
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {

            if (txtTaxId.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter the vendor's Tac ID');</script>");
                //MessageBox.Show("Please enter the vendor's Tac ID", "Vendor Tax ID Required", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.txtTaxId.Focus();
                return;
            }

            if (txtAmount.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter the lien amount.');</script>");
                //MessageBox.Show("Please enter the lien amount.", "Lien Amount is required", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.txtAmount.Focus();
                return;
            }

            if (!modLogic.isNumeric(this.txtAmount.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Salvage Amount Must Be Numeric.');</script>");
                //MessageBox.Show("Salvage Amount Must Be Numeric");
                this.txtAmount.Focus();
                return;
            }

            if (lienId == 0)
            {   //create new lien
                InjuredLien newLien = new InjuredLien();
                newLien.InjuredId = this.injuredId;
                newLien.VendorId = Vendor.getVendorIdForTaxId(txtTaxId.Text);
                newLien.Amount = double.Parse(txtAmount.Text);
                newLien.Update();
            }
            else
            {   //update existing lien
                InjuredLien myLien = new InjuredLien(lienId);
                myLien.Amount = double.Parse(txtAmount.Text);
                myLien.VendorId = vendorId;
                myLien.Update();
            }


            Session["referer3"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);



        }

        protected void txtVendorLookup_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            

           
        }

        private void PopulateVendorControls()
        {
            try
            {
                mm.UpdateLastActivityTime();

                Vendor v = new Vendor(this.vendorId);

                if (!string.IsNullOrEmpty(v.TaxId))
                    this.txtTaxId.Text = v.TaxId;

                this.txtVendorName.Text = v.Name + v.Description;
                this.txtvendorid.Text = vendorId.ToString();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtTaxId_TextChanged(object sender, EventArgs e)
        {
            string taxId = this.txtTaxId.Text.Trim();

            if (taxId == "")
                return;


            //lookup tax id
            vendorId = Vendor.getVendorIdForTaxId(taxId);
            txtvendorid.Text = Vendor.getVendorIdForTaxId(taxId).ToString();

            if (vendorId == 0)
            {
                lblTaxID.Visible = true;
                lblTaxID.Text = "Vendor Not Found Corresponding to Tax Id " + taxId + ".";
                if (this.txtTaxId.Visible)
                    this.txtTaxId.Focus();
                return;
            }
            lblTaxID.Visible = false;
            Vendor v = new Vendor(this.vendorId);

            if (!string.IsNullOrEmpty(v.TaxId))
                this.txtTaxId.Text = v.TaxId;

            this.txtVendorName.Text = v.Description.Replace("\r\n", "<br>");
            this.txtvendorid.Text = vendorId.ToString();


        }

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Session["refererVendor"] = Request.Url.ToString();

            string lookupValue = txtVendorSearch.Text;
            if (lookupValue == "")
                return;

            //show vendor search results
            //show vendor search results
            string url = "frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
            string s2 = "window.open('" + url + "', 'popup_window4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
        }
    }
}
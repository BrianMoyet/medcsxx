﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class PossibleDuplicateClaimsFound : System.Web.UI.Page
    {
        
        private string policyNo = "";
        

        protected void Page_Load(object sender, EventArgs e)
        {
            policyNo = Request.QueryString["policyNo"].ToString();
            //claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            //MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            //claim.GetClaimTitleInfo(claim_id);
            //Page.Title = "Possible Duplicate Claims";
            //tmpSub_claim_type_id = claim.sub_claim_type_id;

            //tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            //string tmpID = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            //id = Convert.ToInt32(tmpID);
            string policy_no = Request.QueryString["policyNo"].ToString();



            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                GetGeneralInfo();
            }
        }

        protected void GetGeneralInfo()
        {
            DataTable dt = new DataTable();
            MedCSX.App_Code.Claim myClaim = new MedCSX.App_Code.Claim();
            dt = myClaim.GetGetPossibleDuplicateClaims(policyNo);
            gvPossibleDuplicateClaims.DataSource = dt;
            gvPossibleDuplicateClaims.DataBind();


        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["CancelCreateClaim"] = "0";

        }
    }
}
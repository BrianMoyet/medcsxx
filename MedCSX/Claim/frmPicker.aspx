﻿<%@ Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPicker.aspx.cs" Inherits="MedCSX.Claim.frmPicker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
      <div class="col-md-12">
            <asp:GridView ID="grSelections" runat="server" AutoGenerateColumns="False" CellPadding="4"  DataKeyNames="userId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True" OnSelectedIndexChanged="grSelections_SelectedIndexChanged"  width="100%" AutoGenerateSelectButton="true">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                              
                         <asp:BoundField DataField="description" HeaderText="Property" />
                         <asp:BoundField DataField="appraisalReq" HeaderText="Appraisal Requested?" />
                         
                          
                     
                    
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                      
                </asp:GridView>

      </div>
</div>
    
 <div class="row">
     <div class="col-md-12">
                <asp:Button ID="btnPickOK" runat="server" class="btn btn-info btn-xs" Text="OK"  OnClick="btnPickOK_Click" />
         <br /><asp:Label ID="lblWarning" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
               <asp:Button ID="chkShowStateOnly" runat="server" class="btn btn-info btn-xs" Text="Show KS Only" OnClick="btnPickOK_Click"/>
              <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
</div>

</asp:Content>

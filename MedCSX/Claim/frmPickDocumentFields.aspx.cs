﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Media.Imaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using MedCsxLogic;
using Document = DocumentFormat.OpenXml.Wordprocessing.Document;
using Paragraph = DocumentFormat.OpenXml.Wordprocessing.Paragraph;
using Run = DocumentFormat.OpenXml.Wordprocessing.Run;
using Text = DocumentFormat.OpenXml.Wordprocessing.Text;

using AI = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using DocumentFormat.OpenXml.Drawing.Wordprocessing;

namespace MedCSX.Claim
{
    public partial class frmPickDocumentFields : System.Web.UI.Page
    {

        private int m_documentTypeId;
        private int m_claimId;
        private MedCsxLogic.Document m_document;
        private MedCsxLogic.DocumentType m_document_type;
        private MedCsxLogic.Claim m_Claim;
        private Vehicle m_Vehicle;
        private Draft m_Draft;
        private Claimant m_Claimant;
        private bool m_Cancelling = false;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

       


        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_claimId = Convert.ToInt32(Request.QueryString["claim_id"].ToString());
            this.m_documentTypeId = Convert.ToInt32(Request.QueryString["document_type_id"].ToString());

            this.m_Claim = new MedCsxLogic.Claim(m_claimId);

            this.m_document = new MedCsxLogic.Document(m_documentTypeId);
            this.m_document.DocumentTypeId = this.m_documentTypeId;
            this.m_document_type = new MedCsxLogic.DocumentType(m_documentTypeId);

            string dp = "";

            if (!m_document_type.FileName.Contains(".dotx"))
                dp = HttpContext.Current.Server.MapPath(m_document_type.FileName.Replace(@"G:\Claims\Templates\Claims Professionals\Templates\", "Documents/Templates/").Replace(".dot", ".dotx"));
            else
                dp = HttpContext.Current.Server.MapPath(m_document_type.FileName.Replace(@"G:\Claims\Templates\Claims Professionals\Templates\", "Documents/Templates/"));

            if (File.Exists(dp))
            {
                if (!Page.IsPostBack)
                PopulateScreen();
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Template File " + @dp.Replace("\\", "\\\\") + " does not exist.');</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }
        }

        private bool HasFieldWithObjectNamed(string p)
        {
            bool returnValue = false;

            string dp = "";
                
                if (!m_document_type.FileName.Contains(".dotx"))
                    dp = HttpContext.Current.Server.MapPath(m_document_type.FileName.Replace(@"G:\Claims\Templates\Claims Professionals\Templates\", "Documents/Templates/").Replace(".dot",".dotx"));
                else
                    dp = HttpContext.Current.Server.MapPath(m_document_type.FileName.Replace(@"G:\Claims\Templates\Claims Professionals\Templates\", "Documents/Templates/"));


            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(dp, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }

               
                switch (p)
                {
                    case "Claimant":
                        if (docText.Contains("Claimant"))
                            returnValue =  true;
                        break;
                    case "Vehicle":
                        if (docText.Contains("Vehicle"))
                            returnValue = true;
                        break;
                    case "Draft":
                        if (docText.Contains("Draft"))
                            returnValue = true;
                        break;
                    default:
                        break;
                }
                docText.Contains(p);

                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }

            return returnValue;
        }

        private void PopulateScreen()
        {
            this.txtAdjuster.Text = ml.CurrentUser.Name;
            this.txtClaimId.Text = this.m_Claim.DisplayClaimId;
            this.txtDocumentType.Text = this.m_document.DocumentType.Description;

            if (this.HasFieldWithObjectNamed("Claimant"))
            {
                this.cboClaimant.Enabled = true;
                this.cboClaimant.DataSource = mm.GetMyClaimants(m_claimId, 0);
                this.cboClaimant.DataTextField = "Person";
                this.cboClaimant.DataValueField = "claimant_Id";
                this.cboClaimant.DataBind();
            }
            else
                this.cboClaimant.Enabled = false;

            if (this.HasFieldWithObjectNamed("Vehicle"))
            {
                this.cboVehicle.Enabled = true;
                this.cboVehicle.DataSource = mm.LoadVehicles(this.m_Claim.VehicleNames, 1);
                this.cboVehicle.DataTextField = "vName";
                this.cboVehicle.DataValueField = "vehicleId";
                this.cboVehicle.DataBind();
            }
            else
                this.cboVehicle.Enabled = false;

            if (this.HasFieldWithObjectNamed("Draft"))
            {
                this.cboDraft.Enabled = true;
                this.cboDraft.DataSource = LoadDrafts();
                this.cboDraft.DataTextField = "description";
                this.cboDraft.DataValueField = "id";
                this.cboDraft.DataBind();
            }
            else
                this.cboDraft.Enabled = false;
        }

        private IEnumerable LoadDrafts()
        {
            List<cboGrid> myDraftList = new List<cboGrid>();
            try
            {
                foreach (Transaction t in this.m_Claim.Transactions)
                {
                    if (t.DraftId != 0)
                    {
                        Draft d = new Draft(t.DraftId);
                        if (!d.isVoid)
                        {
                            cboGrid cg = new cboGrid();
                            cg.id = t.DraftId;
                            cg.description = d.Description;
                            myDraftList.Add(cg);
                        }
                    }
                }
                return myDraftList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDraftList;
            }
        }

        private void PopulateWordFields()
        {
            try
            {

                int m_Claimant_id = 0;
                int m_Vehicle_id = 0;
                int m_Draft_id = 0;
                string dpwrite = "";

                if (cboClaimant.Enabled == true)
                {
                    m_Claimant_id = Convert.ToInt32(cboClaimant.SelectedValue);

                }
                if (cboVehicle.Enabled == true)
                    m_Vehicle_id = Convert.ToInt32(cboVehicle.SelectedValue);
                if (cboDraft.Enabled == true)
                    m_Draft_id = Convert.ToInt32(cboDraft.SelectedValue);


                this.m_Draft = null;
                this.m_Claimant = null;
                this.m_Vehicle = null;

                if (this.cboClaimant.Enabled)
                {
                    m_Claimant = new Claimant(Convert.ToInt32(cboClaimant.SelectedValue));
                }

                if (this.cboVehicle.Enabled)
                {
                    m_Vehicle = new Vehicle(Convert.ToInt32(cboVehicle.SelectedValue));
                }

                if (this.cboDraft.Enabled)
                {
                    m_Draft = new Draft(Convert.ToInt32(cboDraft.SelectedValue));
                }
                string docText = null;


                MedCsxLogic.Document m_document = new MedCsxLogic.Document();
                m_document.DocumentTypeId = m_documentTypeId;
                m_document.DocumentStatusId = 1;
                m_document.ClaimId = m_claimId;
                m_document.ClaimantId = m_Claimant_id;
                m_document.VehicleId = m_Vehicle_id;
                m_document.DraftId = m_Draft_id;
                m_document.ManuallyAdded = true;

                int documentId = m_document.Update();

                string dateTime = m_document.CreatedDate.ToString("MMddyyyy hhmmss");
                string doc2Create = m_document_type.Description + "-" + m_document.ClaimId + "-" + dateTime + "-" + documentId.ToString() + ".dotx";
                string dp = "";
                if (m_document_type.FileName.Contains(".dotx"))
                    dp = HttpContext.Current.Server.MapPath(m_document_type.FileName.Replace(@"G:\Claims\Templates\Claims Professionals\Templates\", "Documents/Templates/"));
                else
                    dp = HttpContext.Current.Server.MapPath(m_document_type.FileName.Replace(@"G:\Claims\Templates\Claims Professionals\Templates\", "Documents/Templates/").Replace(".dot", ".dotx"));

                string dpwritedir = HttpContext.Current.Server.MapPath(@"Documents/Completed Documents/");
                dpwrite = dpwritedir + doc2Create.Replace(".dotx", ".docx");

                string destinationFile = dpwrite;
                string sourceFile = dp;

                // Copy template file and open the copy
                File.Copy(sourceFile, destinationFile, true);
                using (WordprocessingDocument document = WordprocessingDocument.Open(destinationFile, true))
                {
                    // Change the document type to Document
                    document.ChangeDocumentType(DocumentFormat.OpenXml.WordprocessingDocumentType.Document);

                    // Get the MainPart of the document
                    MainDocumentPart mainPart = document.MainDocumentPart;

                    // Get the Document Settings Part
                    DocumentSettingsPart documentSettingPart1 = mainPart.DocumentSettingsPart;

                    // Create a new attachedTemplate and specify a relationship ID
                    AttachedTemplate attachedTemplate1 = new AttachedTemplate() { Id = "relationId1" };

                    // Append the attached template to the DocumentSettingsPart
                    documentSettingPart1.Settings.Append(attachedTemplate1);

                    // Add an ExternalRelationShip of type AttachedTemplate.
                    // Specify the path of template and the relationship ID
                    documentSettingPart1.AddExternalRelationship("http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate", new Uri(sourceFile, UriKind.Absolute), "relationId1");

                    // Save the document
                    mainPart.Document.Save();

                }

                var memoryStream = new MemoryStream();

                using (var fileStream = new FileStream(destinationFile, FileMode.Open, FileAccess.Read))
                    fileStream.CopyTo(memoryStream);

                using (var document = WordprocessingDocument.Open(memoryStream, true))
                {
                    using (StreamReader sr = new StreamReader(document.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }
                    string formattedAddress = m_Claim.Policy().Address.Replace("\r\n", "<w:br/>");
                    docText = docText.Replace("Claim.CompanyName", Company.getCompanyForPolicy(this.m_Claim.Policy().PolicyNo));
                    docText = docText.Replace("Claim.Policy.NameInsd", m_Claim.Policy().NameInsd);
                    docText = docText.Replace("Claim.Policy.Address", formattedAddress);
                    docText = docText.Replace("Claim.DisplayClaimId", m_Claim.DisplayClaimId);
                    docText = docText.Replace("Claim.PolicyNo", m_Claim.PolicyNo);
                    docText = docText.Replace("Claim.Policy.DtEff.ToShortDateString", m_Claim.PolicyEffectiveDate().ToShortDateString());
                    docText = docText.Replace("Claim.Policy.DtExp.ToShortDateString", m_Claim.PolicyExpirationDate().ToShortDateString());
                    docText = docText.Replace("Claim.Policy.DtEff", m_Claim.PolicyEffectiveDate().ToShortDateString());
                    docText = docText.Replace("Claim.Policy.DtExp", m_Claim.PolicyExpirationDate().ToShortDateString());
                    docText = docText.Replace("Claim.DateOfLoss.ToShortDateString", m_Claim.DateOfLoss.ToShortDateString());
                    docText = docText.Replace("Document.CurrentDate", DateTime.Now.ToString("MMMM dd, yyyy"));
                    docText = docText.Replace("User.Signature", "");


                    //Claimant
                    if (this.cboClaimant.Enabled)
                    {
                        docText = docText.Replace("Claimant.Name", m_Claimant.Name);
                    }

                    //user sig section
                    docText = docText.Replace("User.Name", ml.CurrentUser.Name);
                    docText = docText.Replace("User.UserType.Description", ml.CurrentUser.UserType().Description);
                    docText = docText.Replace("User.PhoneExt", ml.CurrentUser.PhoneExt);
                    docText = docText.Replace("User.Email", ml.CurrentUser.Email);
                    docText = docText.Replace("User.DirectFax", ml.CurrentUser.DirectFax);


                    docText = docText.Replace("Claim.Policy.State.StateDefaultText", "");

                    using (StreamWriter sw = new StreamWriter(document.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }

                    //string dpwritedir = HttpContext.Current.Server.MapPath(@"Documents/Completed Documents/");
                    dpwrite = dpwritedir + doc2Create.Replace(".dotx", ".docx");
                    FileStream file = new FileStream(dpwrite, FileMode.Create, FileAccess.Write);
                    memoryStream.WriteTo(file);
                    file.Close();
                    document.Close();


                }

                string path = Server.MapPath(@"Documents/Completed Documents/");
                dpwrite = path + doc2Create.Replace(".dotx", ".docx");
                System.IO.FileInfo fileDownload = new System.IO.FileInfo(dpwrite);

                //string fileNameSig = @"G:\Claims\SignaturesMedCsxx\gragsonl.jpeg";
                string fileName = ml.CurrentUser.SignatureFileName.Replace(".bmp", ".jpg");

                if (fileName.Trim().Length == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Signature File Missing. .jpg file must be created.');</script>");

                }
                else
                {
                    string fileNameSig = Server.MapPath(@"../Signatures/") + fileName.Replace(@"G:\Claims\Signatures\", "");
                    InsertSignature(dpwrite, fileNameSig);
                }


                Response.Clear();

                Response.AppendHeader("content-disposition", "attachment; filename=" + fileDownload.Name);
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(dpwrite);
                Response.Flush();
                Response.End();
<<<<<<< HEAD

                fileDownload = null;
                dpwrite = null;
=======
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            }
            catch (Exception ex)
            {
               // mf.ProcessError(ex);
            }
        }

        private void InsertSignature(string document, string fileName, string tagName = "Picture 3")
        {
            WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true);
            IEnumerable<Drawing> drawings = wordDoc.MainDocumentPart.Document.Descendants<Drawing>().ToList();
            foreach (Drawing drawing in drawings)
            {
                DocProperties dpr = drawing.Descendants<DocProperties>().FirstOrDefault();
                if (dpr != null && ((dpr.Name == "Picture 2") || (dpr.Name == "Picture 3") || (dpr.Name == "Picture 4") || (dpr.Name == "Picture 5") || (dpr.Name == "Picture 6" || (dpr.Name == "Picture 7"))))
                {
                    foreach (DocumentFormat.OpenXml.Drawing.Blip b in drawing.Descendants<DocumentFormat.OpenXml.Drawing.Blip>().ToList())
                    {
                        OpenXmlPart imagePart = wordDoc.MainDocumentPart.GetPartById(b.Embed);
                        using (var writer = new BinaryWriter(imagePart.GetStream()))
                        {
                            writer.Write(File.ReadAllBytes(fileName));
                        }
                    }
                }
            }

            wordDoc.Close();
            wordDoc.Dispose();
        }

        private object EvaluateMethod(object obj, Type mtype, string methodName)
        {
            System.Reflection.MemberInfo[] miArray = mtype.GetMember(methodName);
            if (miArray.Length > 0)
            {
                System.Reflection.MemberInfo mi = miArray[0];
                if (mi.MemberType == System.Reflection.MemberTypes.Property)
                {
                    System.Reflection.PropertyInfo pi = (System.Reflection.PropertyInfo)mi;
                    return pi.GetValue(obj, null);
                }

                if (mi.MemberType == System.Reflection.MemberTypes.Method)
                {
                    System.Reflection.MethodInfo methodInfo = (System.Reflection.MethodInfo)mi;
                    object[] parms = { };
                    return methodInfo.Invoke(obj, parms);
                }

                if (mi.MemberType == System.Reflection.MemberTypes.Field)
                {
                    System.Reflection.FieldInfo fi = (System.Reflection.FieldInfo)mi;
                    return fi.GetValue(obj);
                }
            }
            return null;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (this.cboClaimant.Enabled && (this.cboClaimant.SelectedIndex < 0))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please choose a claimant');</script>");
                this.cboClaimant.Focus();
                return;
            }

            if (this.cboVehicle.Enabled && (this.cboVehicle.SelectedIndex < 0))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please choose a vehicle');</script>");
                this.cboVehicle.Focus();
                return;
            }

            if (this.cboDraft.Enabled && (this.cboDraft.SelectedIndex < 0))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please choose a draft.');</script>");
                this.cboDraft.Focus();
                return;
            }

            PopulateWordFields();

            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
           
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="DemandsAdd.aspx.cs" Inherits="MedCSX.Claim.DemandsAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-2">
        Attorney:
    </div>
    <div class="col-md-10">
        <asp:DropDownList ID="ddlAttorney" runat="server"></asp:DropDownList>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        Demads:
    </div>
</div>
<div class="row">
    <div class="col-md-12">
       <asp:GridView ID="gvDemands" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="reserve_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"   ShowFooter="true" Width="50%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                  <%-- <asp:TemplateField>
                          <ItemTemplate>
                               <asp:Button  ID="btnDemansView" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="View" data-toggle="modal" data-target="#theModal"   OnClientClick=<%# FormatPopupDemands(Eval("demand_id").ToString(), "r") %>  /> 
                          </ItemTemplate>
                          <ItemStyle HorizontalAlign="Center" />
                          <FooterTemplate>
                            <asp:Button  ID="btnDemandsAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD" data-toggle="modal" data-target="#theModal"   OnClientClick=<%# FormatPopupDemands("0", "c") %>  /> 
                          </FooterTemplate>
                    </asp:TemplateField>--%>
                      
                
                    <asp:BoundField DataField="reserve_type" HeaderText="Reserve"/>
                    <asp:BoundField DataField="claimant" HeaderText="Claimant"/>
                 <asp:TemplateField HeaderText="Demand Amount">
                          <ItemTemplate>
                              <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                          </ItemTemplate>
                        
                    </asp:TemplateField>
<%--                    <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                              <asp:Button  ID="btnDemandsUpdate" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Update"  data-toggle="modal" data-target="#theModal"  OnClientClick=<%# FormatPopupDemands(Eval("demand_id").ToString(), "u") %>  />
                     
                           
                             <asp:Button  ID="btnDemandsDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete"  data-toggle="modal" data-target="#theModal"  OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDemandsDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>--%>
                

            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
    </div>
</div>
    <div class="row">
<div class="row">
    <div class="col-md-2">
       Demand Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtDemandDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Received Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtReceivedDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-2">
       Due Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtDueDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>

</asp:Content>

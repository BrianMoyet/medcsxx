﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmManualPhoneNumber : System.Web.UI.Page
    {
        private int m_PhoneNumberId;    //populated on edit
        private int m_ClaimId;
        private PhoneNumber m_PhoneNumber;
        private MedCsxLogic.Claim m_Claim;      //populated on new
        string referer = "";

        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_PhoneNumberId = Convert.ToInt32(Request.QueryString["phone_number_id"]);
            m_ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);

            referer = Session["referer"].ToString();

            this.Page.Title = "New Phone Number";

            if ((!Page.IsPostBack) && (m_PhoneNumberId != 0))
                GetGeneralInfo();
        }

        protected void GetGeneralInfo()
        {


            this.Page.Title = "Edit Phone Number";
            this.m_PhoneNumber = new PhoneNumber(this.m_PhoneNumberId);

            this.txtName.Text = this.m_PhoneNumber.Name;
            this.txtCellPhone.Text = this.m_PhoneNumber.CellPhone;
            this.txtEmail.Text = this.m_PhoneNumber.Email;
            this.txtFax.Text = this.m_PhoneNumber.Fax;
            this.txtHomePhone.Text = this.m_PhoneNumber.HomePhone;
            this.txtWorkPhone.Text = this.m_PhoneNumber.WorkPhone;
        }

        protected void PopulateControls()
        {
          
        }
   

       

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnOK_Click(object sender, EventArgs e)
        {

            try
            {
                mm.UpdateLastActivityTime();

                if (this.txtName.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Name must be entered.');</script>");
                    //MessageBox.Show("Name must be entered.", "Missing Name", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.txtName.Focus();
                    return;
                }

                if (m_PhoneNumberId == 0)
                {
                    
                   m_PhoneNumber = new PhoneNumber();
                    m_PhoneNumber.PersonType = "Manual Entry";
                }
                else
                    m_PhoneNumber = new PhoneNumber(m_PhoneNumberId);
               
                this.m_PhoneNumber.Name = this.txtName.Text;
                this.m_PhoneNumber.CellPhone = this.txtCellPhone.Text;
                this.m_PhoneNumber.Email = this.txtEmail.Text;
                this.m_PhoneNumber.HomePhone = this.txtHomePhone.Text;
                this.m_PhoneNumber.WorkPhone = this.txtWorkPhone.Text;
                this.m_PhoneNumber.Fax = this.txtFax.Text;
                this.m_PhoneNumber.PersonType = "Manual Entry";
                this.m_PhoneNumber.ClaimId = m_ClaimId;

                this.m_PhoneNumber.Update();

                Session["referer"] = null;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }


           
        }

        protected void txtHomePhone_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.txtHomePhone.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.txtHomePhone.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < txtHomePhone.Text.Length; i++)
            {
                if (Char.IsDigit(txtHomePhone.Text[i]))
                    p += txtHomePhone.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    txtHomePhone.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    txtHomePhone.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }

        protected void txtWorkPhone_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.txtWorkPhone.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.txtWorkPhone.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < txtWorkPhone.Text.Length; i++)
            {
                if (Char.IsDigit(txtWorkPhone.Text[i]))
                    p += txtWorkPhone.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    txtWorkPhone.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    txtWorkPhone.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }

        protected void txtCellPhone_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.txtCellPhone.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.txtCellPhone.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < txtCellPhone.Text.Length; i++)
            {
                if (Char.IsDigit(txtCellPhone.Text[i]))
                    p += txtCellPhone.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    txtCellPhone.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    txtCellPhone.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }

        protected void txtFax_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.txtFax.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.txtFax.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < txtFax.Text.Length; i++)
            {
                if (Char.IsDigit(txtFax.Text[i]))
                    p += txtFax.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    txtFax.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    txtFax.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Media.Imaging;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmPicker : System.Web.UI.Page
    {
        #region Class Variables
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        public bool okPressed = false;      //was OK pressed?  Output parameter
        public int personId = 0;            //Output parameter
        public User MySelectedUser = null;
        public UserGroup MySelectedGroup = null;
        public string selectedItem = "";
        public int appraisalRequestId = 0,
            appraisalTaskId = 0;

        private State myState = null;       //current state
        private List<PickerGrid> mySelections;      //repository for Selection Grid
        private int _claimId = 0,
            pickMode,
            _claimantId = 0;
        private bool doneLoading = false;
        private MedCsxLogic.Claim m_claim = null;
        private string referer = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            this.pickMode = Convert.ToInt32(Request.QueryString["mode"]);
            IEnumerable myList = null;
            string myTitle = "";
            State thisState = null;

            if (!Page.IsPostBack)
            { 
                LoadForm(pickMode, myTitle, myList, thisState);
            }
        }

          private void LoadForm(int mode, string title2use = "", IEnumerable list = null, State thisState = null)
          {
                this.chkShowStateOnly.Visible = false;

                switch (mode)
                {
                    case 1: //Address Picker
                        //this.Page.Title = "Pick Address";
                        ////this.Icon = new BitmapImage(new Uri("../../Forms/Images/icoHouse.png", UriKind.Relative));
                        //grSelections.DataSource = LoadAddresses(_claimId);
                        //grSelections.DataBind();

                        //SetUpAddressGrid();
                        break;
                    case 2: //Reserve Assignment
                        //this.Title = "Reserve Assignments";
                        //this.Icon = new BitmapImage(new Uri("../../Forms/Images/icoMan.png", UriKind.Relative));
                        //grSelections.ItemsSource = LoadAssignments(mm.GetReserveAssignments());
                        //SetUpAssignmentGrid();
                        break;
                    case 3: //Pick From List
                        //this.Title = title2use;
                        //grSelections.ItemsSource = LoadList(list);
                        //SetUpPickerGrid();
                        break;
                    case 4: //Pick Doctor
                        //this.Title = title2use;
                        //this.Icon = new BitmapImage(new Uri("../../Forms/Images/icoMan.png", UriKind.Relative));
                        //this.myState = thisState;   //set the current state
                        //this.chkShowStateOnly.Content = "Show " + this.myState.Abbreviation + " Only";
                        //grSelections.ItemsSource = LoadDoctors();
                        //SetUpDoctorGrid();
                        break;
                    case 5:  //Pick Appraisal
                        this.m_claim = new MedCsxLogic.Claim(_claimId);
                        this.Page.Title = "Choose Properties To Appraise";
                        //this.Icon = new BitmapImage(new Uri("../../Forms/Images/tbRedCar.png", UriKind.Relative));
                        grSelections.DataSource = LoadAppraisal();
                        grSelections.DataBind();
                        //SetUpAppraisalGrid();
                        break;
                    case 6:  //Pick Claimant
                        //this.Title = "Pick Claimant";
                        //this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuEditUserGroup.png", UriKind.Relative));
                        //grSelections.ItemsSource = LoadClaimants();
                        //SetUpClaimantGrid();
                        break;
                    default:
                        break;
                }
                doneLoading = true;

          }

        private void GetAppraisal()
        {
            try
            {
                mm.UpdateLastActivityTime();

                //AppraisalRequest ar = new AppraisalRequest();
                ////int vehicleId;

                //ar.ClaimId = this._claimId;
                //ar.RequestingUserId = Convert.ToInt32(Session["userID"]);

                

                string msg = "";
                Vehicle v = new Vehicle(Convert.ToInt32(grSelections.DataKeys[grSelections.SelectedIndex].Value));

                PropertyDamage pd = v.PropertyDamage;
                if (pd != null)
                {
                    if ((pd.WhereSeen.Trim().Length == 0) || (pd.DamageDescription.Trim().Length == 0))
                    {
                        msg = "Where Seen and Damage Description are required fields on the Property Damage screen if you are going to request an appraisal.";
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                        //MessageBox.Show(msg, "Missing Information on " + v.ShortName, MessageBoxButton.OK);

                        string url3 = "frmVehicles.aspx?mode=u&claim_id=" + _claimId + "&vehicle_id=" + grSelections.DataKeys[grSelections.SelectedIndex].Value.ToString();
                        string s3 = "window.open('" + url3 + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                        ClientScript.RegisterStartupScript(this.GetType(), "script", s3, true);

                        ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                    }
                }

                
                //if (grSelections.SelectedRow.Cells[1].Text == mm.OTHER_DESC)
                //{

                //    ar.CreateAppraisalTask(0, this._claimantId);
                //}
                //else
                //{

                //    int vid = Convert.ToInt32(grSelections.DataKeys[grSelections.SelectedIndex].Value);
                //    ar.CreateAppraisalTask(vid);
                //}

                //ar.Update();

                //AppraisalTask at = (AppraisalTask)ar.AppraisalTasks[0];
                //at.AppraisalRequestId = ar.AppraisalRequestId;
                //at.AppraisalTaskStatusId = (int)modGeneratedEnums.AppraisalTaskStatus.Appraisal_Requested;
                //at.Update();

                //int index = Request.Url.ToString().IndexOf("?");
                //if (index > 0)
                //    referer = Request.Url.ToString().Substring(0, index);
                //referer += "?claim_id=" + this._claimId + "&Ptabindex=10";

                //Session["referer"] = referer;

                string url2 = "frmAppraisalRequest.aspx?mode=u&claim_id=" + _claimId + "&vehicle_id=" + v.Id;
                string s2 = "window.open('" + url2 + "', 'popup_windowAppraisal', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);


                //foreach (AppraisalTask t in ar.AppraisalTasks)
                //{
                //    if (this.appraisalTaskId == 0)
                //    {
                //        this.appraisalTaskId = t.AppraisalTaskId;
                //        break;
                //    }
                //}
                //this.appraisalRequestId = ar.AppraisalRequestId;

                //ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void grSelections_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pickMode == 5)
            {
                this.btnPickOK.Enabled = false;

                if (grSelections.SelectedRow.Cells[2].Text == "Yes")
                {
                    lblWarning.Visible = true;
                    lblWarning.Text = "An Appraisal Has Already Been Requested for '" + grSelections.SelectedRow.Cells[1].Text + " If you complete Fees Will be Incurred";
                    //ClientScript.RegisterStartupScript(this.GetType(), "msgBox11", "<script>alert('An Appraisal Has Already Been Requested for '" + grSelections.SelectedRow.Cells[2].Text + " If you complete Fees Will be Incurred');</script>");
                    //return;
                    //if (MessageBox.Show("An Appraisal Has Already Been Requested for '" + grSelections.SelectedRow.Cells[2].Text + "'" + Environment.NewLine + Environment.NewLine + "Are You Sure You Want Another Appraisal Request foth this Item? (Fees Will be Incurred)", "Are You Sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //{
                    //    grSelections.SelectedIndex = -1;
                    //    return;
                    //}
                }
                else
                {
                    lblWarning.Text = "";
                    lblWarning.Visible = false;
                }

                    if (grSelections.SelectedRow.Cells[1].Text == mm.OTHER_DESC)
                    {
                        //frmPicker f = new frmPicker(6, this._claimId);
                        //f.ShowDialog();

                        //if (!f.okPressed)
                        //{   //user clicked cancel - uncheck
                        //    mySelection.isSelected = false;
                        //    return;
                        //}

                        //this._claimantId = f.personId;
                    }
                    this.btnPickOK.Enabled = true;
                
            }

            //try
            //{
            //    string userid = grSelections.SelectedRow.Cells[1].Text;
            //    mm.UpdateLastActivityTime();

            //    this.okPressed = true;
            //    this.personId = Convert.ToInt32(grSelections.SelectedRow.Cells[1]);
            //    if (!string.IsNullOrEmpty(grSelections.SelectedRow.Cells[2].Text))
            //        selectedItem = grSelections.SelectedRow.Cells[2].Text;

            //    if (this.pickMode == 5)
            //        GetAppraisal();

                
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}

        }

  

        

        private IEnumerable LoadAppraisal()
        {
            List<Hashtable> vehicles = this.m_claim.VehicleNames;
            int i = 0;
            foreach (Hashtable vData in vehicles)
            {
                vData.Add("Image_Index", i);
                int count = mm.GetAppraisalTasks(this._claimId, (int)vData["Vehicle_Id"]);
                vData.Add("Appraisal_Requested", count == 0 ? "No" : "Yes");
                i++;
                i = i % 3;
            }

            Hashtable veh = new Hashtable();
            veh.Add("Vehicle_Id", 0);
            veh.Add("Image_Index", 3);
            veh.Add("Name", mm.OTHER_DESC);
            int count2 = mm.GetAppraisalTasks(this._claimId, 0);
            veh.Add("Appraisal_Requested", count2 == 0 ? "No" : "Yes");
            vehicles.Add(veh);

            return LoadAppraisal(vehicles);
        }

        protected void btnPickOK_Click(object sender, EventArgs e)
        {

            if (this.pickMode == 5)
                GetAppraisal();
            
        }

      

        private IEnumerable LoadAppraisal(List<Hashtable> vehicles)
        {
            mySelections = new List<PickerGrid>();
            try
            {
                foreach (Hashtable vData in vehicles)
                {
                    mySelections.Add(new PickerGrid()
                    {
                        userId = (int)vData["Vehicle_Id"],
                        image_Idx = (int)vData["Image_Index"],
                        description = (string)vData["Name"],
                        appraisalReq = (string)vData["Appraisal_Requested"]
                    });
                }
                return mySelections;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return mySelections;
            }
        }


        private void SetUpAppraisalGrid()
        {
            GridView myAppraisals = grSelections;
            //this.ImageCol.Visibility = Visibility.Visible;

            //CheckBox col0 = new CheckBox();
            //col0.Header = "";
            //col0.DataBinding = new DataBinding("isSelected");

            BoundField col1 = new BoundField();
            col1.DataField = "description";
            col1.HeaderText = "Property";
            myAppraisals.Columns.Add(col1);

            BoundField col2 = new BoundField();
            col2.DataField = "appraisalReq";
            col2.HeaderText = "Appraisal Requested";
            myAppraisals.Columns.Add(col2);

         

          
        }

        

        class PickerGrid
        {
            //Reserve Assignment Picker
            public int userId { get; set; }
            public Uri imageIdx { get; set; }
            public string userName { get; set; }
            public string userOrGroup { get; set; }
            public int pdToday { get; set; }
            public int pdWeek { get; set; }
            public int biToday { get; set; }
            public int biWeek { get; set; }
            public int TotalClaims { get; set; }

            //Address Picker
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get { return address2 == null ? address1 : address1 + address2; } }
            public string city { get; set; }
            public string state { get; set; }
            public string zipCode { get; set; }
            public string homePhone { get; set; }

            //Pick From List
            public string description { get; set; }

            //Appraisal Picker
            public int image_Idx { get; set; }
            public bool isSelected { get; set; }
            public string appraisalReq { get; set; }
            public BitmapImage bmImage
            {
                get
                {
                    if (imageIdx == null)
                    {
                        switch (image_Idx)
                        {
                            case 1:
                                return new BitmapImage(new Uri("../../Forms/Images/icoBlueCar.png", UriKind.Relative));
                            case 2:
                                return new BitmapImage(new Uri("../../Forms/Images/icoGreenCar.png", UriKind.Relative));
                            case 3:
                                return new BitmapImage(new Uri("../../Forms/Images/icoPaperClip.png", UriKind.Relative));
                            default: //case 0
                                return new BitmapImage(new Uri("../../Forms/Images/icoRedCar.png", UriKind.Relative));

                        }
                    }
                    else
                        return new BitmapImage(imageIdx);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class vehicles : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;
        private string mode = "";
        Vehicle myVehicle = new Vehicle();
        Vehicle updateVehicle = new Vehicle();

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Vehicle";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);
            mode = Request.QueryString["mode"].ToString();

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
               
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
           
            myVehicle.GetVehicleByID(id);
            txtYear.Text = myVehicle.year_made;
            txtMake.Text = myVehicle.manufacturer;
            txtModel.Text = myVehicle.model;
            txtLicensePlateNo.Text = myVehicle.license_plate_no;
            ddlLicenseState.SelectedValue = myVehicle.license_plate_state_id;
            txtVin.Text = myVehicle.vin;
            txtColor.Text = myVehicle.color;
            if (myVehicle.is_drivable == "1")
                chkVehicleisDrivable.Checked = true;

            txtFirstName.Text = myVehicle.location_contact_first_name;
            txtLastName.Text = myVehicle.location_contact_last_name;
            txtEmail.Text = myVehicle.location_contact_email;
            txtAddress1.Text = myVehicle.location_contact_address1;
            txtAddress2.Text = myVehicle.location_contact_address2;
            txtCity.Text = myVehicle.location_city;
            ddlCurrentState.SelectedValue = myVehicle.location_state_id;
            txtCurrentZip.Text = myVehicle.location_zipcode;
            txtPhone1.Text = myVehicle.location_phone1;
            txtPhone2.Text = myVehicle.location_phone2;

            if (myVehicle.policy_vehicle_id != "0")  //personal lines can't change info
            {
                txtYear.Enabled = false;
                txtMake.Enabled = false;
                txtModel.Enabled = false;
                txtVin.Enabled = false;
            }

            txtPLFirstName.Text = myVehicle.loss_payee_name1;
            txtPLLastName.Text = myVehicle.loss_payee_name2;
            txtPLAddress1.Text = myVehicle.loss_payee_address1;
            txtPLAddress2.Text = myVehicle.loss_payee_address2;
            txtPLCity.Text = myVehicle.loss_payee_city;
            ddlPLState_id.SelectedValue = myVehicle.loss_payee_state_id;
            txtPLZip.Text = myVehicle.loss_payee_zipcode;
            txtPLLoanNo.Text = myVehicle.loss_payee_loan_no;
            txtPLPhone.Text = myVehicle.loss_payee_phone;
            txtPLFax.Text = myVehicle.loss_payee_fax;
            txtPLCorrectedPhone.Text = myVehicle.loss_payee_corrected_phone;

            hprViewCarfaxReport.NavigateUrl = "http://www.carfax.com/cfm/check_order.cfm?vin=" + myVehicle.vin.Trim() + "&partner=OST_6_DEC_31&zipcode=&ClickID=&SiteID=&suggest=N&FID=&PopUpStatus=0";

        }
        protected void PopulateControls()
        {
            DataTable dtState = new DataTable();
            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            dtState = myData.PopulateDDLStates();
            ddlLicenseState.DataSource = dtState;
            ddlLicenseState.DataValueField = dtState.Columns[0].ColumnName;
            ddlLicenseState.DataTextField = dtState.Columns[1].ColumnName;
            ddlLicenseState.DataBind();

            ddlCurrentState.DataSource = dtState;
            ddlCurrentState.DataValueField = dtState.Columns[0].ColumnName;
            ddlCurrentState.DataTextField = dtState.Columns[1].ColumnName;
            ddlCurrentState.DataBind();

            ddlPLState_id.DataSource = dtState;
            ddlPLState_id.DataValueField = dtState.Columns[0].ColumnName;
            ddlPLState_id.DataTextField = dtState.Columns[1].ColumnName;
            ddlPLState_id.DataBind();




        }

        public void PopulateVehicle()
        {
            updateVehicle.policy_vehicle_id = id.ToString();
            updateVehicle.year_made = txtYear.Text;
            updateVehicle.manufacturer = txtMake.Text;
            updateVehicle.model = txtModel.Text;
            updateVehicle.license_plate_no = txtLicensePlateNo.Text;
            updateVehicle.license_plate_state_id = this.ddlLicenseState.SelectedValue;
            updateVehicle.location_state_id = this.ddlCurrentState.SelectedValue;
            updateVehicle.claim_id = claim_id.ToString();
            updateVehicle.vin = txtVin.Text;
            updateVehicle.color = txtColor.Text;
            updateVehicle.is_drivable = Convert.ToInt32(chkVehicleisDrivable.Checked).ToString();

            updateVehicle.location_contact_first_name = txtFirstName.Text;
            updateVehicle.location_contact_last_name = txtLastName.Text;
            updateVehicle.location_contact_email = txtEmail.Text;
            updateVehicle.location_contact_address1 = txtAddress1.Text;
            updateVehicle.location_contact_address2 = txtAddress2.Text;
            updateVehicle.location_city = txtCity.Text;
            updateVehicle.location_state_id = ddlCurrentState.SelectedValue;
            updateVehicle.location_zipcode = txtCurrentZip.Text;
            updateVehicle.location_phone1 = txtPhone1.Text;
            updateVehicle.location_phone2 = txtPhone2.Text;

            updateVehicle.loss_payee_name1 = txtPLFirstName.Text;
            updateVehicle.loss_payee_name2 = txtPLLastName.Text;
            updateVehicle.loss_payee_address1 = txtPLAddress1.Text;
            updateVehicle.loss_payee_address2 = txtPLAddress2.Text;
            updateVehicle.loss_payee_city = txtPLCity.Text;
            updateVehicle.loss_payee_state_id = ddlPLState_id.SelectedValue;
            updateVehicle.loss_payee_zipcode = txtPLZip.Text;
            updateVehicle.loss_payee_loan_no = txtPLLoanNo.Text;
            updateVehicle.loss_payee_phone = txtPLPhone.Text;
            updateVehicle.loss_payee_fax = txtPLFax.Text;
            updateVehicle.loss_payee_corrected_phone = txtPLCorrectedPhone.Text;
            updateVehicle.Modified_By = Session["user_id"].ToString();
            updateVehicle.Created_By = Session["user_id"].ToString();
            updateVehicle.Created_Date = HelperFunctions.GetCurrentDate();
            updateVehicle.Modified_Date = HelperFunctions.GetCurrentDate();
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            this.PopulateVehicle();

            if (mode == "c")
            {
                bool dupVehicle = false;

                DataTable dVeh = new DataTable();
                Vehicle cVehicle = new Vehicle();
                dVeh = cVehicle.GetVehicles_By_ClaimId(claim_id);

                foreach (DataRow row in dVeh.Rows)
                {

                    if ((row["year_made"].ToString() == (string)this.txtYear.Text) &&
                        (row["Manufacturer"].ToString() == (string)this.txtMake.Text) &&
                        (row["Model"].ToString() == (string)this.txtModel.Text) &&
                        (row["VIN"].ToString() == (string)this.txtVin.Text) &&
                        (row["LicensePlateNo"].ToString() == (string)this.txtLicensePlateNo.Text))
                    {
                        dupVehicle = true;
                        break;
                    }
                }

                if (dupVehicle)
                {
                    lblAlert.Visible = true;
                    lblAlert.Text = "This vehicle has already been added to this claim.";
                    return;
                }



                

                int successful = updateVehicle.Add();

            }
            else if (mode == "u")
            {
                this.updateVehicle.Update();
            }

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "sCancel", "window.opener.location.href = window.opener.location.href;self.close()", true);

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnAddEditSIU_Click(object sender, EventArgs e)
        {

        }

        protected void btnSetToDriversAddress_Click(object sender, EventArgs e)
        {
            if (this.id == 0)
                return;

            Vehicle v = new Vehicle();
            v.GetVehicleByID(id);
            string DriverPerson  = v.DriverPerson;

            //if (d.Id == 0)
            //    return;
        }

        public bool VIN_Checksum(string VIN)
        {
            char[] vin = VIN.ToUpper().ToCharArray();

            if (vin.Length != 17)
            {
                lblVinMessage.Visible = true;
                lblVinMessage.Text = "VIN is not valid.";
                return false;
            }
            int vValue = 0;
            int remainder;
            int sumProduct = 0;
            string ckSum;
            char pos9 = ' ';
            for (int pos = 0; pos < vin.Length; pos++)
            {
                int product = 0;

                if (pos == 8)
                    pos9 = vin[pos];
                else
                {
                    if (!int.TryParse(vin[pos].ToString(), out vValue))
                        vValue = Transliterate(vin[pos]);

                    product = vValue * VIN_Weight(pos + 1);
                }
                sumProduct += product;
            }
            remainder = sumProduct % 11;

            ckSum = remainder == 10 ? "X" : remainder.ToString();

            lblVinMessage.Visible = false;
            return ckSum == pos9.ToString();
        }

        private int Transliterate(char p)
        {
            switch (p)
            {
                case 'A':
                case 'J':
                    return 1;
                case 'B':
                case 'K':
                case 'S':
                    return 2;
                case 'C':
                case 'L':
                case 'T':
                    return 3;
                case 'D':
                case 'M':
                case 'U':
                    return 4;
                case 'E':
                case 'N':
                case 'V':
                    return 5;
                case 'F':
                case 'W':
                    return 6;
                case 'G':
                case 'P':
                case 'X':
                    return 7;
                case 'H':
                case 'Y':
                    return 8;
                case 'R':
                case 'Z':
                    return 9;
                default:
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "The VIN has invalid character " + p.ToString() + ".";
                    return 0;
            }
        }

        private int VIN_Weight(int p)
        {
            switch (p)
            {
                case 1:
                case 11:
                    return 8;
                case 2:
                case 12:
                    return 7;
                case 3:
                case 13:
                    return 6;
                case 4:
                case 14:
                    return 5;
                case 5:
                case 15:
                    return 4;
                case 6:
                case 16:
                    return 3;
                case 7:
                case 17:
                    return 2;
                case 8:
                    return 10;
                case 10:
                    return 9;
                default:
                    return 0;
            }
        }

        protected void txtVin_TextChanged(object sender, EventArgs e)
        {

            if (txtVin.Text.Length > 0)
            {
                if (txtVin.Text.Contains(" "))
                {
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "The VIN field is not to be used as a note.  This will cause issues within the system.";
                    txtVin.Focus();
                    btnChange.Enabled = false;
                }

                else if (txtVin.Text.Length <= 7)
                {
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "If VIN is unknown, leave the VIN field blank.";
                    txtVin.Focus();
                    btnChange.Enabled = false;
                }

                else if (VIN_Checksum(txtVin.Text) != false)
                {
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "The VIN number is invalid.";
                    this.txtVin.Focus();
                    btnChange.Enabled = false;
                }
                else
                {
                    lblVinMessage.Visible = false;
                    btnChange.Enabled = true;
                }

            }
            else
            {
                lblVinMessage.Visible = false;
                btnChange.Enabled = true;
            }



        }
    }
}
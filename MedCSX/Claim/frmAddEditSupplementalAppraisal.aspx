﻿<%@  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAddEditSupplementalAppraisal.aspx.cs" Inherits="MedCSX.Claim.frmAddEditSupplementalAppraisal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-9">
     
          <asp:GridView ID="grSupplementals" runat="server" AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"   CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top" PageSize="15" ShowFooter="True" FooterStyle-BackColor="White" Width="100%">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
                 
                  <asp:BoundField DataField="Description" HeaderText="Description"/>
                  <asp:BoundField DataField="Amount" HeaderText="Amount" />
                  <asp:BoundField DataField="Appraiser" HeaderText="Appraiser"/>
                 
               </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
              
               <EditRowStyle BackColor="#999999" />
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
    </div>
    <div class="col-md-3">
        <asp:Button ID="btnEditSupplemental" runat="server" Text="Edit" OnClick="btnEditSupplemental_Click" />
        <br />
        <asp:Button ID="btnDeleteSupplemental" runat="server" Text="Button" OnClick="btnDeleteSupplemental_Click" />
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-3">
                Description:
            </div>
            <div class="col-md-9">
                 <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                 Amount: 
            </div>
            <div class="col-md-9">
                <asp:TextBox ID="txtAmount" TextMode="Number"  step=".01" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                 Appraiser: 
            </div>
            <div class="col-md-9">
                <asp:DropDownList ID="cboAppraisers" runat="server"></asp:DropDownList>
            </div>
        </div>

      
      
       
    </div>
    <div class="col-md-3">
        <asp:Button ID="txtAddSupplemental" runat="server" Text="Add" OnClick="txtAddSupplemental_Click" />
    </div>
</div>

 
      <div class="row">
         <div class="col-md-12">
               
                  <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

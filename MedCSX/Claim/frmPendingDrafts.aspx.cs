﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Forms;
using MedCsxLogic;
using MessageBox = System.Windows.Forms.MessageBox;

namespace MedCSX.Claim
{
    public partial class frmPendingDrafts : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {

            //ArrayList pendingDrafts = MedCsXTable.GetObjects(typeof(PendingDraft));

            //List<Hashtable> Rows = new List<Hashtable>();

            //int pending = 0;
            //int complete = 0;
            //int cancelled = 0;

            //foreach (PendingDraft p in pendingDrafts)
            //{
            //    PendingDraftStatus pds = p.PendingDraftStatus;

            //    if (pds.IsCancelled)
            //        cancelled++;
            //    if (pds.IsComplete)
            //        complete++;
            //    if (pds.IsPending)
            //        pending++;

            //    if ((this.rbCancelled.Checked && pds.IsCancelled) || (this.rbCompleted.Checked && pds.IsComplete) || (this.rbPending.Checked && pds.IsPending))
            //    {
            //        // Create the row
            //        Hashtable row = new Hashtable();
            //        row.Add("Pending_Date", p.CreatedDate);
            //        row.Add("Id", p.Id);
            //        row.Add("Image_Index", pds.ImageIndex);
            //        row.Add("Status", pds.Description);
            //        row.Add("Payee", p.Draft.Payee);

            //        if (p.TransactionId == 0)
            //        {
            //            row.Add("Amount", 0.0);
            //        }
            //        else
            //        {
            //            row.Add("Amount", p.Transaction.TransactionAmount * -1.0);
            //        }

            //        //row.Item("Amount") = System.Convert.ToDouble(row.Item("Amount")).ToString("c");

            //        Rows.Add(row);

            //    }

            //    this.rbCompleted.Text = "Completed (" + complete + ")";
            //    this.rbCancelled.Text = "Cancelled (" + cancelled + ")";
            //    this.rbPending.Text = "Pending (" + pending + ")";

            //}


            //this.grPendingDrafts.DataSource = Rows;
            //this.grPendingDrafts.DataBind();
            //this.sbStatus.Text = Rows.Count + " Pending Drafts";

            this.grPendingDrafts.DataSource = mm.GetPendingDrafts();
            this.grPendingDrafts.DataBind();


        }

        protected void rbPending_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPending.Checked)
            {
                LoadData();
            }
        }

        protected void rbCompleted_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCompleted.Checked)
            {
                LoadData();
            }
        }

        protected void rbCancelled_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCancelled.Checked)
            {
                LoadData();
            }
        }

        protected void tsbNew_Click(object sender, EventArgs e)
        {
            string s = Microsoft.VisualBasic.Interaction.InputBox("Enter Claim Number for Pending Draft", "Enter Claim Number");
            if (s == "")
                return;

            s = Regex.Replace(s, "[^0-9]", "");

            int claimId = Convert.ToInt32(s);

            if (claimId == 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Invalid Claim Id Entered');</script>");
                //MessageBox.Show("Invalid Claim Id Entered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

           //MedCsxLogic.Claim cl = new MedCsxLogic.Claim(claimId);

            string url = "frmPickFromList.aspx?mode=c&claim_id=" + claimId;
            string w = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=400, copyhistory=no, left=500, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", w, true);


        }

        protected void tsbFinalize_Click(object sender, EventArgs e)
        {
            if (grPendingDrafts.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a draft.');</script>");
                //System.Windows.Forms.MessageBox.Show("Please select a draft.", "No Draft Selected", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                return;
            }

            int ID = Convert.ToInt32(grPendingDrafts.DataKeys[grPendingDrafts.SelectedIndex].Value);

            //TODO
            string amountStr = Microsoft.VisualBasic.Interaction.InputBox("Enter Amount of Draft", "Enter Draft Amount");

            if (amountStr.Trim().Length < 1)
                return;

            amountStr = amountStr.Replace("$", "").Replace(",", "");

           while (IsNumeric(amountStr) == false)
           {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a numeric amount.');</script>");
                //System.Windows.Forms.MessageBox.Show("Please enter a numeric amount.", "Amount Not Numberic", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);

                //TODO
                amountStr = Microsoft.VisualBasic.Interaction.InputBox("Enter Amount of Draft", "Enter Draft Amount");

                if (amountStr.Trim().Length < 1)
                    return;

                amountStr = amountStr.Replace("$", "").Replace(",", "");

           }

            PendingDraft pd = new PendingDraft(ID);
            pd.FinalizeDraft(Convert.ToDouble(amountStr));


            LoadData();
        }

        public bool IsNumeric(object num, decimal output = 0)
        {
            if (num == null)
                return false;  //num is not initialized

            if ((num.GetType() == typeof(string)) || (num.GetType() == typeof(char)))
                return decimal.TryParse(num.ToString().Replace("$", "").Replace(",", ""), out output);  //num is a string - parse to validate
            else if ((num.GetType() == typeof(Int16)) || (num.GetType() == typeof(Int32)) ||
                (num.GetType() == typeof(Int64)) || (num.GetType() == typeof(Single)) ||
                (num.GetType() == typeof(double)) || (num.GetType() == typeof(decimal)))
            {       //num is numeric
                output = (decimal)num;
                return true;
            }
            else
                return false;  //unknown type, not a number
        }

        //public bool IsNumeric(string Number)
        //{

        //    //// Return TRUE if the number object is an actual number

        //    //if (Number == null)
        //    //    // The number is not initialized
        //    //    return false;

        //    ////if (Number.GetType() == typeof(string) || Number.GetType() == typeof(char))
        //    ////{
        //    ////    int i;
        //    ////    // The number is a string, test it to see if it is an actual number
        //    ////    bool result = int.TryParse(Number, out i);
        //    ////    return result;
        //    ////}
        //    //if ((Number.GetType() == typeof(Int16))
        //    //    || (Number.GetType() == typeof(Int32)) 
        //    //    || (Number.GetType() == typeof(Int64)) 
        //    //    || (Number.GetType() == typeof(float)) 
        //    //    || (Number.GetType() == typeof(double)) 
        //    //    || (Number.GetType() == typeof(decimal)))
        //    //{
        //    //    // The number is an actual number value
        //    //    //OutputNumber = Number;
        //    //    return true;
        //    //}
        //    //else
        //    //    // Unknown type, not a number
        //    //    return false;
        //}

        protected void tsbCancel_Click(object sender, EventArgs e)
        {
            if (grPendingDrafts.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a draft.');</script>");
                //System.Windows.Forms.MessageBox.Show("Please select a draft.", "No Draft Selected", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                return;
            }

            int ID = Convert.ToInt32(grPendingDrafts.DataKeys[grPendingDrafts.SelectedIndex].Value);

            //if (System.Windows.Forms.MessageBox.Show("Are you sure you want to cancel this appraisal?", "Confirm Cancel", System.Windows.Forms.MessageBoxButtons.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
            //    return;

            PendingDraft pd = new PendingDraft(ID);
            pd.PendingDraftStatusId = 3;
            pd.Update();

            LoadData();
        }

        protected void tsbOpen_Click(object sender, EventArgs e)
        {
            if (grPendingDrafts.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a draft.');</script>");
                //System.Windows.Forms.MessageBox.Show("Please select a draft.", "No Draft Selected", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                return;
            }

            int ID = Convert.ToInt32(grPendingDrafts.DataKeys[grPendingDrafts.SelectedIndex].Value);

            string url = "frmPendingDraft.aspx?pending_draft_id=" + ID;
            string w = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=800, copyhistory=no, left=500, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", w, true);
        }
    }
}
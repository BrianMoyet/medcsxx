﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmNewTask : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private bool claimLocked = false;
        private MedCsxLogic.Claim myClaim;
        private int claimId = 0,
           taskId = 0,  selectedUserId = 0,
            selectedUserGroupId = 0;
        public DataTable DataRowsDT = new DataTable();
        private List<ClaimantGrid> myClaimants;
        private Claimant m_Claimant = null;
        private UserTask m_UserTask = null;


        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (tbClaim.Enabled == true)
            { 
                claimId = Convert.ToInt32(Regex.Replace(tbClaim.Text, "[^0-9]", ""));

                if (claimId == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Valid MedCsX Claim Number.');</script>");
                    //MessageBox.Show("Please Enter a Valid MedCsX Claim Number.", "Missing Claim Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    tbClaim.Focus();
                    return;
                }
            }

            if ((cboClaimants.Enabled == true) && (cboClaimants.Text == ""))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Claimant.');</script>");
                //MessageBox.Show("Please Enter a Claimant.", "Claimant Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                cboClaimants.Focus();
                return;
            }

            if ((cboReserves.Enabled == true) && (cboReserves.Text == ""))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Reserve.');</script>");
                //MessageBox.Show("Please Enter a Reserve.", "Reserve Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                cboReserves.Focus();
                return;
            }

            if (cboTaskType.Text.Trim() == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Choose a Task Type.');</script>");
                //MessageBox.Show("You Must Choose a Task Type.", "Missing Task Type", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                cboTaskType.Focus();
                return;
            }

            if (tbDescription.Text.Trim() == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter a Task Description.');</script>");
                //MessageBox.Show("You Must Enter a Task Description.", "Missing Task Description", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                tbDescription.Focus();
                return;
            }

            string msg = "";

            if (this.SimilarTaskCreated())
            {
                msg = "A " + this.cboTaskType.SelectedItem.Text + " Task Has Already Been Created";
                if (this.cboClaimants.Enabled)
                {
                    if (this.cboReserves.Enabled)
                        msg += " for this Reserve.";
                    else
                        msg += " for this Claimant.";
                }
                else
                    msg += " for this Claim.";

                msg += " Do You Want to Continue Creating This Task?";

                //MessageBox.Show(msg, "Similar Task Already Created", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                //TODO
                //if (MessageBox.Show(msg, "Similar Task Already Created", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
                  
                //    return;

            }

            //save task
            UserTask ut = new UserTask();
            ut.ClaimId = this.myClaim.Id;

            if (this.cboClaimants.Enabled)
                ut.ClaimantId = Convert.ToInt32(cboClaimants.SelectedValue);
            else
                ut.ClaimantId = 0;

            if (this.cboReserves.Enabled)
                ut.ReserveId = Convert.ToInt32(cboReserves.SelectedValue);
            else
                ut.ReserveId = 0;

            ut.Description = this.tbDescription.Text;
            ut.UserTaskStatusId = (int)modGeneratedEnums.UserTaskStatus.Task_Created;
            ut.UserTaskTypeId = Convert.ToInt32(cboTaskType.SelectedValue);
            ut.Update();



            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
        }

        private bool SimilarTaskCreated()
        {
            int claim_id = 0;
            int claimant_id = 0;
            int reserve_id = 0;

            if (tbClaim.Enabled == true)
                claim_id = Convert.ToInt32(Regex.Replace(tbClaim.Text, "[^0-9]", ""));
            

            if (cboClaimants.Enabled)
                claimant_id = Convert.ToInt32(cboClaimants.SelectedValue);

            if (cboReserves.Enabled)
                reserve_id = Convert.ToInt32(cboReserves.SelectedValue);

            //int claim_id = this.claimId;
            //int claimant_id = Convert.ToInt32(cboClaimants.SelectedValue);
            //int reserve_id = Convert.ToInt32(cboReserves.SelectedValue);
            int task_id = Convert.ToInt32(cboTaskType.SelectedValue);
            Reserve res = new Reserve(reserve_id);

            if (res != null)
                claimant_id = res.ClaimantId;

            //get similar task count
            foreach (Hashtable row in mm.GetNumTasks(task_id, claim_id, claimant_id, reserve_id))
            {
                if ((int)row["c"] == 0)
                    return false;
                return true;
            }
            return false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            this.taskId = Convert.ToInt32(Request.QueryString["task_id"]);
            this.myClaim = new MedCsxLogic.Claim(claimId);

            this.tbClaim.Text = myClaim.DisplayClaimId;
            // LoadAssignedTo()
           // LoadTaskTypes();
            if (!Page.IsPostBack)
            {
                LoadCreateNewTask();
               // LoadClaimants();
            }
        }

        private void LoadCreateNewTask()
        {
            this.tbClaim.Text = myClaim.DisplayClaimId;
            this.tbClaim.Enabled = false;

            bool taskTypeLoaded = LoadTaskType();
            bool taskClaimantLoaded = LoadClaimants();
            this.cboClaimants.Enabled = false;
            this.cboClaimants.Enabled = false;
        }

        private bool LoadTaskType()
        {
            mf.LoadTypeTableDDL(this.cboTaskType, "User_Task_Type", true);
            cboTaskType_SelectedIndexChanged(null, null);
            return true;
        }

        //private void LoadTaskTypes()
        //{
        //    mf.LoadTypeTableDDL(this.cboTaskType, "User_Task_Type", true);
            
        //}

        protected void cboClaimants_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadReserves();
        }

        private void DisplayClaimants()
        {
            bool claimantsLoaded = LoadClaimants();

            if (this.claimLocked || this.myClaim.isClosed)
                return;
        }

        protected void cboTaskType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mySelection = cboTaskType.SelectedValue;
            if (mySelection == null)
                return;

            UserTaskType utt = new UserTaskType(cboTaskType.SelectedItem.ToString());

            if (utt.CreateUserTaskLevel().isReserveLevel)
            {
                this.cboClaimants.Enabled = true;
                this.cboReserves.Enabled = true;
            }
            else if (utt.CreateUserTaskLevel().isClaimantLevel)
            {
                this.cboClaimants.Enabled = true;
                this.cboReserves.Enabled = false;
            }
            else
            {
                this.cboClaimants.Enabled = false;
                this.cboReserves.Enabled = false;
            }
        }

        private bool LoadClaimants()
        {
            DataRowsDT = mm.GetMyClaimants(this.claimId);
            //this.myClaimants = mm.LoadClaimants(this.myClaim.PersonNamesAndTypes, 9);//LoadPerson(this.myClaim.PersonNamesAndTypes);
            DataRow newRowR = DataRowsDT.NewRow();
            newRowR["claimant_Id"] = "0";
            newRowR["person"] = "";

            DataRowsDT.Rows.InsertAt(newRowR, 0);
            this.cboClaimants.DataSource = DataRowsDT;
            this.cboClaimants.DataTextField = "person";
            this.cboClaimants.DataValueField = "claimant_Id";
            this.cboClaimants.DataBind();

            return true;
        }

        //private void LoadClaimants()
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();

        //        List<Hashtable> Claimants = this.myClaim.ClaimantNamesAndTypes;
        //        this.cboClaimants.LoadPersonRows(Claimants);
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //    }
        //}

        private void LoadReserves()
        {
            DataTable DataRows = new DataTable();
            DataRows = mm.GetReserveNamesForClaimant(Convert.ToInt32(this.cboClaimants.SelectedValue));

            DataRow newRowR = DataRows.NewRow();
            newRowR["reserve_name"] = "";
            DataRows.Rows.InsertAt(newRowR, 0);

            this.cboReserves.DataSource = mm.GetReserveNamesForClaimant(Convert.ToInt32(this.cboClaimants.SelectedValue));
            this.cboReserves.DataTextField = "reserve_name";
            this.cboReserves.DataValueField = "reserve_id";
            this.cboReserves.DataBind();

        }

        private Claimant Claimant()
        {
            if (this.cboClaimants.Enabled == true)
                return new Claimant(this.myClaim, this.cboClaimants.SelectedValue);

            return null/* TODO Change to default(_) if this is not a reference type */;
        }

       
      





    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAddEditSalvage : System.Web.UI.Page
    {
        public int SalvageId = 0,          //salvage_id for Salvage object.  Input/Output
               VehicleId = 0,          //vehicle_id for Vehicle object.  Input/Output
               VendorId = 0,           //vendor_id for Vendor object.  Input/Output
               AppraisalRequestId = 0,  //appraisal_request_id for Appraisal Request object.  Input/Output
               AppraisalTaskId = 0;    //appraisal_task_id for Appraisal Task object.  Input/Output
        public bool SalvageComplete = false,    //is this salvage complete?
            okPressed = false;          //was OK button pressed?

        private int ClaimId = 0;
        private List<VehicleGrid> myVehicleList;
        private bool is_Add = false;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();


        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Salvage salv;
                if (Convert.ToInt32(Request.QueryString["salvage_id"]) == 0)
                    salv = new Salvage();
                else
                    salv = new Salvage(this.SalvageId);
               

                double storageChgs = 0;
                if (!string.IsNullOrEmpty(this.txtStorageDays.Text) && !string.IsNullOrEmpty(this.txtStoragePerDay.Text))
                {
                    int iDays = int.Parse(this.txtStorageDays.Text);
                    string tStorage = (this.txtStoragePerDay.Text).Replace("$", "").Replace(",", "");
                    double dStorage = double.Parse(tStorage);
                    if ((iDays > 0) && (dStorage > 0))
                    {
                        storageChgs = (double)(dStorage * iDays);
                        salv.StorageDays = iDays;
                        salv.StorageCharges = storageChgs;
                    }
                }

                salv.Towing_Charges = double.Parse((this.txtTowing.Text).Replace("$", "").Replace(",", ""));
                salv.SalvageYardCharges = double.Parse((this.txtOtherAmount.Text).Replace("$", "").Replace(",", ""));
                if (this.txtDtStorageEnd.Text != "")
                    salv.DateCompleted = DateTime.Parse(this.txtDtStorageEnd.Text);

               

                this.SalvageId = salv.Update();

                if (this.txtDtStorageEnd.Text != "")
                    this.SalvageComplete = true;

                this.okPressed = true;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

       

        protected void txtStorageDays_TextChanged(object sender, EventArgs e)
        {
            string myDays = ((TextBox)sender).Text.Trim();
            if (string.IsNullOrEmpty(myDays))
                return;

            int iDays = 0;
            if (!int.TryParse(myDays, out iDays))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Valid Number.');</script>");
                //MessageBox.Show("Please Enter a Valid Number.", "Invalid Days", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.txtStorageDays.Focus();
                return;
            }

            if (iDays < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Valid Number.');</script>");
                //MessageBox.Show("Please Enter a Valid Number.", "Invalid Days", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.txtStorageDays.Focus();
                return;
            }

            if ((iDays > 0) && string.IsNullOrEmpty(this.txtStoragePerDay.Text))
                this.txtStoragePerDay.Focus();
        }

        protected void txtTowing_TextChanged(object sender, EventArgs e)
        {
           
                string myCost = ((TextBox)sender).Text.Trim();
                if (string.IsNullOrEmpty(myCost))
                    return;

                myCost = myCost.Replace("$", "").Replace(",", "");

                double dCost = 0;
                if (!double.TryParse(myCost, out dCost))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Valid Number.');</script>");
                    //MessageBox.Show("Please Enter a Valid Number.", "Invalid Towing Cost", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.txtTowing.Focus();
                    return;
                }

                if (dCost < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Valid Number.');</script>");
                    //MessageBox.Show("Please Enter a Valid Number.", "Invalid Towing Cost", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.txtTowing.Focus();
                    return;
                }

                this.txtTowing.Text = dCost.ToString("c");
           
        }

        protected void cboSalvageVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            int mySelection = Convert.ToInt32(cboSalvageVehicle.SelectedValue);
            if (mySelection == 0)
                return;

            if (mySelection == 0)
                return;

            this.VehicleId = mySelection;
        }

        protected void txtVendorId_TextChanged(object sender, EventArgs e)
        {
            string myTaxId = txtVendorId.Text.Trim();
            if (!mm.VendorExists(myTaxId, 1))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Tax ID entered does not belong to any Vendors in the Database.  Please Enter a Valid Tax Id, or use the Lookup to Find a Vendor.');</script>");
                //MessageBox.Show("The Tax ID entered does not belong to any Vendors in the Database.  Please Enter a Valid Tax Id, or use the Lookup to Find a Vendor.", "Invalid Tax Id", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                txtVendorId.Focus();
                return;
            }

            this.VendorId = mm.GetVendorIdForTaxId(this.txtVendorId.Text);
            PopulateVendorControls();
        }

        private void PopulateVendorControls()
        {
            try
            {
                mm.UpdateLastActivityTime();

                Vendor v = new Vendor(this.VendorId);

                if (!string.IsNullOrEmpty(v.TaxId))
                    this.txtVendorId.Text = v.TaxId;

                this.txtVendorName.Text = v.Name;
                this.txtVendorLocation.Text = v.Description;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtVendorLookup_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;





            //PopulateVendorControls();
        }

        protected void LookUpVendor()
        {
            mm.UpdateLastActivityTime();

            Session["refererVendor"] = Request.Url.ToString();

            //string lookupValue = Microsoft.VisualBasic.Interaction.InputBox("Enter Vendor Name (or first part of one):", "Search Vendors by Name");
            string lookupValue = txtVendorSearch.Text;
            if (lookupValue == "")
                return;

            //show vendor search results
            //show vendor search results
            string url = "frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
            string s2 = "window.open('" + url + "', 'popup_window4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

            vendorlookup.Visible = false;
        }


        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            LookUpVendor();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.SalvageId = Convert.ToInt32(Request.QueryString["salvage_id"]);
            this.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
            this.VendorId = new Salvage(this.SalvageId).VendorId;
            this.VehicleId = Convert.ToInt32(Request.QueryString["vehicle_id"]);



            if (!Page.IsPostBack)
            {
                if (Session["vendorID"] != null)
                {
                    //vendorId = Convert.ToInt32(Session["vendorID"]);
                    Vendor myVendor = new Vendor(Convert.ToInt32(Session["vendorID"]));
                    txtVendorId.Text = myVendor.TaxId.ToString();
                    txtVendorId_TextChanged(null, null);
                    Session["vendorID"] = null;

                }
                if (this.SalvageId != 0)
                {
                    Salvage sal = new Salvage(SalvageId);
                    this.Page.Title = "Claim " + sal.Claim().DisplayClaimId + " - Salvage";

                    if (this.VendorId != 0)
                    {
                        Vendor v = new Vendor(VendorId);
                        this.txtVendorId.Text = v.TaxId;
                        this.txtVendorName.Text = v.Name;
                        this.txtVendorLocation.Text = v.Description;
                        ArrayList copart = GetCopartIds();
                        if (copart.Contains(VendorId))
                            this.rbLoc_Copart.Checked = true;
                    }
                    else
                    {
                        this.txtVendorId.Text = "";
                        this.txtVendorName.Text = "";
                        this.txtVendorLocation.Text = "";
                    }

                    if (this.VehicleId != 0)
                    {
                        this.Page.Title += " for " + new Vehicle(VehicleId).ShortName;
                        PopulateVehicles(this.VehicleId);
                        this.cboSalvageVehicle.Enabled = false;
                    }
                    else
                    {
                        PopulateVehicles();
                        is_Add = true;
                    }
                        

                    this.txtStorageDays.Text = sal.StorageDays.ToString();
                    this.txtTowing.Text = sal.Towing_Charges.ToString("c");
                    if (sal.StorageDays > 0)
                    {
                        double costPerDay = (double)(sal.StorageCharges / sal.StorageDays);
                        this.txtStoragePerDay.Text = costPerDay.ToString("c");
                    }
                    else
                        this.txtStoragePerDay.Text = "$0.00";
                    this.txtOtherAmount.Text = sal.SalvageYardCharges.ToString("c");
                    this.txtOtherDescription.Text = sal.SalvageYardDescription;
                    this.txtDtStorageStart.Text = sal.CreatedDate.ToString("yyyy-MM-dd");
                    if (sal.DateCompleted > ml.DEFAULT_DATE)
                        this.txtDtStorageEnd.Text = sal.DateCompleted.ToString("yyyy-MM-dd");
                    else
                        this.txtDtStorageEnd.Text = "";

                    if (this.AppraisalRequestId != 0)
                    {
                        foreach (AppraisalTask t in new AppraisalRequest(AppraisalRequestId).AppraisalTasks)
                        {
                            if (t.AppraisalTaskId == this.AppraisalTaskId)
                            {
                                this.txtDtAppraiserAssigned.Text = t.CreatedDate.ToString("yyyy-MM-dd");
                                break;
                            }
                        }
                    }
                }
                else
                {
                    this.Page.Title = "Claim " + new MedCsxLogic.Claim(this.ClaimId).DisplayClaimId + " - Salvage";
                    PopulateVendorControls();
                    this.txtVendorName.Text = "";
                    this.txtVendorId.Text = "";
                    this.txtVendorLocation.Text = "";
                    this.txtStorageDays.Text = "";
                    this.txtTowing.Text = "$0.00";
                    this.txtStoragePerDay.Text = "$0.00";
                    this.txtOtherAmount.Text = "$0.00";
                    this.txtOtherDescription.Text = "Description";
                    PopulateVehicles();
                }


            }
        }

        private void PopulateVehicles(int p = 0)
        {
            this.cboSalvageVehicle.DataSource = PopulateVehicles(new MedCsxLogic.Claim(this.ClaimId).Vehicles);
            this.cboSalvageVehicle.DataValueField = "vehicleId";
            this.cboSalvageVehicle.DataTextField = "vShortName";
            this.cboSalvageVehicle.DataBind();
            this.cboSalvageVehicle.SelectedIndex = setSelection(p);
        }

        private ArrayList GetCopartIds()
        {
            ArrayList copartIds = new ArrayList();
            List<Hashtable> results = mm.SearchVendors("Name", "Copart", 1);
            foreach (Hashtable v in results)
                copartIds.Add((int)v["Vendor_Id"]);
            return copartIds;
        }

        private int setSelection(int p)
        {
            int idIdx = 0;
            if (p == 0)
                return idIdx;

            foreach (VehicleGrid v in myVehicleList)
            {
                if (v.vehicleId == p)
                    break;
                idIdx++;
            }
            return idIdx;
        }

        private IEnumerable PopulateVehicles(List<Hashtable> list)
        {
            myVehicleList = new List<VehicleGrid>();
            try
            {
                myVehicleList.Add(new VehicleGrid()
                {
                    vehicleId = 0,
                    vModel = "Select Vehicle..."
                });
                foreach (Hashtable veh in list)
                {
                    if ((((string)veh["Manufacturer"]).Length < 7) || ((((string)veh["Manufacturer"]).Length >= 7) && (((string)veh["Manufacturer"]).Substring(0, 7).ToUpper() != "UNKNOWN")))
                    {
                        myVehicleList.Add(new VehicleGrid()
                        {
                            vehicleId = (int)veh["Vehicle_Id"],
                            //claimId = (int)veh["Claim_Id"],
                            //policyId = (int)veh["Policy_Vehicle_Id"],
                            //vYear = int.Parse((string)veh["Year_Made"]),
                            //vMake = (string)veh["Manufacturer"],
                            //vModel = (string)veh["Model"],
                            //vVIN = (string)veh["VIN"],
                            //vColor = (string)veh["Color"],
                            vShortName = int.Parse((string)veh["Year_Made"]) + " " + (string)veh["Manufacturer"] + " " + (string)veh["Model"] + " " + (string)veh["VIN"]
                        });
                    }
                }
                return myVehicleList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myVehicleList;
            }
        }
    }
}
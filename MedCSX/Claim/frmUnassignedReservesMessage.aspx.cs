﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmUnassignedReservesMessage : System.Web.UI.Page
    {
        public string _claimId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            _claimId = Request.QueryString["claim_id"].ToString();
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            string url = "frmAssignReserves.aspx?mode=z&claim_id=" + _claimId;
            string s = "window.open('" + url + "', 'popup_windowur', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=800, copyhistory=no, left=400, top=100');window.close()";
            ClientScript.RegisterStartupScript(this.GetType(), "scriptur", s, true);
        }
    }
}
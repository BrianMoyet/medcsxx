﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmOffer : System.Web.UI.Page
    {
        public bool okPressed = false;

        private int m_vendorId = 0;
        private int m_reserveId = 0;
        private int m_demandId = 0;
        private int m_ClaimId = 0;
        private int m_offerSentId = 0;

        private List<ReserveGrid> myDemandHistoryList;
        private List<cboList> mySentTypeList;
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            m_vendorId = Convert.ToInt32(Request.QueryString["vendor_id"]);
            m_reserveId = Convert.ToInt32(Request.QueryString["reserve_id"]);
            m_ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);

            if (!Page.IsPostBack)
            {
                mm.UpdateLastActivityTime();
                LoadGrid();
                this.cboOfferType.DataSource = LoadSentType();
                cboOfferType.DataTextField = "description";
                cboOfferType.DataValueField = "id";
                cboOfferType.DataBind();

                if (this.grDemandHistory.Rows.Count > 0)
                {
                    //int rwinx = Convert.ToInt32(grDemandHistory.DataKeys[this.grDemandHistory.Rows.Count - 1].Value);
                    //ReserveGrid lastOffer = this.myDemandHistoryList[rwinx];
                    this.m_demandId = Convert.ToInt32(grDemandHistory.DataKeys[this.grDemandHistory.Rows.Count - 1].Value);
                }
            }
        }

        protected void LoadGrid()
        {
            this.grDemandHistory.DataSource = LoadDemandOffer(Demand.DemandHistory(m_vendorId, m_reserveId));
            this.grDemandHistory.DataBind();
        }

        private IEnumerable LoadSentType()
        {
            mySentTypeList = new List<cboList>();
            try
            {
                foreach (Hashtable sData in TypeTable.getTypeTableRows("Offer_Sent_Type"))
                {
                    mySentTypeList.Add(new cboList()
                    {
                        id = (int)sData["Id"],
                        imageIdx = (int)sData["Image_Index"],
                        description = (string)sData["Description"]
                    });
                }
                return mySentTypeList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return mySentTypeList;
            }
        }

        private IEnumerable LoadDemandOffer(List<Hashtable> list)
        {
            string s;
            myDemandHistoryList = new List<ReserveGrid>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    ReserveGrid cg = new ReserveGrid();
                    cg.id = (int)dData["ID"];
                    cg.reserveType = (string)dData["Type"];
                    cg.dtOfLoss = (DateTime)dData["Date"];
                    s = (string)dData["Amount"];
                    s = s.Trim().Replace("$", "").Replace(",", "");
                    cg.netLossReserve = double.Parse(s);
                    if (dData["Received_Date"].ToString() != "")
                        cg.dtReported = (DateTime)dData["Received_Date"];
                    if (dData["Due_Date"].ToString() != "")
                        cg.dtDue = (DateTime)dData["Due_Date"];
                    myDemandHistoryList.Add(cg);
                }
                return myDemandHistoryList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDemandHistoryList;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            //validation
            string txtAmount = this.txtOfferAmount.Text.Trim().Replace("$", "").Replace(",", "");
            double dAmount = 0;
            bool tmp = double.TryParse(txtAmount, out dAmount);
            if (string.IsNullOrEmpty(txtAmount) || (tmp == false))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The demand amount is invalid.');</script>");
                //MessageBox.Show("The demand amount is invalid.", "Invalid Demand Amount", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.txtOfferAmount.Focus();
                return;
            }

            if (this.cboOfferType.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a valid sent type.');</script>");
                //MessageBox.Show("Please select a valid sent type.", "No Sent Type Selected", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.cboOfferType.Focus();
                return;
            }

            if (this.grDemandHistory.Rows.Count > 0)
            {
                  this.m_demandId = Convert.ToInt32(grDemandHistory.DataKeys[this.grDemandHistory.Rows.Count - 1].Value);
            }

            if (this.dpDemandDate.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter an Offer Date.');</script>");
                //MessageBox.Show("Please enter an Offer Date.", "Offer Date Not Entered", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.dpDemandDate.Focus();
                return;
            }


            //create the new offer
            Offer myOffer = new Offer();
            myOffer.DemandId = this.m_demandId;
            myOffer.OfferSentTypeId = Convert.ToInt32(cboOfferType.SelectedValue);
            myOffer.OfferAmount = dAmount;
            myOffer.OfferDate = Convert.ToDateTime(this.dpDemandDate.Text);

            if (myOffer.Demand.DemandDate > myOffer.OfferDate)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The offer date must be after the last demand date.');</script>");
                //MessageBox.Show("The offer date must be after the last demand date.", "Invalid Offer Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.dpDemandDate.Focus();
                return;
            }

            myOffer.Update();

            //fire the offer made event
            Hashtable parms = new Hashtable();
            parms.Add("Offer_Amount", myOffer.OfferAmount);
            if (myOffer.Demand.VendorId == 0)
                parms.Add("Attorney", myOffer.Demand.Reserve.Claimant.Name);  //no vendor selected - use claimant
            else
                parms.Add("Attorney", myOffer.Demand.Vendor.Name);
            parms.Add("Claimant", myOffer.Demand.Reserve.Claimant.Name);
            parms.Add("Reserve", myOffer.Demand.Reserve.ReserveType.Description);
            myOffer.Demand.Reserve.FireEvent((int)modGeneratedEnums.EventType.Offer_Made, parms);

            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);

        }

        class cboList
        {
            public int id { get; set; }
            public string description { get; set; }
            public int imageIdx { get; set; }
        }
    }
}
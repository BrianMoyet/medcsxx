﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAddEditSalvage.aspx.cs" Inherits="MedCSX.Claim.frmAddEditSalvage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <b>Vehicle</b>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Tax Id:
            </div>
            <div class="col-md-10">
                <asp:TextBox ID="txtVendorId" runat="server" OnTextChanged="txtVendorId_TextChanged" AutoPostBack="true"></asp:TextBox>
                <br />Don't Know Tax Id?  <asp:LinkButton ID="txtVendorLookup" OnClick="txtVendorLookup_Click" CausesValidation="false" runat="server">Lookup Vendor</asp:LinkButton>
            </div>
        </div>
        <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                        Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnVendorSearch_Click"  runat="server" Text="Search" />
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Name:
            </div>
            <div class="col-md-10">
                <asp:Label ID="txtVendorName" runat="server" Text="lblName"></asp:Label>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Location:
            </div>
            <div class="col-md-10">
                <asp:Label ID="txtVendorLocation" runat="server" Text="lblLocation"></asp:Label>
            </div>
        </div>
    </div>
    <div class="col-md-6">
       <div class="row">
            <div class="col-md-12">
                <b>Fees</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                Storage Days:
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtStorageDays" runat="server" OnTextChanged="txtStorageDays_TextChanged" AutoPostBack="true" TabIndex="1"></asp:TextBox>
               
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Cost Per Day:
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtStoragePerDay" runat="server"  OnTextChanged="txtStorageDays_TextChanged" TabIndex="2" ></asp:TextBox>
               
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
               Towing:
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtTowing" runat="server" OnTextChanged="txtTowing_TextChanged" AutoPostBack="true" TabIndex="3"></asp:TextBox>
               
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                Other Charges Description:
            </div>
               <div class="col-md-8">
                Other Amount:
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                <asp:TextBox ID="txtOtherDescription" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="col-md-8">
                    <asp:TextBox ID="txtOtherAmount" runat="server" TabIndex="5"></asp:TextBox>           
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                Vehicle: 
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="**" ControlToValidate="cboSalvageVehicle" MaximumValue="999999999" MinimumValue="1" Text="Select Vehicle" ForeColor="Red"></asp:RangeValidator>
                <br /><asp:DropDownList ID="cboSalvageVehicle" runat="server" OnSelectedIndexChanged="cboSalvageVehicle_SelectedIndexChanged" AutoPostBack="true" TabIndex="6"></asp:DropDownList>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <b>Dates</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                Storage Began:
            </div>
             <div class="col-md-8">
                 <asp:TextBox ID="txtDtStorageStart" runat="server" TextMode="Date" TabIndex="7"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                Appraiser Assigned:
            </div>
             <div class="col-md-8">
                 <asp:TextBox ID="txtDtAppraiserAssigned" runat="server" TextMode="Date" TabIndex="8"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
               Completed:
            </div>
             <div class="col-md-8">
                 <asp:TextBox ID="txtDtStorageEnd" runat="server" TextMode="Date" TabIndex="9"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <b>Location</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:RadioButton ID="rbLoc_Copart" GroupName="rbLocation" runat="server" Text="At Copart" TabIndex="10" selected="true" />
                <asp:RadioButton ID="rbLoc_Self" GroupName="rbLocation" runat="server" Text="OwnerRetained" TabIndex="11" />
                <asp:RadioButton ID="rbLoc_Other" GroupName="rbLocation" runat="server" Text="Other" TabIndex="12" />
                <asp:RadioButton ID="rbLoc_Abandon" GroupName="rbLocation" runat="server" Text="Abandon" TabIndex="13" />
                
            </div>
        </div>
    </div>
</div>
    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click" TabIndex="14"/>
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

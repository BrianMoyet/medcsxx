﻿<%@ Page Title="Find Policy for New Claim" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmFindPolicy.aspx.cs" Inherits="MedCSX.Claim.frmFindPolicy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="DivSearch" runat="server">
    <div class="row">
        <div class="col-md-12">
            Date of Loss: <asp:TextBox ID="txtDateOfLoss" runat="server" TextMode="Date"></asp:TextBox>
            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDateOfLoss" ErrorMessage="**" Font-Bold="True" ForeColor="Red">**</asp:RequiredFieldValidator>
            &nbsp;&nbsp;Policy No: <asp:TextBox ID="txtPolicyNo" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
           First Name:&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            &nbsp; &nbsp; &nbsp;Last Name: <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            SSN: <asp:TextBox ID="txtSSN" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
           Address: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
            &nbsp;
             &nbsp; &nbsp;City: <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            State: <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList> &nbsp;Zip: <asp:TextBox ID="TextBox1" runat="server" Width="60px"></asp:TextBox>

            Zip: <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 bg-light">
            Vehicle 
            </div>
    </div>
     <div class="row">
     <div class="col-md-12">
           Auto Year:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtAutoYear" runat="server"></asp:TextBox>
            &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Make: <asp:TextBox ID="txtMake" runat="server"></asp:TextBox>
            Model: <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>
            VIN: <asp:TextBox ID="txtVIN" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
             
         <div class="col-md-12">
               <asp:Button ID="btnSearch" runat="server" class="btn btn-info btn-xs"  Text="Search"   OnClick="btnSearch_Click" CausesValidation="False"  />&nbsp;<asp:Label ID="lblInvalidSearch" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
               <br /><br />
        </div>
    </div>
      <div class="row">
        <div class="col-md-12 bg-info ">
            <b>Policies Found 
            <br />
            <asp:RadioButtonList ID="rblActivePolicies" runat="server" OnSelectedIndexChanged="rblActivePolicies_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="1">Show Active Policies</asp:ListItem>
                <asp:ListItem Value="0">Show Expired Policies</asp:ListItem>
            </asp:RadioButtonList>
            </b>
            </div>
    </div>
    <div class="row">
         <div class="col-md-12">
          <asp:GridView ID="gvPoliciesFound" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True"   OnSelectedIndexChanged="gvPoliciesFound_SelectedIndexChanged" CellPadding="4" DataKeyNames=""   ShowHeaderWhenEmpty="True" EmptyDataText="No Results Found." 
                    ForeColor="#333333"  Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
            
             
                        <asp:BoundField DataField="Policy" HeaderText="Policy"  SortExpression="Policy"/>
                        <asp:BoundField DataField="version" HeaderText="Version No"  SortExpression="version"/>
                        <asp:BoundField DataField="Eff_Dt" HeaderText="Eff Date" SortExpression="Eff_Dt"/>
                        <asp:BoundField DataField="Exp_Dt" HeaderText="Exp Date" SortExpression="Exp_Dt"/>
                        <asp:BoundField DataField="First_Name" HeaderText="First Name" SortExpression="first_name"/>
                        <asp:BoundField DataField="last_name" HeaderText="Last Name" SortExpression="last_name"/>
                        <asp:BoundField DataField="Year" HeaderText="Year" SortExpression="Year"/>
                        <asp:BoundField DataField="Make" HeaderText="Make" SortExpression="Make"/>
                        <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model"/>
                        <asp:BoundField DataField="VIN" HeaderText="VIN" SortExpression="VIN"/>
                      
                       
            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
             </div>
    </div>
       <div class="row">
        <div class="col-md-12  text-center ">
            <b>Policy Details </b>
            </div>
    </div>
     <div class="row">
        <div class="col-md-12 text-center">
         <b>Individuals on Policy</b>
            </div>
    </div>
      <div class="row">
         <div class="col-md-12">
          <asp:GridView ID="gvPolicyIndividuals" runat="server"   AutoGenerateColumns="False" CellPadding="4" DataKeyNames="no"   ShowHeaderWhenEmpty="True" EmptyDataText="No Results Found." 
                    ForeColor="#333333"   Width="100%" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
            
                        <asp:BoundField DataField="No" HeaderText="No"  SortExpression=""/>
                        <asp:BoundField DataField="Name" HeaderText="Name"  SortExpression=""/>
                        <asp:BoundField DataField="Type" HeaderText="Type"  SortExpression=""/>
                        <asp:BoundField DataField="address_1" HeaderText="Address" SortExpression=""/>
                        <asp:BoundField DataField="Home_phone" HeaderText="Home Phone" SortExpression=""/>
                        <asp:BoundField DataField="date_of_birth" HeaderText="Date of Birth" SortExpression=""/>
                        <asp:BoundField DataField="age" HeaderText="Age" SortExpression=""/>
                        <asp:BoundField DataField="sex" HeaderText="Sex" SortExpression=""/>
                        <asp:BoundField DataField="marital_status" HeaderText="Marital Status" SortExpression=""/>
                        
                      
                       
            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
             </div>
    </div>
     <div class="row">
        <div class="col-md-12 text-center">
         <b>Vehicles on Policy</b>
            </div>
    </div>
      <div class="row">
         <div class="col-md-12">
          <asp:GridView ID="gvPolicyVehicles" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="no"   ShowHeaderWhenEmpty="True" EmptyDataText="No Results Found." 
                    ForeColor="#333333"   Width="100%" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
            
             
                        <asp:BoundField DataField="No" HeaderText="Vehicle No"  SortExpression=""/>
                        <asp:BoundField DataField="Lvl" HeaderText="Level"  SortExpression=""/>
                        <asp:BoundField DataField="Year" HeaderText="Year" SortExpression=""/>
                        <asp:BoundField DataField="Make" HeaderText="Nake" SortExpression=""/>
                        <asp:BoundField DataField="Model" HeaderText="Model" SortExpression=""/>
                        <asp:BoundField DataField="Vin" HeaderText="VIN" SortExpression=""/>
                        <asp:BoundField DataField="Age" HeaderText="Age" SortExpression=""/>
                        
                      
                       
            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
             </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Company: <asp:DropDownList ID="ddlCompany" runat="server"  OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCompany" ErrorMessage="*" ForeColor="Red">*</asp:RequiredFieldValidator>
        </div>
         <div class="col-md-3">
            Location: <asp:DropDownList ID="ddlLocation" runat="server"></asp:DropDownList>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLocation" ErrorMessage="*" ForeColor="Red">*</asp:RequiredFieldValidator>
        </div>
         <div class="col-md-3">
            Cov Type: <asp:DropDownList ID="ddlCovType" runat="server"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
         <div class="col-md-12">
              <asp:Button ID="btnContinueWithPolicy" runat="server" class="btn btn-info btn-xs"  Text="Use This Policy and Continue"   OnClick="btnContinueWithPolicy_Click" Enabled="False"  />
             <asp:Button ID="btnContinueWithoutPolicy" runat="server" class="btn btn-warning btn-xs"  Text="Continue Without Policy"  OnClientClick="return confirm('You have selected  as the Company for this Claim.  Is this correct?');"  OnClick="btnContinueWithoutPolicy_Click"  />
              <asp:Button ID="btnCancel" runat="server" class="btn btn-danger pull-right"  Text="Cancel"    OnClientClick="window.close(); return false;" CausesValidation="False" />
         </div>
     </div>
</div>
    <div runat="server" id="DivPossibleDuplicatesFound" visible="false">
        <div class="row">
    <div class="col-md-12">
         <h4>Possilbe Duplicate Claims Found - Please Investigate</h4>
    </div>
</div>
    <div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvPossibleDuplicateClaims" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="possible_duplicate_claim_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  AllowPaging="True" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                  
                       <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + "=window.open('claim.aspx?tabindex=18&claim_id=" + DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" +  DataBinder.Eval(Container.DataItem,"duplicate_claim_id").ToString() + ".focus()" %> Runat="Server">
                            View</asp:Hyperlink>

                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="claim_id" HeaderText="Claim" />
                        <asp:BoundField DataField="policy_no" HeaderText="Policy" />
                         <asp:BoundField DataField="date_of_loss" HeaderText="Date of Loss"  />
                         <asp:BoundField DataField="description" HeaderText="Match Description" />
              
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
                      
                </asp:GridView>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnCreateNewClaimAnyways" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Create New Claim Anyway"     OnClick="btnCreate_Click"  />
        <asp:Button ID="btnCancelDuplicates" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
    </div>
</div>

    </div>

</asp:Content>

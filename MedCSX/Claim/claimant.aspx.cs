﻿using MedCSX.App_Code;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class claimant : System.Web.UI.Page
    {
        private int claim_id;
        private string request_id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;
        private Claimants myClaimant = new Claimants(); //'Claimant class object for this claimant

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Claimant";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            request_id = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");


            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();


                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        public void GetGeneralInfo()
        {

            ddlClaimant.SelectedValue = request_id;
            string display = HelperFunctions.ExtendedDetails(request_id);

            lblClaimantDetails.Text = display;
        }

        public void PopulateDDLs()
        {
            DataTable dt = new DataTable();
            Claimants myData = new Claimants();
            dt = myData.GetClaimantsForClaim(claim_id);
            ddlClaimant.DataSource = dt;
            ddlClaimant.DataValueField = dt.Columns[0].ColumnName;
            ddlClaimant.DataTextField = dt.Columns["Name"].ToString();
            ddlClaimant.DataBind();
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            string user_id = System.Web.HttpContext.Current.Session["user_id"].ToString();

            myClaimant.person_id = ddlClaimant.SelectedValue;
            myClaimant.claim_id = claim_id.ToString();
            myClaimant.Created_By = user_id;
            myClaimant.Modified_By = user_id;
            myClaimant.Modified_Date = HelperFunctions.GetCurrentDate();
            myClaimant.Created_Date = HelperFunctions.GetCurrentDate();
            int createdClaimant = myClaimant.AddClaimant();

            Hashtable parms = new Hashtable();
            parms.Add("Claimant_ID", createdClaimant);
            parms.Add("Person_ID", myClaimant.person_id);
            parms.Add("Claim_ID", claim_id);

            int successful = ModMain.ClaimantCreatedEventHandler((int)ModGeneratedEnums.EventType.Claimant_Added, parms);

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "sCancel", "window.opener.location.href = window.opener.location.href;self.close()", true);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlClaimant_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblClaimantDetails.Text = HelperFunctions.ExtendedDetails(ddlClaimant.SelectedValue);
            
        }

        //public void ExtendedDetails(string id)
        //{
           
        //    string display = "";

        //    Claimants myData = new Claimants();
        //    myData.GetPersonByID(Convert.ToInt32(id));
        //    display += myData.salutation + " " + myData.first_name + " " + myData.middle_initial + " " + myData.last_name + " " + myData.suffix;
        //    if ((myData.address1 != "") || (myData.city != "") || (myData.state != "") || (myData.zipcode != ""))
        //    { 
        //        display += "<br />" + myData.address1 + " " + myData.address2;
        //        display += "<br />" + myData.city + ", " + myData.state + " " + myData.zipcode;
        //    }
        //    if ((myData.home_phone != "") && (CountDigits(myData.home_phone) > 0))
        //         display += "<br />" + "Home Phone: " + myData.home_phone;
        //    if ((myData.work_phone != "") && (CountDigits(myData.work_phone) > 0))
        //        display += "<br />" + "Work Phone: " + myData.work_phone;
        //    if ((myData.cell_phone != "") && (CountDigits(myData.cell_phone) > 0))
        //        display += "<br />" + "Cell Phone: " + myData.cell_phone;
        //    if ((myData.other_contact_phone != "") && (CountDigits(myData.other_contact_phone) > 0))
        //        display += "<br />" + "Other Contact Phone: " + myData.other_contact_phone;
        //    if ((myData.fax != "") && (CountDigits(myData.fax) > 0))
        //        display += "<br />" + "Fax: " + myData.fax;
        //    if ((myData.email != "") && (myData.email.Length > 0))
        //        display += "<br />" + "Email: " + myData.email;
        //    if ((myData.date_of_birth != "1/1/1900 12:00:00 AM") && (myData.date_of_birth.Length > 0))
        //        display += "<br />" + "Date of Birth: " + myData.date_of_birth;
        //    if (myData.ssn  != "")
        //        display += "<br />" + "SSN: " + myData.ssn;
        //    if (myData.drivers_license_no != "")
        //        display += "<br />" + "Drivers License No: " + myData.drivers_license_no;
        //    if (myData.sex != "")
        //        display += "<br />" + "Sex: " + myData.sex;
        //    if (myData.employer != "")
        //        display += "<br />" + "Employer: " + myData.employer;
        //    if (myData.language_id != "")
        //        display += "<br />" + "Employer: " + HelperFunctions.GetLanguageByID(Convert.ToInt32(myData.language_id));

        //    lblClaimantDetails.Text = display;
        //}

        //public int CountDigits( string s)
        //{
        //    int i;
        //    int count;

        //    count = 0;
        //    for (i = 0; i <= s.Length - 1; i++)
        //    {
        //        if (IsNumeric(s.Substring(i, 1)))
        //            count = count + 1;
        //    }
        //    return count;
        //}

        //static bool IsNumeric(object Expression)
        //{
        //    // Variable to collect the Return value of the TryParse method.
        //    bool isNum;

        //    // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
        //    double retNum;

        //    // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
        //    // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
        //    isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        //    return isNum;
        //}
    }
}
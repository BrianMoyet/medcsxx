﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmFraud : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        private MedCsxLogic.Claim m_Claim;
        private Vendor m_Vendor;
        private Person m_Person;
        private Vehicle m_Vehicle;
        private int m_Mode = 0;
        private bool activated = false;

        int perpetrator = 0;
        int claim_id = 0;
        int mode = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
           perpetrator = Convert.ToInt32(Request.QueryString["perpetrator"]);
           claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
           mode = Convert.ToInt32(Request.QueryString["mode"]);

            this.m_Mode = mode;
            switch (m_Mode)
            {
                case 1:     //vendor
                    this.m_Vendor = new Vendor(perpetrator);
                    //this.m_Claim = new Claim(claim_id);
                    break;
                case 2:     //person
                    this.m_Person = new Person(perpetrator);
                    //this.m_Claim = new Claim(claim_id);
                    break;
                case 4:     //vehicle
                    this.m_Vehicle = new Vehicle(perpetrator);
                    break;
                default:    //claim_id
                    this.m_Claim = new MedCsxLogic.Claim(claim_id);
                    break;
            }

            DataTable fraudClaims;
            switch (this.m_Mode)
            {
                case 1:     //vendor
                    fraudClaims = mm.GetFraudClaims(this.m_Vendor.Id, 0, 0, 0);
                    this.Title = "Fraud Claims for " + this.m_Vendor.Name;
                    break;
                case 2:     //person
                    fraudClaims = mm.GetFraudClaims(0, this.m_Person.Id, 0, 0);
                    this.Title = "Fraud Claims for " + this.m_Person.Name;
                    break;
                case 4:
                    fraudClaims = mm.GetFraudClaims(0, 0, this.m_Vehicle.Id, 0);
                    this.Title = "Fraud Claims for " + this.m_Vehicle.ShortName;
                    break;
                default:
                    fraudClaims = mm.GetFraudClaims(0, 0, 0, this.m_Claim.ClaimId);
                    this.Title = "Fraud Claims for Claim " + this.m_Claim.DisplayClaimId;
                    break;
            }

            this.grFraud.DataSource = fraudClaims;
            this.grFraud.DataBind();
            this.grFraud.SelectedIndex = 0;

            //LoadFraud(fraudClaims);
            //  make them click the add fraud buttton  this.btnAddFraud_Click(null, null);

        }

        private bool LoadFraud(DataTable fraudClaims)
        {
            //this.btnEditFraud.Enabled = false;
            //this.btnDeleteFraud.Enabled = false;

            if (fraudClaims.Rows.Count <= 0)
                return false;

            List<fraudList> myFraudList = new List<fraudList>();
            try
            {
                foreach (DataRow fData in fraudClaims.Rows)
                {
                    fraudList fl = new fraudList();
                    var vendorValue = fData.ItemArray[1];
                    var personValue = fData.ItemArray[2];
                    var vehicleValue = fData.ItemArray[3];
                    var claimValue = fData.ItemArray[4];
                    int testInt;

                    fl.id = (int)fData.ItemArray[0];
                    if (int.TryParse(vendorValue.ToString(), out testInt))
                        fl.vendorId = (int)fData.ItemArray[1];
                    if (int.TryParse(personValue.ToString(), out testInt))
                        fl.personId = (int)fData.ItemArray[2];
                    if (int.TryParse(vehicleValue.ToString(), out testInt))
                        fl.vehicleId = (int)fData.ItemArray[3];
                    if (int.TryParse(claimValue.ToString(), out testInt))
                        fl.claimId = (int)fData.ItemArray[4];
                    fl.personType = (Int16)fData.ItemArray[5];
                    fl.fraudType = (int)fData.ItemArray[6];
                    fl.description = (string)fData.ItemArray[7];
                    
                    myFraudList.Add(fl);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            this.grFraud.DataSource = myFraudList;
            this.grFraud.DataBind();
            this.grFraud.SelectedIndex = 0;
            return true;
        }

        class fraudList
        {
            public int id { get; set; }
            public int vendorId { get; set; }
            public int personId { get; set; }
            public int claimId { get; set; }
            public int vehicleId { get; set; }
            public int personType { get; set; }
            public int fraudType { get; set; }
            public string description { get; set; }
            public string name
            {
                get
                {
                    if (vendorId != 0)
                        return new Vendor(vendorId).Name;
                    else if (personId != 0)
                        return new Person(personId).Name;
                    else if (vehicleId != 0)
                        return new Vehicle(vehicleId).ShortName;
                    else
                        return " ";
                }
            }
            public string displayClaimId
            {
                get { return new MedCsxLogic.Claim(claimId).DisplayClaimId; }
            }
            public string displayType
            {
                get
                {
                    if (vendorId != 0)
                    {
                        if (personId != 0)
                            return "Both";
                        else
                            return "Vendor";
                    }
                    else if (personId != 0)
                        return "Person";
                    else if (vehicleId != 0)
                        return "Vehicle";
                    return " ";
                }
            }
        }

        protected void btnAddFraud_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //frmAddFraud f;

                //switch (m_Mode)
                //{
                //    case 1:     //vendor
                //        f = new frmAddFraud(this.m_Vendor);
                //        break;
                //    case 2:     //person
                //        f = new frmAddFraud(this.m_Person);
                //        break;
                //    case 4:     //vehicle
                //        f = new frmAddFraud(this.m_Vehicle);
                //        break;
                //    default:    //claim_id
                //        f = new frmAddFraud(this.m_Claim);
                //        break;
                //}


               

                Session["referer3"] = Request.Url.ToString();

                string url = "frmAddFraud.aspx?mode=" + m_Mode + "&claim_id=" + claim_id + "&perpetrator=" + perpetrator;
                string s = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=500, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "popup_window3", s, true);
                //f.Show();

                //if (f.OkPressed)
                //    this.activated = false;
                //else
                //    this.Close();
                //Window_Loaded(null, null);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnDeleteFraud_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!this.btnDeleteFraud.Enabled)
                    return;

                if (grFraud.SelectedIndex < 0)
                    return;

                
                int mySelection = Convert.ToInt32(grFraud.DataKeys[grFraud.SelectedIndex].Value);   //not a valid instance

                

                //if (MessageBox.Show("Are you sure you wish to delete this fraud claim?", "Confirm Delete", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                //    return;     //did not confirm

                Fraud f = new Fraud(mySelection);

                f.DeleteRow();

                DataTable fraudClaims;
                switch (this.m_Mode)
                {
                    case 1:     //vendor
                        fraudClaims = mm.GetFraudClaims(this.m_Vendor.Id, 0, 0, 0);
                        this.Page.Title = "Fraud Claims for " + this.m_Vendor.Name;
                        break;
                    case 2:     //person
                        fraudClaims = mm.GetFraudClaims(0, this.m_Person.Id, 0, 0);
                        this.Page.Title = "Fraud Claims for " + this.m_Person.Name;
                        break;
                    case 4:
                        fraudClaims = mm.GetFraudClaims(0, 0, this.m_Vehicle.Id, 0);
                        this.Page.Title = "Fraud Claims for " + this.m_Vehicle.ShortName;
                        break;
                    default:
                        fraudClaims = mm.GetFraudClaims(0, 0, 0, this.m_Claim.ClaimId);
                        this.Page.Title = "Fraud Claims for Claim " + this.m_Claim.DisplayClaimId;
                        break;
                }

                LoadFraud(fraudClaims);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }
    }
}
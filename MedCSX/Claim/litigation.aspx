﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master"  CodeBehind="litigation.aspx.cs" Inherits="MedCSX.Claim.litigation" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-3">
            Status:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Defense Attorney:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="ddlDefenseAttorney" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Plantiff Attorney:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="ddlPlantiffAttorney" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Court Location:                   
    </div>
    <div class="col-md-9">
            <asp:DropDownList ID="ddlCourtLocation" runat="server"></asp:DropDownList>             
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Date Filed:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="txtDateFiled" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Date Served:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="txtDateServed" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-3">
            Resolution Amount:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="txtResolutionAmount" runat="server"></asp:TextBox>        
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
          Comments:                   
    </div>
    <div class="col-md-9">
        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="250px"></asp:TextBox>        
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
          Reserves:                   
    </div>
    <div class="col-md-9">
         <asp:GridView ID="gvReserves" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="reserve_id"  
                    ForeColor="#333333"   AllowPaging="True" CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" PageSize="15" ShowHeader="true">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                      
                        <asp:BoundField DataField="Reserve_Type" HeaderText="Reserve Type"/>
                        <asp:BoundField DataField="claimant" HeaderText="Claimant"/>  
            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                   <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>   
    </div>
</div>
    <div class="row">
    <div class="col-md-3">
          <br />Defendants:                   
    </div>
    <div class="col-md-9">
          <br /><asp:GridView ID="gvDefendants" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="vendor_id"  
                    ForeColor="#333333"   AllowPaging="True" CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" PageSize="15" ShowHeader="true" ShowFooter="true">
                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Name" >
                        <ItemTemplate>
                            <asp:Label runat="server" Text=<%# Eval("Person_Name")%>></asp:Label>
                        </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                      <FooterTemplate>
          <asp:Button  ID="btnDefendantsAdd" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="ADD"  data-toggle="modal" data-target="#theModal"  OnClientClick=<%# FormatPopupDefendants("0", "c") %>  /> 
    </FooterTemplate>
                        </asp:TemplateField>
                    
                        <asp:BoundField DataField="vendor_Name" HeaderText="vendor"/>  
                        <asp:TemplateField HeaderText="" >
                        <ItemTemplate>
                             
                           
                             <asp:Button  ID="btnDefendantsDelete" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Delete" data-toggle="modal" data-target="#theModal"   OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDefendantsDelete_Click"  />
                           
                        </ItemTemplate>
                             </asp:TemplateField>
            </Columns>
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>   
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:content>

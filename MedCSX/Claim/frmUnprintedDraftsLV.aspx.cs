﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmUnprintedDraftsLV : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue) == false)
            {
                btnPrintKC.Enabled = false;
                btnPrintLV.Enabled = false;
            }

            if (!Page.IsPostBack)
            {
                mm.UpdateLastActivityTime();
                LoadDrafts();
                sbStatus.Text = grDrafts.Rows.Count.ToString() + " Drafts in Queue";
            }
        }

        private void LoadDrafts()
        {
            this.grDrafts.DataSource = mm.GetDraftQueueLV();
            this.grDrafts.DataBind();
        }

        protected void rblSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblSelect.SelectedValue == "1")
            {
                for (int i = 0; i < grDrafts.Rows.Count; i++)
                {

                    CheckBox mycheck = ((CheckBox)grDrafts.Rows[i].FindControl("cbSelect"));
                    mycheck.Checked = true;

                }
            }
            else
            {
                for (int i = 0; i < grDrafts.Rows.Count; i++)
                {

                    CheckBox mycheck = ((CheckBox)grDrafts.Rows[i].FindControl("cbSelect"));
                    mycheck.Checked = false;

                }
            }
        }

        protected void btnPrintLV_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue))
                return;

            ArrayList drafts = new ArrayList();

            for (int i = 0; i < grDrafts.Rows.Count; i++)
            {
                GridViewRow row = grDrafts.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {
                   
                    int ID = Convert.ToInt32(grDrafts.DataKeys[row.RowIndex].Value);

                    Draft d = new Draft(ID);

                    drafts.Add(d);
                }
            }

            if (drafts.Count < 1)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('No drafts selected to print.');</script>");
                return;
            }

            int counter = 0;
            try
            {
                foreach (Draft d in drafts)
                {
                    bool overlimit = false;
                    if (d.DraftAmount > Convert.ToDouble(mm.LVDraftSignLimitAmount))
                        overlimit = true;

                    if (d.InDraftQueue == 1)
                    {

                        foreach (DraftPrint dp in d.ChildObjects(typeof(DraftPrint)))
                        {
                            dp.NonNegotiable = ""; // 'just in case it was viewed and set to non-negotiable
                            dp.Update();

                            string msg = "";
                            if (mf.PrintDraftForDraftPrintId(dp.Id, ref msg, overlimit))
                            {
                                d.isPrinted = true;
                            }
                            else
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");


                        }

                        d.InDraftQueue = 0;
                        d.ActualPrintDate = System.DateTime.Now;
                        d.ReleasedFromQueueBy = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                        d.Update();

                        counter++;
                    }
                }


            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Error occurred while printing the drafts..');</script>");
                //MessageBox.Show("Error occurred while printing the drafts.", "Error While Printing Drafts", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            LoadDrafts();
            sbStatus.Text = grDrafts.Rows.Count.ToString() + " Drafts in Queue";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + counter.ToString() + " Drafts Printed.');</script>");
        }

        protected void btnPrintKC_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue))
                return;

            ArrayList drafts = new ArrayList();

            for (int i = 0; i < grDrafts.Rows.Count; i++)
            {
                GridViewRow row = grDrafts.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {

                    int ID = Convert.ToInt32(grDrafts.DataKeys[row.RowIndex].Value);

                    Draft d = new Draft(ID);

                    drafts.Add(d);
                }
            }

            if (drafts.Count < 1)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('No drafts selected to print.');</script>");
                return;
            }

            int counter = 0;
            try
            {
                foreach (Draft d in drafts)
                {
                    if (d.InDraftQueue == 1)
                    {

                        foreach (DraftPrint dp in d.ChildObjects(typeof(DraftPrint)))
                        {
                            dp.NonNegotiable = ""; // 'just in case it was viewed and set to non-negotiable
                            dp.Update();

                            string msg = "";
                            if (mf.PrintDraftForDraftPrintId(dp.Id, ref msg, false, mm.draftPrinterKC))
                            {
                                d.isPrinted = true;
                            }
                            else
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");


                        }

                        d.InDraftQueue = 0;
                        d.ActualPrintDate = System.DateTime.Now;
                        d.ReleasedFromQueueBy = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                        d.Update();

                        counter++;

                    }
                }


            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Error occurred while printing the drafts..');</script>");
                //MessageBox.Show("Error occurred while printing the drafts.", "Error While Printing Drafts", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            LoadDrafts();
            sbStatus.Text = grDrafts.Rows.Count.ToString() + " Drafts in Queue";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + counter.ToString() + " Drafts Printed.');</script>");
        }
    }
}
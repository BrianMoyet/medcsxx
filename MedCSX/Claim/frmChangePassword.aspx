﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/ChangePassword.Master" AutoEventWireup="true" CodeBehind="frmChangePassword.aspx.cs" Inherits="MedCSX.Claim.frmChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <b>&nbsp;&nbsp;&nbsp;Change Password for: </b> <asp:Label ID="lblUser" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            &nbsp;&nbsp;&nbsp;Old Password:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOldPassword" ForeColor="Red" ErrorMessage="**"></asp:RequiredFieldValidator>
        </div>
         <div class="col-md-9">
             &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtOldPassword" TextMode="Password" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           &nbsp;&nbsp;&nbsp;New Password: <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNewPassword" ForeColor="Red" ErrorMessage="**"></asp:RequiredFieldValidator>
        </div>
         <div class="col-md-9">
            &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
          &nbsp;&nbsp;&nbsp;Confirm Password:
        </div>
         <div class="col-md-9">
            &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="row">
    <div class="col-md-12">
       &nbsp;&nbsp;&nbsp;<asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click"  /> <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="New and Confirm passwords must match!" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" ForeColor="Red"></asp:CompareValidator>
           
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

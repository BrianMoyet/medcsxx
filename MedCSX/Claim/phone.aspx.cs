﻿using MedCSX.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class phone : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Add Phone Number";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
           


        }

        protected void PopulateControls()
        {
          
        }
   

       

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

      
    }
}

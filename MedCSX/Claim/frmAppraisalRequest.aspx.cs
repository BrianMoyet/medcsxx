﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Media.Imaging;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAppraisalRequest : System.Web.UI.Page
    {
        private AppraisalRequest m_AppraisalRequest = null;
        private AppraisalTask m_AppraisalTask = null;
        private AppraisalResponse m_AppraisalResponse = null;
        private bool isReadOnly = false;
        private int currentImageIdx = 0;
        private MedCsxLogic.Claim myClaim = null;
        private int claimId;

        private List<cboGrid> myInvestigationList;
        private List<cboGrid> myAppraiserList;
        private List<ListViewGrid> myTaskList;
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private Vehicle m_vehicle = new Vehicle();
        //private AppraisalRequest ar = new AppraisalRequest();
        string referer = "";
        Hashtable parms = new Hashtable();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["vehicle_id"] != null)
            {
                m_vehicle = new Vehicle(Convert.ToInt32(Request.QueryString["vehicle_id"]));
                m_AppraisalTask = new AppraisalTask();
                this.m_AppraisalRequest = new AppraisalRequest();
            }
            else
            { 
                this.m_AppraisalTask = new AppraisalTask(Convert.ToInt32(Request.QueryString["appraisal_task_id"]));
                this.m_AppraisalRequest = this.m_AppraisalTask.AppraisalRequest;
                this.m_AppraisalResponse = mm.GetAppraisalResponseForTask(Convert.ToInt32(Request.QueryString["appraisal_task_id"]));
                m_vehicle = new Vehicle(m_AppraisalTask.VehicleId);
            }

            this.claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            this.myClaim = new MedCsxLogic.Claim(claimId);

            if (Session["referer2"] != null)
            {
                referer = Session["referer2"].ToString();
                Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();

            //the appraisal is read-only
            //this.btnOK.Enabled = false;
            //this.btnEditClaimant.Enabled = false;
            //this.btnEditInsured.Enabled = false;
            //this.btnEditVehicle.Enabled = false;
            //this.btnEditVehicleLoc.Enabled = false;
            //this.lvTasks.Enabled = false;
            //this.isReadOnly = true;

            try
            {
                mm.UpdateLastActivityTime();

                //is user authorized to resend requests?
                this.btnResend.Enabled = true;
                this.Page.Title = "Appraisal Request";

                if (!Page.IsPostBack)
                {
                    m_AppraisalRequest.ClaimId = this.claimId;
                    m_AppraisalRequest.RequestingUserId = Convert.ToInt32(Session["userID"]);

                    if (m_vehicle.Id == 0)
                    {

                        m_AppraisalRequest.CreateAppraisalTask(0, m_vehicle.Claimant.ClaimantId);
                    }
                    else
                    {

                        int vid = m_vehicle.Id;
                        m_AppraisalRequest.CreateAppraisalTask(vid);
                    }


                    bool investigationTypeLoaded = LoadInvestigationType();
                    LoadAppraisers();
                    LoadTasks();
                    txtClaimType.Text = myClaim.ClaimType.Description;
                    //if (m_AppraisalTask != null)
                    if (Request.QueryString["vehicle_id"] != null) 
                    {
                        m_AppraisalTask = new AppraisalTask(m_AppraisalRequest, m_vehicle.Id);

                        this.txtClaimId.Text = this.m_AppraisalTask.Claim().DisplayClaimId;
                        this.txtPolicyNo.Text = this.m_AppraisalTask.Claim().PolicyNo;
                        this.txtDtLoss.Text = Convert.ToDateTime(this.m_AppraisalTask.Claim().DateOfLoss).ToString("yyyy-MM-dd");
                        this.txtDeductible.Text = this.m_AppraisalTask.DeductibleAmount.ToString();
                        this.txtClaimType.Text = this.m_AppraisalTask.TypeOfClaim;
                        this.txtInstructions.Text = this.m_AppraisalTask.SpecialInstructions;
                        this.txtTaskDescrip.Text = this.m_AppraisalTask.TaskDescription;
                        this.txtLossDescrip.Text = this.m_AppraisalTask.LossDescription;
                        if (this.m_AppraisalTask.Vehicle().PropertyDamage != null)
                            this.txtInitialEst.Text = this.m_AppraisalTask.Vehicle().PropertyDamage.EstimatedDamageAmount.ToString();
                       

                        /*** Insurance Company ***/
                        this.txtCompany.Text = this.m_AppraisalTask.InsuranceCoDescription.Replace("\r\n", "<br />");

                        /*** Insured ***/
                        this.txtInsured.Text = this.m_AppraisalTask.InsuredDescription.Replace("\r\n", "<br />");

                        /*** Claimant ***/
                        this.txtClaimant.Text = this.m_AppraisalTask.ClaimantDescription.Replace("\r\n", "<br />");

                        /*** Vehicle ***/
                        this.txtVehicle.Text = this.m_AppraisalTask.VehicleDescription.Replace("\r\n", "<br />");

                        /*** Vehicle Location ***/
                        this.txtVehicleLoc.Text = this.m_AppraisalTask.VehicleLocationDescription.Replace("\r\n", "<br />");

                        //this.cboInvestigationType.SelectedValue = this.m_AppraisalTask.AppraisalInvestigationTypeId.ToString();
                        //string vendorName = this.m_AppraisalTask.SceneAccessAppraiser.Vendor.Name;
                        //int inx = setSelection(vendorName);
                        //this.cboAppraiser.SelectedIndex = inx;

                        /*** Response Data  ***/
                        //if (this.m_AppraisalResponse != null)
                        //{
                        //    if (this.m_AppraisalResponse.Id != 0)
                        //    {
                        //        if (m_AppraisalTask.AppraisalTaskId == 0)
                        //        {
                        //            this.txtInitialEst.Text = "";
                        //        }
                        //        else
                        //        {
                        //            this.txtInitialEst.Text = this.m_AppraisalResponse.InitialEstimate.ToString();
                        //        }
                        //        if (this.m_AppraisalResponse.DateInitialEstimate == null)
                        //        {
                        //            this.m_AppraisalResponse.DateInitialEstimate = ml.DEFAULT_DATE;
                        //        }
                        //        if (this.m_AppraisalResponse.DateInitialEstimate != ml.DEFAULT_DATE)
                        //            this.dtInitialEst.Text = this.m_AppraisalResponse.DateInitialEstimate.ToString("MM/dd/yyyy hh:mm:ss tt");
                        //        else
                        //            this.dtInitialEst.Text = "";


                        //    }
                        //    else
                        //    {
                        //        getGoodAppraisalResponse();
                        //    }
                        //    if (m_AppraisalResponse.AppraisalTaskId > 0)
                        //        this.lblTtlEstimate.Text = TotalEstimate();

                        //    getGoodAppraisalResponse();
                        //}
                    }
                    else
                    {
                        if (m_AppraisalTask.AppraisalTaskId > 0)
                        {
                            RefreshTaskData();
                        }
                        

                    }
                    //select the first appraisal investigation type
                    if ((this.cboInvestigationType.Items.Count > 0) && (this.cboInvestigationType.SelectedIndex == -1))
                        this.cboInvestigationType.SelectedIndex = 0;

                    if (Request.QueryString["mode"].ToString() == "u")
                    {
                        btnOK.Enabled = true;
                        btnResend.Enabled = false;
                    }
                    else
                    {
                        btnOK.Enabled = false;
                        btnResend.Enabled = true;
                    }
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e



                    string sessionName = "ar" + this.claimId.ToString();

                    if (Session[sessionName] != null)
                    { 
                        populateFormFromSessionVariables();

                    }
<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            if (!mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Resend_Appraisal_Requests))
                this.btnResend.Enabled = false;

        }

        private void RefreshTaskData()
        {
            if (this.m_AppraisalTask.SceneAccessAppraiserId > 0)
            {
                btnOK.Enabled = false;
            }


            /***  General ***/
            this.txtClaimId.Text = this.m_AppraisalTask.Claim().DisplayClaimId;
            this.txtPolicyNo.Text = this.m_AppraisalTask.Claim().PolicyNo;
            this.txtDtLoss.Text = Convert.ToDateTime(this.m_AppraisalTask.Claim().DateOfLoss).ToString("yyyy-MM-dd");
            this.txtDeductible.Text = this.m_AppraisalTask.DeductibleAmount.ToString();
            this.txtClaimType.Text = this.m_AppraisalTask.TypeOfClaim;
            this.txtInstructions.Text = this.m_AppraisalTask.SpecialInstructions;
            this.txtTaskDescrip.Text = this.m_AppraisalTask.TaskDescription;
            this.txtLossDescrip.Text = this.m_AppraisalTask.LossDescription;

            this.cboInvestigationType.SelectedValue = this.m_AppraisalTask.AppraisalInvestigationTypeId.ToString();
            string vendorName = this.m_AppraisalTask.SceneAccessAppraiser.Vendor.Name;
            int  inx = setSelection(vendorName);
            this.cboAppraiser.SelectedIndex = inx;

            /*** Response Data  ***/
            if (this.m_AppraisalResponse != null)
            {
                if (this.m_AppraisalResponse.Id != 0)
                {
                    if (m_AppraisalTask.AppraisalTaskId == 0)
                    {
                        this.txtInitialEst.Text = "";
                    }
                    else
                    {
                        this.txtInitialEst.Text = this.m_AppraisalResponse.InitialEstimate.ToString();
                    }
                    if (this.m_AppraisalResponse.DateInitialEstimate == null)
                    {
                        this.m_AppraisalResponse.DateInitialEstimate = ml.DEFAULT_DATE;
                    }
                    if (this.m_AppraisalResponse.DateInitialEstimate != ml.DEFAULT_DATE)
                        this.dtInitialEst.Text = this.m_AppraisalResponse.DateInitialEstimate.ToString("MM/dd/yyyy hh:mm:ss tt");
                    else
                        this.dtInitialEst.Text = "";
                    

                }
                else
                {
                    getGoodAppraisalResponse();
                }
                try
                {
                    if (m_AppraisalResponse.AppraisalTaskId > 0)
                        this.lblTtlEstimate.Text = TotalEstimate();
                }
                catch (Exception ex)
                {
                    this.lblTtlEstimate.Text = "";
                }
                getGoodAppraisalResponse();
            }
            /*** Insurance Company ***/
            this.txtCompany.Text = this.m_AppraisalTask.InsuranceCoDescription.Replace("\r\n","<br />");

            /*** Insured ***/
            this.txtInsured.Text = this.m_AppraisalTask.InsuredDescription.Replace("\r\n", "<br />");

            /*** Claimant ***/
            this.txtClaimant.Text = this.m_AppraisalTask.ClaimantDescription.Replace("\r\n", "<br />");

            /*** Vehicle ***/
            this.txtVehicle.Text = this.m_AppraisalTask.VehicleDescription.Replace("\r\n", "<br />");

            /*** Vehicle Location ***/
            this.txtVehicleLoc.Text = this.m_AppraisalTask.VehicleLocationDescription.Replace("\r\n", "<br />");
        }

        private string TotalEstimate()
        {
            double initialEst = 0;
            double ttlSupplemental = 0;

            if (this.m_AppraisalResponse != null)
            {
                initialEst = double.Parse(this.txtInitialEst.Text.Replace("$", "").Replace(",", ""));
                ttlSupplemental = mm.GetTotalSupplementalForResponse(this.m_AppraisalResponse.Id);
                double finalEst = initialEst + ttlSupplemental;
                this.m_AppraisalResponse.FinalEstimate = finalEst;

                return finalEst.ToString("c");
            }
            else
                return initialEst.ToString("c");
        }

        private void getGoodAppraisalResponse()
        {
            //this.m_AppraisalResponse.VIN = this.m_AppraisalTask.VIN;
            //this.m_AppraisalResponse.ClaimNumber = this.m_AppraisalTask.Claim().DisplayClaimId;
            //this.m_AppraisalResponse.Update();
        }

        private int setSelection(int p)
        {
            int idIdx = 0;
            if (p == 0)
                return idIdx;

            foreach (cboGrid item in myInvestigationList)
            {
                if (item.id == p)
                    break;
                idIdx++;
            }
            return idIdx;
        }

        private int setSelection(string p)
        {
            int idIdx = 0;
            if (p == "")
                return idIdx;

            foreach (cboGrid item in myAppraiserList)
            {
                //if (item.description.Substring(0, p.Length) == p)
                if (item.description.Contains(p))
                    break;
                idIdx++;
            }
            return idIdx;
        }

        private bool LoadInvestigationType()
        {
            //this.cboInvestigationType.DataSource = LoadInvestigationType(TypeTable.getTypeTableRows((string)this.cboInvestigationType.Tag));
            mf.LoadTypeTableDDL(this.cboInvestigationType, "Appraisal_Investigation_Type", true);
           
            return true;
        }

        private void LoadTasks()
        {
            this.lvTasks.Items.Clear();     //clear the tasks

           
            myTaskList = new List<ListViewGrid>();

           
                
                ListViewGrid l = new ListViewGrid();

                if (m_vehicle.Id == 0)
                {
                    l.description = "Other";
                    l.imageIndex = 3;
                }
                else
                {
                    l.description = m_vehicle.ShortName;
                    l.imageIndex = currentImageIdx;

                    currentImageIdx++;
                    currentImageIdx = currentImageIdx % 3;
                }
                l.id = 0;

                myTaskList.Add(l);
            

            this.lvTasks.DataSource = myTaskList;
            this.lvTasks.DataBind();
            //this.lvTasks.Items.Refresh();
            //this.lvTasks.SelectedIndex = 0;
        }

        private void LoadAppraisers()
        {
            this.cboAppraiser.DataSource = LoadAppraisers(mm.GetAppraisers());
            this.cboAppraiser.DataTextField = "description";
            this.cboAppraiser.DataValueField = "id";
            this.cboAppraiser.DataBind();
            this.cboAppraiser.SelectedIndex = 0;
        }

        private void SubmitRequest()
        {
                ////save all task objects and appraisal request
                //if (this.m_AppraisalResponse != null)
                //    this.m_AppraisalResponse.AppraisalTaskId = m_AppraisalTask.AppraisalTaskId;
                //m_AppraisalResponse.VIN = m_AppraisalTask.VIN;
                //m_AppraisalResponse.ClaimNumber = m_AppraisalTask.Claim().DisplayClaimId;
                //m_AppraisalResponse.InitialEstimate = double.Parse(txtInitialEst.Text);

                //if ((this.dtInitialEst.Text == null) || (this.dtInitialEst.Text.Length < 1))
                //{
                //    this.m_AppraisalResponse.DateInitialEstimate = ml.DEFAULT_DATE;
                //}
                //else
                //{ 
                //    m_AppraisalResponse.DateInitialEstimate = Convert.ToDateTime(this.dtInitialEst.Text);
                //}
                //this.m_AppraisalResponse.Update();

           
            


            SubmitEmails();
           
            this.btnOK.Enabled = false;

            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
              
        }

        private void SubmitEmails()
        {
 
            this.m_AppraisalTask = new AppraisalTask(Convert.ToInt32(Request.QueryString["appraisal_task_id"]));
            this.m_AppraisalRequest = this.m_AppraisalTask.AppraisalRequest;

            foreach (AppraisalTask t in m_AppraisalRequest.AppraisalTasks)
            {
                try
                {
                    //AppraisalTask at = new AppraisalTask();
<<<<<<< HEAD
                    string s = t.AppraisalEmailBody(mm.APPRAISAL_TEMPLATE);

                    string s2 = s.Replace("#DATE#", ml.DBNow().ToLongDateString());

                    SceneAccessAppraiser myAppraiser = new SceneAccessAppraiser(Convert.ToInt32(cboAppraiser.SelectedValue));
                    //get appraiser full name an first name
                    string appraiserName = myAppraiser.Vendor.Name;
                    string[] a = appraiserName.Split(' ');
                    string appraiserFirstName;
                    if (a.Length - 1 == 1)
                        appraiserFirstName = a[0];
                    else
                        appraiserFirstName = appraiserName;


                    s2 = s2.Replace("#APPRAISER_FIRST_NAME#", appraiserFirstName);
                    s2 = s2.Replace("#ADJUSTER_NAME#", mm.GetUserName(Convert.ToInt32(Session["userID"])));
                    s2 = s2.Replace("#ADJUSTER_EXT#", ml.CurrentUser.PhoneExt);
                    s2 = s2.Replace("#APPRAISER_NAME#", appraiserName);
                    s2 = s2.Replace("#TASK_DESCRIPTION#", this.txtTaskDescrip.Text);
                    s2 = s2.Replace("#SPECIAL_INSTRUCTIONS#", this.txtInstructions.Text);
                    s2 = s2.Replace("#INSURED_OR_CLAIMANT#", m_AppraisalTask.InsuredOrClaimant);
                    s2 = s2.Replace("#VEHICLE_NAME#", m_vehicle.Name);
                    s2 = s2.Replace("#VIN#", m_AppraisalTask.VIN);
                    s2 = s2.Replace("#VEHICLE_COLOR#", m_vehicle.Color);
                    s2 = s2.Replace("#LICENSE_PLATE_STATE#", m_vehicle.LicensePlateState.Description.Replace("Undefined", ""));
                    s2 = s2.Replace("#LICENSE_PLATE#", m_vehicle.LicensePlateNo);
                    s2 = s2.Replace("#VEHICLE_LOCATION_CONTACT#", m_AppraisalTask.VehicleLocationDescription);
                    s2 = s2.Replace("#VEHICLE_LOCATION#", m_AppraisalTask.VehicleLocationDescription);
                    s2 = s2.Replace("#POLICY_NUMBER#", m_AppraisalTask.PolicyNumber);
                    s2 = s2.Replace("#CLAIM_NUMBER#", myClaim.DisplayClaimId);
                    s2 = s2.Replace("#DATE_OF_LOSS#", m_AppraisalTask.DateOfLoss);
                    s2 = s2.Replace("#DEDUCTIBLE#", m_AppraisalTask.DeductibleAmount.ToString("c"));
                    s2 = s2.Replace("#TYPE_OF_CLAIM#", m_AppraisalTask.TypeOfClaim);
                    s2 = s2.Replace("#LOSS_DESCRIPTION#", m_AppraisalTask.LossDescription);
                    s2 = s2.Replace("#INSURED_INFO", m_AppraisalTask.InsuredDescription);
                    s2 = s2.Replace("#INSURED_VEHICLE#", m_AppraisalTask.InsuredVehicleDescription);
                    s2 = s2.Replace("#CLAIMANT_INFO#", m_AppraisalTask.ClaimantDescription);
                    s2 = s2.Replace("#WHERE_IV_SEEN#", myClaim.WhereSeen);
                    s2 = s2.Replace("#APPRAISAL_REQUEST_ID#", m_AppraisalRequest.Id.ToString());


                    //add property damage fields
                    if (m_vehicle.PropertyDamage != null)
                    {
                        s2 = s2.Replace("#DAMAGE_DESCRIPTION#", m_vehicle.PropertyDamage.DamageDescription);
                        s2 = s2.Replace("#WHERE_SEEN#", m_vehicle.PropertyDamage.WhereSeen);
                        s2 = s2.Replace("#ESTIMATED_DAMAGE_AMOUNT#", txtInitialEst.Text);
                    }
                    else
                    {
                        s2 = s2.Replace("#DAMAGE_DESCRIPTION#", m_AppraisalTask.Claim().VehicleDamage + ": " + m_AppraisalTask.Claim().LossDescription);
                        s2 = s2.Replace("#WHERE_SEEN#", m_AppraisalTask.Claim().WhereSeen);
                        s2 = s2.Replace("#ESTIMATED_DAMAGE_AMOUNT#", txtInitialEst.Text);
                    }

                    if (t.SceneAccessAppraiser.UseEmail)
                        t.SendAppraisalEmail(s2);
                }
                catch (Exception ex)
                {
=======
                    string appraisalBody = t.AppraisalEmailBody(mm.APPRAISAL_TEMPLATE);

                    if (t.SceneAccessAppraiser.UseEmail)
                        t.SendAppraisalEmail(appraisalBody);
                }
                catch (Exception ex)
                { 
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    mf.ProcessError(ex);
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Error sending emails" + ex.ToString() + "');</script>");
                }


            }
        }

        private IEnumerable LoadAppraisers(List<Hashtable> list)
        {
            myAppraiserList = new List<cboGrid>();
            myAppraiserList.Add(new cboGrid()
            {
                id = 0,
                description = "Select Appraiser..."
            });
            try
            {
                foreach (Hashtable aData in list)
                {
                    if (((string)aData["Name"] != "") && ((string)aData["Active"] == "Yes"))
                    {
                        cboGrid cg = new cboGrid();
                        cg.id = (int)aData["Id"];
                        cg.imageIdx = (int)aData["Image_Index"];
                        cg.description = (string)aData["Name"];
                        if ((string)aData["States"] != "")
                            cg.description += "(" + (string)aData["States"] + ")";
                        myAppraiserList.Add(cg);
                    }
                }
                return myAppraiserList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAppraiserList;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
          
            mm.UpdateLastActivityTime();

                if (this.cboAppraiser.SelectedIndex <= 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Pick an Appraiser.');</script>");
                    this.cboAppraiser.Focus();
                    return;
                }

                //investigation type
                if (this.cboInvestigationType.SelectedIndex <= 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Select an Investigation Type.');</script>");
                    this.cboInvestigationType.Focus();
                    return;
                }

                //type of claim
                if (this.txtClaimType.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter a Claim Type.');</script>");
                    this.txtClaimType.Focus();
                    return;
                }

                //task description
                if (this.txtTaskDescrip.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter a Task Description.');</script>");
                    this.txtTaskDescrip.Focus();
                    return;
                }

                //loss description
                if (this.txtLossDescrip.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter a Loss Description.');</script>");
                    this.txtLossDescrip.Focus();
                    return;
                }

                if (!modLogic.isNumeric(this.txtInitialEst.Text))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter an ititial estimate.');</script>");
           
                    this.txtLossDescrip.Focus();
                    return;
                }

                btnOK.Enabled = false;


            //int vehicleId;


            m_AppraisalRequest.ClaimId = this.claimId;
            int arid = m_AppraisalRequest.Update();

            //AppraisalTask at = (AppraisalTask)ar.AppraisalTasks[0];

            //AppraisalTask at = new AppraisalTask();
            m_AppraisalTask = new AppraisalTask(m_AppraisalRequest, m_vehicle.Id);
            m_AppraisalTask.AppraisalRequestId = arid;
            m_AppraisalTask.AppraisalTaskStatusId = (int)modGeneratedEnums.AppraisalTaskStatus.Appraisal_Requested;
            m_AppraisalTask.AppraisalInvestigationTypeId = Convert.ToInt32(cboInvestigationType.SelectedValue);
            m_AppraisalTask.SceneAccessAppraiserId = Convert.ToInt32(this.cboAppraiser.SelectedValue);
            

            m_AppraisalTask.PopulateClaimantFields();
            m_AppraisalTask.PopulateClaimFields();
            m_AppraisalTask.PopulateVehicleFields();
            m_AppraisalTask.PopulateDefaultFields();
            m_AppraisalTask.PopulateUserFields();

            m_AppraisalTask.SpecialInstructions = this.txtInstructions.Text;
            m_AppraisalTask.LossDescription = this.txtLossDescrip.Text;
            m_AppraisalTask.TaskDescription = this.txtTaskDescrip.Text;
             
            int rowid = m_AppraisalTask.Update();

           // ml.GenerateTableInsertEvent("Appraisal_Task", rowid);

            //m_AppraisalRequest.AppraisalTasks

            string s = m_AppraisalTask.AppraisalEmailBody(mm.APPRAISAL_TEMPLATE);

            /*replaces all the placeholders in string s with their actual values.
           * e.g., #CLAIM_NUMBER# is replaced with BMKC11234.
           * returns all replacements made.
          */

            

            string s2 = s.Replace("#DATE#", ml.DBNow().ToLongDateString());

            SceneAccessAppraiser myAppraiser = new SceneAccessAppraiser(Convert.ToInt32(cboAppraiser.SelectedValue));
            //get appraiser full name an first name
            string appraiserName = myAppraiser.Vendor.Name;
            string[] a = appraiserName.Split(' ');
            string appraiserFirstName;
            if (a.Length - 1 == 1)
                appraiserFirstName = a[0];
            else
                appraiserFirstName = appraiserName;

           
            s2 = s2.Replace("#APPRAISER_FIRST_NAME#", appraiserFirstName);
            s2 = s2.Replace("#ADJUSTER_NAME#", mm.GetUserName(Convert.ToInt32(Session["userID"])));
            s2 = s2.Replace("#ADJUSTER_EXT#", ml.CurrentUser.PhoneExt);  
            s2 = s2.Replace("#APPRAISER_NAME#", appraiserName);
            s2 = s2.Replace("#TASK_DESCRIPTION#", this.txtTaskDescrip.Text);
            s2 = s2.Replace("#SPECIAL_INSTRUCTIONS#", this.txtInstructions.Text);
            s2 = s2.Replace("#INSURED_OR_CLAIMANT#", m_AppraisalTask.InsuredOrClaimant);
            s2 = s2.Replace("#VEHICLE_NAME#", m_vehicle.Name);
            s2 = s2.Replace("#VIN#", m_AppraisalTask.VIN);
            s2 = s2.Replace("#VEHICLE_COLOR#", m_vehicle.Color);
            s2 = s2.Replace("#LICENSE_PLATE_STATE#", m_vehicle.LicensePlateState.Description.Replace("Undefined", ""));
            s2 = s2.Replace("#LICENSE_PLATE#", m_vehicle.LicensePlateNo);
            s2 = s2.Replace("#VEHICLE_LOCATION_CONTACT#", m_AppraisalTask.VehicleLocationDescription);
            s2 = s2.Replace("#VEHICLE_LOCATION#", m_AppraisalTask.VehicleLocationDescription);
            s2 = s2.Replace("#POLICY_NUMBER#", m_AppraisalTask.PolicyNumber);
            s2 = s2.Replace("#CLAIM_NUMBER#", myClaim.DisplayClaimId);
            s2 = s2.Replace("#DATE_OF_LOSS#", m_AppraisalTask.DateOfLoss);
            s2 = s2.Replace("#DEDUCTIBLE#", m_AppraisalTask.DeductibleAmount.ToString("c"));
            s2 = s2.Replace("#TYPE_OF_CLAIM#", m_AppraisalTask.TypeOfClaim);
            s2 = s2.Replace("#LOSS_DESCRIPTION#", m_AppraisalTask.LossDescription);
            s2 = s2.Replace("#INSURED_INFO", m_AppraisalTask.InsuredDescription);
            s2 = s2.Replace("#INSURED_VEHICLE#", m_AppraisalTask.InsuredVehicleDescription);
            s2 = s2.Replace("#CLAIMANT_INFO#", m_AppraisalTask.ClaimantDescription);
            s2 = s2.Replace("#WHERE_IV_SEEN#", myClaim.WhereSeen);
            s2 = s2.Replace("#APPRAISAL_REQUEST_ID#", arid.ToString());


            //add property damage fields
            if (m_vehicle.PropertyDamage != null)
            {
                s2 = s2.Replace("#DAMAGE_DESCRIPTION#", m_vehicle.PropertyDamage.DamageDescription);
                s2 = s2.Replace("#WHERE_SEEN#", m_vehicle.PropertyDamage.WhereSeen);
                s2 = s2.Replace("#ESTIMATED_DAMAGE_AMOUNT#", txtInitialEst.Text);
            }
            else
            {
                s2 = s2.Replace("#DAMAGE_DESCRIPTION#", m_AppraisalTask.Claim().VehicleDamage + ": " + m_AppraisalTask.Claim().LossDescription);
                s2 = s2.Replace("#WHERE_SEEN#", m_AppraisalTask.Claim().WhereSeen);
                s2 = s2.Replace("#ESTIMATED_DAMAGE_AMOUNT#", txtInitialEst.Text);
            }

            if (m_AppraisalTask.SceneAccessAppraiser.UseEmail)
                m_AppraisalTask.SendAppraisalEmail(s2);


            this.btnOK.Enabled = false;


            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            
        }

        private void ValidateAppraisal()
        {
            ///** Current Task **/
            ////appraiser
           



            /** All Tasks **/
                //foreach (AppraisalTask a in m_AppraisalRequest.AppraisalTasks)
                //{
                //    //appraiser



                //    //type of claim


                //    //task description
                //    //if (a.TaskDescription == "")
                //    //{
                //    //    if (a.VehicleId == 0)
                //    //        MessageBox.Show("You Must Enter a Task Description for " + mm.OTHER_DESC + ".", "Task Description Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                //    //    else
                //    //        MessageBox.Show("You Must Enter a Task Description for " + a.Vehicle().ShortName + ".", "Task Description Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                //    //    this.txtTaskDescrip.Focus();
                //    //    return;
                //    //}

                //    //loss description
                //    //if (a.LossDescription == "")
                //    //{
                //    //    if (a.VehicleId == 0)
                //    //        MessageBox.Show("You Must Enter a Loss Description for " + mm.OTHER_DESC + ".", "Loss Description Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                //    //    else
                //    //        MessageBox.Show("You Must Enter a Loss Description for " + a.Vehicle().ShortName + ".", "Loss Description Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                //    //    this.txtLossDescrip.Focus();
                //    //    return;
                //    //}
                //}
        }

        protected void btnResend_Click(object sender, EventArgs e)
        {
            
                mm.UpdateLastActivityTime();

                ValidateAppraisal();
                SubmitEmails();     //send emails

                //set the appraisal task status for each appraisal
                foreach (AppraisalTask t in m_AppraisalRequest.AppraisalTasks)
                {
                    t.AppraisalTaskStatusId = (int)modGeneratedEnums.AppraisalTaskStatus.Appraisal_Resent;
                    t.Update();
                }

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Appraisal Request emails has been resent.');</script>");
                //MessageBox.Show("Appraisal Request emails has been resent.", "Email Resent", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);

                //feedback

           
        }

        protected void btnEditInsured_Click(object sender, EventArgs e)
        {
            try
            {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                populateSessionHolders();


<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                mm.UpdateLastActivityTime();

                Session["referer2"] = Request.Url.ToString();

<<<<<<< HEAD
                string url = "frmPersonDetails.aspx?mode=u&person_type_id=1&claim_id=" + this.claimId + "&person_id=" + this.myClaim.InsuredPerson.Id.ToString();
=======
<<<<<<< HEAD
                string url = "frmPersonDetails.aspx?mode=u&person_type_id=1&claim_id=" + this.claimId + "&person_id=" + this.myClaim.InsuredPerson.Id.ToString();
=======
                string url = "frmPersonDetails.aspx?mode=u&person_type_id=1&claim_id=" + this.m_AppraisalRequest.ClaimId + "&person_id=" + this.m_AppraisalRequest.Claim.InsuredPersonId;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                //frmPersonDetails f = new frmPersonDetails(this.m_AppraisalRequest.ClaimId, this.m_AppraisalRequest.Claim.InsuredPersonId);
                //f.ShowDialog();

                //refresh the appraisal data
                //this.m_AppraisalRequest.Claim.RefreshInsuredPerson();
                //this.m_AppraisalTask.PopulateClaimantFields();
                //this.m_AppraisalTask.PopulateClaimFields();
               // RefreshTaskData();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnEditVehicle_Click(object sender, EventArgs e)
        {

            Session["referer2"] = Request.Url.ToString();
            try
            {
                mm.UpdateLastActivityTime();

                EditVehicle();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void EditVehicle()
        {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
           

            //if (this.m_AppraisalTask.VehicleId == 0)
            if (Request.QueryString["vehicle_id"] != null)
            {
                populateSessionHolders();

                Session["referer2"] = Request.Url.ToString();

                string url = "frmVehicles.aspx?mode=u&claim_id=" + this.claimId + "&vehicle_id=" + Request.QueryString["vehicle_id"].ToString();
                string s = "window.open('" + url + "', 'popup_windowarv', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "popup_windowarv", s, true);
            }
            else
            {
                return;     //not a vehicle
            }
            
           
<<<<<<< HEAD
=======
=======
            if (this.m_AppraisalTask.VehicleId == 0)
                return;     //not a vehicle

            Session["referer2"] = Request.Url.ToString();

            string url = "frmVehicles.aspx?mode=u&claim_id=" + this.m_AppraisalRequest.ClaimId + "&vehicle_id=" + this.m_AppraisalTask.VehicleId;
            string s = "window.open('" + url + "', 'popup_windowarv', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowarv", s, true);

            
            //TODO
            //refresh the appraisal data
            //this.m_AppraisalTask.PopulateVehicleFields();
            //RefreshTaskData();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        protected void btnSupplemental_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                string url = "frmAddEditSupplementalAppraisal.aspx?mode=u&claim_id=" + this.m_AppraisalResponse.Id + "&vehicle_id=" + this.m_AppraisalTask.VehicleId;
                string s = "window.open('" + url + "', 'popup_windowars', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "popup_windowars", s, true);

                //this.lblTtlEstimate.Text = TotalEstimate();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
       

        protected void btnEditClaimant_Click(object sender, EventArgs e)
        {
            populateSessionHolders();

<<<<<<< HEAD
=======
=======
        protected void btnEditClaimant_Click(object sender, EventArgs e)
        {
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            Session["referer2"] = Request.Url.ToString();
            try
            {
                mm.UpdateLastActivityTime();

                if (this.m_AppraisalTask.InsuredOrClaimant == "Insured")
                    return;     //using the insured person

                int vehicleOwnerPersonId = 0;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

               // AppraisalTask m_AppraisalTask2 = new AppraisalTask(m_AppraisalRequest, m_vehicle.Id);
                //if (m_AppraisalTask2.Vehicle().OwnerPerson != null)
                vehicleOwnerPersonId = m_vehicle.OwnerPerson.Id;

                string url = "frmPersonDetails.aspx?mode=u&person_type_id=9&claim_id=" + this.claimId + "&person_id=" + m_vehicle.OwnerPerson.Id;
<<<<<<< HEAD
=======
=======
                if (this.m_AppraisalTask.Vehicle().OwnerPerson != null)
                    vehicleOwnerPersonId = this.m_AppraisalTask.Vehicle().OwnerPerson.Id;

                string url = "frmPersonDetails.aspx?mode=u&person_type_id=9&claim_id=" + this.m_AppraisalRequest.ClaimId + "&person_id=" + vehicleOwnerPersonId;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
 

                //refresh the appraisal data
                //this.m_AppraisalRequest.Claim.RefreshInsuredPerson();
                //this.m_AppraisalTask.PopulateClaimantFields();
                //RefreshTaskData();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        public void populateSessionHolders()
        {
            try
            {
                string sessionName = "ar" + this.claimId.ToString();
                Session[sessionName] = "set";

                Session["cboInvestigationType"] = cboInvestigationType.SelectedValue.ToString();
                Session["cboAppraiser"] = cboAppraiser.SelectedValue.ToString();
                Session["txtTaskDescrip"] = txtTaskDescrip.Text;
                Session["txtInstructions"] = txtInstructions.Text;
                Session["txtLossDescrip"] = txtLossDescrip.Text;
                Session["txtInitialEst"] = txtInitialEst.Text;
                Session["dtInitialEst"] = dtInitialEst.Text;

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }


        public void populateFormFromSessionVariables()
        {

            try
            {
                string sessionName = "ar" + this.claimId.ToString();

                if (Session[sessionName] != null)
                {

                    cboInvestigationType.SelectedValue = Session["cboInvestigationType"].ToString();
                    cboAppraiser.SelectedValue = Session["cboAppraiser"].ToString();
                    txtTaskDescrip.Text = Session["txtTaskDescrip"].ToString();
                    txtInstructions.Text = Session["txtInstructions"].ToString();
                    txtLossDescrip.Text = Session["txtLossDescrip"].ToString();
                    txtInitialEst.Text = Session["txtInitialEst"].ToString();
                    dtInitialEst.Text = Session["dtInitialEst"].ToString();

                    Session[sessionName] = null;
                    Session["cboInvestigationType"] = null;
                    Session["cboAppraiser"] = null;
                    Session["txtTaskDescrip"] = null;
                    Session["txtInstructions"] = null;
                    Session["txtLossDescrip"] = null;
                    Session["txtInitialEst"] = null;
                    Session["dtInitialEst"] = null;

                }
            }
            catch (Exception ex)
            {
                string sessionName = "ar" + this.claimId.ToString();
                Session[sessionName] = null;
                Session["cboInvestigationType"] = null;
                Session["cboAppraiser"] = null;
                Session["txtTaskDescrip"] = null;
                Session["txtInstructions"] = null;
                Session["txtLossDescrip"] = null;
                Session["txtInitialEst"] = null;
                Session["dtInitialEst"] = null;


                mf.ProcessError(ex);
            }
        }


<<<<<<< HEAD
=======
=======
       
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        protected void btnEditVehicleLoc_Click(object sender, EventArgs e)
        {
            try
            {
<<<<<<< HEAD

                populateSessionHolders();

=======
<<<<<<< HEAD

                populateSessionHolders();

=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                mm.UpdateLastActivityTime();

                //ListViewGrid mySelection = (ListViewGrid)this.lvTasks.SelectedValue;
                //if (mySelection == null)
                //    return;

                //if (mySelection.description == "Other")
                //{
                //    frmAppraisalLocationForOther f = new frmAppraisalLocationForOther(this.m_AppraisalTask);
                //    f.ShowDialog();
                //}
                //else
                EditVehicle();

                //RefreshTaskData();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        class ListViewGrid
        {
            public int id { get; set; }
        public int imageIndex { get; set; }
        public string description { get; set; }
        public BitmapImage imageIdx
        {
            get
            {
                switch (imageIndex)
                {
                    case 1:
                        return new BitmapImage(new Uri("../Content/Images/imgPeugeotOrange.png", UriKind.Relative));
                    case 2:
                        return new BitmapImage(new Uri("../Content/Images/imgPeugeotBlue.png", UriKind.Relative));
                    case 3:
                        return new BitmapImage(new Uri("../Content/Images/icoPaperClip.png", UriKind.Relative));
                    default:
                        return new BitmapImage(new Uri("../Content/Images/imgPeugeotRed.png", UriKind.Relative));
                }
            }
        }
    }
}
}
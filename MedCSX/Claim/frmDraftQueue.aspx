﻿<%@ Page Title="Draft Queue" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmDraftQueue.aspx.cs" Inherits="MedCSX.Claim.frmDraftQueue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="tsbPrintDrafts" OnClick="tsbPrintDrafts_Click" runat="server">Print Drafts</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="tsbOpenClaim" OnClick="tsbOpenClaim_Click" runat="server">Open Claim</asp:LinkButton>
             &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="sbStatus" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="grDraftQueue" AutoGenerateSelectButton="true" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="draft_id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"  CellSpacing="5"  CaptionAlign="Top" RowStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top"  ShowFooter="True" FooterStyle-BackColor="White" Width="100%">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
                
                  <asp:BoundField DataField="print_date" HeaderText="Date In Queue" />
                  <asp:BoundField DataField="Adjuster" HeaderText="Adjuster" />
                   <asp:BoundField DataField="draft_amount" HeaderText="Amount" DataFormatString="{0:c}" />
                   <asp:BoundField DataField="Claim" HeaderText="Claim" />

                
               </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                 <EditRowStyle BackColor="#999999" />
              <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>
    </div>
</asp:Content>

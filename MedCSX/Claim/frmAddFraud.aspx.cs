﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAddFraud : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();

        private Vendor m_Vendor;
        private Person m_Person;
        private Vehicle m_Vehicle;
        private MedCsxLogic.Claim m_Claim;
        private Fraud m_Fraud;
        private int m_Mode = 0;
        private int frType = 0;
        public bool OkPressed = false;
        public int vendorId = 0;
        string referer = "";
        public int claim_id = 0;
        public int vendor_id = 0;
        public int person_id = 0;
        public int vehicle_id = 0;

        private List<comboBoxGrid> myTransactionTypeList;

        protected void Page_Load(object sender, EventArgs e)
        {
            m_Mode = Convert.ToInt32(Request.QueryString["mode"]);
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));


            switch (m_Mode)
            {
                case 1:     //vendor
                    vendor_id = Convert.ToInt32(Request.QueryString["perpetrator"]);
                    this.m_Vendor = new Vendor(vendor_id);
                    
                     this.m_Claim = new MedCsxLogic.Claim(claim_id);

                    

                    this.txtFraudName.Text = this.m_Vendor.Name;
                    this.txtFraudName.Enabled = false;
                    this.txtVendorLookup.Enabled = false;
                    if (this.m_Claim != null)
                    {
                        this.txtClaimId.Text = this.m_Claim.DisplayClaimId;
                        this.txtClaimId.Enabled = false;
                    }
                    this.rbFraud_Vendor.Checked = true;
                   
                    break;
                case 2:     //person
                   
                    person_id = Convert.ToInt32(Request.QueryString["perpetrator"]);
                    m_Person = new Person(person_id);
                    this.Page.Title += " for " + this.m_Person.Name;

                    this.m_Claim = new MedCsxLogic.Claim(claim_id);

                    this.txtFraudName.Text = this.m_Person.Name;
                    this.txtFraudName.Enabled = false;
                    this.txtVendorLookup.Enabled = true;
                    if (this.m_Claim != null)
                    {
                        this.txtClaimId.Text = this.m_Claim.DisplayClaimId;
                        this.txtClaimId.Enabled = false;
                    }
                    this.rbFraud_Person.Checked = true;
                    pnlVendor.Visible = false;
                    break;
                case 4:
                    vehicle_id = Convert.ToInt32(Request.QueryString["perpetrator"]);
                    m_Vehicle = new Vehicle(vehicle_id);
                    this.Page.Title += " for Vehicle " + this.m_Vehicle.ShortName;
                    this.m_Claim = new MedCsxLogic.Claim(claim_id);

                    if (this.m_Claim != null)
                    {
                        this.txtClaimId.Text = this.m_Claim.DisplayClaimId;
                        this.txtClaimId.Enabled = false;
                    }

                    this.txtVIN.Text = this.m_Vehicle.VIN;
                    this.rbFraud_Vehicle.Checked = true;
                    this.txtVIN.Enabled = false;
                    this.pnlVehicle.Visible = true;
                    pnlVendor.Visible = false;
                    break;
                default:    //claim
                    this.m_Claim = new MedCsxLogic.Claim(claim_id);
                    this.txtClaimId.Text = this.m_Claim.DisplayClaimId;
                    this.txtClaimId.Enabled = false;

                    this.m_Claim = new MedCsxLogic.Claim(claim_id);
                    this.Page.Title += " for Claim " + this.m_Claim.DisplayClaimId;
                    break;
            }


            if (Session["referer3"] != null)
            {
                referer = Session["referer3"].ToString();
            }


            if (!Page.IsPostBack)
            {
                this.cboFraudType.DataSource = LoadComboBox(this.cboFraudType, true, "1=1");
<<<<<<< HEAD
                this.cboFraudType.DataTextField = "Description";
                this.cboFraudType.DataValueField = "Id";
=======
<<<<<<< HEAD
                this.cboFraudType.DataTextField = "Description";
                this.cboFraudType.DataValueField = "Id";
=======
                this.cboFraudType.DataTextField = "description";
                this.cboFraudType.DataValueField = "id";
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                this.cboFraudType.DataBind();

                this.cboFraudType.Items.Insert(0, "Select");
                this.cboFraudType.SelectedValue = "0";

                if (Session["vendorID"] != null)
                {
                    vendorId = Convert.ToInt32(Session["vendorID"]);
                    Vendor myVendor = new Vendor(vendorId);
                    txtVendorTaxID.Text = myVendor.TaxId.ToString();
                    txtVendorTaxID_TextChanged(null, null);
                     Session["vendorID"] = null;
                   
                }
            }

        }

        class comboBoxGrid
        {
            public int id { get; set; }
            public string description { get; set; }
            public int imageIdx { get; set; }
        }

        private IEnumerable LoadComboBox(DropDownList cb, bool includeZeros, string whereClause)
        {
            myTransactionTypeList = new List<comboBoxGrid>();
            try
            {
                foreach (Hashtable tData in TypeTable.getTypeTableRows("NICB_Type", whereClause))
                {
                    comboBoxGrid cg = new comboBoxGrid();
                    cg.id = (int)tData["Id"];
                    cg.description = (string)tData["Description"];
                    if (includeZeros || cg.id != 0)
                        myTransactionTypeList.Add(cg);
                }
                return myTransactionTypeList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myTransactionTypeList;
            }
        }

        protected void txtVendorLookup_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            
        }

        protected void txtVendorTaxID_TextChanged(object sender, EventArgs e)
        {
            if (txtVendorTaxID.Text.Length < 1)
                return;

            vendorId = Vendor.getVendorIdForTaxId(txtVendorTaxID.Text);

           
            if (vendorId == 0)
            {
                lblTaxID.Visible = true;
                lblTaxID.Text = "Vendor Not Found Corresponding to Tax Id " + txtVendorTaxID.Text + ".";
                if (this.txtVendorTaxID.Visible)
                    this.txtVendorTaxID.Focus();
                return;
            }
            lblTaxID.Visible = false;


            this.m_Vendor = new Vendor(vendorId);

            this.txtFraudName.Text = this.m_Vendor.Name;

            if ((bool)this.rbFraud_Person.Checked)
                this.rbFraud_Both.Checked = true;
            else
                this.rbFraud_Vendor.Checked = true;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.OkPressed = false;
                mm.UpdateLastActivityTime();

                if (string.IsNullOrWhiteSpace(this.txtClaimId.Text))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The claim associated with this Fraud Claim has not been entered.');</script>");
                    //MessageBox.Show("The claim associated with this Fraud Claim has not been entered.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.txtClaimId.Focus();
                    return;
                }

                if (!(bool)this.rbFraud_Vendor.Checked && !(bool)this.rbFraud_Person.Checked && !(bool)this.rbFraud_Both.Checked && !(bool)this.rbFraud_Vehicle.Checked)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please indicate if the individual is a Vendor, a Person, Both, or a Vehicle.');</script>");
                    //MessageBox.Show("Please indicate if the individual is a Vendor, a Person, Both, or a Vehicle.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.rbFraud_Vendor.Focus();
                    return;
                }

                if ((bool)this.rbFraud_Vehicle.Checked)
                {
                    if (string.IsNullOrWhiteSpace(this.txtVIN.Text))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The vehicle VIN associated with this Fraud Claim has not been entered.');</script>");
                        //MessageBox.Show("The vehicle VIN associated with this Fraud Claim has not been entered.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        this.txtVIN.Focus();
                        return;
                    }

                    this.m_Person = null;
                    this.m_Vendor = null;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(this.txtFraudName.Text))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The individual associated with this Fraud Claim has not been entered.');</script>");
                        //MessageBox.Show("The individual associated with this Fraud Claim has not been entered.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        this.txtFraudName.Focus();
                        return;
                    }

                    this.m_Vehicle = null;
                }

                if (string.IsNullOrWhiteSpace(this.txtFraudDescription.Text))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The details of this Fraud Claim have not been entered.');</script>");
                    //MessageBox.Show("The details of this Fraud Claim have not been entered.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.txtFraudDescription.Focus();
                    return;
                }

                if (cboFraudType.SelectedValue == "Select")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The type of fraud claim has not been selected.');</script>");
                    //MessageBox.Show("The type of fraud claim has not been selected.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.cboFraudType.Focus();
                    return;
                }

                if (this.m_Fraud == null)
                    this.m_Fraud = new Fraud();

                this.m_Fraud.ClaimId = mm.ExtractClaimId(this.txtClaimId.Text);
                this.m_Fraud.PersonId = 0;
                this.m_Fraud.VendorId = 0;
                this.m_Fraud.VehicleId = 0;
                if (this.m_Vendor != null)
                {
                    this.m_Fraud.VendorId = this.m_Vendor.Id;
                    if (!(bool)this.rbFraud_Both.Checked)
                        this.m_Fraud.PersonTypeId = 0;
                    else
                        this.m_Fraud.PersonTypeId = 0;
                    this.m_Vendor.IsFraudulent = true;
                }

                if (this.m_Person != null)
                {
                    this.m_Fraud.PersonId = this.m_Person.Id;
                    if (!(bool)this.rbFraud_Both.Checked)
                        this.m_Fraud.PersonTypeId = 0;
                    else
                        this.m_Fraud.PersonTypeId = 0;
                    this.m_Person.IsFraudulent = true;
                }
                else if ((bool)rbFraud_Person.Checked)
                {
                    this.m_Fraud.PersonTypeId = (Int16)this.m_Person.PersonTypeId;
                    this.m_Fraud.PersonId = mm.GetPersonIdForDescription(this.txtFraudName.Text, this.m_Fraud.ClaimId);
                    this.m_Person = new Person(this.m_Fraud.PersonId);
                    this.m_Person.IsFraudulent = true;
                }

                if (this.m_Vehicle != null)
                {
                    this.m_Fraud.VehicleId = this.m_Vehicle.Id;
                    this.m_Fraud.PersonTypeId = 4;
                    this.m_Vehicle.IsFraudulent = true;
                }

                this.m_Fraud.Description = this.txtFraudDescription.Text;
                this.m_Fraud.NICBTypeId = Convert.ToInt32(cboFraudType.SelectedValue);
                this.m_Fraud.Update();

                if (this.m_Vendor != null)
                    this.m_Vendor.Update();
                if (this.m_Person != null)
                    this.m_Person.Update();
                if (this.m_Vehicle != null)
                    this.m_Vehicle.Update();

                
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }



            Session["referer3"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
        }

        protected void rbFraud_Person_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFraud_Person.Checked == true)
            {
                try
                {
                    mm.UpdateLastActivityTime();

                    if ((this.m_Person == null) || (this.m_Person.Id == 0) || (this.m_Person.Name != this.txtFraudName.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(this.txtFraudName.Text) && !string.IsNullOrWhiteSpace(this.txtClaimId.Text))
                        {
                            if (mm.GetPersonIdForDescription(this.txtFraudName.Text, mm.ExtractClaimId(this.txtClaimId.Text)) == 0)
                            {
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The name entered does not correspond to any individual on claim " + this.txtClaimId.Text + "');</script>");
                                //MessageBox.Show("The name entered does not correspond to any individual on claim " + this.txtClaimId.Text, "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                                this.txtFraudName.Focus();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    mf.ProcessError(ex);
                }
            }
        }

        protected void rbFraud_Both_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFraud_Person.Checked == true)
            {
                try
                {
                    mm.UpdateLastActivityTime();

                    if ((this.m_Person == null) || (this.m_Person.Id == 0) || (this.m_Person.Name != this.txtFraudName.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(this.txtFraudName.Text) && !string.IsNullOrWhiteSpace(this.txtClaimId.Text))
                        {
                            if (mm.GetPersonIdForDescription(this.txtFraudName.Text, mm.ExtractClaimId(this.txtClaimId.Text)) == 0)
                            {
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The name entered does not correspond to any individual on claim " + this.txtClaimId.Text + "');</script>");
                                //MessageBox.Show("The name entered does not correspond to any individual on claim " + this.txtClaimId.Text, "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                                this.txtFraudName.Focus();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    mf.ProcessError(ex);
                }
            }
        }

        protected void rbFraud_Vehicle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFraud_Person.Checked == true)
            {
                try
                {
                    mm.UpdateLastActivityTime();

                    this.txtFraudName.Text = null;
                    this.pnlVehicle.Visible = true;
                }
                catch (Exception ex)
                {
                    mf.ProcessError(ex);
                }
            }
        }

        protected void ckPerson_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtVIN.Text = null;
                this.pnlVehicle.Visible =false;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtVIN_TextChanged(object sender, EventArgs e)
        {
            string vin = (string)(sender as TextBox).Text;
            if (string.IsNullOrWhiteSpace(vin))
                return;

            if (string.IsNullOrWhiteSpace(this.txtClaimId.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The claim associated with this Fraud Claim has not been entered.');</script>");
                //MessageBox.Show("The claim associated with this Fraud Claim has not been entered.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                this.txtClaimId.Focus();
                return;
            }

            if (mm.VIN_Checksum(vin))
            {
                int veh_id = mm.GetVehicleIdForVIN(vin, this.m_Claim.ClaimId);
                if (veh_id == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This vehicle is not on this claim.');</script>");
                    //MessageBox.Show("This vehicle is not on this claim.", "Incorrect Vehicle", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    this.txtVIN.Focus();
                    return;
                }
                this.m_Vehicle = new Vehicle(veh_id);
            }
        }

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Session["refererVendor"] = Request.Url.ToString();

            string lookupValue = txtVendorSearch.Text;
            if (lookupValue == "")
                return;

            //show vendor search results
            //show vendor search results
            string url = "frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
            string s2 = "window.open('" + url + "', 'popup_window4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            vendorlookup.Visible = false;
        }
    }
}
﻿using MedCSX.App_Code;
using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmMonthlySubroSalvageReport : System.Web.UI.Page
    {
        private static modLogic ml = new modLogic();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        public Periods Periods = Periods.Daily;
        public string dateStoredProc = "";
        public string reportFileName = "";
        public string title = "";
        private List<Hashtable> dates;
        private string[] monthAbbr = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        private Dictionary<string, int> monthDict;
        private string lastSelectedDat = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Periods = Periods.Monthly;
            dateStoredProc = "usp_Get_Monthly_Draft_Report_Dates";
            reportFileName = "DRAFT_MONTHLY_REPORT";
            this.Page.Title = "MedCsX Monthly Subro Salvage Report";

            dates = db.ExecuteStoredProcedureReturnHashList(dateStoredProc);

            if (dates.Count == 0)
                return;
            if (!Page.IsPostBack)
                LoadYears();
        }

        private void LoadYears()
        {




            grYears.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Draft_years");
            grYears.DataBind();
            grYears.SelectedIndex = 0;
            grYears_SelectedIndexChanged(null, null);

        }

        protected void grYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDateAmounts();
        }

        private void LoadDateAmounts()
        {

            int years = Convert.ToInt32(grYears.SelectedRow.Cells[1].Text);
            //string strMonths = grDateAmounts.SelectedRow.Cells[1].Text;
            //int months = (int)(Months)Enum.Parse(typeof(Months), strMonths);

            grDateAmounts.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Monthly_Subro_Salvage_Report_Dates", years);
            grDateAmounts.DataBind();
            grDateAmounts.SelectedIndex = 0;

        }

        protected void tsbFullReport_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Dictionary<string, string> rptParms = new Dictionary<string, string>();
            string tmpdate = grDateAmounts.SelectedRow.Cells[1].Text + "/01/" + grYears.SelectedRow.Cells[1].Text;
            DateTime date1 = Convert.ToDateTime(tmpdate);
            DateTime date2 = date1.AddMonths(1);

            rptParms.Add("Date1", date1.ToLongDateString());
            rptParms.Add("Date2", date2.ToLongDateString());
            rptParms.Add("Option", "All");

            ShowReport("MedCsx Monthly Draft Report", rptParms);
        }

        protected void tsbSummaryReport_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Dictionary<string, string> rptParms = new Dictionary<string, string>();
            string tmpdate = grDateAmounts.SelectedRow.Cells[1].Text + "/01/" + grYears.SelectedRow.Cells[1].Text;
            DateTime date1 = Convert.ToDateTime(tmpdate);
            DateTime date2 = date1.AddMonths(1);

            rptParms.Add("Date1", date1.ToLongDateString());
            rptParms.Add("Date2", date2.ToLongDateString());


            ShowReport("MedCsx Monthly Subro Salvage Report", rptParms);
        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            //rptParms = 
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }
    }
}
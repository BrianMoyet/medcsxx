﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master" CodeBehind="vehicles.aspx.cs" Inherits="MedCSX.Claim.vehicles" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
    <div class="col-md-5">
         <div class="row">
               <div class="col-md-12">
                <b>Vehicle Details</b>
            </div>
         </div>
        <div class="row">
            <div class="col-md-3">
                Year:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtYear" runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Make:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtMake" runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Model:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                License Plate No:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtLicensePlateNo" runat="server" Width="75px"></asp:TextBox> State: <asp:DropDownList ID="ddlLicenseState" runat="server"></asp:DropDownList>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                VIN:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtVin" runat="server" AutoPostBack="True" OnTextChanged="txtVin_TextChanged"></asp:TextBox>
            &nbsp;<asp:Label ID="lblVinMessage" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                Color:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtColor" runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:CheckBox ID="chkVehicleisDrivable" Text="This is Drivable" runat="server" />
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:HyperLink ID="hprViewCarfaxReport" runat="server" Target="_blank">View Carfax Report</asp:HyperLink>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:HyperLink ID="hprViewCarfax" runat="server">HyperLink</asp:HyperLink>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
               <hr />
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
              <b>Loss Payee (From Personal Lines)</b>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                First Name:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLFirstName" runat="server" Enabled="false"></asp:TextBox> 
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Last Name:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLLastName" runat="server" Enabled="false"></asp:TextBox>
             </div>
         </div>
         <div class="row">
            <div class="col-md-3">
                Address 1:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLAddress1" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
         <div class="row">
            <div class="col-md-3">
                Address 2:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLAddress2" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
           <div class="row">
            <div class="col-md-3">
               City:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLCity" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
                  <div class="row">
            <div class="col-md-3">
               State:
            </div>
             <div class="col-md-9">
                 <asp:DropDownList ID="ddlPLState_id" runat="server">
                 </asp:DropDownList>
                 Zip:   <asp:TextBox ID="txtPLZip" runat="server" Enabled="false" Width="60px"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Loan No:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLLoanNo" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Phone:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLPhone" runat="server" Enabled="false" TextMode="Phone"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Fax:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLFax" runat="server" Enabled="false" TextMode="Phone"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-3">
               Corrected Phone:
            </div>
             <div class="col-md-9">
                 <asp:TextBox ID="txtPLCorrectedPhone" runat="server" TextMode="Phone"></asp:TextBox>
            </div>
        </div>
    </div>
     <div class="col-md-7">
         <div class="row">
               <div class="col-md-12">
                <b>Current Location of Vehicle (for Scene Access)</b>
            </div>
         </div>
          
         <div class="row">
            <div class="col-md-2">
                Contact First Name:
            </div>
          <div class="col-md-10">
              <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox> Last Name: <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-2">
                Contact Email:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="txtEmail" runat="server" TextMode="Email"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-2">
                Address 1:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox>
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
                Address 2:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
               City:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </div>
        </div>
          <div class="row">
            <div class="col-md-2">
               State:
            </div>
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCurrentState" runat="server"></asp:DropDownList> &nbsp; Zip: <asp:TextBox ID="txtCurrentZip" runat="server" Width="60px"></asp:TextBox>
                Zipcode is Required for Scene Access
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
               Phone 1:
            </div>
            <div class="col-md-10">
              <asp:TextBox ID="txtPhone1" runat="server"></asp:TextBox> <asp:Button ID="btnSetToDriversAddress"  class="btn btn-info btn-xs"  runat="server" Text="Set to Driver's Address" OnClick="btnSetToDriversAddress_Click" />
            </div>
        </div>
           <div class="row">
            <div class="col-md-2">
               Phone 2:
           </div>
            <div class="col-md-10">
              <asp:TextBox ID="txtPhone2" runat="server"></asp:TextBox> <asp:Button ID="txtSetToOwnersAddress"  class="btn btn-info btn-xs"  runat="server" Text="Set to Owner's Address" />
            </div>
        </div>
    </div>

</div>
      <div class="row">
           <div class="col-md-12">
               <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
               <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
               <asp:Button ID="btnAddEditSIU" runat="server" class="btn btn-info btn-xs"  Text="Add/Edit SIU"     OnClick="btnAddEditSIU_Click"  />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
        </div>
          </div>
    <div class="row">
         <div class="col-md-2">
        <asp:Label ID="lblAlert" runat="server" visible="False" Text="" ForeColor="Red"></asp:Label>
             </div>
    </div>
</asp:Content>



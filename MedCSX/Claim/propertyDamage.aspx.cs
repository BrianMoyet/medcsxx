﻿using MedCSX.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class propertyDamage1 : System.Web.UI.Page
    {
        private int claim_id;
        private int property_damage_id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Property Damage";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpproperty_damage_id = Regex.Replace(Request.QueryString["property_damage_id"].ToString(), "[^0-9]", "");
            property_damage_id = Convert.ToInt32(tmpproperty_damage_id);
          
            
            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();
             

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void GetGeneralInfo()
        {
            PropertyDamage pd = new PropertyDamage();
            pd.GetPropertyDamagesByID(property_damage_id);

            txtWhereSeen.Text = pd.where_seen;
            txtDescribeDamage.Text = pd.damage_description;
            txtEstimatedAmount.Text = pd.estimated_damage_amount;
            ddlAuto.SelectedValue = pd.vehicle_id;
            ddlPropertyType.SelectedValue = pd.property_type_id;
            ddlOwner.SelectedValue = pd.owner_person_id;
            ddlDriver.SelectedValue = pd.driver_person_id;
            ddlReserveType.SelectedValue = pd.reserve_type_id;
            ddlAuto.SelectedValue = pd.vehicle_id;
            ddlState.SelectedValue = pd.other_insurance_state_id;

            txtCompany.Text = pd.other_insurance_company;
            txtPolicyNo.Text = pd.other_insurance_policy_no;
            txtClaimNo.Text = pd.other_insurance_claim_no;
            txtAdjuster.Text = pd.other_insurance_adjuster;
            txtPhone.Text = pd.other_insurance_phone;
            txtFax.Text = pd.other_insurance_fax;
            txtAddress.Text = pd.other_insurance_address;
            txtCity.Text = pd.other_insurance_city;
            txtZip.Text = pd.other_insurance_zipcode;
        }

        protected void PopulateDDLs()
        {
            DataTable dt = new DataTable();

            Reserve myData = new Reserve();
            dt = myData.GetReserveBySubClaimType(Convert.ToInt32(tmpSub_claim_type_id));

            ddlReserveType.DataSource = dt;
            ddlReserveType.DataValueField = dt.Columns[0].ColumnName;
            ddlReserveType.DataTextField = dt.Columns[1].ColumnName;
            ddlReserveType.DataBind();

            DataTable dtPropertyTypes = new DataTable();

            PropertyDamage myPropertyTypes = new PropertyDamage();
            dtPropertyTypes = myPropertyTypes.GetPropertyTypes();

            ddlPropertyType.DataSource = dtPropertyTypes;
            ddlPropertyType.DataValueField = dtPropertyTypes.Columns[0].ColumnName;
            ddlPropertyType.DataTextField = dtPropertyTypes.Columns[1].ColumnName;
            ddlPropertyType.DataBind();

            Vehicle myVehicle = new Vehicle();
            dt = myVehicle.GetVehicles_By_ClaimId(claim_id);

            ddlAuto.DataSource = dt;
            ddlAuto.DataValueField = dt.Columns[0].ColumnName;
            ddlAuto.DataTextField = dt.Columns[38].ColumnName;
            ddlAuto.DataBind();

            DataTable dtPerson = new DataTable();
            MedCSX.App_Code.Claim myDataPerson = new MedCSX.App_Code.Claim();
            dtPerson = myDataPerson.PopulateDDLClaimPersons(claim_id);

            ddlOwner.DataSource = dtPerson;
            ddlOwner.DataValueField = dtPerson.Columns[0].ColumnName;
            ddlOwner.DataTextField = dtPerson.Columns[1].ColumnName;
            ddlOwner.DataBind();

            ddlDriver.DataSource = dtPerson;
            ddlDriver.DataValueField = dtPerson.Columns[0].ColumnName;
            ddlDriver.DataTextField = dtPerson.Columns[1].ColumnName;
            ddlDriver.DataBind();

            DataTable dtState = new DataTable();
            MedCSX.App_Code.Claim myDataState = new MedCSX.App_Code.Claim();
            dtState = myDataState.PopulateDDLStates();
            ddlState.DataSource = dtState;
            ddlState.DataValueField = dtState.Columns[0].ColumnName;
            ddlState.DataTextField = dtState.Columns[1].ColumnName;
            ddlState.DataBind();

            DataTable dtUsers = new DataTable();
            dtUsers = myDataState.GetUsersWhoCanBeAssignedClaims();
            ddlAssignToUser.DataSource = dtUsers;
            ddlAssignToUser.DataValueField = dtUsers.Columns[0].ColumnName;
            ddlAssignToUser.DataTextField = dtUsers.Columns[1].ColumnName;
            ddlAssignToUser.DataBind();

            DataTable dtGroups = new DataTable();
            dtGroups = myDataState.GetGroupsWhoCanBeAssignedClaims();
            ddlAssignToGroup.DataSource = dtGroups;
            ddlAssignToGroup.DataValueField = dtGroups.Columns[0].ColumnName;
            ddlAssignToGroup.DataTextField = dtGroups.Columns[1].ColumnName;
            ddlAssignToGroup.DataBind();
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmReceiveSalvage : System.Web.UI.Page
    {
        #region Class Variables
        public bool okPressed = false;      //was OK button pressed?  Output parameter
        public int transactionId = 0;       //transaction ID of transaction row inserted.  Output parameter

        private int reserveId,              //reserve ID to create transaction for.  Input parameter
            salvageId,                      //salvage_Id of salvage row inserted
            reissueFromId,                  //Id to reissue from 
            claim_id = 0;                 //claim_id for reserve ID
        private int vendorId;       //vendor ID corresponding to tax ID entered
        private Vendor vendor = null;
        private long draftNo;
        private Reserve m_reserve;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();

      

       

        private static ModMain mm = new ModMain();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            m_reserve = new Reserve(this.reserveId);

            if (!Page.IsPostBack)
            { 
                //this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
                //m_reserve = new Reserve(this.reserveId);

                this.Page.Title = "Claim " + m_reserve.Claim.DisplayClaimId + " - Receive Salvage For " + Reserve.ReserveDescription(ref this.reserveId);
                this.txtStorageAmount.Enabled = false;
                this.txtStorageDays.Enabled = false;
                this.txtTowingAmount.Enabled = false;
            }

        }

        protected void txtVendorLookup_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
          
        }

        protected void txtVendorId_TextChanged(object sender, EventArgs e)
        {
            string taxId = this.txtVendorId.Text.Trim();

            if (taxId == "")
                return;
           

            //lookup tax id
            vendorId = Vendor.getVendorIdForTaxId(taxId);
            if (vendorId == 0)
            {
                lblTaxID.Visible = true;
                lblTaxID.Text = "Vendor Not Found Corresponding to Tax Id " + taxId + ".";
                if (this.txtVendorId.Visible)
                    this.txtVendorId.Focus();
                return;
            }

            lblTaxID.Visible = false;
            Vendor v = new Vendor(vendorId);
            string vendorInfo = "";
            vendorInfo += v.Name;
            txtVendorName.Text = vendorInfo;

            if ((v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Auto_Repair_Services) ||
                (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Salvage) ||
                (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Tow_Services))
            {
                this.txtStorageAmount.Enabled = true;
                this.txtStorageDays.Enabled = true;
                this.txtTowingAmount.Enabled = true;
            }

        }

        protected void txtSalvageAmount_TextChanged(object sender, EventArgs e)
        {
            string thisText = ((TextBox)sender).Text;
            if (modLogic.isNumeric(thisText))
                this.txtSalvageAmount.Text = double.Parse(thisText).ToString("c");
        }

        protected void txtStorageAmount_TextChanged(object sender, EventArgs e)
        {
            string thisText = ((TextBox)sender).Text;
            if (modLogic.isNumeric(thisText))
                this.txtStorageAmount.Text = double.Parse(thisText).ToString("c");
        }

        protected void txtTowingAmount_TextChanged(object sender, EventArgs e)
        {
            string thisText = ((TextBox)sender).Text;
            if (modLogic.isNumeric(thisText))
                this.txtTowingAmount.Text = double.Parse(thisText).ToString("c");
        }

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                Session["refererVendor"] = Request.Url.ToString();

                string lookupValue = txtVendorSearch.Text;
                if (lookupValue.Trim() == "")
                    return;     //user hit cancel

                //show vendor search results
                string url = "frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
                string s2 = "window.open('" + url + "', 'popup_window4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

                vendorlookup.Visible = false;

                if (this.txtDraftNo.Visible)
                    this.txtDraftNo.Focus();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (SalvageValidation())
                {
                    Salvage salvage = new Salvage();
                    salvage.ReissuedFromSalvageId = reissueFromId;
                    salvage.VendorId = vendorId;
                    salvage.CheckNo = draftNo;
                    salvage.StorageCharges = double.Parse(this.txtStorageAmount.Text.Trim().Replace("$", "").Replace(",", ""));
                    salvage.StorageDays = int.Parse(this.txtStorageDays.Text);
                    salvage.Towing_Charges = double.Parse(this.txtTowingAmount.Text.Trim().Replace("$", "").Replace(",", ""));
                    salvage.isVoid = false;
                    Hashtable salvageHash = ConvertToHashTable(salvage);

                    transactionId = Salvage.InsertSalvage(salvageHash, this.reserveId, double.Parse(this.txtSalvageAmount.Text.Replace("$", String.Empty)));

                    this.okPressed = true;
                    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private Hashtable ConvertToHashTable(Salvage salvage)
        {
            Hashtable hash = new Hashtable();
            hash.Add("Check_No", salvage.CheckNo);
            hash.Add("Is_Void", salvage.isVoid);
            hash.Add("Reissued_From_Salvage_Id", salvage.ReissuedFromSalvageId);
            //hash.Add("Salvage_Id", salvage.SalvageId);
            hash.Add("Vendor_Id", salvage.VendorId);
            double storageAmount = double.Parse(this.txtStorageAmount.Text.Trim().Replace("$", "").Replace(",", ""));
            int storageDays = int.Parse(this.txtStorageDays.Text.Trim().Replace("$", "").Replace(",", ""));
            if (storageAmount > 0)
            {
                hash.Add("Storage_Charges", storageAmount);
                hash.Add("Storage_Days", storageDays);
            }
            double towingAmount = double.Parse(this.txtTowingAmount.Text.Trim().Replace("$", "").Replace(",", ""));
            if (towingAmount > 0)
                hash.Add("Towing_Charges", towingAmount);

            return hash;
        }

        private bool SalvageValidation()
        {
            if (this.txtSalvageAmount.Text.Trim() == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('alvage Amount is a Required Field.');</script>");
                //MessageBox.Show("Salvage Amount is a Required Field");
                if (this.txtSalvageAmount.Visible)
                    this.txtSalvageAmount.Focus();
                return false;
            }

            if (!modLogic.isNumeric(this.txtSalvageAmount.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(Salvage Amount Must Be Numeric.');</script>");
                //MessageBox.Show("Salvage Amount Must Be Numeric");
                if (this.txtSalvageAmount.Visible)
                    this.txtSalvageAmount.Focus();
                return false;
            }

            double salvageAmount = double.Parse(this.txtSalvageAmount.Text.Trim().Replace("$", "").Replace(",", ""));

            if (salvageAmount <= 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Salvage Amount Must Not Be Zero Or Less');</script>");
                //MessageBox.Show("Salvage Amount Must Not Be Zero Or Less");
                if (this.txtSalvageAmount.Visible)
                    this.txtSalvageAmount.Focus();
                return false;
            }

            if ((this.txtStorageAmount.Text.Trim() == "") || (!this.txtStorageAmount.Enabled))
            {
                this.txtStorageAmount.Text = "0";
            }

            if (!modLogic.isNumeric(this.txtStorageAmount.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Storage Yards Amount Must Be Numeric.');</script>");
                //MessageBox.Show("Storage Yards Amount Must Be Numeric");
                if (this.txtStorageAmount.Visible)
                    this.txtStorageAmount.Focus();
                return false;
            }

            double storageAmount = double.Parse(this.txtStorageAmount.Text.Trim().Replace("$", "").Replace(",", ""));

            if (storageAmount < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Storage Yards Amount Must Not Be Less Than Zero.');</script>");
                //MessageBox.Show("Storage Yards Amount Must Not Be Less Than Zero");
                if (this.txtStorageAmount.Visible)
                    this.txtStorageAmount.Focus();
                return false;
            }

            if (storageAmount > 0)
            {
                if (this.txtStorageDays.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('torage Days is a Required Field when Storage Yard Charges are Present.');</script>");
                    //MessageBox.Show("Storage Days is a Required Field when Storage Yard Charges are Present.");
                    if (this.txtStorageDays.Visible)
                        this.txtStorageDays.Focus();
                    return false;
                }

                if (!modLogic.isNumeric(this.txtStorageDays.Text))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Storage Days Must Be Numeric.');</script>");
                    //MessageBox.Show("Storage Days Must Be Numeric");
                    if (this.txtStorageDays.Visible)
                        this.txtStorageDays.Focus();
                    return false;
                }

                int storageDays = int.Parse(this.txtStorageDays.Text.Trim().Replace("$", "").Replace(",", ""));

                if (storageDays <= 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Storage Days Must Not Be Zero Or Less.');</script>");
                    //MessageBox.Show("Storage Days Must Not Be Zero Or Less");
                    if (this.txtStorageDays.Visible)
                        this.txtStorageDays.Focus();
                    return false;
                }
            }
            else
                this.txtStorageDays.Text = "0";

            if ((this.txtTowingAmount.Text.Trim() == "") || (!this.txtTowingAmount.Enabled))
            {
                this.txtTowingAmount.Text = "0";
            }

            if (!modLogic.isNumeric(this.txtTowingAmount.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Towing Amount Must Be Numeric.');</script>");
                //MessageBox.Show("Towing Amount Must Be Numeric");
                if (this.txtTowingAmount.Visible)
                    this.txtTowingAmount.Focus();
                return false;
            }

            double towingAmount = double.Parse(this.txtTowingAmount.Text.Trim().Replace("$", "").Replace(",", ""));

            if (towingAmount < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Towing Amount Must Not Be Less Than Zero.');</script>");
                //MessageBox.Show("Towing Amount Must Not Be Less Than Zero");
                if (this.txtTowingAmount.Visible)
                    this.txtTowingAmount.Focus();
                return false;
            }

            if (this.txtVendorId.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Vendor Tax Id is a Required Field.');</script>");
                //MessageBox.Show("Vendor Tax Id is a Required Field");
                if (this.txtVendorId.Visible)
                    this.txtVendorId.Focus();
                return false;
            }

            vendorId = Vendor.getVendorIdForTaxId(this.txtVendorId.Text);
            if (vendorId == 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Vendor Tax Id entered is not Valid.');</script>");
                //MessageBox.Show("Vendor Tax Id entered is not Valid");
                if (this.txtVendorId.Visible)
                    this.txtVendorId.Focus();
                return false;
            }

            if (m_reserve.LossAmount < salvageAmount)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Warning - Salvage Amount is Greater than the Total Loss Amount for this Reserve Line.');</script>");
                return false;
                //if (MessageBox.Show("Warning - Salvage Amount is Greater than the Total Loss Amount for this Reserve Line.  Do You Want to Continue Anyway?", "Salvage Greater Than Loss", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                //{
                //    if (this.txtSalvageAmount.Visible)
                //        this.txtSalvageAmount.Focus();
                //    return false;
                //}
            }

            if (!long.TryParse(this.txtDraftNo.Text, out draftNo))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('heck Number Must Be Numeric');</script>");
                //MessageBox.Show("Check Number Must Be Numeric");
                if (this.txtDraftNo.Visible)
                    this.txtDraftNo.Focus();
                return false;
            }
            return true;
        }
    }
}
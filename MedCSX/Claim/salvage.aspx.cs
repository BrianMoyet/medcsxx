﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class salvage : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Salvage";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpID = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpID);


            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();


                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        public void GetGeneralInfo()
        {

        }

        public void PopulateDDLs()
        {

        }


        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
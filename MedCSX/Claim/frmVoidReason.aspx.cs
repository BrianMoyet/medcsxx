﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmVoidReason : System.Web.UI.Page
    {
        private int reserveId,              //reserve ID to create transaction for.  Input parameter
            //salvageId,                      //salvage_Id of salvage row inserted
            //subroId,                        //subro_Id of subro row inserted
           reissueFromId,                  //Id to reissue from 
           m_mode,                         //which screen to present? Subro or Salvage
           reserveTypeId = 0,            //Policy_Coverage_Type_Id
           claim_id = 0;

        private Reserve m_reserve;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void cboVoidReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["voidReasonId"] = this.cboVoidReason.SelectedValue;
        }

        private string voidReasonTable = "";

        //claim_id for reserve ID
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = "Void Reason";

            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            m_reserve = new Reserve(this.reserveId);
            this.m_mode = Convert.ToInt32(Request.QueryString["mode"].ToString());

            voidReasonTable = Request.QueryString["void_reason_table"].ToString();

            mm.UpdateLastActivityTime();

            if (!Page.IsPostBack)
            {
                bool reasonsLoaded = LoadReasons();

                //if (this.voidReasonTable == "Draft_Void_Reason")

                //else if (this.voidReasonTable == "Subro_Salvage_Void_Reason")
                //    this.cboVoidReason.SelectedIndex = setSelection((int)modGeneratedEnums.SubroSalvageVoidReason.NSF_Stop_Payment);
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Session["voidReasonId"] = this.cboVoidReason.SelectedValue;
           // ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);

        }

        private bool LoadReasons()
        {
          
                this.cboVoidReason.DataSource = TypeTable.getTypeTableRowsDDL(this.voidReasonTable);
                this.cboVoidReason.DataValueField = "Id";
                this.cboVoidReason.DataTextField = "Description";
                this.cboVoidReason.DataBind();

                this.cboVoidReason.SelectedIndex = (int)modGeneratedEnums.DraftVoidReason.Printing_Problem;
                this.cboVoidReason_SelectedIndexChanged(null, null);
           
            return true;

          
        }

        //private IEnumerable LoadReasons(List<Hashtable> list)
        //{
        //    myReasonList = new List<ReasonList>();
        //    try
        //    {
        //        foreach (Hashtable rData in list)
        //        {
        //            ReasonList rl = new ReasonList();
        //            rl.id = (int)rData["Id"];
        //            rl.imageIdx = (int)rData["Image_Index"];
        //            rl.description = (string)rData["Description"];
        //            myReasonList.Add(rl);
        //        }
        //        return myReasonList;
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //        return myReasonList;
        //    }
        //}
    }
}
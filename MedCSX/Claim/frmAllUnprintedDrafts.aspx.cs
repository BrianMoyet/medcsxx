﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmAllUnprintedDrafts : System.Web.UI.Page
    {
        private static clsDatabase db = new clsDatabase();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue) == false)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }

            grDrafts.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Unprinted_Drafts");
            grDrafts.DataBind();
        }
    }
}
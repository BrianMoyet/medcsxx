﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxDatabase;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmNewFileNote : System.Web.UI.Page
    {
        private static modLogic ml = new modLogic();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();


        private string _selectedUser1;
        private string _selectedUser2;
        private List<Hashtable> DataRows;
        private List<FileData> myFileNoteType;
        private List<PreviousFileNoteGrid> myPreviousFileNote;
        private List<FileData> myReserveData;
        private List<FileData> myClaimantData;
        private List<FileData> myDiaryData;
        public ObservableCollection<DiaryList> myDiaryList { get; set; }
        public ObservableCollection<AlertList> myAlertList { get; set; }

        private bool m_DataChanged = false;
        private MedCsxLogic.Claim myClaim = null;
        private UnsavedFileNote myUnsavedFileNote = null;
        private Claimant myClaimant = null;
        private bool createNewDiaryForMe = false;
        private bool firstActivated = true;
        private bool _isDiary = false;
        private int _salvageId = 0;

        public bool okPressed = false;  //has OK button been pressed?
        public int fileNoteId = 0;  //ID of newly inserted file note
        public int claimId = 0;
        private string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            createNewDiaryForMe = Convert.ToBoolean(Request.QueryString["create_new_diary"]);
            claimId = Convert.ToInt32(Request.QueryString["claim_id"]);

            this.myClaim = new MedCsxLogic.Claim(claimId);

            myAlertList = new ObservableCollection<AlertList>();
            myDiaryList = new ObservableCollection<DiaryList>();

            if (Session["referer2"] != null)
            {
                referer = Session["referer2"].ToString();
                Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();


            chkFileNoteAlertDefer.Visible = true;
            tbFileNoteAlertDefer.Visible = true;

            if (!Page.IsPostBack) { 


                //lbFileNoteAlert.DataSource = myAlertList;
                //lbFileNoteAlert.DataBind();
                    //Load_lbAlertUserNames();
                    //Load_lbDiaryUserNames();

                this.LoadUsers();

                lbFileNoteAlert.DataSource = myAlertList;
                lbFileNoteAlert.DataBind();

                lbFileNoteDiary.DataSource = myDiaryList;
                lbFileNoteDiary.DataBind();

                if (createNewDiaryForMe)
                {
                    for (int i = 0; i < lbFileNoteDiary.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(lbFileNoteDiary.DataKeys[i].Value) == Convert.ToInt32(Session["userID"]))
                        {
                            CheckBox mycheck = ((CheckBox)lbFileNoteDiary.Rows[i].FindControl("cbSelect"));
                            mycheck.Checked = true;
                           
                        }
                    }

                   
                }

               

                bool fileNoteTypesLoaded = this.LoadFileNoteTypes();  //load file note type DataGrid
                //this.tbFileNoteAlertDefer.Text = mm.DefaultDeferredDate().ToString("MM/dd/yyyy hh:mm:ss tt");  //set default deferred date for alert
                this.tbFileNoteAlertDefer.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");  //set default deferred date for alert
                bool previousFileNotesLoaded = this.LoadPreviousFileNotes();  //load previous file notes DataGrid

                DataTable DataRowsDT = mm.GetMyClaimants(this.claimId);
                //this.myClaimants = mm.LoadClaimants(this.myClaim.PersonNamesAndTypes, 9);//LoadPerson(this.myClaim.PersonNamesAndTypes);
                DataRow newRowR = DataRowsDT.NewRow();
                newRowR["claimant_Id"] = "0";
                newRowR["person"] = "";

                DataRowsDT.Rows.InsertAt(newRowR, 0);

                this.cboFileNoteClaimant.DataSource = DataRowsDT; //claimantData();  //load Claimant combo box
                this.cboFileNoteClaimant.DataValueField = "claimant_Id";
                this.cboFileNoteClaimant.DataTextField = "person";
                this.cboFileNoteClaimant.DataBind();

                if (this.cboFileNoteClaimant.Items.Count > 0)
                    cboFileNoteClaimant_SelectedIndexChanged(null, null);



            

                this.cboFileNoteDiary.DataSource = LoadDiaryData("Diary_Type");    //load Diary type combo box
                this.cboFileNoteDiary.DataTextField = "fileType";
                this.cboFileNoteDiary.DataValueField = "fileId";
                this.cboFileNoteDiary.DataBind();

                      //load alert and diary users from system users



                this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - New File Note";   //set title of window
                if (this.myUnsavedFileNote != null)
                    LoadUnsavedFileNote();
                if (this.createNewDiaryForMe)
                {
                    foreach (GridViewRow gvRow in grFileNoteType.Rows)
                    {
                        if ((int)grFileNoteType.DataKeys[gvRow.DataItemIndex].Value == 12)
                        {
                            grFileNoteType.SelectedIndex = gvRow.DataItemIndex;
                            break;
                        }
                    }
                }
                //grFileNoteType.SelectedValue = this.SelectFileNoteTypeRow("Diary");  //12 select the diary row on the type grid
                else
                {
                    foreach (GridViewRow gvRow in grFileNoteType.Rows)
                    {
                        if ((int)grFileNoteType.DataKeys[gvRow.DataItemIndex].Value == 1)
                        {
                            grFileNoteType.SelectedIndex = gvRow.DataItemIndex;
                            break;
                        }
                    }
                }
                   // grFileNoteType.SelectedIndex = this.SelectFileNoteTypeRow("Memo to File");  //1
                //this.DataChanged = false;
                this.firstActivated = true;


                //for (int i = 0; i < lbFileNoteAlert.Rows.Count; i++)
                //{
                //    if (Convert.ToInt32(lbFileNoteAlert.DataKeys[i].Value) == MedCsXSystem.UserId)
                //    {
                //        CheckBox mycheck = ((CheckBox)lbFileNoteAlert.Rows[i].FindControl("cbSelect"));
                //        mycheck.Checked = true;

                //    }
                //}
                //if (createNewDiaryForMe)
                //{
                //    for (int i = 0; i < lbFileNoteDiary.Rows.Count; i++)
                //    {
                //        if (Convert.ToInt32(lbFileNoteDiary.DataKeys[i].Value) == Convert.ToInt32(Session["userID"])) ;
                //        {
                //            CheckBox mycheck = ((CheckBox)lbFileNoteDiary.Rows[i].FindControl("cbSelect"));
                //            mycheck.Checked = true;

                //        }
                //    }

                //    chkFileNoteAlertDefer.Visible = true;
                //    tbFileNoteAlertDefer.Visible = true;
                //}
            }
        }

        private bool DataChanged
        {
            get { return this.m_DataChanged; }
            set
            {
                this.m_DataChanged = value;
                if (value)
                {
                    this.btnFileActivityOK.Visible = true;
                    this.btnFileActivityOK.Enabled = true;
                }
                else
                {
                    this.btnFileActivityOK.Visible = false;
                    this.btnFileActivityOK.Enabled = false;
                }
            }
        }

        private bool LoadFileNoteTypes()
        {
            grFileNoteType.DataSource = null;
            DataRows = FileNoteType.UserFileNoteTypes;
            //grFileNoteType.Items.Clear();
            if (DataRows.Count > 0)
            {
                grFileNoteType.DataSource = LoadFileNoteTypes(DataRows);
                grFileNoteType.DataBind();
                //grFileNoteType.UpdateLayout();
                //grFileNoteType.SelectedIndex = 1;
            }
            //grFileNoteType.Items.Refresh();
            return true;
        }

        private IEnumerable LoadFileNoteTypes(List<Hashtable> fnData)
        {
            myFileNoteType = new List<FileData>();
            foreach (Hashtable myData in fnData)
            {
                myFileNoteType.Add(new FileData()
                {
                    fileId = (int)myData["File_Note_Type_Id"],
                    imageIdx = (int)myData["Image_Index"],
                    description = (string)myData["Description"]
                });
            }
            return myFileNoteType;
        }


        private int SelectFileNoteTypeRow(string fileNote_id)
        {
            //for (int i = 0; i < grFileNoteType.Items.Count; i++)
            for (int i = 0; i < myFileNoteType.Count; i++)
            {
                //DataGridRow row = (DataGridRow)grFileNoteType.ItemContainerGenerator.ContainerFromIndex(i);
                //if (row == null)
                //{
                //    object notitem = grFileNoteType.Items[i];
                //    grFileNoteType.SelectedItem = notitem;
                //    grFileNoteType.ScrollIntoView(notitem);
                //    row = grFileNoteType.ItemContainerGenerator.ContainerFromIndex(i) as DataGridRow;
                //}
                //TextBlock cellContent = grFileNoteType.Columns[2].GetCellContent(row) as TextBlock;
                //if (cellContent != null && cellContent.Text.Equals(fileNote_id))
                //{
                //    object item = grFileNoteType.Items[i];
                //    grFileNoteType.SelectedItem = item;
                //    grFileNoteType.ScrollIntoView(item);
                //    row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                //    break;
                //}
                if (myFileNoteType[i].description == fileNote_id)
                {
                    //object item = grFileNoteType.Items[i];
                    //grFileNoteType.SelectedValue = item;
                    //grFileNoteType.ScrollIntoView(item);
                    return i;
                }
            }
            return 0;
        }

        private void LoadUnsavedFileNote()
        {
            //this.grFileNoteType.SelectedIndex = this.myUnsavedFileNote.FileNoteTypeId;
            //this.grFileNoteType.ScrollIntoView(this.grFileNoteType.SelectedItem);
            //this.tbFileNoteText.Text = this.myUnsavedFileNote.FileNoteText;

            //if (this.myUnsavedFileNote.ClaimantId != 0)
            //{
            //    for (int i = 0; i < myClaimantData.Count; i++)
            //    {
            //        if (myClaimantData[i].fileId == this.myUnsavedFileNote.Claimant.PersonId)
            //        {
            //            this.cboFileNoteClaimant.SelectedItem = myClaimantData[i];
            //            break;
            //        }
            //    }
            //}

            //if (this.myUnsavedFileNote.ReserveId != 0)
            //{
            //    for (int i = 0; i < myReserveData.Count; i++)
            //    {
            //        if (myReserveData[i].fileId == this.myUnsavedFileNote.ReserveId)
            //        {
            //            this.cboFileNoteReserve.SelectedItem = myReserveData[i];
            //            break;
            //        }
            //    }
            //}

            //int thisDiaryTypeId;
            //if (this.myUnsavedFileNote.DiaryTypeId != 0)
            //    thisDiaryTypeId = this.myUnsavedFileNote.DiaryTypeId;
            //else
            //    thisDiaryTypeId = (int)modGeneratedEnums.DiaryType.General_Diary;

            //for (int i = 0; i < myDiaryData.Count; i++)
            //{
            //    if (myDiaryData[i].fileId == thisDiaryTypeId)
            //    {
            //        this.cboFileNoteDiary.SelectedValue = myDiaryData[i];
            //        break;
            //    }
            //}

            //if (this.myUnsavedFileNote.DiaryNbrDays > 0)
            //    this.tbFileNoteDiaryDue.Text = this.myUnsavedFileNote.DiaryNbrDays.ToString();

            //if (this.myUnsavedFileNote.IsDeferred)
            //    this.chkFileNoteAlertDefer.IsChecked = true;

            //if (this.myUnsavedFileNote.IsSummary)
            //    this.chkFileNoteTextSummary.IsChecked = true;

            //if (this.myUnsavedFileNote.IsUrgent)
            //    this.chkFileNoteUrgent.IsChecked = true;

            //if (this.myUnsavedFileNote.DeferredDate != ml.DEFAULT_DATE)
            //    this.tbFileNoteAlertDefer.Text = this.myUnsavedFileNote.DeferredDate.ToString();

            //ArrayList names = this.myUnsavedFileNote.AlertNames();
            //string thisUser;
            //bool alertMade = false;
            //bool diaryMade = false;

            //foreach (AlertList al in myAlertList)
            //{
            //    thisUser = al.Name;
            //    if (names.Contains(thisUser))
            //    {
            //        al.isChecked = true;
            //        alertMade = true;
            //    }
            //    else
            //        al.isChecked = false;
            //}

            //if (alertMade)
            //{
            //    this.chkFileNoteAlertDefer.Visibility = Visibility.Visible;
            //    this.chkFileNoteUrgent.Visibility = Visibility.Visible;
            //    if ((bool)this.chkFileNoteAlertDefer.IsChecked)
            //        this.tbFileNoteAlertDefer.Visibility = Visibility.Visible;
            //}

            //names = this.myUnsavedFileNote.DiaryNames();

            //foreach (DiaryList dl in myDiaryList)
            //{
            //    thisUser = dl.Name;
            //    if (names.Contains(thisUser))
            //    {
            //        dl.isChecked = true;
            //        diaryMade = true;
            //    }
            //    else
            //        dl.isChecked = false;
            //}

            //if (diaryMade)
            //{
            //    this.fraDiaryType.Visibility = Visibility.Visible;
            //}
        }

        private void LoadUsers()
        {
            myDiaryList.Clear();
            myAlertList.Clear();
            //lbFileNoteAlert.Items.Clear();
            //lbFileNoteDiary.Items.Clear();

            AlertList al1 = new AlertList();
            al1.Name = "Assigned Adjuster";
            al1.Id = 9999999;
            //this.lbFileNoteAlert.Items.Add(al);
            myAlertList.Add(al1);

            AlertList al2 = new AlertList();
            al2.Name = "Their Supervisor";
            al2.Id = 9999998;
            //this.lbFileNoteAlert.Items.Add(al);
            myAlertList.Add(al2);

            //AlertList al3 = new AlertList();
            //al3.Name = "Myself";
            //al3.Id = Convert.ToInt32(Session["userID"]);
            ////this.lbFileNoteAlert.Items.Add(al);
            //myAlertList.Add(al3);


            DiaryList dl1 = new DiaryList();
            dl1.Name = "Assigned Adjuster";
            dl1.Id = 9999999;
            //this.lbFileNoteAlert.Items.Add(al);
            myDiaryList.Add(dl1);

            DiaryList dl2 = new DiaryList();
            dl2.Name = "Their Supervisor";
            dl2.Id = 9999998;
            //this.lbFileNoteAlert.Items.Add(al);
            myDiaryList.Add(dl2);

            //DiaryList dl3 = new DiaryList();
            //dl3.Name = "Myself";
            //dl3.Id = Convert.ToInt32(Session["userID"]);
            ////this.lbFileNoteAlert.Items.Add(al);
            //myDiaryList.Add(dl3);


            foreach (User v in MedCsXSystem.Users())
            {
                if (v.AlertPreferenceId != (int)modGeneratedEnums.AlertPreference.None)
                {
                    if ((v.Name.Trim() != "") && (v.LoginId != "admin") && (v.UserStatusId == (int)modGeneratedEnums.UserStatus.Active))
                    {
                        AlertList al = new AlertList();
                        al.Name = v.Name;
                        al.Id = v.Id;
                        //this.lbFileNoteAlert.Items.Add(al);
                        myAlertList.Add(al);

                        DiaryList dl = new DiaryList();
                        if ((v.Id == Convert.ToInt32(Session["userID"])) && this.createNewDiaryForMe)
                        {
                            dl.isChecked = true;
                        }
                        dl.Name = v.Name;
                        dl.Id = v.Id;
                        //lbFileNoteDiary.Items.Add(dl);
                        myDiaryList.Add(dl);
                    }
                }
            }
            //this.DataContext = this;
        }

        private IEnumerable LoadDiaryData(string tableName)
        {
            myDiaryData = new List<FileData>();
            foreach (Hashtable dData in TypeTable.getTypeTableRows(tableName))
            {
                if ((int)dData["Id"] != 0)
                {
                    myDiaryData.Add(new FileData()
                    {
                        fileId = (int)dData["Id"],
                        imageIdx = (int)dData["Image_Index"],
                        fileType = (string)dData["Description"]
                    });
                }
            }
            return myDiaryData;
        }

        private IEnumerable claimantData()
        {
            DataRows = this.myClaim.ClaimantNamesAndTypes;
            myClaimantData = new List<FileData>();
            if (DataRows.Count <= 0)
                return myClaimantData;

            foreach (Hashtable myClaimant in DataRows)
            {
                if ((string)myClaimant["Name"] != string.Empty)
                {
                    myClaimantData.Add(new FileData()
                    {
                        fileId = (int)myClaimant["Person_Id"],
                        fileName = (string)myClaimant["Name"],
                        fileType = (string)myClaimant["Type"]
                    });
                }
            }
            return myClaimantData;
        }

        private bool LoadPreviousFileNotes()
        {
            //grFileActivityPrevious.Datas = null;
            DataTable dt = this.myClaim.GetFileNotes(0, 0, 0);
            //grFileActivityPrevious.Items.Clear();
            if (dt.Rows.Count > 0)
            {
                grFileActivityPrevious.DataSource = dt;
                grFileActivityPrevious.DataBind();
                grFileActivityPrevious.SelectedIndex = 0;
                grFileActivityPrevious_SelectedIndexChanged(null, null);
            }
            
            //grFileActivityPrevious.Items.Refresh();
            return true;
        }

        //private void Load_lbAlertUserNames()
        //{
        //    try
        //    {
        //        foreach (AlertList dl in myAlertList)
        //            lbAlertUserNames.Items.Add(dl.Name);
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //    }
        //}

        //private void Load_lbDiaryUserNames()
        //{
        //    try
        //    {
        //        foreach (DiaryList dl in myDiaryList)
        //            lbDiaryUserNames.Items.Add(dl.Name);
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //    }
        //}

       

        class FileData
        {
            public int fileId { get; set; }
            public string fileName { get; set; }
            public string fileType { get; set; }
            public int imageIdx { get; set; }
            public string description { get; set; }
          
        }

        class PreviousFileNoteGrid
        {
            public int fileNoteId { get; set; }
            public DateTime dtNoteCreated { get; set; }
            public string fileNoteType { get; set; }
            public string diaryType { get; set; }
            public string diaryCleared { get; set; }
            public string userId { get; set; }
            public string claimantId { get; set; }
            public string reserveId { get; set; }
            public string description { get; set; }
        }

        public class AlertList : INotifyPropertyChanged
        {
            private bool _isChecked = false;
            private string _Name = "";
            private int _Id = 0;

            public event PropertyChangedEventHandler PropertyChanged;

            private void NotifyPropertyChanged(string propertyName = "")
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

            public bool isChecked
            {
                get { return _isChecked; }
                set
                {
                    if (value != _isChecked)
                    {
                        _isChecked = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            public string Name
            {
                get { return _Name; }
                set
                {
                    if (value != _Name)
                    {
                        _Name = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            public int Id
            {
                get { return _Id; }
                set
                {
                    if (value != _Id)
                    {
                        _Id = value;
                        NotifyPropertyChanged();
                    }
                }
            }


        }

        public class DiaryList : INotifyPropertyChanged
        {
            private bool _isChecked = false;
            private string _Name = "";
            private int _Id = 0;

            public event PropertyChangedEventHandler PropertyChanged;

            private void NotifyPropertyChanged(string propertyName = "")
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

            public bool isChecked
            {
                get { return _isChecked; }
                set
                {
                    if (value != _isChecked)
                    {
                        _isChecked = value;
                        NotifyPropertyChanged();
                    }
                }
            }
            public string Name
            {
                get { return _Name; }
                set
                {
                    if (value != _Name)
                    {
                        _Name = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            public int Id
            {
                get { return _Id; }
                set
                {
                    if (value != _Id)
                    {
                        _Id = value;
                        NotifyPropertyChanged();
                    }
                }
            }
        }

       

       

        protected void cboFileNoteClaimant_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                string claimaintId = cboFileNoteClaimant.SelectedValue.ToString();


                ////this.DataChanged = true;
                //this.myClaimant = new Claimant(Convert.ToInt32(claimaintId));

                //myReserveData = new List<FileData>();
                //foreach (Reserve res in this.myClaimant.Reserves())
                //{
                //    myReserveData.Add(new FileData()
                //    {
                //        fileId = res.ReserveType.ReserveTypeId,
                //        fileName = res.ReserveType.Description
                //    });
                //}

                DataTable DataRows = new DataTable();
                DataRows = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claimant", Convert.ToInt32(cboFileNoteClaimant.SelectedValue)); 

                DataRow newRowR = DataRows.NewRow();
                newRowR["Description"] = "";
                newRowR["Reserve_Id"] = "0";
                DataRows.Rows.InsertAt(newRowR, 0);

                this.cboFileNoteReserve.DataSource = DataRows;
                this.cboFileNoteReserve.DataTextField = "Description";
                this.cboFileNoteReserve.DataValueField = "Reserve_Id";
                this.cboFileNoteReserve.DataBind();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private IEnumerable reserveData()
        {
            myReserveData = new List<FileData>();
            if (this.myClaimant == null)
                return myReserveData;

            foreach (Reserve res in this.myClaimant.Reserves())
            {
                myReserveData.Add(new FileData()
                {
                    fileId = res.ReserveType.ReserveTypeId,
                    fileName = res.ReserveType.Description
                });
            }
            return myReserveData;
        }

        private bool IsDiary()
        {

            bool isDiary = false;
            for (int i = 0; i < lbFileNoteDiary.Rows.Count; i++)
            {
                GridViewRow row = lbFileNoteDiary.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {
                    isDiary = true;
                }
            }

            return isDiary;
        }

        

      

        private bool ValidateFields()
        {
            //claimantId
            if (this.cboFileNoteClaimant.SelectedValue == null)
                return false;
            //reserveId
            if (this.cboFileNoteReserve.SelectedValue == null)
                return false;
            //fileNoteText
            if (this.tbFileNoteText.Text == "")
                return false;
            //fileNoteTypeId
            if (this.grFileNoteType.SelectedValue == null)
                return false;

            if (IsDiary())
            {
                //diaryId
                //if (this.lbFileNoteDiary.SelectedValue == null)
                //{
                //    ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Diary for not selected.');</script>");
                //    return false;
                //}
                    
                if (this.cboFileNoteDiary.SelectedValue == null)
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Diary type is required.');</script>");
                    return false;
                }

                if (tbFileNoteDiaryDue.Text.Length < 1)
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Enter diary due date.');</script>");
                    return false;
                }

                if (Convert.ToInt32(tbFileNoteDiaryDue.Text) < -1)
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Diary due date can only be one day in the past.');</script>");
                    return false;
                }

                //double dblDays;
                //if (!double.TryParse(this.tbFileNoteDiaryDue.Text, out dblDays))
                //    return false;
                //else if (dblDays <= 0)
                //    return false;
            }
            else
            {
                //userId
                //if (this.lbFileNoteAlert.SelectedValue == null)
                //    return false;
            }

            return true;
        }

        protected void btnFileActivityOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();


                if (!ValidateFields())
                    return;  //couldn't validate the fields according to the data dictionary

                //check for number of dyas entered when diary person is selected
                if (createNewDiaryForMe && (tbFileNoteDiaryDue.Text.Trim() == ""))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter number of days to set this diary for.');</script>");
                    //MessageBox.Show("Please enter number of days to set this diary for.", "Diary Days Not Entered", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    if (this.tbFileNoteDiaryDue.Visible == true)
                        this.tbFileNoteDiaryDue.Focus();
                    return;
                }

                //check diary person selected if Number of Days entered
                if (!IsDiary() && this.tbFileNoteDiaryDue.Text.Trim() != string.Empty)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please check the user(s) you want this diary set for.');</script>");
                    //MessageBox.Show("Please check the user(s) you want this diary set for.", "No Diary User Checked", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    if (this.lbFileNoteDiary.Visible == true)
                        this.lbFileNoteDiary.Focus();
                    return;
                }

                //check file note text entered
                if (tbFileNoteText.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter file note text.');</script>");
                    //MessageBox.Show("Invalid Alert Deferred Date Entered", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    if (this.tbFileNoteText.Visible == true)
                        this.tbFileNoteText.Focus();
                    return;
                }

                if (chkFileNoteAlertDefer.Checked)
                {
                    //check valid deferred date
                    if (!ml.isDate(this.tbFileNoteAlertDefer.Text))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Invalid Alert Deferred Date Entered');</script>");
                        //MessageBox.Show("Invalid Alert Deferred Date Entered", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //if (this.tbFileNoteAlertDefer.Visible == true)
                        this.tbFileNoteAlertDefer.Focus();
                        return;
                    }
                }

                //make sure claimant & reserve are entered if necessary for file note type
                FileNoteType fileNoteType = new FileNoteType(Convert.ToInt32(grFileNoteType.DataKeys[grFileNoteType.SelectedIndex].Value));

                if (fileNoteType.FileNoteLevelId != (int)modGeneratedEnums.FileNoteLevel.Claim)
                {
                    if (this.cboFileNoteClaimant.Text.Trim() == string.Empty)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Claimant Must Be Entered for This File Note Type');</script>");
                        //MessageBox.Show("Claimant Must Be Entered for This File Note Type", "Claimant Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        this.cboFileNoteClaimant.Focus();
                        return;
                    }
                }

                if ((fileNoteType.FileNoteLevelId == (int)modGeneratedEnums.FileNoteLevel.Reserve) || (fileNoteType.FileNoteLevelId == (int)modGeneratedEnums.FileNoteLevel.Transaction))
                {
                    if (this.cboFileNoteReserve.Text.Trim() == string.Empty)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reserve Must Be Enterd for This File Note Type');</script>");
                        //MessageBox.Show("Reserve Must Be Enterd for This File Note Type", "Reserve Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        this.cboFileNoteReserve.Focus();
                        return;
                    }
                }

                int numDays = 0;
                if (!int.TryParse(this.tbFileNoteDiaryDue.Text.Trim(), out numDays))
                {
                    numDays = 0;
                    this.tbFileNoteDiaryDue.Text = "";
                }

                //if old diaries are to be cleared, warn if new diary doesn't meet time requirements
                int maxDiaryDays = UserTypeDiary.MaxDiaryDaysFor(ml.CurrentUser.UserType(), this.myClaim.DaysSinceReported);

                if (IsDiary() && (maxDiaryDays > 0) && (numDays > maxDiaryDays) &&
                    (this.myClaim.DiariesForCurrentUser().Count > 0) &&
                    ml.CurrentUser.shouldForceDiary && ml.CurrentUser.isAssignedToClaim(this.myClaim))
                {
                    ArrayList DiaryArgs = new ArrayList();
                    string s = "../../Forms/Images/mnuYellowWarning.png";
                    DiaryArgs.Add(s);
                    s = "Diary Warning";
                    DiaryArgs.Add(s);
                    s = "Warning - the new diary entry does not meet the diary requirement of " + maxDiaryDays + " days.  Old diary entries meeting the time requirements will not be cleared.";
                    DiaryArgs.Add(s);
                    DiaryArgs.Add(true);
                    s = "../../Forms/Images/icoSmileyBlank.png";
                    DiaryArgs.Add(s);
                    s = "Leave New Diary As Is";
                    DiaryArgs.Add(s);
                    s = "../../Forms/Images/icoSmiley.png";
                    DiaryArgs.Add(s);
                    s = "Change New Diary to " + maxDiaryDays + " Days to Meet Requirements";
                    DiaryArgs.Add(s);

                    //frmAutoClearDiaryWarning f = new frmAutoClearDiaryWarning(maxDiaryDays);
                    //frmCustomMessageBox f = new frmCustomMessageBox(DiaryArgs);
                    //f.ShowDialog();

                    //if (f.ChangeDiary)
                    numDays = maxDiaryDays;
                }

                //get Alert Users
                ArrayList au = new ArrayList();
                bool auAssignedAdjuster = false;
                bool auTheirSupervisor = false;
                bool auTechnicalSupervisor = false;

                for (int i = 0; i < lbFileNoteAlert.Rows.Count; i++)
                {
                    GridViewRow row = lbFileNoteAlert.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                    if (isChecked)
                    {
                        if (lbFileNoteAlert.Rows[i].Cells[1].Text == "Assigned Adjuster")
                            auAssignedAdjuster = true;
                        else if (lbFileNoteAlert.Rows[i].Cells[1].Text == "Their Supervisor")
                            auTheirSupervisor = true;
                        else if (lbFileNoteAlert.Rows[i].Cells[1].Text == "Technical Supervisor")
                            auTechnicalSupervisor = true;
                        else
                            au.Add(lbFileNoteAlert.Rows[i].Cells[1].Text);
                    }
                }

                ArrayList du = new ArrayList();
                bool duAssignedAdjuster = false;
                bool duTheirSupervisor = false;
                bool duTechnicalSupervisor = false;

                for (int i = 0; i < lbFileNoteDiary.Rows.Count; i++)
                {
                    GridViewRow row = lbFileNoteDiary.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                    if (isChecked)
                    {
                        if (lbFileNoteDiary.Rows[i].Cells[1].Text == "Assigned Adjuster")
                            duAssignedAdjuster = true;
                        else if (lbFileNoteDiary.Rows[i].Cells[1].Text == "Their Supervisor")
                            duTheirSupervisor = true;
                        else if (lbFileNoteDiary.Rows[i].Cells[1].Text == "Technical Supervisor")
                            duTechnicalSupervisor = true;
                        else
                            du.Add(lbFileNoteDiary.Rows[i].Cells[1].Text);
                    }
                }

                string tmpcboFileNoteClaimant = "";
                if (this.cboFileNoteClaimant.SelectedIndex > 0)
                    tmpcboFileNoteClaimant = this.cboFileNoteClaimant.SelectedItem.ToString();

                string tmpcboFileNoteReserve = "";
                if (this.cboFileNoteReserve.SelectedIndex > 0)
                    tmpcboFileNoteReserve = this.cboFileNoteReserve.SelectedItem.ToString();

                string tmpcboFileNoteDiary = "";
                if (createNewDiaryForMe)
                    tmpcboFileNoteDiary = this.cboFileNoteDiary.SelectedItem.ToString();


                DateTime dt = DateTime.Parse(this.tbFileNoteAlertDefer.Text.Trim());

                DateTime dtFinal = new DateTime();
                DateTime dtDefault = DateTime.Now.AddDays(1);

                if (chkFileNoteAlertDefer.Checked)
                {
                    try
                    {
                        dtFinal = dt;
                    }
                    catch (Exception ex)
                    {
                        //throw ex;
                    }
                }
                else
                {
                    dtFinal = Convert.ToDateTime("1900-01-01 00:00:00.000");
                }


                //if (dt.Date != dtDefault.Date)
                //{
                //    dtFinal = Convert.ToDateTime(this.tbFileNoteAlertDefer.Text.Trim());

                //}
                //else
                //{

                //    if (chkFileNoteAlertDefer.Checked)
                //        dtFinal = Convert.ToDateTime(this.tbFileNoteAlertDefer.Text.Trim());
                //    else
                //        dtFinal = Convert.ToDateTime("1900-01-01 00:00:00.000");
                //}

                try
                {
                    //everything is OK - insert file note
                    fileNoteId = FileNote.InsertManualFileNote(this.myClaim.Id,
                    this.tbFileNoteText.Text.Trim(), fileNoteType.Description,
                    auAssignedAdjuster,
                   auTheirSupervisor,
                    auTechnicalSupervisor,
                    au,
                    duAssignedAdjuster,
                    duTheirSupervisor,
                    duTechnicalSupervisor,
                    du,
                    (short)numDays, tmpcboFileNoteClaimant,
                    tmpcboFileNoteReserve, tmpcboFileNoteDiary,
                    (bool)this.chkFileNoteAlertDefer.Checked,
                    (bool)this.chkFileNoteTextSummary.Checked,
                    dtFinal, (bool)this.chkFileNoteUrgent.Checked);
                }
                catch (Exception ex)
                { }

                //delete the unsaved file note now that real file note has been inserted
                //if (this.myUnsavedFileNote != null)
                //    this.myUnsavedFileNote.DeleteRow();

                if (this.IsDiary() && ml.CurrentUser.AutoClearOlderDiaries)
                    this.AutoClearOldDiaries();

                //salvage unlock
                if (!this.IsDiary() && this._salvageId != 0 && (fileNoteType.FileNoteTypeId == (int)modGeneratedEnums.FileNoteType.Salvage_Status))
                {
                    foreach (Hashtable salv in mm.GetSalvageHashForClaim(this.myClaim.Id))
                    {
                        if ((int)salv["Salvage_Id"] == this._salvageId)
                        {
                            Salvage sal = new Salvage(_salvageId);
                            if (sal.IsLocked)
                                sal.IsLocked = false;
                            break;
                        }
                    }
                }

                if (Convert.ToInt32(grFileNoteType.DataKeys[grFileNoteType.SelectedIndex].Value) == (int)modGeneratedEnums.FileNoteType.Liability_Evaluation)
                {
                    if (myClaim.AtFaultTypeId == (int)modGeneratedEnums.AtFaultType.Undetermined)
                    {
                        string url = "frmSetAtFault.aspx?claim_id=" + this.claimId;
                        string s = "window.open('" + url + "', 'popup_windowaf', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=400, copyhistory=no, left=400, top=200');";
                        ClientScript.RegisterStartupScript(this.GetType(), "scriptaf", s, true);

                    }

                }
                //the ok button was pressed
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(''MecCsX Error: " + ex.ToString() + "');</script>");
            }
            finally
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
            }
        }

        private void AutoClearOldDiaries()
        {
            try
            {
                int maxDiaryDays = this.myClaim.maxDiaryDaysAllowed;
                DateTime maxDueDate = ml.DBNow().AddDays(maxDiaryDays);
                ArrayList diaries = this.myClaim.DiariesForCurrentUser();

                if (diaries.Count == 0)
                    return;

                Diary newDiary = (Diary)diaries[diaries.Count - 1];
                bool newDiaryMeetsTimeRequirements = true;

                if ((maxDiaryDays > 0) && (newDiary.NbrDays > maxDiaryDays))
                    newDiaryMeetsTimeRequirements = false;

                foreach (Diary d in diaries)
                {
                    if (newDiary.DiaryId != d.DiaryId)
                    {
                        if (newDiaryMeetsTimeRequirements ||
                            (!newDiaryMeetsTimeRequirements && d.DiaryDueDate > maxDueDate))
                            d.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void picDiaryCalendar_TextChanged(object sender, EventArgs e)
        {
            DateTime? selectedDate = Convert.ToDateTime(picDiaryCalendar.Text);
            if (selectedDate == null)
            {
                tbFileNoteDiaryDue.Text = string.Empty;
                tbFileNoteDiaryDue.Visible = false;
            }
            else
            {
                tbFileNoteDiaryDue.Text = ((DateTime)selectedDate).Subtract(DateTime.Today).Days.ToString();
                tbFileNoteDiaryDue_TextChanged(null, null);
            }
        }

        protected void grFileActivityPrevious_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grFileActivityPrevious.Rows.Count > 0)
            {
                Hashtable ht = ml.getRow("File_Note", Convert.ToInt32(grFileActivityPrevious.DataKeys[grFileActivityPrevious.SelectedIndex].Value));
                tbFileActivityPreviousText.Text = grFileActivityPrevious.SelectedRow.Cells[7].Text;
                chkFileNoteTextSummary.Checked = (bool)ht["Is_Summary"];
            }
        }

        protected void tbFileNoteDiaryDue_TextChanged(object sender, EventArgs e)
        {
            if ((tbFileNoteDiaryDue.Text.Trim() == "") || (modLogic.isNumeric(tbFileNoteDiaryDue.Text) == false ))
            {
                tbFileNoteDiaryDue.Text = "";
                lblDiaryDueDate.Visible = false;
                return;
            }

            DateTime dueDate = DateTime.Now.AddDays(Convert.ToInt32(tbFileNoteDiaryDue.Text));
            lblDiaryDueDate.Text = dueDate.ToLongDateString();
            lblDiaryDueDate.Visible = true;
           
        }
    }
}
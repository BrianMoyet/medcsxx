﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;

namespace MedCSX.Claim
{
    public partial class reserves : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " -  Transactions for ";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            DataTable dt = new DataTable();

            Reserve myData = new Reserve();
            dt = myData.GeTransactions_By_ReserveId(id);

            //dt.DefaultView.Sort = "created_date Desc";
            gvTransactions.DataSource = dt;
            gvTransactions.DataBind();
            gvTransactions.Visible = true;
            Session["Transactions" + id + "Table"] = dt;

            Reserve myReserve= new Reserve();
            myReserve.GetReserveByID(id);
            lblClaimant.Text = myReserve.claimant_name;
            lblCoverageLimit.Text = HelperFunctions.GetReserveCoverageDescriptionByID(Convert.ToInt32(myReserve.claim_id), Convert.ToInt32(myReserve.policy_coverage_type_id));
            lblCoverageType.Text = myReserve.reserve_type;
            lblExpensePaid.Text = string.Format("{0:c}", Convert.ToDouble(myReserve.paid_expense));
            lblLossPaid.Text = string.Format("{0:c}", Convert.ToDouble(myReserve.paid_loss));
            lblNetExpenseReserve.Text = string.Format("{0:c}", Convert.ToDouble(myReserve.net_expense_reserve));
            lblNetLossReserve.Text = string.Format("{0:c}", Convert.ToDouble(myReserve.net_loss_reserve));

        }

        protected void gvTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Transactions" + id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvTransactions.DataSource = Session["Transactions" + id + "Table"];
                gvTransactions.DataBind();

                Session["Transactions" + id + "Table"] = dt;
            }

        }

        protected void gvTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Reserve myData = new Reserve();
            dt = myData.GeTransactions_By_ReserveId(id);
            gvTransactions.DataSource = dt;
            gvTransactions.PageIndex = e.NewPageIndex;
            DataBind();
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void PopulateControls()
        {
            //DataTable dt = new DataTable();
            //FileNotes myFN = new FileNotes();
            //dt = myFN.GetFileNoteUserTypes();
            //lbFileNoteType.DataSource = dt;
            //lbFileNoteType.DataValueField = dt.Columns[0].ColumnName;
            //lbFileNoteType.DataTextField = dt.Columns[1].ColumnName;
            //lbFileNoteType.DataBind();

            //dt = HelperFunctions.getTypeTableRows("Diary_Type", "Description", "1=1");
            //ddlDiaryType.DataSource = dt;
            //ddlDiaryType.DataValueField = dt.Columns[0].ColumnName;
            //ddlDiaryType.DataTextField = dt.Columns[1].ColumnName;
            //ddlDiaryType.DataBind();

            //DataTable dtc = new DataTable();
            //Claimants myClaimant = new Claimants();
            //dtc = myClaimant.GetClaimantsAndIDsForClaim(claim_id);
            //DataRow newRowC = dtc.NewRow();
            //newRowC["claimant_id"] = 0;
            //newRowC["Person"] = "";
            //dtc.Rows.InsertAt(newRowC, 0);

            //ddlClaimant.DataSource = dtc;
            //ddlClaimant.DataValueField = dtc.Columns[0].ColumnName;
            //ddlClaimant.DataTextField = dtc.Columns[2].ColumnName;
            //ddlClaimant.DataBind();

            //dt = HelperFunctions.getTypeTableRows("Event_Type", "Description", "1=1");
            //ddlEventType.DataSource = dt;
            //ddlEventType.DataValueField = dt.Columns[0].ColumnName;
            //ddlEventType.DataTextField = dt.Columns[1].ColumnName;
            //ddlEventType.DataBind();
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

    }
}
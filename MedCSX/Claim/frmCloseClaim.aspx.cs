﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmCloseClaim : System.Web.UI.Page
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();

        private int _claimId = 0;        //claim ID of this claim
        string referer = "";
        private MedCsxLogic.Claim myClaim = null;
        string msg = "";

        public MedCsxLogic.Claim Claim
        {
            get { return myClaim; }
            set { myClaim = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this._claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            this.Claim = new MedCsxLogic.Claim(_claimId);  //get the claim object

            if (Session["referer2"] != null)
            {
                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();


            if (!Page.IsPostBack)
                LoadOpenItems();
        }

        protected void LoadOpenItems()
        {

            //msg = "" + Environment.NewLine;

            //foreach (Reserve res in this.Claim.Reserves())
            //{
            //    msg += "Open Reserves" + Environment.NewLine;
            //    //if ((res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve) && (res.ReserveTypeId != (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage)
            //    //    && ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive)
            //    //    || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
            //    if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
            //    {
            //        msg += res.Description() + Environment.NewLine;
            //    }
            //}

            // Check for Uncleared Diary Entries
            List<Hashtable> UnclearedDiaries = Claim.UnclearedDiaryList;
            if (UnclearedDiaries.Count > 0)
            {
                msg = "Open Diaries" + Environment.NewLine;
                DataTable dt = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Uncleared_Diary_Entries_With_Images", Claim.Id);

                foreach (DataRow row in dt.Rows)
                {
                    msg += " Due Date: " + row["diary_due_date"].ToString() + " Sent To: " + row["sent_to"].ToString() + " Sent From: " + row["sent_from"].ToString() + " Type " + row["Diary_Type"].ToString() + Environment.NewLine;
                  
                }

            }

            if (Claim.OutstandingTasks().Count > 0)
            {
                msg += Environment.NewLine + "Unclosed Tasks" + Environment.NewLine;
                foreach (UserTask t in Claim.OutstandingTasks())
                {

                    msg += "Task: " + t.UserTaskType().Description + Environment.NewLine;

                }
            }

            //check for open appraisals
            List<Hashtable> OpenAppraisals = Claim.OpenAppraisals();
            if (OpenAppraisals.Count > 0)
            {
                msg += Environment.NewLine + "Open Appraisals" + Environment.NewLine; 

                DataTable dt = mm.GetAppraisalTasks(Claim.ClaimId);

                foreach (DataRow row in dt.Rows)
                {

                    if ((row["appraisal_status"].ToString() == "Appraisal Requested") || (row["appraisal_status"].ToString() == "Awaiting Response") || (row["appraisal_status"].ToString() == "Appraisal Resent"))
                        msg += "Date Requested: " + Convert.ToDateTime(row["request_date"]).ToShortDateString() + " Type: " + row["type_of_investigation_needed"].ToString() + " Status: " + row["appraisal_status"].ToString() + " Vehicle: " + row["vehicle"].ToString()  + " Claiamant: " + row["claimant"].ToString() + Environment.NewLine;

                }

               
            }


            lblMessage.Text = msg.Replace("\r\n", "<br>"); 
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Claim))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Close Claim.');</script>");
                return;
            }

            if (mf.CloseClaimWithWarnings(this.Claim, ref msg))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Claim Closed Successfully.');</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);

            }
            else
            {
 
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
            }

        }

        //private void CloseClaim()
        //{
        //    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Claim))
        //    {
        //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Close Claim.');</script>");
        //        return;
        //    }


        //    foreach (Reserve res in this.Claim.Reserves())
        //    {
        //        //if ((res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve) && (res.ReserveTypeId != (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage)
        //        //    && ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive)
        //        //    || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
        //        if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
        //        {
        //            //MouseNormal();
        //            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There are open reserves for this claim, please close them prior to closing claim.');</script>");

        //            return;

        //        }
        //    }


        //    string msg = "";
        //    if (mf.CloseClaimWithWarnings(this.Claim, ref msg))
        //    {
        //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Claim Closed Successfully.');</script>");
        //        ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='viewClaim.aspx?claim_id=" + this.Claim.ClaimId + "&Ptabindex=7';window.close();", true);
        //    }
        //    else
        //    {
                
        //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
        //    }
        //}

    }
}
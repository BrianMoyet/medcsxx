﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmLockingInfo : System.Web.UI.Page
    {
        #region Class Variables
        public bool hasLocks = false;
        public string referer = "";
        private modGeneratedEnums.RestrictionType lockType = modGeneratedEnums.RestrictionType.Undefined;
        private int id = 0, //claim, reserve, or transaction ID
            lockId = 0;         //lock ID of selection
        private MedCsxLogic.Claim myClaim = null;
        private Reserve myReserve = null;
        private Transaction myTransaction = null;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private List<LockGrid> myLockList;
        public string msg = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["referer2"] != null)
            {

                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();


           
                this.id = Convert.ToInt32(Request.QueryString["transaction_id"]);
                string locktypeid = Request.QueryString["lock_type_id"].ToString();
                this.lockType = (modGeneratedEnums.RestrictionType)Enum.Parse(typeof(modGeneratedEnums.RestrictionType), locktypeid, true);
               


                this.btnManLock.Visible = false;

                switch (lockType)
                {
                    case modGeneratedEnums.RestrictionType.Claim:
                        this.myClaim = new MedCsxLogic.Claim(id);
                        this.Page.Title = "Claim " + myClaim.DisplayClaimId + " - Claim Locks";
                        this.btnManLock.Visible = true;
                        break;
                    case modGeneratedEnums.RestrictionType.Reserve:
                        this.myReserve = new Reserve(id);
                        this.Page.Title = "Claim " + myReserve.Claim.DisplayClaimId + " - Reserve Locks For " + Reserve.ReserveDescription(ref id);
                        break;
                    case modGeneratedEnums.RestrictionType.Transaction:
                        this.myTransaction = new Transaction(id);
                        this.Page.Title = "Claim " + myTransaction.Claim.DisplayClaimId + " - Transaction Locks For " + myTransaction.Description();
                        break;
                    default:
                        //MessageBox.Show("Invalid lock type.", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                        return;
                }
                if (!Page.IsPostBack)
                    this.RefreshData();
            }
            
       

        private void RefreshData()
        {
            bool locksLoaded = false;
            switch (lockType)
            {
                case modGeneratedEnums.RestrictionType.Claim:
                    locksLoaded = LoadLocks(mm.GetLocks(1, id));
                    break;
                case modGeneratedEnums.RestrictionType.Reserve:
                    locksLoaded = LoadLocks(mm.GetLocks(2, id));
                    break;
                case modGeneratedEnums.RestrictionType.Transaction:
                    locksLoaded = LoadLocks(mm.GetLocks(3, id));
                    break;
                default:
                    break;
            }

            if (grSelections.SelectedIndex > 0)
                this.btnUnLock.Enabled = false;
        }

        private bool LoadLocks(List<Hashtable> list)
        {
            if (list.Count > 0)
            {
                grSelections.DataSource = LocksLoaded(list);
                grSelections.DataBind();
               
                grSelections.SelectedIndex = setSelection(id);
                grSelections_SelectedIndexChanged(null, null);
                return true;
            }
            else
            {
                lblLocking.Visible = true;
                return false;
            }
        }

        private int setSelection(int id)
        {
            int selectIdx = 0;
            if (id == 0)
                return selectIdx;

            foreach (LockGrid lg in myLockList)
            {
                if (lg.lockId == id)
                    return selectIdx;
                selectIdx++;
            }
            return 0;
        }

        private IEnumerable LocksLoaded(List<Hashtable> list)
        {
            myLockList = new List<LockGrid>();
            try
            {
                mm.UpdateLastActivityTime();

                foreach (Hashtable lData in list)
                {
                    LockGrid lg = new LockGrid();
                    lg.lockId = (int)lData["id"];
                    lg.isLocked = (string)lData["locked"];
                    lg.lockReason = (string)lData["lock_reason"];
                    lg.dtLocked = (DateTime)lData["lock_date"];
                    lg.userId = (string)lData["unlockedby"];
                    lg.dtUnlocked = (DateTime)lData["unlock_date"];
                    lg.imageIdx = (int)lData["Image_Index"];
                    myLockList.Add(lg);
                }
                return myLockList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myLockList;
            }
        }

        protected void btnUnLock_Click(object sender, EventArgs e)
        {
            //try
            //{
                mm.UpdateLastActivityTime();

                if (grSelections.Rows.Count > 0)
                {
                    if (grSelections.SelectedIndex < 0)
                        return;
                }


                //unlock the lock
                msg = "";
                switch (lockType)
                {
                    case modGeneratedEnums.RestrictionType.Claim:
                        mf.UnlockClaimLock(Convert.ToInt32(grSelections.DataKeys[grSelections.SelectedIndex].Value), txtFileNote.Text, ref msg);
                        if (msg.Length > 0)
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                        }
                        break;
                    case modGeneratedEnums.RestrictionType.Reserve:
                        mf.UnlockReserveLock(Convert.ToInt32(grSelections.DataKeys[grSelections.SelectedIndex].Value), txtFileNote.Text, ref msg);
                        if (msg.Length > 0)
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                        }
                        break;
                    case modGeneratedEnums.RestrictionType.Transaction:
                        UnlockTransactionLock(Convert.ToInt32(grSelections.DataKeys[grSelections.SelectedIndex].Value), txtFileNote.Text);
                        break;
                    default:
                        break;
                }

                this.RefreshData();
                Session["referer2"] = null;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
                this.RefreshData();
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        public void UnlockTransactionLock(int transactionLockId, string fn)
        {
            try
            {

                string s; //required text

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                    return;

                MedCsxLogic.TransactionLock tl = new TransactionLock(transactionLockId);

                if (!ml.CurrentUser.isManagement())
                {
                    btnUnLock.Enabled = false;
                    lblLockPermissions.Text = "You must be a supervisor or above to clear this lock.";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('ou must be a supervisor or above to clear this lock.');</script>");
                    return;
                }

                MedCsxLogic.Transaction t = tl.Transaction();


                //check for financial authority to unlock transaction
                if (tl.Reserve().ReserveType.UserAuthority() < tl.LockFileNote().AuthorityNeededToUnlock)
                {
                    btnUnLock.Enabled = false;
                    lblLockPermissions.Text = "You do not have sufficient financial authority to unlock this transaction.";

                    return;
                }


                //get required text
                s = t.TransactionType().Description + "Transaction For " + t.TransactionAmount.ToString("c");

                if (t.DraftId != 0)
                    s += " To " + t.Draft.Payee;

                if (t.SubroId != 0)
                    s += " From " + t.Subro().RecoverySource;

                if (t.SalvageId != 0)
                    s += " From " + t.Salvage().Vendor().Name;

                s += " Unlocked By " + ml.CurrentUser.Name;



               

                //prompt for file note text
                //string lookupValue = Microsoft.VisualBasic.Interaction.InputBox("Enter File Note Text", "File Note Text");
                //if (lookupValue == "")
                //    return;


                //TODO  get text from formLock
                s += Environment.NewLine + fn;

                //unlock claim
                tl.Unlock(s);

                Session["referer2"] = Request.Url.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnManLock_Click(object sender, EventArgs e)
         {
            try
            {
                mm.UpdateLastActivityTime();

                if (grSelections.Rows.Count> 0)
                { 
                    if (grSelections.SelectedIndex < 0)
                        return;
                }


                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Manually_Lock_Claim))
                    return;     //user not authorized to manually lock claims

                //create file note text
                string s = "";
                s = "Claim " + this.myClaim.DisplayClaimId;
                s += " Manually Locked By " + mm.GetUserName(Convert.ToInt32(Session["userID"]));

                //string lookupValue = Microsoft.VisualBasic.Interaction.InputBox("Enter File Note Text", "File Note Text");
                //if (lookupValue == "")
                //    return;

                //add the user's file note text
                s += Environment.NewLine + txtFileNote.Text;

                //manually lock claim
                Hashtable parms = new Hashtable();
                parms.Add("File_Note_Text", s);
                mm.FireClaimEvent(this.id, (int)modGeneratedEnums.EventType.Manual_Claim_Lock, parms);

                this.RefreshData();
                Session["referer2"] = null;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
                //this.RefreshData();     //update grid
                ////this.grSelections.SelectedItem = setSelection(this.lockId);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void grSelections_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                if (grSelections.SelectedIndex < 0)
                    return;

                this.lockId = Convert.ToInt32(grSelections.DataKeys[grSelections.SelectedIndex].Value);
                this.lblLocked.Text = grSelections.SelectedRow.Cells[1].Text;
                if (grSelections.SelectedRow.Cells[1].Text == "Yes")
                {
                    this.btnUnLock.Enabled = true;
                    //this.btnUnLock.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    this.btnUnLock.Enabled = false;
                    //this.btnUnLock.ForeColor = System.Drawing.Color.DarkGray;
                }

                this.lblLockReason.Text = grSelections.SelectedRow.Cells[2].Text;
                this.lbldtLock.Text = grSelections.SelectedRow.Cells[3].Text;

               


                Hashtable row;
                switch (lockType)
                {
                    case modGeneratedEnums.RestrictionType.Claim:
                        row = ml.getRow("Claim_Lock", this.lockId);
                        break;
                    case modGeneratedEnums.RestrictionType.Reserve:
                        row = ml.getRow("Reserve_Lock", this.lockId);
                        break;
                    case modGeneratedEnums.RestrictionType.Transaction:
                        row = ml.getRow("Transaction_Lock", this.lockId);
                        break;
                    default:
                        row = new Hashtable();
                        break;
                }

                string lbllockTxt = (string)((ml.getRow("File_Note", (int)row["Lock_File_Note_Id"]))["File_Note_Text"]);
                this.lblLockTxt.Text = lbllockTxt.Replace("\r\n", "<br>");
             
                this.lblLockReason.Text = (string)((ml.getRow("Event_Type", (int)(ml.getRow("File_Note", (int)row["Lock_File_Note_Id"]))["Event_Type_Id"]))["Description"]);
                if (grSelections.SelectedRow.Cells[1].Text == "No")
                {
                    this.lblUnLock.Text = grSelections.SelectedRow.Cells[4].Text;
                    this.lbldtUnLock.Text = grSelections.SelectedRow.Cells[5].Text;
                    string lblUnlockTxt = (string)((ml.getRow("File_Note", (int)row["Unlock_File_Note_Id"]))["File_Note_Text"]);
                    this.lblUnLockTxt.Text = lblUnlockTxt.Replace("\r\n", "<br>");
                }
                else
                {
                    this.lblUnLock.Text = "";
                    this.lbldtUnLock.Text = "";
                    this.lblUnLockTxt.Text = "";
                }

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["referer2"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
        }

        class LockGrid
        {
            public int lockId { get; set; }
            public int imageIdx { get; set; }
            public string isLocked { get; set; }
            public string lockReason { get; set; }
            public DateTime dtLocked { get; set; }
            public string dtLockedTxt { get { return dtLocked == ml.DEFAULT_DATE ? "" : dtLocked.ToString("MM/dd/yyyy hh:mm:ss tt"); } }
            public string userId { get; set; }
            public DateTime dtUnlocked { get; set; }
            public string dtUnlockedTxt { get { return dtUnlocked == ml.DEFAULT_DATE ? "" : dtUnlocked.ToString("MM/dd/yyyy hh:mm:ss tt"); } }
            public string lockNote { get; set; }
            public System.Drawing.Color rowColor { get; set; }
        }
    }
}
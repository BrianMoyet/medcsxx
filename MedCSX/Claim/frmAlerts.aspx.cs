﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmAlerts : System.Web.UI.Page
    {
        private static modLogic ml = new modLogic();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();

        public DateTime alertStartDate = ml.DEFAULT_DATE;
        public bool showDeferred = false;
        public bool isStartup = false;
        private bool hasUrgentAlerts = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!hasUrgentAlerts)
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            //}

            LoadAlerts();

            if (hasUrgentAlerts)
            {
                Resort();
            }
        }

        private void LoadAlerts()
        {
            //'Load the alerts to the grid

            //'The alerts list
            DataTable rows = new DataTable();

            //'Get Alerts after alertStartDate
            if (isStartup == false)
            {
                rows = mm.GetMyAlerts(Convert.ToInt32(Session["userID"]), alertStartDate, showDeferred);
            }
            else
            {
                rows = mm.GetMyStartupAlerts(alertStartDate, showDeferred);
            }

            if (rows.Rows.Count < 1)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }

            //'Load grid with Alerts and select the first alert
            LoadGrid(rows);
           // grAlerts.SelectedIndex = 0;

            ////'Set Urgent Alerts to Red
            //hasUrgentAlerts = false;
            //pnlUrgent.Visible = false;

            //For Each Row As DataGridViewRow In Me.grAlerts.Rows
            //    Dim a As New Alert(CType(Row.Tag, Integer))
            //    If a.IsUrgent Then
            //        'The alert is urgent, set its background color
            //        Row.DefaultCellStyle.BackColor = Color.Salmon
            //        Me.hasUrgentAlerts = True
            //        Me.TopMost = True
            //        Me.pnlUrgent.Visible = True
            //    End If
            //Next
        }

        protected void LoadGrid(DataTable rows)
        {
            grAlerts.DataSource = rows;
            grAlerts.DataBind();

            Session["AlertsPopupDataTable"] = rows;
        }

        protected void grAlerts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //'Set Urgent Alerts to Red
            //hasUrgentAlerts = false;
            //pnlUrgent.Visible = false;
           
            GridViewRow grv = e.Row;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (grv.Cells[1].Text == "Yes")
                {
<<<<<<< HEAD
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
                    e.Row.ForeColor = System.Drawing.Color.White;

=======
<<<<<<< HEAD
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
                    e.Row.ForeColor = System.Drawing.Color.White;

=======
                    e.Row.BackColor = System.Drawing.Color.Salmon;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    hasUrgentAlerts = true;
                    pnlUrgent.Visible = true;
                    e.Row.ForeColor = System.Drawing.Color.White;
                    HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");
                    arEdit.ForeColor = System.Drawing.Color.White;

                }

                if (grv.Cells[3].Text == "1/1/1900 12:00:00 AM")
                {
                    grv.Cells[3].Text = "";
                }

                if (e.Row.BackColor.ToString() == "")
                {
                    e.Row.ForeColor = System.Drawing.Color.White;
                }


            }

        }

        protected void grAlerts_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = Session["AlertsPopupDataTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                grAlerts.DataSource = dt;
                grAlerts.DataBind();

                Session["AlertsPopupDataTable"] = dt;
            }
        }

        private void Resort()
        {
            DataTable dt = Session["AlertsPopupDataTable"] as DataTable;
            dt.DefaultView.Sort = "Urgent" + " " + "DESC";
            grAlerts.DataSource = dt;
            grAlerts.DataBind();

            Session["AlertsPopupDataTable"] = dt;
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }
    }
}
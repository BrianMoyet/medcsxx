﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmTransactions.aspx.cs" Inherits="MedCSX.Claim.frmTransactions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <style>
     .hiddencol
      {
      display: none;
      }
            </style>
    <div  style="background-color: #CCCCCC">
    <div class="row">
          <div class="col-md-2">
                Claimant:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblClaimantName" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Coverage Type:
           </div>         
           <div class="col-md-2">
               <asp:Label ID="lblCoverageType" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Coverage Limit:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblCoverageLimit" runat="server" Text="Label"></asp:Label>
          </div>
      </div>
    
     <div class="row">
          <div class="col-md-2">
                Loss Paid:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblLossPaid" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Net Loss Reserve:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblLossReserve" runat="server" Text="Label"></asp:Label>
          </div>
      </div>
      <div class="row">
          <div class="col-md-2">
                Expense Paid:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblExpensePaid" runat="server" Text="Label"></asp:Label>
          </div>
           <div class="col-md-2">
                Net Net Expense Reserve:
          </div>
           <div class="col-md-2">
               <asp:Label ID="lblExpenseReserve" runat="server" Text="Label"></asp:Label>
          </div>
      </div>
<<<<<<< HEAD
</div>    ` 
=======
<<<<<<< HEAD
</div>    ` 
=======
</div>    
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
     <div class="row">
          <div class="col-md-12">
              <asp:LinkButton ID="hprShowLoss" OnClick="hprShowLoss_Click" runat="server"  ForeColor="Blue">Show Loss</asp:LinkButton>
               &nbsp;&nbsp;<asp:LinkButton ID="hprShowExpenses" OnClick="hprShowExpenses_Click" runat="server"  ForeColor="Blue">Show Expenses</asp:LinkButton>
               &nbsp;&nbsp;<asp:LinkButton ID="hprShowAll" OnClick="hprShowAll_Click" runat="server"  ForeColor="Blue">Show All</asp:LinkButton>
               &nbsp;&nbsp;<asp:LinkButton ID="hprTransactionReport" OnClick="hprTransactionReport_Click" runat="server"  ForeColor="Blue">Transaction Report</asp:LinkButton>
          </div>
     </div>
  
     <div class="row">
          <div class="col-md-9">
              <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
                    TargetControlID="btnResVoid"
             PopupControlID="Panel1"
             BackgroundCssClass="modalBackground"
             DropShadow="true"
             OkControlID="VoidTransaction"
             OnOkScript="ok()"
             CancelControlID="btnCancelDraft" 
                  ></ajaxToolkit:ModalPopupExtender>

                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server"
                    TargetControlID="btnReissueDraft"
             PopupControlID="Panel2"
             BackgroundCssClass="modalBackground"
             DropShadow="true"
             OkControlID="Reissue"
             OnOkScript="ok()"
             CancelControlID="btnCancelReissue" 
                  ></ajaxToolkit:ModalPopupExtender>
            
              <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none"  >
                  <iframe id="ifrmVoidReason" runat="server" src="" frameborder="0" width="100%" height="200px"></iframe> 
                 
                        
               <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnVoidReasonsOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnVoidReasonsOK_Click"/>
               <asp:Button ID="btnCancelDraft" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close" />
        </div>
    </div>
    </asp:Panel>

            <%--   <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Style="display: none"  >
                  <iframe id="ifrmVoidReason2" runat="server" src="" frameborder="0" width="100%" height="200px"></iframe> 
                 
                        
               <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnReissueVoidReasonOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnReissueVoidReasonOK_Click"/>
               <asp:Button ID="btnCancelReissue" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close" />
        </div>--%>
   <%-- </div>
    </asp:Panel>--%>
           
              
                        <asp:GridView ID="grTransactions" runat="server" AutoGenerateColumns="False" CellPadding="4"  DataKeyNames="transactionId"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" OnSelectedIndexChanged="grTransactions_SelectedIndexChanged" AutoGenerateSelectButton="true" PageSize="500" OnRowDataBound="grTransactions_RowDataBound">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                                 
                         <asp:BoundField DataField="dtTransaction" HeaderText="Date" />
                         <asp:BoundField DataField="transactionType" HeaderText="Transaction Type" />
                         <asp:BoundField DataField="transactionAmount" HeaderText="Amount" />
                         <asp:BoundField DataField="ExpenseLoss" HeaderText="Loss/Expense"  />
                         <asp:BoundField DataField="draftNo" HeaderText="Draft No" />
                         <asp:BoundField DataField="isVoided" HeaderText="Void" />
                         <asp:BoundField DataField="voidReason" HeaderText="Void Reason"  />
                         <asp:BoundField DataField="draftUnprinted"  ItemStyle-CssClass="hiddencol"  />
                          
                     
                    
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                   <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                    <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
          </div>
           <div class="col-md-3">
               <asp:LinkButton ID="btnTrnsDraft" runat="server"  OnClick="btnTrnsDraft_Click" ForeColor="Blue">Issue Draft</asp:LinkButton><br />
               <asp:LinkButton ID="btnPrintDraft" runat="server" OnClick="btnPrintDraft_Click" ForeColor="Blue">Print Draft</asp:LinkButton><br />
               <asp:LinkButton ID="btnViewDraft" runat="server" OnClick="btnViewDraft_Click"  ForeColor="Blue">View Draft</asp:LinkButton><br />
               <asp:LinkButton ID="btnResSalvage" runat="server" OnClick="btnResSalvage_Click"  ForeColor="Blue">Receive Salvage</asp:LinkButton><br />
               <asp:LinkButton ID="btnResSubro" runat="server" OnClick="btnResSubro_Click"  ForeColor="Blue">Receive Subro</asp:LinkButton><br />
               <asp:LinkButton ID="btnReissueDraft" runat="server" OnClick="btnReissueDraft_Click"   ForeColor="Blue">Reissue Draft</asp:LinkButton><br />
<<<<<<< HEAD
               <asp:LinkButton ID="btnResVoid" runat="server" OnClick="btnResVoid_Click"  ForeColor="Blue">Void</asp:LinkButton><br />
=======
<<<<<<< HEAD
               <asp:LinkButton ID="btnResVoid" runat="server" OnClick="btnResVoid_Click"  ForeColor="Blue">Void</asp:LinkButton><br />
=======
               <asp:LinkButton ID="btnResVoid" runat="server" OnClick="btnResVoid_Click"  ForeColor="Blue">Void Draft</asp:LinkButton><br />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
               <asp:LinkButton ID="btnUnVoid" runat="server" OnClick="btnUnVoid_Click"  ForeColor="Blue">Unvoid Draft</asp:LinkButton><br />
               <asp:LinkButton ID="btnResWireTransfer" runat="server"  OnClick="btnResWireTransfer_Click" ForeColor="Blue">Wire Transfer</asp:LinkButton><br />
               <asp:LinkButton ID="btnResWireOverpayment" runat="server" OnClick="btnResWireOverpayment_Click">Wire Transfer Overpayment</asp:LinkButton><br />
               <asp:LinkButton ID="btnResPendingDraft" runat="server" OnClick="btnResPendingDraft_Click" ForeColor="Blue">Issue Pending Draft</asp:LinkButton><br />


           </div>
     </div>
    <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
     <div class="row">
         <div class="col-md-12">
             <asp:Label ID="gbTransactionLock" runat="server" Text="" Font-Bold="True"  ForeColor="Blue"></asp:Label>
         &nbsp;&nbsp;
             <asp:Label ID="lbltransactionID" runat="server"></asp:Label>
         </div>
    </div>
    <div class="row">
         <div class="col-md-8">
             Lock Reason: <asp:Label ID="lblReserveLockReason" runat="server" Text=""></asp:Label><br />
             Lock Date: <asp:Label ID="lblReserveLockDate" runat="server" Text=""></asp:Label>
         </div>
        <div class="col-md-4">
            <asp:LinkButton ID="btnUnlockReserve" OnClick="btnUnlockReserve_Click" runat="server"  ForeColor="Blue" CausesValidation="false">Unlock Reserve</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnLockReserveInfo" runat="server" OnClick="btnLockReserveInfo_Click" CausesValidation="false">Lock Info</asp:LinkButton>
            <br /><asp:Label ID="lblLockPermissions" runat="server" Text="" ForeColor="Red"></asp:Label>
         </div>
    </div>
    <div id ="TransactionDetails" runat="server" visible="false">
        <div class="row">
            <div class="col-md-12">
                <b>Transaction Details</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Type:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionTypeTD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Loss/Expense:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionReserveTD" runat="server"></asp:Label>
            </div>
         </div>
        <div class="row">
            <div class="col-md-2">
                Date:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionDateTD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Amount:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionAmountTD" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                User:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionUserTD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Voided Draft #:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionDraftTD" runat="server"></asp:Label>
            </div>
         </div>
    </div>
    <div id="SubroDetails" runat="server" visible="false">
      <div class="row">
            <div class="col-md-12">
                <b>Subro Details</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Type:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionTypeSub" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Loss/Expense:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionReserveSub" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Date:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionDateSub" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Amount:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionAmountSub" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                User:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionUserSub" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Check No:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionDraftSub" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Void:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionVoidSub" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Void Reason:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionVoidReasonSub" runat="server"></asp:Label>
            </div>
         </div>
        <div class="row">
            <div class="col-md-2">
             Recovery:
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionRecoverySub" runat="server"></asp:Label>
            </div>
         </div>
    </div>
    <div id="DraftDetails" runat="server" visible="false">
         <div class="row">
            <div class="col-md-12">
                <b>Draft Details</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Type:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionTypeD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Loss/Expense:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionReserveD" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Date:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionDateD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Amount:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionAmountD" runat="server"></asp:Label>
            </div>
         </div>
        <div class="row">
            <div class="col-md-2">
                User:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionUserD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Check No:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionDraftD" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Payee:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionPayeeD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Reissue From Check No:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionDraftReissueD" runat="server"></asp:Label>
            </div>
         </div>
        <div class="row">
            <div class="col-md-2">
                &nbsp;
            </div>
            
         </div>
         <div class="row">
            <div class="col-md-2">
                &nbsp;
            </div>
            
         </div>
         <div class="row">
            <div class="col-md-2">
                Void:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionVoidD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Void Reason:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionVoidReasonD" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Vendor:
            </div>
              <div class="col-md-10">
                  <asp:Label ID="lblTransactionVendorD" runat="server"></asp:Label>
           
            </div>
         </div>
        <div class="row">
            <div class="col-md-2">
                Mail To:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionMailToD" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Explanation of Proceeds:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionExplanationD" runat="server"></asp:Label>
            </div>
         </div>
    </div>
    <div id="SalvageDetails" runat="server" visible="false">
         <div class="row">
            <div class="col-md-12">
                <b>Salvage Details</b>
            </div>
        </div>
         <div class="row">
            <div class="col-md-2">
                Type:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionTypeS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Loss/Expense:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionReserveS" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Date:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionDateS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Amount:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionAmountS" runat="server"></asp:Label>
            </div>
         </div>
        <div class="row">
            <div class="col-md-2">
                User:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionUserS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Check No:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionDraftS" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Void:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionVoidS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Void Reason:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionVoidReasonS" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Vendor:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionVendorS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Salvage Yard Charges:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionSalvageYardS" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Storage Days:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionStorageDaysS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
                Storage Charges:    
            </div>
              <div class="col-md-3">
                <asp:Label ID="lblTransactionStorageS" runat="server"></asp:Label>
            </div>
         </div>
         <div class="row">
            <div class="col-md-2">
                Towing Charges:
            </div>
              <div class="col-md-3">
                  <asp:Label ID="lblTransactionTowingS" runat="server"></asp:Label>
            </div>
            <div class="col-md-2">
               
            </div>
              <div class="col-md-3">
                
            </div>
         </div>
    </div>


     <div class="row">
          <div class="col-md-12">
               
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close" CausesValidation="false" OnClick="btnCancel_Click" />
              </div>
        </div>
</asp:Content>

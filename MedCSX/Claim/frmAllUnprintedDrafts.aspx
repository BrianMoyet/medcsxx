﻿<%@ Page Title="All Unprinted Drafts" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAllUnprintedDrafts.aspx.cs" Inherits="MedCSX.Claim.frmAllUnprintedDrafts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
             <asp:GridView ID="grDrafts" AutoGenerateSelectButton="false" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"   AllowPaging="False"   width="100%"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                     <Columns>
                    <asp:TemplateField HeaderText="Claim">
                            <ItemTemplate>
                            <asp:Hyperlink  ID="Hyperlink2" NavigateUrl=<%#"javascript:my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "=window.open('ViewClaim.aspx?Ptabindex=7&claim_id=" + DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "','my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" +  DataBinder.Eval(Container.DataItem,"display_claim_id").ToString() + ".focus()" %> Runat="Server">
                            <%# Eval("display_claim_id") %></asp:Hyperlink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Adjuster" HeaderText="Adjuster"/>
                        <asp:BoundField DataField="display_claim_id" HeaderText="Claim"/>
                         <asp:BoundField DataField="reserve type" HeaderText="Reserve"/>
                         <asp:BoundField DataField="draft no" HeaderText="Draft No"/>
                         <asp:BoundField DataField="amount" HeaderText="Amount"/>
                         <asp:BoundField DataField="Payee" HeaderText="Payee"/>
                         <asp:BoundField DataField="address1" HeaderText="Address"/>
                         <asp:BoundField DataField="city" HeaderText="City"/>
                         <asp:BoundField DataField="state" HeaderText="State"/>
                         <asp:BoundField DataField="zip" HeaderText="Zip"/>

                      
                     </Columns>
                    
                     <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
        </div>
    </div>

</asp:Content>

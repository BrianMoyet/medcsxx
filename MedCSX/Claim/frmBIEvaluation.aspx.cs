﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCSX.App_Code;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmBIEvaluation : System.Web.UI.Page
    {
        #region Class Variables
        private Functions func = new Functions();
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        public int injuredId = 0;
        private Injured m_injured;

        private BIEvalGrid changedListItem = null;        //grid item being changed
        private int changedListItemSubIndex = 0;            //sub item index for the list item that has been changed
        private string beforeChangeItemText = "",           //holds the text from the selected item before change has been made, used for replication
            afterChangeItemText = "";                       //holds the text from the selected item before change has been made, used for replication
        private bool cellChanging = false,                  //a cell is changing
            doneLoading = false,
            cellChangedNow = false,
            addARow = false;

        private List<BIEvalGrid> myGeneralList;
        private List<BIEvalGrid> myDamagesList;
        private List<BIEvalGrid> myDiagnosticsList;
        private List<BIEvalGrid> myTreatmentsList;
        private List<InjuryTable> myInjuryTypeList;
        private List<InjuryTable> myDoctorList;
        private List<InjuryTable> myDiagnosticCboList;
        private List<InjuryTable> myDiagnosisList;
        private List<InjuryTable> myChronicList;
        private ArrayList ListOfDoctors = new ArrayList();
        private BIEvalGrid genSelection;
        private BIEvalGrid dmgSelection;
        private BIEvalGrid diagSelection;
        private BIEvalGrid treatSelection;
        public string referer = "";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            injuredId = Convert.ToInt32(Request.QueryString["injured_id"]);
            this.m_injured = new Injured(this.injuredId);
            //grDiagnostics.DataContext = myDiagnosticCboList;
            this.MyBIEvaluation = m_injured.BIEvaluation();

            //if (this.MyBIEvaluation == null)
            //{
            //    AddBIEvaluation();
            //    //evaluation doesn't exist, must insert
            //}


            this.Page.Title = this.MyBIEvaluation.Claim().DisplayClaimId + " - BI Evaluation for " + this.MyBIEvaluation.Injured().Person().Name;

            //if (Session["referer2"] != null)
            //{

            //    referer = Session["referer2"].ToString();
            //    //Session["referer2"] = null;

            //}
            //else
            //    referer = Session["referer"].ToString();

            if (!Page.IsPostBack)
            {
                try
                {
                    if (this.MyBIEvaluation == null)
                        return;     //no BI Evaluation object

                    //load the injured details
                    this.txtClaimNo.Text = this.MyBIEvaluation.Injured().Claim.DisplayClaimId;
                    this.txtClaimantId.Text = this.MyBIEvaluation.Injured().Person().Name;
                    this.txtDtLoss.Text = this.MyBIEvaluation.Injured().Claim.DateOfLoss.ToShortDateString();

                    //load the liability values
                    this.txtAdjExpectValue.Text = func.FormatCurrency(this.MyBIEvaluation.AdjustedExpectedValue, 0);
                    this.txtAdjLossRange.Text = func.FormatCurrency(this.MyBIEvaluation.AdjustedLossLow, 0) + " - " + func.FormatCurrency(this.MyBIEvaluation.AdjustedLossHigh, 0);
                    this.txtUnadjExpectValue.Text = func.FormatCurrency(this.MyBIEvaluation.UnadjustedExpectedValue, 0);
                    this.txtUnadjLossRange.Text = func.FormatCurrency(this.MyBIEvaluation.UnadjustedLossLow, 0) + " - " + func.FormatCurrency(this.MyBIEvaluation.UnadjustedLossHigh, 0);

                    LoadCbo(this.cboInjuryType);
                    this.cboInjuryType.DataSource = myInjuryTypeList;
                    this.cboInjuryType.DataValueField = "injuryId";
                    this.cboInjuryType.DataTextField = "description";
                    this.cboInjuryType.DataBind();

                    this.cboInjuryType.SelectedIndex = SelectCboIndex(this.MyBIEvaluation.BIInjuryType().Description);

                    LoadDiagnosticGrid();
                    LoadTreatmentsGrid();
                    this.PreLoadGrids();
                   

                    this.txtNotes.Text = this.MyBIEvaluation.Notes;     //display the notes

                    //load diagnostics and treatments
                    //this.LoadDiagnostics();
                    //this.LoadTreatments();

                    //load damages
                    this.CalculateTotalDamages();

                    //if (myDoctorList != null)
                    //    this.cboOrderedBy.ItemsSource = myDoctorList;
                    //else
                    //    this.cboOrderedBy.Visibility = Visibility.Collapsed;
                    //if (myTreatingDrList != null)
                    //    this.cboTreatingDoctor.ItemsSource = myTreatingDrList;
                    //else
                    //    this.cboTreatingDoctor.Visibility = Visibility.Collapsed;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                    mf.ProcessError(ex);
                }
            }
        }

        //private void AddBIEvaluation()
        //{
        //    Hashtable htDiagnostics = new Hashtable();
        //    htDiagnostics.Add("Injured_Id", m_injured.Id);

        //    int insertid = ml.InsertRow(htDiagnostics, "Bi_Evaluation");

        //    Page_Load(null, null);
        //}

        private void LoadDiagnosticGrid()
        {
            DataTable dt = mm.GetBIDiagnostics(m_injured.BIEvaluation().Id);
            //myDamagesList = this.MyBIEvaluation.BIDiagnostics();
            //if (dt.Rows.Count > 0)
            //{
                grDiagnostics.DataSource = dt;
                grDiagnostics.DataBind();
            //}
            //else
            //{
            //    grDiagnostics.DataSource = Get_EmptyDataTable();
            //    grDiagnostics.DataBind();
            //}
        }

        public DataTable Get_EmptyDataTable()
        {
            DataTable dtEmpty = new DataTable();
            //Here ensure that you have added all the column available in your gridview
            dtEmpty.Columns.Add("Diagnostic_Type", typeof(string));
            dtEmpty.Columns.Add("diagnostic_date", typeof(string));
            dtEmpty.Columns.Add("best_Case_Cost", typeof(string));
            dtEmpty.Columns.Add("worst_Case_Cost", typeof(string));
            dtEmpty.Columns.Add("ordered_by", typeof(string));
            DataRow datatRow = dtEmpty.NewRow();

            //Inserting a new row,datatable .newrow creates a blank row
            dtEmpty.Rows.Add(datatRow);//adding row to the datatable
            return dtEmpty;
        }

        private void LoadTreatmentsGrid()
        {
            //myDamagesList = this.MyBIEvaluation.BIDiagnostics();
            grTreatments.DataSource = mm.GetBITreatments(m_injured.BIEvaluation().Id);
            grTreatments.DataBind();
        }

        private void CalculateTotalDamages()
        {
            double ttlBestCase = 0;
            double ttlWorstCase = 0;
            this.myDamagesList[4].bestCaseCost = this.TotalBestCaseTreatments() + this.TotalBestCaseDiagnostics();
            this.myDamagesList[4].worstCaseCost = this.TotalWorstCaseTreatments() + this.TotalWorstCaseDiagnostics();

            for (int i = 0; i <= 4; i++)
            {
                ttlBestCase += myDamagesList[i].bestCaseCost;
                ttlWorstCase += myDamagesList[i].worstCaseCost;
            }
            this.myDamagesList[5].bestCaseCost = ttlBestCase;
            this.myDamagesList[5].worstCaseCost = ttlWorstCase;
        }

       

        private double TotalBestCaseTreatments()
        {
            double ttlBestCase = 0;
            if (myTreatmentsList != null)
            { 
                foreach (BIEvalGrid t in myTreatmentsList)
                {
                    ttlBestCase += t.bestCaseCost;
                }
            }
            return ttlBestCase;
        }

        private double TotalWorstCaseTreatments()
        {
            double ttlWorstCase = 0;
            if (myTreatmentsList != null)
            { 
                foreach (BIEvalGrid t in myTreatmentsList)
                {
                    ttlWorstCase += t.worstCaseCost;
                }
            }
            return ttlWorstCase;
        }

        private void LoadTreatments()
        {
            try
            {
                //if (this.MyBIEvaluation == null)
                //    return;

                ////load the grid
                //foreach (BITreatment t in this.MyBIEvaluation.BITreatments())
                //{
                //    myDiagnosticsList.Add(new BIEvalGrid()
                //    {
                //        biEvalId = t.Id,
                //        orderedBy = t.TreatingDoctor().Name,
                //        dtStart = t.TreatmentStartDate,
                //        dtEnd = t.TreatmentEndDate,
                //        treatments = t.NbrTreatments,
                //        description = t.BIDiagnosisType().Description,
                //        isChronic = t.Chronic,
                //        bestCaseCost = t.CostBestCase,
                //        worstCaseCost = t.CostWorstCase,
                //        fees = t.ReportFees
                //    });

                //    //this.AddDoctortoComboBoxes(t.TreatingDoctor().Name);
                //}
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void LoadDiagnostics()
        {
            try
            {
            //    if (this.MyBIEvaluation == null)
            //        return;

            //    //load grid
            //    foreach (BIDiagnostic d in this.MyBIEvaluation.BIDiagnostics())
            //    {
            //        myDiagnosticsList.Add(new BIEvalGrid()
            //        {
            //            biEvalId = d.Id,
            //            description = d.BIDiagnosticType().Description,
            //            dtDiagnostic = d.DiagnosticDate,
            //            bestCaseCost = d.BestCaseCost,
            //            worstCaseCost = d.WorstCaseCost,
            //            orderedBy = d.OrderedBy().Name,
            //            BI_Diagnostic_Type_Id = d.BIDiagnosticType().Id
            //        });
            //        this.AddDoctortoComboBoxes(d.OrderedBy().Name);
            //    }


            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void AddDoctortoComboBoxes(string p)
        {
            if (!ListOfDoctors.Contains(p))
            {
                myDoctorList.Add(new InjuryTable()
                {
                    description = p
                });
                ListOfDoctors.Add(p);
            }
        }

        private double TotalBestCaseDiagnostics()
        {
            double ttlBestCase = 0;

            if (myDiagnosticsList != null)
            { 
                foreach (BIEvalGrid d in myDiagnosticsList)
                {
                    ttlBestCase += d.bestCaseCost;
                }
            }
            return ttlBestCase;
        }

        private double TotalWorstCaseDiagnostics()
        {
            double ttlWorstCase = 0;
            if (myDiagnosticsList != null)
            {
                foreach (BIEvalGrid d in myDiagnosticsList)
                {
                    ttlWorstCase += d.worstCaseCost;
                }
            }
            return ttlWorstCase;
        }

        private void PreLoadGrids()
        {
            //General
            myGeneralList = new List<BIEvalGrid>();
            string[] genDescriptions = { "Probability of Outcome - Damage Evaluation", "Probability of Outcome - Liability Evaluation",
                                           "Best Case Comparative Negligence Outcome", "Worst Case Comparative Negligence Outcome" };
            double d1, d2;
            for (int i = 0; i < genDescriptions.Length; i++)
            {
                switch (i)
                {
                    case 1:
                        txtProbablityLiability.Text = (this.MyBIEvaluation.ProbabilityLiability * 100).ToString();
                        d1 = this.MyBIEvaluation.ProbabilityLiability;// * 100;
                        break;
                    case 2:
                        txtProbablityNegligenceBest.Text = (this.MyBIEvaluation.ComparativeBestCase * 100).ToString();
                        d1 = this.MyBIEvaluation.ComparativeBestCase;// * 100;
                        break;
                    case 3:
                       txtProbablityNegligenceWorst.Text = (this.MyBIEvaluation.ComparativeWorstCase * 100).ToString();
                        d1 = this.MyBIEvaluation.ComparativeWorstCase;// * 100;
                        break;
                    default:    //case 0
                        txtProbablityDamage.Text = (this.MyBIEvaluation.ProbabilityDamage * 100).ToString();
                        d1 = this.MyBIEvaluation.ProbabilityDamage;// * 100;
                        break;
                }

                myGeneralList.Add(new BIEvalGrid()
                {
                    biEvalId = i,
                    description = genDescriptions[i],
                    percent = d1
                });
            }

            //Damages
            myDamagesList = new List<BIEvalGrid>();
            string[] damageDescriptions = { "General Damages", "Wage Loss", "Ambulance, Emergency Room and Initial Hospital Care Other than Diagnostic Costs",
                                              "Estimated Future Medical Amount", "Diagnostics and Treatments Total", "Grand Total" };
            for (int i = 0; i < damageDescriptions.Length; i++)
            {
                switch (i)
                {
                    case 1:
                        txtWageBest.Text = this.MyBIEvaluation.WageLossBestCase.ToString();
                        txtWageWorst.Text = this.MyBIEvaluation.WageLossWorstCase.ToString();
                        d1 = this.MyBIEvaluation.WageLossBestCase;
                        d2 = this.MyBIEvaluation.WageLossWorstCase;
                        break;
                    case 2:
                        txtAmbulanceBest.Text = this.MyBIEvaluation.AmbulanceBestCase.ToString();
                        txtAmbulanceWorst.Text = this.MyBIEvaluation.AmbulanceWorstCase.ToString();
                        d1 = this.MyBIEvaluation.AmbulanceBestCase;
                        d2 = this.MyBIEvaluation.AmbulanceWorstCase;
                        break;
                    case 3:
                        this.txtMedicalBest.Text = this.MyBIEvaluation.FutureMedicalBestCase.ToString();
                        this.txtMedicalWorst.Text = this.MyBIEvaluation.FutureMedicalWorstCase.ToString();
                        d1 = this.MyBIEvaluation.FutureMedicalBestCase;
                        d2 = this.MyBIEvaluation.FutureMedicalWorstCase;
                        break;
                    case 4:
                    case 5:
                        d1 = 0;
                        d2 = 0;

                        break;
                    default:
                        this.txtGeneralbest.Text = this.MyBIEvaluation.GeneralDamagesBestCase.ToString();
                        this.txtGeneralWorst.Text = this.MyBIEvaluation.GeneralDamagesWorstCase.ToString();
                        d1 = this.MyBIEvaluation.GeneralDamagesBestCase;
                        d2 = this.MyBIEvaluation.GeneralDamagesWorstCase;
                        break;
                }


               

                BIEvalGrid bg = new BIEvalGrid();
                bg.biEvalId = i;
                bg.description = damageDescriptions[i];
                bg.bestCaseCost = d1;
                bg.worstCaseCost = d2;
                if (i >= 4)
                    bg.embolden = true;
                myDamagesList.Add(bg);

            }

            double ttldiagtreatBest = 0.0;
            double ttldiagtreatWorst = 0.0;
            for (int g = 0; g < grTreatments.Rows.Count; g++)
            {

                GridViewRow row = (GridViewRow)grTreatments.Rows[g];
                try
                {
                    Label bestcasecost = (Label)row.FindControl("bestCaseCost");
                    ttldiagtreatBest += Convert.ToDouble(bestcasecost.Text.Trim());
                   // ttldiagtreatBest += Convert.ToDouble(grTreatments.Rows[g].Cells[7].Text.Trim());   //best
                }
                catch (Exception ex) { }
                try
                {
                    Label worstcasecost = (Label)row.FindControl("worstCaseCost");
                    ttldiagtreatWorst += Convert.ToDouble(worstcasecost.Text.Trim());   //worst
                }
                catch (Exception ex) { }
            }

            for (int g = 0; g < grDiagnostics.Rows.Count; g++)
            {

                GridViewRow row = (GridViewRow)grDiagnostics.Rows[g];
                try
                {
                    Label bestcasecost = (Label)row.FindControl("bestCaseCost");
                    ttldiagtreatBest += Convert.ToDouble(bestcasecost.Text.Trim());
                    // ttldiagtreatBest += Convert.ToDouble(grTreatments.Rows[g].Cells[7].Text.Trim());   //best
                }
                catch (Exception ex) { }
                try
                {
                    Label worstcasecost = (Label)row.FindControl("worstCaseCost");
                    ttldiagtreatWorst += Convert.ToDouble(worstcasecost.Text.Trim());   //worst
                }
                catch (Exception ex) { }
            }

            
            txtDiagTreatBest.Text = ttldiagtreatBest.ToString();
            txtDiagTreatWorst.Text = ttldiagtreatWorst.ToString();

            ttldiagtreatBest += Convert.ToDouble(txtGeneralbest.Text);
            ttldiagtreatWorst += Convert.ToDouble(txtGeneralWorst.Text);

            ttldiagtreatBest += Convert.ToDouble(txtWageBest.Text);
            ttldiagtreatWorst += Convert.ToDouble(txtWageWorst.Text);

            ttldiagtreatBest += Convert.ToDouble(txtAmbulanceBest.Text);
            ttldiagtreatWorst += Convert.ToDouble(txtAmbulanceWorst.Text);

            ttldiagtreatBest += Convert.ToDouble(txtMedicalBest.Text);
            ttldiagtreatWorst += Convert.ToDouble(txtMedicalWorst.Text);

            txtGrandTotalBest.Text = ttldiagtreatBest.ToString();
            txtGrandTotalWorst.Text = ttldiagtreatWorst.ToString();
            ////Diagnostics
            //myDiagnosticsList = new List<BIEvalGrid>();
            //myDiagnosticsList.Add(new BIEvalGrid()
            //{
            //    dtDiagnostic = ml.DEFAULT_DATE,
            //    bestCaseCost = 0,
            //    worstCaseCost = 0
            //});

            //myDoctorList = new List<InjuryTable>();
            //myDoctorList.Add(new InjuryTable()
            //{
            //    injuryId = -2,
            //    description = ""
            //});
            //myDoctorList.Add(new InjuryTable()
            //{
            //    injuryId = -1,
            //    description = "(Pick Doctor...)"
            //});

            ////Treatments
            //myTreatmentsList = new List<BIEvalGrid>();
            //myTreatmentsList.Add(new BIEvalGrid()
            //{
            //    dtStart = ml.DEFAULT_DATE,
            //    dtEnd = ml.DEFAULT_DATE,
            //    treatments = 0,
            //    isChronic = false,
            //    bestCaseCost = 0,
            //    worstCaseCost = 0,
            //    fees = 0
            //});

            //grGeneral.DataSource = myGeneralList;
            //grGeneral.DataBind();
            //grDamages.DataSource = myDamagesList;
            //grDamages.DataBind();



            //grTreatments.DataSource = myTreatmentsList;
            //grTreatments.DataBind();
        }

        private int SelectCboIndex(string p, int mode = 0)
        {
            switch (mode)
            {
                case 1:
                    for (int i = 0; i < myDoctorList.Count; i++)
                    {
                        if (myDoctorList[i].description == p)
                            return i;
                    }
                    break;
                default:    //case 0:
                    for (int i = 0; i < myInjuryTypeList.Count; i++)
                    {
                        if (myInjuryTypeList[i].description == p)
                            return i;
                    }
                    break;
            }
            return 0;
        }

        private void LoadCbo(DropDownList cb, int mode = 0)
        {
            List<InjuryTable> myDummyList = new List<InjuryTable>();
            if (mode < 3)
            {
                List<Hashtable> myData = TypeTable.getTypeTableRows("BI_Injury_Type");
                foreach (Hashtable iData in myData)
                {
                    myDummyList.Add(new InjuryTable()
                    {
                        injuryId = (int)iData["Id"],
                        description = (string)iData["Description"],
                        imageIdx = (int)iData["Image_Index"]
                    });
                }
            }
            else //chronic
            {
                myDummyList.Add(new InjuryTable()
                {
                    description = "No"
                });
                myDummyList.Add(new InjuryTable()
                {
                    description = "Yes"
                });
            }
            switch (mode)
            {
                case 1: //diagnostic
                    myDiagnosticCboList = myDummyList;
                    break;
                case 2: //diagnosis
                    myDiagnosisList = myDummyList;
                    break;
                case 3: //chronic
                    myChronicList = myDummyList;
                    break;
                default:
                    myInjuryTypeList = myDummyList;
                    break;
            }

        }

        protected void btnPrintVersion_Click(object sender, EventArgs e)
        {
            try
            {

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsBI();

                //Dictionary<string, string> rptParms = new Dictionary<string, string>();
                //rptParms.Add("textbox1", "");

                ShowReport("Bodily Injury Evaluation Report", rptParms);

               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }

        private Dictionary<string, string> GetReportParmsBI()
        {

            Dictionary<string, string> newparms = new Dictionary<string, string>();
             newparms.Add("bi_evaluation_id", this.MyBIEvaluation.Id.ToString());

            return newparms;
        }

        protected void btnCalcExplanation_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                this.SaveTreatmentsAndDiagnostics();    //save treatment and diagnostic entries

                Session["referer3"] = Request.Url.ToString();

                string url = "frmBICalculations.aspx?injured_id=" + this.injuredId;
                string s = "window.open('" + url + "', 'popup_windowBICalculations', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=750, copyhistory=no, left=400, top=100');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
               
            }
            catch (Exception ex)
            {
                //WebMsgBox.Show(ex.ToString());
                mf.ProcessError(ex);
            }
        }

       

        private void SaveTreatmentsAndDiagnostics()
        {
            if (this.ValidateDiagnostics())
                this.SaveDiagnostics();
            if (this.ValidateTreatments())
                this.SaveTreatments();
        }

        private void SaveTreatments()
        {
            try
            {
                mm.UpdateLastActivityTime();

                if ((this.MyBIEvaluation == null) || !doneLoading)
                    return;

                this.MyBIEvaluation.DeleteBITreatments();   //clear the treatments

                //create the treatments
                foreach (BIEvalGrid tr in myTreatmentsList)
                {
                    BITreatment t = new BITreatment();
                    t.BIEvaluationId = this.MyBIEvaluation.Id;

                    Vendor v = Vendor.Named(tr.orderedBy);
                    if (v == null)
                        t.TreatingDoctorId = 0;
                    else
                        t.TreatingDoctorId = v.Id;

                    t.TreatmentStartDate = tr.dtStart;
                    t.TreatmentEndDate = tr.dtEnd;
                    t.NbrTreatments = tr.treatments;
                    t.BIDiagnosisTypeId = (new BIDiagnosisType(tr.description)).Id;
                    t.Chronic = tr.isChronic;
                    t.CostBestCase = Convert.ToDecimal(tr.bestCaseCost);
                    t.CostWorstCase = Convert.ToDecimal(tr.worstCaseCost);
                    t.ReportFees = Convert.ToDecimal(tr.fees);

                    t.Update();
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void SaveDiagnostics()
        {
            try
            {
                mm.UpdateLastActivityTime();

                if ((this.MyBIEvaluation == null) || !doneLoading)
                    return;

                this.MyBIEvaluation.DeleteBIDiagnostics();  //clear the diagnostics

                //create the diagnostics
                foreach (BIEvalGrid d in myDiagnosticsList)
                {
                    BIDiagnostic b = new BIDiagnostic();
                    b.BIDiagnosticId = this.MyBIEvaluation.Id;
                    b.BIDiagnosticTypeId = d.biEvalId;
                    b.DiagnosticDate = d.dtDiagnostic;
                    b.BestCaseCost = d.bestCaseCost;
                    b.WorstCaseCost = d.worstCaseCost;
           
                    Vendor v = Vendor.Named(d.orderedBy);
                    if (v == null)
                        b.OrderedById = 0;
                    else
                        b.OrderedById = v.Id;

                    b.Update();
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void RefreshDiagnostics()
        {
            if (cellChangedNow)
            {
                
                grDiagnostics.DataSource = myDiagnosticsList;
                grDiagnostics.DataBind();
            }
            cellChangedNow = false;
        }

        private bool ValidateTreatments()
        {
            try
            {
                bool isValid = false;
                int numInvalid = 0;
                List<BIEvalGrid> deleteMe = new List<BIEvalGrid>();
                if (myTreatmentsList != null)
                { 
                    foreach (BIEvalGrid tr in myTreatmentsList)
                    {
                        bool thisValid = false;
                        if ((tr.orderedBy != "") && (tr.dtStart != ml.DEFAULT_DATE) && (tr.dtEnd != ml.DEFAULT_DATE) && (tr.treatments != 0) && (tr.diagnosis != "") && (tr.bestCaseCost != tr.worstCaseCost))
                            thisValid = true;
                        else
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There is an incomplete entry in the Treatments.');</script>");
                            //if (MessageBox.Show("There is an incomplete entry in the Treatments.  Do you want to remove this entry?  Enter Yes to remove.  Enter No to complete the Treatments entry.", "Incomplete Treatments", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                            //{
                            //    deleteMe.Add(tr);
                            //    thisValid = true;
                            //}
                            //else
                            //{
                            //    thisValid = false;
                            //    numInvalid++;
                            //}
                            thisValid = false;
                            return false;
                        }
                        isValid = thisValid;
                    }
                }

                foreach (BIEvalGrid del in deleteMe)
                {
                    myTreatmentsList.Remove(del);
                }
                return (isValid && (numInvalid == 0));
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return false;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {



            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
        }

        

       

        protected void grDiagnostics_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void grDiagnostics_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

        protected void grTreatments_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

       

        protected void sqldsDoctors_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
                e.Command.Parameters["@stateid"].Value = Convert.ToInt32(mm.GetStateId(this.MyBIEvaluation.State.Abbreviation));
        }

        protected void grDiagnostics_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grDiagnostics.EditIndex = -1;
            LoadDiagnosticGrid(); 
        }

        protected void grTreatments_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grTreatments.EditIndex = -1;
            LoadTreatmentsGrid();
        }

        protected void grDiagnostics_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grDiagnostics.EditIndex = e.NewEditIndex;
            LoadDiagnosticGrid();

        }

        protected void grDiagnostics_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            int id = Convert.ToInt32(grDiagnostics.DataKeys[e.RowIndex].Value);
            GridViewRow row = (GridViewRow)grDiagnostics.Rows[e.RowIndex];

            DropDownList DiagnosticEditDropDownList = (DropDownList)row.FindControl("DiagnosticEditDropDownList");
            TextBox dtDiagnosticEdit = (TextBox)row.FindControl("dtDiagnosticEdit");
            TextBox bestCaseCostEdit = (TextBox)row.FindControl("bestCaseCostEdit");
            TextBox worstCaseCostEdit = (TextBox)row.FindControl("worstCaseCostEdit");
            DropDownList OrderedDREditDropDownList = (DropDownList)row.FindControl("OrderedDREditDropDownList");

            Hashtable htDiagnostics = new Hashtable();
            htDiagnostics.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htDiagnostics.Add("BI_Diagnostic_Type_Id", Convert.ToInt32(DiagnosticEditDropDownList.SelectedValue));
            htDiagnostics.Add("diagnostic_date", dtDiagnosticEdit.Text);
            htDiagnostics.Add("best_Case_Cost", bestCaseCostEdit.Text);
            htDiagnostics.Add("worst_Case_Cost", worstCaseCostEdit.Text);
            htDiagnostics.Add("Ordered_By_Id", OrderedDREditDropDownList.SelectedValue.ToString());
            htDiagnostics.Add("BI_Diagnostic_Id", id);
            ml.updateRow("BI_Diagnostic", htDiagnostics);

            //save the update here
            grDiagnostics.EditIndex = -1;
            //Bind the grid
            LoadDiagnosticGrid();

            PreLoadGrids();
        }

        protected void grTreatments_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            int id = Convert.ToInt32(grTreatments.DataKeys[e.RowIndex].Value);
            GridViewRow row = (GridViewRow)grTreatments.Rows[e.RowIndex];

           
            TextBox dtStartEdit = (TextBox)row.FindControl("dtStartEdit");
            TextBox dtEndEdit = (TextBox)row.FindControl("dtEndEdit");
            TextBox nbrTreatmentsEdit = (TextBox)row.FindControl("nbrTreatmentsEdit");
            DropDownList ChronicEdit = (DropDownList)row.FindControl("ChronicEdit");
            TextBox bestCaseCostEdit = (TextBox)row.FindControl("bestCaseCostEdit");
            TextBox worstCaseCostEdit = (TextBox)row.FindControl("worstCaseCostEdit");
            TextBox reportFeesEdit = (TextBox)row.FindControl("reportFeesEdit");
            DropDownList diagnosisEdit = (DropDownList)row.FindControl("diagnosisEdit");
            DropDownList TreatmentsDREditDropDownList = (DropDownList)row.FindControl("TreatmentsDREditDropDownList");


            Hashtable htTreatments = new Hashtable();
            htTreatments.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htTreatments.Add("Treatment_Start_Date", dtStartEdit.Text);
            htTreatments.Add("Treatment_End_Date", dtEndEdit.Text);
            htTreatments.Add("Nbr_Treatments", nbrTreatmentsEdit.Text);
            htTreatments.Add("Chronic", ChronicEdit.SelectedValue.ToString());
            htTreatments.Add("Cost_Best_Case", bestCaseCostEdit.Text);
            htTreatments.Add("Cost_Worst_Case", worstCaseCostEdit.Text);
            htTreatments.Add("Report_Fees", reportFeesEdit.Text);
            htTreatments.Add("Bi_Diagnosis_Type_Id", diagnosisEdit.SelectedValue.ToString());
            htTreatments.Add("Treating_Doctor_Id", TreatmentsDREditDropDownList.SelectedValue.ToString());
            htTreatments.Add("BI_Treatment_Id", id);
            ml.updateRow("BI_Treatment", htTreatments);

            //save the update here
            grTreatments.EditIndex = -1;
            //Bind the grid
            LoadTreatmentsGrid();
            PreLoadGrids();
        }

        protected void AddTreatmentButton_Click(object sender, EventArgs e)
        {
           


            TextBox dtStartAdd = (TextBox)grTreatments.FooterRow.FindControl("dtStartAdd");
            TextBox dtEndAdd = (TextBox)grTreatments.FooterRow.FindControl("dtEndAdd");
            TextBox nbrTreatmentsAdd = (TextBox)grTreatments.FooterRow.FindControl("nbrTreatmentsAdd");
            DropDownList ChronicAdd = (DropDownList)grTreatments.FooterRow.FindControl("ChronicAdd");
            TextBox bestCaseCostAdd = (TextBox)grTreatments.FooterRow.FindControl("bestCaseCostAdd");
            TextBox worstCaseCostAdd = (TextBox)grTreatments.FooterRow.FindControl("worstCaseCostAdd");
            TextBox reportFeesAdd = (TextBox)grTreatments.FooterRow.FindControl("reportFeesAdd");
            DropDownList diagnosisAdd = (DropDownList)grTreatments.FooterRow.FindControl("diagnosisAdd");
            DropDownList TreatmentsDRAddDropDownList = (DropDownList)grTreatments.FooterRow.FindControl("TreatmentsDRAddDropDownList");


            Hashtable htTreatments = new Hashtable();
            htTreatments.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htTreatments.Add("Treatment_Start_Date", dtStartAdd.Text);
            htTreatments.Add("Treatment_End_Date", dtEndAdd.Text);
            htTreatments.Add("Nbr_Treatments", nbrTreatmentsAdd.Text);
            htTreatments.Add("Chronic", ChronicAdd.SelectedValue.ToString());
            htTreatments.Add("Cost_Best_Case", bestCaseCostAdd.Text);
            htTreatments.Add("Cost_Worst_Case", worstCaseCostAdd.Text);
            htTreatments.Add("Report_Fees", reportFeesAdd.Text);
            htTreatments.Add("Bi_Diagnosis_Type_Id", diagnosisAdd.SelectedValue.ToString());
            htTreatments.Add("Treating_Doctor_Id", TreatmentsDRAddDropDownList.SelectedValue.ToString());
          
            int insertid = ml.InsertRow(htTreatments, "Bi_Treatment");

            LoadTreatmentsGrid();
            PreLoadGrids();
        }

        protected void grDiagnostics_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != grDiagnostics.EditIndex)
            //{
            //    (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            //}

            //DataTable dt = mm.GetDiagnosticTypes();

            ////Label hdnval = ((Label)grDiagnostics.Rows[Convert.ToInt32(e.Row.RowIndex)].FindControl("diagType"));
            ////string hdnval = mycheck.Text;
            ////HiddenField hdnval = (HiddenField)e.Row.FindControl("hdnBI_Diagnostic_Type_Id");
            ////DropDownList ddlCountries = GridView1.FooterRow.FindControl("ddlCountries") as DropDownList;
            //Label hdnval = (Label)e.Row.FindControl("diagType");
            //DropDownList DiagnosticEditDropDownList = (DropDownList)e.Row.FindControl("DiagnosticEditDropDownList");
            ////DropDownList DiagnosticEditDropDownList = grDiagnostics.FindControl("DiagnosticAddDropDownList") as DropDownList;
            ////DropDownList DiagnosticEditDropDownList = (e.Row.FindControl("DiagnosticEditDropDownList") as DropDownList);
            ////DropDownList DiagnosticEditDropDownList = (DropDownList)e.Row.FindControl("DiagnosticEditDropDownList");

            ////DiagnosticEditDropDownList.DataSource = dt;
            ////DiagnosticEditDropDownList.DataTextField = "Description";
            ////DiagnosticEditDropDownList.DataValueField = "BI_Diagnostic_Type_Id";

           
            //DiagnosticEditDropDownList.Items.FindByValue(hdnval.Text).Selected = true;


            ////if (e.Row.RowType == DataControlRowType.DataRow)
            ////{
            ////    if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            ////    {
            ////        DropDownList ddList = (DropDownList)e.Row.FindControl("DiagnosticEditDropDownList");
            ////        //bind dropdown-list
            ////        DataTable dt = mm.GetDiagnosticTypes();
            ////        ddList.DataSource = dt;
            ////        ddList.DataTextField = "Description";
            ////        ddList.DataValueField = "BI_Diagnostic_Type_Id";
            ////        ddList.DataBind();

            ////        DataRowView dr = e.Row.DataItem as DataRowView;
            ////        //ddList.SelectedItem.Text = dr["category_name"].ToString();
            ////        //ddList.SelectedValue = dr["category_name"].ToString();
            ////    }
            ////}
        }

        protected void grDiagnostics_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(grDiagnostics.DataKeys[e.RowIndex].Value);
            ml.deleteRow("BI_Diagnostic", id);
            LoadDiagnosticGrid();
            PreLoadGrids();

        }

        protected void grTreatments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(grTreatments.DataKeys[e.RowIndex].Value);
            ml.deleteRow("BI_Treatment", id);
            LoadTreatmentsGrid();
            PreLoadGrids();

        }

        protected void AddDiagnosticButton_Click(object sender, EventArgs e)
        {

            //if (e.CommandName == "InserDiag")
            //{
                DropDownList DiagnosticAddDropDownList = (DropDownList)grDiagnostics.FooterRow.FindControl("DiagnosticAddDropDownList");
                TextBox dtDiagnosticAdd = (TextBox)grDiagnostics.FooterRow.FindControl("dtDiagnosticAdd");
                TextBox bestCaseCostAdd = (TextBox)grDiagnostics.FooterRow.FindControl("bestCaseCostAdd");
                TextBox worstCaseCostAdd = (TextBox)grDiagnostics.FooterRow.FindControl("worstCaseCostAdd");
                DropDownList OrderedDRAddDropDownList = (DropDownList)grDiagnostics.FooterRow.FindControl("OrderedDRAddDropDownList");

                Hashtable htDiagnostics = new Hashtable();
                htDiagnostics.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
                htDiagnostics.Add("BI_Diagnostic_Type_Id", Convert.ToInt32(DiagnosticAddDropDownList.SelectedValue));
                htDiagnostics.Add("diagnostic_date", dtDiagnosticAdd.Text);
                htDiagnostics.Add("best_Case_Cost", bestCaseCostAdd.Text);
                htDiagnostics.Add("worst_Case_Cost", worstCaseCostAdd.Text);
                htDiagnostics.Add("Ordered_By_Id", OrderedDRAddDropDownList.SelectedValue.ToString());
                int insertid = ml.InsertRow(htDiagnostics, "Bi_Diagnostic");

                LoadDiagnosticGrid();

                PreLoadGrids();
        }

        protected void grDiagnostics_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "InserDiag")
            {
                DropDownList DiagnosticAddDropDownList = (DropDownList)grDiagnostics.FindControl("DiagnosticAddDropDownList");
                TextBox dtDiagnosticAdd = (TextBox)grDiagnostics.FindControl("dtDiagnosticAdd");
                TextBox bestCaseCostAdd = (TextBox)grDiagnostics.FindControl("bestCaseCostAdd");
                TextBox worstCaseCostAdd = (TextBox)grDiagnostics.FindControl("worstCaseCostAdd");
                DropDownList OrderedDRAddDropDownList = (DropDownList)grDiagnostics.FindControl("OrderedDRAddDropDownList");

                Hashtable htDiagnostics = new Hashtable();
                htDiagnostics.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
                htDiagnostics.Add("BI_Diagnostic_Type_Id", Convert.ToInt32(DiagnosticAddDropDownList.SelectedValue));
                htDiagnostics.Add("diagnostic_date", dtDiagnosticAdd.Text);
                htDiagnostics.Add("best_Case_Cost", bestCaseCostAdd.Text);
                htDiagnostics.Add("worst_Case_Cost", worstCaseCostAdd.Text);
                htDiagnostics.Add("Ordered_By_Id", OrderedDRAddDropDownList.SelectedValue.ToString());
                int insertid = ml.InsertRow(htDiagnostics, "Bi_Diagnostic");

                LoadDiagnosticGrid();

                PreLoadGrids();
            }
        }

        protected void AddEmptyRow_Click(object sender, EventArgs e)
        {
            Hashtable htDiagnostics = new Hashtable();
            htDiagnostics.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htDiagnostics.Add("BI_Diagnostic_Type_Id", 0);
            htDiagnostics.Add("diagnostic_date", DateTime.Now);
            htDiagnostics.Add("best_Case_Cost", "0");
            htDiagnostics.Add("worst_Case_Cost", "0");
            htDiagnostics.Add("Ordered_By_Id", 0);
            int insertid = ml.InsertRow(htDiagnostics, "Bi_Diagnostic");

           
            int editindex = 0;
            for (int i = 0; i <= grDiagnostics.Rows.Count - 1; i++)
            {
                if (Convert.ToInt32(grDiagnostics.DataKeys[i].Value) == insertid)
                {
                    editindex = i;
                    
                }
            }
                grDiagnostics.EditIndex = editindex;
            LoadDiagnosticGrid();

        }

        protected void AddEmptyRowTreatment_Click(object sender, EventArgs e)
        {
            Hashtable htTreatments = new Hashtable();
            htTreatments.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htTreatments.Add("Treatment_Start_Date", Convert.ToDateTime("1900-01-01"));
            htTreatments.Add("Treatment_End_Date", Convert.ToDateTime("1900-01-01"));
            htTreatments.Add("Nbr_Treatments", 0);
            htTreatments.Add("Chronic", 0);
            htTreatments.Add("Cost_Best_Case", 0);
            htTreatments.Add("Cost_Worst_Case", 0);
            htTreatments.Add("Report_Fees", 0);
            htTreatments.Add("Bi_Diagnosis_Type_Id", 0);
            htTreatments.Add("Treating_Doctor_Id", 0);
         
            int insertid = ml.InsertRow(htTreatments, "Bi_Treatment");

            int editindex = 0;
            for (int i = 0; i <= grTreatments.Rows.Count - 1; i++)
            {
                if (Convert.ToInt32(grTreatments.DataKeys[i].Value) == insertid)
                {
                    editindex = i;

                }
            }
            grTreatments.EditIndex = editindex;

            LoadTreatmentsGrid();

           
        }

        protected void grTreatments_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grTreatments.EditIndex = e.NewEditIndex;
            LoadTreatmentsGrid();


            //DropDownList ddlFrom = (DropDownList)grdUsedCatheters.Rows[e.NewEditIndex].FindControl("ddFrom");
        }

        protected void txtProbablityDamage_TextChanged(object sender, EventArgs e)
        {

            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("probability_damage", (Convert.ToDecimal(txtProbablityDamage.Text) / 100));
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtProbablityLiability.Focus();
        }

        protected void txtProbablityLiability_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Probability_Liability", (Convert.ToDecimal(txtProbablityLiability.Text) / 100));
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtProbablityNegligenceBest.Focus();
        }

        protected void txtProbablityNegligenceBest_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Comparative_Best_Case", (Convert.ToDecimal(txtProbablityNegligenceBest.Text) / 100));
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtProbablityNegligenceWorst.Focus();
        }

        protected void txtProbablityNegligenceWorst_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Comparative_Worst_Case", (Convert.ToDecimal(txtProbablityNegligenceWorst.Text) / 100));
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtGeneralbest.Focus();
        }

        protected void cboInjuryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Bi_Injury_Type_Id", Convert.ToInt32(cboInjuryType.SelectedValue));
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtGeneralbest.Focus();
        }

        protected void txtGeneralbest_TextChanged(object sender, EventArgs e)
        {
           
                //if (Convert.ToDouble(txtGeneralWorst.Text) < Convert.ToDouble(txtGeneralbest.Text))
                //{
                //    ((TextBox)sender).Focus();
                //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('General Damages Worst Case can't be negative and must be more than Best Case.');</script>");
                    
                //    return;
                //}
            

            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("General_Damages_Best_Case", txtGeneralbest.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtGeneralWorst.Focus();
        }

        protected void txtGeneralWorst_TextChanged(object sender, EventArgs e)
                    {
            
                //if (Convert.ToDouble(txtGeneralWorst.Text) < Convert.ToDouble(txtGeneralbest.Text))
                //{
                //    ((TextBox)sender).Focus();
                //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('General Damages Worst Case can't be negative and must be more than Best Case.');</script>");
                  
                //    return;
                //}
            
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("General_Damages_Worst_Case", txtGeneralWorst.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtWageBest.Focus();
        }

        protected void txtWageBest_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Wage_Loss_Best_Case", txtWageBest.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtWageWorst.Focus();
        }

        protected void txtWageWorst_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Wage_Loss_Worst_Case", txtWageWorst.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtAmbulanceBest.Focus();
        }

        protected void txtAmbulanceBest_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Ambulance_Best_Case", txtAmbulanceBest.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtAmbulanceBest.Focus();
        }

        protected void txtAmbulanceWorst_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Ambulance_Worst_Case", txtAmbulanceWorst.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtMedicalBest.Focus();
        }

        protected void txtMedicalBest_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Future_Medical_Best_Case", txtMedicalBest.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtMedicalWorst.Focus();
        }

        protected void txtMedicalWorst_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Future_Medical_Worst_Case", txtMedicalWorst.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
            txtDiagTreatBest.Focus();
        }

        protected void txtDiagTreatBest_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtNotes_TextChanged(object sender, EventArgs e)
        {
            Hashtable htBIEvaluation = new Hashtable();
            htBIEvaluation.Add("BI_Evaluation_Id", m_injured.BIEvaluation().Id);
            htBIEvaluation.Add("Notes", txtNotes.Text);
            ml.updateRow("BI_Evaluation", htBIEvaluation);
           
        }

        private bool ValidateDiagnostics()
        {
            try
            {
                bool isValid = false;
                int numInvalid = 0;
                List<BIEvalGrid> deleteMe = new List<BIEvalGrid>();
                if (myDiagnosticsList != null)
                { 
                    foreach (BIEvalGrid d in myDiagnosticsList)
                    {
                        bool thisValid = false;
                        if ((d.orderedBy != "") && (d.dtDiagnostic != ml.DEFAULT_DATE) && (d.diagType != "") && (d.bestCaseCost != d.worstCaseCost))
                            thisValid = true;
                        else
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There is an incomplete entry in the Diagnostics.');</script>");
                            thisValid = false;
                            return false;
                            //if (MessageBox.Show("There is an incomplete entry in the Diagnostics.  Do you want to remove this entry?  Enter Yes to remove.  Enter No to complete the Diagnostics entry.", "Incomplete Diagnostics", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                            //{
                            //    deleteMe.Add(d);
                            //    thisValid = true;
                            //}
                            //else
                            //{
                            //    thisValid = false;
                            //    numInvalid++;
                            //}
                        }
                        isValid = thisValid;
                    }
                }

                foreach (BIEvalGrid del in deleteMe)
                {
                    myDiagnosticsList.Remove(del);
                }
                return (isValid && (numInvalid == 0));
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return false;
            }
        }

        class BIEvalGrid
        {
            public int biEvalId { get; set; }

            //General
            public string description { get; set; }
            public double percent { get; set; }

            //Damages
            public double bestCaseCost { get; set; }
            public double worstCaseCost { get; set; }

            //Diagnostics
            public string diagType { get; set; }
            public DateTime dtDiagnostic { get; set; }
            public string orderedBy { get; set; }

            //Treatments
            public DateTime dtStart { get; set; }
            public DateTime dtEnd { get; set; }
            public int treatments { get; set; }
            public string diagnosis { get; set; }
            public string txtChronic
            {
                get { return isChronic ? "Yes" : "No"; }
                set { dmyString = value; }
            }
            public bool isChronic { get; set; }
            public double fees { get; set; }

            public bool embolden { get; set; }
            private string dmyString;

            public int BI_Diagnostic_Type_Id { get; set; }
        }

        class InjuryTable
        {
            public int injuryId { get; set; }
            public string description { get; set; }
            public int imageIdx { get; set; }
        }

        private BIEvaluation MyBIEvaluation { get; set; }

    }
}
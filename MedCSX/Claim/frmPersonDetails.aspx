﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPersonDetails.aspx.cs" Inherits="MedCSX.Claim.frmPersonDetails" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:RadioButtonList ID="rbPersonDetailPerson" runat="server"  RepeatDirection="Horizontal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbPersonDetailPerson_SelectedIndexChanged">
                    <asp:ListItem Value="0" Text="Person" Selected="True" />
                    <asp:ListItem Value="1" Text="Company" />
            </asp:RadioButtonList>
       
        </div>
</div>
<div runat="server" id="Person">
    <div class="row">
            <div class="col-md-12">
                <b>Person</b>
              
            </div>
    </div>
     <div class="row">
            <div class="col-md-6">
                Prefix: <asp:TextBox ID="tbPersonPrefix" runat="server" TabIndex="1"></asp:TextBox>
            </div>
          <div class="col-md-6">
                Suffix: <asp:TextBox ID="tbPersonSuffix" runat="server" TabIndex="2"></asp:TextBox>
            </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                First: <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="**" ControlToValidate="tbPersonFName" ForeColor="Red">**</asp:RequiredFieldValidator><asp:TextBox ID="tbPersonFName" runat="server" TabIndex="3"></asp:TextBox> Mid: <asp:TextBox ID="tbPersonMI" runat="server" Width="15" TabIndex="4"></asp:TextBox> Last: <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="**" ControlToValidate="tbPersonLName" ForeColor="Red">**</asp:RequiredFieldValidator><asp:TextBox ID="tbPersonLName" runat="server" TabIndex="5"></asp:TextBox>
                
            </div>
        </div>
    </div>
    <div runat="server" id="Company" visible="false">
        <div class="row">
                <div class="col-md-12">
                    <b>Company:</b>  
                   
                </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    Company Name: <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="**" ControlToValidate="tbCompanyName" ForeColor="Red">**</asp:RequiredFieldValidator>   <asp:TextBox ID="tbCompanyName" runat="server"></asp:TextBox>
                   
                </div>
        </div>
    </div>
   
        <div class="row">
             <div class="col-md-10">
                    <b>Address</b>    
       
                </div>
            <div class="col-md-2">
                <asp:Button ID="btnPickAddress" runat="server"  Visible="true" class="btn btn-info btn-xs" OnClick="btnPickAddress_Click" CausesValidation="false"   Text="Pick Address" />
            </div>
        </div>
    <div runat="server" id="divPickAddress" visible="false">
          <asp:GridView ID="grSelections" runat="server" AutoGenerateColumns="False" CellPadding="4"  DataKeyNames="id" OnSelectedIndexChanged="grSelections_SelectedIndexChanged"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"   width="100%" AutoGenerateSelectButton="true">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                              
                         <asp:BoundField DataField="address1" HeaderText="Address 1" />
                         <asp:BoundField DataField="address2" HeaderText="Address 2" />
                        <asp:BoundField DataField="city" HeaderText="City" />
                        <asp:BoundField DataField="state" HeaderText="State" />
                        <asp:BoundField DataField="zipcode" HeaderText="Zip" />
                         <asp:BoundField DataField="home_phone" HeaderText="Home Phone" />
                          
                     
                    
                    </Columns>
<<<<<<< HEAD
                     <EditRowStyle BackColor="#999999" />
=======
                    <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
    </div>
        <div class="row">
             <div class="col-md-1">
                    Address 1:         
                </div>
            <div class="col-md-11">
                   <asp:TextBox ID="tbAddress1" runat="server" TabIndex="6"></asp:TextBox>           
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                    Address 2:         
                </div>
            <div class="col-md-11">
                   <asp:TextBox ID="tbAddress2" runat="server" TabIndex="7"></asp:TextBox>           
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                   City:         
                </div>
            <div class="col-md-11">
                   <asp:TextBox ID="tbAddressCity" runat="server" TabIndex="8"></asp:TextBox> State: <asp:DropDownList ID="cboAddressState" runat="server" TabIndex="9"></asp:DropDownList> Zip: <asp:TextBox ID="tbAddressZip" runat="server" Width="60px" AutoPostBack="True" OnTextChanged="tbAddressZip_TextChanged" TabIndex="10"></asp:TextBox>          
                </div>
        </div>
      <div class="row">
             <div class="col-md-12">
                    <b>Phone</b>    
             
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                Home Phone:
             </div>
              <div class="col-md-5">
                      <asp:TextBox ID="tbPhoneHome" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="tbPhoneHome_TextChanged" TabIndex="11"></asp:TextBox>            
             </div>
             <div class="col-md-1">
                Work Phone:       
             </div>
              <div class="col-md-5">
                <asp:TextBox ID="tbPhoneWork" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="tbPhoneWork_TextChanged" TabIndex="12"></asp:TextBox>                    
             </div>
       </div>
     <div class="row">
             <div class="col-md-1">
                Cell Phone:
             </div>
              <div class="col-md-5">
                      <asp:TextBox ID="tbPhoneCell" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="tbPhoneCell_TextChanged" TabIndex="13"></asp:TextBox>            
             </div>
             <div class="col-md-1">
               Fax:       
             </div>
              <div class="col-md-5">
                <asp:TextBox ID="tbPhoneFax" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="tbPhoneFax_TextChanged" TabIndex="14"></asp:TextBox>                    
             </div>
       </div>
     <div class="row">
             <div class="col-md-1">
                Other Contact:
             </div>
              <div class="col-md-5">
                      <asp:TextBox ID="tbPhoneOther" runat="server" TextMode="Phone" AutoPostBack="True" OnTextChanged="tbPhoneOther_TextChanged" TabIndex="15"></asp:TextBox>            
             </div>
            
       </div>
     <div class="row">
             <div class="col-md-12">
                    <b>Other</b>    
          
                </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Email:
             </div>
         <div class="col-md-5">
             <asp:TextBox ID="tbOtherEmail" runat="server" TextMode="Email" AutoPostBack="True" OnTextChanged="tbOtherEmail_TextChanged" TabIndex="16"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 DOB: <asp:CompareValidator ID="CompareValidator1" runat="server"
ControlToValidate="tbOtherDOB" ErrorMessage="**"
Operator="DataTypeCheck" Type="Date" ForeColor="Red">**</asp:CompareValidator>
             </div>
         <div class="col-md-5">
                 <asp:TextBox ID="tbOtherDOB" runat="server" TextMode="Date" TabIndex="17"></asp:TextBox>
             </div>

     </div>
    <div class="row">
             <div class="col-md-1">
                 SSN:
             </div>
         <div class="col-md-5">
             <asp:TextBox ID="tbOtherSSN" runat="server" TabIndex="18" AutoPostBack="True" OnTextChanged="tbOtherSSN_TextChanged"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 DL No:
             </div>
         <div class="col-md-5">
                 <asp:TextBox ID="tbOtherDriversLicNo" runat="server" TabIndex="19"></asp:TextBox>
             </div>
     </div>
    <div class="row">
             <div class="col-md-1">
                 Employer:
             </div>
         <div class="col-md-5">
             <asp:TextBox ID="tbOtherEmployer" runat="server" TabIndex="20"></asp:TextBox>
             </div>
         <div class="col-md-6">
                <asp:DropDownList Visible="false" ID="cboOtherDLState" runat="server"></asp:DropDownList><asp:DropDownList Visible="false" ID="cboOtherDLClass" runat="server"></asp:DropDownList>
             </div>
     </div>
     <div class="row">
             <div class="col-md-1">
                 Language:
             </div>
         <div class="col-md-5">
             <asp:DropDownList ID="cboOtherLanguage" runat="server" TabIndex="21"></asp:DropDownList>
             </div>
         <div class="col-md-1">
                Sex:
             </div>
         <div class="col-md-5">
             <asp:RadioButtonList ID="tbOtherSex" runat="server"  RepeatDirection="Horizontal" TabIndex="22"> <asp:ListItem Value="M" Text="Male"/>
                    <asp:ListItem Value="F" Text="Female" /></asp:RadioButtonList>
             </div>
     </div>
     <div class="row">
             <div class="col-md-1">
                 Comments:
             </div>
         <div class="col-md-11">
             <asp:TextBox ID="tbOtherComments" runat="server" TextMode="MultiLine" OnTextChanged="txtComments_TextChanged" AutoPostBack="True" TabIndex="23"></asp:TextBox>
             <br />
             <asp:Label ID="lblComments" runat="server" ForeColor="Red" Text="You have entered comments in the contact information of this person.  This information DOES NOT REPLACE a file note associated with the claim.  Nor will the information entered as a comment convert to a file note.  It is information that pertains to the PERSON not the CLAIM." Visible="False"></asp:Label>
             </div>
     </div>
    <div id="otherInsurance" runat="server" visible="false">
      <div class="row">
             <div class="col-md-12">
                    <b>Other Insurance</b>    
                 <hr />
                </div>
        </div>
        <div class="row">
             <div class="col-md-1">
                 Company:
             </div>
            <div class="col-md-11">
                <asp:TextBox ID="tbOthIns_Company" runat="server"></asp:TextBox>
             </div>
        </div>
    <div class="row">
             <div class="col-md-1">
                 Policy #:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="tbOthIns_PolicyNo" runat="server"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 Claim #:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="tbOthIns_ClaimNo" runat="server"></asp:TextBox>
             </div>
        </div>
      <div class="row">
             <div class="col-md-1">
                 Adjuster:
             </div>
            <div class="col-md-11">
                <asp:TextBox ID="tbOthIns_Adjuster" runat="server"></asp:TextBox>
             </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Address:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="tbOthIns_Address" runat="server"></asp:TextBox>
             </div>
         <div class="col-md-1">
                 City:
             </div>
            <div class="col-md-5">
                <asp:TextBox ID="tbOthIns_City" runat="server"></asp:TextBox> State: <asp:DropDownList ID="cboOtherIns_State" runat="server"></asp:DropDownList>
             </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Phone:
             </div>
            <div class="col-md-5">
<<<<<<< HEAD
                <asp:TextBox ID="tbOthIns_Phone" runat="server" OnTextChanged="tbOthIns_Phone_TextChanged" AutoPostBack="True" TextMode="Phone"></asp:TextBox>
=======
                <asp:TextBox ID="tbOthIns_Phone" runat="server" TextMode="Phone"></asp:TextBox>
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
             </div>
          <div class="col-md-1">
                 Fax:
             </div>
            <div class="col-md-5">
<<<<<<< HEAD
                <asp:TextBox ID="tbOthIns_Fax" OnTextChanged="tbOthIns_Fax_TextChanged"  AutoPostBack="True" TextMode="Phone" runat="server"></asp:TextBox> ZIP: <asp:TextBox ID="tbOthIns_Zip" runat="server" OnTextChanged="tbOthIns_Zip_TextChanged" CausesValidation="false" AutoPostBack="True"></asp:TextBox>
=======
                <asp:TextBox ID="tbOthIns_Fax"  TextMode="Phone" runat="server"></asp:TextBox> ZIP: <asp:TextBox ID="tbOthIns_Zip" runat="server" OnTextChanged="tbOthIns_Zip_TextChanged" CausesValidation="false" AutoPostBack="True"></asp:TextBox>
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
             </div>
        </div>
     <div class="row">
             <div class="col-md-1">
                 Email:
             </div>
            <div class="col-md-11">
                <asp:TextBox ID="tbOthIns_Email" runat="server" TextMode="Email"></asp:TextBox>
             </div>
        </div>
</div>
       <div class="row">
           <div class="col-md-12">
                   <asp:Button ID="btnPersonDetailOK" runat="server"  class="btn btn-info btn-xs"  Text="OK" OnClick="btnPersonDetailOK_Click" TabIndex="24" />&nbsp;&nbsp;
               <asp:Button ID="btnAddInsurance" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Add Insurance"  causesValidation="false"   OnClick="btnAddInsurance_Click"  />&nbsp;&nbsp;
               <asp:Button ID="btnSIU" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Add/Edit SIU"  causesValidation="false"   OnClick="btnSIU_Click"  />&nbsp;&nbsp;
               <asp:Button ID="pPersonalDetailShowMap" runat="server" Visible="true" class="btn btn-info btn-xs"  Text="Show Map"  causesValidation="false"  OnClick="pPersonalDetailShowMap_Click"/>&nbsp;&nbsp;
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
            </div>
    </div>
     <div class="row">
           <div class="col-md-12">
               <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
           </div>
    </div>
</asp:Content>

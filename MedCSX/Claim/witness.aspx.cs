﻿using MedCSX.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class witness : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

       
        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Witness";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            try  //if add won't exist
            {
                string tmpid = Regex.Replace(Request.QueryString["witness"].ToString(), "[^0-9]", "");
                id = Convert.ToInt32(tmpid);
            }
            catch { }

          

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            Witness myData = new Witness();
            myData.GetWitnessInfoByID(id);
            ddlWitnessPassengerTypes.SelectedValue = myData.witness_passenger_type_id;
            ddlPersonType.SelectedValue = myData.witness_passenger_type_id;
           // witness_passenger_type_id
        }

        protected void PopulateDDLs()
        {
            DataTable dt = new DataTable();

            Witness myData = new Witness();
            dt = myData.GetWitnessPassengerTypes();

            ddlWitnessPassengerTypes.DataSource = dt;
            ddlWitnessPassengerTypes.DataValueField = dt.Columns[0].ColumnName;
            ddlWitnessPassengerTypes.DataTextField = dt.Columns[1].ColumnName;
            ddlWitnessPassengerTypes.DataBind();

            dt = myData.GetWitnessForClaim(claim_id);

            ddlPersonType.DataSource = dt;
            ddlPersonType.DataValueField = dt.Columns[0].ColumnName;
            ddlPersonType.DataTextField = dt.Columns[2].ColumnName;
            ddlPersonType.DataBind();
        }

        protected void ddlVehicleSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                switch (ddlVehicleSelect.SelectedValue)
            {
                case "VehCar":
                    tblCar.Style.Add("background-image", "url('../content/images/icoredcar2.png')");
                    this.rbWitness3rdRowDrivers.Visible = false;
                    this.rbWitness3rdRowPassenger.Visible = false;
                    this.rbWitness3rdRowCenter.Visible = false;
                    this.rbWitness4thRowDriver.Visible = false;
                    this.rbWitness4thRowPassenger.Visible = false;
                    this.rbWitness4thRowCenter.Visible = false;
                    this.rbWitness3rdRowPassenger.Checked = false;
                    this.rbWitness3rdRowDrivers.Checked = false;
                    this.rbWitness3rdRowCenter.Checked = false;
                    this.rbWitness4thRowPassenger.Checked = false;
                    this.rbWitness4thRowDriver.Checked = false;
                    this.rbWitness4thRowCenter.Checked = false;
                    break;
                case "VehVan":
                    tblCar.Style.Add("background-image", "url('../content/images/icoredvantop2.png')");
                    this.rbWitness4thRowDriver.Visible = false;
                    this.rbWitness4thRowPassenger.Visible = false;
                    this.rbWitness4thRowCenter.Visible = false;
                    this.rbWitness4thRowPassenger.Checked = false;
                    this.rbWitness4thRowDriver.Checked = false;
                    this.rbWitness4thRowCenter.Checked = false;
                    this.rbWitness3rdRowDrivers.Visible = true;
                    this.rbWitness3rdRowPassenger.Visible = true;
                    break;
                case "VehTruck":
                    tblCar.Style.Add("background-image", "url('../content/images/icotrucktop2.png')");
                    this.rbWitness4thRowDriver.Visible = true;
                    this.rbWitness4thRowPassenger.Visible = true;
                    this.rbWitness4thRowCenter.Visible = true;
                    this.rbWitness3rdRowDrivers.Visible = true;
                    this.rbWitness3rdRowPassenger.Visible = true;
                    this.rbWitness3rdRowCenter.Visible = true;
                    break;
            }
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmSetAtFault : System.Web.UI.Page
    {
        private int _claimId = 0;        //claim ID of this claim

        //claim object for the open claim
        private MedCsxLogic.Claim MyClaim = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            this._claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            MyClaim = new MedCsxLogic.Claim(_claimId);

            if (!Page.IsPostBack)
            {

                lblLossDescription.Text = MyClaim.LossDescription;
                lblPaidLoss.Text = MyClaim.PaidLoss.ToString("c");
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            MyClaim.AtFaultTypeId = Convert.ToInt32(rbAtFault.SelectedValue);
            MyClaim.Update();


            string url = "ViewClaim?claim_id=" + this._claimId + "&Ptabindex=4";
            string win = "window.open('" + url + "','my_window" + MyClaim.DisplayClaimId + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=900, copyhistory=no, left=400, top=100').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
            //ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='ViewClaim?claim_id=" + this._claimId + "&Ptabindex=4';window.close();", true);
            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
        }
    }
}
﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using MedCsxLogic;
using PdfPrintingNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmDocuments : System.Web.UI.Page
    {

        StreamReader m_streamReader;
        private StreamReader streamToPrint;

        protected void Page_Load(object sender, EventArgs e)
        {


            var pdfPrint = new PdfPrint("Med James, Inc", "g/4JFMjn6KuJU6poHlm/Jhoj99HtdbmaxsQP3/y31LL6ZgDF+AS6ibql/wfepKxZ7GOU1be1TLhmsfUsY77NBjs9ImJjHPsgg/4JFMjn6Ks8SFhkXZQn3Q==");
            string pdfFile = @"C:\\inetpub\\wwwroot\\Draft.pdf";
            int numberOfPages = pdfPrint.GetNumberOfPages(pdfFile);
            pdfPrint.PrinterName = "PLAuto-M3560";
            var status = pdfPrint.Print(pdfFile);

            if (status == PdfPrint.Status.OK)
            { 
               //printdraft = true;
            }
            //// if you have pdf document in byte array that is also supported
            //byte[] pdfContent = YourCustomMethodWhichReturnsPdfDocumentAsByteArray();
            //status = pdfPrint.Print(pdfFile);



            //string m_fileName = "C:\\GitCodeRepos\\medcsxx Integration\\MedCSX\\Documents\\Completed Documents\\KEY Contact Letter to Claimant-140644-12012020 014219-389415.docx";
            //if (m_fileName != null)
            //{

            //    m_streamReader = new StreamReader(m_fileName);

            //}




            //MedCsxPrint printSpecialReport = new MedCsxPrint();
            //printSpecialReport.DocumentName = "Test";
            //printSpecialReport.Print();




            //if (!Page.IsPostBack)
            //{
            //    //string dp = HttpContext.Current.Server.MapPath("Documents/Completed Documents/" + "briantest.docx");
            //    //SearchAndReplace(dp);

            //    string dp = HttpContext.Current.Server.MapPath("Documents/Templates/" + "KEY Contact insured Third Party Claim Pending.dotx");
            //    SearchAndReplaceLive(dp);

            //    // OpenTemplate("Documents/Completed Documents/", "Test Text");
            //    // CreateWordprocessingDocument(@"C:\GitCodeRepos\medcsxx Integration\MedCSX\Documents\Completed Documents\briantest.docx");

            //}
        }

        private void OpenTemplate(string filepath, string txt)
        {

            string dp = HttpContext.Current.Server.MapPath(filepath + "briantest.docx");
            //
            // Open a WordprocessingDocument for editing using the filepath.
            WordprocessingDocument wordprocessingDocument =
                WordprocessingDocument.Open(dp, true);

            // Assign a reference to the existing document body.
            Body body = wordprocessingDocument.MainDocumentPart.Document.Body;

            // Add new text.
            Paragraph para = body.AppendChild(new Paragraph());
            Run run = para.AppendChild(new Run());
            run.AppendChild(new Text(txt));

            // Close the handle explicitly.
            wordprocessingDocument.Close();
        }

        private void LoadFile(string filepath, string txt)
        {
            //_tempFilePath = CloneFileInTemp(pathToDocx);

            string dp = HttpContext.Current.Server.MapPath(filepath + "briantest.docx");
            WordprocessingDocument _document = WordprocessingDocument.Open(dp, true);
            MainDocumentPart _documentElement = _document.MainDocumentPart.Document.MainDocumentPart;

            using (FileStream fileStream = new FileStream(filepath + "briantest.docx", FileMode.Create))
            {
                _document.MainDocumentPart.Document.Save(fileStream);
            }
        }

        public static void CreateWordprocessingDocument(string filepath)
        {
            // Create a document by supplying the filepath. 
            using (WordprocessingDocument wordDocument =
                WordprocessingDocument.Create(filepath, WordprocessingDocumentType.Document))
            {
                // Add a main document part. 
                MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();

                // Create the document structure and add some text.
                mainPart.Document = new DocumentFormat.OpenXml.Wordprocessing.Document();
                Body body = mainPart.Document.AppendChild(new Body());
                Paragraph para = body.AppendChild(new Paragraph());
                Run run = para.AppendChild(new Run());
                run.AppendChild(new Text("Create text in body - CreateWordprocessingDocument"));
            }
        }


        public static void SearchAndReplace(string document)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }

                Regex regexText = new Regex("Hello world!");
                docText = regexText.Replace(docText, "Hi Everyone!");

                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }
        }

        public static void SearchAndReplaceLive(string document)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }

                Regex regexText = new Regex("Claim.DisplayClaimId");
                docText = regexText.Replace(docText, "KIKC123456");

                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }
        }
    }
}
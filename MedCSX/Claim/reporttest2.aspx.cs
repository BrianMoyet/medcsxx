﻿using MedCSX.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class reporttest2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            byte[] bytes;
            Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = "All Diary Entries for User";
            rptParms = GetReportParmsDepartment();
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Response.Clear();
            //ControlsView(false);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + rptDocument.Name + ".PDF");
            Response.BinaryWrite(bytes);
        }



        private Dictionary<string, string> GetReportParmsDepartment()
        {
            Dictionary<string, string> newparms = new Dictionary<string, string>();


            newparms.Add("user_id", "6");


            return newparms;
        }
    }
}
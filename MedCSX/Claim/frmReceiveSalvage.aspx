﻿<%@ Page Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmReceiveSalvage.aspx.cs" Inherits="MedCSX.Claim.frmReceiveSalvage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

              <div class="row">
                <div class="col-md-2">
                    Tax Id: 
                </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtVendorId" runat="server" AutoPostBack="True" OnTextChanged="txtVendorId_TextChanged"></asp:TextBox>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTaxID" runat="server" ForeColor="Red" ></asp:Label>
                      <br />Don't Know Tax Id? <asp:LinkButton ID="txtVendorLookup" OnClick="txtVendorLookup_Click" runat="server" TabIndex="1">Lookup Vendor</asp:LinkButton>
                     
                </div>
            </div>
     <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                        Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-2">
                    Vendor: 
                </div>
                  <div class="col-md-10">
                      <asp:Label ID="txtVendorName" runat="server" Text=""></asp:Label>
                </div>
            </div>
             <div class="row">
                <div class="col-md-2">
                    Check No: 
                </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtDraftNo" runat="server" TabIndex="2"></asp:TextBox>
                </div>
            </div>
     <div class="row">
                <div class="col-md-2">
                    Net Salvage Amount: 
                </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtSalvageAmount" OnTextChanged="txtSalvageAmount_TextChanged" runat="server" AutoPostBack="true" TabIndex="3"></asp:TextBox>
                </div>
            </div>
     <div class="row">
                <div class="col-md-2">
                    Storage Yards: 
                </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtStorageAmount" runat="server" OnTextChanged="txtStorageAmount_TextChanged" AutoPostBack="true" TabIndex="4"></asp:TextBox> Storage Days: <asp:TextBox ID="txtStorageDays" runat="server" TabIndex="5"></asp:TextBox>
                </div>
            </div>
      <div class="row">
                <div class="col-md-2">
                    Towing: 
                </div>
                  <div class="col-md-10">
                      <asp:TextBox ID="txtTowingAmount" runat="server" OnTextChanged="txtTowingAmount_TextChanged" AutoPostBack="true" TabIndex="6"></asp:TextBox>
                </div>
            </div>
      

   
     <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click" TabIndex="7"/>
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

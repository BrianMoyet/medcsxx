﻿<%@ Page Title="Pending Draft" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmPendingDraft.aspx.cs" Inherits="MedCSX.Claim.frmPendingDraft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Sent To:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtSendTo" runat="server"  Width="319px"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            
        </div>
         <div class="col-md-6">
              <asp:RadioButton ID="rbLoss" GroupName="lossexpense" Text="Loss" OnCheckedChanged="rbLoss_CheckedChanged" AutoPostBack="true" CausesValidation="false"  runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:RadioButton ID="rbExpense" GroupName="lossexpense" Text="Expense" OnCheckedChanged="rbLoss_CheckedChanged" AutoPostBack="true" CausesValidation="false" runat="server" />
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Transaction Type:
        </div>
        <div class="col-md-6">
            <asp:DropDownList ID="cboTransactionType" OnSelectedIndexChanged="cboTransactionType_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
        </div>
         
    </div>
    <div class="row">
        <div class="col-md-3">
            Tax Id:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtTaxId" OnTextChanged="txtTaxId_TextChanged" AutoPostBack="true" CausesValidation="false" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
           
        </div>
        <div class="col-md-9">
            Don't Know Tax Id? <asp:LinkButton ID="btnPendVendorLookup" runat="server" OnClick="btnPendVendorLookup_Click" CausesValidation="false">Lookup Vendor...</asp:LinkButton>
                    <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                        Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false" OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
                </div>
            </div>
        </div>
            &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkClaimantAttorney" runat="server" Text="Claimant Attorney"/>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
           Payee:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtPayee" TextMode="MultiLine" runat="server" Height="30px" Width="319px" AutoPostBack="True" OnTextChanged="txtPayee_TextChanged"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Explanation of Benefits:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtExplanation" TextMode="MultiLine" runat="server" Height="30px" Width="319px"></asp:TextBox>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
           Mail TO:
        </div>
        <div class="col-md-9">
            
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Name:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtMailToName"  runat="server" Width="319px" AutoPostBack="True" OnTextChanged="txtMailToName_TextChanged"></asp:TextBox>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
           Address 1:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtAddress1" runat="server" AutoPostBack="True" OnTextChanged="txtMailToName_TextChanged" ></asp:TextBox>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
           Address 2:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtAddress2" runat="server" AutoPostBack="True" OnTextChanged="txtMailToName_TextChanged"></asp:TextBox>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
           City:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtCity" runat="server" AutoPostBack="True" OnTextChanged="txtMailToName_TextChanged"></asp:TextBox>&nbsp;&nbsp;
            St: <asp:DropDownList ID="cboState" runat="server"></asp:DropDownList>&nbsp;&nbsp;
            Zip: <asp:TextBox ID="txtZip" runat="server" AutoPostBack="True" OnTextChanged="txtMailToName_TextChanged"></asp:TextBox>
        </div>
    </div>



      <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOk" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOk_Click" TabIndex="14"/>
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

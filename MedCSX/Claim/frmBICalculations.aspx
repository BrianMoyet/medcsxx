﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmBICalculations.aspx.cs" Inherits="MedCSX.Claim.frmBICalculations" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <b>BI Calculated Fields</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Unadjusted
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">
            Adjusted for Liability
        </div>
        <div class="col-md-3">

        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
           Loss Range: <asp:Label ID="lblUnadjLossRange" runat="server" ForeColor="Blue"></asp:Label>
        </div>
        <div class="col-md-3">
            Expected Value: <asp:Label ID="lblUnadjExpValue" runat="server" ForeColor="Blue"></asp:Label>
        </div>
        <div class="col-md-3">
           Loss Range: <asp:Label ID="lblAdjLossRange" runat="server" ForeColor="Blue"></asp:Label>
        </div>
        <div class="col-md-3">
             Expected Value: <asp:Label ID="lblAdjExpValue" runat="server" ForeColor="Blue"></asp:Label>
        </div>
    </div>
      <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <%--Left Column Unadjusted Loss Low--%>
        <div class="col-md-6">
           <b>Unadjusted Loss (Low)</b>
            <div class="row">
                <div class="col-md-6">
                    Best Case General Damages:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjLowGenDamages" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                    Best Case Wage Loss:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjLowWageLoss" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                    Best Case Ambulance:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjLowAmbulance" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                    Future Medical:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjLowFutureMed" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                   Total Best Case Diagnostics:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:Label ID="lblTtlBestCaseDiag" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                   Total Best Case Treatments:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:Label ID="lblTtlBestCaseTreat" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                   Unadjusted Loss (Low):
                </div>
                 <div class="col-md-6 float-right">
                     <asp:Label ID="lblTtlUnadjLossLow" Font-Bold="True" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label>
                </div>
            </div>
        </div>

        <%--Right Column Unadjusted Loss High--%>
        <div class="col-md-6">
            <b>Unadjusted Loss (High)</b>
             <div class="row">
                <div class="col-md-6">
                    Worst Case General Damages:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjHighGenDamages" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    Worst Case Wage Loss:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjHighWageLoss" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                    Worst Case Ambulance:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjHighAmbulance" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                    Worst Case Future Medical:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:TextBox ID="txtUnadjHighFutureMed" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtBox_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox> +
                </div>
            </div>
              <div class="row">
                <div class="col-md-6">
                   Total Worst Case Diagnostics:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:Label ID="lblTtlWorstCaseDiag" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> +
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                   Total Worst Case Treatments:
                </div>
                 <div class="col-md-6 float-right">
                     <asp:Label ID="lblTtlWorstCaseTreat" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> =
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                   Unadjusted Loss (High):
                </div>
                 <div class="col-md-6 float-right">
                     <asp:Label ID="lblTtlUnadjLossHigh" runat="server" Font-Bold="True" ForeColor="Blue" Text="" Width="75px"></asp:Label>
                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <b>Unadjusted Expected Value</b>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
           Unadjusted Expected Value = (Unadjusted Loss High - Unadjusted Loss Low) * (1 - Probablity of Damage Evaluation) + Unadjusted Loss Low
     <br />Unadjusted Expected Value = (<asp:Label ID="lblUnadjLossHigh" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> -  <asp:Label ID="lblUnadjLossLow" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label>)
            * (1 -  <asp:TextBox ID="txtGenDamages" runat="server" TextMode="Number" Width="75px" AutoPostBack="True" CausesValidation="True" OnTextChanged="txtGenDamages_TextChanged">0</asp:TextBox>)
            + <asp:Label ID="lblUnadjLossLow2" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> = <asp:Label ID="lblUnadjExpectedValue" runat="server" Font-Bold="True" ForeColor="Blue" Text="" Width="75px"></asp:Label>
        </div>
    </div>
      <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <b>Adjusted Loss (Low)</b>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            Adjusted Loss Low = Best Case Comparative Negligence * Unadjusted Expected Value
            <br />Adjusted Loss Low = <asp:TextBox ID="txtGenBestCaseNegligence" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtGenDamages_TextChanged" AutoPostBack="True" CausesValidation="True">0</asp:TextBox> * <asp:Label ID="lblUnadjExpectedValue2" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> = <asp:Label ID="lblAdjLossLow" runat="server" Font-Bold="True" ForeColor="Blue" Text="" Width="75px"></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <b>Adjusted Loss (High)</b>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            Adjusted Loss High = Worst Case Comparative Negligence * Unadjusted Expected Value
            <br />Adjusted Loss High = <asp:TextBox ID="txtGenWorstCaseNegligence" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtGenDamages_TextChanged" AutoPostBack="True" CausesValidation="True">0</asp:TextBox> * <asp:Label ID="lblUnadjExpectedValue3" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> = <asp:Label ID="lblAdjLossHigh" runat="server" Font-Bold="True" ForeColor="Blue" Text="" Width="75px"></asp:Label>
        </div>
    </div>
       <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <b>Adjusted Expected Value</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Adjusted Expected Value = (Adjusted Loss High - Adjusted Loss Low) * (1 - Probablity of Liability Evaluation) + Adjusted Loss Low
            <br />Adjusted Expected Value = (<asp:Label ID="lblAdjLossHigh2" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> - <asp:Label ID="lblAdjLossLow2" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label>)
            * (1 - <asp:TextBox ID="txtGenLiability" runat="server" TextMode="Number" Width="75px" OnTextChanged="txtGenDamages_TextChanged" AutoPostBack="True" CausesValidation="True">0</asp:TextBox>) + <asp:Label ID="lblAdjLossLow3" runat="server" ForeColor="Blue" Text="" Width="75px"></asp:Label> = <asp:Label ID="lblAdjExpectedValue" runat="server" Font-Bold="True" ForeColor="Blue" Text="" Width="75px"></asp:Label>
        </div>
    </div>
      <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="Close" OnClick="btnOK_Click"/>
    </div>
</asp:Content>

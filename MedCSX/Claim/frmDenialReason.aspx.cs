﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmDenialReason : System.Web.UI.Page
    {
        private int reserveId,              //reserve ID to create transaction for.  Input parameter
                                            //salvageId,                      //salvage_Id of salvage row inserted
                                            //subroId,                        //subro_Id of subro row inserted
          reissueFromId,                  //Id to reissue from 
          m_mode,                         //which screen to present? Subro or Salvage
          reserveTypeId = 0,            //Policy_Coverage_Type_Id
          claim_id = 0;

        private Reserve m_reserve;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void btnPickOK_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            m_reserve = new Reserve(this.reserveId);
            m_reserve.Close(Convert.ToInt32(this.cboDenialReason.SelectedValue)); //close the reserve

            //ClientScript.RegisterStartupScript(this.GetType(), "close", "parent.location.reload();", true);
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='viewClaim.aspx?claim_id=" + m_reserve.ClaimId + "&Ptabindex=7';window.close();", true);
         
        }

        private string voidReasonTable,
          msg = string.Empty,
          reasonPart = string.Empty,
          noLockPart = ". Proceeding Will Place the Reserve in a Locked State Until Approved By Your Supervisor. Do You Wish to Proceed?",
          lockedPart = ". this would normally add a lock to the reserve, but since it's already locked, the system will just create a note instead. Do You Wish to Proceed?";


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = "Denial Reason";

            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            m_reserve = new Reserve(this.reserveId);
            this.m_mode = Convert.ToInt32(Request.QueryString["mode"].ToString());
            voidReasonTable = Request.QueryString["void_reason_table"].ToString();

            mm.UpdateLastActivityTime();

            if (!Page.IsPostBack)
            {
                bool reasonsLoaded = LoadReasons();

                //if (this.voidReasonTable == "Draft_Void_Reason")

                //else if (this.voidReasonTable == "Subro_Salvage_Void_Reason")
                //    this.cboVoidReason.SelectedIndex = setSelection((int)modGeneratedEnums.SubroSalvageVoidReason.NSF_Stop_Payment);
            }
        }

        private bool LoadReasons()
        {

            this.cboDenialReason.DataSource = TypeTable.getTypeTableRowsDDL(this.voidReasonTable);
            this.cboDenialReason.DataValueField = "Id";
            this.cboDenialReason.DataTextField = "Description";
            this.cboDenialReason.DataBind();

            this.cboDenialReason.SelectedIndex = (int)modGeneratedEnums.DraftVoidReason.Printing_Problem;
            this.cboDenialReason_SelectedIndexChanged(null, null);

            return true;


        }

        protected void cboDenialReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["DenailReasonID"] = this.cboDenialReason.SelectedValue;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmPicker2 : System.Web.UI.Page
    {
        #region Class Variables
        private int reserveId = 0;
        private int m_mode;
        private MedCsxLogic.Claim m_Claim = null;

        private List<ClaimantGrid> myDraftList;
        private List<siteGrid> mySiteList;
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_mode = 0;
            if (this.m_mode == 0)
            {
                this.reserveId = Convert.ToInt32(Request.QueryString["reserve_id"]);
                this.m_Claim = new MedCsxLogic.Claim(new Reserve(this.reserveId).ClaimId);
                RefreshData();
            }
          
        }

        private void RefreshData()
        {
            try
            {
                bool draftsLoaded;
                mm.UpdateLastActivityTime();

                if (m_mode == 0)
                {
                    this.btnPickOK.Visible = false;
                    this.btnPickCancel.Visible = false;
                    //this.btnPickClose.Visibility = Visibility.Visible;
                }
                //else
                //{
                //    this.btnPickOK.Visibility = Visibility.Visible;
                //    this.btnPickCancel.Visibility = Visibility.Visible;
                //    this.btnPickClose.Visibility = Visibility.Collapsed;
                //}
                this.Page.Title = "Claim " + this.m_Claim.DisplayClaimId + " - Unprinted Drafts";

                if (reserveId == 0)
                {
                    this.txtHeaderLabel.Text = "This Claim has Drafts Which Have Not Been Printed. The Claim Cannot be Closed.";
                    draftsLoaded = LoadDrafts(1);
                }
                else
                {
                    this.Page.Title += " On " + Reserve.ReserveDescription(ref this.reserveId);
                    this.txtHeaderLabel.Text = "This Reserve has Drafts Which Have Not Been Printed. The Reserve Cannot be Closed.";
                    draftsLoaded = LoadDrafts(2);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadDrafts(int p)
        {
            if (p == 1)
            { 
                this.grSelections.DataSource = LoadDrafts(mm.GetUnPrintedDrafts(this.m_Claim.Id, 1));
                this.grSelections.DataBind();
            }
            else
            { 
                this.grSelections.DataSource = LoadDrafts(mm.GetUnPrintedDrafts(this.reserveId, 2));
                this.grSelections.DataBind();
            }
           
            this.grSelections.SelectedIndex = 0;
            return true;
        }

        private IEnumerable LoadDrafts(List<Hashtable> list)
        {
            myDraftList = new List<ClaimantGrid>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    ClaimantGrid cg = new ClaimantGrid();
                    cg.claimantId = (int)dData["Id"];
                    cg.fullName = (string)dData["Claimant"];
                    cg.personType = (string)dData["Reserve Type"];
                    cg.draftNo = (int)dData["Draft No"];
                    cg.draftAmount = (double)((decimal)dData["Amount"]);
                    cg.payee = (string)dData["Payee"];
                    cg.streetAddress = (string)dData["Address1"];
                    cg.Address2 = (string)dData["Address2"];
                    cg.city = (string)dData["City"];
                    cg.state = (string)dData["State"];
                    cg.postalCode = (string)dData["Zip"];
                    cg.imageIdx = (int)dData["Image_Index"];
                    myDraftList.Add(cg);
                }
                return myDraftList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDraftList;
            }
        }

        class siteGrid
        {
            public int id { get; set; }
            public string siteName { get; set; }
            public string siteAddress { get; set; }
            public string siteLogon { get; set; }
            public string sitePassword { get; set; }
        }

        protected void btnPickOpen_Click(object sender, EventArgs e)
        {

            Session["referer"] = Request.Url.ToString();

            string url = "frmTransactions.aspx?mode=c&claim_id=" + m_Claim.ClaimId + "&reserve_id=" + reserveId;
            string s = "window.open('" + url + "', 'popup_windowTR', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "scriptTR", s, true);
        }

        protected void btnPickRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public enum DraftDisplay : int
    {
        Print_Draft,
        Draft_Pending
    };

    public partial class frmDraft : System.Web.UI.Page
    {
        public int transactionId = 0;     //inserted transaction ID for the draft.  output parameter

        private int reserveId = 0,            //reserve ID for draft.  Input parameter
            reissueFromTransactionId = 0,     //transaction ID this draft is reissued from.  input parameter
            voidReasonId = 0,                 //void reason ID - from reissue.  input parameter
            draftId = 0,                      //inserted draft ID
            claimId,                        //claim ID associated with draft
            transactionTypeId = 0,            //selected transaction type ID
            reissuedFromDraftId = 0,          //draft ID - from reissue
            vendorId = 0;                     //vendor ID corresponding to tax id
        private Reserve res = null;
        private MedCsxLogic.Claim claim = null;
        private Draft origDraft;
        private string oldTaxId = "",         //previous tax ID.  used to see if it changed on a LostFocus event
            taxId = "",                       //tax ID entered
            lossPayeeName = "",
            claimantName = "";
        private bool lockTransaction = false, //does this draft cause a lock to be placed on the transaction?
            isLoss = false,                   //is transaction type chosen a loss?  otherwise expense
            doneLoading = false,              //is form finished loading?
            isPerPeriod = false,              //is draft for per-period coverage?
            isPrinting = false,               //is currently printing?
            reserveLocked = false,
            claimLocked = false,
            compOrCollision = false,
            userChangedMailingAddress = false,
            userChangedPayee = false;

        private string referer = "";
       

        private bool _contentLoaded;

        protected void Page_Load(object sender, EventArgs e)
        {

            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            this.reissueFromTransactionId = 0;
            this.voidReasonId = 0;
            this.res = new Reserve(this.reserveId);
            this.claim = this.res.Claim;
            this.m_mode = Convert.ToInt32(Request.QueryString["mode"].ToString());

          


            try
            {
                reissueFromTransactionId = Convert.ToInt32(Request.QueryString["reissue_from_transaction_id"]);
                voidReasonId = Convert.ToInt32(Request.QueryString["void_reason_id"]);
            }
            catch (Exception ex)
            {

            }

           

            if (Session["referer2"] != null)
            {

                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();


            if (!Page.IsPostBack)  //this was just changed from doneloading which would never be true after reload
            {

               
                this.lockTransaction = false;
                if (m_mode == (int)DraftDisplay.Print_Draft)
                {
                    tiDraft.Visible = true; 
                    this.txtLossPayee.Text = "";

                    claimId = Reserve.GetClaimIdForReserveId(ref reserveId);        //set claim ID

                    CheckReserveClaimLocks();

                    mf.LoadStateComboBox(this.cboMailToState);

                    //populate checkboxes for review
                    PopulateLossPayee();
                    PopulateDeductible();
                    PopulateMedicalLien();

                    if (!Page.IsPostBack)
                    {
                       
                        

                        //set title
                        this.Page.Title = "Claim " + this.claim.DisplayClaimId + " - Draft For " + Reserve.ReserveDescription(ref reserveId);

                        PopulateReissue();      //populate fields from original draft, if reissue

                        if (reserveLocked || claimLocked)
                        {
                            this.rbTransLoss.Enabled = false;
                            lblTransactionTypes.Visible = true;
                            lblTransactionTypes.Text = "Only Expense Payments are Allowed because this Claim is Locked";

                        }

                        if (MedCsXSystem.Settings.DraftPrintingCurrentlyOn)
                            this.lblDraftQueueWarning.Visible = false;
                        else
                            this.lblDraftQueueWarning.Visible = true;
                        if (txtVendorId.Text.Length == 0)
                            DisableControls(false);      //disable screen until loss/expense and transaction type is chosen


                        //this.txtTaxId.Enabled = true;
                        btnVendorLookup.Enabled = true;
                    }
                }
                else if (m_mode == (int)DraftDisplay.Draft_Pending)
                {

                    if (!Page.IsPostBack)
                    {
                        this.tiPending.Visible = true;
                        this.Page.Title = "Pending Draft";
                        //this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuClock.png", UriKind.Relative));

                        if (this._pendingDraft != null)
                        {
                            this.res = this._pendingDraft.Reserve;
                            this.reserveId = this.res.Id;

                            mf.LoadStateComboBox(this.cboPendMailToState);

                            if (this._pendingDraft.TransactionType.isLoss)
                                this.rbPendTransLoss.Checked = true;
                            else
                                this.rbPendTransExpense.Checked = true;

                            this.txtPendMailToAddress1.Text = this._pendingDraft.Draft.Address1;
                            this.txtPendMailToAddress2.Text = this._pendingDraft.Draft.Address2;
                            this.txtPendMailToCity.Text = this._pendingDraft.Draft.City;
                            this.txtPendExplanation.Text = this._pendingDraft.Draft.ExplanationOfProceeds;
                            this.txtPendMailToName.Text = this._pendingDraft.Draft.MailToName;
                            this.txtPendPayee.Text = this._pendingDraft.Draft.Payee;
                            this.txtPendSentTo.Text = this._pendingDraft.PendingDraftSentTo;

                            if (this._pendingDraft.Draft.Vendor() != null)
                            {
                                this.txtPendTaxId.Text = this._pendingDraft.Draft.Vendor().TaxId;
                                this.chkPendClaimantAttorney.Checked = this._pendingDraft.Draft.Vendor().isClaimantAttorney;
                            }

                            this.txtPendMailToZip.Text = this._pendingDraft.Draft.Zipcode;
                            this.cboPendMailToState.SelectedIndex = this._pendingDraft.Draft.StateId + 4;
                            this.cboPendTransactionType.SelectedIndex = setSelection(1, 0, this._pendingDraft.TransactionType.Description);
                        }
                        DisableControls(false);      //disable screen until loss/expense and transaction type is chosen

                        this.rbPendTransExpense.Enabled = true;
                        this.rbPendTransLoss.Enabled = false;
                    }
                }

               

                doneLoading = true;
            }

            if ((Request.QueryString["reissue"] != null) && (!Page.IsPostBack))
            {
                reason.Visible = true;
                reissueFromTransactionId = Convert.ToInt32(Request.QueryString["reissue_from_transaction_id"]);
                string voidReasonTable = Request.QueryString["void_reason_table"].ToString();
                this.cboVoidReason.DataSource = TypeTable.getTypeTableRowsDDL(voidReasonTable);
                this.cboVoidReason.DataValueField = "Id";
                this.cboVoidReason.DataTextField = "Description";
                this.cboVoidReason.DataBind();

                //this.cboVoidReason.SelectedIndex = (int)modGeneratedEnums.DraftVoidReason.Lost_In_Mail;
                //this.cboVoidReason_SelectedIndexChanged(null, null);

                //PopulateReissue();
            }

            if (Session["vendorID"] != null)
            {
                vendorId = Convert.ToInt32(Session["vendorID"]);
                Vendor myVendor = new Vendor(vendorId);
                txtVendorId.Text = vendorId.ToString();
                txtTaxId.Text = myVendor.TaxId.ToString();
                txtTaxId_TextChanged(null, null);
                Session["vendorID"] = null;
                DisableControls(true);
                btnPrint.Enabled = false;
                gbMailTo.Disabled = false;
                gbPendMailTo.Disabled = false;
            }
        }

        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string msg = "";

            if (Request.QueryString["reissue"] != null)
            {
                if (Convert.ToInt32(cboVoidReason.SelectedValue) < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Select Reissue Reason.');</script>");
                    //MessageBox.Show("Please Select Reissue Reason.", "Reissue Reason not selected", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    cboVoidReason.Focus();
                    return;
                }
            }
            if (cboTransactionType.Visible == true)
            { 
                if (cboTransactionType.SelectedValue.ToString() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Select Transaction Type.');</script>");
                    //MessageBox.Show("Please Select Transaction Type.", "Transaction Type not selected", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    cboTransactionType.Focus();
                    return;
                }
            }

            if (cboPendTransactionType.Visible == true)
            {
                if (cboPendTransactionType.SelectedValue.ToString() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Select Transaction Type.');</script>");
                    cboPendTransactionType.Focus();
                    return;
                }
            }

            if (cboTransactionType.Visible == true)
                transactionTypeId = Convert.ToInt32(cboTransactionType.SelectedValue);
            else
                transactionTypeId = Convert.ToInt32(cboPendTransactionType.SelectedValue);

            if ((chkLossPayee.Visible == true) && (!chkLossPayee.Checked))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Review Loss Payee Information Before Proceeding.');</script>");
                //MessageBox.Show("Please Review Loss Payee Information Before Proceeding.", "Review Loss Payee", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                chkLossPayee.Focus();
                return;
            }

            if ((chkDeductible.Visible == true) && (!chkDeductible.Checked))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Review Deductible Information Before Proceeding.');</script>");
                //MessageBox.Show("Please Review Deductible Information Before Proceeding.", "Review Deductible Info", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                chkDeductible.Focus();
                return;
            }

            if ((chkMedicalLien.Visible == true) && (!chkMedicalLien.Checked))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Review Medical Lien Information Before Proceeding.');</script>");
                //MessageBox.Show("Please Review Medical Lien Information Before Proceeding.", "Review Medical Lien", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                chkMedicalLien.Focus();
                return;
            }

           



            //try
            //{
                string extendedErr;

                if (this.isPrinting)
                    return;     //prevent duplicate drafts
                this.isPrinting = true;

                mm.UpdateLastActivityTime();
                this.btnPrint.Enabled = false;

                //validate draft fields entered
                if (!ValidateDraft(ref msg))
                {
                    this.btnPrint.Enabled = true;
                    this.isPrinting = false;
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                    return;
                }

                //if final payment, ask if total loss
                bool isTtlLoss = false;

                if ((transactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment) &&
                    (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve) &&
                    ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) ||
                    (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive) ||
                    (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) ||
                    (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
                {

                    //if (MessageBox.Show("Is this Reserve a Total Loss?", "Is Reserve Total Loss", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                    if (chkTotalLoss.Checked)
                    {
                        isTtlLoss = true;
                        res.isTotalLoss = true;
                        res.Update();
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This Reserve Will be Closed Pending Salvage.');</script>");
                        //MessageBox.Show("This Reserve Will be Closed Pending Salvage.", "Reserve Closed Pending Salvage", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                }

                if (((transactionTypeId == (int)modGeneratedEnums.TransactionType.Legal_Expenses) ||
                    (transactionTypeId == (int)modGeneratedEnums.TransactionType.Legal_Fees)) &&
                    (this.reissuedFromDraftId == 0))
                {   /*legal fees or legal expenses
                     * create the drafts if the amount > 0
                     * new draft - not reissued*/
                    try
                    {
                        if (double.Parse(this.txtTransAmount.Text) > 0)
                            CreateDraft((int)modGeneratedEnums.TransactionType.Legal_Fees, double.Parse(this.txtTransAmount.Text), this.eventTypeIds, isTtlLoss);

                        if (double.Parse(this.txtTransExpenseAmount.Text) > 0)
                            CreateDraft((int)modGeneratedEnums.TransactionType.Legal_Expenses, double.Parse(this.txtTransExpenseAmount.Text), this.expenseEventTypeIds, isTtlLoss);
                    }
                    catch (Exception excep)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('An error occurred in first Try: MSG - " + excep.Message + " : SOURCE - " + excep.Source + " : TRACE - " + excep.StackTrace + "');</script>");
                        //MessageBox.Show("An error occurred in first Try: MSG - " + excep.Message + " : SOURCE - " + excep.Source + " : TRACE - " + excep.StackTrace, "Draft Print Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        excep.Message.Remove(0, 1);
                    }
                }
                else
                {
                    //try
                    //{
                        //string tmpTransAmont = this.txtTransAmount.Text.Replace("$","").Replace(",","");
                        //    Double tmpTransAmount = Convert.ToDouble(tmpTransAmont);
                        //    //Double tmpTransAmont = double.Parse(this.txtTransAmount.Text, CultureInfo.InvariantCulture);
                        //Double tmpTransAmont = double.Parse(this.txtTransAmount.Text, CultureInfo.InvariantCulture);
                        CreateDraft(this.transactionId, double.Parse(txtTransAmount.Text.Replace("$", "").Replace(",", "")), this.eventTypeIds, isTtlLoss);
                    //}
                    //catch (Exception excep)
                    //{
                    //    extendedErr = "transactionTypeId: " + this.transactionId.ToString();
                    //    extendedErr += Environment.NewLine + Environment.NewLine + " | txtAmount: " + this.txtTransAmount.Text;
                    //    extendedErr += Environment.NewLine + Environment.NewLine + " | isTotalLoss: " + isTtlLoss.ToString();
                    //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('An error occurred in second Try: MSG - " + excep.Message + " : SOURCE - " + excep.Source + " : TRACE - " + excep.StackTrace + "');</script>");
                    //    //MessageBox.Show("An error occurred in second Try: MSG - " + excep.Message + " : SOURCE - " + excep.Source + " : TRACE - " + excep.StackTrace, "Draft Printing Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    //    excep.Message.Remove(0, 1);
                    //}
                }
            
                if (msg.Length < 0)
                    Session["msg"] = "Draft successfully created and placed in Draft Queue.";
                else
                    Session["msg"] = msg + " Draft successfully created and placed in Draft Queue.";

                string url2 = "frmMessage.aspx";
                string s = "window.open('" + url2 + "', 'popup_window_message', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=200, copyhistory=no, left=400, top=400');";
                ClientScript.RegisterStartupScript(this.GetType(), "popup_window_message", s, true);


                Session["referer2"] = null;

                string myWindow = "my_window" + claim.DisplayClaimId;
                string url = Session["referer"].ToString();
                string s2 = "window.open('" + url + "', '" + myWindow + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
                //ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
                btnPrint.Enabled = false;
                btnOK.Enabled = false;


<<<<<<< HEAD
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
=======
<<<<<<< HEAD
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
=======
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';" + s2 + ";window.close();", true);
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


            //if (msg.Length < 0)
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft successfully created.');</script>");
            //else
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + " Draft successfully created.');</script>");

            //}
            //catch (Exception ex)
            //{
            //    this.btnPrint.Enabled = true;
            //    this.isPrinting = false;
            //    mf.ProcessError(ex);
            //}
        }

        private Hashtable CloneToHashtable(Draft draft)
        {
            Hashtable clone = new Hashtable();
            clone.Add("Address1", draft.Address1);
            clone.Add("Address2", draft.Address2);
            clone.Add("City", draft.City);
            //TODO
            //clone.Add("Draft_Amount", draft.DraftAmount);
            clone.Add("Draft_Amount", draft.DraftAmount);
            clone.Add("Draft_Void_Reason_Id", draft.DraftVoidReasonId);
            clone.Add("Explanation_Of_Proceeds", draft.ExplanationOfProceeds);
            clone.Add("Mail_To_Name", draft.MailToName);
            clone.Add("Payee", draft.Payee);
            if (this.isPerPeriod)
            {
                clone.Add("Pip_Date_From", draft.PipDateFrom);
                clone.Add("Pip_Date_To", draft.PipDateTo);
            }
            clone.Add("Reissued_From_Draft_Id", draft.ReissuedFromDraftId);
            clone.Add("State_Id", draft.StateId);
            clone.Add("Zipcode", draft.Zipcode);

            return clone;
        }

        private void CreateDraft(int p1, double amount, Hashtable parms, bool isTtlLoss)
        {

            //Draft draft = new Draft();
            //PopulateDraft(draft);
            //draft.DraftAmount = (double)amount;
            //draft.StateId = this.cboMailToState.SelectedIndex - 2;
            //draft.VendorId = Vendor.getVendorIdForTaxId(txtTaxId.Text);
            ////draft.VendorId = vendorId;
            //draft.ReissuedFromDraftId = reissuedFromDraftId;
            //Hashtable draftHT = CloneToHashtable(draft);

            Hashtable clone = new Hashtable();
            PopulateDraft(clone);
            clone.Add("Draft_Amount", amount);
            clone.Add("State_Id", mm.GetStateId(cboMailToState.SelectedValue.ToString()));
            clone.Add("Vendor_Id", Vendor.getVendorIdForTaxId(txtTaxId.Text));
            clone.Add("Reissued_From_Draft_Id", reissuedFromDraftId);
            //clone.Add("transaction_type_id", Convert.ToInt32(cboTransactionType.SelectedValue));

            //if (isPerPeriod == false)
            //{

            //}
            if (reason.Visible == true)
            {
                reissueFromTransactionId = Convert.ToInt32(Request.QueryString["reissue_from_transaction_id"]);
                voidReasonId = Convert.ToInt32(this.cboVoidReason.SelectedValue);
            }


            //insert draft row, transaction row, void transaction (if necessary), update reserve, fire events, etc.
            //try
            //{
            this.transactionId = Draft.InsertDraft(clone, this.reserveId, Convert.ToInt32(cboTransactionType.SelectedValue), parms, this.voidReasonId, this.reissueFromTransactionId, isTtlLoss);
            //}
            //catch (Exception ex)
            //{
            //    string extendedErr = "reserveID: " + this.reserveId.ToString();
            //    extendedErr += Environment.NewLine + Environment.NewLine + " | TransactionTypeId: " + p1.ToString();
            //    extendedErr += Environment.NewLine + Environment.NewLine + " | voidReasonId: " + this.voidReasonId.ToString();
            //    extendedErr += Environment.NewLine + Environment.NewLine + " | reissueFromTransactionId: " + this.reissueFromTransactionId.ToString();
            //    extendedErr += Environment.NewLine + Environment.NewLine + " | isTotalLoss: " + isTtlLoss.ToString();
              
            //    MessageBox.Show("An error occurred in CreateDraft.Draft.InsertDraft: MSG - " + ex.Message + " : SOURCE - " + ex.Source + " : TRACE - " + ex.StackTrace + Environment.NewLine + Environment.NewLine + "Extended Msg - " + extendedErr, "Draft Print Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    ex.Message.Remove(0, 1);
                //return;
            //}

            //get draft ID from transaction ID.  if we're this far, we have a valid transaction ID
            Transaction tr = new Transaction(transactionId);
            this.draftId = tr.DraftId;

            //check for locks
            EventType et;
            bool canPrintDraft = true;
            if (parms != null)
            {
                foreach (int v in parms.Keys)
                {
                    et = new EventType(v);
                    if (et.isLockDefined)
                        canPrintDraft = false;      //the draft issued locks, don't print it
                }
            }

            if (canPrintDraft)
            {
                CheckReserveClaimLocks();
                if (reserveLocked && isLoss)
                    canPrintDraft = false;
               
               mf.PrintDraft(this.draftId, false);
               

            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            string msg = "";
            if (Request.QueryString["reissue"] != null)
            {
                if (Convert.ToInt32(cboVoidReason.SelectedValue) < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Select Reissue Reason.');</script>");
                    //MessageBox.Show("Please Select Reissue Reason.", "Reissue Reason not selected", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    cboVoidReason.Focus();
                    return;
                }
            }

            try
            {
                mm.UpdateLastActivityTime();

                if (!ValidateDraft(ref msg))
                    return;

                if (this._pendingDraft == null)
                    CreatePendingDraft();

                Session["referer2"] = null;

                string myWindow = "my_window" + claim.DisplayClaimId;
                string url = Session["referer"].ToString();
                string s2 = "window.open('" + url + "', '" + myWindow + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
                //ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
                //ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';" + s2 + ";window.close();", true);
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';" + s2 + ";", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void PopulateDraft(Hashtable draft)
        {
            if (this.m_mode == (int)DraftDisplay.Print_Draft)
            {
                draft.Add("Mail_To_Name", this.txtMailToName.Text);
                draft.Add("Address1", this.txtMailToAddress1.Text);
                draft.Add("Address2", this.txtMailToAddress2.Text);
                draft.Add("City", this.txtMailToCity.Text);
                draft.Add("Zipcode", this.txtMailToZip.Text);
                draft.Add("Draft_Void_Reason_Id", this.voidReasonId);
                draft.Add("Explanation_Of_Proceeds", this.txtExplanation.Text);
                draft.Add("Payee", this.txtPayee.Text);
                if (this.isPerPeriod)
                {
                    draft.Add("Pip_Date_From", DateTime.Parse(this.txtPIPfrom.Text));
                    draft.Add("Pip_Date_To", DateTime.Parse(this.txtPIPto.Text));
                }
            }
            else
            {
                draft.Add("Mail_To_Name", this.txtPendMailToName.Text);
                draft.Add("Address1", this.txtPendMailToAddress1.Text);
                draft.Add("Address2", this.txtPendMailToAddress2.Text);
                draft.Add("City", this.txtPendMailToCity.Text);
                draft.Add("Zipcode", this.txtPendMailToZip.Text);
                draft.Add("Draft_Void_Reason_Id", this.voidReasonId);
                draft.Add("Explanation_Of_Proceeds", this.txtPendExplanation.Text);
                draft.Add("Payee", this.txtPendPayee.Text);
            }
            
        }

        private void PopulateDraft(Draft draft)
        {
            if (this.m_mode == (int)DraftDisplay.Print_Draft)
            {
                draft.MailToName = this.txtMailToName.Text;
                draft.Address1 = this.txtMailToAddress1.Text;
                draft.Address2 = this.txtMailToAddress2.Text;
                draft.City = this.txtMailToCity.Text;
                draft.Zipcode = this.txtMailToZip.Text;
                draft.DraftVoidReasonId = this.voidReasonId;
                draft.ExplanationOfProceeds = this.txtExplanation.Text;
                draft.Payee = this.txtPayee.Text;
                if (this.isPerPeriod)
                {
                    draft.PipDateFrom = DateTime.Parse(this.txtPIPfrom.Text);
                    draft.PipDateTo = DateTime.Parse(this.txtPIPto.Text);
                }
            }
            else
            {
                draft.MailToName = this.txtPendMailToName.Text;
                draft.Address1 = this.txtPendMailToAddress1.Text;
                draft.Address2 = this.txtPendMailToAddress2.Text;
                draft.City = this.txtPendMailToCity.Text;
                draft.Zipcode = this.txtPendMailToZip.Text;
                draft.DraftVoidReasonId = this.voidReasonId;
                draft.ExplanationOfProceeds = this.txtPendExplanation.Text;
                draft.Payee = this.txtPendPayee.Text;
            }
        }

        private void CreatePendingDraft()
        {
            //draft
            Draft d = new Draft();
            PopulateDraft(d);
            d.BankAccountNo = this.res.Claim.CompanyLocation().Company.BankAccountNo;
            d.BankRoutingNo = this.res.Claim.CompanyLocation().Company.BankRoutingNo;
            d.DraftNo = this.res.Claim.NextDraftNo;
            d.PrintDate = DateTime.Now;
            if (this.txtPendTaxId.Text.Trim() != "")
                d.VendorId = vendorId;
            d.Update();

            //draft print
            DraftPrint dp = d.InsertDraftPrintForPending(this.res);

            //pending draft
            PendingDraft pd = new PendingDraft();
            pd.ReserveId = this.reserveId;
            pd.DraftId = d.Id;
            pd.DraftPrintId = dp.Id;
            pd.PendingDraftStatusId = (int)modGeneratedEnums.PendingDraftStatus.Pending;
            pd.PendingDraftSentTo = this.txtPendSentTo.Text;
            pd.TransactionTypeId = this.transactionTypeId;
            pd.Update();

            
            string msg = "";
            if (mf. PrintDraftForDraftPrintId(dp.Id, ref msg))
                d.isPrinted = true;
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
           
        }

        private bool ValidateDraft(ref string msg)
        {
            double amount = 0,        //draft amount
                expenseAmount = 0,    //expenses draft amount
                prevPayments = 0,     //previous payments on reserve
                perPerson = 0,        //per person coverage
                perAccident = 0,      //per accident coverage
                netLossReserve = 0,   //net loss reserve amount
                netExpReserve = 0;    //net expense reserve amount
            int noPeriods = 0,        //PIP per person periods
                periodTypeId = 0,     //per person perido type ID (day, month, year)
                actualPeriods = 0;
            DateTime minDate = default(DateTime),
                maxDate = default(DateTime),
                pipDateFrom,
                pipDateTo;
            //string msg = "",
            string periodName = "";       //Month, Day, or Year
            bool deductible = false,
                changeReserve = false; //change the net loss/expense reserve

            if (tiDraft.Visible == true)
            {
                if (rbTransLoss.Checked)
                {
                    isLoss = true;
                }
                else
                {
                    isLoss = false;
                }
            }
            else
            {
                if (rbPendTransLoss.Checked)
                {
                    isLoss = true;
                }
                else
                {
                    isLoss = false;
                }
            }


            if (this.m_mode == (int)DraftDisplay.Print_Draft)
            {
                
                if (this.txtTransAmount.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Transasction Amount is Required.');</script>");
                    //MessageBox.Show("Transasction Amount is Required.", "Transaction Amount Required", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);

                    if (this.txtTransAmount.Visible)
                        this.txtTransAmount.Focus();
                    return false;
                }

                if (this.txtPayee.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Payee is Required.');</script>");
                    txtPayee.Focus();
                    return false;
                }

                if (this.txtExplanation.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Explanation is Required.');</script>");
                    txtExplanation.Focus();
                    return false;
                }

                if (this.txtMailToName.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Mail To Name is Required.');</script>");
                    txtMailToName.Focus();
                    return false;
                }

                if (this.txtMailToAddress1.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Mail To Address 1 is Required.');</script>");
                    txtMailToAddress1.Focus();
                    return false;
                }

                if (this.txtMailToCity.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Mail To City is Required.');</script>");
                    txtMailToCity.Focus();
                    return false;
                }

                if (this.cboMailToState.SelectedIndex == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Mail To State is Required.');</script>");
                    cboMailToState.Focus();
                    return false;
                }

                if (this.txtMailToZip.Text.Length < 1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Mail To Zip is Required.');</script>");
                    txtMailToZip.Focus();
                    return false;
                }


                amount = double.Parse(this.txtTransAmount.Text.Trim().Replace("$", "").Replace(",", ""));    //get draft amount
                expenseAmount = double.Parse(this.txtTransExpenseAmount.Text.Trim().Replace("$", "").Replace(",", ""));  //get expenses draft amount

                //per-person and per-accident coverage info
                if (res.PolicyCoverage() != null)
                {
                    perAccident = res.PolicyCoverage().PerAccidentCoverage;
                    perPerson = res.PolicyCoverage().PerPersonCoverage;
                    noPeriods = res.PolicyCoverage().PerPersonNoPeriods;
                    deductible = res.PolicyCoverage().hasDeductible;
                    periodTypeId = res.PolicyCoverage().PerPersonPeriodTypeId;
                }

                if (this.isPerPeriod)
                    perPerson = perPerson * noPeriods;

                prevPayments = claim.LossForCoverageType(res.ReserveTypeId);

                //default event type ID to zero
                this.eventTypeIds = new Hashtable();
                this.expenseEventTypeIds = new Hashtable();

                //check for reviewed loss payee
                if (this.gbLossPayee.Visible && !(bool)this.chkLossPayee.Checked)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Review Loss Payee Information Before Proceeding.');</script>");
                    //MessageBox.Show("Please Review Loss Payee Information Before Proceeding.", "Review Loss Payee", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    //MessageBox.Show("Please Review Loss Payee Information Before Proceeding.", "Review Loss Payee", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.chkLossPayee.Focus();
                    return false;
                }

                //check for review medical lien
                if (this.gbMedicalLien.Visible && !(bool)this.chkMedicalLien.Checked)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Review Medical Lien Information Before Proceeding.');</script>");
                    //MessageBox.Show("Please Review Medical Lien Information Before Proceeding.", "Review Medical Lien", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    //MessageBox.Show("Please Review Medical Lien Information Before Proceeding.", "Review Medical Lien", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.chkMedicalLien.Focus();
                    return false;
                }

                /*test for zero or negative draft amounts for the legal fees and expenses
                 * must have at least one to be a valid amount
                 * only performed on new draft - not reissued*/
                if (((transactionTypeId == (int)modGeneratedEnums.TransactionType.Legal_Expenses) ||
                    (transactionTypeId == (int)modGeneratedEnums.TransactionType.Legal_Fees)) &&
                    (this.reissuedFromDraftId == 0))
                {
                    if (amount < 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Fees Amount Must Be Positive Amount.  Please enter a Valid Amount.);</script>");
                        //MessageBox.Show("Fees Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Fees Amount Is Negative", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("Fees Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Fees Amount Is Negative", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtTransAmount.Visible)
                            this.txtTransAmount.Focus();
                        return false;
                    }

                    if (expenseAmount < 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Expenses Amount Must Be Positive Amount.  Please enter a Valid Amount.');</script>");
                        //MessageBox.Show("Expenses Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Expenses Amount Is Negative", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("Expenses Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Expenses Amount Is Negative", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtTransExpenseAmount.Visible)
                            this.txtTransExpenseAmount.Focus();
                        return false;
                    }

                    if ((amount == 0) && (expenseAmount == 0))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Fees and Expenses Amount are both Zero. At least one Must be Greater than Zero.  Please enter a Valid Amount.');</script>");
                        //MessageBox.Show("Fees and Expenses Amount are both Zero. At least one Must be Greater than Zero.  Please enter a Valid Amount.", "Amount Are Zero", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("Fees and Expenses Amount are both Zero. At least one Must be Greater than Zero.  Please enter a Valid Amount.", "Amount Are Zero", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtTransAmount.Visible)
                            this.txtTransAmount.Focus();
                        else if (this.txtTransExpenseAmount.Visible)
                            this.txtTransExpenseAmount.Focus();
                        return false;
                    }
                }
                else
                {
                    if (amount < 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount Must Be Positive Amount.  Please enter a Valid Amount.');</script>");
                        //MessageBox.Show("Draft Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Draft Amount Is Negative", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("Draft Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Draft Amount Is Negative", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtTransAmount.Visible)
                            this.txtTransAmount.Focus();
                        return false;
                    }

                    if (amount == 0)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount is Zero. Draft Amount Must Be Positive Amount.  Please enter a Valid Amount.');</script>");
                        //MessageBox.Show("Draft Amount is Zero. Draft Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Draft Amount Is Zero", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("Draft Amount is Zero. Draft Amount Must Be Positive Amount.  Please enter a Valid Amount.", "Draft Amount Is Zero", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtTransAmount.Visible)
                            this.txtTransAmount.Focus();
                        return false;
                    }
                }

                bool taxIdNotRequired = false;
                taxId = this.txtTaxId.Text.Trim();
                //if tax ID is not required, go to next validation
                if (this.vendorId != 0)
                {
                    Vendor vendor = new Vendor(vendorId);
                    if (!vendor.TaxIdRequired)
                        taxIdNotRequired = true;
                }

                //if (!taxIdNotRequired)
                //{
                if (this.taxId == "")
                {
                    if (rbTransExpense.Checked &&
                    (this.chkClaimantAttorney.Visible && (bool)this.chkClaimantAttorney.Checked))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Tax Id is a Required Field');</script>");
                        if (this.txtTaxId.Visible)
                            this.txtTaxId.Focus();
                        return false;
                    }
                }
                else
                {
                    if ((Vendor.getVendorTypeIdForVendorId(vendorId) != (int)modGeneratedEnums.VendorType.Claimant_Attorney) && this.chkClaimantAttorney.Visible && (bool)this.chkClaimantAttorney.Checked)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Tax Id is Not for Claimant Attorney.');</script>");
                        if (this.txtTaxId.Visible)
                            this.txtTaxId.Focus();
                        return false;
                    }
                }
                // }

                //test PIP dates
                if (isPerPeriod && isLoss)
                {
                    //date from
                    if (!ml.isDate(this.txtPIPfrom.Text))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('PIP Date From is Not a Valid Date.');</script>");
                        //MessageBox.Show("PIP Date From is Not a Valid Date", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("PIP Date From is Not a Valid Date", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtPIPfrom.Visible)
                            this.txtPIPfrom.Focus();
                        return false;
                    }
                    this.txtPIPfrom.Text = (DateTime.Parse(this.txtPIPfrom.Text)).ToShortDateString();

                    if (!ml.isDate(this.txtPIPto.Text))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('PIP Date To is Not a Valid Date.');</script>");
                        //MessageBox.Show("PIP Date To is Not a Valid Date", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        //MessageBox.Show("PIP Date To is Not a Valid Date", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtPIPto.Visible)
                            this.txtPIPto.Focus();
                        return false;
                    }
                    this.txtPIPto.Text = (DateTime.Parse(this.txtPIPto.Text)).ToShortDateString();

                    pipDateFrom = DateTime.Parse(this.txtPIPfrom.Text);
                    pipDateTo = DateTime.Parse(this.txtPIPto.Text);

                    if (pipDateFrom > pipDateTo)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('PIP Date From Greater than Date To.');</script>");
                        //MessageBox.Show("PIP Date From Must be Earlier than PIP Date To", "Date From Greater than Date To", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);

                        //MessageBox.Show("PIP Date From Must be Earlier than PIP Date To", "Date From Greater than Date To", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtPIPfrom.Visible)
                            this.txtPIPfrom.Focus();
                        return false;
                    }

                    //is PIP coverage exceeded (365 days for PIP Ess Serv, 12 months PIP wages, etc.)
                    res.getPIPDateRange(ref minDate, ref maxDate);      //determine min/mas dates
                    if (minDate == ml.DEFAULT_DATE)
                        minDate = pipDateFrom;
                    else if (pipDateFrom < minDate)
                        minDate = pipDateFrom;

                    if (maxDate == ml.DEFAULT_DATE)
                        maxDate = pipDateTo;
                    else if (pipDateTo > maxDate)
                        maxDate = pipDateTo;

                    //determine number of periods between min/max dates
                    switch (periodTypeId)
                    {
                        case (int)modGeneratedEnums.PeriodType.Per_Day:
                            actualPeriods = maxDate.Subtract(minDate).Days;
                            periodName = "Days";
                            break;
                        case (int)modGeneratedEnums.PeriodType.Per_Month:
                            actualPeriods = ((maxDate.Year - minDate.Year) * 12) + maxDate.Month - minDate.Month;
                            //actualPeriods = (int)Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Month, minDate, maxDate);
                            periodName = "Months";
                            break;
                        case (int)modGeneratedEnums.PeriodType.Per_Year:
                            //actualPeriods = (int)Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Year, minDate, maxDate);
                            DateTime zeroTime = new DateTime(1, 1, 1);
                            TimeSpan span = maxDate - minDate;
                            actualPeriods = (zeroTime + span).Year - 1;
                            periodName = "Years";
                            break;
                        default:
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Unknown PIP Period Type.  Period Type Id = " + periodTypeId + "');</script>");
                            //MessageBox.Show("Unknown PIP Period Type.  Period Type Id = " + periodTypeId);
                            //MessageBox.Show("Unknown PIP Period Type.  Period Type Id = " + periodTypeId);
                            return false;
                    }

                    //are policy periods exceeding
                    if (actualPeriods > noPeriods)
                    {
                        msg = "The Policy Allows for " + noPeriods + " " + periodName;
                        msg += " of Coverage, Which Have Been Exceeded. ";
                        msg += " You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";



                        Hashtable parms = new Hashtable();
                        parms.Add("Policy_Periods", noPeriods);
                        parms.Add("Period_Name", periodName);
                        parms.Add("Actual_Periods", actualPeriods);
                        parms.Add("Min_Date", minDate);
                        parms.Add("Max_Date", maxDate);

                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Pip_Policy_Periods_Exceeded, parms);
                    }
                }



                //test for max supplemental payments
                if (claim.maxSupplementalPaymentReached)
                {   //against Closed Claim
                    msg += "The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments.');</script>");
                    //MessageBox.Show("The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments.",
                    //    "Max Supplemental Payments for Claim", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    return false;
                }

                if (res.maxSupplementalPaymentsReached)
                {   //against Closed Reserve
                    msg += "The Maximum Number of Supplemental Payments have been Made Against This Reserve Line. It Must be Reopened to allow Payments.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Maximum Number of Supplemental Payments have been Made Against This Reserve Line. It Must be Reopened to allow Payments.');</script>");
                    //MessageBox.Show("The Maximum Number of Supplemental Payments have been Made Against This Reserve Line. It Must be Reopened to allow Payments.",
                    //    "Max Supplemental Payments for Reserve", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    return false;
                }

                //draft amount cannot be greater than net reserve for non-final payment
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                {
                    if (isLoss)
                    {
                        netLossReserve = res.NetLossReserve;

                        if (reissuedFromDraftId > 0)
                            netLossReserve += this.origDraft.DraftAmount;

                        if ((amount > netLossReserve) && (transactionTypeId != (int)modGeneratedEnums.TransactionType.Final_Loss_Payment))
                            {
                                msg += "Draft Amount of " + amount.ToString("c") +
                                " is Greater than the Net Loss Reserve of " + netLossReserve.ToString("c") +
                                ". Net Loss Reserve will be increased by " +
                                (amount - netLossReserve).ToString("c") + " to Cover the Draft Amount";

                                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount of " + amount.ToString("c") +
                                //" is Greater than the Net Loss Reserve of " + netLossReserve.ToString("c") +
                                //". Net Loss Reserve will be increased by " +
                                //(amount - netLossReserve).ToString("c") + " to Cover the Draft Amount');</script>", true);


                                //increase net loss reserves
                                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Loss_Reserve))
                                    return false;

                                //string msg2 = "";
                                mf.ChangeNetLossReserve(this.reserveId, amount, ref msg);

                                //if (msg.Length > 1)
                                //{

                                //    msg += "Draft Not Validated. ";
                                //    msg += "<p> ---------------------</p><p>";
                                //    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Not Validated. " + msg + "');</script>", true);
                                //    return false;
                                //}
                                //else
                                //{ 
                                res = new Reserve(reserveId);
                                changeReserve = true;
                                //}
                        }
                    }
                    else
                    {
                        netExpReserve = res.NetExpenseReserve;
                        double expensePlus = amount + expenseAmount;

                        if (reissuedFromDraftId > 0)
                            netExpReserve += this.origDraft.DraftAmount;

                        if ((expensePlus > netExpReserve) && (transactionTypeId != (int)modGeneratedEnums.TransactionType.Final_Loss_Payment))
                        {
                            msg += "Total Amount of " + expensePlus.ToString("c") +
                                " is Greater than the Net Expense Reserve of " + netExpReserve.ToString("c") +
                                ". Net Expense Reserve will be increased by " +
                                (expensePlus - netExpReserve).ToString("c") + " to Cover the Draft Amount";

                            
                            //increase net loss reserves
                            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Expense_Reserve))
                                return false;
                              
                            mf.ChangeNetExpenseReserve(this.reserveId, expensePlus, ref msg);
                               
                            res = new Reserve(reserveId);
                            changeReserve = true;
                              
                        }
                    }
                    
                }

                /*check for draft amount plus previous loss 
                 * and expense payments in excess of user authority*/

                //don't count reissue twice
                double amountToCheck = 0,
                    expenseAmountToCheck = 0;

                if (this.reissuedFromDraftId > 0)
                    amountToCheck = amount - origDraft.DraftAmount;
                else
                {
                    amountToCheck = amount;
                    expenseAmountToCheck = expenseAmount;
                }

                if (res.AuthorityExceeded(amountToCheck, isLoss))
                {
                    EventType et = new EventType((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_User_Authority);
                    if (et.isLockDefined)
                    {
                        msg += "Draft Amount of " + amount.ToString("c") +
                            " Causes Reserve to Exceed Your Financial Authority of " + res.UserAuthority().ToString("c") +
                            ".  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount of " + amount.ToString("c") +
                        //    " Causes Reserve to Exceed Your Financial Authority of " + res.UserAuthority().ToString("c") +
                        //    ".  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");
                        //if (MessageBox.Show("Draft Amount of " + amount.ToString("c") +
                        //    " Causes Reserve to Exceed Your Financial Authority of " + res.UserAuthority().ToString("c") +
                        //    ". Do You Wish to Continue Anyway? You Will Not be Able to Print the Draft Until Approved by Your Supervisor.",
                        //    "Draft Exceeds User Authority", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                        //    return false;
                    }

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", amount);
                    parms.Add("Previous_Payments", prevPayments);
                    parms.Add("User_Authority", res.UserAuthority());
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_User_Authority, parms);
                }

                if (!isLoss && (this.reissuedFromDraftId == 0) &&
                    (res.AuthorityExceeded(expenseAmountToCheck, isLoss)))
                {   //check legal expense amount against the user authority
                    EventType et = new EventType((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_User_Authority);
                    if (et.isLockDefined)
                    {
                        msg += "Draft Amount of " + expenseAmount.ToString("c") +
                                          " Causes Reserve to Exceed Your Financial Authority of " + res.UserAuthority().ToString("c") +
                                          ".  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount of " + expenseAmount.ToString("c") +
                        //                  " Causes Reserve to Exceed Your Financial Authority of " + res.UserAuthority().ToString("c") +
                        //                  ".  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");

                    }

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", expenseAmount);
                    parms.Add("Previous_Payments", prevPayments);
                    parms.Add("User_Authority", res.UserAuthority());
                    expenseEventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_User_Authority, parms);
                }

                //check for loss payee
                if (!CheckPayee())
                {
                    msg += "Payee is Missing the Named Insured or the Loss Payee. " +
                         " An Alert Will be Sent to Your Supervisor, " +
                         "but the Draft Will Not be Locked.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Payee is Missing the Named Insured or the Loss Payee. " +
                    //     " An Alert Will be Sent to Your Supervisor, " +
                    //     "but the Draft Will Not be Locked.');</script>");
                    //if (MessageBox.Show("Payee is Missing the Named Insured or the Loss Payee. " +
                    //     "Do You Wish to Continue Anyway? an Alert Will be Sent to Your Supervisor, " +
                    //     "but the Draft Will Not be Locked.", "Payee Missing Info",
                    //     MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //    return false;

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", amount);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Draft_Missing_Insured_Or_Loss_Payee, parms);
                }

                //check for at-fault not set on ABI/PD loss payment
                if (isLoss && !this.claim.AtFaultType().isYes &&
                    (this.res.ReserveType.isAbi || this.res.ReserveType.isPropertyDamage))
                {
                    msg += "This loss payment is inconsistent with the At Fault setting of " +
                       this.claim.AtFaultType().Description + ". The At Fault indicator changed to Yes on the General Tab.";

                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Paid_With_At_Fault_Undetermined, new Hashtable());
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This loss payment is inconsistent with the At Fault setting of " +
                    //   this.claim.AtFaultType().Description + ". Set the At Fault indicator to Yes on the General Tab.');</script>");
                    //return false;
                    //TODO
                    //if (MessageBox.Show("This loss payment is inconsistent with the At Fault setting of " +
                    //   this.claim.AtFaultType().Description + ". Do you want to set the At Fault indicator to Yes?",
                    //   "At Fault Not Consistent", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                    //{
                    this.claim.AtFaultTypeId = (int)modGeneratedEnums.AtFaultType.Yes;
                    this.claim.AtFaultDateSet = ml.DBNow();
                    this.claim.Update();
                    //}
                    //else
                    //{

                    //}
                }

                //comp/collision - no coverage limits
                if (deductible)
                    return true;

                //check for no coverage
                if (isLoss && ((perAccident == 0) && (perPerson == 0)))
                {
                    msg += "There is no Coverage for this Reserve. " +
                        "You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There is no Coverage for this Reserve. " +
                    //    "You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");
                    //TODO
                    //if (MessageBox.Show("There is no Coverage for this Reserve. Do You Wish to Continue Anyway? " +
                    //    "You Will Not be Able to Print the Draft Until Approved by Your Supervisor.",
                    //    "Draft Issued With No Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //    return false;

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", amount);
                    ReserveType rt = new ReserveType(res.ReserveTypeId);
                    PolicyCoverageType pct = new PolicyCoverageType(rt.PolicyCoverageTypeId);

                    if (pct.hasPerPersonCoverage)
                    {
                        parms.Add("Previous_Payments", prevPayments);
                        parms.Add("Per_Person_Coverage", perPerson);
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage, parms);
                    }
                    else if (pct.hasPerAccidentCoverage)
                    {
                        parms.Add("Previous_Payments", prevPayments);
                        parms.Add("Per_Accident_Coverage", perAccident);
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Accident_Coverage, parms);
                    }
                    return true;
                }

                //check for draft amount plus loss payments of same coverage type across claim in excess of per accident coverage
                if (isLoss && (res.PerAccidentCoverage > 0) && (amountToCheck + prevPayments > perAccident))
                {
                    msg += "Draft Amount of " + amount.ToString("c") +
                      " Causes the Reserve to Exceed the Per-Accident Coverage of " + perAccident.ToString("c") +
                      ".  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount of " + amount.ToString("c") +
                    //  " Causes the Reserve to Exceed the Per-Accident Coverage of " + perAccident.ToString("c") +
                    //  ".  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");
                    //TODO
                    //if (MessageBox.Show("Draft Amount of " + amount.ToString("c") +
                    //  " Causes the Reserve to Exceed the Per-Accident Coverage of " + perAccident.ToString("c") +
                    //  ". Do You Wish to Continue Anyway? You Will Not be Able to Print the Draft Until Approved by Your Supervisor.", "Draft Exceeds Per Accident Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //    return false;

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", amount);
                    parms.Add("Previous_Payments", prevPayments);
                    parms.Add("Per_Accident_Coverage", perAccident);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Accident_Coverage, parms);
                }

                //check for draft amount plus previous loss payments in excess of per person coverage
                if (isLoss && (perPerson > 0) && (amountToCheck + res.TotalLossPayments > perPerson))
                {
                    msg = "Draft Amount of " + amount.ToString("c") +
                          " Causes the Reserve to Exceed the Per-Person Coverage of " + perPerson.ToString("c") +
                          ". You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Amount of " + amount.ToString("c") +
                    //      " Causes the Reserve to Exceed the Per-Person Coverage of " + perPerson.ToString("c") +
                    //      ". You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");
                    //TODO
                    //if (MessageBox.Show("Draft Amount of " + amount.ToString("c") +
                    //      " Causes the Reserve to Exceed the Per-Person Coverage of " + perPerson.ToString("c") +
                    //      ". Do You Wish to Continue Anyway? You Will Not be Able to Print the Draft Until Approved by Your Supervisor.", "Draft Exceeds Per Accident Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //    return false;

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", amount);
                    parms.Add("Previous_Payments", prevPayments);
                    parms.Add("Per_Person_Coverage", perPerson);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage, parms);
                }

                //check for possible duplicat draft amounts
                if (isLoss && mm.CheckForPossibleDuplicateDrafts(this.reserveId, this.vendorId, amount))
                {
                    msg += "The Amount and Payee are Identical to a Prior Draft from this Reserve.  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Amount and Payee are Identical to a Prior Draft from this Reserve.  You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");


                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", amount);
                    parms.Add("Vendor_Id", this.vendorId);
                    parms.Add("Reserve_Id", this.reserveId);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Is_Printed_Changed, parms);

                }
                else if (mm.CheckForPossibleDuplicateDrafts(this.reserveId, this.vendorId, expenseAmount))
                {
                    msg += "The Amount and Payee are Identical to a Prior Draft from this Reserve. " +
                         " You Will Not be Able to Print the Draft Until Approved by Your Supervisor.";

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Amount and Payee are Identical to a Prior Draft from this Reserve. " +
                    //     " You Will Not be Able to Print the Draft Until Approved by Your Supervisor.');</script>");
                    //TODO
                    //if (MessageBox.Show("The Amount and Payee are Identical to a Prior Draft from this Reserve. " +
                    //     "Do You Wish to Continue Anyway? You Will Not be Able to Print the Draft Until Approved by Your Supervisor.",
                    //     "Possible Duplicate Draft", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //    return false;

                    Hashtable parms = new Hashtable();
                    parms.Add("Draft_Amount", expenseAmount);
                    parms.Add("Vendor_Id", this.vendorId);
                    parms.Add("Reserve_Id", this.reserveId);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Draft_Is_Printed_Changed, parms);

                }
            }
            else
            {
                if (this.txtPendSentTo.Text.Trim() == "")
                {
                    //msg += "Sent To is a Required Field.";
                    //     msg += "<p> ---------------------</p><p>";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Sent To is a Required Field.');</script>");
                    //MessageBox.Show("Sent To is a Required Field");
                    if (this.txtPendSentTo.Visible)
                        this.txtPendSentTo.Focus();
                    return false;
                }

                if (this.cboPendTransactionType.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Must select a transaction type.');</script>");
                    //MessageBox.Show("Must select a transaction type.", "Transaction Type Required", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    if (this.cboPendTransactionType.Visible)
                        this.cboPendTransactionType.Focus();
                    return false;
                }

                if (this.txtPendPayee.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Payee is a Required Field.');</script>");
                    //MessageBox.Show("Payee is a Required Field");
                    if (this.txtPendPayee.Visible)
                        this.txtPendPayee.Focus();
                    return false;
                }

                if (this.txtPendExplanation.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Explanation of Proceeds is a Required Field');</script>");
                    //MessageBox.Show("Explanation of Proceeds is a Required Field");
                    if (this.txtPendExplanation.Visible)
                        this.txtPendExplanation.Focus();
                    return false;
                }

                if (this.txtPendMailToName.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recipient Name is a Required Field');</script>");
                    //MessageBox.Show("Recipient Name is a Required Field");
                    if (this.txtPendMailToName.Visible)
                        this.txtPendMailToName.Focus();
                    return false;
                }

                if (this.txtPendMailToAddress1.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recipient Address is a Required Field');</script>");
                    //MessageBox.Show("Recipient Address is a Required Field");
                    if (this.txtPendMailToAddress1.Visible)
                        this.txtPendMailToAddress1.Focus();
                    return false;
                }

                if (this.txtPendMailToCity.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recipient City is a Required Field.');</script>");
                    //MessageBox.Show("Recipient City is a Required Field");
                    if (this.txtPendMailToCity.Visible)
                        this.txtPendMailToCity.Focus();
                    return false;
                }

                if (this.txtPendMailToZip.Text == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recipient Zip Code is a Required Field.');</script>");
                    //MessageBox.Show("Recipient Zip Code is a Required Field");
                    if (this.txtPendMailToZip.Visible)
                        this.txtPendMailToZip.Focus();
                    return false;
                }

                taxId = this.txtPendTaxId.Text.Trim();
                Vendor _vendor = new Vendor(vendorId);
                if (vendorId != 0)
                {
                    if (!_vendor.TaxIdRequired)
                        return true;
                }

                if ((taxId == "") && ((!(bool)rbPendTransLoss.Checked) || (this.chkPendClaimantAttorney.Visible && (bool)this.chkPendClaimantAttorney.Checked)))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Tax Id is a Required Field.');</script>");
                    //MessageBox.Show("Tax Id is a Required Field");
                    if (this.txtPendTaxId.Visible)
                        this.txtPendTaxId.Focus();
                    return false;
                }

                if (taxId != "")
                {
                    if (_vendor.isClaimantAttorney && this.chkPendClaimantAttorney.Visible && !(bool)this.chkPendClaimantAttorney.Checked)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Tax Id is not for a Claimant Attorney.');</script>");
                        //MessageBox.Show("Tax Id is not for a Claimant Attorney", "Invalid Tax Id", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        if (this.txtPendTaxId.Visible)
                            this.txtPendTaxId.Focus();
                        return false;
                    }
                }
            }
            return true;
        }

        private string AmountAsDollar(string inputNum)
        {
            string num = inputNum.Trim().Replace("$", "").Replace(",", "");
            if (num == "")
                num = "0";

            string[] digits = num.Split('.');
            if (digits.Length > 2)
                return "0.00";

            if (modLogic.isNumeric(digits[0]))
            {
                if (digits.Length == 2)
                {
                    if (!modLogic.isNumeric(digits[1]))
                        return "0.00";
                }

                double number = double.Parse(num);
                return number.ToString("c");
            }
            return "0.00";
        }

        private bool CheckPayee()
        {
            if (isLoss && compOrCollision)
            {
                string payee = this.txtPayee.Text;
                //check for loss payee
                if (payee.IndexOf(lossPayeeName) == 0)
                    return false;

                //check for named insured
                if (payee.IndexOf(claimantName) == 0)
                    return false;
            }
            return true;
        }

        protected void rbTransExpense_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTransExpense.Checked)
            {
                //this.rbPendTransLoss.Checked = false;
                //this.rbTransLoss.Checked = false;
                this.isLoss = false;
                //DisableControls(true);
                LoadTransactionTypes();
                DisableControls(false);
                if (this.m_mode == (int)DraftDisplay.Print_Draft)
                {
                    this.cboTransactionType.Enabled = true;
                    this.cboTransactionType.SelectedIndex = -1;
                }
                else
                {
                    this.cboPendTransactionType.Enabled = true;
                    this.cboPendTransactionType.SelectedIndex = -1;
                }
            }
        }

      

        private void PopulateFromClaimant()
        {
            try
            {
                Person p = res.Claimant.Person;

                if (p == null)
                    return;

                if (this.txtMailToName.Text != "")
                    return;     //user has already entered - don't overwrite

                this.txtMailToName.Text = p.Name;
                this.txtPayee.Text = p.Name;
                this.txtMailToAddress1.Text = p.Address1;
                this.txtMailToAddress2.Text = p.Address2;
                this.txtMailToCity.Text = p.City;
                this.txtMailToZip.Text = p.Zipcode;
                this.cboMailToState.SelectedValue = mm.GetStateForId(p.StateId);

                //if comp or collision and there is a loss payee, add it to the payee text
                if (this.lossPayeeName != "")
                {
                    if (compOrCollision)
                        this.txtPayee.Text += " And " + lossPayeeName;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtTaxId_TextChanged(object sender, EventArgs e)
            {
            try
            {
                mm.UpdateLastActivityTime();

                if (m_mode == (int)DraftDisplay.Print_Draft)
                {
                    //taxId = this.txtTaxId.Text.Trim();

                    //if (taxId == "")
                    //    return;
                    //else if (taxId == oldTaxId)
                    //    return;
                    //oldTaxId = taxId;

                    //lookup tax id
                   
                    lblTaxID.Visible = false;
                    txtVendorId.Text = vendorId.ToString();
                    if (txtTaxId.Text.Length > 0)
                    {
                        try
                        {
                            vendorId = Vendor.getVendorIdForTaxId(txtTaxId.Text);
                        }
                        catch (Exception ex)
                        {
                            vendorId = 0;
                        }
                    }
                    else
                    {
                        vendorId = Convert.ToInt32(txtVendorId.Text);
                    }
                    
                    if (vendorId == 0)
                    {
                        lblTaxID.Visible = true;
                        lblTaxID.Text = "Vendor Not Found Corresponding to Tax Id " + taxId + ".";
                        if (this.txtTaxId.Visible)
                            this.txtTaxId.Focus();
                        return;
                    }

                    Vendor vendor = new Vendor(vendorId);
                    if ((Vendor.getVendorTypeIdForVendorId(vendorId) != (int)modGeneratedEnums.VendorType.Claimant_Attorney) &&
                        this.chkClaimantAttorney.Visible && (bool)this.chkClaimantAttorney.Checked)
                    {
                        lblTaxID.Visible = true;
                        lblTaxID.Text = "Tax Id is Not for a Claimant Attorney";
                        if (this.txtTaxId.Visible)
                            this.txtTaxId.Focus();
                        return;
                    }
                    lblTaxID.Visible = false;
                }
                else
                {
                    taxId = this.txtPendTaxId.Text.Trim();

                    if (taxId == "")
                        return;
                    else if (taxId == oldTaxId)
                        return;
                    oldTaxId = taxId;

                    //lookup tax id
                    vendorId = Vendor.getVendorIdForTaxId(taxId);
                    if (vendorId == 0)
                    {
                        lblTaxID.Visible = true;
                        lblTaxID.Text = "Vendor Not Found Corresponding to Tax Id " + taxId + ".";
                        if (this.txtPendTaxId.Visible)
                            this.txtPendTaxId.Focus();
                        return;
                    }

                    lblTaxID.Visible = false;

                    Vendor vendor = new Vendor(vendorId);
                    if ((Vendor.getVendorTypeIdForVendorId(vendorId) != (int)modGeneratedEnums.VendorType.Claimant_Attorney) &&
                        this.chkPendClaimantAttorney.Visible && (bool)this.chkPendClaimantAttorney.Checked)
                    {
                        lblTaxID.Visible = true;
                        lblTaxID.Text = "Tax Id is Not for a Claimant Attorney";
                        if (this.txtPendTaxId.Visible)
                            this.txtPendTaxId.Focus();
                        return;
                    }
                }
                PopulateVendorControls();       //populate vendor controls
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void PopulateVendorControls()
        {
            Vendor v = new Vendor(vendorId);
            if (m_mode == (int)DraftDisplay.Print_Draft)
            {
                if (this.reissueFromTransactionId == 0)
                {
                    this.txtTaxId.Text = v.TaxId;

                    if (!this.userChangedPayee)
                        this.txtPayee.Text = v.Name;

                    if (!this.userChangedMailingAddress)
                    {
                        this.txtMailToName.Text = v.Name;
                        this.txtMailToAddress1.Text = v.Address1;
                        this.txtMailToAddress2.Text = v.Address2;
                        this.txtMailToCity.Text = v.City;
                        this.txtMailToZip.Text = v.Zipcode;
                        string tmpstate = mm.GetStateForId(v.StateId);
                        this.cboMailToState.Text = tmpstate;
                    }
                }
            }
            else
            {
                this.txtPendTaxId.Text = v.TaxId;

                if (!this.userChangedPayee)
                    this.txtPendPayee.Text = v.Name;

                if (!this.userChangedMailingAddress)
                {
                    this.txtPendMailToName.Text = v.Name;
                    this.txtPendMailToAddress1.Text = v.Address1;
                    this.txtPendMailToAddress2.Text = v.Address2;
                    this.txtPendMailToCity.Text = v.City;
                    this.txtPendMailToZip.Text = v.Zipcode;
                    string tmpstate = mm.GetStateForId(v.StateId);
                    this.cboPendMailToState.Text = tmpstate;
                }
            }
        }

        protected void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mySelection = cboTransactionType.SelectedValue;
                if (mySelection == "")
                    return;
                else
                    DisableControls(true);

                mm.UpdateLastActivityTime();
                this.transactionTypeId = Convert.ToInt32(mySelection);

                /*make claimant attorney checkbox visible for losses only and defaut to checked
                 * if checked, a vendor ID is required for loss payments.  
                 * a vendor ID is always required for expense payments.*/
                if ((transactionTypeId == (int)modGeneratedEnums.TransactionType.Partial_Loss_Payment) ||
                    (transactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment) ||
                    (transactionTypeId == (int)modGeneratedEnums.TransactionType.Supplemental_Loss_Payment))
                {
                    this.chkClaimantAttorney.Visible = true;
                    this.chkClaimantAttorney.Checked = true;

                    if (reissueFromTransactionId == 0)
                        PopulateFromClaimant();     //populate payee and mail to address fields from claimant
                }
                else
                    this.chkClaimantAttorney.Visible = false;

                if ((transactionTypeId == (int)modGeneratedEnums.TransactionType.Legal_Expenses) ||
                    ((transactionTypeId == (int)modGeneratedEnums.TransactionType.Legal_Fees) && (reissuedFromDraftId == 0)))
                {
                    //allow user to enter legal fees and legal expenses on same draft screen
                    //still creates 2 drafts

                    this.lblTransAmount.Text = "Fees Amount:";
                    this.lblTransExpense.Visible = true;
                    this.txtTransExpenseAmount.Visible = true;
                    this.btnTransAmountInfo.Visible = true;
                    this.btnTransExpensInfo.Visible = true;
                }
                else
                {
                    this.lblTransAmount.Text = "Amount:";
                    this.lblTransExpense.Visible = false;
                    this.txtTransExpenseAmount.Visible = false;
                    this.btnTransAmountInfo.Visible = false;
                    this.btnTransExpensInfo.Visible = false;
                }

                //display PIP dates if per period coverage
                if (res.isPerPeriod && isLoss)
                {
                    isPerPeriod = true;
                    this.lblPIPfrom.Visible = true;
                    this.txtPIPfrom.Visible = true;
                    this.lblPIPto.Visible = true;
                    this.txtPIPto.Visible = true;
                }
                else
                {
                    isPerPeriod = false;
                    this.lblPIPfrom.Visible = false;
                    this.txtPIPfrom.Visible = false;
                    this.lblPIPto.Visible = false;
                    this.txtPIPto.Visible = false;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboPendTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mySelection = cboTransactionType.SelectedValue.ToString();
                if (mySelection == "")
                    return;
                DisableControls(true);

                mm.UpdateLastActivityTime();
                this.transactionTypeId = Convert.ToInt32(mySelection);

                /*make claimant attorney checkbox visible for losses only and defaut to checked
                 * if checked, a vendor ID is required for loss payments.  
                 * a vendor ID is always required for expense payments.*/
                if ((transactionTypeId == (int)modGeneratedEnums.TransactionType.Partial_Loss_Payment) ||
                    (transactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment) ||
                    (transactionTypeId == (int)modGeneratedEnums.TransactionType.Supplemental_Loss_Payment))
                {
                    this.chkPendClaimantAttorney.Visible = true;
                    this.chkPendClaimantAttorney.Checked = true;
                }
                else
                    this.chkPendClaimantAttorney.Visible = false;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtMailToZip_TextChanged(object sender, EventArgs e)
        {
            ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
            if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
            {
                if (this.txtMailToCity.Text.ToUpper() != lookup.zCity)
                    this.txtMailToCity.Text = lookup.zCity;
                this.cboMailToState.Text = lookup.zState;
                this.txtMailToCity.Focus();
            }
            else
                this.txtMailToZip.Focus();
        }

        protected void txtPendMailToName_TextChanged(object sender, EventArgs e)
        {
            if (this.doneLoading)
                this.userChangedMailingAddress = true;
        }

        protected void txtPendMailToZip_TextChanged(object sender, EventArgs e)
        {
          
        }

        protected void txtTransAmount_TextChanged(object sender, EventArgs e)
        {
            string num = ((TextBox)sender).Text;
            this.txtTransAmount.Text = AmountAsDollar(num);
        }

        protected void txtTransExpenseAmount_TextChanged(object sender, EventArgs e)
        {
            string num = ((TextBox)sender).Text;
            this.txtTransExpenseAmount.Text = AmountAsDollar(num);
        }

        protected void cboVoidReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            voidReasonId = Convert.ToInt32(cboVoidReason.SelectedValue);
            if (voidReasonId > 0)
            {
                btnPrint.Enabled = true;
            }
        }

       

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            Session["refererVendor"] = Request.Url.ToString();
            mm.UpdateLastActivityTime();

            string lookupValue = txtVendorSearch.Text;
            if (lookupValue == "")
                return;
            //show vendor search results
            string url = "frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
            string s2 = "window.open('" + url + "', 'popup_window4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            vendorlookup.Visible = false;
        }

        protected void txtVendorId_TextChanged(object sender, EventArgs e)
        {
            vendorId = Convert.ToInt32(txtVendorId.Text);
            if (tiDraft.Visible == true)
            {
                rbTransExpense.Enabled = true;
                rbTransLoss.Enabled = true;
                this.rbTransLoss.Checked = true;
                this.rbTransLoss_CheckedChanged(null, null);
                this.cboTransactionType.SelectedIndex = setSelection(0, this.transactionTypeId);
            }
            else
            {
                rbPendTransExpense.Enabled = true;
                rbPendTransLoss.Enabled = true;
                this.rbPendTransLoss.Checked = true;
                this.rbTransLoss_CheckedChanged(null, null);
                this.cboTransactionType.SelectedIndex = setSelection(0, this.transactionTypeId);
            }


            PopulateVendorControls();
            //else
            //{
            //    this.txtPendMailToName.Text = v.Name;
            //    this.txtPendMailToAddress1.Text = v.Address1;
            //    this.txtPendMailToAddress2.Text = v.Address2;
            //    this.txtPendMailToCity.Text = v.City;
            //    this.txtPendMailToZip.Text = v.Zipcode;
            //    this.cboPendMailToState.SelectedIndex = v.StateId + 2;
            //}
        }

        protected void rbTransLoss_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTransLoss.Checked)
            {
                //this.rbPendTransExpense.Checked = false;
                //this.rbPendTransExpense.Checked = false;
                //this.rbTransExpense.Checked = false;
                this.isLoss = true;
                DisableControls(true);
                LoadTransactionTypes();
                DisableControls(false);
                if (this.m_mode == (int)DraftDisplay.Print_Draft)
                {
                    this.cboTransactionType.Enabled = true;
                    this.cboTransactionType.SelectedIndex = -1;
                }
                else
                {
                    this.cboPendTransactionType.Enabled = true;
                    this.cboPendTransactionType.SelectedIndex = -1;
                }
            }
        }

       

        private void LoadTransactionTypes()
        {
            //if (this.m_mode == (int)DraftDisplay.Print_Draft)
            //{
            //    //if ((bool)this.rbTransLoss.Checked && (reserveLocked || claimLocked))
            //    if (reserveLocked || claimLocked)
            //    {
            //        lblTransactionTypes.Visible = true;
            //        lblTransactionTypes.Text = "Only Expense Payments are Allowed because this Claim is Locked";
            //        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Locked! Only Expense Payments are allowed.');</script>");
            //        this.rbTransExpense.Checked = true;
            //        rbTransExpense_CheckedChanged(null, null);
                    
            //    }
            //}
            //else
            //{
            //    //if ((bool)this.rbPendTransLoss.Checked && (reserveLocked || claimLocked))
            //    if (reserveLocked || claimLocked)
            //    {

            //        lblTransactionTypes.Visible = true;
            //        lblTransactionTypes.Text = "Only Expense Payments are Allowed because this Claim is Locked";

            //        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Locked! Only Expense Payments are allowed.');</script>");
            //        this.rbPendTransExpense.Checked = true;
            //        rbTransExpense_CheckedChanged(null, null);
                   
            //    }
            //}

            string whereClause;



            if (res.isOpen)
                whereClause = "Is_Payment_Type = 1";
            else
                whereClause = "Is_Supplemental_Payment_Type = 1";

            if ((bool)this.rbTransLoss.Checked)
                whereClause += " and Is_Loss = 1";
            else
                whereClause += " and Is_Loss = 0";

          



            if (this.m_mode == (int)DraftDisplay.Print_Draft)
            { 
                this.cboTransactionType.DataSource = LoadComboBox(this.cboTransactionType, false, whereClause);
                this.cboTransactionType.DataTextField = "description";
                this.cboTransactionType.DataValueField = "id";
                this.cboTransactionType.DataBind();

                this.cboTransactionType.Items.Insert(0, "");
            }
            else
            { 
                this.cboPendTransactionType.DataSource = LoadComboBox(this.cboPendTransactionType, false, whereClause);
                this.cboPendTransactionType.DataTextField = "description";
                this.cboPendTransactionType.DataValueField = "id";
                this.cboPendTransactionType.DataBind();
                this.cboPendTransactionType.Items.Insert(0, "");
            }

        }

        private IEnumerable LoadComboBox(DropDownList cb, bool includeZeros, string whereClause)
        {
            myTransactionTypeList = new List<comboBoxGrid>();
            try
            {
                foreach (Hashtable tData in TypeTable.getTypeTableRows("Transaction_Type", whereClause))
                {
                    comboBoxGrid cg = new comboBoxGrid();
                    cg.id = (int)tData["Id"];
                    cg.imageIdx = (int)tData["Image_Index"];
                    cg.description = (string)tData["Description"];
                    if (includeZeros || cg.id != 0)
                        myTransactionTypeList.Add(cg);
                }
                return myTransactionTypeList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myTransactionTypeList;
            }
        }

        private void DisableControls(bool enable)
        {
            if (m_mode == (int)DraftDisplay.Print_Draft)
            {
                //rbTransExpense.Enabled = enable;
                //rbTransLoss.Enabled = enable;
                this.cboTransactionType.Enabled = enable;
                //this.txtTaxId.Enabled = enable;
                this.txtPayee.Enabled = enable;
                this.txtPIPfrom.Enabled = enable;
                this.txtPIPto.Enabled = enable;
                this.txtExplanation.Enabled = enable;
                this.chkClaimantAttorney.Enabled = enable;
                this.btnPrint.Enabled = enable;
                this.txtTransAmount.Enabled = enable;
                this.txtTransExpenseAmount.Enabled = enable;
                this.btnTransAmountInfo.Enabled = enable;
                this.btnTransExpensInfo.Enabled = enable;
                //this.btnVendorLookup.Enabled = enable;
                this.gbLossPayee.Disabled = !enable;
                //this.gbMedicalLien.Visible = enable;
                this.gbMailTo.Disabled = !enable;
                this.txtMailToName.Enabled = enable;
                txtMailToAddress1.Enabled = enable;
                txtMailToAddress2.Enabled = enable;
                txtMailToCity.Enabled = enable;
                cboMailToState.Enabled = enable;
                txtMailToZip.Enabled = enable;
                this.gbPendMailTo.Disabled = !enable;
                this.btnOK.Visible = false;
                this.btnPrint.Visible = true;
                this.chkTotalLoss.Enabled = enable;
            }
            else
            {
                this.txtPendSentTo.Enabled = enable;
                this.cboPendTransactionType.Enabled = enable;
                this.txtPendTaxId.Enabled = enable;
                this.txtPendPayee.Enabled = enable;
                this.txtPendExplanation.Enabled = enable;
                this.chkPendClaimantAttorney.Enabled = enable;
                this.btnPendVendorLookup.Enabled = enable;
                this.gbPendMailTo.Disabled = !enable;
                this.chkTotalLoss.Enabled = enable;
                this.btnOK.Visible = true;
                this.btnPrint.Visible = false;
                //Thickness margin = this.btnCancel.Margin;
                //margin.Left = 65;
                //this.btnCancel.Margin = margin;
            }
        }

        private Hashtable eventTypeIds = null,    //event type IDs to fire
            expenseEventTypeIds = null;           //event type IDs to fire for legal expenses
        private PendingDraft _pendingDraft;

        private List<comboBoxGrid> myTransactionTypeList;
        private int m_mode;
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

      
        private void PopulateReissue()
        {
            if (this.reissueFromTransactionId == 0)
                return;     //not from a reissue

            Transaction origTrans = new Transaction(reissueFromTransactionId);
            reissuedFromDraftId = origTrans.DraftId;
            origDraft = new Draft(reissuedFromDraftId);
            Vendor origVendor = new Vendor(origDraft.VendorId);

            /*if original draft has final or partial loss payment transaction type 
             * and now the reserve line is closed, change transaction type to 
             * supplemental loss payment*/
            if ((res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed) &&
                ((origTrans.TransactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment) ||
                (origTrans.TransactionTypeId == (int)modGeneratedEnums.TransactionType.Partial_Loss_Payment)))
                origTrans.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Supplemental_Loss_Payment;

            //update inputs on screen
            this.txtTaxId.Text = origVendor.TaxId;
            this.txtPayee.Text = origDraft.Payee;
            this.txtPIPfrom.Text = origDraft.PipDateFrom.ToShortDateString();
            this.txtPIPto.Text = origDraft.PipDateTo.ToShortDateString();
            this.txtExplanation.Text = origDraft.ExplanationOfProceeds;
            this.txtMailToName.Text = origDraft.MailToName;
            this.txtMailToAddress1.Text = origDraft.Address1;
            this.txtMailToAddress2.Text = origDraft.Address2;
            this.txtMailToCity.Text = origDraft.City;
            this.txtMailToZip.Text = origDraft.Zipcode;
            this.txtTransAmount.Text = origDraft.DraftAmount.ToString();

            this.cboTransactionType.SelectedIndex = setSelection(1, 0, origTrans.TransactionType().Description);
            //cboTransactionType_SelectedIndexChanged(null, null);
            //txtTaxId_TextChanged(null, null);
            this.cboMailToState.SelectedValue = mm.GetStateForId(origDraft.StateId);

            if (Vendor.getVendorTypeIdForVendorId(origVendor.Id) == (int)modGeneratedEnums.VendorType.Claimant_Attorney)
                this.chkClaimantAttorney.Checked = true;
            else
                this.chkClaimantAttorney.Checked = false;
            btnPrint.Enabled = true;

           
        }


        private int setSelection(int mode, int idx, string description = "")
        {
            int cbIdx = 0;
            switch (mode)
            {
                case 1:
                    foreach (comboBoxGrid cb in myTransactionTypeList)
                    {
                        if (description == cb.description)
                            return cbIdx;
                        cbIdx++;
                    }
                    return 0;
                default:  //mode 0:
                    foreach (comboBoxGrid cb in myTransactionTypeList)
                    {
                        if (idx == cb.id)
                            return cbIdx;
                        cbIdx++;
                    }
                    return 0;
            }
        }

        private void PopulateMedicalLien()
        {
            //default the lien group box to hidden
            this.gbMedicalLien.Visible = false;

            Injured inj = this.res.Injured();
            if (inj == null)
                return;     //no injured person on this reserve

            string lienDescription = inj.LiensDescription;
            if (lienDescription == "")
                return;     //there are no liens

            this.gbMedicalLien.Visible = true;
            this.txtMedicalLien.Text = lienDescription;
        }

        private void PopulateDeductible()
        {
            this.gbDeductible.Visible = false;

            double deductibleAmount = 0;

            PolicyCoverage pc = res.PolicyCoverage();
            if (pc == null)
                return;

            deductibleAmount = pc.DeductibleAmount;

            if (deductibleAmount > 0)
            {
                this.gbDeductible.Visible = true;
                this.txtDeductible.Text = deductibleAmount.ToString("c");
            }
        }

        private void PopulateLossPayee()
        {
            //default loss payee group box to hidden
            this.gbLossPayee.Visible = false;

            string lossPayee = "";
            int reserveTypeId = res.ReserveTypeId;

            if ((reserveTypeId != (int)modGeneratedEnums.ReserveType.Collision) &&
                (reserveTypeId != (int)modGeneratedEnums.ReserveType.Comprehensive) &&
                (reserveTypeId != (int)modGeneratedEnums.ReserveType.Comp_Collision))
            {
                compOrCollision = false;
                return;
            }
            else
                compOrCollision = true;

            this.gbLossPayee.Visible = true;
            this.chkLossPayee.Text = "I Have Investigated for Loss Payees who were Reported to Underwriting Prior to Loss.";

            if (claim.InsuredVehicleId != 0)
            {
                Vehicle veh = claim.InsuredVehicle;
                if (veh.LossPayeeName1 != "")
                {   //we have a loss payee for insured vehicle
                    this.chkLossPayee.Text = "I Have Reviewed Loss Payee:";
                    lossPayee = veh.LossPayeeName1;
                    this.lossPayeeName = veh.LossPayeeName1;
                    if (veh.LossPayeeName2 != "")
                        lossPayee += Environment.NewLine + veh.LossPayeeName2;

                    if (veh.LossPayeeAddress1 != "")
                        lossPayee += Environment.NewLine + veh.LossPayeeAddress1;

                    if (veh.LossPayeeAddress2 != "")
                        lossPayee += Environment.NewLine + veh.LossPayeeAddress2;

                    lossPayee += Environment.NewLine + veh.LossPayeeCity + ", " + veh.LossPayeeState.Abbreviation + " " + veh.LossPayeeZipcode;
                }
            }
            this.txtLossPayee.Text = lossPayee;
        }

        private void CheckReserveClaimLocks()
        {
            reserveLocked = res.FirstLockId != 0;

            claimLocked = claim.FirstLockId() != 0;
        }

        protected void btnVendorLookup_Click(object sender, EventArgs e)
        {

            vendorlookup.Visible = true;
        }

        class comboBoxGrid
        {
            public int id { get; set; }
            public string description { get; set; }
            public int imageIdx { get; set; }
        }
    }
}
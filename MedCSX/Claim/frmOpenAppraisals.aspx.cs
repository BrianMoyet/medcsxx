﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmOpenAppraisals : System.Web.UI.Page
    {
        private string AppraiserInfo;

        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

               if (!Page.IsPostBack)
               { 
                 RefreshAppraisers();
               }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void RefreshAppraisers()
        {
           // mm.CompleteAppraisalTasks();

            this.grAppraisers.DataSource = mm.GetOpenAppraisals();
            this.grAppraisers.DataBind();

            if (!Page.IsPostBack)
            { 
                this.grAppraisers.SelectedIndex = 0;
            }

            LoadAppraisals(0);


        }

        private void UpdateAppraisals()
        {
            //get closed appraisals
            DataTable closedAppraisals = mm.GetClosedAppraisals();
            int count = 0;
            foreach (DataRow row in closedAppraisals.Rows)
            {
                if ((DateTime)row["Appraiser_Response_Date"] == default(DateTime))
                {
                    UpdateResponseDate((int)row["appraisal_task_id"]);
                    count++;
                }
                if (count > 10000)
                    break;
            }
        }

        private void UpdateResponseDate(int appraisal_id)
        {
            AppraisalTask App = new AppraisalTask(appraisal_id);
            Vehicle veh = new Vehicle(App.VehicleId);
            string vehicle_name = string.Format("{0} {1} {2}", veh.YearMade, veh.Manufacturer, veh.Model);
            string[] veh_name = vehicle_name.Split(' ');
            bool foundit = false;
            DateTime dateRec = default(DateTime);

            IEnumerable IRDocs = mf.getIRDocumentsByClaim(App.ClaimNumber, mm.ClaimsDrawer);
            foreach (MedJames.ServiceWrappers.ImageRight.Model.Document IRDoc in IRDocs)
            {
                if (IRDoc.TypeCode.ToString() == "APPR" || IRDoc.TypeCode.ToString() == "PHOT")
                {
                    //List<MedJames.ServiceWrappers.ImageRight.Interfaces.IPage> IRPages = mf.getIRPagesByDoc(IRDoc.ReferenceId);
                    IEnumerable IRPages = mf.getIRPagesByDoc(IRDoc.ReferenceId);
                    foreach (MedJames.ServiceWrappers.ImageRight.Model.Page IRPage in IRPages)
                    {
                        string s = IRPage.Description;
                        for (int i = 0; i < veh_name.Length; i++)
                        {
                            foundit = false;
                            if (s.IndexOf(veh_name[i]) > 0)
                                foundit = true;
                            else
                                break;
                        }
                        if (foundit)
                            dateRec = IRDoc.DateCreated.AddHours(-5);
                    }
                }
                if (foundit)
                    break;
            }

            if (dateRec > default(DateTime))
            {
                App.AppraiserResponseDate = dateRec;
                App.AppraisalTaskStatusId = (int)modGeneratedEnums.AppraisalTaskStatus.Appraisal_Completed;
                App.Update();
            }
        }

        protected void btnOpenClaim_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                int cid = 0;

                for (int i = 0; i < grAppraisals.Rows.Count; i++)
                {
                    GridViewRow row = grAppraisals.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                    if (isChecked)
                    {
                        cid = Convert.ToInt32(Regex.Replace(grAppraisals.Rows[i].Cells[2].Text, "[^0-9]", ""));
                       

                    }
                }


               
                if (cid == 0)
                    return;

                OpenClaim(cid);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadAppraisals(int p)
        {
            this.grAppraisals.DataSource = mm.GetOpenAppraisals((int)grAppraisers.SelectedDataKey.Value);
            this.grAppraisals.DataBind();
         
            return true;
        }

        public void OpenClaim(int claimId, int fileNoteId = 0, int reserveId = 0, int transactionId = 0, bool isClaimNew = false)
        {
            try
            {
                if (claimId == 0)
                    return; //claim ID is invalid

                if (claimId > int.Parse(ModMain.MAX_CLAIM_ID))
                {
                    string str_claimId = claimId.ToString();
                    claimId = int.Parse(str_claimId.Substring(str_claimId.Length - ModMain.MAX_CLAIM_ID.Length, ModMain.MAX_CLAIM_ID.Length));
                }

                MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(claimId);
                //if (!mm.ComanyAllowedForUser(myClaim.CompanyLocation().CompanyId))
                //{    //user not allowed to open this claim
                //    MessageBox.Show("you are not authorized to open " + myClaim.CompanyLocation().Company.Description + " claims.", "Company Not Authorized", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                //    return;
                //}

                bool ok2OpenClaim = false;
                foreach (Alert urgentAlert in mm.urgentAlerts)
                {
                    if (urgentAlert.ClaimId == claimId)
                        ok2OpenClaim = true;
                }

                if (!ok2OpenClaim)
                {    /*claim to open wasn't in Urgent Alert IDs list
                     * see if urgent alerts should be ignored - have they been cleared/responded to?*/
                    foreach (Alert urgentAlert in mm.urgentAlerts)
                    {
                        if (!urgentAlert.AlertsClearedForClaim || !urgentAlert.hasRepliedToAlert)
                        {
                            string msg = "You cannot open this claim until you have dealt with the urgen alert on claim ";
                            msg += urgentAlert.Claim.DisplayClaimId + ".  Before continuing,you have to:" + Environment.NewLine + Environment.NewLine;
                            msg += "1) Clear all your alerts for this claim." + Environment.NewLine;
                            msg += "2) Create a file note for this claim, sending an alert to the urgent alert sender.";

                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg +"');</script>");
                            //System.Windows.MessageBox.Show(msg, "Urgent Alerts", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                            return;
                        }
                    }
                    mm.urgentAlerts = new ArrayList();  //all urgent alerts have been handled

                    if (myClaim.AdjusterId == 0)  //check adjuster ID
                    {
                        foreach (Reserve res in myClaim.Reserves())
                        {
                            if (res.AdjusterId != 0)
                            {
                                myClaim.AdjusterId = res.AdjusterId;
                                myClaim.Update();
                                break;
                            }
                        }
                    }
                }

                if (ml.CurrentUser.AutoOpenImageRightClaims)
                {
                    //if (myClaim == null)  //claim object is null - create one
                    //  MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(claimId);

                    try
                    {
                        mf.getIRDocumentByClaim(myClaim.DisplayClaimId, mf.DetermineClaimsImageRightDrawer(myClaim.CompanyLocation().CompanyId));
                    }
                    catch (Exception ex) { }
                }

                string url = "ViewClaim.aspx?claim_id=" + claimId;
                string win = "window.open('" + url + "','mywindow" + claimId + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100').focus();";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
                //frmViewClaim f;
                //if (mm.ClaimWindows.ContainsKey(claimId))
                //{    //claim is open in another window
                //     /*f = (frmViewClaim)mm.ClaimWindows(claimId);
                //       f.Focus();*/
                //    return;
                //}

                //open claim window
                //f = new frmViewClaim(claimId);
                //Thread newClaim = new Thread(() =>
                //{
                //    f = new frmViewClaim(claimId);
                //    f.isNew = isClaimNew;

                //    if (fileNoteId != 0)
                //        f.fileNoteId = fileNoteId;  //set the file note ID

                //    if (reserveId != 0)
                //        f.reserveId = reserveId;  //set the reserve ID


                //    if (transactionId != 0)
                //        f.transactionId = transactionId;  //set the transaction ID

                //    mm.ClaimWindows.Add(claimId, f); //add the claim window to active windows

                //    f.Show();
                //    f.Closed += (sender1, e1) => f.Dispatcher.InvokeShutdown();

                //    //System.Threading.Dispatcher.Run();
                //});
                //newClaim.SetApartmentState(ApartmentState.STA);
                //newClaim.IsBackground = true;
                //newClaim.Start();


                //f.Show();  //display the form

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnOpenVehicle_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                int cid = 0;
                int vid = 0;

               

                for (int i = 0; i < grAppraisals.Rows.Count; i++)
                {
                    GridViewRow row = grAppraisals.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                    if (isChecked)
                    {
                        cid = Convert.ToInt32(Regex.Replace(grAppraisals.Rows[i].Cells[2].Text, "[^0-9]", ""));
                        vid = Convert.ToInt32(grAppraisals.Rows[i].Cells[6].Text);
             
                    }
                }

                if (vid == 0)
                    return;


                string url = "frmVehicles.aspx?mode=u&claim_id=" + cid + "&vehicle_id=" + vid;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnAppraisalComplete_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

               

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Mark_Appraisals_as_Completed))
                    return;     //user not authorized to complete

                //cboGrid selectedAppraiser = (cboGrid)grAppraisers.SelectedItem;
                //Convert.ToInt32(grAppraisals.DataKeys[row.RowIndex].Value)
                //int openAppraisals = int.Parse(selectedAppraiser.type);

                for (int i = 0; i < grAppraisals.Rows.Count; i++)
                {
                    GridViewRow row = grAppraisals.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                    if (isChecked)
                    {
                        mm.CompleteAppraisals(Convert.ToInt32(grAppraisals.DataKeys[row.RowIndex].Value));
                        //openAppraisals--;

                    }
                }

        

                //selectedAppraiser.type = openAppraisals.ToString();

                RefreshAppraisers();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnCancelAppraisal_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Cancel_Appraisal))
                    return;     //user not authorized to complete task

                int ati = 0;
                for (int i = 0; i < grAppraisals.Rows.Count; i++)
                {
                    GridViewRow row = grAppraisals.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                   
                    if (isChecked)
                    {
                        ati = Convert.ToInt32(grAppraisals.DataKeys[row.RowIndex].Value);
       

                    }
                }

                if (ati == 0)
                    return;

                Session["referer2"] = Request.Url.ToString();

                string url = "frmCancelAppraisals.aspx?mode=u&appraisal_task_id=" + ati;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=300, copyhistory=no, left=600, top=300');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
               

               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    IList items = (IList)grAppraisals.SelectedItems;
            //    IEnumerable<AppraisalGrid> mySelection = items.Cast<AppraisalGrid>();
            //    if (mySelection == null)
            //        return;     //nothing selected

            //    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Cancel_Appraisal))
            //        return;     //user not authorized to complete task

            //    foreach (AppraisalGrid selection in mySelection)
            //    {
            //        frmCancelAppraisal f = new frmCancelAppraisal(selection.id);
            //        f.ShowDialog();
            //    }

            //    RefreshAppraisers();        //reload appraisal data
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void btnIRRefresh_Click(object sender, EventArgs e)
        {
            string cid = "";
            for (int i = 0; i < grAppraisals.Rows.Count; i++)
            {
                GridViewRow row = grAppraisals.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {
                    cid = grAppraisals.Rows[i].Cells[2].Text;


                }
            }


            if (cid == "")
                return;

            mf.getIRDocumentByClaim(cid.ToString(), mm.ClaimsDrawer);
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Resend_Appraisal_Emails))
                return;     //user is not authorized to complete

            //TODO
            //if (MessageBox.Show("Are you sure you want to resend appraisal emails?",
            //    "Confirm Resend", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
            //    return;     //confirm user wants to send emails



            int counter = 0;

            for (int i = 0; i < grAppraisals.Rows.Count; i++)
            {
                GridViewRow row = grAppraisals.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {

                    try
                    {
                        AppraisalTask t = new AppraisalTask(Convert.ToInt32(grAppraisals.DataKeys[row.RowIndex].Value));
<<<<<<< HEAD
                        //string appraisalBody = t.AppraisalEmailBody(mm.APPRAISAL_TEMPLATE);
                        t.SendAppraisalEmail();
=======
                        string appraisalBody = t.AppraisalEmailBody(mm.APPRAISAL_TEMPLATE);
                        t.SendAppraisalEmail(appraisalBody);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                        counter++;
                    }
                    catch (Exception ex)
                    {

                        mf.ProcessError(ex);
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Error sending emails" + ex.ToString() + "');</script>");
                    }

                }
            }



            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + counter + " appraisal emails successfully resent.');</script>");
            //MessageBox.Show(counter + " appraisal emails successfully resent.", "Emails Sent", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

            RefreshAppraisers();    //reload appraiser data
            
        }

        class AppraisalGrid
        {
            public int id { get; set; }
            public DateTime dtAssigned { get; set; }
            public int claim_id { get; set; }
            public string adjuster_id { get; set; }
            public int vehicle_id { get; set; }
            public string taskDescrip { get; set; }
            public int imageIdx { get; set; }
          
            public string vehicleName
            {
                get
                {
                    Vehicle v = new Vehicle(vehicle_id);
                    return v.Name;
                }
            }
            public string displayClaim
            {
                get
                {
                    MedCsxLogic.Claim c = new MedCsxLogic.Claim(claim_id);
                    return c.DisplayClaimId;
                }
            }
        }

        protected void grAppraisers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                int mySelection = (int)grAppraisers.SelectedDataKey.Value;
               

                bool appraisalsLoaded = LoadAppraisals(mySelection);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmFindPolicy : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();
        public DateTime dateOfLoss = default(DateTime);     //if associateWithNewPolicy, then this is the old and new Date of Loss chosen.  IO Parameter

        public string policyNo = "",        //if associateWithNewPolicy, then this is the new Policy No chosen. Output Parameter
                     versionNo = "";        //if associateWithNewPolicy, then this is the new Version No chosen. Output Parameter
        public bool okPressed = false,        //if associateWithNewPolicy, OK = true, Cancel = false
                    isExpired = false,          //active policies = true, expired policies = false. Output Parameter
                    formLoaded = false,
                    LOBLoaded = false;
        public int Company_Id = 0;

        private bool currentlySearching = false;        //currently searching for policies
        private List<Hashtable> activePolicies = null,
                                expiredPolicies = null,
                                allPolicies = null;

      

        private bool associateWithNewPolicy = false;    //is this form called from the claim form?

        private SubClaimType mySubClaimType = null;     //type of claim being created/edited
        private const bool searchUsingBackgroundWorker = false;     //is search using background worker?
        private Thread searchThread = null;     //thread to search for policies
        private BackgroundWorker bwPolicySearch = null;     //background worker to search for policies
        //private DispatcherTimer SearchTimer = new DispatcherTimer();

        private List<PolicyGrid> myPolicyList;      //repository for Policy grid
        private List<ClaimantGrid> myIndividualList;      //repository for Individual grid
        private List<VehicleGrid> myVehicleList;        //repository for Vehicle grid
        private List<Hashtable> myDataRows;         //repository for results from database
        delegate void updateCallback();
        public string SelectedPolicy = "";
        public string SelectedVersion = "";

        private List<DuplicateClaims> myDuplicateClaims;
        private int claimId;
        private DateTime dol;   //Date of loss
        int dupCount = 0;
        string referer = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["referer"] != null)
            {
                referer = Session["referer"].ToString();
            }

            if (Request.QueryString["mode"] != null)
            {
                this.dateOfLoss = Convert.ToDateTime(Request.QueryString["date_of_loss"]);
                this.Company_Id = mm.GetCompanyId(Convert.ToInt32(Request.QueryString["CompanyLocationId"]));
                this.associateWithNewPolicy = true;
                this.mySubClaimType = new SubClaimType(Convert.ToInt32(Request.QueryString["SubClaimTypeId"]));
                this.claimId = Convert.ToInt32(Request.QueryString["claim_id"]);
            }
            else
            {
                this.mySubClaimType = new SubClaimType(Convert.ToInt32(Request.QueryString["subClaimTypeID"]));
            }
            //string tmpsubClaimTypeID = Request.QueryString["subClaimTypeID"].ToString();

            if (!Page.IsPostBack)
            {
                this.Load_Form();
                btnPolicyOK.Visible = false;
            }

          

                  

           
        }

        private void SearchForPolicies()
        {

            //if (btnPolicySearch.Dispatcher.CheckAccess() == false)
            //{
            //    updateCallback uCallBack = new updateCallback(SearchForPolicies);
            //    this.Dispatcher.Invoke(uCallBack);
            //}
            //else
            //{
                //find active policies
                this.activePolicies = ml.FindPolicies(true, this.tbPolicyDOL.Text,
                    this.tbPolicyNum.Text, this.tbPolicyFName.Text, this.tbPolicyLName.Text,
                    this.tbPolicySSN.Text, this.tbPolicyVehicleYear.Text,
                    this.tbPolicyVehicleMake.Text, this.tbPolicyVehicleModel.Text,
                    this.tbPolicyVehicleVIN.Text, this.tbPolicyAddress.Text,
                    this.tbPolicyCity.Text, this.cboPolicyState.Text,
                    this.tbPolicyZipCode.Text);

                ArrayList activePolicyNumbers = new ArrayList();
                foreach (Hashtable row in activePolicies)
                    activePolicyNumbers.Add(row["Policy"]);

                //find expired policies
                this.expiredPolicies = ml.FindPolicies(false, this.tbPolicyDOL.Text,
                    this.tbPolicyNum.Text, this.tbPolicyFName.Text, this.tbPolicyLName.Text,
                    this.tbPolicySSN.Text, this.tbPolicyVehicleYear.Text,
                    this.tbPolicyVehicleMake.Text, this.tbPolicyVehicleModel.Text,
                    this.tbPolicyVehicleVIN.Text, this.tbPolicyAddress.Text,
                    this.tbPolicyCity.Text, this.cboPolicyState.Text,
                    this.tbPolicyZipCode.Text, activePolicyNumbers);

                this.currentlySearching = false;

            DisplayPolicies();
            //if (!searchUsingBackgroundWorker)
            //    {
            //        if (this.searchThread != null)
            //        {
            //            this.searchThread.Abort();
            //            this.searchThread = null;
            //        }

            //        DisplayPolicies();
            //    }

            //}
        }

        private void DisplayPolicies()
        {

            this.showActivePolicies.Text = "Show Active Policies (" + activePolicies.Count + ")";

            //update the number of expired policies
            this.showExpiredPolicies.Text = "Show Expired Polices (" + expiredPolicies.Count + ")";

            if (activePolicies.Count == 0)
                lblInvalidSearch.Text = "No Active Policies Found.";
            // update the number of active policies
            //this.showActivePolicies.Text = "Show Active Policies (" + activePolicies.Count + ")";

            ////update the number of expired policies
            //this.showExpiredPolicies.Text = "Show Expired Polices (" + expiredPolicies.Count + ")";
            if (!isExpired)
            { 
                if (activePolicies.Count == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('No Active Policies Found.');</script>");
                    // MessageBox.Show("No Active Policies Found.", "No Policies Found", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            ShowActivePolicies();

            this.ResetSearch();
        }

        private void ShowActivePolicies()
        {
            try
            {
                if (activePolicies == null)
                    return;

             

                //this.gbAutoPolicies.Header = activePolicies.Count.ToString() + " Active Policies Found";
                isExpired = false;

                if (activePolicies.Count == 0)
                {   //no active policies
                    this.grPolicyIndividuals.DataSource = new List<ClaimantGrid>();
                    this.grPolicyIndividuals.DataBind();
                    this.grPolicyVehicles.DataSource = new List<VehicleGrid>();
                    this.grPolicyVehicles.DataBind();
                    btnPolicyOK.Enabled = false;
                    btnPolicyOK.Visible = false;
                    btnUsePolicyOK.Enabled = false;
                    btnUsePolicyOK.Visible = true;
                    return;
                }
                else
                {   //active policies found
                    btnPolicyOK.Enabled = false;
                    btnPolicyOK.Visible = false;
                    btnUsePolicyOK.Enabled = true;
                    btnUsePolicyOK.Visible = true;
                }

                if (Request.QueryString["mode"] != null)
                {
                    btnPolicyOK.Enabled = true;
                    btnPolicyOK.Visible = true;
                    btnUsePolicyOK.Enabled = false;
                    btnUsePolicyOK.Visible = false;
                }

                if (activePolicies.Count >= 1000)
                { 
                    lblInvalidSearch.Visible = true;
                    lblInvalidSearch.Text = "Greater than 1000 Policies Found.  Only the First 1000 Will be Displayed.";
                }

                bool policiesLoaded = LoadPolicies();   //Load the policies to the grid
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
               
            }
        }

        private void ShowExpiredPolicies() 
        {
            try
            {
                SearchForPolicies();

                if (expiredPolicies == null)
                    return;

                
                //gbAutoPolicies.Header = expiredPolicies.Count.ToString() + " Expired Policies Found";
                isExpired = true;

                if (expiredPolicies.Count == 0)
                {   //no expired policies found
                    this.grPolicySearch.DataSource = new List<PolicyGrid>();
                    this.grPolicySearch.DataBind();
                    this.grPolicyIndividuals.DataSource = new List<ClaimantGrid>();
                    this.grPolicyIndividuals.DataBind();
                    this.grPolicyVehicles.DataSource = new List<VehicleGrid>();
                    this.grPolicyVehicles.DataBind();
                    this.btnPolicyOK.Enabled = false;
                    this.btnPolicyOK.Visible = false;
                    this.btnUsePolicyOK.Enabled = false;
                    this.btnUsePolicyOK.Visible = true;
                    return;
                }
                else
                {   //expired policies found
                    this.btnPolicyOK.Enabled = false;
                    this.btnPolicyOK.Visible = false;
                    this.btnUsePolicyOK.Enabled = true;
                    this.btnUsePolicyOK.Visible = true;
                }

                if (Request.QueryString["mode"] != null)
                {
                    btnPolicyOK.Enabled = true;
                    btnPolicyOK.Visible = true;
                    btnUsePolicyOK.Enabled = false;
                    btnUsePolicyOK.Visible = false;
                }

                bool expiredPoliciesLoaded = LoadPolicies();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
              
            }
        }

        private bool LoadPolicies()
        {
            if (isExpired)
            { 
                this.grPolicySearch.DataSource = LoadPolicies(expiredPolicies);
                this.grPolicySearch.DataBind();
                this.grPolicySearch.SelectedIndex = 0;
                SelectedPolicy = grPolicySearch.SelectedRow.Cells[1].Text;
                grPolicySearch_SelectedIndexChanged(null, null);
                //this.cboPolicyCompany.Text = Company.getCompanyForPolicy(SelectedPolicy);
                cboPolicyCompany_SelectedIndexChanged(cboPolicyCompany, EventArgs.Empty);
            }
            else
            { 
                this.grPolicySearch.DataSource = LoadPolicies(activePolicies);
                this.grPolicySearch.DataBind();
                this.grPolicySearch.SelectedIndex = 0;
                SelectedPolicy = grPolicySearch.SelectedRow.Cells[1].Text;
                grPolicySearch_SelectedIndexChanged(null, null);
                //this.cboPolicyCompany.Text = Company.getCompanyForPolicy(SelectedPolicy);
                cboPolicyCompany_SelectedIndexChanged(cboPolicyCompany, EventArgs.Empty);


            }
            return true;
        }


        private void ResetSearch()
        {
            //this.lblSearchStatus.Content = "";
            //this.pbSearchProgress.Visibility = Visibility.Collapsed;
            this.btnPolicySearch.Enabled = true;
            //this.cmdSearch.Text = "Search";
        }

        protected void btnPolicySearch_Click(object sender, EventArgs e)
        {
            btnUsePolicyOK.Enabled = false;
            formLoaded = false;
            LOBLoaded = false;

            try
            {
                mm.UpdateLastActivityTime();

                     //search for policies
                        //trim text boxes for data entered
                        this.tbPolicyDOL.Text = this.tbPolicyDOL.Text.Trim();
                        this.tbPolicyNum.Text = this.tbPolicyNum.Text.Trim();
                        this.tbPolicyAddress.Text = this.tbPolicyAddress.Text.Trim();
                        this.tbPolicyFName.Text = this.tbPolicyFName.Text.Trim();
                        this.tbPolicyLName.Text = this.tbPolicyLName.Text.Trim();
                        this.tbPolicySSN.Text = this.tbPolicySSN.Text.Trim();
                        this.tbPolicyVehicleYear.Text = this.tbPolicyVehicleYear.Text.Trim();
                        this.tbPolicyVehicleMake.Text = this.tbPolicyVehicleMake.Text.Trim();
                        this.tbPolicyVehicleModel.Text = this.tbPolicyVehicleModel.Text.Trim();
                        this.tbPolicyVehicleVIN.Text = this.tbPolicyVehicleVIN.Text.Trim();
                        this.tbPolicyCity.Text = this.tbPolicyCity.Text.Trim();
                        this.tbPolicyZipCode.Text = this.tbPolicyZipCode.Text.Trim();

                        if (!ValidateSearchData())
                            return;

                //provide feedback - let user know we are searching and allow them to stop the search

                SearchForPolicies();
                        ////search for policies
                        //this.currentlySearching = true;
                        //if (searchUsingBackgroundWorker)
                        //{   //background worker
                        //    bwPolicySearch = new BackgroundWorker();
                        //    bwPolicySearch.WorkerSupportsCancellation = true;
                        //    bwPolicySearch.WorkerReportsProgress = true;
                        //    bwPolicySearch.RunWorkerAsync();
                        //}
                        //else
                        //{   //thread


                //    this.searchThread = new Thread(SearchForPolicies);
                //    searchThread.Priority = ThreadPriority.Lowest;
                //    searchThread.Start();
                //}


            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool ValidateSearchData()
        {
            //check for date of loss
            if (!ValidDateOfLoss())
                return false;

            if ((this.tbPolicyNum.Text == "") && (this.tbPolicyFName.Text == "") &&
                (this.tbPolicyLName.Text == "") && (this.tbPolicySSN.Text == "") &&
                (this.tbPolicyVehicleYear.Text == "") && (this.tbPolicyVehicleMake.Text == "") &&
                (this.tbPolicyVehicleModel.Text == "") && (this.tbPolicyVehicleVIN.Text == "") &&
                (this.tbPolicyAddress.Text == "") && (this.tbPolicyCity.Text == "") &&
                (this.cboPolicyState.Text == "") && (this.tbPolicyZipCode.Text == ""))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter at Least One Search Field Other than the Date of Loss');</script>");
                //MessageBox.Show("You Must Enter at Least One Search Field Other than the Date of Loss", "No Search Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                return false;
            }
            return true;    //valid search data
        }

        private bool ValidDateOfLoss()
        {
            if (!ml.isDate(this.tbPolicyDOL.Text))
            {   //DOL must be date
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter a Valid Date of Loss');</script>");
                //MessageBox.Show("You Must Enter a Valid Date of Loss", "Invalid Date of Loss", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                if (this.tbPolicyDOL.Visible == true)
                    this.tbPolicyDOL.Focus();
                return false;
            }
            else if (DateTime.Parse(this.tbPolicyDOL.Text).Year < 1900)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You Must Enter a Valid Date of Loss');</script>");
                //MessageBox.Show("You Must Enter a Valid Date of Loss", "Invalid Date of Loss", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                if (this.tbPolicyDOL.Visible == true)
                    this.tbPolicyDOL.Focus();
                return false;
            }
            else if (DateTime.Parse(this.tbPolicyDOL.Text) > DateTime.Now.Date)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Date of Loss Must Not be in the Future');</script>");
                //MessageBox.Show("Date of Loss Must Not be in the Future", "Invalid Date of Loss", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                if (this.tbPolicyDOL.Visible == true)
                    this.tbPolicyDOL.Focus();
                return false;
            }
            return true;    //valid date of loss
        }

        protected void cboPolicyCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.LoadCompanyLocations();    //load the company locations

                /*if company selected is different than the company on policy
                 * don't allow user to use this policy to create a claim*/
                if (tbPolicyNum.Text.Length > 0)
                { 
                    if (this.cboPolicyCompany.Text == Company.getCompanyForPolicy(grPolicySearch.SelectedRow.Cells[1].Text))
                    {
                        this.btnUsePolicyOK.Enabled = true;
                        this.btnUsePolicyOK.Visible = true;
                        this.btnPolicyOK.Enabled = false;
                        this.btnPolicyOK.Visible = false;
                    }
                    else
                    {
                        this.btnUsePolicyOK.Enabled = false;
                        this.btnUsePolicyOK.Visible = true;
                        this.btnPolicyOK.Enabled = false;
                        this.btnPolicyOK.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        //private string SelectedPolicy
        //{
        //    get
        //    {
        //        return grPolicySearch.SelectedRow.Cells[1].Text;
        //        //PolicyGrid mySelection = (PolicyGrid)grPolicySearch.SelectedItem;
        //        //return mySelection == null ? "" : mySelection.policyNo;
        //    }
        //}

        private void LoadCompanyLocations()
        {
            try
            {
                string[] companyData;
                string txtCompanies = "";
                int count = 0;
                myDataRows = mm.GetCompanyLocations(this.cboPolicyCompany.Text);
                foreach (Hashtable comp in myDataRows)
                {
                    if (count > 0)
                        txtCompanies += ",";
                    txtCompanies += ((string)comp["Description"]).Trim();
                    count++;
                }
                companyData = txtCompanies.Split(',');
                this.cboPolicyCoLocation.DataSource = companyData;
                this.cboPolicyCoLocation.DataBind();
                this.cboPolicyCoLocation.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadIndividuals()
        {
            try
            {
                if ((this.SelectedPolicy == "") || this.SelectedVersion == "")
                    return false;       //no policy or version has been selected

                myDataRows = mm.GetPolicyIndividuals(SelectedPolicy, SelectedVersion);

                //concatenate the address
                string streetAddress = "";
                foreach (Hashtable myRow in myDataRows)
                {
                    streetAddress = (string)myRow["Address_1"] + ", " + (string)myRow["City"] + ", " + (string)myRow["State"] + " " + (string)myRow["Zip"];
                    streetAddress = streetAddress.Replace(" , , ", string.Empty).Replace(" , ", string.Empty).Trim();
                    myRow["Address_1"] = streetAddress;
                }

                grPolicyIndividuals.DataSource = LoadIndividuals(myDataRows);
                grPolicyIndividuals.DataBind();
                grPolicyIndividuals.SelectedIndex = 0;
               
                return true;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return false;
            }
        }

        protected void grPolicySearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedPolicy = grPolicySearch.SelectedRow.Cells[1].Text;
            SelectedVersion = grPolicySearch.SelectedRow.Cells[2].Text;
            try
            {
                mm.UpdateLastActivityTime();

                //enable buttons
                btnPolicyOK.Enabled = false;
                btnPolicyOK.Visible = false;
                btnUsePolicyOK.Enabled = true;
                btnUsePolicyOK.Visible = true;

                bool individualsLoaded = LoadIndividuals();     //get individuals
                bool vehiclesLoaded = LoadVehicles();       //get vehiclesl

                //set company for policy
                try
                {
                    this.cboPolicyCompany.Text = Company.getCompanyForPolicy(SelectedPolicy);
                    this.cboPolicyCompany_SelectedIndexChanged(sender, null);
                }
                catch (Exception ex)
                { }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadVehicles()
        {
            try
            {
                if ((this.SelectedPolicy == "") || this.SelectedVersion == "")
                    return false;       //no policy or version has been selected

                myDataRows = mm.GetPolicyVehicles(SelectedPolicy, SelectedVersion);

                grPolicyVehicles.DataSource = LoadVehicles(myDataRows);
                grPolicyVehicles.DataBind();
                grPolicyVehicles.SelectedIndex = 0;
               
                return true;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return false;
            }
        }

        protected void btnUsePolicyOK_Click(object sender, EventArgs e)
        {
            if (grPolicySearch.SelectedIndex < 0)
                return;

            btnUsePolicyOK.Enabled = false;
            LoadClaims();



        }

        private bool LoadClaims()
        {
            int claimId = 0;

            MedCsxLogic.Claim cl = new MedCsxLogic.Claim(claimId);  //create the claim object

            ArrayList claims;
            if (this.claimId == 0)
                claims = cl.DupicateChildObjects(typeof(PossibleDuplicateClaim), "policy_no='" + grPolicySearch.SelectedRow.Cells[1].Text + "' and date_of_loss='" + tbPolicyDOL.Text + "'");
            else
                claims = cl.PossibleDuplicateClaims();

            dupCount = claims.Count;
            if (claims.Count > 0)
            {
                DivPossibleDuplicatesFound.Visible = true;
                grPossibleDuplicates.DataSource = LoadClaims(claims);
                grPossibleDuplicates.DataBind();
                grPossibleDuplicates.SelectedIndex = 0;
            }
            else
            {
                Hashtable parms = new Hashtable();

                if (Request.QueryString["mode"] != null)
                {
                    btnPolicyOK_Click(null, null);
                }
                else
                {
                    claimId = this.CreateClaimWithPolicy(grPolicySearch.SelectedRow.Cells[1].Text, grPolicySearch.SelectedRow.Cells[2].Text, isExpired, DateTime.Parse(tbPolicyDOL.Text), cboPolicyCompany.SelectedValue.ToString(), cboPolicyCoLocation.SelectedValue, Convert.ToInt32(Request.QueryString["subClaimTypeID"]), 0);
                }

                if (Request.QueryString["mode"] == null)
                {
                    string url = "viewClaim.aspx?Ptabindex=0&claim_id=" + claimId;
                    string win = "window.open('" + url + "','my_window" + claimId.ToString() + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=400, top=200');";
                    ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

                    parms.Add("Claim_Id", claimId);

                    //int successful = ModMain.FileNoteClaimCreatedEventHandler((int)ModGeneratedEnums.EventType.Claim_Created_Without_Policy, parms);


                    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);    //close this form
                }
            }

            return false;
        }


        public int CreateClaimWithPolicy(string policyNo, string versionNo, bool expired, DateTime dateOfLoss, string company, string location, int subClaimTypeID, int lossLocationNo, string LOB = "")
        {
            ArrayList c = new ArrayList();
            int claimId,
                companyId,
                companyLocationId;
            Hashtable parms = new Hashtable(),
                claim = new Hashtable(),
                newClaim,
                companyRow,
                companyLocation;

            companyId = ml.getId("Company", company);
            companyLocationId = ml.getId("Company_Location", location, "Description", "Company_Id = " + companyId);

            //insert claim record
            claim.Add("Policy_No", policyNo);
            claim.Add("Version_No", versionNo);
            claim.Add("Is_Expired_Policy", expired);
            claim.Add("Date_Of_Loss", dateOfLoss);
            claim.Add("Company_Location_Id", companyLocationId);
            claim.Add("Has_Coverage_Issue", false);
            claim.Add("Claim_Status_Id", (int)modGeneratedEnums.ClaimStatus.Claim_Open);
            claim.Add("Date_Reported", ml.DBNow());
            claim.Add("Sub_Claim_Type_Id", subClaimTypeID);
            claim.Add("Loss_Location_Nbr", lossLocationNo);
            claimId = ml.InsertRow(claim, "Claim");

            //update claim with Display_Claim)Id
            claim = ml.getRow("Claim", claimId);
            newClaim = ml.copyDictionary(claim);
            companyRow = ml.getRow("Company", companyId);
            companyLocation = ml.getRow("Company_Location", companyLocationId);

            string displayclaim =  (string)companyRow["Abbreviation"] + (string)companyLocation["Abbreviation"] + claimId.ToString("000000");
            newClaim["Display_Claim_Id"] = displayclaim;

            /*we don't want a file note inserted for this update.
             * it will be handled through a manually entered file note later*/
            ml.updateRow("Claim", newClaim, claim, null, false);


           

            //refresh policy data
            MedCsxLogic.Claim cl = new MedCsxLogic.Claim(claimId);
            cl.RefreshPolicyData();

            //set default insured vehicle and insured, owner and drivers
            mm.UpdateDefaultVehiclePersons(claimId, Convert.ToInt32(HttpContext.Current.Session["userID"]));

          

            //fire claim created event
            parms.Add("Claim_Id", claimId);
            parms.Add("Policy_Number", policyNo);
            parms.Add("Version_Number", versionNo);
            mm.FireEvent((int)modGeneratedEnums.EventType.Claim_Created_Using_Policy, parms);

            //write ImageRight FUP File
            ImageRightFupFile fup = new ImageRightFupFile(claimId, displayclaim);
            fup.WriteFile();

            //write ImageRight AFUP File
            ImageRightAFupFile afup = new ImageRightAFupFile(claimId);
            afup.WriteFile();

            return claimId;
        }



        private List<DuplicateClaims> LoadClaims(ArrayList a)
        {
            Hashtable claimIds = new Hashtable();
            myDuplicateClaims = new List<DuplicateClaims>();
            try
            {
                foreach (PossibleDuplicateClaim d in a)
                {
                    if (!claimIds.ContainsKey(d.DuplicateClaim()))
                    {
                        claimIds.Add(d.DuplicateClaimId, 0);

                        MedCsxLogic.Claim dc = d.DuplicateClaim();
                        myDuplicateClaims.Add(new DuplicateClaims()
                        {
                            duplicateClaim = dc.DisplayClaimId,
                            claimId = dc.ClaimId,
                            policyNo = dc.PolicyNo,
                            dtLoss = dc.DateOfLoss.ToShortDateString(),
                            matchDescrip = d.Description
                        });
                    }
                }
                return myDuplicateClaims;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDuplicateClaims;
            }
        }

        protected void btnPolicyOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.SelectedPolicy = grPolicySearch.SelectedRow.Cells[1].Text;
                this.SelectedVersion = grPolicySearch.SelectedRow.Cells[2].Text;


                if (this.SelectedPolicy == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('No policy selected.');</script>");
                    //MessageBox.Show("No policy selected.", "No Policy", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    return;
                }

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You have selected " + this.cboPolicyCompany.Text + " as the Company for this Claim');</script>");
               

                //ok to assign the claim a policy
                this.policyNo = this.SelectedPolicy;       //set the policy to use
                this.versionNo = this.SelectedVersion;
                this.dateOfLoss = DateTime.Parse(this.tbPolicyDOL.Text);

                this.okPressed = true;  //the OK button was pressed
                MedCsxLogic.Claim claim = new MedCsxLogic.Claim(this.claimId);
                claim.AssociateWithNewPolicy(policyNo, versionNo, isExpired);
                string msg = claim.SyncPolicy(true);
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + ".');</script>");
                //MessageBox.Show(msg, "Sync Policy", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                claim.UpdateNamedInsured();
                string SaveDisplayClaimId = claim.DisplayClaimId;
                
                claim.UpdateCompanyLocationId(cboPolicyCompany.Text, associateWithNewPolicy);


                //write ImageRight FUP File
                ImageRightFupFile fup = new ImageRightFupFile(claimId, SaveDisplayClaimId);
                fup.WriteFile();

                //write ImageRight AFUP File
                ImageRightAFupFile afup = new ImageRightAFupFile(claimId);
                afup.WriteFile();


                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
                
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void Load_Form()
        {
           
            if (this.associateWithNewPolicy)
            {   //associating claim with a policy
                this.tbPolicyDOL.Text = this.dateOfLoss.ToString("yyyy-MM-dd");
                this.btnPolicyOK.Visible = true;
                this.btnUsePolicyOK.Visible = false;
                this.btnNoPolicyOK.Visible = false;
                this.Page.Title = "Find New Policy To Associate With Claim";
            }
            else
            {   //creating new claim
                this.btnPolicyOK.Visible = false;
                this.btnUsePolicyOK.Visible = true;
                this.btnNoPolicyOK.Visible = true;
                this.Title = "Find Policy for New Claim";
            }

            this.Page.Title += " - " + this.mySubClaimType.Description;  //let user know what kind of policy they are looking for

            mf.LoadStateComboBox(this.cboPolicyState);
            this.LoadCompanies();

            //Auto policies
            if (Company_Id == 0)
            { 
                this.cboPolicyCompany.Text = "Key Insurance";
                cboPolicyCompany_SelectedIndexChanged(cboPolicyCompany, EventArgs.Empty);
            }
            else
            { 
                this.cboPolicyCompany.SelectedValue = Company_Id.ToString();
                cboPolicyCompany_SelectedIndexChanged(cboPolicyCompany, EventArgs.Empty);
            }
        }

        protected void btnCreateWithDuplicate_Click(object sender, EventArgs e)
        {
            btnCreateWithDuplicate.Enabled = false;
            int claimId = 0;
            try
            {
                mm.UpdateLastActivityTime();

                if (Request.QueryString["mode"] != null)
                {
                    btnPolicyOK_Click(null, null);
                }
                else
                {
                    claimId = this.CreateClaimWithPolicy(grPolicySearch.SelectedRow.Cells[1].Text, Convert.ToDecimal(grPolicySearch.SelectedRow.Cells[2].Text), this.isExpired, DateTime.Parse(this.tbPolicyDOL.Text), this.cboPolicyCompany.Text, this.cboPolicyCoLocation.Text, this.mySubClaimType.Id, 0, "");
                }

                //if possible duplicates, fire event so file note is entered
                //if (dupCount > 0)
                //{
                //    try
                //    {
                //        MedCsxLogic.Claim c = new MedCsxLogic.Claim(claimId);
                //        c.checkForPossibleDuplicateClaims(PossibleDuplicateClaimMode.Claim_Created);
                //    }
                //    catch (Exception ex)
                //    {
                //        mf.ProcessError(ex);
                //    }
                //}

                if (Request.QueryString["mode"] == null)
                {
                    string url = "ViewClaim.aspx?Ptabindex=0&claim_id=" + claimId;
                    string win = "window.open('" + url + "','my_window" + claimId.ToString() + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no');";
                    ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

                    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);    //close this form
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public int CreateClaimWithoutPolicy(DateTime dateOfLoss, string company, string location, int subClaimTypeID)
        {
            int claimId,
                companyId,
                companyLocationId;
            Hashtable parms = new Hashtable(),
                claim = new Hashtable(),
                newClaim,
                companyRow,
                companyLocation;

            companyId = ml.getId("Company", company);
            string whereclse = " Description = '" + location + "' and Company_Id = " + companyId;
            companyLocationId = ml.getId("Company_Location", 1, whereclse);

            //insert claim record
            claim.Add("Policy_No", "");
            claim.Add("Version_No", 0);
            claim.Add("Date_Of_Loss", dateOfLoss);
            claim.Add("Company_Location_Id", companyLocationId);
            claim.Add("Has_Coverage_Issue", true);
            claim.Add("Claim_Status_Id", (int)modGeneratedEnums.ClaimStatus.Claim_Open);
            claim.Add("Date_Reported", ml.DBNow());
            claim.Add("Sub_Claim_Type_Id", subClaimTypeID);
            claimId = ml.InsertRow(claim, "Claim");

            //update claim with Display_Claim)Id
            claim = ml.getRow("Claim", claimId);
            newClaim = ml.copyDictionary(claim);
            companyRow = ml.getRow("Company", companyId);
            companyLocation = ml.getRow("Company_Location", companyLocationId);

            newClaim["Display_Claim_Id"] = (string)companyRow["Abbreviation"] + (string)companyLocation["Abbreviation"] + claimId.ToString("000000");

            /*we don't want a file note inserted for this update.
             * it will be handled through a manually entered file note later*/
            ml.updateRow("Claim", newClaim, claim, null, false);

            //fire claim created event
            parms.Add("Claim_Id", claimId);
            mm.FireEvent((int)modGeneratedEnums.EventType.Claim_Created_Without_Policy, parms);

            return claimId;
        }

        public int CreateClaimWithPolicy(string policyNo, decimal versionNo, bool expired, DateTime dateOfLoss, string company, string location, int subClaimTypeID, int lossLocationNo, string LOB = "")
        {
            ArrayList c = new ArrayList();
            int claimId,
                companyId,
                companyLocationId;
            Hashtable parms = new Hashtable(),
                claim = new Hashtable(),
                newClaim,
                companyRow,
                companyLocation;

            companyId = ml.getId("Company", company);
            companyLocationId = ml.getId("Company_Location", location, "Description", "Company_Id = " + companyId);

            //insert claim record
            claim.Add("Policy_No", policyNo);
            claim.Add("Version_No", versionNo);
            claim.Add("Is_Expired_Policy", expired);
            claim.Add("Date_Of_Loss", dateOfLoss);
            claim.Add("Company_Location_Id", companyLocationId);
            claim.Add("Has_Coverage_Issue", false);
            claim.Add("Claim_Status_Id", (int)modGeneratedEnums.ClaimStatus.Claim_Open);
            claim.Add("Date_Reported", ml.DBNow());
            claim.Add("Sub_Claim_Type_Id", subClaimTypeID);
            claim.Add("Loss_Location_Nbr", lossLocationNo);
            claimId = ml.InsertRow(claim, "Claim");

            //update claim with Display_Claim)Id
            claim = ml.getRow("Claim", claimId);
            newClaim = ml.copyDictionary(claim);
            companyRow = ml.getRow("Company", companyId);
            companyLocation = ml.getRow("Company_Location", companyLocationId);

            newClaim["Display_Claim_Id"] = (string)companyRow["Abbreviation"] + (string)companyLocation["Abbreviation"] + claimId.ToString("000000");

            /*we don't want a file note inserted for this update.
             * it will be handled through a manually entered file note later*/
            ml.updateRow("Claim", newClaim, claim, null, false);

            //refresh policy data
            MedCsxLogic.Claim cl = new MedCsxLogic.Claim(claimId);
            cl.RefreshPolicyData();

            //set default insured vehicle and insured, owner and drivers
            mm.UpdateDefaultVehiclePersons(claimId, Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //fire claim created event
            parms.Add("Claim_Id", claimId);
            parms.Add("Policy_Number", policyNo);
            parms.Add("Version_Number", versionNo);
            mm.FireEvent((int)modGeneratedEnums.EventType.Claim_Created_Using_Policy, parms);

            return claimId;
        }

        protected void btnNoPolicyOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

               

                if (!ValidDateOfLoss())
                    return;

               

                int claimId;

               

                //create claim without a policy
                claimId = this.CreateClaimWithoutPolicy(DateTime.Parse(this.tbPolicyDOL.Text), this.cboPolicyCompany.Text, this.cboPolicyCoLocation.Text, this.mySubClaimType.Id);

                //open then new claim
                string url = "ViewClaim.aspx?Ptabindex=0&claim_id=" + claimId;
                string win = "window.open('" + url + "','my_window" + claimId.ToString() + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no');";
                ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void showActivePolicies_Click(object sender, EventArgs e)
        {
            ShowActivePolicies();
        }

        protected void showExpiredPolicies_Click(object sender, EventArgs e)
        {
            isExpired = true;
            ShowExpiredPolicies();
        }

        private void LoadCompanies()
        {
            try
            {
                mf.LoadTypeTableComboBox(this.cboPolicyCompany, "Company", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }


        private List<PolicyGrid> LoadPolicies(List<Hashtable> pData)
        {
            myPolicyList = new List<PolicyGrid>();
            try
            {
                foreach (Hashtable myData in pData)
                {
                    int tvehYear = 0;
                    string tvehMake = "";
                    string tvehModel = "";
                    string tvehVIN = "";

                    if ((string)myData["Model"] != "")
                    {
                        tvehYear = int.Parse((string)myData["Year"]);
                        tvehMake = (string)myData["Make"];
                        tvehModel = (string)myData["Model"];
                        tvehVIN = (string)myData["Vin"];
                    }
                    else
                    {
                        tvehYear = 0;
                        tvehMake = "";
                        tvehModel = "";
                        tvehVIN = "";
                    }

                    myPolicyList.Add(new PolicyGrid()
                    {
                        policyNo = (string)myData["Policy"],
                        versionNo = (double)((decimal)myData["Version"]),
                        dtEffective = ((DateTime)myData["Eff_Dt"]).ToShortDateString(),
                        dtExpire = ((DateTime)myData["Exp_Dt"]).ToShortDateString(),
                        fName = (string)myData["First_Name"],
                        lName = (string)myData["Last_Name"],
                        vehYear = tvehYear,
                        vehMake = tvehMake,
                        vehModel = tvehModel,
                        vehVIN = tvehVIN

                    });

                   
                }
                return myPolicyList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myPolicyList;
            }
        }

        private List<ClaimantGrid> LoadIndividuals(List<Hashtable> indData)
        {
            myIndividualList = new List<ClaimantGrid>();
            try
            {
                foreach (Hashtable myData in indData)
                {
                    ClaimantGrid ig = new ClaimantGrid();
                    ig.claimantId = (byte)myData["No"];
                    ig.personType = (string)myData["Type"];
                    ig.fullName = (string)myData["Name"];
                    ig.streetAddress = (string)myData["Address_1"];
                    if ((string)myData["Home_Phone"] != null)
                        ig.phone = (string)myData["Home_Phone"];
                    else if ((string)myData["Bus_Phone"] != null)
                        ig.phone = (string)myData["Bus_Phone"];
                    ig.cDOB = ((DateTime)myData["Date_of_Birth"]).ToShortDateString();
                    ig.cAge = (byte)myData["Age"];
                    ig.cSex = (string)myData["Sex"];
                    ig.MaritalStatus = (string)myData["Marital_Status"];

                    myIndividualList.Add(ig);
                }
                return myIndividualList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myIndividualList;
            }
        }

        private List<VehicleGrid> LoadVehicles(List<Hashtable> vData)
        {
            myVehicleList = new List<VehicleGrid>();
            try
            {
                foreach (Hashtable myData in vData)
                {
                    myVehicleList.Add(new VehicleGrid()
                    {
                        vehicleId = (byte)myData["No"],
                        level = (string)myData["Lvl"],
                        vYear = int.Parse((string)myData["Year"]),
                        vMake = (string)myData["Make"],
                        vModel = (string)myData["Model"],
                        vVIN = (string)myData["VIN"],
                        vAge = (byte)myData["Age"]
                    });
                }
                return myVehicleList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myVehicleList;
            }
        }

        class PolicyGrid
        {
            public string policyNo { get; set; }
            public double versionNo { get; set; }
            public string dtEffective { get; set; }
            public string dtExpire { get; set; }
            public string fName { get; set; }
            public string lName { get; set; }
            public int vehYear { get; set; }
            public string vehMake { get; set; }
            public string vehModel { get; set; }
            public string vehVIN { get; set; }
        }

        class DuplicateClaims
        {
            public string duplicateClaim { get; set; }
            public int claimId { get; set; }
            public string policyNo { get; set; }
            public string dtLoss { get; set; }
            public string matchDescrip { get; set; }
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
   
    public partial class frmVendors : System.Web.UI.Page
    {
        public string searchValue = "";
        public Vendor vendor = null;
        public bool okPressed = false;      //was the OK button pressed?

        private bool isLookup = false,      //is this form used to lookup a vendor?
            isFromFraud = false;            //is this form from adding fraud claim?
        private string searchColumn = "",     //column to search - taxId, address, name, etc
            lookupValue = "";                 //search Value when called in lookup mode
        int vendorTypeId = 0;               //vendor type id for search

        //private List<VendorTypeGrid> myVendorTypeList;
        //private List<VendorTypeGrid> myVendorSubTypeList;
        private List<ClaimantGrid> myVendorList;
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        
        protected void Page_Load(object sender, EventArgs e)
        {
            lookupValue = Request.QueryString["lookup_value"].ToString();
            searchColumn = Request.QueryString["search_column"].ToString();
            if (Request.QueryString["vendor_type_id"] != null)
            {
                vendorTypeId = Convert.ToInt32(Request.QueryString["vendor_type_id"]);
            }
            this.isLookup = true;
            this.isFromFraud = false;

            if (!Page.IsPostBack)
            {
                if (this.isLookup)
                {   //selecting a vendor
                    this.btnCancel.Visible = true;
                    //this.btnUseVendor.Visibility = Visibility.Visible;
                    //this.btnClose.Visibility = Visibility.Collapsed;
                }
                else
                {
                    this.btnCancel.Visible = false;
                    //this.btnUseVendor.Visibility = Visibility.Collapsed;
                    //this.btnClose.Visibility = Visibility.Visible;
                }

                //if (this.isFromFraud)
                //    this.btnAddEditFraud.Visibility = Visibility.Collapsed;
                //else
                //    this.btnAddEditFraud.Visibility = Visibility.Visible;

                this.txtSearchCriteria.Visible = false;   //don't show until criterion selected
                bool typeLoaded = LoadTypes();

                if (this.vendorTypeId > 0)
                {
                    if (this.vendorTypeId == (int)modGeneratedEnums.VendorType.Claimant_Attorney)
                        this.grVendorType.SelectedIndex = 3;
                    else if (this.vendorTypeId == (int)modGeneratedEnums.VendorType.Legal_Services)
                        this.grVendorType.SelectedIndex = 10;
                    //this.grVendorType.SelectedIndex = vendorTypeId;
                    this.grVendorSubType_SelectedIndexChanged(this.grVendorType.SelectedItem, null);
                }
                else if (this.isLookup)
                {
                    this.searchValue = this.lookupValue;
                    this.searchColumn = "Name";
                    this.txtSearchCriteria.Text = "Vendor Name Starts With: '" + searchValue + "'";
                    SearchVendors();        //find vendors using search criteria
                }
            }

        }

       

        private bool LoadTypes()
        {
            this.grVendorType.DataSource = mm.GetVendorTypes();
            this.grVendorType.DataValueField = "vendor_type_id";
            this.grVendorType.DataTextField = "description";
            this.grVendorType.DataBind();

            this.grVendorType.SelectedIndex = 0;
            grVendorType_SelectedIndexChanged(null, null);
            return true;
        }


        private void SearchVendors()
        {
            bool vendorsLoaded = false;
            if (this.searchColumn.Trim() != "")
                vendorsLoaded = LoadVendors(true);

            this.txtSearchCriteria.Visible = true;       //display the search criteria
        }

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //set the search criteria
                if (lblVendorSearch.Text == "Enter Vendor Name (or first part of one):")
                    this.searchColumn = "Name";
                else if (lblVendorSearch.Text == "Enter Tax Id (or first part of one):")
                    this.searchColumn = "Tax_Id";
                else if (lblVendorSearch.Text == "Enter Address (or first part of one):")
                    this.searchColumn = "Address1";
                else if (lblVendorSearch.Text == "Enter City (or first part of one):")
                    this.searchColumn = "City";
                else if (lblVendorSearch.Text == "Enter State Abbreviation:")
                    this.searchColumn = "State";
                else if (lblVendorSearch.Text == "Enter Vendor Zip Code (or first part of one):")
                    this.searchColumn = "Zip";

                this.searchValue = txtVendorSearch.Text;

                if (this.searchValue == "")
                    return;     //no value entered or cancel buttn clicked

                //display the search criteria
                this.txtSearchCriteria.Text = searchColumn + " Starts With: '" + this.searchValue + "'";

                if (this.searchColumn == "State")
                    this.txtSearchCriteria.Text = searchColumn + " equals: '" + this.searchValue + "'";

                SearchVendors();        //find the vendors

                txtVendorSearch.Text = "";
                vendorlookup.Visible = false;
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + ex.ToString() + "');</script>");
                //mf.ProcessError(ex);
            }
        }

        protected void btnSearchByTaxId_Click(object sender, EventArgs e)
        {

            vendorlookup.Visible = true;
            lblVendorSearch.Text = "Enter Tax Id (or first part of one):";
            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    //set the search criteria
            //    this.searchColumn = "Tax_Id";
            //    this.searchValue = Microsoft.VisualBasic.Interaction.InputBox("Enter Tax Id (or first part of one):", "Search Vendors by Tax Id");

            //    if (this.searchValue == "")
            //        return;     //no value entered or cancel buttn clicked

            //    //display the search criteria
            //    this.txtSearchCriteria.Text = "Tax Id Starts With: '" + this.searchValue + "'";

            //    SearchVendors();        //find the vendors
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void btnSearchByName_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            lblVendorSearch.Text = "Enter Vendor Name (or first part of one):";
            
        }

        protected void btnSearchByAddress_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            lblVendorSearch.Text = "Enter Address (or first part of one):";

            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    //set the search criteria
            //    this.searchColumn = "Address1";
            //    this.searchValue = Microsoft.VisualBasic.Interaction.InputBox("Enter Address (or first part of one):", "Search Vendors by Address");

            //    if (this.searchValue == "")
            //        return;     //no value entered or cancel buttn clicked

            //    //display the search criteria
            //    this.txtSearchCriteria.Text = "Address Starts With: '" + this.searchValue + "'";

            //    SearchVendors();        //find the vendors
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void btnSearchByCity_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            lblVendorSearch.Text = "Enter City (or first part of one):";
            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    //set the search criteria
            //    this.searchColumn = "City";
            //    this.searchValue = Microsoft.VisualBasic.Interaction.InputBox("Enter City (or first part of one):", "Search Vendors by City");

            //    if (this.searchValue == "")
            //        return;     //no value entered or cancel buttn clicked

            //    //display the search criteria
            //    this.txtSearchCriteria.Text = "City Starts With: '" + this.searchValue + "'";

            //    SearchVendors();        //find the vendors
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void btnSearchByState_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            lblVendorSearch.Text = "Enter State Abbreviation:";

            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    //set the search criteria
            //    this.searchColumn = "State";
            //    this.searchValue = Microsoft.VisualBasic.Interaction.InputBox("Enter State Abbreviation:", "Search Vendors by State");

            //    if (this.searchValue == "")
            //        return;     //no value entered or cancel buttn clicked

            //    if (this.searchValue.Length != 2)
            //    {
            //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Enter a Valid State Abbrevation.');</script>");
            //        //MessageBox.Show("Enter a Valid State Abbrevation.", "Invalid State", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            //        return;
            //    }
            //    else if (mm.GetStateId(this.searchValue) == 0)
            //    {
            //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Enter a Valid State Abbrevation.');</script>");
            //        //MessageBox.Show("Enter a Valid State Abbrevation.", "Invalid State", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            //        return;
            //    }

            //    //display the search criteria
            //    this.txtSearchCriteria.Text = "State Equals: '" + this.searchValue + "'";

            //    SearchVendors();        //find the vendors
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void grVendorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //clear search criteria
                this.txtSearchCriteria.Visible = false;
                this.searchColumn = "";
                this.searchValue = "";

                //VendorTypeGrid mySelection = (VendorTypeGrid)((DataGrid)sender).SelectedItem;
                //if (mySelection == null)
                //    return;

                this.grVendorSubType.DataSource = mm.GetVendorSubTypes(grVendorType.SelectedItem.ToString());
                this.grVendorSubType.DataTextField = "Description";
                this.grVendorSubType.DataValueField = "vendor_sub_type_id";
                this.grVendorSubType.DataBind();
                this.grVendorSubType.SelectedIndex = 0;
                this.grVendorSubType_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
               
            }
        }

        protected void grVendorSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                bool vendorsLoaded = LoadVendors();
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + ex.ToString() + "');</script>");
                // mf.ProcessError(ex);
            }
        }

        protected void btnVendorsAdd_Click(object sender, EventArgs e)
        {
            Session["referer2"] = "frmVendors.aspx?lookup_value=aa&search_column=Name";
            string url = "frmAddEditVendor.aspx?mode=u&claim_id=0&vendor_Id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnVendorsUpdate_Click(object sender, EventArgs e)
        {
            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(grVendors.DataKeys[index].Value);


            Session["referer2"] = "frmVendors.aspx?lookup_value=aa&search_column=Name";
            string url = "frmAddEditVendor.aspx?mode=u&claim_id=0&vendor_Id=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void grVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["vendorID"] = Convert.ToInt32(grVendors.DataKeys[grVendors.SelectedIndex].Value);
            string refererVendor = Session["refererVendor"].ToString();
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + refererVendor + "';window.close();", true);
        }

        //protected void btnOK_Click(object sender, EventArgs e)
        //{
        //    string refererVendor = Session["refererVendor"].ToString();
        //    ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + refererVendor + "';window.close();", true);

        //}

        protected void btnSearchByZip_Click(object sender, EventArgs e)
        {
            vendorlookup.Visible = true;
            lblVendorSearch.Text = "Enter Vendor Zip Code (or first part of one):";
        }

        //protected void copyToClipBoard_Click(object sender, EventArgs e)
        //{
        //    var closeLink = (Control)sender;
        //    GridViewRow row = (GridViewRow)closeLink.NamingContainer;
            
        //    Clipboard.SetText(row.Cells[1].Text);

        //    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
        //}

        private bool LoadVendors(bool hasSearchResults = false)
        {
            if (!hasSearchResults)
            {
                //VendorTypeGrid mySelection = (VendorTypeGrid)grVendorSubType.SelectedItem;
                //if (mySelection == null)
                //    return false;
                string tmp = grVendorSubType.SelectedItem.ToString();
                grVendors.DataSource = mm.GetVendorsBySubType(tmp);
                grVendors.DataBind();
            }
            else
            {
                grVendors.DataSource = mm.SearchVendors(this.searchColumn, this.searchValue);
                grVendors.DataBind();
            }
            //grVendors.Columns[1].Header = "Vendor Names (" + grVendors.Items.Count + ")";
            //grVendors.SelectedIndex = 0;
            return true;
        }

        //class VendorTypeGrid
        //{
        //    public int id { get; set; }
        //    public int imageIdx { get; set; }
        //    public string description { get; set; }
           
        //}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class alert : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
     


        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Alerts";
           

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();

                if (Request.QueryString["mode"].ToString() == "c")
                {

                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {

                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {

                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {

                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            //Witness myData = new Witness();
            //myData.GetWitnessInfoByID(id);
            //ddlWitnessPassengerTypes.SelectedValue = myData.witness_passenger_type_id;
            //ddlPersonType.SelectedValue = myData.witness_passenger_type_id;
            //// witness_passenger_type_id
        }

        protected void PopulateDDLs()
        {
            //DataTable dt = new DataTable();
            //dt = HelperFunctions.getTypeTableRows("Appraisal_Investigation_Type", "Description", "1=1");

            //DataRow newRow = dt.NewRow();
            //newRow["Id"] = 0;
            //newRow["Description"] = "All";
            //dt.Rows.InsertAt(newRow, 0);

            //ddlInvestigationType.DataSource = dt;
            //ddlInvestigationType.DataValueField = dt.Columns[0].ColumnName;
            //ddlInvestigationType.DataTextField = dt.Columns[1].ColumnName;
            //ddlInvestigationType.DataBind();
            ////DataTable dt = new DataTable();

            //Appraisals myData = new Appraisals();
            //dt = myData.GetSceneAccessAppraisers();


            //for (int i = dt.Rows.Count - 1; i >= 0; i--)
            //{
            //    DataRow dr = dt.Rows[i];
            //    if (dr["name"].ToString() == "")
            //        dr.Delete();
            //}

            //dt.AcceptChanges();

            //dt.Columns.Add(new DataColumn("DisplayColumn"));
            //foreach (DataRow row in dt.Rows)
            //{
            //    if (row["States"].ToString() != "")
            //        row["DisplayColumn"] = row["Name"].ToString() + " (" + row["States"].ToString() + ")";
            //    else
            //        row["DisplayColumn"] = row["Name"].ToString();
            //}

            //DataRow newRow2 = dt.NewRow();
            //newRow2["Id"] = 0;
            //newRow2["DisplayColumn"] = "Select Appraiser";
            //dt.Rows.InsertAt(newRow2, 0);

            //ddlAppraiser.DataSource = dt;
            //ddlAppraiser.DataValueField = dt.Columns["id"].ToString();
            //ddlAppraiser.DataTextField = dt.Columns["DisplayColumn"].ToString();
            //ddlAppraiser.DataBind();


        }
    }
}
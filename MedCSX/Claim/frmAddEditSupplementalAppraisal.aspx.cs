﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAddEditSupplementalAppraisal : System.Web.UI.Page
    {
        private int m_AppraisalResponseId;
        private AppraisalSupplemental m_Supplemental;
        private List<cboGrid> myAppraiserList;

        public bool okPressed = false;
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = "Add / Edit Supplemental Appraisals";
            LoadAppraiser();     //load appraisal combo box
            bool gridLoaded = LoadSupplemental();
            RefreshInput();

            this.btnDeleteSupplemental.Enabled = false;
            this.btnEditSupplemental.Enabled = false;
        }

        private bool LoadSupplemental()
        {
            this.grSupplementals.DataSource = LoadSupplemental(mm.GetSupplementalForResponse(m_AppraisalResponseId));
            this.grSupplementals.DataBind();
            return true;
        }

        private void RefreshInput()
        {
            this.txtDescription.Text = "";
            this.txtAmount.Text = "";
            this.cboAppraisers.SelectedIndex = 0;
            this.m_Supplemental = null;

            this.txtAddSupplemental.Text = "Add";
        }

        private IEnumerable LoadSupplemental(List<Hashtable> list)
        {
            List<SupplementalList> mySupplements = new List<SupplementalList>();
            try
            {
                foreach (Hashtable sData in list)
                {
                    mySupplements.Add(new SupplementalList()
                    {
                        id = (int)sData["Id"],
                        description = (string)sData["Description"],
                        amount = (double)((decimal)sData["Amount"]),
                        appraiser = (string)sData["Name"]
                    });
                }
                return mySupplements;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return mySupplements;
            }
        }

        private void LoadAppraiser()
        {
            this.cboAppraisers.DataSource = LoadAppraisers(mm.GetAppraisers());
            this.cboAppraisers.DataTextField = "description";
            this.cboAppraisers.DataValueField = "id";
            this.cboAppraisers.DataBind();
            this.cboAppraisers.SelectedIndex = 0;
        }

        private IEnumerable LoadAppraisers(List<Hashtable> list)
        {
            myAppraiserList = new List<cboGrid>();
            myAppraiserList.Add(new cboGrid()
            {
                id = 0,
                description = "Select Appraiser..."
            });
            try
            {
                foreach (System.Collections.Hashtable aData in list)
                {
                    if (((string)aData["Name"] != "") && ((string)aData["Active"] == "Yes"))
                    {
                        cboGrid cg = new cboGrid();
                        cg.id = (int)aData["Id"];
                        cg.imageIdx = (int)aData["Image_Index"];
                        cg.description = (string)aData["Name"];
                        if ((string)aData["States"] != "")
                            cg.description += "(" + (string)aData["States"] + ")";
                        myAppraiserList.Add(cg);
                    }
                }
                return myAppraiserList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAppraiserList;
            }
        }

       

        class SupplementalList
        {
            public int id { get; set; }
            public string description { get; set; }
            public string appraiser { get; set; }
            public double amount { get; set; }
        }

        protected void txtAddSupplemental_Click(object sender, EventArgs e)
        {
            //TODO
            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    if (!this.btnEditSupplemental.Enabled)
            //        return;

            //    SupplementalList supplement = (SupplementalList)grSupplementals.SelectedItem;
            //    if (supplement == null)
            //        return;

            //    this.txtDescription.Text = supplement.description;
            //    this.txtAmount.Text = supplement.amount.ToString("c");
            //    this.cboAppraisers.Text = supplement.appraiser;
            //    this.m_Supplemental = new AppraisalSupplemental(supplement.id);

            //    this.txtAddSupplemental.Text = "Save";
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void btnEditSupplemental_Click(object sender, EventArgs e)
        {
            //TODO
            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    if (!this.btnEditSupplemental.Enabled)
            //        return;

            //    SupplementalList supplement = (SupplementalList)grSupplementals.SelectedItem;
            //    if (supplement == null)
            //        return;

            //    this.txtDescription.Text = supplement.description;
            //    this.txtAmount.Text = supplement.amount.ToString("c");
            //    this.cboAppraisers.Text = supplement.appraiser;
            //    this.m_Supplemental = new AppraisalSupplemental(supplement.id);

            //    this.txtAddSupplemental.Text = "Save";
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        protected void btnDeleteSupplemental_Click(object sender, EventArgs e)
        {
            //TODO
            //try
            //{
            //    mm.UpdateLastActivityTime();

            //    if (!this.btnDeleteSupplemental.IsEnabled)
            //        return;

            //    SupplementalList supplement = (SupplementalList)grSupplementals.SelectedItem;
            //    if (supplement == null)
            //        return;

            //    if (MessageBox.Show("Are you sure you want to delete this Supplemental Appraisal?", "Confirm Delete", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
            //        return;

            //    this.m_Supplemental = new AppraisalSupplemental(supplement.id);
            //    this.m_Supplemental.DeleteRow();

            //    bool refreshGrid = LoadSupplemental();
            //    RefreshInput();
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }
    }
}
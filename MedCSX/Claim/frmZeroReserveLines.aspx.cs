﻿using MedCsxDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmZeroReserveLines : System.Web.UI.Page
    {
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime StartDate = Convert.ToDateTime("1/1/2003");
            DateTime EndDate = DateTime.Now.AddDays(-20);
            grZeroReserves.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Zero_Reserve_Lines", StartDate, EndDate);
            grZeroReserves.DataBind();
        }
    }
}
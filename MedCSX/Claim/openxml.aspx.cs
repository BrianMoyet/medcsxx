﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO.Packaging;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using System.Text.RegularExpressions;


namespace MedCSX.Claim
{
    public partial class openxml : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Test();
            SearchAndReplace();
        }

        public static void SearchAndReplace()
        {
            string document = @"C:\GitCodeRepos\medcsxx Integration\MedCSX\Claim\KEY Copart Sell Letter2.docx";

            //WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true);
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                     
                }

                Regex regexText = new Regex("Hello world!");
                docText = regexText.Replace(docText, "Hi Everyone!");

                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }
        }

        private void Test()
        {

            DataTable dt = new DataTable();//suppose data comes fome database
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");
            dt.Columns.Add("Sex");
            dt.Rows.Add(1, "Tom", "male");
            dt.Rows.Add(2, "Jim", "male");
            dt.Rows.Add(3, "LiSa", "female");
            dt.Rows.Add(4, "LiLi", "female");

            string curFile = @"C:\GitCodeRepos\medcsxx Integration\MedCSX\Claim\KEY Copart Sell Letter2.docx";

            if (File.Exists(curFile))
            {
                using (
                    WordprocessingDocument wordDoc = WordprocessingDocument.Open(curFile, true))
                {
                    var mainPart = wordDoc.MainDocumentPart;
                    var res = from mark in mainPart.Document.Body.Descendants<BookmarkStart>()
                              where mark.Name == "Text26"
                              select mark;
                    var bookmark = res.SingleOrDefault();
                    if (bookmark != null)
                    {
                        var parent = bookmark.Parent;

                        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();
                        TableProperties tblProp = new DocumentFormat.OpenXml.Wordprocessing.TableProperties(
                            new TableBorders(
                                new Border()
                                {
                                    Val = new DocumentFormat.OpenXml.EnumValue<BorderValues>(BorderValues.DotDash),
                                    Size = 24
                                }
                            )
                        );
                        table.AppendChild<TableProperties>(tblProp);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DocumentFormat.OpenXml.Wordprocessing.TableRow tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {

                                DocumentFormat.OpenXml.Wordprocessing.TableCell tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();
                                tc.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "240" }));
                                tc.Append(new Paragraph(new Run(new Text(dt.Rows[i][j].ToString()))));
                                tr.Append(tc);
                            }
                            table.Append(tr);
                        }

                        parent.InsertAfterSelf(table);

                    }


                }
            }
            else
            {
                return;
            }
        }
    }
}
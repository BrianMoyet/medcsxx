﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Forms;
using MedCsxLogic;
using MedCsxDatabase;

namespace MedCSX.Claim
{
    public partial class frmLitigation : System.Web.UI.Page
    {

        public bool okPressed = false;      //was OK button pressed?

        private Litigation m_Litigation = null;     //Litigation object being edited
        private MedCsxLogic.Claim m_Claim = null;               //Claim object parent to this Litigation
        private bool hasChanged = false;            //has this object been changed?
        private List<int> DeletedDefendents = new List<int>();  //defendent IDs that have been deleted
        private List<int> DeletedPlaintiffs = new List<int>();  //plaintiff IDs that have been deleted
        List<Hashtable> DataList = new List<Hashtable>();
        private List<ReserveGrid> myReserveList;
        private List<cboList> myStatusList;
        private List<ClaimantGrid> myDefenseList;
        private List<ClaimantGrid> myProsecuteList;
        private List<ClaimantGrid> myLocationList;
        private List<cboList> myDefendantList;
        private List<cboList> myPlaintiffList;
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        public int claim_id = 0;
        public int litigation_id = 0;
        string referer = "";


        modLogic ml = new modLogic();

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Request.QueryString["claim_id"]);
            this.m_Claim = new MedCsxLogic.Claim(claim_id);

            litigation_id = Convert.ToInt32(Request.QueryString["litigation_id"]);
            if (litigation_id > 0)
            {
                
                m_Litigation = new Litigation(litigation_id);
            }

            referer = Session["referer"].ToString();
            mm.UpdateLastActivityTime();
            try
            {
                if (litigation_id == 0)
                {
                    grDefendants.Visible = true;
                    lblDefendents.Visible = false;
                    lblAddDefendantsMessage.Visible = true;
                }
                else
                    lblAddDefendantsMessage.Visible = false;

                if (!Page.IsPostBack)
                {
                    
                    //load combo boxes
                    bool statusLoaded = LoadStatus();
                    this.cboLitigationStatus.SelectedIndex = setSelection((int)modGeneratedEnums.LitigationStatus.Open, this.myStatusList);

                    bool defenseLoaded = LoadDefenseAttorney();
                    bool plaitiffLoaded = LoadPlaintiffAttorney();
                    bool courtLoaded = LoadLocation();

                    //load grids
                    bool reservesLoaded = LoadReserves();
                    bool defendantLoaded = false;
                    bool plaintiffLoaded = false;

                    //set values from Litigation - editing
                    if (this.m_Litigation != null)
                    {
                        this.cboLitigationStatus.SelectedIndex = setSelection(this.m_Litigation.LitigationStatusId, this.myStatusList);
                        this.cboLitigationDefenseAttorney.SelectedIndex = setSelection(this.m_Litigation.DefenseAttorneyId, this.myDefenseList, 1);
                        this.cboLitigationPlaintiffAttorney.SelectedIndex = setSelection(this.m_Litigation.PlaintiffAttorneyId, this.myProsecuteList, 1);
                        this.cboLitigationCourtLocation.SelectedIndex = setSelection(this.m_Litigation.CourtLocationId, this.myLocationList, 1);
                        this.dpFileDate.Text = Convert.ToDateTime(this.m_Litigation.DateServed.ToString()).ToString("yyyy-MM-dd"); // Convert.ToDateTime(this.m_Litigation.DateFiled).ToShortDateString();
                        this.dpServeDate.Text = Convert.ToDateTime(this.m_Litigation.DateFiled.ToString()).ToString("yyyy-MM-dd");  //Convert.ToDateTime(this.m_Litigation.DateServed).ToShortDateString();
                        this.txtAmount.Text = this.m_Litigation.Resolution_Amount.ToString("c");
                        this.txtComments.Text = this.m_Litigation.Comments;

                        foreach (Hashtable res in this.m_Litigation.Reserves())
                        {   //check the reserves that have been added to this litigation object
                            if ((int)res["Reserve_Id"] != 0)
                            {
                                //foreach (ReserveGrid rg in myReserveList)
                                //{
                                   
                                        for (int i = 0; i < grReserves.Rows.Count; i++)
                                        {
                                            if ((grReserves.Rows[i].Cells[1].Text == res["Reserve_Type"].ToString()) && (grReserves.Rows[i].Cells[2].Text == res["Claimant"].ToString()))
                                            {
                                                System.Web.UI.WebControls.CheckBox mycheck = ((System.Web.UI.WebControls.CheckBox)grReserves.Rows[i].FindControl("cbSelect"));
                                                mycheck.Checked = true;
                                                //grReserves.Rows[i].Cells[2].Text = res.assignedUser;
                                            }
                                        }
                                   



                                    //if (rg.id == (int)res["Reserve_Id"])
                                    //{
                                    //    rg.isSelected = true;
                                    //    break;
                                    //}
                                //}
                            }
                        }

                        defendantLoaded = LoadDefendants();
                        //plaintiffLoaded = LoadPlaintiffs();

                        //LoadCheckedReserves();
                    }
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        //private void LoadCheckedReserves()
        //{

        //    LitReserves = this.MyLitigation.Reserves

        //    foreach (InjuredReserves res in myReserveList)
        //    {
        //        if (ic.ReserveType().Description == res.coverageType)
        //        {
        //            res.hasCoverage = true;
        //            res.assignedUser = ic.Reserve.AssignedToDescription();

        //            for (int i = 0; i < grReserves.Rows.Count; i++)
        //            {
        //                if (grReserves.Rows[i].Cells[1].Text == res.coverageType)
        //                {
        //                    CheckBox mycheck = ((CheckBox)grReserves.Rows[i].FindControl("cbSelect"));
        //                    mycheck.Checked = true;
        //                    grReserves.Rows[i].Cells[2].Text = res.assignedUser;
        //                }
        //            }

        //        }
        //    }
        //}


       

        private Hashtable ConvertToHashTable()
        {
            Hashtable hash = new Hashtable();
            //hash.Add("Check_No", salvage.CheckNo);
            //hash.Add("Is_Void", salvage.isVoid);
            //hash.Add("Reissued_From_Salvage_Id", salvage.ReissuedFromSalvageId);
            ////hash.Add("Salvage_Id", salvage.SalvageId);
            //hash.Add("Vendor_Id", salvage.VendorId);
            //double storageAmount = double.Parse(this.txtStorageAmount.Text.Trim().Replace("$", "").Replace(",", ""));
            //int storageDays = int.Parse(this.txtStorageDays.Text.Trim().Replace("$", "").Replace(",", ""));
            //if (storageAmount > 0)
            //{
            //    hash.Add("Storage_Charges", storageAmount);
            //    hash.Add("Storage_Days", storageDays);
            //}
            //double towingAmount = double.Parse(this.txtTowingAmount.Text.Trim().Replace("$", "").Replace(",", ""));
            //if (towingAmount > 0)
            //    hash.Add("Towing_Charges", towingAmount);

            return hash;
        }


        private int setSelection(int p, IEnumerable list, int mode = 0)
        {
            int idIdx = 0;
            if (p == 0)
                return idIdx;

            if (mode == 0)
            {
                foreach (cboList c in list)
                {
                    if (c.id == p)
                        break;
                    idIdx++;
                }
            }
            else
            {
                foreach (ClaimantGrid c in list)
                {
                    if (c.claimantId == p)
                        break;
                    idIdx++;
                }
            }
            return idIdx;
        }

        private bool LoadStatus()
        {
           
            this.cboLitigationStatus.DataSource = LoadStatus(TypeTable.getTypeTableRows("Litigation_Status")); // TypeTable.getTypeTableRowsDDL("Litigation_Status");
            this.cboLitigationStatus.DataValueField = "Id";
            this.cboLitigationStatus.DataTextField = "Description";
            this.cboLitigationStatus.DataBind(); ;
            return true;
        }

        private IEnumerable LoadStatus(List<Hashtable> list)
        {
            myStatusList = new List<cboList>();
            try
            {
                foreach (Hashtable sData in list)
                {
                    myStatusList.Add(new cboList()
                    {
                        id = (int)sData["Id"],
                        imageIndex = (int)sData["Image_Index"],
                        description = (string)sData["Description"]
                    });
                }
                return myStatusList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myStatusList;
            }
        }

        private bool LoadDefenseAttorney()
        {

            this.myDefenseList = mm.LoadClaimants(Vendor.VendorsOfType(Litigation.defenseAttorneyType), 4);
            this.cboLitigationDefenseAttorney.DataSource = this.myDefenseList;
            this.cboLitigationDefenseAttorney.DataValueField = "claimantId";
            this.cboLitigationDefenseAttorney.DataTextField = "fullName";
            this.cboLitigationDefenseAttorney.DataBind();
            return true;
        }

        private bool LoadPlaintiffAttorney()
        {
            this.myProsecuteList = mm.LoadClaimants(Vendor.VendorsOfType(Litigation.plaintiffAttorneyType), 4);
            this.cboLitigationPlaintiffAttorney.DataSource = this.myProsecuteList;
            this.cboLitigationPlaintiffAttorney.DataValueField = "claimantId";
            this.cboLitigationPlaintiffAttorney.DataTextField = "fullName";
            this.cboLitigationPlaintiffAttorney.DataBind();
            return true;
        }

        private bool LoadLocation()
        {
            this.myLocationList = mm.LoadClaimants(Vendor.VendorsOfType(Litigation.courtLocationType), 4);
            this.cboLitigationCourtLocation.DataSource = this.myLocationList;
            this.cboLitigationCourtLocation.DataValueField = "claimantId";
            this.cboLitigationCourtLocation.DataTextField = "fullname";
            this.cboLitigationCourtLocation.DataBind();
            return true;
        }

        private bool LoadReserves()
        {
            myReserveList = mm.LoadReserves(mm.GetReservesHashByClaim(this.m_Claim.Id));
            this.grReserves.DataSource = myReserveList; //mm.GetReservesByClaim(this.m_Claim.Id);
            this.grReserves.DataBind();
            return true;
        }

        private bool LoadDefendants()
        {
            this.grDefendants.DataSource = LoadDefendants(this.m_Litigation.Defendents());
            this.grDefendants.DataBind();
            return true;
        }

        public string FormatPopupDefendants(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:popup_window4" + claim_id + "=window.open('frmDefendant.aspx?mode=" + mode + "&claim_id=" + claim_id + "&litigation_id=0','popup_window4" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');popup_window4" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:popup_window4" + claim_id + "=window.open('frmDefendant.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','popup_window4" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');popup_window4" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }

        private IEnumerable LoadDefendants(List<Hashtable> list)
        {
            myDefendantList = new List<cboList>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    myDefendantList.Add(new cboList()
                    {
                        id = (int)dData["Litigation_Defendent_Id"],
                        description = (string)dData["Name"],
                        vendorId = (int)dData["Vendor_Id"]
                    });
                }
                return myDefendantList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDefendantList;
            }
        }


       

        class cboList
        {
            public int id { get; set; }
            public int imageIndex { get; set; }
            public string description { get; set; }
            public int vendorId { get; set; }

        }

        //protected void btnDefendantsDelete_Click(object sender, EventArgs e)
        //{
        //    mm.UpdateLastActivityTime();

        //    if (System.Windows.MessageBox.Show("Are you sure you Want to delete this defendant?", "Delete Defendant", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
        //        return;



        //    int defendantid = Convert.ToInt32(grDefendants.DataKeys[e.RowIndex].Value);

        //    ml.deleteRow("Litigation_Defendent", defendantid);
        //    LoadDefendants();

        //}

        protected void grReserves_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnDefendantsAdd2_Click(object sender, EventArgs e)
        {
            btnOK_Click(null, null);

            Session["referer2"] = Request.Url.ToString();

            string url = "frmAddDefendant.aspx?mode=c&claim_id=" + claim_id + "&litigation_id=" + Request.QueryString["litigation_id"].ToString();
            string s = "window.open('" + url + "', 'popup_windowLAD', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=300, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowLAD", s, true);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //validate status selected
                if (this.cboLitigationStatus.SelectedIndex < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a litigation status.');</script>");
                    //System.Windows.MessageBox.Show("Please select a litigation status.", "No Litigation Status Selected", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.cboLitigationStatus.Focus();
                    return;
                }

                //validate status selected
                if (this.dpFileDate.Text.Length  < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter Date Filed.');</script>");
                    //System.Windows.MessageBox.Show("Please enter Date Filed.", "Date Filed Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.dpFileDate.Focus();
                    return;
                }

                if (this.dpServeDate.Text.Length < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter Date Served.');</script>");
                    //System.Windows.MessageBox.Show("Please enter Date Served.", "Served Date Missing", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.dpServeDate.Focus();
                    return;
                }


                //validate reserves selected
                bool resSelected = false;
                for (int i = 0; i < grReserves.Rows.Count; i++)
                {

                    System.Web.UI.WebControls.CheckBox mycheck = ((System.Web.UI.WebControls.CheckBox)grReserves.Rows[i].FindControl("cbSelect"));

                    if (mycheck.Checked)
                    {
                        resSelected = true;
                        break;
                    }
                }
                if (!resSelected)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Must associate at least one reserve with this litigation.');</script>");
                    //System.Windows.MessageBox.Show("Must associate at least one reserve with this litigation.", "No Reserve Selected", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.grReserves.Focus();
                    return;
                }


                if (!modLogic.isNumeric(txtAmount.Text.Replace("$", "").Replace(",", "")))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The resolution amount is invalid.');</script>");
                    //System.Windows.Forms.MessageBox.Show("The resolution amount is invalid.", "Invalid Resolution Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    txtAmount.Focus();
                    return;
                }

                if (litigation_id == 0)
                    m_Litigation = new Litigation();
                else
                    m_Litigation = new Litigation(litigation_id);
                //create new litigation or denote change

                m_Litigation.ClaimId = this.claim_id;
                m_Litigation.LitigationStatusId = Convert.ToInt32(this.cboLitigationStatus.SelectedValue);
                m_Litigation.DefenseAttorneyId = Convert.ToInt32(this.cboLitigationDefenseAttorney.SelectedValue);
                m_Litigation.PlaintiffAttorneyId = Convert.ToInt32(this.cboLitigationPlaintiffAttorney.SelectedValue);
                m_Litigation.CourtLocationId = Convert.ToInt32(this.cboLitigationCourtLocation.SelectedValue);
                m_Litigation.DateFiled = Convert.ToDateTime(this.dpFileDate.Text);
                m_Litigation.DateServed = Convert.ToDateTime(this.dpServeDate.Text);
                m_Litigation.Resolution_Amount = double.Parse(this.txtAmount.Text.Trim().Replace("$", "").Replace(",", "")); //System.Convert.ToDouble(this.txtAmount.Text);
                m_Litigation.Comments = this.txtComments.Text;
               int myLitID =  m_Litigation.Update();



                //insert reserve if ! exist

                //foreach (ReserveGrid rg in myReserveList)
                //{
                mm.DeleteLitigationReserves(myLitID);
                for (int i = 0; i < grReserves.Rows.Count; i++)
                {
                    bool exists = false;

                    System.Web.UI.WebControls.CheckBox mycheck = ((System.Web.UI.WebControls.CheckBox)grReserves.Rows[i].FindControl("cbSelect"));
                    

                    if (mycheck.Checked == true)  //Reserve already exists
                    {
                        
                        foreach (Hashtable res in this.m_Litigation.Reserves())
                        {
                            if ((grReserves.Rows[i].Cells[1].Text == res["Reserve_Type"].ToString()) && (grReserves.Rows[i].Cells[2].Text == res["Claimant"].ToString()))  //reserve doesn't exists
                            {
                                exists = true;
                            }
                        }

                        if (exists == false)
                        {
                            Hashtable newRow = new Hashtable();
                            newRow["Litigation_Id"] = myLitID;
                            newRow["Reserve_Id"] = Convert.ToInt32(grReserves.DataKeys[i].Value);
                            ml.InsertRow(newRow, "Litigation_Reserve");
                        }

                    }
                }

                Hashtable parms = new Hashtable();
                parms.Add("Litigation_Status", m_Litigation.LitigationStatus.Description);
                parms.Add("Claim", m_Litigation.Claim.DisplayClaimId);
                parms.Add("Defense_Attorney", m_Litigation.DefenseAttorney.Name);
                parms.Add("Plaintiff_Attorney", m_Litigation.PlaintiffAttorney.Name);
                parms.Add("Court_Location", m_Litigation.CourtLocation.Name);
                parms.Add("Date_Filed", Convert.ToDateTime(this.dpFileDate.Text));
                parms.Add("Date_Served", Convert.ToDateTime(this.dpServeDate.Text));
                parms.Add("Resolution_Amount", double.Parse(this.txtAmount.Text.Trim().Replace("$", "").Replace(",", "")));
                parms.Add("Comments", this.txtComments.Text);



                if (Convert.ToInt32(Request.QueryString["litigation_id"]) == 0)  //new
                {   //'This is a new litigation
                    //'Fire the litigation created event
                    m_Litigation.Claim.FireEvent((int)modGeneratedEnums.EventType.Litigation_Created, parms);
                }
                else
                {
                    //'Not a new litigation
                    //'Fire the litigation updated event
                    m_Litigation.Claim.FireEvent((int)modGeneratedEnums.EventType.Litigation_Updated, parms);
                }

               
                    ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);


            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void grDefendants_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //if (System.Windows.MessageBox.Show("Are you sure you Want to delete this defendant?", "Delete Defendant", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
            //    return;
            


            int defendantid = Convert.ToInt32(grDefendants.DataKeys[e.RowIndex].Value);

            ml.deleteRow("Litigation_Defendent", defendantid);
            LoadDefendants();
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmVehicles : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        public int vehicleId = 0;       //Vehicle ID added/to edit.  Input and Output Parameter.
        public bool okPressed = false;      //Was OK pressed?  Output parameter
        public MedCsxLogic.Claim myClaim = null;
        public int myClaimId = 0;
        private Hashtable oldRow = new Hashtable();
        private Hashtable newRow = new Hashtable();
        public string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            myClaimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            string tmpid = Regex.Replace(Request.QueryString["vehicle_id"].ToString(), "[^0-9]", "");
            vehicleId = Convert.ToInt32(tmpid);

            if (Session["referer2"] != null)
            {
                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();

            this.myClaim = new MedCsxLogic.Claim(myClaimId);
            if (!Page.IsPostBack)
            {
               


                mm.UpdateLastActivityTime();

                mf.LoadStateDDL(this.cboVehicleState);     //load license plate state combo box
                mf.LoadStateDDL(this.cboLocationState);    //load location state combo box
                mf.LoadStateDDL(this.tbPayeeState);

                if (this.vehicleId == 0)
                {   //insert vehicle
            
                    this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - Add Vehicle";
                    this.tbPayeeCorrectedPhone.Enabled = true;
                    this.btnVehicleOK.Text = "Add";

                }
                else
                {
                    this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - Edit Vehicle";
                    this.tbPayeeCorrectedPhone.Enabled = false;

                    this.oldRow = ml.getRow("Vehicle", vehicleId);
                    this.newRow = ml.copyDictionary(oldRow);
                    if ((int)oldRow["Policy_Vehicle_Id"] != 0)
                    {
                        /*this is a personal lines vehicle
                         * don't allow changes to specific details - messes up PLA refresh*/
                        this.tbVehicleYear.Enabled = false;
                        this.tbVehicleMake.Enabled = false;
                        this.tbVehicleModel.Enabled = false;
                        this.tbVehicleVIN.Enabled = false;
                    }

                    this.PopulateForm(this.oldRow);
                    if ((string)this.oldRow["Loss_Payee_Name1"] == "")
                        this.tbPayeeCorrectedPhone.Enabled = true;
                }
            }
          


            //try
            //{
               
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}

        }

        private void PopulateForm(Hashtable vData)
        {
            try
            {
                Vehicle myVehicle = new Vehicle((int)vData["Vehicle_Id"]);
                this.tbVehicleYear.Text = myVehicle.YearMade;
                this.tbVehicleMake.Text = myVehicle.Manufacturer;
                this.tbVehicleModel.Text = myVehicle.Model;
                this.tbVehicleLicNo.Text = myVehicle.LicensePlateNo;
                this.cboVehicleState.SelectedValue = myVehicle.LicensePlateStateId.ToString();
                this.tbVehicleVIN.Text = myVehicle.VIN;
                this.tbVehicleColor.Text = myVehicle.Color;
                this.chkVehicleDrivable.Checked = myVehicle.isDrivable;

                this.tbLocationFName.Text = myVehicle.LocationContactFirstName;
                this.tbLocationLName.Text = myVehicle.LocationContactLastName;
                this.tbLocationAddress1.Text = myVehicle.LocationAddress1;
                this.tbLocationAddress2.Text = myVehicle.LocationAddress2;
                this.tbLocationCity.Text = myVehicle.LocationCity;
                this.cboLocationState.SelectedValue = myVehicle.LocationStateId.ToString();
                this.tbLocationZipcode.Text = myVehicle.LocationZipcode;
                this.tbLocationEmail.Text = myVehicle.LocationContactEmailAddress;
                this.tbLocationPhone1.Text = myVehicle.LocationPhone1;
                this.tbLocationPhone2.Text = myVehicle.LocationPhone2;

                this.tbPayeeName.Text = myVehicle.LossPayeeName1;
                this.tbPayeeAddress1.Text = myVehicle.LossPayeeAddress1;
                this.tbPayeeAddress2.Text = myVehicle.LossPayeeAddress2;
                this.tbPayeeCity.Text = myVehicle.LossPayeeCity;
                this.tbPayeeState.Text = mm.GetStateForId(myVehicle.LossPayeeStateId);
                this.tbPayeeZip.Text = myVehicle.LossPayeeZipcode;
                this.tbPayeeLoanNo.Text = myVehicle.LossPayeeLoanNo;
                this.tbPayeePhone.Text = myVehicle.LossPayeePhone;
                this.tbPayeeFax.Text = myVehicle.LossPayeeFax;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }


        }
        private void PopulateFromForm(Hashtable vData)
        {
            try
            {
                vData["Vehicle_Id"] = Request.QueryString["vehicle_id"].ToString();
                vData["Claim_Id"] = Convert.ToInt32(Request.QueryString["claim_id"]);
                vData["Year_Made"] = this.tbVehicleYear.Text;
                vData["Manufacturer"] = this.tbVehicleMake.Text;
                vData["Model"] = this.tbVehicleModel.Text;
                vData["License_Plate_No"] = this.tbVehicleLicNo.Text;
                vData["License_Plate_State_Id"] = Convert.ToInt32(this.cboVehicleState.SelectedValue);
                vData["VIN"] = this.tbVehicleVIN.Text;
                vData["Color"] = this.tbVehicleColor.Text;
                vData["Is_Drivable"] = (bool)this.chkVehicleDrivable.Checked;

                vData["Location_Contact_First_Name"] = this.tbLocationFName.Text;
                vData["Location_Contact_Last_Name"] = this.tbLocationLName.Text;
                vData["Location_Address1"] = this.tbLocationAddress1.Text;
                vData["Location_Address2"] = this.tbLocationAddress2.Text;
                vData["Location_City"] = this.tbLocationCity.Text;
                vData["Location_State_Id"] = this.cboLocationState.SelectedValue;
                vData["Location_Zipcode"] = this.tbLocationZipcode.Text;
                vData["Location_Contact_Email_Address"] = this.tbLocationEmail.Text;
                vData["Location_Phone1"] = this.tbLocationPhone1.Text.Replace("(","").Replace(")", "-");
                vData["Location_Phone2"] = this.tbLocationPhone2.Text;

                vData["Loss_Payee_Name1"] = this.tbPayeeName.Text;
                vData["Loss_Payee_Address1"] = this.tbPayeeAddress1.Text;
                vData["Loss_Payee_Address2"] = this.tbPayeeAddress2.Text;
                vData["Loss_Payee_City"] = this.tbPayeeCity.Text;
                vData["Loss_Payee_State_Id"] = mm.GetStateId(this.tbPayeeState.Text);
                vData["Loss_Payee_Zipcode"] = this.tbPayeeZip.Text;
                vData["Loss_Payee_Loan_No"] = this.tbPayeeLoanNo.Text;
                vData["Loss_Payee_Phone"] = this.tbPayeePhone.Text;
                vData["Loss_Payee_Fax"] = this.tbPayeeFax.Text;
                vData["Loss_Payee_Corrected_Phone"] = this.tbPayeeCorrectedPhone.Text;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }
        //public void btnChange_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();

        //        if (!string.IsNullOrWhiteSpace(this.tbLocationEmail.Text) && (!mm.ValidEmailAddress(this.tbLocationEmail.Text)))
        //        {
        //            //MessageBox.Show("The EMail Address Entered is not Valid");
        //            this.tbLocationEmail.Focus();
        //            return;
        //        }

        //        bool dupVehicle = false;
        //        foreach (Hashtable h in this.myClaim.Vehicles)
        //        {
        //            Vehicle dVeh = new Vehicle((int)h["Vehicle_Id"]);
        //            if ((dVeh.YearMade == (string)this.tbVehicleYear.Text) &&
        //                (dVeh.Manufacturer == (string)this.tbVehicleMake.Text) &&
        //                (dVeh.Model == (string)this.tbVehicleModel.Text) &&
        //                (dVeh.VIN == (string)this.tbVehicleVIN.Text) &&
        //                (dVeh.LicensePlateNo == (string)this.tbVehicleLicNo.Text))
        //            {
        //                dupVehicle = true;
        //                break;
        //            }
        //        }

        //        if (dupVehicle)
        //        {
        //            lblAlert.Text = "This vehicle has already been added to this claim.  Do you wish to add it again?";
        //            return;
        //        }

        //        Vehicle v = new Vehicle(vehicleId);
        //        string thisVeh = "";
        //        if (!v.Set_ISO_Code(this.tbVehicleMake.Text, this.tbVehicleModel.Text, ref thisVeh))
        //        {
        //            lblAlert.Text = "The system has found a vehicle close to the one entered.  Is " + thisVeh.Trim() + " the same make and model?";

        //            string[] makeModel = thisVeh.Split(' ');
        //            string make = makeModel[0];
        //            string model = "";
        //            for (int i = 1; i < makeModel.Length; i++)
        //                model += makeModel[i] + " ";

        //            model = model.Trim();
        //            bool setCode = v.Set_ISO_Code(make, model, ref thisVeh);
        //            //TODO
        //            //if (MessageBox.Show("The system has found a vehicle close to the one entered.  Is " + thisVeh.Trim() + " the same make and model?", "Vehicle Code Not Found", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
        //            //{
        //            //    MessageBox.Show("Please enter a valid Make and Model.", "Invalid Vehicle", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
        //            //    this.tbVehicleMake.Focus();
        //            //    return;
        //            //}
        //            //else
        //            //{
        //            //    string[] makeModel = thisVeh.Split(' ');
        //            //    string make = makeModel[0];
        //            //    string model = "";
        //            //    for (int i = 1; i < makeModel.Length; i++)
        //            //        model += makeModel[i] + " ";

        //            //    model = model.Trim();
        //            //    bool setCode = v.Set_ISO_Code(make, model, ref thisVeh);
        //            //}
        //        }

        //        this.okPressed = true;      //OK button pressed

        //        //update database
        //        this.PopulateFromForm(this.newRow);
        //        newRow["License_Plate_State_Id"] = this.cboVehicleState.SelectedIndex + 1;
        //        newRow["Location_State_Id"] = this.cboLocationState.SelectedIndex + 1;
        //        newRow["Claim_Id"] = this.myClaim.Id;
        //        newRow["Manufacture_Code"] = v.ManufactureCode;
        //        newRow["Model_Code"] = v.ModelCode;

        //        if (vehicleId == 0)
        //        {   //insert vehicle
        //            vehicleId = ml.InsertRow(newRow, "Vehicle");
        //        }
        //        else
        //            ml.updateRow("Vehicle", newRow, oldRow);
        //        //update vehicle

        //        //check for possible duplicate claims
        //        mf.CheckForPossibleDuplicateClaims(this.myClaim, PossibleDuplicateClaimMode.Add_Update_Vehicle);


        //        TextBox txt = this.Parent.FindControl("lblTitle") as TextBox;
        //        txt.Visible = false;

        //        DropDownList ddl = this.Parent.FindControl("cboGIClDetailVehicle") as DropDownList;
        //        int ddlselected = Convert.ToInt32(ddl.SelectedValue);
        //        ((ViewClaim)this.Page).ReloadVehicleComboBox();
        //        ddl.SelectedValue = ddlselected.ToString();
        //        //Parent.ReloadVehicleComboBox();
        //        Parent.FindControl("pnlGI").Visible = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //    }
        //}



        protected void btnAddEditSIU_Click(object sender, EventArgs e)
        {
            //fraud.Visible = true;
            //loadFraud();
            Session["referer2"] = Request.Url.ToString();
            string url3 = "frmfraud.aspx?mode=4&claim_id=" + myClaimId + "&perpetrator=" + vehicleId;
            string s3 = "window.open('" + url3 + "', 'popup_windowFraud', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowFraud", s3, true);
        }

       

        protected void btnSetToDriversAddress_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (this.vehicleId == 0)
                    return;

                Vehicle v = new Vehicle(this.vehicleId);
                Person d = v.DriverPerson;

                if (d == null)
                    return;

                if (d.Id == 0)
                    return;

                //populate fields
                this.tbLocationAddress1.Text = d.Address1;
                this.tbLocationAddress2.Text = d.Address2;
                this.tbLocationCity.Text = d.City;
                this.tbLocationEmail.Text = d.Email;
                this.tbLocationFName.Text = d.FirstName;
                this.tbLocationLName.Text = d.LastName;
                this.tbLocationPhone1.Text = mm.phoneStrip(d.HomePhone);
                this.tbLocationPhone2.Text = mm.phoneStrip(d.WorkPhone);
                this.tbLocationZipcode.Text = d.Zipcode;
                if (d.StateId > 0)
                    this.cboLocationState.SelectedValue = d.StateId.ToString();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public bool VIN_Checksum(string VIN)
        {
            char[] vin = VIN.ToUpper().ToCharArray();

            if (vin.Length != 17)
            {
                lblVinMessage.Visible = true;
                lblVinMessage.Text = "VIN is not valid.";
                return false;
            }
            int vValue = 0;
            int remainder;
            int sumProduct = 0;
            string ckSum;
            char pos9 = ' ';
            for (int pos = 0; pos < vin.Length; pos++)
            {
                int product = 0;

                if (pos == 8)
                    pos9 = vin[pos];
                else
                {
                    if (!int.TryParse(vin[pos].ToString(), out vValue))
                        vValue = Transliterate(vin[pos]);

                    product = vValue * VIN_Weight(pos + 1);
                }
                sumProduct += product;
            }
            remainder = sumProduct % 11;

            ckSum = remainder == 10 ? "X" : remainder.ToString();

            lblVinMessage.Visible = false;
            return ckSum == pos9.ToString();
        }

        private int Transliterate(char p)
        {
            switch (p)
            {
                case 'A':
                case 'J':
                    return 1;
                case 'B':
                case 'K':
                case 'S':
                    return 2;
                case 'C':
                case 'L':
                case 'T':
                    return 3;
                case 'D':
                case 'M':
                case 'U':
                    return 4;
                case 'E':
                case 'N':
                case 'V':
                    return 5;
                case 'F':
                case 'W':
                    return 6;
                case 'G':
                case 'P':
                case 'X':
                    return 7;
                case 'H':
                case 'Y':
                    return 8;
                case 'R':
                case 'Z':
                    return 9;
                default:
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "The VIN has invalid character " + p.ToString() + ".";
                    return 0;
            }
        }

        private int VIN_Weight(int p)
        {
            switch (p)
            {
                case 1:
                case 11:
                    return 8;
                case 2:
                case 12:
                    return 7;
                case 3:
                case 13:
                    return 6;
                case 4:
                case 14:
                    return 5;
                case 5:
                case 15:
                    return 4;
                case 6:
                case 16:
                    return 3;
                case 7:
                case 17:
                    return 2;
                case 8:
                    return 10;
                case 10:
                    return 9;
                default:
                    return 0;
            }
        }

        protected void tbVehicleVIN_TextChanged(object sender, EventArgs e)
        {

            if (tbVehicleVIN.Text.Length > 0)
            {
                if (tbVehicleVIN.Text.Contains(" "))
                {
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "The VIN field is not to be used as a note.  This will cause issues within the system.";
                    tbVehicleVIN.Focus();
                    btnVehicleOK.Enabled = false;
                }

                else if (tbVehicleVIN.Text.Length <= 7)
                {
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "If VIN is unknown, leave the VIN field blank.";
                    tbVehicleVIN.Focus();
                    btnVehicleOK.Enabled = false;
                }

                else if (VIN_Checksum(tbVehicleVIN.Text) == false)
                {
                    lblVinMessage.Visible = true;
                    lblVinMessage.Text = "The VIN number is invalid.";
                    this.tbVehicleVIN.Focus();
                    btnVehicleOK.Enabled = false;
                }
                else
                {
                    lblVinMessage.Visible = false;
                    btnVehicleOK.Enabled = true;
                }

            }
            else
            {
                lblVinMessage.Visible = false;
                btnVehicleOK.Enabled = true;
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Parent.FindControl("pnlGI").Visible = false;
        }

        protected void txtSetToOwnersAddress_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (this.vehicleId == 0)
                    return;

                Vehicle v = new Vehicle(this.vehicleId);
                Person o = v.DriverPerson;

                if (o == null)
                    return;

                if (o.Id == 0)
                    return;

                //populate fields
                this.tbLocationAddress1.Text = o.Address1;
                this.tbLocationAddress2.Text = o.Address2;
                this.tbLocationCity.Text = o.City;
                this.tbLocationEmail.Text = o.Email;
                this.tbLocationFName.Text = o.FirstName;
                this.tbLocationLName.Text = o.LastName;
                this.tbLocationPhone1.Text = mm.phoneStrip(o.HomePhone);
                this.tbLocationPhone2.Text = mm.phoneStrip(o.WorkPhone);
                this.tbLocationZipcode.Text = o.Zipcode;
                if (o.StateId > 0)
                    this.cboLocationState.SelectedValue = o.StateId.ToString();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnVehicleOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!string.IsNullOrWhiteSpace(this.tbLocationEmail.Text) && (!mm.ValidEmailAddress(this.tbLocationEmail.Text)))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('he Email Address Entered is not Valid.');</script>");
                    //MessageBox.Show("The Email Address Entered is not Valid");
                    this.tbLocationEmail.Focus();
                    return;
                }

                bool dupVehicle = false;
                foreach (Hashtable h in this.myClaim.Vehicles)
                {
                    Vehicle dVeh = new Vehicle((int)h["Vehicle_Id"]);
                    if ((dVeh.YearMade == (string)this.tbVehicleYear.Text) &&
                        (dVeh.Manufacturer == (string)this.tbVehicleMake.Text) &&
                        (dVeh.Model == (string)this.tbVehicleModel.Text) &&
                        (dVeh.VIN == (string)this.tbVehicleVIN.Text) &&
                        (dVeh.LicensePlateNo == (string)this.tbVehicleLicNo.Text))
                    {
                        dupVehicle = true;
                        break;
                    }
                }

                if (vehicleId == 0)  //this is an update
                { 
                    if (dupVehicle)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This vehicle has already been added to this claim.');</script>");
                        //if (MessageBox.Show("This vehicle has already been added to this claim.  Do you wish to add it again?", "Duplicate Vehicle", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                        //{
                        //    //MessageBox.Show("This vehicle has already been added to this claim.", "Duplicate Vehicle", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);

                        //    //ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
                        //    return;
                        //}
                        return;
                    }
                }

                Vehicle v = new Vehicle(vehicleId);
                string thisVeh = "";


       
                //if (!this.tbVehicleMake.Text.ToLower().Contains("unknown"))  // if unknown don't verify
                //{ 
                //    if (!v.Set_ISO_Code(this.tbVehicleMake.Text, this.tbVehicleModel.Text, ref thisVeh))
                //    {
                //        if (MessageBox.Show("The system has found a vehicle close to the one entered.  Is " + thisVeh.Trim() + " the same make and model?", "Vehicle Code Not Found", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                //        {
                //            MessageBox.Show("Please enter a valid Make and Model.", "Invalid Vehicle", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                //            this.tbVehicleMake.Focus();
                //            return;
                //        }
                //        else
                //        {
                //            string[] makeModel = thisVeh.Split(' ');
                //            string make = makeModel[0];
                //            string model = "";
                //            for (int i = 1; i < makeModel.Length; i++)
                //                model += makeModel[i] + " ";

                //            model = model.Trim();
                //            bool setCode = v.Set_ISO_Code(make, model, ref thisVeh);
                //        }
                //    }
                //}


                //update database
                this.PopulateFromForm(this.newRow);
                //newRow["License_Plate_State_Id"] = this.cboVehicleState.SelectedValue;
                //newRow["Location_State_Id"] = Convert.ToInt32(this.cboLocationState.SelectedValue);
                //newRow["Claim_Id"] = this.myClaim.Id;
                //newRow["Manufacture_Code"] = tbVehicleMake.Text;
                //newRow["Model_Code"] = tbVehicleModel.Text;
                //newRow["Color"] = tbVehicleModel.Text;

                if (vehicleId == 0)
                {   //insert vehicle
                    this.newRow.Remove("Vehicle_Id");
                    vehicleId = ml.InsertRow(newRow, "Vehicle");
                }
                else
                    ml.updateRow("Vehicle", newRow, oldRow);
                //update vehicle

                //check for possible duplicate claims
                bool dc = mf.CheckForPossibleDuplicateClaims(this.myClaim, PossibleDuplicateClaimMode.Add_Update_Vehicle);

                if (dc)
                {
                    string url = "frmPossibleDuplicateClaims.aspx?mode=c&policy_no=" + this.myClaim.PolicyNo + "&DoL=" + myClaim.DateOfLoss.ToShortDateString() + "&claimID=" + this.myClaim.ClaimId;
                    string s = "window.open('" + url + "', 'popup_windowdup', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=250, copyhistory=no, left=200, top=200');";
                    ClientScript.RegisterStartupScript(this.GetType(), "scriptdup", s, true);

                }

                string control = "";
                if (Request.QueryString["control"] != null)
                    control = Request.QueryString["control"].ToString();


                Session["referer2"] = null;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "&control=" + control + "&vehicleid=" + vehicleId.ToString() + "';window.close();", true);


            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnCarfax_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (this.tbVehicleVIN.Text.Trim().Length == 17)
                {   //display CARFAX report in web browser

                    string url = "http://www.carfax.com/cfm/check_order.cfm?vin=" + this.tbVehicleVIN.Text.Trim() + "&partner=OST_6_DEC_31&zipcode=&ClickID=&SiteID=&suggest=N&FID=&PopUpStatus=0";
                    string s = "window.open('" + url + "', 'popup_windowCF', 'width=900,height=1100,left=100,top=100,resizable=yes');";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                    // System.Diagnostics.Process.Start("http://www.carfax.com/cfm/check_order.cfm?vin=" + this.tbVehicleVIN.Text.Trim() + "&partner=OST_6_DEC_31&zipcode=&ClickID=&SiteID=&suggest=N&FID=&PopUpStatus=0");
                }
                else
                    lblVinMessage.Text = "VIN number is invalid.";

                //invalid VIN
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void tbPayeeZip_TextChanged(object sender, EventArgs e)
        {
            if (this.tbPayeeZip.Text.Length > 0)
            {
                ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
                if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
                {
                    tbPayeeCity.Text = lookup.zCity;
                    tbPayeeState.SelectedValue = null;
                    tbPayeeState.Items.FindByText((string)lookup.zState).Selected = true;
                    tbPayeeCity.Focus();
                }
                else
                    tbPayeeZip.Focus();
            }
        }

        protected void tbLocationZipcode_TextChanged(object sender, EventArgs e)
        {

            if (this.tbLocationZipcode.Text.Length > 0)
            {
                ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
                if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
                {
                    tbLocationCity.Text = lookup.zCity;
                    cboLocationState.SelectedValue = null;
                    cboLocationState.Items.FindByText((string)lookup.zState).Selected = true;
                    tbLocationCity.Focus();
                }
                else
                    tbLocationZipcode.Focus();
            }
        }
    }
}
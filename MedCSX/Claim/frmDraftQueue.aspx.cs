﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmDraftQueue : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue) == false)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }

            if (!Page.IsPostBack)
            {
                mm.UpdateLastActivityTime();
                LoadDrafts();
                sbStatus.Text = grDraftQueue.Rows.Count.ToString() + " Drafts in Queue";
            }
        }

        private void LoadDrafts()
        {
            this.grDraftQueue.DataSource = mm.GetDraftQueue();
            this.grDraftQueue.DataBind();
        }

        protected void tsbPrintDrafts_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue))
                return;

            ArrayList drafts = new ArrayList();

            for (int i = 0; i < grDraftQueue.Rows.Count; i++)
            {
                GridViewRow row = grDraftQueue.Rows[i];
                int ID = Convert.ToInt32(grDraftQueue.DataKeys[row.RowIndex].Value);

                Draft d = new Draft(ID);

                drafts.Add(d);
            }

            try
            { 
                foreach (Draft d in drafts)
                {
                    if (d.InDraftQueue == 1)
                    {

                        foreach (DraftPrint dp in d.ChildObjects(typeof(DraftPrint)))
                        {
                            dp.NonNegotiable = ""; // 'just in case it was viewed and set to non-negotiable
                            dp.Update();

                            string msg = "";
                            if (mf.PrintDraftForDraftPrintId(dp.Id, ref msg))
                            {
                                d.isPrinted = true;
                            }
                            else
                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                            //Byte[] PDFDocument = mf.PrintDraftForDraftPrintId(dp.Id );
                            //d.isPrinted = true;
                            //Response.ContentType = "Application/pdf";
                            //Response.BinaryWrite(PDFDocument);

                            //Session["binaryData"] = PDFDocument;
                            ////Response.Redirect("frmReport.aspx");
                            //string url = "frmReport.aspx";
                            //string s = "window.open('" + url + "', 'popup_windowReport" + d.DraftNo +"', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
                            ////ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
                            //ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport" + d.DraftNo, s, true);



                            ////check that Reader is closed
                            //string msg = "Acrobat Reader is currently opened.  This may create issues in printing drafts." + Environment.NewLine + Environment.NewLine;
                            //msg += "Please close Acrobat Reader and Click OK.  Click Cancel to place the draft in the print queue.";
                            //Process[] acrobats = Process.GetProcessesByName("AcroRd32");
                            //if (acrobats.Count() > 0)
                            //{ 
                            //    //foreach (Process reader in acrobats)
                            //    //{
                            //    //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                            //    //    //if (MessageBox.Show(msg, "Conflicting Applications", MessageBoxButton.OKCancel, MessageBoxImage.Information, MessageBoxResult.Cancel) == MessageBoxResult.Cancel)

                            //    //}
                            //}
                            //else
                            //{ 
                            //    if (mf.PrintDraftForDraftPrintId(dp.Id))
                            //    {
                            //        d.isPrinted = true;
                            //    }
                            //}
                        }

                        d.InDraftQueue = 0;
                        d.ActualPrintDate = System.DateTime.Now;
                        d.ReleasedFromQueueBy = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                        d.Update();

                    }
                }


            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Error occurred while printing the drafts..');</script>");
                //MessageBox.Show("Error occurred while printing the drafts.", "Error While Printing Drafts", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);

        }

        protected void tsbOpenClaim_Click(object sender, EventArgs e)
        {
            try
            {
                if (grDraftQueue.SelectedIndex < 0)
                    return;
                mm.UpdateLastActivityTime();
                int cid = 0;

                cid = Convert.ToInt32(Regex.Replace(grDraftQueue.SelectedRow.Cells[4].Text, "[^0-9]", ""));

                if (cid == 0)
                    return;

                OpenClaim(cid);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void OpenClaim(int claimId, int fileNoteId = 0, int reserveId = 0, int transactionId = 0, bool isClaimNew = false)
        {
            try
            {
                if (claimId == 0)
                    return; //claim ID is invalid

                if (claimId > int.Parse(ModMain.MAX_CLAIM_ID))
                {
                    string str_claimId = claimId.ToString();
                    claimId = int.Parse(str_claimId.Substring(str_claimId.Length - ModMain.MAX_CLAIM_ID.Length, ModMain.MAX_CLAIM_ID.Length));
                }

                MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(claimId);
                //if (!mm.ComanyAllowedForUser(myClaim.CompanyLocation().CompanyId))
                //{    //user not allowed to open this claim
                //    MessageBox.Show("you are not authorized to open " + myClaim.CompanyLocation().Company.Description + " claims.", "Company Not Authorized", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                //    return;
                //}

                bool ok2OpenClaim = false;
                foreach (Alert urgentAlert in mm.urgentAlerts)
                {
                    if (urgentAlert.ClaimId == claimId)
                        ok2OpenClaim = true;
                }

                if (!ok2OpenClaim)
                {    /*claim to open wasn't in Urgent Alert IDs list
                     * see if urgent alerts should be ignored - have they been cleared/responded to?*/
                    foreach (Alert urgentAlert in mm.urgentAlerts)
                    {
                        if (!urgentAlert.AlertsClearedForClaim || !urgentAlert.hasRepliedToAlert)
                        {
                            string msg = "You cannot open this claim until you have dealt with the urgen alert on claim ";
                            msg += urgentAlert.Claim.DisplayClaimId + ".  Before continuing,you have to:" + Environment.NewLine + Environment.NewLine;
                            msg += "1) Clear all your alerts for this claim." + Environment.NewLine;
                            msg += "2) Create a file note for this claim, sending an alert to the urgent alert sender.";

                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                            //System.Windows.MessageBox.Show(msg, "Urgent Alerts", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                            return;
                        }
                    }
                    mm.urgentAlerts = new ArrayList();  //all urgent alerts have been handled

                    if (myClaim.AdjusterId == 0)  //check adjuster ID
                    {
                        foreach (Reserve res in myClaim.Reserves())
                        {
                            if (res.AdjusterId != 0)
                            {
                                myClaim.AdjusterId = res.AdjusterId;
                                myClaim.Update();
                                break;
                            }
                        }
                    }
                }

                if (ml.CurrentUser.AutoOpenImageRightClaims)
                {
                    //if (myClaim == null)  //claim object is null - create one
                    //  MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(claimId);

                    try
                    {
                        mf.getIRDocumentByClaim(myClaim.DisplayClaimId, mf.DetermineClaimsImageRightDrawer(myClaim.CompanyLocation().CompanyId));
                    }
                    catch (Exception ex) { }
                }

                string url = "ViewClaim.aspx?claim_id=" + claimId;
                string win = "window.open('" + url + "','mywindow" + claimId + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100').focus();";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
               

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }
    }
}
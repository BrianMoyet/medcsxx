﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmDenialReason.aspx.cs" Inherits="MedCSX.Claim.frmDenialReason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
           This Reserve is Being Closed Without Payment. Please Enter a Denial Reason Type.
        </div>
       
    </div>
     <div class="row">
        <div class="col-md-3">
            Denial Reason:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboDenialReason" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboDenialReason_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>
      <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnPickOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnPickOK_Click" />
               <asp:Button ID="btnPickCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"   OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

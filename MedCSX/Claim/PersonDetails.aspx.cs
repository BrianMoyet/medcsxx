﻿using MedCSX.App_Code;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class PersonDetails : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;
        private int _personType = 0;        //type of person to add
        private int person_id = 0;  // 'Person Id added or Person Id to edit.  Input and Output Parameter.
        private Boolean siu_flag = false;
        private string mode = "";
        Person myPerson = new Person();
        Person updatePerson = new Person();

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Claimant";
            tmpSub_claim_type_id = claim.sub_claim_type_id;



            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);
            try
            {
                _personType = Convert.ToInt32(Request.QueryString["person_type_id"]);
            }
            catch (Exception ex)
            {}

            mode = Request.QueryString["mode"].ToString();

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (mode == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (mode == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (mode == "u")
                {
                    myPerson.person_id = id.ToString();
                    this.person_id = id;
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (mode == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
           
            myPerson.GetPersonByPersonID(id);
            if (myPerson.isCompany)
                rblClaimantType.SelectedValue = "1";
            txtSuffix.Text = myPerson.suffix;
            txtLast.Text = myPerson.last_name;
            txtMiddleInitial.Text = myPerson.middle_initial;
            txtFirstName.Text = myPerson.first_name;
            txtAddress1.Text = myPerson.address1;
            txtAddress2.Text = myPerson.address2;
            txtCity.Text = myPerson.city;
            ddlState.SelectedValue = myPerson.state_id;
            //ddlDLState.SelectedValue = myPerson.
            txtZip.Text = myPerson.zipcode;
            txtHomePhone.Text = myPerson.home_phone;
            txtWorkPhone.Text = myPerson.work_phone;
            txtCellPhone.Text = myPerson.cell_phone;
            txtOtherPhone.Text = myPerson.other_contact_phone;
            txtFax.Text = myPerson.fax;
            txtEmail.Text = myPerson.email;
            txtDOB.Text = Convert.ToDateTime(myPerson.date_of_birth).ToString("yyyy-MM-dd");
            txtSSn.Text = myPerson.ssn;
            txtDriversLicenseNo.Text = myPerson.drivers_license_no;
            if (myPerson.sex.ToUpper() == "M")
                rblSex.SelectedValue = "1";
            else if (myPerson.sex.ToUpper() == "F")
                rblSex.SelectedValue = "2";
            txtEmployer.Text = myPerson.employer;
            ddlLanguage.SelectedValue = myPerson.language_id;
            txtComments.Text = myPerson.comments;
      

            if (myPerson.HasOtherInsurance)
            {
                otherInsurance.Visible = true;
                txtOtherInsCompany.Text = myPerson.OtherInsCompany;
                txtOtherInsPolicyNumber.Text = myPerson.OtherInsPolicyNo;
                txtOtherInsClaimNo.Text = myPerson.OtherInsClaimNo;
                txtOtherInsAdjuster.Text = myPerson.OtherInsAgentName;
                txtOtherInsAddress.Text  = myPerson.OtherInsAgentAddress;
                txtOtherInsCity.Text  = myPerson.OtherInsAgentCity;
                ddlOtherState.SelectedValue = myPerson.OtherInsAgentStateId;
                txtOtherInsZip.Text = myPerson.OtherInsAgentZipCode;
                txtOtherInsPhone.Text = myPerson.OtherInsAgentPhone;
                txtOtherInsFax.Text = myPerson.OtherInsAgentFax;
                txtOtherInsEmail.Text = myPerson.OtherInsAgentEmail;

            }


        }

        protected void PopulateControls()
        {
            DataTable dtState = new DataTable();
            MedCSX.App_Code.Claim myPersonState = new MedCSX.App_Code.Claim();
            dtState = myPersonState.PopulateDDLStates();
            ddlState.DataSource = dtState;
            ddlState.DataValueField = dtState.Columns[0].ColumnName;
            ddlState.DataTextField = dtState.Columns[1].ColumnName;
            ddlState.DataBind();

            ddlDLState.DataSource = dtState;
            ddlDLState.DataValueField = dtState.Columns[0].ColumnName;
            ddlDLState.DataTextField = dtState.Columns[1].ColumnName;
            ddlDLState.DataBind();

            ddlOtherState.DataSource = dtState;
            ddlOtherState.DataValueField = dtState.Columns[0].ColumnName;
            ddlOtherState.DataTextField = dtState.Columns[1].ColumnName;
            ddlOtherState.DataBind();


            DataTable dtPerson = new DataTable();
            Person myPerson = new Person();
            dtPerson = myPerson.GetLanguages();
            ddlLanguage.DataSource = dtPerson;
            ddlLanguage.DataValueField = dtPerson.Columns[0].ColumnName;
            ddlLanguage.DataTextField = dtPerson.Columns[1].ColumnName;
            ddlLanguage.DataBind();
        }


        protected void rblClaimantType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblClaimantType.SelectedValue == "1")
            {

                Company.Visible = true;
                Person.Visible = false;
            }
            else
            {
                Person.Visible = true;
                Company.Visible = false;
            }
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            this.PopulatePerson();

            if (mode == "c")
            {
                if (siu_flag == false)
                {
                    this.person_id = this.updatePerson.AddPerson();

                    //'Make sure to check for possible duplicate claims again
                    //CheckForPossibleDuplicateClaims(Me.MyClaim, PossibleDuplicateClaimMode.Add_Update_Person)
                }
            }
            else if (mode == "u")
            {
                this.updatePerson.UpdatePerson();
            }


            
           
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "sCancel", "window.opener.location.href = window.opener.location.href;self.close()", true);
           


        }

      
        private void PopulatePerson()
            {
                bool rblclaimant = true;
            this.updatePerson.person_id = id.ToString();
            // Populate the new person object
            if (this.rblClaimantType.SelectedValue == "0")
                rblclaimant = false;
                    
                this.updatePerson.isCompany = rblclaimant;
                //this.siuTable = new Hashtable();

                if (rblClaimantType.SelectedValue == "1")
                {
                    // The new person is a company
                    this.updatePerson.salutation = "";
                    this.updatePerson.first_name = this.txtCompanyName.Text;
                    this.updatePerson.middle_initial = "";
                    // Me.MyPerson.LastName = ModMain.PERSONTABLE_COMPANY_LAST_NAME
                    this.updatePerson.last_name = "";
                    this.updatePerson.suffix = "";
                }
                else
                {
                    // This new person is a person
                    //if (this.siu_flag)
                    //{
                    //    this.siuTable.Add("Salutation", this.txtSalutation.Text);
                    //    this.siuTable.Add("First_Name", this.txtFirstName.Text);
                    //    this.siuTable.Add("Middle_Initial", this.txtMidInit.Text);
                    //    this.siuTable.Add("Last_Name", this.txtLastName.Text);
                    //    this.siuTable.Add("Suffix", this.txtSuffix.Text);
                    //}
                    this.updatePerson.salutation = this.txtPrefix.Text;
                    this.updatePerson.suffix = txtSuffix.Text;
                    this.updatePerson.first_name = this.txtFirstName.Text;
                    this.updatePerson.middle_initial = this.txtMiddleInitial.Text;
                    this.updatePerson.last_name = this.txtLast.Text;
                    
                }

                if (this._personType != 0)
                    this.updatePerson.person_type_id = this._personType.ToString();

                this.updatePerson.address1 = this.txtAddress1.Text;
                this.updatePerson.address2 = this.txtAddress2.Text;
                this.updatePerson.city = this.txtCity.Text;
                this.updatePerson.claim_id = claim_id.ToString();
                this.updatePerson.home_phone = this.txtHomePhone.Text;
                this.updatePerson.work_phone = this.txtWorkPhone.Text;
                this.updatePerson.cell_phone = this.txtCellPhone.Text;
                this.updatePerson.other_contact_phone = this.txtOtherPhone.Text;
                this.updatePerson.zipcode = this.txtZip.Text;
                this.updatePerson.state_id = this.ddlState.SelectedValue;
                this.updatePerson.fax = this.txtFax.Text;
                this.updatePerson.email = this.txtEmail.Text;
                this.updatePerson.drivers_license_no = this.txtDriversLicenseNo.Text;
                if (isDate(this.txtDOB.Text))
                    this.updatePerson.date_of_birth = this.txtDOB.Text.ToString();
                else
                    this.updatePerson.date_of_birth = "1/1/1900";
                this.updatePerson.employer = this.txtEmployer.Text;
                this.updatePerson.sex = this.rblSex.SelectedValue;
                this.updatePerson.ssn = this.txtSSn.Text;
                this.updatePerson.language_id = this.ddlLanguage.SelectedValue;
                this.updatePerson.comments = this.txtComments.Text;

            //TODO fraud 
            if (string.IsNullOrWhiteSpace(this.txtOtherInsCompany.Text) == false)
                this.updatePerson.HasOtherInsurance = true;
            else
                this.updatePerson.HasOtherInsurance = false;
            this.updatePerson.fraudInd = false;
            if (this.updatePerson.HasOtherInsurance)
            {
                this.updatePerson.OtherInsCompany = this.txtOtherInsCompany.Text;
                this.updatePerson.OtherInsPolicyNo = this.txtOtherInsPolicyNumber.Text;
                this.updatePerson.OtherInsClaimNo = this.txtOtherInsClaimNo.Text;
                this.updatePerson.OtherInsAgentName = this.txtOtherInsAdjuster.Text;
                this.updatePerson.OtherInsAgentAddress = this.txtOtherInsAddress.Text;
                this.updatePerson.OtherInsAgentCity = this.txtOtherInsCity.Text;
                this.updatePerson.OtherInsAgentZipCode = this.txtOtherInsZip.Text;
                this.updatePerson.OtherInsAgentStateId = this.ddlOtherState.SelectedValue;
                this.updatePerson.OtherInsAgentPhone = this.txtOtherInsPhone.Text;
                this.updatePerson.OtherInsAgentFax = this.txtOtherInsFax.Text;
                this.updatePerson.OtherInsAgentEmail = this.txtOtherInsEmail.Text;
            }
            if (siu_flag)
                {
                    //this.siuTable.Add("Address1", this.txtAddress1.Text);
                    //this.siuTable.Add("Address2", this.txtAddress2.Text);
                    //this.siuTable.Add("City", this.txtCity.Text);
                    //this.siuTable.Add("Home_Phone", this.txtHomePhone.Text);
                    //this.siuTable.Add("Work_Phone", this.txtWorkPhone.Text);
                    //this.siuTable.Add("Cell_Phone", this.txtCellPhone.Text);
                    //this.siuTable.Add("Other_Contact_Phone", this.txtOtherContactPhone.Text);
                    //this.siuTable.Add("Zipcode", this.txtZip.Text);
                    //this.siuTable.Add("State_Id", this.cboState.SelectedID);
                    //this.siuTable.Add("Fax", this.txtFax.Text);
                    //this.siuTable.Add("Email", this.txtEmail.Text);
                    //this.siuTable.Add("Drivers_License_No", this.txtDriversLicenseNo.Text);
                    //this.siuTable.Add("DateOfBirth", this.MyPerson.DateOfBirth);
                    //this.siuTable.Add("Employer", this.txtEmployer.Text);
                    //this.siuTable.Add("Sex", this.txtSex.Text);
                    //this.siuTable.Add("SSN", this.txtSsn.Text);
                    //this.siuTable.Add("Language_Id", this.cboLanguage.SelectedID);
                    //this.siuTable.Add("Comments", convertString(this.txtOtherComments.Text));
                }
            }

        public bool isDate(object inputDate, DateTime outputDate = default(DateTime))
        {
            //is string date?
            if (inputDate.GetType() == typeof(DateTime))
            {
                outputDate = DateTime.Parse((string)inputDate);
                return true;
            }
            return DateTime.TryParse((string)inputDate, out outputDate);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnAddInsurance_Click(object sender, EventArgs e)
        {
            otherInsurance.Visible = true;
            btnAddInsurance.Visible = false;
        }

        protected void btnSIU_Click(object sender, EventArgs e)
        {

        }

        protected void txtComments_TextChanged(object sender, EventArgs e)
        {
            lblComments.Visible = true;
        }
    }
}
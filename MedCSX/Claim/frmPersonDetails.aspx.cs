﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;
using System.Collections;
using System.Windows;

namespace MedCSX.Claim
{
    public partial class frmPersonDetails : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();
        #region Class Variables
        public int personId = 0;        //person ID added or person ID to edit.  Input/Output parameter.
        public bool okPressed = false;      //was OK pressed?  Output parameter

        private int _claimId = 0;       //claim id to associate person with.  Input parameter
        private int _personType = 0;        //type of person to add
        private Person _myPerson = new Person();
        private MedCsxLogic.Claim _myClaim;
        private List<cboGrid> myDLClassList;
        private string DLClass;
        private string referer = "";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this._claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            this._personType = Convert.ToInt32(Request.QueryString["person_type_id"]);
            this._myClaim = new MedCsxLogic.Claim(_claimId);
            this.personId = Convert.ToInt32(Regex.Replace(Request.QueryString["person_id"].ToString(), "[^0-9]", ""));

            if (Session["referer2"] != null)
            {

                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();

            if (this.personId > 0)
                this._myPerson = new Person(this.personId);

            if (this.personId == 0)     //adding new person
                this.Page.Title = "Claim " + this._myClaim.DisplayClaimId + " - Add Claimant";
            else
            {   //load person/insured - edit mode
                this.Page.Title = "Claim " + this._myClaim.DisplayClaimId + " - Edit Claimant";
            }


            if (!Page.IsPostBack)
            {

                bool DLClass_Load = LoadDLClasses();

                if (this.personId == 0)     //adding new person
                    this.Page.Title = "Claim " + this._myClaim.DisplayClaimId + " - Add Claimant";
                else
                {   //load person/insured - edit mode
                    this.Page.Title = "Claim " + this._myClaim.DisplayClaimId + " - Edit Claimant";

               

                    if (this._myPerson.PolicyIndividualId != 0)
                    {
                        /*This is a personal lines individual - don't allow name change - screws up PLA refresh
                         * Make the name field read only*/
                        this.tbPersonFName.Enabled = false;
                        this.tbPersonLName.Enabled = false;
                        this.tbPersonMI.Enabled = false;
                        this.tbPersonPrefix.Enabled = false;
                        this.tbPersonSuffix.Enabled = false;

                   
                    }
                }

                mf.LoadStateDDL(this.cboAddressState);     //load states
                //this.cboAddressState.Text = "";

                mf.LoadTypeTableDDL(this.cboOtherLanguage, "Language", true);    //load languages
                this.cboOtherLanguage.SelectedValue = "0";            //default language to English

                mf.LoadStateDDL(this.cboOtherIns_State);     //load states for Drivers License
                                                             //this.cboOtherIns_State.Text = "";

                // bool DLClass_Load = LoadDLClasses();     //load classes from Drivers_License_Class

                mf.LoadStateDDL(this.cboOtherDLState);     //load states for Other Insurance
                //this.cboOtherDLState.Text = "";

                try
                {
                    //todo on new person is null
                    if (this._myPerson.isCompany)
                    {
                        this.rbPersonDetailPerson.SelectedValue = "1";
                        Person.Visible = false;
                        Company.Visible = true;
                    }
                    else
                    {
                        this.rbPersonDetailPerson.SelectedValue = "0";
                        Person.Visible = true;
                        Company.Visible = false;
                    }
            

                    //load the form with the person details
                    this.tbAddress1.Text = this._myPerson.Address1;
                    this.tbAddress2.Text = this._myPerson.Address2;
                    this.tbAddressCity.Text = this._myPerson.City;
                    this.cboAddressState.SelectedValue = this._myPerson.StateId.ToString();
                    this.tbAddressZip.Text = this._myPerson.Zipcode;
                    this.tbPersonFName.Text = this._myPerson.FirstName;
                    this.tbPersonLName.Text = this._myPerson.LastName;
                    this.tbPersonMI.Text = this._myPerson.MiddleInitial;
                    this.tbPersonPrefix.Text = this._myPerson.Salutation;
                    this.tbPersonSuffix.Text = this._myPerson.Suffix;
                    this.tbPhoneHome.Text = this._myPerson.HomePhone;
                    this.tbPhoneWork.Text = this._myPerson.WorkPhone;
                    this.tbPhoneCell.Text = this._myPerson.CellPhone;
                    this.tbPhoneFax.Text = this._myPerson.Fax;
                    this.tbPhoneOther.Text = this._myPerson.OtherContactPhone;
                    this.tbOtherEmail.Text = this._myPerson.Email;
                    this.tbOtherSSN.Text = this._myPerson.SSN;
                    this.tbOtherEmployer.Text = this._myPerson.Employer;
                    this.tbOtherDOB.Text = ((DateTime)this._myPerson.DateOfBirth).ToString("yyyy-MM-dd");
                    this.tbOtherDriversLicNo.Text = this._myPerson.DriversLicenseNo;
                    this.tbOtherSex.SelectedValue = this._myPerson.Sex;
                    this.cboOtherLanguage.SelectedIndex = mm.LanguageCBOIndex(this._myPerson.LanguageId);
                    this.tbCompanyName.Text = this._myPerson.FirstName;
                    this.tbOtherComments.Text = this._myPerson.Comments;

<<<<<<< HEAD
                    if (this._myPerson.hasOtherInsurance)
                        otherInsurance.Visible = true;

=======
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    //other insurance
                    this.tbOthIns_Address.Text = this._myPerson.OtherInsuranceAddress;
                    this.tbOthIns_Adjuster.Text = this._myPerson.OtherInsuranceAgentName;
                    this.tbOthIns_City.Text = this._myPerson.OtherInsuranceCity;
                    this.tbOthIns_ClaimNo.Text = this._myPerson.OtherInsuranceClaimNo;
                    this.tbOthIns_Company.Text = this._myPerson.OtherInsuranceCompany;
                    this.tbOthIns_Email.Text = this._myPerson.OtherInsuranceAgentEmail;
                    this.tbOthIns_Fax.Text = this._myPerson.OtherInsuranceAgentFax;
                    this.tbOthIns_Phone.Text = this._myPerson.OtherInsuranceAgentPhone;
                    this.tbOthIns_PolicyNo.Text = this._myPerson.OtherInsurancePolicyNo;
                    this.tbOthIns_Zip.Text = this._myPerson.OtherInsuranceZipCode;
                    this.cboOtherIns_State.SelectedValue = this._myPerson.OtherInsuranceStateId.ToString();
                    //this.cboOtherDLClass.SelectedValue = this._myPerson.DriversLicenseClass;
                }
                catch (Exception ex)
                {
                }
            }

            tbPersonFName.Focus();
        }


        private bool LoadDLClasses()
        {
            //this.cboOtherDLClass.DataSource = LoadDLClasses(TypeTable.getTypeTableRows("Drivers_License_Class"));
            //this.cboOtherDLClass.DataValueField = "id";
            //this.cboOtherDLClass.DataTextField = "description";
            //this.cboOtherDLClass.DataBind();
            return true;
        }

        private IEnumerable LoadDLClasses(List<Hashtable> list)
        {
            myDLClassList = new List<cboGrid>();
            try
            {
                foreach (Hashtable dl in list)
                {
                    myDLClassList.Add(new cboGrid()
                    {
                        id = (int)dl["Id"],
                        description = (string)dl["Description"],
                        imageIdx = (int)dl["Image_Index"]
                    });
                }
                return myDLClassList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDLClassList;
            }
        }


        protected void btnAddInsurance_Click(object sender, EventArgs e)
        {
            otherInsurance.Visible = true;
            btnAddInsurance.Visible = false;
        }

        protected void btnSIU_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString();
            string url3 = "frmfraud.aspx?mode=2&claim_id=" + _claimId + "&perpetrator=" + personId;
            string s3 = "window.open('" + url3 + "', 'popup_windowFraud', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowFraud", s3, true);


        }

        protected void txtComments_TextChanged(object sender, EventArgs e)
        {
            lblComments.Visible = true;
        }

        protected void rbPersonDetailPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbPersonDetailPerson.SelectedValue == "0") //person
            {
                Person.Visible = true;
                Company.Visible = false;
            }
            else
            {
                Person.Visible = false;
                Company.Visible = true;
            }
        }

        protected void btnPersonDetailOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (rbPersonDetailPerson.SelectedValue == "0")
                {   //Person checked
                    if (this.tbPersonFName.Text.Trim() == "")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The first name is required.');</script>");
                        //MessageBox.Show("The first name is required.", "Need First Name", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                        this.tbPersonFName.Focus();
                        return;
                    }
                    else if (this.tbPersonLName.Text.Trim() == "")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The last name is required.');</script>");
                        //MessageBox.Show("The last name is required.", "Need Last Name", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                        this.tbPersonLName.Focus();
                        return;
                    }
                }
                else if (rbPersonDetailPerson.SelectedValue == "1")
                {   //Company checked
                    if (this.tbCompanyName.Text.Trim() == "")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The company name is required.');</script>");
                        //MessageBox.Show("The company name is required.", "Need Company Name", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                        this.tbCompanyName.Focus();
                        return;
                    }
                }

                if ((this.tbOtherDOB.Text.Trim().Length > 0) && !ml.isDate(this.tbOtherDOB.Text.Trim()))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Invalid Date of Birth.');</script>");
                    //MessageBox.Show("Invalid Date of Birth.", "Invalid Date", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    this.tbOtherDOB.Focus();
                    return;
                }

                if ((this.tbOtherComments.Text.Trim().Length > 0) && (string.IsNullOrEmpty(this._myPerson.Comments) || (this.tbOtherComments.Text.Trim().Length != this._myPerson.Comments.Length)))
                {
                    //TODO
                    //string s = "You have entered comments in the contact information of this person.  This information DOES NOT REPLACE a file note associated with the claim.  Nor will the information entered as a comment convert to a file note.  It is information that pertains to the PERSON not the CLAIM." + Environment.NewLine;
                    //s += " Do you wish to continue?";
                    //if (MessageBox.Show(s, "Comments Entered", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    //{
                    //    this.tbOtherComments.Focus();
                    //    return;
                    //}
                }
                this.PopulatePerson();      //populate the person object
                
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


                this.personId = this._myPerson.Update();        //update the person object

<<<<<<< HEAD
=======
               
                if (this.personId != 0)
                    {   //edit mode
                        mm._personIdsForDescriptions = new Hashtable();
                        ml.myScriptFunctions.personalDescriptions.Remove(this.personId);    //update person caches
                    }

=======
                this.personId = this._myPerson.Update();        //update the person object

>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                if (this.personId != 0)
                {   //edit mode
                    mm._personIdsForDescriptions = new Hashtable();
                    ml.myScriptFunctions.personalDescriptions.Remove(this.personId);    //update person caches
                }

                //TODO
<<<<<<< HEAD
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                bool dc = mf.CheckForPossibleDuplicateClaims(this._myClaim, PossibleDuplicateClaimMode.Add_Update_Person);        //check for possible duplicate claims again

                if (dc)
                {
                    string url = "frmPossibleDuplicateClaims.aspx?mode=c&policy_no=" + this._myClaim.PolicyNo + "&DoL=" + _myClaim.DateOfLoss.ToShortDateString() + "&claimID=" + this._myClaim.ClaimId;
                    string s = "window.open('" + url + "', 'popup_windowdup', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=250, copyhistory=no, left=200, top=200');";
                    ClientScript.RegisterStartupScript(this.GetType(), "scriptdup", s, true);

                }

                lblMessage.Text = "Person Saved.";
                //close this form
                //if (Session["referer2"] != null)
                //{
                //    string tmpreferrer = Session["referer2"].ToString();
                //    Session["referer2"] = null;
                //    ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + tmpreferrer + "';window.close();", true);
                //}
                //else

                string control = "";
                if (Request.QueryString["control"] != null)
                    control = Request.QueryString["control"].ToString();

                Session["referer2"] = null;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "&control=" + control  + "&personid=" + personId + "';window.close();", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void PopulatePerson()
        {   //populate the new person object
            if(this.rbPersonDetailPerson.SelectedValue == "0")
                this._myPerson.isCompany = false;
            else
                this._myPerson.isCompany = true;

           if (Convert.ToInt32(Request.QueryString["person_id"]) == 0)
           {
                this._myPerson.PolicyIndividualId = 0;
           }


            if (this._myPerson.isCompany)
            {   //company
                this._myPerson.Salutation = "";
                this._myPerson.FirstName = this.tbCompanyName.Text;
                this._myPerson.MiddleInitial = "";
                this._myPerson.LastName = "";
                this._myPerson.Suffix = "";
                Company.Visible = true;
                Person.Visible = false;
            }
            else
            {   //person
                this._myPerson.Salutation = this.tbPersonPrefix.Text;
                this._myPerson.FirstName = this.tbPersonFName.Text;
                this._myPerson.MiddleInitial = this.tbPersonMI.Text;
                this._myPerson.LastName = this.tbPersonLName.Text;
                this._myPerson.Suffix = this.tbPersonSuffix.Text;
                Company.Visible = false;
                Person.Visible = true;
            }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //if (this._personType != 0)
            //    this._myPerson.PersonTypeId = this._personType;
            try
            {
                if (this._myPerson.PersonTypeId != 0)
                    this._myPerson.PersonTypeId = this._personType;
            }
            catch (Exception ex)
            {
                if (Request.QueryString["Control"] != null)
                {
                    if ((Request.QueryString["Control"].ToString() == "tbGIClDetailInsured") || (Request.QueryString["Control"].ToString() == "cboGIClDetailOwner") || (Request.QueryString["Control"].ToString() == "cboGIClDetailDriver"))
                    {
                        this._myPerson.PersonTypeId = _personType;
                    }
                    else
                    {
                        this._myPerson.PersonTypeId = 0;
                    }
                }
                else
                { 
                    this._myPerson.PersonTypeId = 0;
                }
            }
<<<<<<< HEAD
=======
=======
            if (this._personType != 0)
                this._myPerson.PersonTypeId = this._personType;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //if (Convert.ToInt32(Request.QueryString["person_id"]) == 0)
            //    this._myPerson.PersonTypeId = 0;
            //else
            //    this._myPerson.PersonTypeId = this._personType;

            this._myPerson.ClaimId = _claimId;
            this._myPerson.Address1 = this.tbAddress1.Text;
            this._myPerson.Address2 = this.tbAddress2.Text;
            this._myPerson.City = this.tbAddressCity.Text;
            this._myPerson.StateId = Convert.ToInt32(this.cboAddressState.SelectedValue); //mm.GetStateId(this.cboAddressState.Text);
            this._myPerson.Zipcode = this.tbAddressZip.Text;
            this._myPerson.HomePhone = this.tbPhoneHome.Text;
            this._myPerson.CellPhone = this.tbPhoneCell.Text;
            this._myPerson.WorkPhone = this.tbPhoneWork.Text;
            this._myPerson.Fax = this.tbPhoneFax.Text;
            this._myPerson.OtherContactPhone = this.tbPhoneOther.Text;

            this._myPerson.Email = this.tbOtherEmail.Text;
            this._myPerson.DriversLicenseNo = this.tbOtherDriversLicNo.Text;
            if (!string.IsNullOrWhiteSpace(this.tbOtherDriversLicNo.Text.Trim()))
            {
                this._myPerson.DriversLicenseState = this.cboOtherDLState.Text;

                //string sql = "SELECT [Enum] FROM Drivers_License_Class WHERE Drivers_License_Class_Id=" + cboOtherDLClass.SelectedValue.ToString();
                //DLClass = (string)mm.ExecuteSelectReturnValue(sql);
                //this._myPerson.DriversLicenseClass = DLClass;
            }
            if (ml.isDate(this.tbOtherDOB.Text.Trim()))
                this._myPerson.DateOfBirth = DateTime.Parse(this.tbOtherDOB.Text);
            else
                this._myPerson.DateOfBirth = ml.DEFAULT_DATE;
            this._myPerson.Employer = this.tbOtherEmployer.Text;
            this._myPerson.SSN = this.tbOtherSSN.Text;
            this._myPerson.Sex = this.tbOtherSex.Text;
            this._myPerson.LanguageId = mm.GetLanguageId(this.cboOtherLanguage.Text);
            this._myPerson.Comments = this.tbOtherComments.Text;
<<<<<<< HEAD

            if (tbOthIns_Company.Text.Length > 0)
                this._myPerson.hasOtherInsurance = true;
            else
                this._myPerson.hasOtherInsurance = false;



            this._myPerson.OtherInsuranceCompany = tbOthIns_Company.Text;
            this._myPerson.OtherInsurancePolicyNo = tbOthIns_PolicyNo.Text;
            this._myPerson.OtherInsuranceClaimNo = tbOthIns_ClaimNo.Text;
            this._myPerson.OtherInsuranceAgentName = tbOthIns_Adjuster.Text;
            this._myPerson.OtherInsuranceAddress = tbOthIns_Address.Text;
            this._myPerson.OtherInsuranceCity = tbOthIns_City.Text;
            this._myPerson.OtherInsuranceAgentPhone = tbOthIns_Phone.Text;
            this._myPerson.OtherInsuranceAgentFax = tbOthIns_Fax.Text;
            this._myPerson.OtherInsuranceZipCode = tbOthIns_Zip.Text;
            this._myPerson.OtherInsuranceAgentEmail = tbOthIns_Email.Text;
            this._myPerson.OtherInsuranceStateId = Convert.ToInt32(cboOtherIns_State.SelectedValue);

           if (this.personId == 0)
                this._myPerson.IsFraudulent = false;

=======
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        protected void tbPhoneHome_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbPhoneHome.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbPhoneHome.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbPhoneHome.Text.Length; i++)
            {
                if (Char.IsDigit(tbPhoneHome.Text[i]))
                    p += tbPhoneHome.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbPhoneHome.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbPhoneHome.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }

        protected void tbPhoneWork_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbPhoneWork.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbPhoneWork.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbPhoneWork.Text.Length; i++)
            {
                if (Char.IsDigit(tbPhoneWork.Text[i]))
                    p += tbPhoneWork.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbPhoneWork.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbPhoneWork.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

            tbPhoneCell.Focus();
        }

        protected void tbPhoneCell_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbPhoneCell.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbPhoneCell.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbPhoneCell.Text.Length; i++)
            {
                if (Char.IsDigit(tbPhoneCell.Text[i]))
                    p += tbPhoneCell.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbPhoneCell.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbPhoneCell.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

            tbPhoneFax.Focus();
        }

        protected void tbPhoneFax_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbPhoneFax.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbPhoneFax.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbPhoneFax.Text.Length; i++)
            {
                if (Char.IsDigit(tbPhoneFax.Text[i]))
                    p += tbPhoneFax.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbPhoneFax.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbPhoneFax.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

            tbPhoneOther.Focus();
        }

        protected void tbPhoneOther_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbPhoneOther.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbPhoneOther.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbPhoneOther.Text.Length; i++)
            {
                if (Char.IsDigit(tbPhoneOther.Text[i]))
                    p += tbPhoneOther.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                { 
                    tbPhoneOther.Text = String.Format("{0:(###) ###-####}", val);
                }
                else 
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));
                    
                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbPhoneOther.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

            tbOtherEmail.Focus();
        }

        protected void tbOtherEmail_TextChanged(object sender, EventArgs e)
        {
            string thisEmail = ((TextBox)sender).Text;
            if (thisEmail != "")
            {
                if (!mm.ValidEmailAddress(thisEmail))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Email not valid.');</script>");
                    //MessageBox.Show("The Email Address Entered is not a Valid Email Address.", "Invalid Email", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    this.tbOtherEmail.Focus();
                    return;
                }
            }
        }

        protected void tbAddressZip_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
                if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
                {
                    tbAddressCity.Text = lookup.zCity;
                    cboAddressState.SelectedValue = null;
                    cboAddressState.Items.FindByText((string)lookup.zState).Selected = true; // lookup.zState;
                    tbAddressCity.Focus();
                }
                else
                    tbAddressZip.Focus();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Invalid Zipcode');</script>");
                tbAddressZip.Focus();
            }
        }

       

        protected void pPersonalDetailShowMap_Click(object sender, EventArgs e)
        {
            //mf.DisplayMap(this.tbAddress1.Text.Trim(), this.tbAddressCity.Text.Trim(), this.cboAddressState.SelectedItem.Text, this.tbAddressZip.Text.Trim());
            //Process.Start("http://www.google.com/maps?q=" + address.Trim().Replace(" ", "+") + city.Trim().Replace(" ", "+") + state.Trim().Replace(" ", "+") + zip.Trim().Replace(" ", "+") + "+USA");
            string url = "http://www.google.com/maps?q=" + this.tbAddress1.Text.Trim() + "+" + this.tbAddressCity.Text.Trim() + "+" + this.cboAddressState.SelectedItem.Text + "+" + this.tbAddressZip.Text.Trim() + "+USA";
            string s = "window.open('" + url + "', 'gmap', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=900, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "gmap", s, true);
        }

        protected void tbOthIns_Zip_TextChanged(object sender, EventArgs e)
        {
            if (this.tbOthIns_Zip.Text.Length > 0 )
            { 
                ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
                if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
                {
                    tbOthIns_City.Text = lookup.zCity;
                    cboOtherIns_State.SelectedValue = null;
                    cboOtherIns_State.Items.FindByText((string)lookup.zState).Selected = true;
                    tbOthIns_City.Focus();
                }
                else
                    tbOthIns_Zip.Focus();
            }
        }

        protected void btnPickAddress_Click(object sender, EventArgs e)
        {
            divPickAddress.Visible = true;
            grSelections.DataSource = mm.GetAddressesDT(_claimId);
            grSelections.DataBind();

        }

        protected void grSelections_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tbAddress1.Text = grSelections.SelectedRow.Cells[1].Text.Replace("&nbsp;", "").Replace("&amp;", "").Replace("nbsp;", "");
            this.tbAddress2.Text = grSelections.SelectedRow.Cells[2].Text.Replace("&nbsp;", "").Replace("&amp;", "").Replace("nbsp;", "");
            this.tbAddressCity.Text = grSelections.SelectedRow.Cells[3].Text.Replace("&nbsp;", "").Replace("&amp;", "").Replace("nbsp;", "");
            this.tbAddressZip.Text = grSelections.SelectedRow.Cells[5].Text.Replace("&nbsp;", "").Replace("&amp;", "").Replace("nbsp;", "");
            this.tbPhoneHome.Text = grSelections.SelectedRow.Cells[6].Text.Replace("&nbsp;", "").Replace("&amp;", "").Replace("nbsp;", "");
            this.cboAddressState.SelectedValue = mm.GetStateId(grSelections.SelectedRow.Cells[4].Text).ToString();
            divPickAddress.Visible = false;
        }

        protected void tbOtherSSN_TextChanged(object sender, EventArgs e)
        {
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbOtherSSN.Text.Length; i++)
            {
                if (Char.IsDigit(tbOtherSSN.Text[i]))
                    p += tbOtherSSN.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
               
                    tbOtherSSN.Text = String.Format("{0:000-00-0000}", val);
                
               
            }

            tbOtherSSN.Focus();


        }
<<<<<<< HEAD

        protected void tbOthIns_Phone_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbOthIns_Phone.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbOthIns_Phone.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbOthIns_Phone.Text.Length; i++)
            {
                if (Char.IsDigit(tbOthIns_Phone.Text[i]))
                    p += tbOthIns_Phone.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbOthIns_Phone.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbOthIns_Phone.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }

        protected void tbOthIns_Fax_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.tbOthIns_Fax.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.tbOthIns_Fax.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");

                    return;//MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            //tbPhoneWork.Focus();
            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < tbOthIns_Fax.Text.Length; i++)
            {
                if (Char.IsDigit(tbOthIns_Fax.Text[i]))
                    p += tbOthIns_Fax.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    tbOthIns_Fax.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    tbOthIns_Fax.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }
        }
=======
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
    }
}
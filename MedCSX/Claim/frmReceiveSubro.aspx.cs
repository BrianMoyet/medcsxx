﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

enum SalvageSubro : int
{
    Salvage,
    Subro,
    Void,
    Reserve_Reopen,
    Denial,
    Defendant,
    Plaintiff
};

namespace MedCSX.Claim
{
   
    public partial class frmReceiveSubro : System.Web.UI.Page
    {
        #region Class Variables
        public bool okPressed = false;      //was OK button pressed?  Output parameter
        public int transactionId = 0;       //transaction ID of transaction row inserted.  Output parameter
        public int voidReasonId = 0;        //void reason ID selected.  Output parameter
        public int PersonId = 0,        //person ID selected.  Output parameter
          VendorId = 0;        //vendor ID selected.  Output parameter
        public string DefendantName = "";   //defendant name from ID.  Output parameter

        private int reserveId,              //reserve ID to create transaction for.  Input parameter
                                            //salvageId,                      //salvage_Id of salvage row inserted
                                            //subroId,                        //subro_Id of subro row inserted
            reissueFromId,                  //Id to reissue from 
            m_mode,                         //which screen to present? Subro or Salvage
            reserveTypeId = 0,            //Policy_Coverage_Type_Id
            claim_id = 0;                 //claim_id for reserve ID
                                          //private int vendorId;       //vendor ID corresponding to tax ID entered
        private long draftNo;
        private Reserve m_reserve;
        private ReserveType m_reserveType;
        private MedCsxLogic.Claim m_Claim;
        private List<Tuple<int, Hashtable>> eventTypeIds = new List<Tuple<int, Hashtable>>();
        private string voidReasonTable,
            msg = string.Empty,
            reasonPart = string.Empty,
            noLockPart = ". Proceeding Will Place the Reserve in a Locked State Until Approved By Your Supervisor. Do You Wish to Proceed?",
            lockedPart = ". this would normally add a lock to the reserve, but since it's already locked, the system will just create a note instead. Do You Wish to Proceed?";

       

        private bool doneLoading = false;
        private List<ReasonList> myReasonList;
        private List<ClaimantGrid> myPersonList;
        private List<ClaimantGrid> myVendorList;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", ""));
            m_reserve = new Reserve(this.reserveId);
            this.m_mode = Convert.ToInt32(Request.QueryString["mode"].ToString());
            this.Page.Title = "Claim " + m_reserve.Claim.DisplayClaimId + " - Receive Subro For " + Reserve.ReserveDescription(ref this.reserveId);

            if (!Page.IsPostBack)
            {
               
                //TODO
                //switch (this.m_mode)
                //{
                //    //case (int)SalvageSubro.Salvage:
                //    //    this.Title = "Claim " + m_reserve.Claim.DisplayClaimId + " - Receive Salvage For " + Reserve.ReserveDescription(ref this.reserveId);
                //    //    this.Icon = new BitmapImage(new Uri("../../Forms/Images/tbRedCar.png", UriKind.Relative));
                //    //    tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiSalvage);
                //    //    break;
                //    case (int)SalvageSubro.Subro:
                //        this.Title = "Claim " + m_reserve.Claim.DisplayClaimId + " - Receive Subro For " + Reserve.ReserveDescription(ref this.reserveId);
                //        this.Icon = new BitmapImage(new Uri("../../Forms/Images/icoSmiley.png", UriKind.Relative));
                //        tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiSubro);
                //        break;
                //    case (int)SalvageSubro.Void:
                //        try
                //        {
                //            mm.UpdateLastActivityTime();

                //            this.Title = "Void Reason";
                //            this.Icon = new BitmapImage(new Uri("../../Forms/Images/icoVoid.png", UriKind.Relative));
                //            tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiVoidReason);
                //            bool reasonsLoaded = LoadReasons();

                //            if (this.voidReasonTable == "Draft_Void_Reason")
                //                this.cboVoidReason.SelectedIndex = setSelection((int)modGeneratedEnums.DraftVoidReason.Printing_Problem);
                //            else if (this.voidReasonTable == "Subro_Salvage_Void_Reason")
                //                this.cboVoidReason.SelectedIndex = setSelection((int)modGeneratedEnums.SubroSalvageVoidReason.NSF_Stop_Payment);
                //        }
                //        catch (Exception ex)
                //        {
                //            mf.ProcessError(ex);
                //        }
                //        break;
                //    case (int)SalvageSubro.Reserve_Reopen:
                //        try
                //        {
                //            mm.UpdateLastActivityTime();

                //            this.Title = "Claim " + m_reserve.Claim.DisplayClaimId + " - Reopen " + Reserve.ReserveDescription(ref this.reserveId);
                //            this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuFileOpen.png", UriKind.Relative));
                //            tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiReopenReserve);
                //            LoadReopen();
                //        }
                //        catch (Exception ex)
                //        {
                //            mf.ProcessError(ex);
                //        }
                //        break;
                //    case (int)SalvageSubro.Denial:
                //        try
                //        {
                //            mm.UpdateLastActivityTime();

                //            this.Title = "Denial Reason";
                //            this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuFileExit.png", UriKind.Relative));
                //            tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiDenialReason);
                //            bool reasonsLoaded = LoadReasons();

                //            if (this.reserveId != 0)
                //                this.txtDescription.Text = Reserve.ReserveDescription(ref this.reserveId);
                //        }
                //        catch (Exception ex)
                //        {
                //            mf.ProcessError(ex);
                //        }
                //        break;
                //    case (int)SalvageSubro.Defendant:
                //        try
                //        {
                //            doneLoading = false;
                //            mm.UpdateLastActivityTime();

                //            this.Title = "Select Defendant";
                //            this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuEditUser.png", UriKind.Relative));
                //            tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiDefendant);
                //            bool insLoaded = LoadInsuranceCo();
                //            bool peopleLoaded = false;
                //            if (this.m_Claim.Id != 0)
                //                peopleLoaded = LoadPeople();
                //            doneLoading = true;
                //        }
                //        catch (Exception ex)
                //        {
                //            mf.ProcessError(ex);
                //        }
                //        break;
                //    case (int)SalvageSubro.Plaintiff:
                //        try
                //        {
                //            doneLoading = false;
                //            mm.UpdateLastActivityTime();

                //            this.Title = "Select Plaintiff";
                //            this.lblDefendantType.Text = "The Plaintiff is a:";
                //            this.Icon = new BitmapImage(new Uri("../../Forms/Images/mnuEditUser.png", UriKind.Relative));
                //            tcReceive.SelectedIndex = tcReceive.Items.IndexOf(tiDefendant);
                //            bool insLoaded = LoadInsuranceCo();
                //            bool peopleLoaded = false;
                //            if (this.m_Claim.Id != 0)
                //                peopleLoaded = LoadPeople();
                //            doneLoading = true;
                //        }
                //        catch (Exception ex)
                //        {
                //            mf.ProcessError(ex);
                //        }
                //        break;
                //}
            }

        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (SubroValidation())
                {
                    Subro subro = new Subro();
                    subro.RecoverySource = this.txtSubroId.Text;
                    subro.ReissueFromSubroId = reissueFromId;
                    subro.CheckNo = draftNo;
                    subro.isVoid = false;
                    subro.SubroId = 0;
                    Hashtable subroHash = ConvertToHashTable2(subro);

                    transactionId = Subro.InsertSubro(subroHash, this.reserveId, double.Parse(this.txtSubroAmount.Text));

                    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private Hashtable ConvertToHashTable2(Subro subro)
        {
            Hashtable hash = new Hashtable();
            hash.Add("Check_No", subro.CheckNo);
            hash.Add("Is_Void", subro.isVoid);
            hash.Add("Recovery_Source", subro.RecoverySource);
            hash.Add("Reissued_From_Subro_Id", subro.ReissueFromSubroId);
           

            return hash;
        }

        private bool SubroValidation()
        {
            if (this.txtSubroAmount.Text.Trim() == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Subro Amount is a Required Field.');</script>");
                //MessageBox.Show("Subro Amount is a Required Field");
                if (this.txtSubroAmount.Visible)
                    this.txtSubroAmount.Focus();
                return false;
            }

            if (!modLogic.isNumeric(this.txtSubroAmount.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Subro Amount Must Be Numberic.');</script>");
                //MessageBox.Show("Subro Amount Must Be Numberic");
                if (this.txtSubroAmount.Visible)
                    this.txtSubroAmount.Focus();
                return false;
            }

            if (this.txtSubroAmount.Text == "0")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Subro Amount Must Not Be Zero.');</script>");
                //MessageBox.Show("Subro Amount Must Not Be Zero");
                if (this.txtSubroAmount.Visible)
                    this.txtSubroAmount.Focus();
                return false;
            }

            double subroAmount = double.Parse(this.txtSubroAmount.Text.Trim().Replace("$", "").Replace(",", ""));

            if (m_reserve.LossAmount < subroAmount)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Warning - Subro Amount is Greater than the Total Loss Amount for this Reserve Line.');</script>");
                return false;
                //if (MessageBox.Show("Warning - Subro Amount is Greater than the Total Loss Amount for this Reserve Line.  Do You Want to Continue Anyway?", "Subro Greater Than Loss", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                //{
                //    if (this.txtSubroAmount.Visible)
                //        this.txtSubroAmount.Focus();
                //    return false;
                //}
            }

            if (this.txtSubroId.Text == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Recovery Source is a Required Field.');</script>");
                //MessageBox.Show("Recovery Source is a Required Field");
                if (this.txtSubroId.Visible)
                    this.txtSubroId.Focus();
                return false;
            }

            if (!long.TryParse(this.txtSubroDraftNo.Text, out draftNo))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Check Number Must Be Numeric.');</script>");
                //MessageBox.Show("Check Number Must Be Numeric");
                if (this.txtSubroDraftNo.Visible)
                    this.txtSubroDraftNo.Focus();
                return false;
            }
            return true;
        }


        class ReasonList
        {
            public int id { get; set; }
            public int imageIdx { get; set; }
            public string description { get; set; }
        }
    }
}
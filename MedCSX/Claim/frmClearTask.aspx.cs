﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;


namespace MedCSX.Claim
{
    public partial class frmClearTask : System.Web.UI.Page
    {
        private MedCsxLogic.Claim m_Claim = null;
        private Claimant m_Claimant = null;
        private UserTask m_UserTask = null;
        private int mode = 0;
        private int userTaskID = 0;
        private int claimId = 0;

        private List<cboGrid> myTaskTypeList;
        private List<ClaimantGrid> myTaskClaimantList;
        private List<cboGrid> myTaskReserveList;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        


        protected void Page_Load(object sender, EventArgs e)
        {
           
            userTaskID = Convert.ToInt32(Request.QueryString["user_task_id"]);
            mode = Convert.ToInt32(Request.QueryString["mode"]);
            claimId = Convert.ToInt32(Request.QueryString["claim_id"]);
            m_UserTask = new UserTask(userTaskID);
            lblClaim.Text = m_UserTask.Claim.DisplayClaimId;
            lblTaskType.Text = m_UserTask.UserTaskType().Description;
            txtRequiredText.Text = m_UserTask.Description;

            if (!Page.IsPostBack)
            {
                
                txtComments.Focus();
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            this.m_UserTask.ClearTask(this.txtRequiredText.Text + Environment.NewLine + this.txtComments.Text);
            
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
            //ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href=document.referrer;window.close();", true); 
        }
    }
}
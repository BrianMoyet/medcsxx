﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmPossibleDuplicateClaims : System.Web.UI.Page
    {

        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
       
        public bool okPressed = false;      //was Create Claim pressed?  Output parameter
        private string policyNo;        //policy number to show claims for.  Input parameter
        private int claimId;
        private DateTime dol;   //Date of loss
        private List<DuplicateClaims> myDuplicateClaims;

        protected void Page_Load(object sender, EventArgs e)
        {

            this.policyNo = Request.QueryString["policy_no"].ToString();
            this.claimId = Convert.ToInt32(Request.QueryString["claimID"]);
            this.dol = Convert.ToDateTime(Request.QueryString["DoL"]);

           LoadClaims();

        }

        public void LoadClaims()
        {
            MedCsxLogic.Claim cl = new MedCsxLogic.Claim(claimId);  //create the claim object

            ArrayList claims;
            if (this.claimId == 0)
                claims = cl.ChildObjects(typeof(PossibleDuplicateClaim), "policy_no='" + this.policyNo + "' and date_of_loss='" + this.dol.ToShortTimeString() + "'");
            else
                claims = cl.PossibleDuplicateClaims();

            grPossibleDuplicates.DataSource = LoadClaims(claims);
            grPossibleDuplicates.DataBind();
        }

        private List<DuplicateClaims> LoadClaims(ArrayList a)
        {
            Hashtable claimIds = new Hashtable();
            myDuplicateClaims = new List<DuplicateClaims>();
            try
            {
                foreach (PossibleDuplicateClaim d in a)
                {
                    if (!claimIds.ContainsKey(d.DuplicateClaim()))
                    {
                        claimIds.Add(d.DuplicateClaimId, 0);

                        MedCsxLogic.Claim dc = d.DuplicateClaim();
                        myDuplicateClaims.Add(new DuplicateClaims()
                        {
                            duplicateClaim = dc.DisplayClaimId,
                            claimId = dc.ClaimId,
                            policyNo = dc.PolicyNo,
                            dtLoss = dc.DateOfLoss.ToShortDateString(),
                            matchDescrip = d.Description
                        });
                    }
                }
                return myDuplicateClaims;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDuplicateClaims;
            }
        }

        class DuplicateClaims
        {
            public string duplicateClaim { get; set; }
            public int claimId { get; set; }
            public string policyNo { get; set; }
            public string dtLoss { get; set; }
            public string matchDescrip { get; set; }
        }
    }
}
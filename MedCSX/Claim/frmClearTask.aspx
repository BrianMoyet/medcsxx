﻿<%@ Page Title="Clear Task" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmClearTask.aspx.cs" Inherits="MedCSX.Claim.frmClearTask" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-2">
            Claim:
        </div>
         <div class="col-md-10">
             <asp:Label ID="lblClaim" runat="server" ForeColor="#0033CC"></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-2">
            Task Type:
        </div>
         <div class="col-md-10">
             <asp:Label ID="lblTaskType" runat="server" ForeColor="#0033CC"></asp:Label>
        </div>
    </div>
<%--     <div class="row">
        <div class="col-md-2">
            File Note Type:
        </div>
         <div class="col-md-10">
             <asp:Label ID="Label3" runat="server" ForeColor="#0033CC"></asp:Label>
        </div>
    </div>--%>
     <div class="row">
        <div class="col-md-2">
            Required Text:
        </div>
         <div class="col-md-10">
             <asp:TextBox ID="txtRequiredText" TextMode="MultiLine" runat="server" Height="57px" Width="500px"></asp:TextBox>
        </div>
    </div>
      <div class="row">
        <div class="col-md-2">
            Optional Comments:
        </div>
         <div class="col-md-10">
             <asp:TextBox ID="txtComments" TextMode="MultiLine" runat="server" Height="57px" Width="500px"></asp:TextBox>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click" />
        <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>


﻿<%@ Page Title="Cancel Appraisals" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmCancelAppraisals.aspx.cs" Inherits="MedCSX.Claim.frmCancelAppraisals" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-9">
            <asp:Label ID="lblClaimId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-9">
            <asp:Label ID="lblVehicleId" runat="server" Text=""></asp:Label>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
            Appraiser:
        </div>
        <div class="col-md-9">
            <asp:Label ID="lblAppraiserId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            Reason for Cancellation:
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:TextBox ID="txtCancellationReason" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:CheckBox ID="ckCreateFileNote" Text="Create File Note" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="ckEmailAppraiser" Text="Send Email To Appraiser" runat="server" />
        </div>
    </div>
       <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs" OnClick="btnOK_Click"  Text="OK" TabIndex="14"/>
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

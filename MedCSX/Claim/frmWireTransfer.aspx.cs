﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public enum wireXfer : int
    {
        Transfer,
        Overpayment
    };

    public partial class frmWireTransfer : System.Web.UI.Page
    {

        #region Class Variables
        private Reserve myReserve = null;
        private Reserve res = null;
        private MedCsxLogic.Claim myClaim = null;
        private Dictionary<string, int> myWireTransfers;    //key is description, value is wire transfer ID
        private WireTransfer myWireTransfer = null;         //selected wire transfer

        private int m_mode = 0;
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            this.res = new Reserve(Convert.ToInt32(Request.QueryString["reserve_id"]));
            this.myReserve = res;
            
            this.myClaim = myReserve.Claim;
            this.m_mode = Convert.ToInt32(Request.QueryString["mode"]);

            //display reserve and claim info
            if (this.m_mode == (int)wireXfer.Transfer)
            {
                this.txtClaimId.Text = this.myClaim.DisplayClaimId;
                this.dtLoss.Text = this.myClaim.DateOfLoss.ToShortDateString();
                this.txtReserveId.Text = this.myReserve.Description();
            }
            else
            {
                this.txtWTOClaimId.Text = this.myClaim.DisplayClaimId;
                this.dtWTOLoss.Text = this.myClaim.DateOfLoss.ToShortDateString();
                this.txtWTOReserveId.Text = this.myReserve.Description();

                Transaction tr = new Transaction();
                WireTransfer wt = new WireTransfer();
                foreach (Transaction t in res.ChildObjects(tr.GetType()))
                {
                    if (t.TransactionTypeId == (int)modGeneratedEnums.TransactionType.Wire_Transfer)
                    {
                        int wtid = ((WireTransfer)(t.ChildObjects(wt.GetType())[0])).Id;
                        WireTransfer xfer = new WireTransfer(wtid);
                        this.cboWTO.Items.Add(xfer.Description);
                        if (!this.myWireTransfers.ContainsKey(xfer.Description))
                            this.myWireTransfers.Add(xfer.Description, xfer.Id);
                    }
                }
            }

            string overpayment = "";
            if (this.m_mode == (int)wireXfer.Overpayment)
            {
                overpayment += " Overpayment";
                EnableFields(false);
                if (this.cboWTO.Items.Count > 0)
                    this.cboWTO.SelectedIndex = 0;
            }
            this.Page.Title = "Wire Transfer " + overpayment;

            if (this.m_mode == (int)wireXfer.Transfer)
                this.dpDateSent.Text = DateTime.Today.ToShortDateString();
            else
                this.dpWTODateSent.Text = DateTime.Today.ToShortDateString();
        }

        private void EnableFields(bool p)
        {
            if (this.m_mode == (int)wireXfer.Overpayment)
            {
                this.txtWTOAmount.Enabled = p;
                this.dpWTODateSent.Enabled = p;
                this.txtRecFrom.Enabled = p;
                this.txtWTODraftNo.Enabled = p;
                this.txtWTONotes.Enabled = p;
            }
            else
            {
                this.txtWireAmount.Enabled = p;
                this.dpDateSent.Enabled = p;
                this.txtSendTo.Enabled = p;
                this.txtWireNotes.Enabled = p;
                this.rbExpense.Enabled = p;
                this.rbLoss.Enabled = p;
            }
            this.btnOK.Enabled = p;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!Validate())
                    return;

                //insert transaction
                Transaction tr = new Transaction();
                int tid;

                if (this.m_mode == (int)wireXfer.Overpayment)
                {
                    tr.ReserveId = this.myReserve.Id;
                    tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Wire_Transfer_Overpayment;
                    tr.TransactionAmount = double.Parse(this.txtWTOAmount.Text);
                    tid = tr.Update();

                    //insert offsetting reserve transaction
                    this.myReserve.InsertOffsettingTransaction(tr.TransactionAmount * -1, this.myWireTransfer.Transaction().isLoss);

                    //insert wire transfer overpayment
                    WireTransferOverpayment wto = new WireTransferOverpayment();
                    wto.WireTransferId = this.myWireTransfer.Id;
                    wto.TransactionId = tid;
                    wto.DateReceived = Convert.ToDateTime(this.dpWTODateSent.Text);
                    wto.DraftNo = int.Parse(this.txtWTODraftNo.Text);
                    wto.ReceivedFrom = this.txtRecFrom.Text;
                    wto.Notes = this.txtWTONotes.Text;
                    wto.Update();
                }
                else if (this.m_mode == (int)wireXfer.Transfer)
                {
                    tr.ReserveId = this.myReserve.Id;
                    tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Wire_Transfer;
                    tr.TransactionAmount = double.Parse(this.txtWireAmount.Text) * -1;
                    tr.isLoss = (bool)this.rbLoss.Checked;
                    tid = tr.Update();

                    //insert offsetting reserve transaction
                    this.myReserve.InsertOffsettingTransaction(double.Parse(this.txtWireAmount.Text), (bool)this.rbLoss.Checked);

                    //insert wire transfer 
                    WireTransfer wt = new WireTransfer();
                    wt.TransactionId = tid;
                    wt.DateWired = Convert.ToDateTime(this.dpDateSent.Text);
                    wt.WiredTo = this.txtSendTo.Text;
                    wt.Notes = this.txtWireNotes.Text;
                    wt.Update();
                }
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);      //close form
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboWTO_TextChanged(object sender, EventArgs e)
        {
            string myText = ((TextBox)sender).Text;
            try
            {
                mm.UpdateLastActivityTime();

                if ((this.cboWTO.SelectedIndex < 0) || (!this.myWireTransfers.ContainsKey(myText)))
                {   //no wire transfer selected
                    this.myWireTransfer = null;
                    return;
                }

                int wid = this.myWireTransfers[myText];
                this.myWireTransfer = new WireTransfer(wid);
                EnableFields(true);

                this.txtRecFrom.Text = this.myWireTransfer.WiredTo;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtWireAmount_TextChanged(object sender, EventArgs e)
        {
            if (modLogic.isNumeric(this.txtWireAmount.Text))
                txtWireAmount.Text = (double.Parse(txtWireAmount.Text)).ToString("c");
        }

        protected void txtWTOAmount_TextChanged(object sender, EventArgs e)
        {
            if (modLogic.isNumeric(txtWTOAmount.Text))
                txtWTOAmount.Text = (double.Parse(txtWTOAmount.Text)).ToString("c");
        }

        private bool Validate()
        {
            double WTOAmount = 0;
            if (m_mode == (int)wireXfer.Overpayment)
            {
                if (this.cboWTO.SelectedIndex < 0)
                {
                    Response.Write("<script>alert('Please choose wire transfer to record overpayment.');</script>");
                    
                    this.cboWTO.Focus();
                    return false;
                }

                if (this.txtWTOAmount.Text.Trim() == "")
                {
                    Response.Write("<script>alert('Amount is a Required Field');</script>");
                    this.txtWTOAmount.Focus();
                    return false;
                }

                if (!modLogic.isNumeric(this.txtWTOAmount.Text))
                {
                    Response.Write("<script>alert('Please enter a numeric amount');</script>");
                    this.txtWTOAmount.Focus();
                    return false;
                }

                WTOAmount = double.Parse(this.txtWTOAmount.Text.Trim().Replace("$", "").Replace(",", ""));
                if (WTOAmount > System.Math.Abs(this.myWireTransfer.Transaction().TransactionAmount))
                {
                    Response.Write("<script>alert('Overpayment amount must be less than the original wire transfer amount of " + System.Math.Abs(this.myWireTransfer.Transaction().TransactionAmount).ToString("c") + "');</script>");
                    
                    this.txtWTOAmount.Focus();
                    return false;
                }

                if (dpWTODateSent.Text == null)
                {
                    Response.Write("<script>alert('Date Received is a Required Field');</script>");
                    this.dpWTODateSent.Focus();
                    return false;
                }

                if (txtWTODraftNo.Text.Trim() == "")
                {
                    Response.Write("<script>alert('Draft Number is a Required Field');</script>");
                    this.txtWTODraftNo.Focus();
                    return false;
                }
                else if (!modLogic.isNumeric(this.txtWTODraftNo.Text))
                {
                    Response.Write("<script>alert('Draft Number Must be Numeric');</script>");
                    this.txtWTODraftNo.Focus();
                    return false;
                }

                if (this.txtRecFrom.Text.Trim() == "")
                {
                    Response.Write("<script>alert('Received From is a Required Field');</script>");
                    this.txtRecFrom.Focus();
                    return false;
                }

                if (this.txtWTONotes.Text.Trim() == "")
                {
                    Response.Write("<script>alert('A Description of the Wire Transfer is required');</script>");
                    this.txtWTONotes.Focus();
                    return false;
                    
                }
            }
            else
            {
                if (this.txtWireAmount.Text.Trim() == "")
                {
                    Response.Write("<script>alert('Amount is a Required Field');</script>");
                    this.txtWireAmount.Focus();
                    return false;
                }

                if (!modLogic.isNumeric(this.txtWireAmount.Text))
                {
                    Response.Write("<script>alert('Please enter a numeric amount');</script>");
                    this.txtWireAmount.Focus();
                    return false;
                }

                WTOAmount = double.Parse(this.txtWireAmount.Text.Trim().Replace("$", "").Replace(",", ""));
                if (WTOAmount > System.Math.Abs(this.myWireTransfer.Transaction().TransactionAmount))
                {
                    Response.Write("<script>alert('Overpayment amount must be less than the original wire transfer amount of " + System.Math.Abs(this.myWireTransfer.Transaction().TransactionAmount).ToString("c") + "');</script>");
                    this.txtWireAmount.Focus();
                    return false;
                }

                if (dpDateSent.Text == null)
                {
                    Response.Write("<script>alert('Date Received is a Required Field');</script>");
                    this.dpDateSent.Focus();
                    return false;
                }

                if (this.txtSendTo.Text.Trim() == "")
                {
                    Response.Write("<script>alert('Received From is a Required Field');</script>");
                    this.txtSendTo.Focus();
                    return false;
                }

                if (this.txtWireNotes.Text.Trim() == "")
                {
                    Response.Write("<script>alert('A Description of the Wire Transfer is Suggested');</script>");
                    this.txtWireNotes.Focus();
                    return false;
                    
                }
            }
            return true;
        }
    }
}
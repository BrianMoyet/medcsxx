﻿<%@ Page Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmNewFileNote.aspx.cs" Inherits="MedCSX.Claim.frmNewFileNote" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <script type="text/javascript">
    window.onload = function () {
        var strCook = document.cookie;
        if (strCook.indexOf("!~") != 0) {
            var intS = strCook.indexOf("!~");
            var intE = strCook.indexOf("~!");
            var strPos = strCook.substring(intS + 2, intE);
            document.getElementById("grdWithScroll").scrollTop = strPos;
        }
    }
    function SetDivPosition() {
        var intY = document.getElementById("grdWithScroll").scrollTop;
        document.cookie = "yPos=!~" + intY + "~!";
    }
</script>
      <div class="row">
          <div class="col-md-3">
              Type:
              <br />
             <div  id="grdWithScroll"  style="width: 100%; height: 250px; overflow: scroll" onloadstart="SetDivPosition()" onscroll="SetDivPosition()">
                <asp:GridView ID="grFileNoteType" runat="server" AutoGenerateColumns="false" CellPadding="4" DataKeyNames="fileId"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" AutoGenerateSelectButton="true" PageSize="500" >
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />  
                    <Columns>
                          <asp:TemplateField HeaderText=""  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center">
                        <ItemTemplate>
                             <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("ImageIdx", "~/content/images/{0}.png") %>' Width="20px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:BoundField DataField="Description" HeaderText="" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
                 </div>
                
          </div>
           <div class="col-md-3">
                File Note Text:<br />
               <asp:TextBox ID="tbFileNoteText" runat="server" TextMode="MultiLine"  width="225px" Height="225px"></asp:TextBox>
               <br /><asp:CheckBox ID="chkFileNoteTextSummary" Text="Summary" runat="server" /> 
               <asp:CheckBox ID="chkFileNoteUrgent" runat="server" Text="Urgent" />
          </div>
           <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        Claimant: <asp:DropDownList ID="cboFileNoteClaimant" AutoPostBack="true" runat="server" CausesValidation="false" OnSelectedIndexChanged="cboFileNoteClaimant_SelectedIndexChanged"></asp:DropDownList><br />
                        Reserve: <asp:DropDownList ID="cboFileNoteReserve" runat="server"></asp:DropDownList>
                     </div>
                   </div>
                    <div class="row">
                         <div class="col-md-6">
                             Send Alert To:<br />
                             
                             <div style="width: 100%; height: 150px; overflow: scroll">
                              <asp:GridView ID="lbFileNoteAlert" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" CellPadding="4"  ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" AutoGenerateSelectButton="false" PageSize="500">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />  
                    <Columns>
                                  <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                     <asp:BoundField DataField="Name" HeaderText="" />
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                     <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                    <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
                                  </div>
                                 <br />
                                 <asp:CheckBox ID="chkFileNoteAlertDefer" Text="Defer Alerts Until" runat="server" Visible="False" />
                                 <asp:TextBox ID="tbFileNoteAlertDefer" TextMode="Date" runat="server" Visible="False"></asp:TextBox>
                          </div>
                        
                         <div class="col-md-6">
                             Set Diary For:
                                <br />
                              <div style="width: 100%; height: 150px; overflow: scroll">
                               <asp:GridView ID="lbFileNoteDiary"  DataKeyNames="Id" runat="server" AutoGenerateColumns="false" CellPadding="4" 
                                     ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" AutoGenerateSelectButton="false" PageSize="500">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />  
                    <Columns>
                                  <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:BoundField DataField="Name" HeaderText="" />
                    </Columns>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                     <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                    <EditRowStyle BackColor="#999999" />
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
                         </div>  
                               <br /><asp:DropDownList ID="cboFileNoteDiary" runat="server" Width="175px"></asp:DropDownList><br />
                              No of Days: <asp:TextBox ID="tbFileNoteDiaryDue" TextMode="Number" runat="server" Width="50px" OnTextChanged="tbFileNoteDiaryDue_TextChanged" AutoPostBack="True"></asp:TextBox> 

                             <asp:TextBox ID="picDiaryCalendar" TextMode="Date" runat="server" OnTextChanged="picDiaryCalendar_TextChanged"  AutoPostBack="true" CausesValidation="false"></asp:TextBox><br />
                             <asp:Label ID="lblDiaryDueDate" runat="server" Visible="false" Text="" ForeColor="blue"></asp:Label>
                         </div>
                    </div>
                 
                    
                  
         
             
          </div>
  </div>    
   
      <div class="row">
        <div class="col-md-12">
            <div style="width: 965px; height: 150px; overflow: scroll">
            <asp:GridView ID="grFileActivityPrevious" runat="server" AutoGenerateColumns="false" CellPadding="4"  DataKeyNames="file_note_id"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                    ForeColor="#333333"  RowStyle-ForeColor="White" ShowFooter="True"  width="100%" AutoGenerateSelectButton="true" PageSize="500" OnSelectedIndexChanged="grFileActivityPrevious_SelectedIndexChanged">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />  
                    <Columns>
                         <asp:BoundField DataField="created_date" HeaderText="Created Date" ItemStyle-Width="150px" />
                           <asp:BoundField DataField="file_note_type" HeaderText="File Note Type"  ItemStyle-Width="100px" />
                     <asp:BoundField DataField="Diary_Cleared" HeaderText="Diary Cleared" ItemStyle-Width="50px"  />
                        <asp:BoundField DataField="user_name" HeaderText="User" ItemStyle-Width="100px"  />
                     <asp:BoundField DataField="Claimant" HeaderText="Claimant" />
                        <asp:BoundField DataField="Reserve" HeaderText="Reserve" />
                    <asp:BoundField DataField="file_note_text" HeaderText="Text" />
                        
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    
                      
                </asp:GridView>
            </div>
        </div>
    </div>
      <div class="row">
        <div class="col-md-12">
            <asp:TextBox ID="tbFileActivityPreviousText" TextMode="MultiLine" Height="100px" Width="100%" runat="server"></asp:TextBox>
           
       </div>
      </div>
      <div class="row">
           <div class="col-md-12">
               <asp:Button ID="btnFileActivityOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnFileActivityOK_Click" />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
               </div>
        </div>
</asp:Content>

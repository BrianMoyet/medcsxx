﻿<%@ Page Title="Injured Liens" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmLiens.aspx.cs" Inherits="MedCSX.Claim.frmLiens" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class ="col-md-2">
             Injured:
        </div>
        <div class="col-md-9">
            <asp:Label ID="lblInjured" runat="server" Text="Label"></asp:Label>
        </div>
    </div>
      <div class="row">
        <div class ="col-md-2">
             Vendor Tax ID:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtTaxId" runat="server" CausesValidation="false" AutoPostBack="true" OnTextChanged="txtTaxId_TextChanged" ></asp:TextBox> &nbsp;&nbsp;
            <asp:LinkButton ID="txtVendorLookup" CausesValidation="false" OnClick="txtVendorLookup_Click" runat="server">Lookup Vendor</asp:LinkButton>
             <div runat="server" id="vendorlookup" visible="false">
            <div class="row">
                <div class="col=md-12">
                        Enter Vendor Name (or first part of one):<asp:TextBox ID="txtVendorSearch" runat="server"></asp:TextBox> 
                         <asp:Button ID="btnVendorSearch"  class="btn btn-info btn-xs" CausesValidation="false"  OnClick="btnVendorSearch_Click" runat="server" Text="Search" />
                </div>
            </div>
        </div>
            <br /><asp:Label ID="lblTaxID" Visible="false" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-9">
            
            <asp:Label ID="txtVendorName" runat="server" height="88px" Width="206px"></asp:Label>
            <asp:TextBox ID="txtvendorid" Visible="false" runat="server" EnableViewState="False"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class ="col-md-2">
             Amount:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtAmount" TextMode="Number" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="btnOK"   class="btn btn-info btn-xs"  OnClick="btnOK_Click" runat="server">OK</asp:LinkButton>
        </div>
    </div>
</asp:Content>

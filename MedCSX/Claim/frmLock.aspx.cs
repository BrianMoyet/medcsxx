﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
	public partial class frmLock1 : System.Web.UI.Page
	{
        #region Class Variables
        public string fileNoteText = "";
        public bool okPressed = false;
        private string referer = "";

        private string requiredText = "";
        private static ModMain mm = new ModMain();
        private int lockMode = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
            if (Session["referer2"] != null)
            {

                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();


            if (!Page.IsPostBack)
            {
                this.requiredText = Request.QueryString["required_text"].ToString();
                lockMode = Convert.ToInt32(Request.QueryString["mode"]);

                if (lockMode == 0)
                {
                    this.Page.Title = "Unlock";
                    this.txtPrompt.Text = "Enter File Notes for Unlock:";
                    //this.lockImage.Source = new BitmapImage(new Uri("../../Forms/Images/icoLockKey.png", UriKind.Relative));
                }
                else if (lockMode == 1)
                {
                    this.Page.Title = "Manually Lock Claim";
                    this.txtPrompt.Text = "Enter File Notes for Lock:";
                    //this.lockImage.Source = new BitmapImage(new Uri("../../Forms/Images/lock-icon.png", UriKind.Relative));
                }
                this.txtRequiredText.Text = this.requiredText;
                this.txtFileNoteText.Text = this.fileNoteText;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            this.okPressed = true;
            this.fileNoteText = this.txtFileNoteText.Text;

            Session["referer2"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
        }
    }
}
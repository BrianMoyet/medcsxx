﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCSX.App_Code;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmTransactions : System.Web.UI.Page
    {
        #region Class Variables
        public int transactionId = 0;       //selected transaction ID - Alert is input parameter
        public string referer = "";
        private Reserve myReserve = null;       //reserve object
        private bool transactionLocked = false,   //is transaction locked
            reserveLocked = false,                //is reserve locked
            claimLocked = false,                  //is claim locked
            includeLoss = false,                  //display loss transactions
            includeExpense = false;               //display expense transactions
        private int draftId = 0,                  //draft_Id
            salvageId = 0,                        //salvage_Id
            subroId = 0,                          //subro_Id
            deductiblesId = 0,                    //deductibles_Id
            recoverySirId = 0,                    //SIR_Recovery_Id
            creditSirId = 0;                      //SIR_Credit_Id
        private MedCsxLogic.Claim myClaim = null;           //claim object
        private ReserveType reserveType = null;     //reserve_type row
        private Claimant claimant = null;           //claimant row
        private Transaction tr = null;              //transaction row
        private Draft draft = null;                 //draft row
        private Subro subro = null;                 //subro row
        private Salvage salvage = null;             //salvage row
        private List<Hashtable> commmercialPolicyValues = new List<Hashtable>();        //list of limits and deductibles for the policy
        private List<Hashtable> SIRvalues = new List<Hashtable>();
        List<Hashtable> mySIRvalues = null;
        List<Hashtable> myCommercialPolicyValues = null;

        private List<TransactionGrid> myTransactionList;
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        #endregion

      

        protected void Page_Load(object sender, EventArgs e)
        {
            this.myReserve = new Reserve(Convert.ToInt32(Regex.Replace(Request.QueryString["reserve_id"].ToString(), "[^0-9]", "")));


           
                referer = Session["referer"].ToString();


            this.myClaim = new MedCsxLogic.Claim(myReserve.ClaimId);

           
            if (!Page.IsPostBack)
            {
           
            //myClaim = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
           
                this.myClaim = new MedCsxLogic.Claim(myReserve.ClaimId);
                this.commmercialPolicyValues = myCommercialPolicyValues;
                this.SIRvalues = mySIRvalues;

                //overlay detail frames
                //this.tcTransactionDetail.SelectedIndex = this.tcTransactionDetail.Items.IndexOf(TransactionDetails);

                //look for reserve or claim locks
                this.CheckReserveClaimLocks();

                //load transaction data
                this.ShowAll();

                //set window title
                this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - Transactions for " + this.lblClaimantName.Text + " - " + myReserve.ReserveType.Description;
                if (grTransactions.Rows.Count > 0)
                { 
                    grTransactions.SelectedIndex = 0;
                    grTransactions_SelectedIndexChanged(null, null);
                }
            }

            this.ModalPopupExtender3.Enabled = true;

            this.Page.Title = "Claim " + this.myClaim.DisplayClaimId + " - Transactions for " + this.lblClaimantName.Text + " - " + myReserve.ReserveType.Description;
        }

        private void ShowAll(string mode = "")
        {
            try
            {
                mm.UpdateLastActivityTime();

                switch (mode)
                {
                    case "Loss":
                        this.includeLoss = true;
                        this.includeExpense = false;
                        this.RefreshData();
                        break;
                    case "Expense":
                        this.includeLoss = false;
                        this.includeExpense = true;
                        this.RefreshData();
                        break;
                    default:
                        this.includeLoss = true;
                        this.includeExpense = true;
                        this.RefreshData();
                        break;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void RefreshTransactionLock()
        {
            try
            {
                int transactionLockId = 0;      //transaction lock ID
                TransactionLock tl;     //transaction lock for transaction
                Transaction tr = new Transaction(this.transactionId);

                transactionLockId = tr.FirstLockId;
                if (transactionLockId == 0)
                {   //transaction not locked
                    this.transactionLocked = false;
                    this.gbTransactionLock.Text = "Transaction Not Locked";
                  
                    this.btnUnlockReserve.Enabled = false;
                    this.btnUnlockReserve.ForeColor = System.Drawing.Color.DarkGray;
                    this.lblReserveLockReason.Text = "";
                    this.lblReserveLockDate.Text = "";
                }
                else
                {
                    this.transactionLocked = true;
                    this.gbTransactionLock.Text = "*** TRANSACTION LOCKED ***";
                    this.gbTransactionLock.ForeColor = System.Drawing.Color.Red;
                    this.btnUnlockReserve.Enabled = true;
                    btnUnlockReserve.ForeColor = System.Drawing.Color.Blue;
                    this.btnTrnsDraft.Enabled = false;
                    //get lock reason and date
                    tl = new TransactionLock(transactionLockId);
                    EventType et = tl.LockFileNote().EventType();

                    this.lblReserveLockReason.Text = et.Description;
                    this.lblReserveLockDate.Text = tl.LockDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void RefreshData()
        {
            this.PopulateReserveControls();     //populate reserve labels
            bool transactionsLoaded = LoadTransactions();

            if (this.grTransactions.Rows.Count == 0)
                return;
            this.grTransactions.SelectedIndex = 0;
        }

        protected void btnUnlockReserve_Click(object sender, EventArgs e)
        {
            this.transactionId = (int)grTransactions.SelectedDataKey.Value;
            if (this.transactionId == 0)
                return;

            UnlockTransactionLock(this.transactionId);
            this.RefreshTransactionLock();      //refresh the transaction's locks
        }

        public void UnlockTransactionLock(int transactionLockId)
        {
            try
            {
               
                string s; //required text

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(Not Authorized to modify Claim Data.');</script>");

                    return;
                }

                MedCsxLogic.TransactionLock tl = new TransactionLock(transactionLockId);

                if (!ml.CurrentUser.isManagement())
                {
                    btnUnlockReserve.Enabled = false;
                    lblLockPermissions.Text = "You must be a supervisor or above to clear this lock.";
                    return;
                }

                MedCsxLogic.Transaction t = tl.Transaction();

                
                //check for financial authority to unlock transaction
                if (tl.Reserve().ReserveType.UserAuthority() < tl.LockFileNote().AuthorityNeededToUnlock)
                {
                    btnUnlockReserve.Enabled = false;
                    lblLockPermissions.Text = "You do not have sufficient financial authority to unlock this transaction.";
                          
                    return;
                }

                //get required text
                s = t.TransactionType().Description + "Transaction For " + t.TransactionAmount.ToString("c");

                if (t.DraftId != 0)
                    s += " To " + t.Draft.Payee;

                if (t.SubroId != 0)
                    s += " From " + t.Subro().RecoverySource;

                if (t.SalvageId != 0)
                    s += " From " + t.Salvage().Vendor().Name;

                s += " Unlocked By " + ml.CurrentUser.Name;

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this.myClaim.ClaimId + "&Ptabindex=7";

                Session["referer2"] = referer;

                //prompt for file note text
                string url = "frmLock.aspx?mode=c&claim_id=" + myClaim.ClaimId + "&lock_type_id" + modGeneratedEnums.RestrictionType.Transaction.ToString();
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

               
                //TODO  get text from formLock
                //s += Environment.NewLine + f.fileNoteText;

                //unlock claim
                tl.Unlock(s);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnLockReserveInfo_Click(object sender, EventArgs e)
        {
            this.transactionId = (int)grTransactions.SelectedDataKey.Value;
            if (this.transactionId == 0)
                return;


            referer = Request.Url.ToString();

            

            Session["referer2"] = referer;

            string url = "frmLockingInfo.aspx?mode=c&transaction_id=" + this.transactionId + "&lock_type_id=" + modGeneratedEnums.RestrictionType.Transaction.ToString();
            string s = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            
           // this.RefreshTransactionLock();      //refresh the transaction's locks
        }

        private void ClearDefaults()
        {
            this.lblTransactionTypeTD.Text = "";
            this.lblTransactionDateTD.Text = "";
            this.lblTransactionAmountTD.Text = "";
            this.lblTransactionDraftTD.Text = "";
            this.lblTransactionTypeSub.Text = "";
            this.lblTransactionDateSub.Text = "";
            this.lblTransactionAmountSub.Text = "";
            this.lblTransactionVoidSub.Text = "";
            this.lblTransactionRecoverySub.Text = "";
            this.lblTransactionAmountSub.Text = "";
            this.lblTransactionDraftSub.Text = "";
            this.lblTransactionVoidReasonSub.Text = "";
            this.lblTransactionTypeS.Text = "";
            this.lblTransactionDateS.Text = "";
            this.lblTransactionVendorS.Text = "";
            this.lblTransactionAmountS.Text = "";
            this.lblTransactionDraftS.Text = "";
            this.lblTransactionVoidS.Text = "";
            this.lblTransactionVoidReasonS.Text = "";
            this.lblTransactionStorageDaysS.Text = "";
            this.lblTransactionSalvageYardS.Text = "";
            this.lblTransactionStorageS.Text = "";
            this.lblTransactionTowingS.Text = "";
            this.lblTransactionTypeD.Text = "";
            this.lblTransactionDateD.Text = "";
            this.lblTransactionPayeeD.Text = "";
            this.lblTransactionVendorD.Text = "";
            this.lblTransactionMailToD.Text = "";
            this.lblTransactionAmountD.Text = "";
            this.lblTransactionDraftD.Text = "";
            this.lblTransactionDraftReissueD.Text = "";
            this.lblTransactionVoidReasonD.Text = "";
            this.lblTransactionExplanationD.Text = "";
            this.lblTransactionReserveTD.Text = "";
            this.lblTransactionReserveSub.Text = "";
            this.lblTransactionReserveS.Text = "";
            this.lblTransactionReserveD.Text = "";
            this.lblTransactionUserTD.Text = "";
            this.lblTransactionUserSub.Text = "";
            this.lblTransactionUserS.Text = "";
            this.lblTransactionUserD.Text = "";
        }

        protected void grTransactions_SelectedIndexChanged(object sender, EventArgs e)
        {
            Draft voidDraft;
            string userName = "",                 //user name (corresponding to User_Id on Transaction table)
                mailTo = "";                      //draft main to name

            try
            {
                mm.UpdateLastActivityTime();

                draftId = 0;
                subroId = 0;
                salvageId = 0;

                TransactionDetails.Visible = true;
                this.ClearDefaults();

                //populate transaction fields
                //TransactionGrid mySelection = (TransactionGrid)grTransactions.SelectedRow.Cells;
                int mySelection = (int)grTransactions.SelectedDataKey.Value;
                //if (mySelection == null)
                //    return;     //no selection made

                ifrmVoidReason.Src = "frmVoidReason.aspx?reserve_id=" + this.myReserve.Id.ToString() + "&mode=" + (int)SalvageSubro.Subro + "&void_reason_table=Draft_Void_Reason";
                //ifrmVoidReason2.Src = "frmVoidReason.aspx?reserve_id=" + this.myReserve.Id.ToString() + "&mode=" + (int)SalvageSubro.Subro + "&void_reason_table=Draft_Void_Reason";

                this.transactionId = mySelection;
                tr = new Transaction(this.transactionId);

                this.lbltransactionID.Text = this.transactionId.ToString();
                this.lblTransactionTypeTD.Text = tr.TransactionType().Description;
                this.lblTransactionDateTD.Text = tr.TransactionDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                this.lblTransactionAmountTD.Text = tr.TransactionAmount.ToString("c");
                if (tr.VoidDraftId != 0)
                {
                    voidDraft = new Draft(tr.VoidDraftId);
                    this.lblTransactionDraftTD.Text = voidDraft.DraftNo.ToString();
                    voidDraft = null;
                    btnPrintDraft.Enabled = true;
                }

                //populate Subro fields
                subroId = tr.SubroId;
                if (subroId != 0)
                {
                    subro = new Subro(subroId);

                    this.lblTransactionTypeSub.Text = subro.Transaction().TransactionType().Description;
                    this.lblTransactionDateSub.Text = subro.Transaction().TransactionDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                    this.lblTransactionAmountSub.Text = subro.Transaction().TransactionAmount.ToString("c");
                    if (subro.isVoid)
                    {
                        this.lblTransactionVoidSub.Text = "*** VOID ***";
                        this.lblTransactionVoidSub.ForeColor = System.Drawing.Color.Red;
                    }
                    this.lblTransactionRecoverySub.Text = subro.RecoverySource;
                    this.lblTransactionAmountSub.Text = subro.Transaction().TransactionAmount.ToString("c");
                    this.lblTransactionDraftSub.Text = subro.CheckNo.ToString();
                    if (subro.SubroSalvageVoidReasonId != 0)
                        this.lblTransactionVoidReasonSub.Text = mm.GetSubroSalvageVoidReason(subro.SubroSalvageVoidReasonId);
                    else
                        this.lblTransactionVoidReasonSub.Text = "";
                    TransactionDetails.Visible = false;
                    SubroDetails.Visible = true;
                    DraftDetails.Visible = false;
                    SalvageDetails.Visible = false;
                }

                //populate salvage fields
                salvageId = tr.SalvageId;
                if (salvageId != 0)
                {
                    salvage = new Salvage(salvageId);

                    this.lblTransactionTypeS.Text = salvage.Transaction().TransactionType().Description;
                    this.lblTransactionDateS.Text = salvage.Transaction().TransactionDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                    this.lblTransactionVendorS.Text = salvage.Vendor().Name;
                    this.lblTransactionAmountS.Text = salvage.Transaction().TransactionAmount.ToString("c");
                    this.lblTransactionDraftS.Text = salvage.CheckNo.ToString();
                    if (salvage.isVoid)
                    {
                        this.lblTransactionVoidS.Text = "*** VOID ***";
                        this.lblTransactionVoidS.ForeColor = System.Drawing.Color.Red;
                    }
                    if (salvage.SubroSalvageVoidReasonId != 0)
                        this.lblTransactionVoidReasonS.Text = mm.GetSubroSalvageVoidReason(salvage.SubroSalvageVoidReasonId);
                    else
                        this.lblTransactionVoidReasonS.Text = "";
                    this.lblTransactionStorageDaysS.Text = salvage.StorageDays.ToString();
                    this.lblTransactionSalvageYardS.Text = salvage.SalvageYardCharges.ToString("c");
                    this.lblTransactionStorageS.Text = salvage.StorageCharges.ToString("c");
                    this.lblTransactionTowingS.Text = salvage.Towing_Charges.ToString("c");
                    TransactionDetails.Visible = false;
                    SubroDetails.Visible = false;
                    DraftDetails.Visible = false;
                    SalvageDetails.Visible = true;

                }

                //populate Draft fields
                draftId = tr.DraftId;
                if (draftId != 0)
                {
                    draft = new Draft(draftId);

                    this.lblTransactionTypeD.Text = draft.Transaction().TransactionType().Description;
                    this.lblTransactionDateD.Text = draft.Transaction().TransactionDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                    this.lblTransactionPayeeD.Text = draft.Payee;
                    this.lblTransactionVendorD.Text = draft.Vendor().Name;
                    mailTo = draft.MailToName.Trim() + Environment.NewLine + draft.Address1.Trim();
                    if (draft.Address2.Trim() != "")
                        mailTo += Environment.NewLine + draft.Address2.Trim();
                    mailTo += Environment.NewLine + draft.City.Trim() + ", " + draft.State().Abbreviation + " " + draft.Zipcode;
                    this.lblTransactionMailToD.Text = mailTo;
                    this.lblTransactionAmountD.Text = draft.Transaction().TransactionAmount.ToString("c");
                    this.lblTransactionDraftD.Text = draft.DraftNo.ToString();
                    if (draft.ReissuedFromDraftId != 0)
                    {
                        Draft draft2 = new Draft(draft.ReissuedFromDraftId);
                        this.lblTransactionDraftReissueD.Text = draft2.DraftNo.ToString();
                        draft2 = null;
                    }
                    this.lblTransactionVoidReasonD.Text = draft.DraftVoidReason().Description;
                    this.lblTransactionExplanationD.Text = draft.ExplanationOfProceeds;
                    TransactionDetails.Visible = false;
                    SubroDetails.Visible = false;
                    DraftDetails.Visible = true;
                    SalvageDetails.Visible = false;
                }
               


                //populate user and expense fields
                if (tr.isLoss)
                {
                    this.lblTransactionReserveTD.Text = "Loss";
                    this.lblTransactionReserveSub.Text = "Loss";
                    this.lblTransactionReserveS.Text = "Loss";
                    this.lblTransactionReserveD.Text = "Loss";
                }
                else
                {
                    this.lblTransactionReserveTD.Text = "Expense";
                    this.lblTransactionReserveSub.Text = "Expense";
                    this.lblTransactionReserveS.Text = "Expense";
                    this.lblTransactionReserveD.Text = "Expense";
                }

                userName = MedCsxLogic.User.UserNameForId(tr.UserId);
                this.lblTransactionUserTD.Text = userName;
                this.lblTransactionUserSub.Text = userName;
                this.lblTransactionUserS.Text = userName;
                this.lblTransactionUserD.Text = userName;

                this.RefreshTransactionLock();      //refresh locking info
                this.EnableDisableControls();       //enable/disable buttons, etc

                if (grTransactions.SelectedRow.Cells[5].Text == "")
                    btnPrintDraft.Enabled = false;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void EnableDisableControls()
        {
            int reserveStatusId = this.myReserve.ReserveStatusId;
            this.btnReissueDraft.Enabled = false;
            this.btnReissueDraft.ForeColor = System.Drawing.Color.DarkGray;
            this.btnTrnsDraft.Enabled = false;
            this.btnTrnsDraft.ForeColor = System.Drawing.Color.DarkGray;
            this.btnViewDraft.Enabled = false;
            this.btnViewDraft.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResSalvage.Enabled = false;
            this.btnResSalvage.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResSubro.Enabled = false;
            this.btnResSubro.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResVoid.Enabled = false;
            this.ModalPopupExtender1.Enabled = false;
            this.ModalPopupExtender3.Enabled = false;
            this.btnPrintDraft.Enabled = false;
            this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
            this.btnUnVoid.Enabled = false;
            this.btnUnVoid.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResWireTransfer.Enabled = false;
            this.btnResWireTransfer.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResWireOverpayment.Enabled = false;
            this.btnResWireOverpayment.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResPendingDraft.Enabled = false;
            this.btnResPendingDraft.ForeColor = System.Drawing.Color.DarkGray;
            this.btnPrintDraft.ForeColor = System.Drawing.Color.DarkGray;

            if (reserveStatusId == (int)modGeneratedEnums.ReserveStatus.Void)
                return;     //don't allow any transactions if voided reserve

            this.btnTrnsDraft.Enabled = true;
            this.btnTrnsDraft.ForeColor = System.Drawing.Color.Blue;
            if (draftId != 0)
            {
                //enable/disable void/unvoid buttons
                if (!draft.isVoid)
                {
                    this.btnPrintDraft.Enabled = true;
                    this.btnResVoid.Enabled = true;
                    this.ModalPopupExtender1.Enabled = true;
                    this.ModalPopupExtender3.Enabled = true;
                    this.btnUnVoid.Enabled = false;
                    this.btnPrintDraft.ForeColor = System.Drawing.Color.Blue;
                    this.btnResVoid.ForeColor = System.Drawing.Color.Blue;
                    this.btnUnVoid.ForeColor = System.Drawing.Color.DarkGray;


                }
                else
                { 
                    this.btnUnVoid.Enabled = true;
                    this.btnUnVoid.ForeColor = System.Drawing.Color.Blue;
                    this.btnResVoid.Enabled = false;
                    this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
                    this.ModalPopupExtender1.Enabled = false;
                    this.ModalPopupExtender3.Enabled = false;
                    this.btnPrintDraft.Enabled = false;
                    this.btnPrintDraft.ForeColor = System.Drawing.Color.DarkGray;

                }
                //reissue button
                if (!Draft.hasDraftReissue(draftId))
                {
                    this.btnReissueDraft.Enabled = true;
                    btnReissueDraft.ForeColor = System.Drawing.Color.Blue;
                }

<<<<<<< HEAD
                if ((draft.isPrinted) || (draft.InDraftQueue ==1))
=======
<<<<<<< HEAD
                if ((draft.isPrinted) || (draft.InDraftQueue ==1))
=======
                if (draft.isPrinted)
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                {
                    this.btnViewDraft.Enabled = true;     //view button
                    btnViewDraft.ForeColor = System.Drawing.Color.Blue;
                }
                else if (!transactionLocked)
                {
                    this.btnPrintDraft.Enabled = true;    //print button
                    btnPrintDraft.ForeColor = System.Drawing.Color.Blue;
                }
            }

            if (subroId != 0)
            {
                if (!subro.isVoid)
                { 
                    this.btnResVoid.Enabled = true;
                    this.ModalPopupExtender1.Enabled = true;
                    this.ModalPopupExtender3.Enabled = true;
                    this.btnResVoid.ForeColor = System.Drawing.Color.Blue;
                }
            }

            if (salvageId != 0)
            {
                if (!salvage.isVoid)
                {
                    this.btnResVoid.Enabled = true;
                    this.ModalPopupExtender1.Enabled = true;
                    this.ModalPopupExtender3.Enabled = true;
                    this.btnResVoid.ForeColor = System.Drawing.Color.Blue;
                }
            }

            //enable subro/salvage buttons
            if ((reserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed) ||
                (reserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage) ||
                (reserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro) ||
                (reserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Deductibles) ||
                (reserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_SIR_Recovery))
            {
                this.btnResSalvage.Enabled = true;
                this.btnResSubro.Enabled = true;
                this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                this.btnResSubro.ForeColor = System.Drawing.Color.Blue;

            }

            //transaction locked, adjuster unassigned, claim closed
            if (reserveLocked || claimLocked)
            {
                this.btnResSalvage.Enabled = false;
                this.btnResSubro.Enabled = false;
                this.btnTrnsDraft.Enabled = false;
                this.btnTrnsDraft.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResSalvage.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResSubro.ForeColor = System.Drawing.Color.DarkGray;
            }

            if (transactionLocked)
            {
                this.btnTrnsDraft.Enabled = false;
                this.btnResSalvage.Enabled = false;
                this.btnResSubro.Enabled = false;
                this.btnPrintDraft.Enabled = false;
                this.btnReissueDraft.Enabled = false;
                this.btnResVoid.Enabled = false;
                this.ModalPopupExtender1.Enabled = false;
                this.ModalPopupExtender3.Enabled = false;
                this.btnUnVoid.Enabled = false;

                this.btnTrnsDraft.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResSalvage.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResSubro.ForeColor = System.Drawing.Color.DarkGray;
                this.btnPrintDraft.ForeColor = System.Drawing.Color.DarkGray;
                this.btnReissueDraft.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
                this.btnUnVoid.ForeColor = System.Drawing.Color.DarkGray;
            }
        }

        protected void btnTrnsDraft_Click(object sender, EventArgs e)
        {
            IssueDraft(this.myReserve.Id);


            //string myWindow = "my_window" + myClaim.DisplayClaimId;
            //string url = referer;
            //string s2 = "window.open('" + url + "', '" + myWindow + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

        }

        public void IssueDraft(int reserveId, List<Hashtable> CommercialPolicyValues = null, int claimId = 0)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                //if (!MicrFontsExist())
                //    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Issue_Draft))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Issue Draft.');</script>");
                    return;     //user not authorized
                }

                Reserve res = new Reserve(reserveId);
                if (res.Claim.maxSupplementalPaymentReached)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments.');</script>");
                    return;     //user not authorized
                }
               

                if (res.maxSupplementalPaymentsReached)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments.');</script>");

                      return;
                }

                Session["referer2"] = Request.Url.ToString();
                string url = "frmDraft.aspx?mode=0&reserve_id=" + reserveId;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
                //frmDraft f = new frmDraft(reserveId);//, CommercialPolicyValues, claimId);
                //f.Show();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void IssuePendingDraft(int reserve_id)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (reserve_id == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Pending_Drafts))
                    return;      //user not authorized

                Session["referer2"] = Request.Url.ToString();
                string url = "frmDraft.aspx?mode=1&reserve_id=" + reserve_id;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnPrintDraft_Click(object sender, EventArgs e)
        {

           

            mm.UpdateLastActivityTime();

            this.btnPrintDraft.Enabled = false;       //disable button to prevent multiple printings
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Issue_Draft))
            {
                
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(Not Authorized to Issue Drafts.');</script>");
                return;     //user is not authorized
            }

            tr = new Transaction((int)grTransactions.SelectedDataKey.Value);
            this.draftId = tr.DraftId;


            if (this.draftId == 0)
                return;     //no draft to print
                            //string msg = "Acrobat Reader is currently opened.  This may create issues in printing drafts." + Environment.NewLine + Environment.NewLine;
                            //msg += "Please close Acrobat Reader and Click OK.  Click Cancel to place the draft in the print queue.";
                            //Process[] acrobats = Process.GetProcessesByName("AcroRd32");
                            //if (acrobats.Count() > 0)
                            //{
                            //    foreach (Process reader in acrobats)
                            //    {
                            //        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                            //        //if (MessageBox.Show(msg, "Conflicting Applications", MessageBoxButton.OKCancel, MessageBoxImage.Information, MessageBoxResult.Cancel) == MessageBoxResult.Cancel)

            //    }
            //}
            //else
            //{

            mf.PrintDraft(this.draftId, false);
            //Byte[] PDFDocument = mf.PrintDraft(this.draftId, false);
            //Session["binaryData"] = PDFDocument;
            ////Response.Redirect("frmReport.aspx");
            //string url = "frmReport.aspx";
            //string s = "window.open('" + url + "', 'popup_windowReport" + draftId + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            ////ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            //ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport" + draftId, s, true);
            //Response.ContentType = "Application/pdf";
            //Response.BinaryWrite(PDFDocument);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Created and placed in Draft Queue.');</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "close", "this.location.href='" + Request.Url.ToString() + "';", true);
            
            //Page_Load(null, null);
            //}
        }

        protected void btnViewDraft_Click(object sender, EventArgs e)
        {
            ViewDraft();
        }

        protected void btnResSalvage_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Receive_Salvage))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Receive Salvage.');</script>");
                    return;     //user not authorized
                }
                //gvDemands.SelectedRow.Cells[5].Text)
                string url = "frmReceiveSalvage.aspx?mode=0&reserve_id=" + this.myReserve.Id.ToString();
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=800, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
                //frmReceiveSalvage f = new frmReceiveSalvage(this.myReserve.Id);
                //f.ShowDialog();

                //TODO get transaction id from form
                //mf.MouseBusy();
                //this.transactionId = f.transactionId;
                RefreshData();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            { 
            }
        }

        protected void btnResSubro_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Receive_Subro))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Receive Subro.');</script>");
                    return;     //user not authorized
                }
                string url = "frmReceiveSubro.aspx?reserve_id=" + this.myReserve.Id.ToString() + "&mode=" + (int)SalvageSubro.Subro;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=300, height=500, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

               
                //todo
                //this.transactionId = f.transactionId;
                RefreshData();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
               
            }
        }

        protected void btnReissueDraft_Click(object sender, EventArgs e)
        {

            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Reissue_Draft))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Reissue Draft.');</script>");
                return;     //user not authorized
            }


            this.transactionId = (int)grTransactions.SelectedDataKey.Value;

            Session["referer2"] = Request.Url.ToString();
            string url = "frmDraft.aspx?mode=0&reserve_id=" + this.myReserve.Id.ToString() + "&transaction_id=" + this.transactionId + "&reissue=1&void_reason_table=Draft_Void_Reason&reissue_from_transaction_id=" + transactionId;
            string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

            //Session["referer2"] = Request.Url.ToString();
            //string url = "frmVoidReason.aspx?reserve_id=" + this.myReserve.Id.ToString() + "&transaction_id=" + grTransactions.DataKeys[grTransactions.SelectedIndex].Value.ToString() + "&mode=1&void_reason_table=Draft_Void_Reason";
            //string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=300, copyhistory=no, left=300, top=200');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            ////Reissue();
        }

        //private void Reissue()
        //{
        //    if (!mf.MicrFontsExist())
        //    {
        //        MessageBox.Show("MICR Fonts are not installed on this machine.  They must be installed in order to print drafts.", "Missing MICR Fonts", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
        //        return;     //machine doesn't have correct fonts
        //    }
        //    this.transactionId = (int)grTransactions.SelectedDataKey.Value;
        //    tr = new Transaction((int)grTransactions.SelectedDataKey.Value);

        //    //frmDraft f;
        //    int voidReasonId;

        //    try
        //    {
        //        mm.UpdateLastActivityTime();

        //        if (tr.DraftId != 0)
        //        {
        //            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Reissue_Draft))
        //                return;     //user not authorized

        //            voidReasonId = getVoidReasonId("Draft_Void_Reason");
        //            if (voidReasonId == 0)
        //                return;     //user cancelled

        //            Session["referer2"] = Request.Url.ToString();
        //            string url = "frmDraft.aspx?mode=0&reserve_id=" + this.myReserve.Id.ToString() + "&transaction_id=" + this.transactionId + "&void_reason_id=" + voidReasonId + "&reissue_from_transaction_id=" + this.transactionId;
        //            string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=800, copyhistory=no, left=300, top=200');";
        //            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
        //            //f = new frmDraft(this.myReserve.Id, transactionId, voidReasonId);
        //            //f.Show();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //    }
        //}

        private int getVoidReasonId(string p)
        {
            int voidReasonId = 0;


            //string url = "frmVoidReason.aspx?reserve_id=" + this.myReserve.Id.ToString() + "&mode=" + (int)SalvageSubro.Subro + "&void_reason_table=" + p;
            //string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=300, height=500, copyhistory=no, left=200, top=200');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

            if (Session["voidReasonId"] != null)
            {
                voidReasonId = Convert.ToInt32(Session["voidReasonId"]);
            }
            else
            {
                voidReasonId = 0;
            }
           
            return voidReasonId;
        }

        private int getVoidReasonId2(string p)
        {
            int voidReasonId = 0;

            //string url = "frmVoidReason.aspx?reserve_id=" + this.myReserve.Id.ToString() + "&mode=" + (int)SalvageSubro.Subro + "&void_reason_table=" + p;
            //string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=300, height=500, copyhistory=no, left=200, top=200');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

            if (Session["voidReasonId"] != null)
            {
                voidReasonId = Convert.ToInt32(Session["voidReasonId"]);
            }
            else
            {
                voidReasonId = 0;
            }

            return voidReasonId;
        }

        protected void btnResVoid_Click(object sender, EventArgs e)
        {
            VoidTransaction();
           
        }

        protected void btnUnVoid_Click(object sender, EventArgs e)
        {
            UnvoidDraft();
        }

        private void UnvoidDraft()
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Unvoid_Draft))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Unvoid Draft.');</script>");
                    return;     //user not authorized
                }

                tr = new Transaction(Convert.ToInt32(grTransactions.SelectedDataKey.Value));
                //make sure transaction is a voided draft
                if (tr.DraftId == 0)
                {
                    Response.Write("<script>alert('The Selected Transaction is not a Draft.');</script>");

                    return;
                }

                if (!tr.Draft.isDraftNoVoid)
                {
                    Response.Write("<script>alert('The Selected Draft Must be Void.');</script>");
                    
                    return;
                }

                double draftAmount = tr.TransactionAmount * -1;
                if (tr.Reserve.ReserveStatus.isOpen)
                {
                    if (tr.isLoss)
                    {
                        if (draftAmount > tr.Reserve.NetLossReserve)
                            tr.Reserve.ChangeNetLossReserve(ref draftAmount);
                    }
                    else  //expense
                    {
                        if (draftAmount > tr.Reserve.NetExpenseReserve)
                            tr.Reserve.ChangeNetExpenseReserve(draftAmount);
                    }
                }

                /***** Unvoid Draft ******/

                Hashtable d = ml.copyDictionary(tr.Draft.row);      //create new draft row
                d["Is_Void"] = false;
                d["Is_Printed"] = true;
                d["Draft_Void_Reason_Id"] = 0;
                d.Remove("Draft_Id");
                d.Remove("Created_Date");
                d.Remove("Modified_Date");
                d.Remove("Created_By");
                d.Remove("Modified_By");

                Hashtable eventTypeIds = new Hashtable();
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Unvoid_Draft, new Hashtable());

                if (tr.isLoss)
                    Draft.InsertDraft(d, this.myReserve.Id, (int)modGeneratedEnums.TransactionType.Unvoid_Loss_Draft,
                        eventTypeIds, 0, 0, false, true, tr.DraftId);
                else
                    Draft.InsertDraft(d, this.myReserve.Id, (int)modGeneratedEnums.TransactionType.Unvoid_Expense_Draft,
                        eventTypeIds, 0, 0, false, true, tr.DraftId);

                RefreshData();

               // Session["referer2"] = null;

                string myWindow = "my_window" + myClaim.DisplayClaimId;
                string url = Session["referer"].ToString();
                string s2 = "window.open('" + url + "', '" + myWindow + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
                //ClientScript.RegisterStartupScript(this.GetType(), "close",   s2 + ";, true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

       
        public void WireTransfer(int reserve_Id)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (reserve_Id == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reserve Not Selected.');</script>");
                    return;     //user not authorized
                }

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Wire_Transfer))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Wire Transfers.');</script>");
                    return;     //user not authorized
                }

                Reserve res = new Reserve(reserve_Id);
                string url = "frmWireTransfer.aspx?reserve_id=" + res + "&mode=" + (int)wireXfer.Transfer;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=4, height=600, copyhistory=no, left=300, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResWireOverpayment_Click(object sender, EventArgs e)
        {
            WireTransferOverpayment(this.myReserve.Id);
        }

        public void WireTransferOverpayment(int reserve_id)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (reserve_id == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reserve Not Selected.');</script>");
                    return;     //user not authorized
                }

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Wire_Transfer))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Wire Transfer.');</script>");
                    return;     //user not authorized
                }

                //Reserve res = new Reserve(reserve_id);
                string url = "frmWireTransfer.aspx?reserve_id=" + reserve_id + "&mode=" + (int)wireXfer.Overpayment;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=4, height=600, copyhistory=no, left=300, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResPendingDraft_Click(object sender, EventArgs e)
        {
            IssuePendingDraft(this.myReserve.Id);
        }

        private void VoidTransaction()
        {
            int voidReasonId;

            this.transactionId = (int)grTransactions.SelectedDataKey.Value;
            tr = new Transaction(transactionId);
            this.draftId = tr.DraftId;
           
            try
            {
                mm.UpdateLastActivityTime();

                if (tr.DraftId != 0)
                {   //void draft
                    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Void_Draft))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Void Draft.');</script>");
                        return;     //user not authorized
                    }

                    voidReasonId = getVoidReasonId("Draft_Void_Reason");
                    if (voidReasonId == 0)
                        return;     //user cancelled
                    this.transactionId = Transaction.VoidDraft(this.transactionId, voidReasonId);
                }

                if (tr.SubroId != 0)
                {   //void subro
                    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Void_Subro))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Void Subro.');</script>");
                        return;     //user not authorized
                    }

                    voidReasonId = getVoidReasonId("Subor_Salvage_Void_Reason");
                    if (voidReasonId == 0)
                        return;     //user cancelled
                    this.transactionId = Transaction.VoidSubro(this.transactionId, voidReasonId);
                }

                if (tr.SalvageId != 0)
                {   //void salvage
                    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Void_Salvage))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Void Salvage.');</script>");
                        return;     //user not authorized
                    }

                    voidReasonId = getVoidReasonId("Subor_Salvage_Void_Reason");
                    if (voidReasonId == 0)
                        return;     //user cancelled
                    this.transactionId = Transaction.VoidSalvage(this.transactionId, voidReasonId);
                }

                if (tr.DeductiblesId != 0)
                {   //void deductibles
                    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Void_Deductibles))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Void Deductibles.');</script>");
                        return;     //user not authorized
                    }

                    voidReasonId = getVoidReasonId("Subor_Salvage_Void_Reason");
                    if (voidReasonId == 0)
                        return;     //user cancelled
                    this.transactionId = Transaction.VoidDeductibles(this.transactionId, voidReasonId);
                }

                RefreshData();

                string myWindow = "my_window" + myClaim.DisplayClaimId;
                string url = Session["referer"].ToString();
                string s2 = "window.open('" + url + "', '" + myWindow + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=650, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadTransactions()
        {
            this.grTransactions.DataSource = LoadTransactions(Transaction.getTransactions(this.myReserve.Id, this.includeLoss, this.includeExpense));
            this.grTransactions.DataBind();
            return true;
        }

        protected void btnVoidReasonsOK_Click(object sender, EventArgs e)
        {
            VoidTransaction();
        }

        protected void hprShowLoss_Click(object sender, EventArgs e)
        {
            this.ShowAll("Loss");
        }

        protected void hprShowExpenses_Click(object sender, EventArgs e)
        {
            this.ShowAll("Expense");
        }

        protected void hprShowAll_Click(object sender, EventArgs e)
        {
            this.ShowAll();
        }

        protected void hprTransactionReport_Click(object sender, EventArgs e)
        {
            mf.ShowTransactionReport(this.myReserve.Id);
        }

        protected void btnResWireTransfer_Click(object sender, EventArgs e)
        {
            WireTransfer(this.myReserve.Id);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
        }

        protected void grTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow grv = e.Row;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (grv.Cells[8].Text == "True")
                {
<<<<<<< HEAD
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
                    e.Row.ForeColor = System.Drawing.Color.White;

=======
<<<<<<< HEAD
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
                    e.Row.ForeColor = System.Drawing.Color.White;

=======
                    e.Row.BackColor = System.Drawing.Color.Salmon;
                    e.Row.ForeColor = System.Drawing.Color.White;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    //HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");
                    //arEdit.ForeColor = System.Drawing.Color.White;

                }

<<<<<<< HEAD

=======
<<<<<<< HEAD

=======
                
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


            }
        }



        //protected void btnReissueVoidReasonOK_Click(object sender, EventArgs e)
        //{
        //    Reissue();
        //}

        private IEnumerable LoadTransactions(List<Hashtable> list)
        {
            myTransactionList = new List<TransactionGrid>();
            try
            {
                foreach (Hashtable tData in list)
                {
                    myTransactionList.Add(new TransactionGrid()
                    {
                        transactionId = (int)tData["id"],
                        dtTransaction = (DateTime)tData["transaction_date"],
                        transactionType = (string)tData["transaction_type"],
                        transactionAmount = ((double)((decimal)tData["amount"])).ToString("c"),
                        ExpenseLoss = (string)tData["expense_loss"],
                        draftNo = (string)tData["draft_no"],
                        isVoided = (string)tData["void"],
                        voidReason = (string)tData["void_reason"],
                        imageIdx = (int)tData["Image_Index"]
                    });
                }

                foreach (Hashtable ud in this.myReserve.getUnprintedDrafts())
                {
                    foreach (TransactionGrid tg in myTransactionList)
                    {
                        if (tg.transactionId == (int)ud["Id"])
                        {
                            tg.draftUnprinted = true;
                            break;
                        }
                    }
                }
                return myTransactionList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myTransactionList;
            }
        }

        private void ViewDraft()
        {
            try
            {
                mm.UpdateLastActivityTime();
                tr = new Transaction((int)grTransactions.SelectedDataKey.Value);

                Draft.MakeDraftNonNegotiable(tr.DraftId);   //mark draft as non-negotiable

                Dictionary<string, string> rptparms = new Dictionary<string, string>();

                rptparms.Add("Draft_Id", tr.DraftId.ToString());
                ShowReport(mm.DRAFT_REPORT, rptparms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = "Draft"; 
            //rptParms = 
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }

        private void PopulateReserveControls()
        {
            this.lblClaimantName.Text = myReserve.Claimant.Person.Name;
            this.lblLossPaid.Text = myReserve.PaidLoss.ToString("c");
            this.lblExpensePaid.Text = myReserve.PaidExpense.ToString("c");
            this.lblCoverageType.Text = myReserve.PolicyCoverageType().Description;
            this.lblLossReserve.Text = myReserve.NetLossReserve.ToString("c");
            this.lblExpenseReserve.Text = myReserve.NetExpenseReserve.ToString("c");
            this.lblCoverageLimit.Text = myReserve.CoverageDescription;
        }

        private void CheckReserveClaimLocks()
        {
            this.reserveLocked = new Reserve(this.myReserve.Id).FirstLockId != 0;

            this.claimLocked = new MedCsxLogic.Claim(this.myReserve.ClaimId).FirstLockId() != 0;
        }

        protected void btnShowLoss_Click(object sender, EventArgs e)
        {

        }

        protected void btnShowExpense_Click(object sender, EventArgs e)
        {

        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {

        }

        protected void btnTransactionRpt_Click(object sender, EventArgs e)
        {

        }

        class TransactionGrid
        {
            public int transactionId { get; set; }
            public int imageIdx { get; set; }
            public DateTime dtTransaction { get; set; }
            public string transactionType { get; set; }
            public string transactionAmount { get; set; }
            public string ExpenseLoss { get; set; }
            public string draftNo { get; set; }
            public string isVoided { get; set; }
            public string voidReason { get; set; }
            public bool draftUnprinted { get; set; }
        }
    }
}
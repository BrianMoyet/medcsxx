﻿<%@ Page Title="Assign Reserve" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAssignReserve.aspx.cs" Inherits="MedCSX.Claim.frmAssignReserve" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-3">
            <asp:Label ID="txtClaimId" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Reserve:
        </div>
        <div class="col-md-3">
            <asp:Label ID="txtReserveId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Assign To user:
        </div>
        <div class="col-md-3">
            <asp:DropDownList ID="cboAssignUser" runat="server"></asp:DropDownList>&nbsp;&nbsp;&nbsp;<%--Or Group: --%><asp:DropDownList ID="cboAssignGroup" Visible="false" runat="server"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"/>
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

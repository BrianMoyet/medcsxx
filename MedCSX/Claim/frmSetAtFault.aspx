﻿<%@ Page Title="Fault Not Determined" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmSetAtFault.aspx.cs" Inherits="MedCSX.Claim.frmSetAtFault" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h4>Fault Not Yet Determined</h4>
        </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                Loss Description: <asp:Label ID="lblLossDescription" runat="server" Text=""></asp:Label>
            </div>
        </div>
     <div class="row">
            <div class="col-md-12">
               <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                Paid Loss: <asp:Label ID="lblPaidLoss" ForeColor="Blue" runat="server" Text=""></asp:Label>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
               <hr />
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
              Was The Driver of the Insured Vehicle at Fault?
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <asp:RadioButtonList ID="rbAtFault" runat="server">
                    <asp:ListItem Value="3" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="4" Text="No"></asp:ListItem>
                    <asp:ListItem Value="0" Selected="True" Text="Undetermined"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
          
             
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click" />
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>

</asp:Content>

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCSX.App_Code;
using MedCsxDatabase;
using MedCsxLogic;
using Microsoft.Reporting.WebForms;



namespace MedCSX.Claim
{
    public partial class Default : System.Web.UI.Page
    {
        private static modLogic ml = new modLogic();
        private static clsDatabase db = new clsDatabase();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();


        #region Class Variables
        private const int maxRowsForDisplay = 500;              //the maximun number of rows that can be displayed at once
        public const string RowColorHashtableKey = "RowColor";  //the row color hashtable key for the ExtendedDataGridViews
        public int currentClaimType = 1;                        //the type of claim we are currently using
        bool firstActivated = true;                             //is this the first time the form has been activated?
        DateTime lastCountTime = DateTime.Now.AddDays(-1);      //keeps track of the last time the screen counts were updated 
        MedCsxLogic.Claim myClaim = null;                                   //place holder for claim
        Reserve res = null;                                     //place holder for reserve
        private string _selectedUser1;                          //Alert user from ComboBox
        private string _selectedUser2;                          //Reserve user from ComboBox
        private string _selectedUser3;                          //Diary user from ComboBox
        private string _selectedUser4;                          //Task user from ComboBox
        private string _selectedUser5;                          //Total Loss user from ComboBox
        private string _selectedUser6;
      
        //Total Loss user from ComboBox
        //private List<AlertGrid> myAlertList;                    //repository for alert DataGrid
        //private List<ReserveGrid> myReserveList;                //repository for reserve DataGrid
        //private List<DiaryGrid> myDiaryList;                    //repository for diary DataGrid
        //private List<TaskGrid> myTaskList;                      //repository for task DataGrid
        //private List<TtlLossGrid> myTtlLossList;                //repository for total loss DataGrid
        //private List<ReconGrid> myReconList;                    //repository for recon DataGrid
        //private List<ClaimsGrid> srchClaim;                     //repository for search results DataGrid
        //private List<SalvageGrid> mySalvageList;                //repository for salvage DataGrid
        //private List<Hashtable> DataRows;                       //holder for results from database
        private DataTable DataRows;
        private int DiaryMode = 1;
        private int DiaryThemeMode = 0;
        private int ActivationIteration = 0;


        /***** Selected Items ****/
        int claimID = 0;                                        //currently selected claim ID
        int alertID = 0;                                        //currently selected alert ID                            
        int diaryID = 0;                                        //currently selected diary ID                            
        int reserveID = 0;                                      //currently selected reserve ID    
        int salvageID = 0;                                      //currently selected salvage ID    
        int demandID = 0;

        /***** Indexes of Images  *****/
        public const int ImageIndex_Lock = 33;
        public const int ImageIndex_Book = 44;
        public const int ImageIndex_Pen = 50;
        public const int ImageIndex_Pencil = 51;
        public const int ImageIndex_Clock = 113;
        public const int ImageIndex_PencilLock = 115;
        public const int ImageIndex_Snowflake = 117;
        public const int ImageIndex_Sun = 118;

        /***** Miscellaneous Variables ****/
        bool isLoading = true;                                  //currently loading the form
        bool LoadedUserComboBoxes = false;                      //finished loading the user comboboxes
        bool CheckForAlerts = false;                            //currently checking for new alerts
        bool hasOverdueReserves = false;                        //user currently has overdue reserves
        bool hasOverdueDiaries = false;                         //user currently has overdue diaries
        bool hasOverdueSalvage = false;                         //user currently has overdue salvage
        public bool alertsScreenShowing = false;                //the alerts screen is currently being displayed
        /* to be created
         * frmAlerts fAlerts = null; //Alerts window
         * frmLocked fLocked = null; //Locked window
         * public frmNonIntrusiveAlerts alertWindow = null; //Non-intrusive alerts window
         * TaskBarNotifier myTaskBarNotifier = new TaskBarNotifier(); //task bar notifier window
         */
        int myCurrentAlertId;                                   //Alert ID of the current alert selected
        Hashtable ImageCache;                                   //cached images indexed by filename
        bool pastDueDiary = false;                              //diary entries are past due
        TimeSpan RefreshPeriod = new TimeSpan(0, 0, 2, 0);      //how often to refresh
        DateTime? currentHolidayDate = null;                    //stores the last date set in the holiday message function
        private string srchFilter = "";                         //scalar to modify filter stored procedure
        private int srchCriteria = 0;                           //Which search was performed so that filter can re-process
        private int filterOffice = 0;                           //Office number
        private int searchSortDirection = 0;

        /***** User ID's of the selected user *****/
        int reserveUserID = 0;
        int taskUserID = 0;
        int totalLossUserID = 0;
        int diaryUserID = 0;
        int alertUserID = 0;
        int salvageUserID = 0;
        int demandUserID = 0;

        /***** Date placeholders ****/
        DateTime myAlertsLoaded = ml.DEFAULT_DATE;              //time the alerts were loaded
        DateTime myReservesLoaded = ml.DEFAULT_DATE;            //time the reserves were loaded
        DateTime myDiaryLoaded = ml.DEFAULT_DATE;               //time the diaries were loaded
        DateTime myTasksLoaded = ml.DEFAULT_DATE;               //time the tasks were loaded
        DateTime myTotalLossLoaded = ml.DEFAULT_DATE;           //time the total losses were loaded
        DateTime mySalvageLoaded = ml.DEFAULT_DATE;
        DateTime myDemandsLoaded = ml.DEFAULT_DATE;                //time the Demands items were loaded

        /***** Recent Claims ****/
        //the recent claims menu items, kept in an array.  This way we can just change the 
        //item in this array and the corresponding menu item will change also
        MenuItem[] recentClaimsMenu = new MenuItem[10];
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (reserveUserID == 0)
                reserveUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);

            DataTable dt = mm.GetMyUrgentAlerts(mm.UserId);
            if (dt.Rows.Count > 0)
            {
                string url = "frmAlerts.aspx";
                string sAlert = "window.open('" + url + "', 'popup_windowAlert', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=700, height=300, copyhistory=no, left=400, top=200');";
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "popup_windowAlert", sAlert, true);
            }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
           

            if (!Page.IsPostBack)
            {

                LoadAlerts(false);
<<<<<<< HEAD
=======
=======
            LoadAlerts(false);

            if (!Page.IsPostBack)
            {
               
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                if (alertUserID == 0)
                    alertUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                //if (cboMyAlertsUsers.Text == "")
                //    cboMyAlertsUsers.Text = mm.GetUserName(this.alertUserID);
               
                //if (cboMyReserveUsers.Text == "")
                //    cboMyReserveUsers.Text = mm.GetUserName(reserveUserID);



               
                    mm.UpdateLastActivityTime();

                    //reset variables
                    this.hasOverdueDiaries = false;
                    this.hasOverdueReserves = false;
                    this.hasOverdueSalvage = false;
                    this.pastDueDiary = false;
                    this.lastCountTime = DateTime.Now.AddDays(-1);

                    //unlock all controls
<<<<<<< HEAD
                    //this.UnlockControls();
=======
<<<<<<< HEAD
                    //this.UnlockControls();
=======
                    this.UnlockControls();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                    //set up current user values
                    ml.CurrentUser.OpenClaims = 0;
                    ml.CurrentUser.Update();

                    //load the user's company authority
                    mm.LoadUserCompanyAuthority();

                    LoadUsersComboBoxes();

                    //splash.ShowNextMsg();

                    //set up the form

                    //TabContainer1_ActiveTabChanged(TabContainer1, null);
                    //this.cboDiaryPastDueTheme.SelectedIndex = ml.CurrentUser.DiaryThemeId;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  

                    this.txtUserName.Text = mm.GetUserName(Convert.ToInt32(Session["userID"]));

                   // bool reservesDisplayed = LoadReserves(false);
<<<<<<< HEAD
=======
=======
                    string txtDiaryTheme = "Normal";
                    switch (ml.CurrentUser.DiaryThemeId)
                    {
                        case 1:
                            txtDiaryTheme = "Summer";
                            break;
                        case 2:
                            txtDiaryTheme = "Christmas";
                            break;
                        default:
                            txtDiaryTheme = "Normal";
                            break;
                    }

                    this.txtUserName.Text = mm.GetUserName(Convert.ToInt32(Session["userID"]));

                    bool reservesDisplayed = LoadReserves(false);
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


                    if (this.hasOverdueReserves)
                    {
                        TabContainer1.ActiveTabIndex = 1;
                        TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                        return;
                    }
                    if (Request.QueryString["Ptabindex"] != null)
                    {
                        TabContainer1.ActiveTabIndex = Convert.ToInt32(Request.QueryString["Ptabindex"]);
                        TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                    }
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                //if (Request.QueryString["Ptabindex"] != null)
                //{
                //    TabContainer1.ActiveTabIndex = Convert.ToInt32(Request.QueryString["Ptabindex"]);
                //    TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                //}
<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e



                //splash.ShowNextMsg();

                //display user reserves.  if any are past due, they must be corrected before proceeding
                //bool reservesDisplayed = LoadReserves(false);

                //check menu options
                //this.mnuAutoClearOlderDiaryEntries.IsChecked = ml.CurrentUser.AutoClearOlderDiaries;
                //this.mnuAutoOpenImageRightClaims.IsChecked = ml.CurrentUser.AutoOpenImageRightClaims;
                //this.mnuSearchAsIType.IsChecked = ml.CurrentUser.SearchAsIType;
                //this.mnuWordWrapAlerts.IsChecked = ml.CurrentUser.WordWrapAlerts;



                this.pastDueDiary = false;
                    if (mm.GetMyDiaries(Convert.ToInt32(Session["userID"]), 1).Rows.Count > 0)    //get past due diaries
                    {   //select the my Diary tab
                        this.hasOverdueDiaries = true;
                        TabContainer1.ActiveTabIndex = 2;
                        TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);

                        if (Request.QueryString["Ptabindex"] != null)
                        {
                            TabContainer1.ActiveTabIndex = Convert.ToInt32(Request.QueryString["Ptabindex"]);
                            TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                        }
                        return;
                    }

                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    //foreach (Hashtable s in mm.GetMySalvage(Convert.ToInt32(Session["userID"])).Rows)
                    //{
                    //    Salvage salv = new Salvage((int)s["Salvage_Id"]);
                    //    if (salv.IsLocked)
                    //    {
                    //        this.hasOverdueSalvage = true;
                    //        TabContainer1.ActiveTabIndex = 6;
                    //        TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                    //        break;
                    //    }
                    //}


               
<<<<<<< HEAD
=======
=======
                    foreach (Hashtable s in mm.GetMySalvage(Convert.ToInt32(Session["userID"])).Rows)
                    {
                        Salvage salv = new Salvage((int)s["Salvage_Id"]);
                        if (salv.IsLocked)
                        {
                            this.hasOverdueSalvage = true;
                            TabContainer1.ActiveTabIndex = 6;
                            TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                            break;
                        }
                    }


                if (Request.QueryString["Ptabindex"] != null)
                {
                    TabContainer1.ActiveTabIndex = Convert.ToInt32(Request.QueryString["Ptabindex"]);
                    TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


                //if (Session["user_id"] != null)
                //{ }
                //else
                //{
                //    Response.Redirect("../default.aspx");
                //}
                //if (!Page.IsPostBack)
                //{
                //    //wSteps.ActiveStepIndex = 0;

                //    populateDDLShowFor();
                //    TabContainer1_ActiveTabChanged(TabContainer1, null);
                //}

                TabContainer1_ActiveTabChanged(null, null);
            }

            


        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            //rptParms = 
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }

        private Dictionary<string, string> GetReportParmsMyDiary()
        {
            string thisDiaryUser = ddlShowDiaryFor.SelectedValue as string;
            thisDiaryUser = thisDiaryUser.Trim();
            string[] a = thisDiaryUser.Split(' ');
            string fName = a[0];
            string lName = "";
            if (a.Length > 2)
                lName = a[1] + " " + a[2];
            else
                lName = a[1];
            diaryUserID = mm.GetReserveUserId(fName, lName);

            Dictionary<string, string> newparms = new Dictionary<string, string>();
            
            newparms.Add("user_id", diaryUserID.ToString());
            
            return newparms;
        }

        public int GetReserveUserId(string firstName, string lastName)
        {
            return db.GetIntFromStoredProcedure("usp_Get_User_Id_For_Name", firstName, lastName);
        }

        private List<int> ReserveIdsWithoutAdjusterEnteredNotes(int userId)
        {
            try
            {
                List<int> reserveIds = new List<int>();  //keep track of the reserve IDs
                List<Hashtable> ReservesWithoutNotes = (List<Hashtable>)mm.GetReservesWithoutNotes(userId);
                foreach (Hashtable row in ReservesWithoutNotes)
                {
                    if (!reserveIds.Contains((int)row["Reserve_Id"]))
                        reserveIds.Add((int)row["Reserve_Id"]);
                }
                return reserveIds;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return new List<int>();
            }
        }

     

        #region Lock/Unlock Controls
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        //private void UnlockControls()
        //{
        //    //enable all the specified controls
        //    this.EnableControls(true);
        //}

        //private void LockControls()
        //{
        //    //disable all the specified controls
        //    this.EnableControls(false);
        //}

      
<<<<<<< HEAD
=======
=======
        private void UnlockControls()
        {
            //enable all the specified controls
            this.EnableControls(true);
        }

        private void LockControls()
        {
            //disable all the specified controls
            this.EnableControls(false);
        }

        private void EnableControls(bool enabled)
        {
            //TODO
            //enable/disable all the specified controls
            /****** Menu ******/
            //this.mnuRecentClaims.IsEnabled = enabled;
            //this.mnuClaimNew.IsEnabled = enabled;
            //this.mnuOpenClaimNumber.IsEnabled = enabled;
            //this.mnuUnsavedFileNotes.IsEnabled = enabled;

            ///******* Tabs ******/
            //this.tpMyAlerts.IsEnabled = enabled;
            //this.tpMyDiary.IsEnabled = enabled;
            //this.tpMyTasks.IsEnabled = enabled;
            //this.tpMyReserves.IsEnabled = enabled;
            //this.tpMyTotalLosses.IsEnabled = enabled;
            //this.tpRecon.IsEnabled = enabled;
            //this.tpSearchClaims.IsEnabled = enabled;
            //this.tpMySalvage.IsEnabled = enabled;

            ///******* ToolStrip ******/
            //this.tsMain.IsEnabled = enabled;
        }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        #endregion


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtSearch.Text.Trim() == "")
            {
                this.srchCriteria = 0;
                lblSearch.Text = "You Must Enter a Valid Search Criteria.";
                return;
            }

            //this.srchCriteria = 7;
            bool claimsLoaded = LoadClaims(0, this.txtSearch.Text.Trim());
           txtSearch.Focus();

           

        }

      

        protected void rblSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = ((RadioButtonList)sender);
            var tag = int.Parse(cb.SelectedValue.ToString());
            bool claimsLoaded = false;



            switch (Convert.ToInt32(tag))
            {
                case 0:
                    txtSearch.TextMode = TextBoxMode.SingleLine;
                    recentpending.Visible = false;
                    search.Visible = true;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    btnSearch.Visible = true;
                    lblSearch.Text = "Claim Number";

                    break;
                case 1:  //policy number
                    txtSearch.TextMode = TextBoxMode.SingleLine;
                    recentpending.Visible = false;
                    search.Visible = true;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    lblSearch.Text = "Policy Number: ";
                    btnSearch.Visible = true;
                    break;
                case 2: //name search
                    recentpending.Visible = false;
                    search.Visible = false;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = true;
                    vehicleSearch.Visible = false;
                    btnSearch.Visible = true;

                    break;
                case 3: //date of loass
                    recentpending.Visible = false;
                    search.Visible = true;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    lblSearch.Text = "Date of Loss:";
                    txtSearch.TextMode = TextBoxMode.Date;
                    cvSearch.Enabled = true;
                    cvSearch.Type = ValidationDataType.Date;
                    btnSearch.Visible = true;
                    break;
                case 4:  //vehicle
                    recentpending.Visible = false;
                    search.Visible = false;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = true;

                    btnSearch.Visible = true;
                  
                  
                    break;
                case 5: //draft number
                    recentpending.Visible = false;
                    search.Visible = true;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    lblSearch.Text = "Draft Number";
                    txtSearch.TextMode = TextBoxMode.SingleLine;
                    btnSearch.Visible = true;
                    cvSearch.Enabled = false;


                    break;
                case 6:  //recent pending
                    vehicleSearch.Visible = false;
                    nameSearch.Visible = false;
                    recentpending.Visible = true;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    search.Visible = false;
                    
                    claimsLoaded = LoadClaims(0, rblRecentPending.SelectedValue);
                    break;
                case 7: //all pending
                    recentpending.Visible = false;

                    search.Visible = false;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    btnSearch.Visible = true;

                    claimsLoaded = LoadClaims(7, "0");
                    gvSearch.Visible = true;

                    Session["SearchTable"] = (DataTable)gvSearch.DataSource;
                    break;
<<<<<<< HEAD
=======
=======
                    search.Visible = false;
                    
                    claimsLoaded = LoadClaims(0, rblRecentPending.SelectedValue);
                    break;
                //case 7: //all locked pending
                //    vehicleSearch.Visible = false;
                //    nameSearch.Visible = false;
                //    recentpending.Visible = false;
                //    search.Visible = false;
                //    dt = myData.SearchClaimByAllLockedPending();
                //    gvSearch.DataSource = dt;
                //    gvSearch.DataBind();
                //    gvSearch.Visible = true;

                //    Session["SearchTable"] = dt;
                //    break;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                case 8:  //all locked pending
                    recentpending.Visible = false;

                    search.Visible = false;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    btnSearch.Visible = true;
                   
                    claimsLoaded = LoadClaims(0,"0");


                    break;
                case 9:  //phone number
                    recentpending.Visible = false;
                    search.Visible = true;
                    cvSearch.Enabled = false;
                    nameSearch.Visible = false;
                    vehicleSearch.Visible = false;
                    lblSearch.Text = "Phone Number: ";
                    txtSearch.Visible = true;
                    btnSearch.Visible = true;
                    txtSearch.TextMode = TextBoxMode.Phone;
                    
<<<<<<< HEAD
                    break;
                case 10:  //Vehicle
                    vehicleSearch.Visible = true;
                    nameSearch.Visible = false;
                    recentpending.Visible = false;
                    search.Visible = false;

                   
                    break;
=======
                    break;
                case 10:  //Vehicle
                    vehicleSearch.Visible = true;
                    nameSearch.Visible = false;
                    recentpending.Visible = false;
                    search.Visible = false;

                   
                    break;
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                case 11:  //email
                    txtSearch.TextMode = TextBoxMode.SingleLine;
                    vehicleSearch.Visible = true;
                    gvSearch.Visible = false;
                    nameSearch.Visible = false;
                    recentpending.Visible = false;
                    search.Visible = false;

                   
                    break;
                default:
                    break;
            }

            //dt.Dispose();
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

<<<<<<< HEAD


        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {


            //try
            //{
                if (TabContainer1.ActiveTabIndex == 0)  //alert
                {
                //DataTable dtAlert = new DataTable();

                //Alert myAlert = new Alert();
                //dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 1);

                //gvAlerts.DataSource = dtAlert;
                //gvAlerts.DataBind();
                //gvAlerts.Visible = true;

                //Session["AlertTable"] = dtAlert;

                /////////////////////////////////////////
                ///
                    if (alertUserID == 0)
                        alertUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                    LoadAlerts(false);
                   // bool reservesDisplayed = LoadReserves(false);
                }

                if (TabContainer1.ActiveTabIndex == 1)  //reserve
                {
                    bool reservesDisplayed = LoadReserves(false);

                    //if (this.hasOverdueReserves)
                    //{
                    //    //TabContainer1.ActiveTabIndex = 0;
                    //    //TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                    //    //done loading if reserves overdue
                    //    //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMyReserves);
                    //    return;
                    //}

                    // this.cboDiaryPastDueTheme.SelectedItem = txtDiaryTheme;

                    //display status
                    //this.UserName.Text = ml.CurrentUser.Name;

                    //splash.ShowNextMsg();

                    //display user reserves.  if any are past due, they must be corrected before proceeding
                    //bool reservesDisplayed = LoadReserves(false);

                    //check menu options
                    //this.mnuAutoClearOlderDiaryEntries.IsChecked = ml.CurrentUser.AutoClearOlderDiaries;
                    //this.mnuAutoOpenImageRightClaims.IsChecked = ml.CurrentUser.AutoOpenImageRightClaims;
                    //this.mnuSearchAsIType.IsChecked = ml.CurrentUser.SearchAsIType;
                    //this.mnuWordWrapAlerts.IsChecked = ml.CurrentUser.WordWrapAlerts;

                    //        if (this.hasOverdueReserves)
                    //        {   //done loading if reserves overdue
                    //            //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMyReserves);
                    //            return;
                    //        }

                    //        this.pastDueDiary = false;
                    //        if (mm.GetMyDiaries(mm.UserId, 1).Count > 0)    //get past due diaries
                    //        {   //select the my Diary tab
                    //            this.hasOverdueDiaries = true;
                    //            //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMyDiary);
                    //            return;
                    //        }

                    //        foreach (Hashtable s in mm.GetMySalvage(mm.UserId))
                    //        {
                    //            Salvage salv = new Salvage((int)s["Salvage_Id"]);
                    //            if (salv.IsLocked)
                    //            {
                    //                this.hasOverdueSalvage = true;
                    //                //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMySalvage);
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //{
                    //    mf.ProcessError(ex);
                    //}
                }

                if (TabContainer1.ActiveTabIndex == 2)  //diary
                {
                    DisplayMyDiaries();
                    btnShowDiaryAll_Click(null, null);

                    //DataTable dt = new DataTable();

                //Diary myData = new Diary();
                //dt = myData.GetDiary(Convert.ToInt32(ddlShowDiaryFor.SelectedValue));

                //gvDiary.DataSource = dt;
                //gvDiary.DataBind();
                //gvDiary.Visible = true;

                //Session["DiaryTable"] = dt;
            }

                if (TabContainer1.ActiveTabIndex == 3) //tasks
                {
                    bool TasksLoaded = LoadTasks(true);
                    //DataTable dt = new DataTable();

                    //Loses myData = new Loses();
                    //dt = myData.GetLoses(Convert.ToInt32(ddlShowLosesFor.SelectedValue));

                    //gvLoses.DataSource = dt;
                    //gvLoses.DataBind();
                    //gvLoses.Visible = true;

                    //Session["LosesTable"] = dt;
=======


        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {


            //try
            //{
                if (TabContainer1.ActiveTabIndex == 0)  //alert
                {
                //DataTable dtAlert = new DataTable();

                //Alert myAlert = new Alert();
                //dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 1);

                //gvAlerts.DataSource = dtAlert;
                //gvAlerts.DataBind();
                //gvAlerts.Visible = true;

                //Session["AlertTable"] = dtAlert;

                /////////////////////////////////////////
                ///
                    if (alertUserID == 0)
                        alertUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                    LoadAlerts(false);
<<<<<<< HEAD
                   // bool reservesDisplayed = LoadReserves(false);
=======
                    bool reservesDisplayed = LoadReserves(false);
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }

                if (TabContainer1.ActiveTabIndex == 4) //total loses
                {
<<<<<<< HEAD
                    bool sameUser = false;
                    string origUser = ddlShowLosesFor.Text;
                    string thisUser = ddlShowLosesFor.SelectedValue as string;
                    if (ddlShowLosesFor.Text == thisUser)
                        sameUser = true;
                    if (!firstActivated && (thisUser == null))
                    {
                        ddlShowLosesFor.Text = origUser;
                        return;
                    }
                    if (thisUser != null)
                    {
                        thisUser = thisUser.Trim();
                        string[] a = thisUser.Split(' ');
                        string fName = a[0];
                        string lName = "";
                        if (a.Length > 2)
                            lName = a[1] + " " + a[2];
                        else
                            lName = a[1];
                        this.totalLossUserID = mm.GetReserveUserId(fName, lName);
                    }
                    bool TLLoaded = LoadTtlLoss(sameUser);
                    ////DataTable dt = new DataTable();

                    ////Task myData = new Task();
                    ////dt = myData.GetTask(Convert.ToInt32(ddlShowTasksFor.SelectedValue));

                    ////gvTasks.DataSource = dt;
                    ////gvTasks.DataBind();
                    ////gvTasks.Visible = true;

                    ////Session["TasksTable"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 6) //salvage
                {
                //DisplayMySalvage();
                //this.DisplayMyRecon();
                //DataTable dt = new DataTable();

                //Salvage myData = new Salvage();
                //dt = myData.GetSalvage(Convert.ToInt32(ddlShowSalvageFor.SelectedValue));

                //gvSalvage.DataSource = dt;
                //gvSalvage.DataBind();
                //gvSalvage.Visible = true;

                //Session["SalvageTable"] = dt;
            }

                if (TabContainer1.ActiveTabIndex == 7) //demands
                {
                DisplayMyDemands();
                //DataTable dt = new DataTable();

                //Demands myData = new Demands();
                //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

                //gvDemands.DataSource = dt;
                //gvDemands.DataBind();
                //gvDemands.Visible = true;

                //Session["DemandsTable"] = dt;
            }
                if (TabContainer1.ActiveTabIndex == 7) //demands
                {
                    DisplayMyDemands();
                    //DataTable dt = new DataTable();

                    //Demands myData = new Demands();
                    //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

                    //gvDemands.DataSource = dt;
                    //gvDemands.DataBind();
                    //gvDemands.Visible = true;

                    //Session["DemandsTable"] = dt;
                }
            //}

            //catch (Exception ex)
            //{
            //    throw;
            //}
            //finally
            //{

            //}
        }

        private void DisplayMyDiaries()
        {
            try
            {
                //if (firstActivated)
                //    return;

                mm.UpdateLastActivityTime();



                bool diariesLoaded = LoadDiaries(false);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private void DisplayMyRecon()
        {
            try
            {
                if (firstActivated)
                    return;

                mm.UpdateLastActivityTime();

            

                this.reconRefresh_Click(null, null);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
             
            }
        }

        private void DisplayMySalvage()
        {
            try
            {
                //if (firstActivated)
                //    return;

                mm.UpdateLastActivityTime();



                bool salvageLoaded = LoadSalvage(true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private void DisplayMyDemands()
        {
            try
            {
                if (firstActivated)
                    return;

                mm.UpdateLastActivityTime();



                bool salvageLoaded = LoadDemands(true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private bool LoadSalvage(bool ignoreDate)
        {
            DateTime salvageLastChanged = new User(this.salvageUserID).MySalvageChanged.Add(RefreshPeriod);
            if ((this.mySalvageLoaded > salvageLastChanged) && ignoreDate)
                return false;       //if salvage hasn't changed, don't reload grid

            this.mySalvageLoaded = ml.DBNow();      //salvage loaded now
            DataRows = new DataTable();
            List<Hashtable> DataList = new List<Hashtable>();
            List<Hashtable> Rows2Keep = new List<Hashtable>();


            string thisSalvageUser = this.ddlShowSalvageFor.SelectedValue.Trim();
            string[] a = thisSalvageUser.Split(' ');
            string fName = a[0];
            string lName = "";
            if (a.Length > 2)
                lName = a[1] + " " + a[2];
            else
                lName = a[1];
            int thisSalvageUserid = mm.GetReserveUserId(fName, lName);



           
            DataRows = mm.GetMySalvage(thisSalvageUserid);     //get salvage items
            ArrayList mySalvageIDs = new ArrayList();

            //this.openSalvage.IsEnabled = false;
            double Days = 0;
            string sId = "";
            int WarningItems = 0;
            int OverdueItems = 0;
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow row in DataRows.Rows)
                {
                    //TODO
                    //if (((DateTime?)sData["Completed_Date"] == null) || ((DateTime?)sData["Completed_Date"] == ml.DEFAULT_DATE))
                    //{
                    //    Salvage mySalvage = new Salvage((int)sData["Salvage_Id"]);
                    //    try
                    //    {   //get days between open date and today
                    //        TimeSpan myTS = DateTime.Today.Subtract(mySalvage.CreatedDate);
                    //        Days = myTS.TotalDays;
                    //        bool isLocked = mySalvage.LockSalvage(mySalvage.ReserveId);  //send alerts if necessary
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        mf.ProcessError(ex);
                    //        Days = 0;
                    //    }

                    //    if (Days > 60)
                    //    {
                    //        if (mySalvage.IsLocked)
                    //        {
                    //            sData.Add(RowColorHashtableKey, Colors.Salmon);
                    //            OverdueItems++;
                    //            Rows2Keep.Add(sData);
                    //        }
                    //        else
                    //        {
                    //            sData.Add(RowColorHashtableKey, Colors.Yellow);
                    //            WarningItems++;
                    //        }
                    //    }
                    //    else if (Days > 30)
                    //    {
                    //        sData.Add(RowColorHashtableKey, Colors.Yellow);
                    //        WarningItems++;
                    //    }

                    //    DataList.Add(sData);
                    //}
                }

                //if (OverdueItems > 0)
                //{
                //    DataList = Rows2Keep;  //force overdue salvage to be worked first
                //    mySalvageWarning.Text = "You Have " + OverdueItems + " Salvage Items > 60 Days Old.  Title Processing Must be Documented Before Proceeding.";
                //    //mySalvageWarning.Content = "You Have " + OverdueItems + " Salvage Items > " + MedCsXSystem.Settings.SalvageRequirementDays + " Days Old.  Title Processing Must be Documented Before Proceeding.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Red;
                //}
                //else if (WarningItems > 0)
                //{
                //    mySalvageWarning.Text = "You Have " + WarningItems + " Salvage Items > 30 Days Old.  They Should be Reviewed for Title Processing.";
                //    //mySalvageWarning.Content = "You Have " + WarningItems + " Average Reserves > " + MedCsXSystem.Settings.SalvageWarningDays + " Days Old.  They Should be Reviewed for Title Processing.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    mySalvageWarning.Text = "You Have No Overdue Salvage Items.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Blue;
                //}


                //if (OverdueItems > 0)
                //{
                //    LockControls();
                //    //TODO
                //    //this.tpMySalvage.IsEnabled = true;
                //}
                //else
                //    UnlockControls();

                this.gvSalvage.DataSource = DataRows;
                gvSalvage.DataBind();
                //openSalvage.IsEnabled = true;
                this.gvSalvage.SelectedIndex = 0;
            }
            else
            {
                //this.grMySalvage.ItemsSource = null;
                mySalvageWarning.Text = "You Have No Overdue Salvage Items.";
                mySalvageWarning.ForeColor = System.Drawing.Color.Blue;
                //UnlockControls();
            }
            
            this.UpdateCount(this.lblNoSalvage, DataList.Count);
            return true;
        }

        private void reconRefresh_Click(object sender, EventArgs e)
        {
            bool reconLoaded = false;
            if (rblReconType.SelectedValue == "0")  //all
                reconLoaded = LoadRecon(0, this.lblAllRecon);
            else 
                reconLoaded = LoadRecon(1, this.lblPDRecon);
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            //if (DiaryMode == 1)
            //{
                DiaryMode = 0;
                LoadDiaries(false);
           //}
        }

        private bool LoadClaims(int mode, string claimId = "", string secondString = "", string vehString3 = "", string vehString4 = "")
        {
            DataRows = new DataTable();

            switch (Convert.ToInt32(rblSearchType.SelectedValue))
            {
                case 1:  //policy number
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByPolicyNo(claimId);
                    else
                        DataRows = mm.SearchClaimsByPolicyNo(claimId, srchFilter);
                    break;
                case 2:  //Name
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByName(claimId + "%", secondString + "%");
                    else
                        DataRows = mm.SearchClaimsByName(claimId + "%", secondString + "%", srchFilter);
                    break;
                case 3: //date of loss
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDateOfLoss(claimId);
                    else
                        DataRows = mm.SearchClaimsByDateOfLoss(claimId, srchFilter);
                    break;
                case 4:  //vehicle
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByVehicle(claimId, secondString, vehString3, vehString4);
                    else
                        DataRows = mm.SearchClaimsByVehicle(claimId, secondString, vehString3, vehString4, srchFilter);
                    break;
                case 5:
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDraftNo(claimId);
                    else
                        DataRows = mm.SearchClaimsByDraftNo(claimId, srchFilter);
                    break;
                case 6:  //Recent Pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDate(int.Parse(claimId));
                    else
                        DataRows = mm.SearchClaimsByDate(int.Parse(claimId), srchFilter);
                    break;
                case 7:  // all locked pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAllPending();
                    else
                        DataRows = mm.SearchClaimsByAllPending(this.srchFilter);
                    break;
                case 8: // all lock pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAllLockedPending();
                    else
                        DataRows = mm.SearchClaimsByAllLockedPending(srchFilter);
                    break;
                case 9: //by phone number
                    if (srchFilter == "")
                        DataRows = mm.SearchRecordsByPhoneNumber(claimId);
                    break;
                case 10:  //by address
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAddress(claimId, secondString, vehString3);
                    else
                        DataRows = mm.SearchClaimsByAddress(claimId, secondString, vehString3, srchFilter);
                    break;
                case 11: //email
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByEmail(claimId);
                    else
                        DataRows = mm.SearchClaimsByEmail(claimId, srchFilter);
                    break;
                default:  //case 0  by claim number
                    if (claimId != "")
                        DataRows = mm.SearchClaimsByClaimNo(claimId);
                    else
                        DataRows = new DataTable();
                    break;
            }
            //if (DataRows.Rows.Count > 0)
            //{
            //    foreach (Hashtable dr in DataRows)
            //        mf.ConvertHashtableKeysToLowercase(dr);

            //    this.AddClaimIcons(DataRows);

            //    this.grMySearchResults.ItemsSource = LoadClaims(DataRows);
            //    grMySearchResults.SelectedIndex = 0;
            //}
            gvSearch.DataSource = DataRows;
            gvSearch.DataBind();

            Session["SearchTable"] = DataRows;
            //grMySearchResults.Items.Refresh();
            lblNumbFound.Text = "(" + DataRows.Rows.Count + " Claims Found)";
            //mf.MouseNormal();
            return true;
        }

        private bool LoadReserves(bool ignoreDate)
        {
            //if (myReserveList != null)
            //{
            //    myReserveList.Clear();
            //    this.grMyReserves.ItemsSource = myReserveList;
            //}
            //this.grMyReserves.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime ReservesLastChanged = new User(this.reserveUserID).MyReservesChanged.Add(RefreshPeriod);
            if ((this.myReservesLoaded > ReservesLastChanged) && ignoreDate)
                return false;  //if reserves haven't changed, don't reload grid

            this.myReservesLoaded = ml.DBNow();  //reserves loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyReserves(this.reserveUserID);  //get reserves

            //get the reserves that don't have file notes
            List<int> reservesWithoutNotes = new List<int>();
            if (DataRows.Rows.Count > 0)
                reservesWithoutNotes = this.ReserveIdsWithoutAdjusterEnteredNotes(this.reserveUserID);

            double Days = 0;
            bool isLocked = false;
            int OverdueItems = 0;
            int WarningItems = 0;
            DataTable Rows2Keep = new DataTable();  //overdue reserves
            foreach (DataRow row in DataRows.Rows)
            {
                if (reservesWithoutNotes.Contains((int)row["Reserve_Id"]))
                    row["Image_Index"] = ImageIndex_Pencil;

                try  //days open will determine the color of the row
                {
                    TimeSpan myTimeSpan = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                    Days = myTimeSpan.TotalDays;
                }
                catch (Exception ex)
                {
                    Days = 0;
                }

                //Is claim locked?
                isLocked = (bool)row["Reserve_Locked"];
                if (isLocked)  //set image for loced claims
                {
                    if ((int)row["Image_Index"] == ImageIndex_Pen)
                        row["Image_Index"] = ImageIndex_PencilLock;
                    else
                        row["Image_Index"] = ImageIndex_Lock;
                }

                if (((string)row["Reserving"] == "Average") && (!(bool)row["Reserve_Closed"]) &&
                    (!(bool)row["Reserve_Closed_Pending_Salvage"]) && (!(bool)row["Reserve_Closed_Pending_Subro"]))
                {
                    if (Days > MedCsXSystem.Settings.ReserveRequirementDays)  //reserve is older than required number of days
                    {
                        if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))  //reserve is not locked OR it is locked and requires reserves
                        {
                            if (row["Policy_No"] != null)
                            {
                                if (((string)row["Policy_No"]).Trim() != "")  //claim must have policy # before it is considered overdue
                                {
                                    //row.Add(RowColorHashtableKey, Colors.Salmon);
                                    OverdueItems++;
                                    Rows2Keep.ImportRow(row);
                                    //Rows2Keep.Rows.Add(row);
                                }
                            }
                        }
                        else
                        {
                            //row.Add(RowColorHashtableKey, Colors.Yellow);
                            WarningItems++;
                        }
                    }
                    else
                    {
                        if (Days > MedCsXSystem.Settings.ReserveWarningDays)  //reserve is older than required warning days
                        {
                            //row.Add(RowColorHashtableKey, Colors.Yellow);
                            WarningItems++;
                        }
                    }
                }
            }

            if (OverdueItems > 0)
            {
                //TODO DataRows = Rows2Keep;  //force overdue reserves to be worked first
                lblMyReserveWarning.Text = "You Have " + OverdueItems + " Average Reserves > " + MedCsXSystem.Settings.ReserveRequirementDays + " Days Old.  Case Reserves Must be Set Before Proceeding.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Red;
            }
            else if (WarningItems > 0)
            {
                lblMyReserveWarning.Text = "You Have " + WarningItems + " Average Reserves > " + MedCsXSystem.Settings.ReserveWarningDays + " Days Old.  They Should be Reviewed for Case Reserves.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMyReserveWarning.Text = "You Have No Overdue Average Reserves.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Blue;
            }

            if (DataRows.Rows.Count > 0)
            {
              

                gvReserves.DataSource = DataRows;
                gvReserves.DataBind();

                Session["ReservesTable"] = DataRows;
                this.UpdateCount(this.lblNoReserves, DataRows.Rows.Count);
            }

            ////Lock Controls if Overdue
            //if ((OverdueItems > 0) && (Convert.ToInt32(Session["userID"]) == this.reserveUserID))
            //{
            //    this.hasOverdueReserves = true;
            //    this.LockControls();
            //    //this.tpMyReserves.IsEnabled = true;
            //}
            //else
            //{
            //    this.hasOverdueReserves = false;
            //    this.UnlockControls();
            //}

            //gvReserves.Items.Refresh();
            return true;
        }

        private bool LoadDiaries(bool ignoreDate)
        {
            //grMyDiaries.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime DiariesLastChanged = new User(this.diaryUserID).MyDiaryChanged.Add(RefreshPeriod);
            if ((this.myDiaryLoaded > DiariesLastChanged) && ignoreDate)
                return false;  //if diaries haven't changed, don't reload grid

            this.myDiaryLoaded = ml.DBNow();  //diaries loaded now

            string thisDiaryUser = ddlShowDiaryFor.SelectedValue as string;
           
           
                thisDiaryUser = thisDiaryUser.Trim();
                string[] a = thisDiaryUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.diaryUserID = mm.GetReserveUserId(fName, lName);
              
            DataRows = mm.GetMyDiaries(diaryUserID, DiaryMode);  //get diaries



            //grMyDiaries.Items.Clear();
            int overdue = 0;
            //openDiary.IsEnabled = false;
            //btnAll.IsEnabled = true;
            int gracePeriod = ml.CurrentUser.getDiaryGracePeriod();
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow Row in DataRows.AsEnumerable())
                {

                    bool overDue = false;
                    if ((DateTime)Row["diary_due_date"] <= ml.DBNow().AddDays(gracePeriod * -1))
                    {

                        overdue++;
                    }
=======
                    bool reservesDisplayed = LoadReserves(false);

                    //if (this.hasOverdueReserves)
                    //{
                    //    //TabContainer1.ActiveTabIndex = 0;
                    //    //TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                    //    //done loading if reserves overdue
                    //    //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMyReserves);
                    //    return;
                    //}

                    // this.cboDiaryPastDueTheme.SelectedItem = txtDiaryTheme;

                    //display status
                    //this.UserName.Text = ml.CurrentUser.Name;

                    //splash.ShowNextMsg();

                    //display user reserves.  if any are past due, they must be corrected before proceeding
                    //bool reservesDisplayed = LoadReserves(false);

                    //check menu options
                    //this.mnuAutoClearOlderDiaryEntries.IsChecked = ml.CurrentUser.AutoClearOlderDiaries;
                    //this.mnuAutoOpenImageRightClaims.IsChecked = ml.CurrentUser.AutoOpenImageRightClaims;
                    //this.mnuSearchAsIType.IsChecked = ml.CurrentUser.SearchAsIType;
                    //this.mnuWordWrapAlerts.IsChecked = ml.CurrentUser.WordWrapAlerts;

                    //        if (this.hasOverdueReserves)
                    //        {   //done loading if reserves overdue
                    //            //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMyReserves);
                    //            return;
                    //        }

                    //        this.pastDueDiary = false;
                    //        if (mm.GetMyDiaries(mm.UserId, 1).Count > 0)    //get past due diaries
                    //        {   //select the my Diary tab
                    //            this.hasOverdueDiaries = true;
                    //            //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMyDiary);
                    //            return;
                    //        }

                    //        foreach (Hashtable s in mm.GetMySalvage(mm.UserId))
                    //        {
                    //            Salvage salv = new Salvage((int)s["Salvage_Id"]);
                    //            if (salv.IsLocked)
                    //            {
                    //                this.hasOverdueSalvage = true;
                    //                //this.tcPages.SelectedIndex = this.tcPages.Items.IndexOf(this.tpMySalvage);
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //{
                    //    mf.ProcessError(ex);
                    //}
                }

                if (TabContainer1.ActiveTabIndex == 2)  //diary
                {
                    DisplayMyDiaries();
                    btnShowDiaryAll_Click(null, null);

                    //DataTable dt = new DataTable();
<<<<<<< HEAD

                //Diary myData = new Diary();
                //dt = myData.GetDiary(Convert.ToInt32(ddlShowDiaryFor.SelectedValue));

                //gvDiary.DataSource = dt;
                //gvDiary.DataBind();
                //gvDiary.Visible = true;

                //Session["DiaryTable"] = dt;
            }

                if (TabContainer1.ActiveTabIndex == 3) //tasks
                {
                    bool TasksLoaded = LoadTasks(true);
                    //DataTable dt = new DataTable();

                    //Loses myData = new Loses();
                    //dt = myData.GetLoses(Convert.ToInt32(ddlShowLosesFor.SelectedValue));

                    //gvLoses.DataSource = dt;
                    //gvLoses.DataBind();
                    //gvLoses.Visible = true;

                    //Session["LosesTable"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 4) //total loses
                {
                    bool sameUser = false;
                    string origUser = ddlShowLosesFor.Text;
                    string thisUser = ddlShowLosesFor.SelectedValue as string;
                    if (ddlShowLosesFor.Text == thisUser)
                        sameUser = true;
                    if (!firstActivated && (thisUser == null))
                    {
                        ddlShowLosesFor.Text = origUser;
                        return;
                    }
                    if (thisUser != null)
                    {
                        thisUser = thisUser.Trim();
                        string[] a = thisUser.Split(' ');
                        string fName = a[0];
                        string lName = "";
                        if (a.Length > 2)
                            lName = a[1] + " " + a[2];
                        else
                            lName = a[1];
                        this.totalLossUserID = mm.GetReserveUserId(fName, lName);
                    }
                    bool TLLoaded = LoadTtlLoss(sameUser);
                    ////DataTable dt = new DataTable();

                    ////Task myData = new Task();
                    ////dt = myData.GetTask(Convert.ToInt32(ddlShowTasksFor.SelectedValue));

                    ////gvTasks.DataSource = dt;
                    ////gvTasks.DataBind();
                    ////gvTasks.Visible = true;

                    ////Session["TasksTable"] = dt;
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }

                if (overdue > 0)
                {
                    lblDairyWarning.Visible = true;
                    lblDairyWarning.Text = "You Have " + overdue + " Overdue Diaries.";
                    //btnShowDiaryAll.Enabled = false;
                }
                else
                {
<<<<<<< HEAD
                    lblDairyWarning.Visible = false;
                }
                //gvDiary.SelectedIndex = 0;
                this.UpdateCount(this.lblNoDiaries, DataRows.Rows.Count);
                //openDiary.IsEnabled = true;
            }


            this.UpdateCount(this.lblNoDiaries, DataRows.Rows.Count);
            gvDiary.DataSource = DataRows;
            gvDiary.DataBind();
            Session["DiaryTable"] = DataRows;

            //grMyDiaries.Items.Refresh();
            return true;
        }

       

        private bool LoadAlerts(bool ignoreDate, bool AlwaysDisplayAlerts = false)
        {   //pulls Alerts for user
            //grMyAlerts.ItemsSource = null;  //disassociate the array with the ItemsSource
            try
            {
                string thisAlertUser = ddlShowAlertsFor.SelectedValue.Trim();
                string[] a = thisAlertUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
           
                this.alertUserID = mm.GetReserveUserId(fName, lName);
            }
            catch (Exception ex)
            {
                this.alertUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            }


            DataRows = new DataTable();

            // if (ddlShowAlertsFor.SelectedValue == ml.CurrentUser.Name)
            if (rblAlertType.SelectedValue == "1")
            {
                DateTime AlertsLastChanged = new User(this.alertUserID).MyAlertsChanged.Add(RefreshPeriod);
                if ((this.myAlertsLoaded > AlertsLastChanged) && ignoreDate)
                    return false;  //if alerts haven't changed, don't reload grid

                this.myAlertsLoaded = ml.DBNow();  //alerts loaded now
                //DataRows = mm.GetMyAlerts(this.alertUserID, ml.DEFAULT_DATE, true);  //get alerts
                DataRows = mm.GetMyAlerts(this.alertUserID, ml.DEFAULT_DATE, true);  //get alerts
            }
            else
            {
                DataRows = mm.GetAlertsToMe(this.alertUserID);
            }

            List<Hashtable> Row2Keep = new List<Hashtable>();
            //foreach (DataRow row in DataRows.Rows)
            //{
            //    DateTime deferredDate = (DateTime)Row["Deferred_Date"];
            //    DateTime dateSent = (DateTime)Row["date_sent"];
            //    if (deferredDate > dateSent)
            //        Row.Add("date_received", deferredDate);
            //    else
            //        Row.Add("date_received", dateSent);

            //    if ((string)Row["Urgent"] == "Yes")
            //    {
            //        if (!Row.ContainsKey("RowColor"))
            //            Row.Add("RowColor", System.Drawing.Color.Salmon);
            //        else
            //            Row["RowColor"] = System.Drawing.Color.Salmon;
            //        Row2Keep.Add(Row);
            //    }
            //}
            //if ((Row2Keep.Count > 0) && (this.AlertsToMe.IsChecked == true))
            //    DataRows = Row2Keep;
            //grMyAlerts.Items.Clear();
            //openClaim.IsEnabled = false;
            //openNotePad.IsEnabled = false;
            //ClearMyAlert.IsEnabled = false;
            //ClearMyAlert.Visibility = Visibility.Collapsed;
            //if (DataRows.Rows.Count > 0)
            //{   //load grid with Alerts 
                //this.grMyAlerts.ItemsSource = LoadAlerts(DataRows);
                //if (this.AlertsToMe.IsChecked == true)
                //{
                //    grMyAlerts.Columns[3].Header = "Sent From";
                //    Sent_From.Content = "Sent From:";
                //}
                //else
                //{
                //    this.grMyAlerts.Columns[3].Header = "Sent To";
                //    this.Sent_From.Content = "Sent To:";
                //}
                //openNotePad.IsEnabled = true;
                //openClaim.IsEnabled = true;
                //ClearMyAlert.IsEnabled = true;
                //ClearMyAlert.Visibility = Visibility.Visible;
                //grMyAlerts.UpdateLayout();
                //grMyAlerts.SelectedIndex = 0;
                this.UpdateCount(this.lblNoAlerts, DataRows.Rows.Count);
                Session["DefaultAlertTable"] = DataRows;
                gvAlerts.DataSource = DataRows;
                gvAlerts.DataBind();
            //}

            //grMyAlerts.Items.Refresh();
            return true;
        }

        private bool LoadTasks(bool ignoreDate)
        {
            //grMyTasks.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime TasksLastChanged = new User(this.taskUserID).MyTasksChanged.Add(RefreshPeriod);
            if ((this.myTasksLoaded > TasksLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myTasksLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyTasks(this.taskUserID);  //get diaries

            //grMyTasks.Items.Clear();
            //opentskClaim.IsEnabled = false;
            newTask.Enabled = false;
            clearTask.Enabled = false;
            if (DataRows.Rows.Count > 0)
            {
                gvTasks.DataSource = DataRows;
                gvTasks.DataBind();
                //opentskClaim.IsEnabled = true;
                newTask.Enabled = true;
                clearTask.Enabled = true;
                //gvTasks.SelectedIndex = 0;
            }
            
            this.UpdateCount(this.lblNoTasks, DataRows.Rows.Count);
            return true;
        }

        private bool LoadDemands(bool ignoreDate)
        {
            //grMyTasks.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime DemandsLastChanged = new User(this.demandUserID).MyTasksChanged.Add(RefreshPeriod);
            if ((this.myDemandsLoaded > DemandsLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myDemandsLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyDemands(this.demandUserID);  //get diaries

            //grMyTasks.Items.Clear();
            //opentskClaim.IsEnabled = false;
            //newTask.IsEnabled = false;
            //clearTask.IsEnabled = false;
            //if (DataRows.Rows.Count > 0)
            //{
            gvDemands.DataSource = DataRows;
            gvDemands.DataBind();
            //opentskClaim.IsEnabled = true;
            //newTask.IsEnabled = true;
            //clearTask.IsEnabled = true;
            //gvDemands.SelectedIndex = 0;
            //}

            this.UpdateCount(this.lblNoDemands, DataRows.Rows.Count);
            return true;
        }

        private bool LoadTtlLoss(bool ignoreDate)
        {
            //grMyTtlLosses.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime TtlLossesLastChanged = new User(this.totalLossUserID).MyTotalLossesChanged.Add(RefreshPeriod);
            if ((this.myTotalLossLoaded > TtlLossesLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myTotalLossLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyTtlLosses(this.totalLossUserID);  //get diaries

            //grMyTtlLosses.Items.Clear();
            //opentlRes.IsEnabled = false;
            double Days = 0;
            bool isLocked = false;
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow row in DataRows.Rows)
                {
                    //Decorate rows
                 
                    try
                    {   //get days between open date and today
                        TimeSpan myTS = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                        Days = myTS.TotalDays;
                    }
                    catch (Exception ex)
                    {
                        Days = 0;
                    }

                    Reserve res = new Reserve((int)row["Reserve_Id"]); //get associated reserve
                    if ((res.Claim.FirstLockId() != 0) || (res.FirstLockId != 0))
                        isLocked = true;
                    else
                        isLocked = false;

                    if (isLocked)
                    {  //set image for locked reserves
                        //if (row.ContainsKey("Image_Index"))
                        //{
                        //    if ((int)thisRow["Image_Index"] == ImageIndex_Pencil)
                        //        thisRow["Image_Index"] = ImageIndex_PencilLock;
                        //    else
                        //        thisRow["Image_Index"] = ImageIndex_Lock;
                        //}
                    }

                    //if (((string)thisRow["Reserving"] == "Average") && !res.isClosed && 
                    //    !res.isClosedPendingSalvage && !res.isClosedPendingSubro)
                    //{
                    //if (Days > MedCsXSystem.Settings.ReserveRequirementDays)
                    //{
                    //    if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))
                    //    {
                    //        if (((string)thisRow["Policy_No"]).Trim() != "")
                    //            thisRow.Add(RowColorHashtableKey, Colors.Salmon);
                    //        //item is overdue
                    //    }
                    //    else
                    //    {   //item is about to become overdue
                    //        thisRow.Add(RowColorHashtableKey, Colors.Yellow);
                    //    }
                    //}
                    //else
                    //{
                    //    if (Days > MedCsXSystem.Settings.ReserveWarningDays)
                    //        thisRow.Add(RowColorHashtableKey, Colors.Yellow);
                    //    //item is about to become overdue
                    //}
                    //}
=======
                //DisplayMySalvage();
                //this.DisplayMyRecon();
                //DataTable dt = new DataTable();

                //Salvage myData = new Salvage();
                //dt = myData.GetSalvage(Convert.ToInt32(ddlShowSalvageFor.SelectedValue));

                //gvSalvage.DataSource = dt;
                //gvSalvage.DataBind();
                //gvSalvage.Visible = true;

                //Session["SalvageTable"] = dt;
            }

                if (TabContainer1.ActiveTabIndex == 7) //demands
                {
                DisplayMyDemands();
                //DataTable dt = new DataTable();

                //Demands myData = new Demands();
                //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

                //gvDemands.DataSource = dt;
                //gvDemands.DataBind();
                //gvDemands.Visible = true;

                //Session["DemandsTable"] = dt;
            }
                if (TabContainer1.ActiveTabIndex == 7) //demands
                {
                    DisplayMyDemands();
                    //DataTable dt = new DataTable();

                    //Demands myData = new Demands();
                    //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

                    //gvDemands.DataSource = dt;
                    //gvDemands.DataBind();
                    //gvDemands.Visible = true;

                    //Session["DemandsTable"] = dt;
=======

                //Diary myData = new Diary();
                //dt = myData.GetDiary(Convert.ToInt32(ddlShowDiaryFor.SelectedValue));

                //gvDiary.DataSource = dt;
                //gvDiary.DataBind();
                //gvDiary.Visible = true;

                //Session["DiaryTable"] = dt;
            }

                if (TabContainer1.ActiveTabIndex == 3) //tasks
                {
                    bool TasksLoaded = LoadTasks(true);
                    //DataTable dt = new DataTable();

                    //Loses myData = new Loses();
                    //dt = myData.GetLoses(Convert.ToInt32(ddlShowLosesFor.SelectedValue));

                    //gvLoses.DataSource = dt;
                    //gvLoses.DataBind();
                    //gvLoses.Visible = true;

                    //Session["LosesTable"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 4) //total loses
                {
                    bool sameUser = false;
                    string origUser = ddlShowLosesFor.Text;
                    string thisUser = ddlShowLosesFor.SelectedValue as string;
                    if (ddlShowLosesFor.Text == thisUser)
                        sameUser = true;
                    if (!firstActivated && (thisUser == null))
                    {
                        ddlShowLosesFor.Text = origUser;
                        return;
                    }
                    if (thisUser != null)
                    {
                        thisUser = thisUser.Trim();
                        string[] a = thisUser.Split(' ');
                        string fName = a[0];
                        string lName = "";
                        if (a.Length > 2)
                            lName = a[1] + " " + a[2];
                        else
                            lName = a[1];
                        this.totalLossUserID = mm.GetReserveUserId(fName, lName);
                    }
                    bool TLLoaded = LoadTtlLoss(sameUser);
                    ////DataTable dt = new DataTable();

                    ////Task myData = new Task();
                    ////dt = myData.GetTask(Convert.ToInt32(ddlShowTasksFor.SelectedValue));

                    ////gvTasks.DataSource = dt;
                    ////gvTasks.DataBind();
                    ////gvTasks.Visible = true;

                    ////Session["TasksTable"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 6) //salvage
                {
                //DisplayMySalvage();
                //this.DisplayMyRecon();
                //DataTable dt = new DataTable();

                //Salvage myData = new Salvage();
                //dt = myData.GetSalvage(Convert.ToInt32(ddlShowSalvageFor.SelectedValue));

                //gvSalvage.DataSource = dt;
                //gvSalvage.DataBind();
                //gvSalvage.Visible = true;

                //Session["SalvageTable"] = dt;
            }

                if (TabContainer1.ActiveTabIndex == 7) //demands
                {
                DisplayMyDemands();
                //DataTable dt = new DataTable();

                //Demands myData = new Demands();
                //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

                //gvDemands.DataSource = dt;
                //gvDemands.DataBind();
                //gvDemands.Visible = true;

                //Session["DemandsTable"] = dt;
            }
                if (TabContainer1.ActiveTabIndex == 7) //demands
                {
                    DisplayMyDemands();
                    //DataTable dt = new DataTable();

                    //Demands myData = new Demands();
                    //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

                    //gvDemands.DataSource = dt;
                    //gvDemands.DataBind();
                    //gvDemands.Visible = true;

                    //Session["DemandsTable"] = dt;
                }
            //}

            //catch (Exception ex)
            //{
            //    throw;
            //}
            //finally
            //{

            //}
        }

        private void DisplayMyDiaries()
        {
            try
            {
                //if (firstActivated)
                //    return;

                mm.UpdateLastActivityTime();



                bool diariesLoaded = LoadDiaries(false);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private void DisplayMyRecon()
        {
            try
            {
                if (firstActivated)
                    return;

                mm.UpdateLastActivityTime();

            

                this.reconRefresh_Click(null, null);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
             
            }
        }

        private void DisplayMySalvage()
        {
            try
            {
                //if (firstActivated)
                //    return;

                mm.UpdateLastActivityTime();



                bool salvageLoaded = LoadSalvage(true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private void DisplayMyDemands()
        {
            try
            {
                if (firstActivated)
                    return;

                mm.UpdateLastActivityTime();



                bool salvageLoaded = LoadDemands(true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private bool LoadSalvage(bool ignoreDate)
        {
            DateTime salvageLastChanged = new User(this.salvageUserID).MySalvageChanged.Add(RefreshPeriod);
            if ((this.mySalvageLoaded > salvageLastChanged) && ignoreDate)
                return false;       //if salvage hasn't changed, don't reload grid

            this.mySalvageLoaded = ml.DBNow();      //salvage loaded now
            DataRows = new DataTable();
            List<Hashtable> DataList = new List<Hashtable>();
            List<Hashtable> Rows2Keep = new List<Hashtable>();


            string thisSalvageUser = this.ddlShowSalvageFor.SelectedValue.Trim();
            string[] a = thisSalvageUser.Split(' ');
            string fName = a[0];
            string lName = "";
            if (a.Length > 2)
                lName = a[1] + " " + a[2];
            else
                lName = a[1];
            int thisSalvageUserid = mm.GetReserveUserId(fName, lName);



           
            DataRows = mm.GetMySalvage(thisSalvageUserid);     //get salvage items
            ArrayList mySalvageIDs = new ArrayList();

            //this.openSalvage.IsEnabled = false;
            double Days = 0;
            string sId = "";
            int WarningItems = 0;
            int OverdueItems = 0;
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow row in DataRows.Rows)
                {
                    //TODO
                    //if (((DateTime?)sData["Completed_Date"] == null) || ((DateTime?)sData["Completed_Date"] == ml.DEFAULT_DATE))
                    //{
                    //    Salvage mySalvage = new Salvage((int)sData["Salvage_Id"]);
                    //    try
                    //    {   //get days between open date and today
                    //        TimeSpan myTS = DateTime.Today.Subtract(mySalvage.CreatedDate);
                    //        Days = myTS.TotalDays;
                    //        bool isLocked = mySalvage.LockSalvage(mySalvage.ReserveId);  //send alerts if necessary
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        mf.ProcessError(ex);
                    //        Days = 0;
                    //    }

                    //    if (Days > 60)
                    //    {
                    //        if (mySalvage.IsLocked)
                    //        {
                    //            sData.Add(RowColorHashtableKey, Colors.Salmon);
                    //            OverdueItems++;
                    //            Rows2Keep.Add(sData);
                    //        }
                    //        else
                    //        {
                    //            sData.Add(RowColorHashtableKey, Colors.Yellow);
                    //            WarningItems++;
                    //        }
                    //    }
                    //    else if (Days > 30)
                    //    {
                    //        sData.Add(RowColorHashtableKey, Colors.Yellow);
                    //        WarningItems++;
                    //    }

                    //    DataList.Add(sData);
                    //}
                }

                //if (OverdueItems > 0)
                //{
                //    DataList = Rows2Keep;  //force overdue salvage to be worked first
                //    mySalvageWarning.Text = "You Have " + OverdueItems + " Salvage Items > 60 Days Old.  Title Processing Must be Documented Before Proceeding.";
                //    //mySalvageWarning.Content = "You Have " + OverdueItems + " Salvage Items > " + MedCsXSystem.Settings.SalvageRequirementDays + " Days Old.  Title Processing Must be Documented Before Proceeding.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Red;
                //}
                //else if (WarningItems > 0)
                //{
                //    mySalvageWarning.Text = "You Have " + WarningItems + " Salvage Items > 30 Days Old.  They Should be Reviewed for Title Processing.";
                //    //mySalvageWarning.Content = "You Have " + WarningItems + " Average Reserves > " + MedCsXSystem.Settings.SalvageWarningDays + " Days Old.  They Should be Reviewed for Title Processing.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    mySalvageWarning.Text = "You Have No Overdue Salvage Items.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Blue;
                //}


                //if (OverdueItems > 0)
                //{
                //    LockControls();
                //    //TODO
                //    //this.tpMySalvage.IsEnabled = true;
                //}
                //else
                //    UnlockControls();

                this.gvSalvage.DataSource = DataRows;
                gvSalvage.DataBind();
                //openSalvage.IsEnabled = true;
                this.gvSalvage.SelectedIndex = 0;
            }
            else
            {
                //this.grMySalvage.ItemsSource = null;
                mySalvageWarning.Text = "You Have No Overdue Salvage Items.";
                mySalvageWarning.ForeColor = System.Drawing.Color.Blue;
                UnlockControls();
            }
            
            this.UpdateCount(this.lblNoSalvage, DataList.Count);
            return true;
        }

        private void reconRefresh_Click(object sender, EventArgs e)
        {
            bool reconLoaded = false;
            if (rblReconType.SelectedValue == "0")  //all
                reconLoaded = LoadRecon(0, this.lblAllRecon);
            else 
                reconLoaded = LoadRecon(1, this.lblPDRecon);
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            //if (DiaryMode == 1)
            //{
                DiaryMode = 0;
                LoadDiaries(false);
           //}
        }

        private bool LoadClaims(int mode, string claimId = "", string secondString = "", string vehString3 = "", string vehString4 = "")
        {
            DataRows = new DataTable();

            switch (Convert.ToInt32(rblSearchType.SelectedValue))
            {
                case 1:  //policy number
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByPolicyNo(claimId);
                    else
                        DataRows = mm.SearchClaimsByPolicyNo(claimId, srchFilter);
                    break;
                case 2:  //Name
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByName(claimId + "%", secondString + "%");
                    else
                        DataRows = mm.SearchClaimsByName(claimId + "%", secondString + "%", srchFilter);
                    break;
                case 3: //date of loss
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDateOfLoss(claimId);
                    else
                        DataRows = mm.SearchClaimsByDateOfLoss(claimId, srchFilter);
                    break;
                case 4:  //vehicle
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByVehicle(claimId, secondString, vehString3, vehString4);
                    else
                        DataRows = mm.SearchClaimsByVehicle(claimId, secondString, vehString3, vehString4, srchFilter);
                    break;
                case 5:
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDraftNo(claimId);
                    else
                        DataRows = mm.SearchClaimsByDraftNo(claimId, srchFilter);
                    break;
                case 6:  //Recent Pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDate(int.Parse(claimId));
                    else
                        DataRows = mm.SearchClaimsByDate(int.Parse(claimId), srchFilter);
                    break;
                //case 7:  // all locked pending
                //    if (srchFilter == "")
                //        DataRows = mm.SearchClaimsByAllPending();
                //    else
                //        DataRows = mm.SearchClaimsByAllPending(this.srchFilter);
                //    break;
                case 8: // all lock pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAllLockedPending();
                    else
                        DataRows = mm.SearchClaimsByAllLockedPending(srchFilter);
                    break;
                case 9: //by phone number
                    if (srchFilter == "")
                        DataRows = mm.SearchRecordsByPhoneNumber(claimId);
                    break;
                case 10:  //by address
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAddress(claimId, secondString, vehString3);
                    else
                        DataRows = mm.SearchClaimsByAddress(claimId, secondString, vehString3, srchFilter);
                    break;
                case 11: //email
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByEmail(claimId);
                    else
                        DataRows = mm.SearchClaimsByEmail(claimId, srchFilter);
                    break;
                default:  //case 0  by claim number
                    if (claimId != "")
                        DataRows = mm.SearchClaimsByClaimNo(claimId);
                    else
                        DataRows = new DataTable();
                    break;
            }
            //if (DataRows.Rows.Count > 0)
            //{
            //    foreach (Hashtable dr in DataRows)
            //        mf.ConvertHashtableKeysToLowercase(dr);

            //    this.AddClaimIcons(DataRows);

            //    this.grMySearchResults.ItemsSource = LoadClaims(DataRows);
            //    grMySearchResults.SelectedIndex = 0;
            //}
            gvSearch.DataSource = DataRows;
            gvSearch.DataBind();

            Session["SearchTable"] = DataRows;
            //grMySearchResults.Items.Refresh();
            lblNumbFound.Text = "(" + DataRows.Rows.Count + " Claims Found)";
            //mf.MouseNormal();
            return true;
        }

        private bool LoadReserves(bool ignoreDate)
        {
            //if (myReserveList != null)
            //{
            //    myReserveList.Clear();
            //    this.grMyReserves.ItemsSource = myReserveList;
            //}
            //this.grMyReserves.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime ReservesLastChanged = new User(this.reserveUserID).MyReservesChanged.Add(RefreshPeriod);
            if ((this.myReservesLoaded > ReservesLastChanged) && ignoreDate)
                return false;  //if reserves haven't changed, don't reload grid

            this.myReservesLoaded = ml.DBNow();  //reserves loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyReserves(this.reserveUserID);  //get reserves

            //get the reserves that don't have file notes
            List<int> reservesWithoutNotes = new List<int>();
            if (DataRows.Rows.Count > 0)
                reservesWithoutNotes = this.ReserveIdsWithoutAdjusterEnteredNotes(this.reserveUserID);

            double Days = 0;
            bool isLocked = false;
            int OverdueItems = 0;
            int WarningItems = 0;
            DataTable Rows2Keep = new DataTable();  //overdue reserves
            foreach (DataRow row in DataRows.Rows)
            {
                if (reservesWithoutNotes.Contains((int)row["Reserve_Id"]))
                    row["Image_Index"] = ImageIndex_Pencil;

                try  //days open will determine the color of the row
                {
                    TimeSpan myTimeSpan = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                    Days = myTimeSpan.TotalDays;
                }
                catch (Exception ex)
                {
                    Days = 0;
                }

                //Is claim locked?
                isLocked = (bool)row["Reserve_Locked"];
                if (isLocked)  //set image for loced claims
                {
                    if ((int)row["Image_Index"] == ImageIndex_Pen)
                        row["Image_Index"] = ImageIndex_PencilLock;
                    else
                        row["Image_Index"] = ImageIndex_Lock;
                }

                if (((string)row["Reserving"] == "Average") && (!(bool)row["Reserve_Closed"]) &&
                    (!(bool)row["Reserve_Closed_Pending_Salvage"]) && (!(bool)row["Reserve_Closed_Pending_Subro"]))
                {
                    if (Days > MedCsXSystem.Settings.ReserveRequirementDays)  //reserve is older than required number of days
                    {
                        if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))  //reserve is not locked OR it is locked and requires reserves
                        {
                            if (row["Policy_No"] != null)
                            {
                                if (((string)row["Policy_No"]).Trim() != "")  //claim must have policy # before it is considered overdue
                                {
                                    //row.Add(RowColorHashtableKey, Colors.Salmon);
                                    OverdueItems++;
                                    Rows2Keep.ImportRow(row);
                                    //Rows2Keep.Rows.Add(row);
                                }
                            }
                        }
                        else
                        {
                            //row.Add(RowColorHashtableKey, Colors.Yellow);
                            WarningItems++;
                        }
                    }
                    else
                    {
                        if (Days > MedCsXSystem.Settings.ReserveWarningDays)  //reserve is older than required warning days
                        {
                            //row.Add(RowColorHashtableKey, Colors.Yellow);
                            WarningItems++;
                        }
                    }
                }
            }

            if (OverdueItems > 0)
            {
                //TODO DataRows = Rows2Keep;  //force overdue reserves to be worked first
                lblMyReserveWarning.Text = "You Have " + OverdueItems + " Average Reserves > " + MedCsXSystem.Settings.ReserveRequirementDays + " Days Old.  Case Reserves Must be Set Before Proceeding.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Red;
            }
            else if (WarningItems > 0)
            {
                lblMyReserveWarning.Text = "You Have " + WarningItems + " Average Reserves > " + MedCsXSystem.Settings.ReserveWarningDays + " Days Old.  They Should be Reviewed for Case Reserves.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMyReserveWarning.Text = "You Have No Overdue Average Reserves.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Blue;
            }

            if (DataRows.Rows.Count > 0)
            {
              

                gvReserves.DataSource = DataRows;
                gvReserves.DataBind();

                Session["ReservesTable"] = DataRows;
                this.UpdateCount(this.lblNoReserves, DataRows.Rows.Count);
            }

            //Lock Controls if Overdue
            if ((OverdueItems > 0) && (Convert.ToInt32(Session["userID"]) == this.reserveUserID))
            {
                this.hasOverdueReserves = true;
                this.LockControls();
                //this.tpMyReserves.IsEnabled = true;
            }
            else
            {
                this.hasOverdueReserves = false;
                this.UnlockControls();
            }

            //gvReserves.Items.Refresh();
            return true;
        }

        private bool LoadDiaries(bool ignoreDate)
        {
            //grMyDiaries.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime DiariesLastChanged = new User(this.diaryUserID).MyDiaryChanged.Add(RefreshPeriod);
            if ((this.myDiaryLoaded > DiariesLastChanged) && ignoreDate)
                return false;  //if diaries haven't changed, don't reload grid

            this.myDiaryLoaded = ml.DBNow();  //diaries loaded now

            string thisDiaryUser = ddlShowDiaryFor.SelectedValue as string;
           
           
                thisDiaryUser = thisDiaryUser.Trim();
                string[] a = thisDiaryUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.diaryUserID = mm.GetReserveUserId(fName, lName);
              
            DataRows = mm.GetMyDiaries(diaryUserID, DiaryMode);  //get diaries



            //grMyDiaries.Items.Clear();
            int overdue = 0;
            //openDiary.IsEnabled = false;
            //btnAll.IsEnabled = true;
            int gracePeriod = ml.CurrentUser.getDiaryGracePeriod();
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow Row in DataRows.AsEnumerable())
                {

                    bool overDue = false;
                    if ((DateTime)Row["diary_due_date"] <= ml.DBNow().AddDays(gracePeriod * -1))
                    {

                        overdue++;
                    }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
                }
            //}

            //catch (Exception ex)
            //{
            //    throw;
            //}
            //finally
            //{

            //}
        }

        private void DisplayMyDiaries()
        {
            try
            {
                //if (firstActivated)
                //    return;

                mm.UpdateLastActivityTime();



                bool diariesLoaded = LoadDiaries(false);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private void DisplayMyRecon()
        {
            try
            {
                if (firstActivated)
                    return;

                mm.UpdateLastActivityTime();

            

                this.reconRefresh_Click(null, null);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
             
            }
        }

        private void DisplayMySalvage()
        {
            try
            {
                //if (firstActivated)
                //    return;

                mm.UpdateLastActivityTime();



                bool salvageLoaded = LoadSalvage(true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private void DisplayMyDemands()
        {
            try
            {
                if (firstActivated)
                    return;

                mm.UpdateLastActivityTime();



<<<<<<< HEAD
                bool salvageLoaded = LoadDemands(true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {

            }
        }

        private bool LoadSalvage(bool ignoreDate)
        {
            DateTime salvageLastChanged = new User(this.salvageUserID).MySalvageChanged.Add(RefreshPeriod);
            if ((this.mySalvageLoaded > salvageLastChanged) && ignoreDate)
                return false;       //if salvage hasn't changed, don't reload grid

            this.mySalvageLoaded = ml.DBNow();      //salvage loaded now
            DataRows = new DataTable();
            List<Hashtable> DataList = new List<Hashtable>();
            List<Hashtable> Rows2Keep = new List<Hashtable>();


            string thisSalvageUser = this.ddlShowSalvageFor.SelectedValue.Trim();
            string[] a = thisSalvageUser.Split(' ');
            string fName = a[0];
            string lName = "";
            if (a.Length > 2)
                lName = a[1] + " " + a[2];
            else
                lName = a[1];
            int thisSalvageUserid = mm.GetReserveUserId(fName, lName);



           
            DataRows = mm.GetMySalvage(thisSalvageUserid);     //get salvage items
            ArrayList mySalvageIDs = new ArrayList();

            //this.openSalvage.IsEnabled = false;
            double Days = 0;
            string sId = "";
            int WarningItems = 0;
            int OverdueItems = 0;
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow row in DataRows.Rows)
                {
                    //TODO
                    //if (((DateTime?)sData["Completed_Date"] == null) || ((DateTime?)sData["Completed_Date"] == ml.DEFAULT_DATE))
                    //{
                    //    Salvage mySalvage = new Salvage((int)sData["Salvage_Id"]);
                    //    try
                    //    {   //get days between open date and today
                    //        TimeSpan myTS = DateTime.Today.Subtract(mySalvage.CreatedDate);
                    //        Days = myTS.TotalDays;
                    //        bool isLocked = mySalvage.LockSalvage(mySalvage.ReserveId);  //send alerts if necessary
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        mf.ProcessError(ex);
                    //        Days = 0;
                    //    }

                    //    if (Days > 60)
                    //    {
                    //        if (mySalvage.IsLocked)
                    //        {
                    //            sData.Add(RowColorHashtableKey, Colors.Salmon);
                    //            OverdueItems++;
                    //            Rows2Keep.Add(sData);
                    //        }
                    //        else
                    //        {
                    //            sData.Add(RowColorHashtableKey, Colors.Yellow);
                    //            WarningItems++;
                    //        }
                    //    }
                    //    else if (Days > 30)
                    //    {
                    //        sData.Add(RowColorHashtableKey, Colors.Yellow);
                    //        WarningItems++;
                    //    }

                    //    DataList.Add(sData);
                    //}
                }

                //if (OverdueItems > 0)
                //{
                //    DataList = Rows2Keep;  //force overdue salvage to be worked first
                //    mySalvageWarning.Text = "You Have " + OverdueItems + " Salvage Items > 60 Days Old.  Title Processing Must be Documented Before Proceeding.";
                //    //mySalvageWarning.Content = "You Have " + OverdueItems + " Salvage Items > " + MedCsXSystem.Settings.SalvageRequirementDays + " Days Old.  Title Processing Must be Documented Before Proceeding.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Red;
                //}
                //else if (WarningItems > 0)
                //{
                //    mySalvageWarning.Text = "You Have " + WarningItems + " Salvage Items > 30 Days Old.  They Should be Reviewed for Title Processing.";
                //    //mySalvageWarning.Content = "You Have " + WarningItems + " Average Reserves > " + MedCsXSystem.Settings.SalvageWarningDays + " Days Old.  They Should be Reviewed for Title Processing.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    mySalvageWarning.Text = "You Have No Overdue Salvage Items.";
                //    mySalvageWarning.ForeColor = System.Drawing.Color.Blue;
                //}


                //if (OverdueItems > 0)
                //{
                //    LockControls();
                //    //TODO
                //    //this.tpMySalvage.IsEnabled = true;
                //}
                //else
                //    UnlockControls();

                this.gvSalvage.DataSource = DataRows;
                gvSalvage.DataBind();
                //openSalvage.IsEnabled = true;
                this.gvSalvage.SelectedIndex = 0;
            }
            else
            {
                //this.grMySalvage.ItemsSource = null;
                mySalvageWarning.Text = "You Have No Overdue Salvage Items.";
                mySalvageWarning.ForeColor = System.Drawing.Color.Blue;
                //UnlockControls();
            }
            
            this.UpdateCount(this.lblNoSalvage, DataList.Count);
            return true;
        }

        private void reconRefresh_Click(object sender, EventArgs e)
        {
            bool reconLoaded = false;
            if (rblReconType.SelectedValue == "0")  //all
                reconLoaded = LoadRecon(0, this.lblAllRecon);
            else 
                reconLoaded = LoadRecon(1, this.lblPDRecon);
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            //if (DiaryMode == 1)
            //{
                DiaryMode = 0;
                LoadDiaries(false);
           //}
        }

        private bool LoadClaims(int mode, string claimId = "", string secondString = "", string vehString3 = "", string vehString4 = "")
        {
            DataRows = new DataTable();

            switch (Convert.ToInt32(rblSearchType.SelectedValue))
            {
                case 1:  //policy number
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByPolicyNo(claimId);
                    else
                        DataRows = mm.SearchClaimsByPolicyNo(claimId, srchFilter);
                    break;
                case 2:  //Name
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByName(claimId + "%", secondString + "%");
                    else
                        DataRows = mm.SearchClaimsByName(claimId + "%", secondString + "%", srchFilter);
                    break;
                case 3: //date of loss
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDateOfLoss(claimId);
                    else
                        DataRows = mm.SearchClaimsByDateOfLoss(claimId, srchFilter);
                    break;
                case 4:  //vehicle
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByVehicle(claimId, secondString, vehString3, vehString4);
                    else
                        DataRows = mm.SearchClaimsByVehicle(claimId, secondString, vehString3, vehString4, srchFilter);
                    break;
                case 5:
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDraftNo(claimId);
                    else
                        DataRows = mm.SearchClaimsByDraftNo(claimId, srchFilter);
                    break;
                case 6:  //Recent Pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByDate(int.Parse(claimId));
                    else
                        DataRows = mm.SearchClaimsByDate(int.Parse(claimId), srchFilter);
                    break;
                case 7:  // all locked pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAllPending();
                    else
                        DataRows = mm.SearchClaimsByAllPending(this.srchFilter);
                    break;
                case 8: // all lock pending
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAllLockedPending();
                    else
                        DataRows = mm.SearchClaimsByAllLockedPending(srchFilter);
                    break;
                case 9: //by phone number
                    if (srchFilter == "")
                        DataRows = mm.SearchRecordsByPhoneNumber(claimId);
                    break;
                case 10:  //by address
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByAddress(claimId, secondString, vehString3);
                    else
                        DataRows = mm.SearchClaimsByAddress(claimId, secondString, vehString3, srchFilter);
                    break;
                case 11: //email
                    if (srchFilter == "")
                        DataRows = mm.SearchClaimsByEmail(claimId);
                    else
                        DataRows = mm.SearchClaimsByEmail(claimId, srchFilter);
                    break;
                default:  //case 0  by claim number
                    if (claimId != "")
                        DataRows = mm.SearchClaimsByClaimNo(claimId);
                    else
                        DataRows = new DataTable();
                    break;
            }
            //if (DataRows.Rows.Count > 0)
            //{
            //    foreach (Hashtable dr in DataRows)
            //        mf.ConvertHashtableKeysToLowercase(dr);

            //    this.AddClaimIcons(DataRows);

            //    this.grMySearchResults.ItemsSource = LoadClaims(DataRows);
            //    grMySearchResults.SelectedIndex = 0;
            //}
            gvSearch.DataSource = DataRows;
            gvSearch.DataBind();

            Session["SearchTable"] = DataRows;
            //grMySearchResults.Items.Refresh();
            lblNumbFound.Text = "(" + DataRows.Rows.Count + " Claims Found)";
            //mf.MouseNormal();
            return true;
        }

        private bool LoadReserves(bool ignoreDate)
        {
            //if (myReserveList != null)
            //{
            //    myReserveList.Clear();
            //    this.grMyReserves.ItemsSource = myReserveList;
            //}
            //this.grMyReserves.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime ReservesLastChanged = new User(this.reserveUserID).MyReservesChanged.Add(RefreshPeriod);
            if ((this.myReservesLoaded > ReservesLastChanged) && ignoreDate)
                return false;  //if reserves haven't changed, don't reload grid

            this.myReservesLoaded = ml.DBNow();  //reserves loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyReserves(this.reserveUserID);  //get reserves

            //get the reserves that don't have file notes
            List<int> reservesWithoutNotes = new List<int>();
            if (DataRows.Rows.Count > 0)
                reservesWithoutNotes = this.ReserveIdsWithoutAdjusterEnteredNotes(this.reserveUserID);

            double Days = 0;
            bool isLocked = false;
            int OverdueItems = 0;
            int WarningItems = 0;
            DataTable Rows2Keep = new DataTable();  //overdue reserves
            foreach (DataRow row in DataRows.Rows)
            {
                if (reservesWithoutNotes.Contains((int)row["Reserve_Id"]))
                    row["Image_Index"] = ImageIndex_Pencil;

                try  //days open will determine the color of the row
                {
                    TimeSpan myTimeSpan = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                    Days = myTimeSpan.TotalDays;
                }
                catch (Exception ex)
                {
                    Days = 0;
                }

                //Is claim locked?
                isLocked = (bool)row["Reserve_Locked"];
                if (isLocked)  //set image for loced claims
                {
                    if ((int)row["Image_Index"] == ImageIndex_Pen)
                        row["Image_Index"] = ImageIndex_PencilLock;
                    else
                        row["Image_Index"] = ImageIndex_Lock;
                }

                if (((string)row["Reserving"] == "Average") && (!(bool)row["Reserve_Closed"]) &&
                    (!(bool)row["Reserve_Closed_Pending_Salvage"]) && (!(bool)row["Reserve_Closed_Pending_Subro"]))
                {
                    if (Days > MedCsXSystem.Settings.ReserveRequirementDays)  //reserve is older than required number of days
                    {
                        if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))  //reserve is not locked OR it is locked and requires reserves
                        {
                            if (row["Policy_No"] != null)
                            {
                                if (((string)row["Policy_No"]).Trim() != "")  //claim must have policy # before it is considered overdue
                                {
                                    //row.Add(RowColorHashtableKey, Colors.Salmon);
                                    OverdueItems++;
                                    Rows2Keep.ImportRow(row);
                                    //Rows2Keep.Rows.Add(row);
                                }
                            }
                        }
                        else
                        {
                            //row.Add(RowColorHashtableKey, Colors.Yellow);
                            WarningItems++;
                        }
                    }
                    else
                    {
                        if (Days > MedCsXSystem.Settings.ReserveWarningDays)  //reserve is older than required warning days
                        {
                            //row.Add(RowColorHashtableKey, Colors.Yellow);
                            WarningItems++;
                        }
                    }
                }
            }

            if (OverdueItems > 0)
            {
                //TODO DataRows = Rows2Keep;  //force overdue reserves to be worked first
                lblMyReserveWarning.Text = "You Have " + OverdueItems + " Average Reserves > " + MedCsXSystem.Settings.ReserveRequirementDays + " Days Old.  Case Reserves Must be Set Before Proceeding.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Red;
            }
            else if (WarningItems > 0)
            {
                lblMyReserveWarning.Text = "You Have " + WarningItems + " Average Reserves > " + MedCsXSystem.Settings.ReserveWarningDays + " Days Old.  They Should be Reviewed for Case Reserves.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMyReserveWarning.Text = "You Have No Overdue Average Reserves.";
                lblMyReserveWarning.ForeColor = System.Drawing.Color.Blue;
            }

            if (DataRows.Rows.Count > 0)
            {
              

                gvReserves.DataSource = DataRows;
                gvReserves.DataBind();

                Session["ReservesTable"] = DataRows;
                this.UpdateCount(this.lblNoReserves, DataRows.Rows.Count);
            }

            ////Lock Controls if Overdue
            //if ((OverdueItems > 0) && (Convert.ToInt32(Session["userID"]) == this.reserveUserID))
            //{
            //    this.hasOverdueReserves = true;
            //    this.LockControls();
            //    //this.tpMyReserves.IsEnabled = true;
            //}
            //else
            //{
            //    this.hasOverdueReserves = false;
            //    this.UnlockControls();
            //}

            //gvReserves.Items.Refresh();
            return true;
        }

        private bool LoadDiaries(bool ignoreDate)
        {
            //grMyDiaries.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime DiariesLastChanged = new User(this.diaryUserID).MyDiaryChanged.Add(RefreshPeriod);
            if ((this.myDiaryLoaded > DiariesLastChanged) && ignoreDate)
                return false;  //if diaries haven't changed, don't reload grid

            this.myDiaryLoaded = ml.DBNow();  //diaries loaded now

            string thisDiaryUser = ddlShowDiaryFor.SelectedValue as string;
           
           
                thisDiaryUser = thisDiaryUser.Trim();
                string[] a = thisDiaryUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.diaryUserID = mm.GetReserveUserId(fName, lName);
              
            DataRows = mm.GetMyDiaries(diaryUserID, DiaryMode);  //get diaries



            //grMyDiaries.Items.Clear();
            int overdue = 0;
            //openDiary.IsEnabled = false;
            //btnAll.IsEnabled = true;
            int gracePeriod = ml.CurrentUser.getDiaryGracePeriod();
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow Row in DataRows.AsEnumerable())
                {

                    bool overDue = false;
                    if ((DateTime)Row["diary_due_date"] <= ml.DBNow().AddDays(gracePeriod * -1))
                    {

                        overdue++;
                    }
                }

                if (overdue > 0)
                {
                    lblDairyWarning.Visible = true;
                    lblDairyWarning.Text = "You Have " + overdue + " Overdue Diaries.";
                    //btnShowDiaryAll.Enabled = false;
                }
                else
                {
                    lblDairyWarning.Visible = false;
                }
                //gvDiary.SelectedIndex = 0;
                this.UpdateCount(this.lblNoDiaries, DataRows.Rows.Count);
                //openDiary.IsEnabled = true;
            }


            this.UpdateCount(this.lblNoDiaries, DataRows.Rows.Count);
            gvDiary.DataSource = DataRows;
            gvDiary.DataBind();
            Session["DiaryTable"] = DataRows;

            //grMyDiaries.Items.Refresh();
            return true;
        }

       

        private bool LoadAlerts(bool ignoreDate, bool AlwaysDisplayAlerts = false)
        {   //pulls Alerts for user
            //grMyAlerts.ItemsSource = null;  //disassociate the array with the ItemsSource
            try
            {
                string thisAlertUser = ddlShowAlertsFor.SelectedValue.Trim();
                string[] a = thisAlertUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
           
                this.alertUserID = mm.GetReserveUserId(fName, lName);
            }
            catch (Exception ex)
            {
                this.alertUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            }


            DataRows = new DataTable();

            // if (ddlShowAlertsFor.SelectedValue == ml.CurrentUser.Name)
            if (rblAlertType.SelectedValue == "1")
            {
                DateTime AlertsLastChanged = new User(this.alertUserID).MyAlertsChanged.Add(RefreshPeriod);
                if ((this.myAlertsLoaded > AlertsLastChanged) && ignoreDate)
                    return false;  //if alerts haven't changed, don't reload grid

                this.myAlertsLoaded = ml.DBNow();  //alerts loaded now
                //DataRows = mm.GetMyAlerts(this.alertUserID, ml.DEFAULT_DATE, true);  //get alerts
                DataRows = mm.GetMyAlerts(this.alertUserID, ml.DEFAULT_DATE, true);  //get alerts
            }
            else
            {
                DataRows = mm.GetAlertsToMe(this.alertUserID);
            }

            List<Hashtable> Row2Keep = new List<Hashtable>();
            //foreach (DataRow row in DataRows.Rows)
            //{
            //    DateTime deferredDate = (DateTime)Row["Deferred_Date"];
            //    DateTime dateSent = (DateTime)Row["date_sent"];
            //    if (deferredDate > dateSent)
            //        Row.Add("date_received", deferredDate);
            //    else
            //        Row.Add("date_received", dateSent);

            //    if ((string)Row["Urgent"] == "Yes")
            //    {
            //        if (!Row.ContainsKey("RowColor"))
            //            Row.Add("RowColor", System.Drawing.Color.Salmon);
            //        else
            //            Row["RowColor"] = System.Drawing.Color.Salmon;
            //        Row2Keep.Add(Row);
            //    }
            //}
            //if ((Row2Keep.Count > 0) && (this.AlertsToMe.IsChecked == true))
            //    DataRows = Row2Keep;
            //grMyAlerts.Items.Clear();
            //openClaim.IsEnabled = false;
            //openNotePad.IsEnabled = false;
            //ClearMyAlert.IsEnabled = false;
            //ClearMyAlert.Visibility = Visibility.Collapsed;
            //if (DataRows.Rows.Count > 0)
            //{   //load grid with Alerts 
                //this.grMyAlerts.ItemsSource = LoadAlerts(DataRows);
                //if (this.AlertsToMe.IsChecked == true)
                //{
                //    grMyAlerts.Columns[3].Header = "Sent From";
                //    Sent_From.Content = "Sent From:";
                //}
                //else
                //{
                //    this.grMyAlerts.Columns[3].Header = "Sent To";
                //    this.Sent_From.Content = "Sent To:";
                //}
                //openNotePad.IsEnabled = true;
                //openClaim.IsEnabled = true;
                //ClearMyAlert.IsEnabled = true;
                //ClearMyAlert.Visibility = Visibility.Visible;
                //grMyAlerts.UpdateLayout();
                //grMyAlerts.SelectedIndex = 0;
                this.UpdateCount(this.lblNoAlerts, DataRows.Rows.Count);
                Session["DefaultAlertTable"] = DataRows;
                gvAlerts.DataSource = DataRows;
                gvAlerts.DataBind();
            //}

            //grMyAlerts.Items.Refresh();
            return true;
        }

        private bool LoadTasks(bool ignoreDate)
        {
            //grMyTasks.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime TasksLastChanged = new User(this.taskUserID).MyTasksChanged.Add(RefreshPeriod);
            if ((this.myTasksLoaded > TasksLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myTasksLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyTasks(this.taskUserID);  //get diaries

            //grMyTasks.Items.Clear();
            //opentskClaim.IsEnabled = false;
            newTask.Enabled = false;
            clearTask.Enabled = false;
            if (DataRows.Rows.Count > 0)
            {
                gvTasks.DataSource = DataRows;
                gvTasks.DataBind();
                //opentskClaim.IsEnabled = true;
                newTask.Enabled = true;
                clearTask.Enabled = true;
                //gvTasks.SelectedIndex = 0;
            }
            
            this.UpdateCount(this.lblNoTasks, DataRows.Rows.Count);
            return true;
        }

        private bool LoadDemands(bool ignoreDate)
        {
            //grMyTasks.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime DemandsLastChanged = new User(this.demandUserID).MyTasksChanged.Add(RefreshPeriod);
            if ((this.myDemandsLoaded > DemandsLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myDemandsLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyDemands(this.demandUserID);  //get diaries

            //grMyTasks.Items.Clear();
            //opentskClaim.IsEnabled = false;
            //newTask.IsEnabled = false;
            //clearTask.IsEnabled = false;
            //if (DataRows.Rows.Count > 0)
            //{
            gvDemands.DataSource = DataRows;
            gvDemands.DataBind();
            //opentskClaim.IsEnabled = true;
            //newTask.IsEnabled = true;
            //clearTask.IsEnabled = true;
            //gvDemands.SelectedIndex = 0;
            //}

            this.UpdateCount(this.lblNoDemands, DataRows.Rows.Count);
            return true;
        }

        private bool LoadTtlLoss(bool ignoreDate)
        {
            //grMyTtlLosses.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime TtlLossesLastChanged = new User(this.totalLossUserID).MyTotalLossesChanged.Add(RefreshPeriod);
            if ((this.myTotalLossLoaded > TtlLossesLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myTotalLossLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyTtlLosses(this.totalLossUserID);  //get diaries

            //grMyTtlLosses.Items.Clear();
            //opentlRes.IsEnabled = false;
            double Days = 0;
            bool isLocked = false;
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow row in DataRows.Rows)
                {
                    //Decorate rows
                 
                    try
                    {   //get days between open date and today
                        TimeSpan myTS = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                        Days = myTS.TotalDays;
                    }
                    catch (Exception ex)
                    {
                        Days = 0;
                    }

                    Reserve res = new Reserve((int)row["Reserve_Id"]); //get associated reserve
                    if ((res.Claim.FirstLockId() != 0) || (res.FirstLockId != 0))
                        isLocked = true;
                    else
                        isLocked = false;

                    if (isLocked)
                    {  //set image for locked reserves
                        //if (row.ContainsKey("Image_Index"))
                        //{
                        //    if ((int)thisRow["Image_Index"] == ImageIndex_Pencil)
                        //        thisRow["Image_Index"] = ImageIndex_PencilLock;
                        //    else
                        //        thisRow["Image_Index"] = ImageIndex_Lock;
                        //}
                    }

                    //if (((string)thisRow["Reserving"] == "Average") && !res.isClosed && 
                    //    !res.isClosedPendingSalvage && !res.isClosedPendingSubro)
                    //{
                    //if (Days > MedCsXSystem.Settings.ReserveRequirementDays)
                    //{
                    //    if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))
                    //    {
                    //        if (((string)thisRow["Policy_No"]).Trim() != "")
                    //            thisRow.Add(RowColorHashtableKey, Colors.Salmon);
                    //        //item is overdue
                    //    }
                    //    else
                    //    {   //item is about to become overdue
                    //        thisRow.Add(RowColorHashtableKey, Colors.Yellow);
                    //    }
                    //}
                    //else
                    //{
                    //    if (Days > MedCsXSystem.Settings.ReserveWarningDays)
                    //        thisRow.Add(RowColorHashtableKey, Colors.Yellow);
                    //    //item is about to become overdue
                    //}
                    //}
=======
                if (overdue > 0)
                {
                    lblDairyWarning.Visible = true;
                    lblDairyWarning.Text = "You Have " + overdue + " Overdue Diaries.";
                    //btnShowDiaryAll.Enabled = false;
                }
                else
                {
                    lblDairyWarning.Visible = false;
                }
                //gvDiary.SelectedIndex = 0;
                this.UpdateCount(this.lblNoDiaries, DataRows.Rows.Count);
                //openDiary.IsEnabled = true;
            }


            this.UpdateCount(this.lblNoDiaries, DataRows.Rows.Count);
            gvDiary.DataSource = DataRows;
            gvDiary.DataBind();
            Session["DiaryTable"] = DataRows;

            //grMyDiaries.Items.Refresh();
            return true;
        }

       

        private bool LoadAlerts(bool ignoreDate, bool AlwaysDisplayAlerts = false)
        {   //pulls Alerts for user
            //grMyAlerts.ItemsSource = null;  //disassociate the array with the ItemsSource
            try
            {
                string thisAlertUser = ddlShowAlertsFor.SelectedValue.Trim();
                string[] a = thisAlertUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
           
                this.alertUserID = mm.GetReserveUserId(fName, lName);
            }
            catch (Exception ex)
            {
                this.alertUserID = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            }


            DataRows = new DataTable();

            // if (ddlShowAlertsFor.SelectedValue == ml.CurrentUser.Name)
            if (rblAlertType.SelectedValue == "1")
            {
                DateTime AlertsLastChanged = new User(this.alertUserID).MyAlertsChanged.Add(RefreshPeriod);
                if ((this.myAlertsLoaded > AlertsLastChanged) && ignoreDate)
                    return false;  //if alerts haven't changed, don't reload grid

                this.myAlertsLoaded = ml.DBNow();  //alerts loaded now
                //DataRows = mm.GetMyAlerts(this.alertUserID, ml.DEFAULT_DATE, true);  //get alerts
                DataRows = mm.GetMyAlerts(this.alertUserID, ml.DEFAULT_DATE, true);  //get alerts
            }
            else
            {
                DataRows = mm.GetAlertsToMe(this.alertUserID);
            }

            List<Hashtable> Row2Keep = new List<Hashtable>();
            //foreach (DataRow row in DataRows.Rows)
            //{
            //    DateTime deferredDate = (DateTime)Row["Deferred_Date"];
            //    DateTime dateSent = (DateTime)Row["date_sent"];
            //    if (deferredDate > dateSent)
            //        Row.Add("date_received", deferredDate);
            //    else
            //        Row.Add("date_received", dateSent);

            //    if ((string)Row["Urgent"] == "Yes")
            //    {
            //        if (!Row.ContainsKey("RowColor"))
            //            Row.Add("RowColor", System.Drawing.Color.Salmon);
            //        else
            //            Row["RowColor"] = System.Drawing.Color.Salmon;
            //        Row2Keep.Add(Row);
            //    }
            //}
            //if ((Row2Keep.Count > 0) && (this.AlertsToMe.IsChecked == true))
            //    DataRows = Row2Keep;
            //grMyAlerts.Items.Clear();
            //openClaim.IsEnabled = false;
            //openNotePad.IsEnabled = false;
            //ClearMyAlert.IsEnabled = false;
            //ClearMyAlert.Visibility = Visibility.Collapsed;
            //if (DataRows.Rows.Count > 0)
            //{   //load grid with Alerts 
                //this.grMyAlerts.ItemsSource = LoadAlerts(DataRows);
                //if (this.AlertsToMe.IsChecked == true)
                //{
                //    grMyAlerts.Columns[3].Header = "Sent From";
                //    Sent_From.Content = "Sent From:";
                //}
                //else
                //{
                //    this.grMyAlerts.Columns[3].Header = "Sent To";
                //    this.Sent_From.Content = "Sent To:";
                //}
                //openNotePad.IsEnabled = true;
                //openClaim.IsEnabled = true;
                //ClearMyAlert.IsEnabled = true;
                //ClearMyAlert.Visibility = Visibility.Visible;
                //grMyAlerts.UpdateLayout();
                //grMyAlerts.SelectedIndex = 0;
                this.UpdateCount(this.lblNoAlerts, DataRows.Rows.Count);
                Session["DefaultAlertTable"] = DataRows;
                gvAlerts.DataSource = DataRows;
                gvAlerts.DataBind();
            //}

            //grMyAlerts.Items.Refresh();
            return true;
        }

        private bool LoadTasks(bool ignoreDate)
        {
            //grMyTasks.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime TasksLastChanged = new User(this.taskUserID).MyTasksChanged.Add(RefreshPeriod);
            if ((this.myTasksLoaded > TasksLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myTasksLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyTasks(this.taskUserID);  //get diaries

            //grMyTasks.Items.Clear();
            //opentskClaim.IsEnabled = false;
            newTask.Enabled = false;
            clearTask.Enabled = false;
            if (DataRows.Rows.Count > 0)
            {
                gvTasks.DataSource = DataRows;
                gvTasks.DataBind();
                //opentskClaim.IsEnabled = true;
                newTask.Enabled = true;
                clearTask.Enabled = true;
                //gvTasks.SelectedIndex = 0;
            }
            
            this.UpdateCount(this.lblNoTasks, DataRows.Rows.Count);
            return true;
        }

        private bool LoadDemands(bool ignoreDate)
        {
            //grMyTasks.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime DemandsLastChanged = new User(this.demandUserID).MyTasksChanged.Add(RefreshPeriod);
            if ((this.myDemandsLoaded > DemandsLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myDemandsLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyDemands(this.demandUserID);  //get diaries

            //grMyTasks.Items.Clear();
            //opentskClaim.IsEnabled = false;
            //newTask.IsEnabled = false;
            //clearTask.IsEnabled = false;
            //if (DataRows.Rows.Count > 0)
            //{
            gvDemands.DataSource = DataRows;
            gvDemands.DataBind();
            //opentskClaim.IsEnabled = true;
            //newTask.IsEnabled = true;
            //clearTask.IsEnabled = true;
            //gvDemands.SelectedIndex = 0;
            //}

            this.UpdateCount(this.lblNoDemands, DataRows.Rows.Count);
            return true;
        }

        private bool LoadTtlLoss(bool ignoreDate)
        {
            //grMyTtlLosses.ItemsSource = null;  //disassociate the array with the ItemsSource

            DateTime TtlLossesLastChanged = new User(this.totalLossUserID).MyTotalLossesChanged.Add(RefreshPeriod);
            if ((this.myTotalLossLoaded > TtlLossesLastChanged) && ignoreDate)
                return false;  //if tasks haven't changed, don't reload grid

            this.myTotalLossLoaded = ml.DBNow();  //diaries loaded now
            DataRows = new DataTable();
            DataRows = mm.GetMyTtlLosses(this.totalLossUserID);  //get diaries

            //grMyTtlLosses.Items.Clear();
            //opentlRes.IsEnabled = false;
            double Days = 0;
            bool isLocked = false;
            if (DataRows.Rows.Count > 0)
            {
                foreach (DataRow row in DataRows.Rows)
                {
                    //Decorate rows
                 
                    try
                    {   //get days between open date and today
                        TimeSpan myTS = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                        Days = myTS.TotalDays;
                    }
                    catch (Exception ex)
                    {
                        Days = 0;
                    }

                    Reserve res = new Reserve((int)row["Reserve_Id"]); //get associated reserve
                    if ((res.Claim.FirstLockId() != 0) || (res.FirstLockId != 0))
                        isLocked = true;
                    else
                        isLocked = false;

                    if (isLocked)
                    {  //set image for locked reserves
                        //if (row.ContainsKey("Image_Index"))
                        //{
                        //    if ((int)thisRow["Image_Index"] == ImageIndex_Pencil)
                        //        thisRow["Image_Index"] = ImageIndex_PencilLock;
                        //    else
                        //        thisRow["Image_Index"] = ImageIndex_Lock;
                        //}
                    }

                    //if (((string)thisRow["Reserving"] == "Average") && !res.isClosed && 
                    //    !res.isClosedPendingSalvage && !res.isClosedPendingSubro)
                    //{
                    //if (Days > MedCsXSystem.Settings.ReserveRequirementDays)
                    //{
                    //    if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))
                    //    {
                    //        if (((string)thisRow["Policy_No"]).Trim() != "")
                    //            thisRow.Add(RowColorHashtableKey, Colors.Salmon);
                    //        //item is overdue
                    //    }
                    //    else
                    //    {   //item is about to become overdue
                    //        thisRow.Add(RowColorHashtableKey, Colors.Yellow);
                    //    }
                    //}
                    //else
                    //{
                    //    if (Days > MedCsXSystem.Settings.ReserveWarningDays)
                    //        thisRow.Add(RowColorHashtableKey, Colors.Yellow);
                    //    //item is about to become overdue
                    //}
                    //}
                }

                this.gvLoses.DataSource = DataRows;
                this.gvLoses.DataBind();
                //opentlRes.IsEnabled = true;
                //this.gvLoses.SelectedIndex = 0;
            }
            //grMyTtlLosses.Items.Refresh();
            this.UpdateCount(this.lblNoLoses, DataRows.Rows.Count);
            return true;
        }

        private bool LoadRecon(int All_or_PD, object updateCountObject)
        {
            try
            {
                int currQtr = Functions.GetQuarter(DateTime.Now.Month);
                DateTime qtrDate;
                switch (currQtr)
                {
                    case 2:
                       
                        qtrDate = new DateTime(DateTime.Now.Year, 4, 1);
                        break;
                    case 3:
                        qtrDate = new DateTime(DateTime.Now.Year, 7, 1);
                        break;
                    case 4:
                        qtrDate = new DateTime(DateTime.Now.Year, 10, 1);
                        break;
                    default: //case 1:
                        qtrDate = new DateTime(DateTime.Now.Year, 1, 1);
                        break;
                }
              
                
                DataRows = mm.GetMyRecon(qtrDate, All_or_PD);

                //format values into currency format
                double Days = 0;
                bool isLocked = false;
                //foreach (DataRow row in DataRows.Rows)
                //{
                //    row["Net_Loss_Reserve"] = ((double)((decimal)row["Net_Loss_Reserve"])).ToString("c");
                //    try
                //    {       //get the difference between today and date reported
                //        TimeSpan myTS = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                //        Days = myTS.TotalDays;
                //    }
                //    catch (Exception ex)
                //    {
                //        Days = 0;
                //    }

                //    //is claim locked?
                //    if (!(bool)row["Reserve_Locked"] || !(bool)row["Claim_Locked"])
                //        isLocked = true;
                //    else
                //        isLocked = false;

                //    if (isLocked)
                //    {
                //        if ((int)row["Image_Index"] == ImageIndex_Pencil)
                //            row["Image_Index"] = ImageIndex_PencilLock;
                //        else
                //            row["Image_Index"] = ImageIndex_Lock;
                //    }
                //}

                this.gvRecon.DataSource = DataRows;
                gvRecon.DataBind();
                //gvRecon.SelectedIndex = 0;
                //grMyRecon.Items.Refresh();
                this.UpdateCount(updateCountObject, this.DataRows.Rows.Count);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void LoadUsersComboBoxes()
        {
            this.LoadedUserComboBoxes = false;

            //add active users to combo boxes
            ddlShowAlertsFor.DataSource = activeUsers();
            ddlShowAlertsFor.DataBind();
            //if (this.ddlShowReservesFor.DataSource != null)
            //{ 
            ddlShowReservesFor.DataSource = activeUsers();
            ddlShowReservesFor.DataBind();
            //}
            ddlShowDiaryFor.DataSource = activeUsers();
            ddlShowDiaryFor.DataBind();
            ddlShowDemandsFor.DataSource = activeUsers();
            ddlShowDemandsFor.DataBind();
            ddlShowTasksFor.DataSource = activeUsers();
            ddlShowTasksFor.DataBind();
            ddlShowLosesFor.DataSource = activeUsers();
            ddlShowLosesFor.DataBind();
            ddlShowSalvageFor.DataSource = activeUsers();
            ddlShowSalvageFor.DataBind();

            this.LoadedUserComboBoxes = true;

            //select the current user
            this.ddlShowAlertsFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowReservesFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowDemandsFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowDiaryFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowTasksFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowSalvageFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowLosesFor.SelectedValue = ml.CurrentUser.Name;
            //this.cboMyDiaryUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMyTasksUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMyTtlLossUsers.SelectedItem = ml.CurrentUser.Name;
            //if (this.cboMyReserveUsers != null)
            //this.cboMyReserveUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMySalvageUsers.SelectedItem = ml.CurrentUser.Name;
        }

        private string[] activeUsers()
        {   //populates list for combo boxes of users
            List<Hashtable> ActiveUsers = (List<Hashtable>)mm.GetActiveUsers();
            string[] cbo_Data;
            string users = "";
            int count = 0;
            foreach (Hashtable user in ActiveUsers)
            {
                if ((string)user["Name"] != string.Empty)
                {
                    if (count > 0)
                        users += ",";
                    users += ((string)user["Name"]).Trim();
                    count++;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
                }
            }
            cbo_Data = users.Split(',');
            return cbo_Data;
        }

<<<<<<< HEAD
                this.gvLoses.DataSource = DataRows;
                this.gvLoses.DataBind();
                //opentlRes.IsEnabled = true;
                //this.gvLoses.SelectedIndex = 0;
            }
            //grMyTtlLosses.Items.Refresh();
            this.UpdateCount(this.lblNoLoses, DataRows.Rows.Count);
            return true;
        }

        private bool LoadRecon(int All_or_PD, object updateCountObject)
        {
            try
            {
                int currQtr = Functions.GetQuarter(DateTime.Now.Month);
                DateTime qtrDate;
                switch (currQtr)
                {
                    case 2:
                       
                        qtrDate = new DateTime(DateTime.Now.Year, 4, 1);
                        break;
                    case 3:
                        qtrDate = new DateTime(DateTime.Now.Year, 7, 1);
                        break;
                    case 4:
                        qtrDate = new DateTime(DateTime.Now.Year, 10, 1);
                        break;
                    default: //case 1:
                        qtrDate = new DateTime(DateTime.Now.Year, 1, 1);
                        break;
                }
              
                
                DataRows = mm.GetMyRecon(qtrDate, All_or_PD);

                //format values into currency format
                double Days = 0;
                bool isLocked = false;
                //foreach (DataRow row in DataRows.Rows)
                //{
                //    row["Net_Loss_Reserve"] = ((double)((decimal)row["Net_Loss_Reserve"])).ToString("c");
                //    try
                //    {       //get the difference between today and date reported
                //        TimeSpan myTS = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                //        Days = myTS.TotalDays;
                //    }
                //    catch (Exception ex)
                //    {
                //        Days = 0;
                //    }

                //    //is claim locked?
                //    if (!(bool)row["Reserve_Locked"] || !(bool)row["Claim_Locked"])
                //        isLocked = true;
                //    else
                //        isLocked = false;

                //    if (isLocked)
                //    {
                //        if ((int)row["Image_Index"] == ImageIndex_Pencil)
                //            row["Image_Index"] = ImageIndex_PencilLock;
                //        else
                //            row["Image_Index"] = ImageIndex_Lock;
                //    }
                //}

                this.gvRecon.DataSource = DataRows;
                gvRecon.DataBind();
                //gvRecon.SelectedIndex = 0;
                //grMyRecon.Items.Refresh();
                this.UpdateCount(updateCountObject, this.DataRows.Rows.Count);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void LoadUsersComboBoxes()
        {
            this.LoadedUserComboBoxes = false;

            //add active users to combo boxes
            ddlShowAlertsFor.DataSource = activeUsers();
            ddlShowAlertsFor.DataBind();
            //if (this.ddlShowReservesFor.DataSource != null)
            //{ 
            ddlShowReservesFor.DataSource = activeUsers();
            ddlShowReservesFor.DataBind();
            //}
            ddlShowDiaryFor.DataSource = activeUsers();
            ddlShowDiaryFor.DataBind();
            ddlShowDemandsFor.DataSource = activeUsers();
            ddlShowDemandsFor.DataBind();
            ddlShowTasksFor.DataSource = activeUsers();
            ddlShowTasksFor.DataBind();
            ddlShowLosesFor.DataSource = activeUsers();
            ddlShowLosesFor.DataBind();
            ddlShowSalvageFor.DataSource = activeUsers();
            ddlShowSalvageFor.DataBind();

            this.LoadedUserComboBoxes = true;

            //select the current user
            this.ddlShowAlertsFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowReservesFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowDemandsFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowDiaryFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowTasksFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowSalvageFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowLosesFor.SelectedValue = ml.CurrentUser.Name;
            //this.cboMyDiaryUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMyTasksUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMyTtlLossUsers.SelectedItem = ml.CurrentUser.Name;
            //if (this.cboMyReserveUsers != null)
            //this.cboMyReserveUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMySalvageUsers.SelectedItem = ml.CurrentUser.Name;
        }

        private string[] activeUsers()
        {   //populates list for combo boxes of users
            List<Hashtable> ActiveUsers = (List<Hashtable>)mm.GetActiveUsers();
            string[] cbo_Data;
            string users = "";
            int count = 0;
            foreach (Hashtable user in ActiveUsers)
            {
                if ((string)user["Name"] != string.Empty)
                {
                    if (count > 0)
                        users += ",";
                    users += ((string)user["Name"]).Trim();
                    count++;
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }
            }
            cbo_Data = users.Split(',');
            return cbo_Data;
        }

<<<<<<< HEAD
                this.gvLoses.DataSource = DataRows;
                this.gvLoses.DataBind();
                //opentlRes.IsEnabled = true;
                //this.gvLoses.SelectedIndex = 0;
            }
            //grMyTtlLosses.Items.Refresh();
            this.UpdateCount(this.lblNoLoses, DataRows.Rows.Count);
            return true;
        }

        private bool LoadRecon(int All_or_PD, object updateCountObject)
        {
            try
            {
                int currQtr = Functions.GetQuarter(DateTime.Now.Month);
                DateTime qtrDate;
                switch (currQtr)
                {
                    case 2:
                       
                        qtrDate = new DateTime(DateTime.Now.Year, 4, 1);
                        break;
                    case 3:
                        qtrDate = new DateTime(DateTime.Now.Year, 7, 1);
                        break;
                    case 4:
                        qtrDate = new DateTime(DateTime.Now.Year, 10, 1);
                        break;
                    default: //case 1:
                        qtrDate = new DateTime(DateTime.Now.Year, 1, 1);
                        break;
                }
              
                
                DataRows = mm.GetMyRecon(qtrDate, All_or_PD);

                //format values into currency format
                double Days = 0;
                bool isLocked = false;
                //foreach (DataRow row in DataRows.Rows)
                //{
                //    row["Net_Loss_Reserve"] = ((double)((decimal)row["Net_Loss_Reserve"])).ToString("c");
                //    try
                //    {       //get the difference between today and date reported
                //        TimeSpan myTS = DateTime.Today.Subtract((DateTime)row["Date_Open"]);
                //        Days = myTS.TotalDays;
                //    }
                //    catch (Exception ex)
                //    {
                //        Days = 0;
                //    }

                //    //is claim locked?
                //    if (!(bool)row["Reserve_Locked"] || !(bool)row["Claim_Locked"])
                //        isLocked = true;
                //    else
                //        isLocked = false;

                //    if (isLocked)
                //    {
                //        if ((int)row["Image_Index"] == ImageIndex_Pencil)
                //            row["Image_Index"] = ImageIndex_PencilLock;
                //        else
                //            row["Image_Index"] = ImageIndex_Lock;
                //    }
                //}

                this.gvRecon.DataSource = DataRows;
                gvRecon.DataBind();
                //gvRecon.SelectedIndex = 0;
                //grMyRecon.Items.Refresh();
                this.UpdateCount(updateCountObject, this.DataRows.Rows.Count);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void LoadUsersComboBoxes()
        {
            this.LoadedUserComboBoxes = false;

            //add active users to combo boxes
            ddlShowAlertsFor.DataSource = activeUsers();
            ddlShowAlertsFor.DataBind();
            //if (this.ddlShowReservesFor.DataSource != null)
            //{ 
            ddlShowReservesFor.DataSource = activeUsers();
            ddlShowReservesFor.DataBind();
            //}
            ddlShowDiaryFor.DataSource = activeUsers();
            ddlShowDiaryFor.DataBind();
            ddlShowDemandsFor.DataSource = activeUsers();
            ddlShowDemandsFor.DataBind();
            ddlShowTasksFor.DataSource = activeUsers();
            ddlShowTasksFor.DataBind();
            ddlShowLosesFor.DataSource = activeUsers();
            ddlShowLosesFor.DataBind();
            ddlShowSalvageFor.DataSource = activeUsers();
            ddlShowSalvageFor.DataBind();

            this.LoadedUserComboBoxes = true;

            //select the current user
            this.ddlShowAlertsFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowReservesFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowDemandsFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowDiaryFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowTasksFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowSalvageFor.SelectedValue = ml.CurrentUser.Name;
            this.ddlShowLosesFor.SelectedValue = ml.CurrentUser.Name;
            //this.cboMyDiaryUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMyTasksUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMyTtlLossUsers.SelectedItem = ml.CurrentUser.Name;
            //if (this.cboMyReserveUsers != null)
            //this.cboMyReserveUsers.SelectedItem = ml.CurrentUser.Name;
            //this.cboMySalvageUsers.SelectedItem = ml.CurrentUser.Name;
        }

        private string[] activeUsers()
        {   //populates list for combo boxes of users
            List<Hashtable> ActiveUsers = (List<Hashtable>)mm.GetActiveUsers();
            string[] cbo_Data;
            string users = "";
            int count = 0;
            foreach (Hashtable user in ActiveUsers)
            {
                if ((string)user["Name"] != string.Empty)
                {
                    if (count > 0)
                        users += ",";
                    users += ((string)user["Name"]).Trim();
                    count++;
                }
            }
            cbo_Data = users.Split(',');
            return cbo_Data;
        }



=======


=======


>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        private void UpdateCount(object myItem, int count)
        {
            //take old count off of the item lable
            //string str = (string)((Label)myItem).Content;
            //if (str == null)
            string str = "";
            //int pos = str.IndexOf('(');

            //if (pos > 2)
            //{
            //    str = str.Substring(0, pos - 2);
            //    str = str.Trim();
            //}

            //put count in label
            str += " (" + count.ToString() + ")";
            ((Label)myItem).Text = str;
        }

        protected void rblAlertType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlShowAlertsFor.SelectedValue != null)
            {   //pulls user info
                string thisAlertUser = ddlShowAlertsFor.SelectedValue.Trim();
                string[] a = thisAlertUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.alertUserID = mm.GetReserveUserId(fName, lName);


                if (rblAlertType.SelectedValue == "1")
                {
                    LoadAlerts(false);
                }
                else
                {


                    bool alertsLoaded = false;
                    alertsLoaded = LoadAlerts(false);
                }
            }
            //if (cboMyAlertsUsers != null)
            //{
            //    if (cboMyAlertsUsers.Text != null)
            //alertsLoaded = LoadAlerts(false);
            //}
            //DataTable dtAlert = new DataTable();
            //Alert myAlert = new Alert();


            //if (rblAlertType.SelectedValue == "1")
            //{
            //    dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 1);  // to me
            //}
            //else
            //{
            //    dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 2); //from me
            //}

            //gvAlerts.DataSource = dtAlert;
            //gvAlerts.DataBind();
            //gvAlerts.Visible = true;

            //Session["AlertTable"] = dtAlert;
        }


        protected void rblReconType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable();
            //Recon myData = new Recon();

            //var dateAndTime  = DateTime.Now.AddDays(-30);
            //var timeInterval = dateAndTime.Date;
            if (rblReconType.SelectedValue == "0")  //all
            {
                bool reconLoaded = LoadRecon(0, this.lblAllRecon);
            }
            else
            {
                bool reconLoaded = LoadRecon(1, this.lblPDRecon);

            }

            //gvRecon.DataSource = dt;
            //gvRecon.DataBind();
            //gvRecon.Visible = true;

            //Session["ReconTable"] = dt;
        }

      

        protected void ddlShowAlertsFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool sameUser = false;
            string origUser = ddlShowAlertsFor.SelectedValue;
            string thisAlertUser = ddlShowAlertsFor.SelectedValue as string;
            if (!firstActivated && (thisAlertUser == null))
            {   //stop process if window is just opening
                ddlShowAlertsFor.Text = origUser;
                return;
            }
            if (ddlShowAlertsFor.Text == thisAlertUser)
                sameUser = true;        //name has not changed
            if (thisAlertUser != null)
            {   //pulls user info
                thisAlertUser = thisAlertUser.Trim();
                string[] a = thisAlertUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.alertUserID = mm.GetReserveUserId(fName, lName);
            }
            bool alertsLoaded = LoadAlerts(sameUser);
            //DataTable dt = new DataTable();
            //Search myData = new Search();
            //dt = myData.SearchClaimByRecentPending(Convert.ToInt32(rblRecentPending.SelectedValue));
            //gvSearch.DataSource = dt;
            //gvSearch.DataBind();
            //gvSearch.Visible = true;
        }

        protected void rblRecentPending_SelectedIndexChanged(object sender, EventArgs e)
        {
           
               
                bool claimsLoaded = LoadClaims(0, rblRecentPending.SelectedValue);
         
        }

        protected void ddlShowReservesFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool sameUser = false;
            string origUser = ddlShowReservesFor.SelectedValue;
            string thisReserveUser = ddlShowReservesFor.SelectedValue as string;
            if (!firstActivated && (thisReserveUser == null))
            {   //stop process if window is just opening
                ddlShowAlertsFor.Text = origUser;
                return;
            }
            if (ddlShowAlertsFor.Text == thisReserveUser)
                sameUser = true;        //name has not changed
            if (thisReserveUser != null)
            {   //pulls user info
                thisReserveUser = thisReserveUser.Trim();
                string[] a = thisReserveUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.reserveUserID = mm.GetReserveUserId(fName, lName);
            }
            bool reservesLoaded = LoadReserves(sameUser);
            //DataTable dtReserves = new DataTable();
            //Reserve myReserves = new Reserve();

            //dtReserves = myReserves.GetReserves(Convert.ToInt32(ddlShowReservesFor.SelectedValue));  
<<<<<<< HEAD

            //gvReserves.DataSource = dtReserves;
            //gvReserves.DataBind();
            //gvReserves.Visible = true;

=======

            //gvReserves.DataSource = dtReserves;
            //gvReserves.DataBind();
            //gvReserves.Visible = true;

>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //Session["ReservesTable"] = dtReserves;
        }

        protected void ddlShowDiaryFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDairyWarning.Visible = false;

            bool sameUser = false;
            string origUser = ddlShowDiaryFor.SelectedValue;
            string thisDiaryUser = ddlShowDiaryFor.SelectedValue as string;
            if (!firstActivated && (thisDiaryUser == null))
            {   //stop process if window is just opening
                ddlShowAlertsFor.Text = origUser;
                return;
            }
            if (ddlShowDiaryFor.Text == thisDiaryUser)
                sameUser = true;        //name has not changed
            if (thisDiaryUser != null)
            {   //pulls user info
                thisDiaryUser = thisDiaryUser.Trim();
                string[] a = thisDiaryUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.diaryUserID = mm.GetReserveUserId(fName, lName);
            }
            bool DiaryLoaded = LoadDiaries(sameUser);

            //DataTable dtDiary = new DataTable();
            //Diary myDiary = new Diary();
<<<<<<< HEAD

            //dtDiary = myDiary.GetDiary(Convert.ToInt32(ddlShowDiaryFor.SelectedValue)); 

=======

            //dtDiary = myDiary.GetDiary(Convert.ToInt32(ddlShowDiaryFor.SelectedValue)); 

>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //gvDiary.DataSource = dtDiary;
            //gvDiary.DataBind();
            //gvDiary.Visible = true;

            //Session["DiaryTable"] = dtDiary;
        }

        protected void ddlShowTasksFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool sameUser = false;
            string origUser = ddlShowTasksFor.SelectedValue;
            string thisUser = ddlShowTasksFor.SelectedValue as string;
            if (ddlShowTasksFor.Text == thisUser)
                sameUser = true;
            if (!firstActivated && (thisUser == null))
            {
                ddlShowTasksFor.Text = origUser;
                return;
            }
            if (thisUser != null)
            {
                thisUser = thisUser.Trim();
                string[] a = thisUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.taskUserID = mm.GetReserveUserId(fName, lName);
            }
            bool TasksLoaded = LoadTasks(sameUser);
            //DataTable dt = new DataTable();
            //Task myTask = new Task();
<<<<<<< HEAD

            //dt = myTask.GetTask(Convert.ToInt32(ddlShowTasksFor.SelectedValue));  


            //gvTasks.DataSource = dt;
            //gvTasks.DataBind();
            //gvTasks.Visible = true;

=======

            //dt = myTask.GetTask(Convert.ToInt32(ddlShowTasksFor.SelectedValue));  


            //gvTasks.DataSource = dt;
            //gvTasks.DataBind();
            //gvTasks.Visible = true;

>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //Session["TasksTable"] = dt;
        }

        protected void ddlShowLosesFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool sameUser = false;
            string origUser = ddlShowLosesFor.Text;
            string thisUser = ddlShowLosesFor.SelectedValue as string;
            if (ddlShowLosesFor.Text == thisUser)
                sameUser = true;
            if (!firstActivated && (thisUser == null))
            {
                ddlShowLosesFor.Text = origUser;
                return;
            }
            if (thisUser != null)
            {
                thisUser = thisUser.Trim();
                string[] a = thisUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.totalLossUserID = mm.GetReserveUserId(fName, lName);
            }
            bool TLLoaded = LoadTtlLoss(sameUser);
            //DataTable dt = new DataTable();
            //Loses myData = new Loses();

            //dt = myData.GetLoses(Convert.ToInt32(ddlShowLosesFor.SelectedValue));


            //gvLoses.DataSource = dt;
            //gvLoses.DataBind();
            //gvLoses.Visible = true;

            //Session["LosesTable"] = dt;
        }

        protected void ddlShowSalvageFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable();
            //Salvage mySalvage = new Salvage();

            //dt = mySalvage.GetSalvage(Convert.ToInt32(ddlShowSalvageFor.SelectedValue)); 
<<<<<<< HEAD

            //gvSalvage.DataSource = dt;
            //gvSalvage.DataBind();
            //gvSalvage.Visible = true;

=======

            //gvSalvage.DataSource = dt;
            //gvSalvage.DataBind();
            //gvSalvage.Visible = true;

>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //Session["SalvageTable"] = dt;
        }

        protected void ddlShowDemandsFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool sameUser = false;
            string origUser = ddlShowDemandsFor.SelectedValue;
            string thisDemandUser = ddlShowDemandsFor.SelectedValue as string;
            if (!firstActivated && (thisDemandUser == null))
            {   //stop process if window is just opening
                ddlShowAlertsFor.Text = origUser;
                return;
            }
            if (ddlShowAlertsFor.Text == thisDemandUser)
                sameUser = true;        //name has not changed
            if (thisDemandUser != null)
            {   //pulls user info
                thisDemandUser = thisDemandUser.Trim();
                string[] a = thisDemandUser.Split(' ');
                string fName = a[0];
                string lName = "";
                if (a.Length > 2)
                    lName = a[1] + " " + a[2];
                else
                    lName = a[1];
                this.demandUserID = mm.GetReserveUserId(fName, lName);
            }
            bool DemandsLoaded = LoadDemands(sameUser);
            //DataTable dt = new DataTable();
            //Demands myData = new Demands();

            //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));

            //gvDemands.DataSource = dt;
            //gvDemands.DataBind();
            //gvDemands.Visible = true;

            //Session["DemandsTable"] = dt;
        }


        #region paging
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Search myData = new Search();

            //switch (Convert.ToInt32(rblSearchType.SelectedValue))
            //{
            //    case 1:
            //        dt = myData.SearchClaimByRecentPending(Convert.ToInt32(rblRecentPending.SelectedValue));
            //        break;
            //    case 2:  //claim #
            //        dt = myData.SearchClaimByClaimNo(txtSearch.Text);
            //        break;
            //    case 3: //date of loss
            //        dt = myData.SearchClaimByDateOfLoss(txtSearch.Text);
            //        break;
            //    case 4: //phone number
            //        dt = myData.SearchClaimByPhoneNo(txtSearch.Text);
            //        break;
            //    case 5: //all pending
            //        dt = myData.SearchClaimByAllPending();
            //        break;
            //    case 6: //policy number
            //        dt = myData.SearchClaimByPolicyNo(txtSearch.Text);
            //        break;
            //    case 7: // all locked
            //        dt = myData.SearchClaimByAllLockedPending();
            //        break;
            //    case 8:
            //        dt = myData.SearchClaimByName(txtFirstName.Text, txtLastName.Text);
            //        break;
            //    case 9: //draft no
            //        dt = myData.SearchClaimByDraftNo(txtSearch.Text);
            //        break;
            //    case 10:
            //        dt = myData.SearchClaimByVehicle(txtYear.Text, txtMake.Text, txtModel.Text, txtVIN.Text);
            //        break;
            //    default:
            //        break;
            //}
            //gvSearch.DataSource = dt;

            //gvSearch.PageIndex = e.NewPageIndex;
            //DataBind();

        }
        protected void gvDemands_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt= new DataTable();

            //Demands myData = new Demands();
            //dt = myData.GetDemands(Convert.ToInt32(ddlShowDemandsFor.SelectedValue));
            //gvDemands.DataSource = dt;

            //gvDemands.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvAlerts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            LoadAlerts(false);
            ////DataTable dtAlert = new DataTable();

            ////Alert myAlert = new Alert();
            ////dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 1);
            ////gvAlerts.DataSource = dtAlert;

            gvAlerts.PageIndex = e.NewPageIndex;
            ////DataBind();
        }

       

        protected void gvDiary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            LoadDiaries(false);
            //DataTable dtDiary = new DataTable();

            //Diary myDiary = new Diary();
            //dtDiary = myDiary.GetDiary(Convert.ToInt32(ddlShowDiaryFor.SelectedValue));
            //gvDiary.DataSource = dtDiary;

            gvDiary.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void gvTasks_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();
            //Task myData = new Task();
            //dt = myData.GetTask(Convert.ToInt32(ddlShowTasksFor.SelectedValue));
            //gvTasks.DataSource = dt;

            //gvTasks.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void gvLoses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();
            //Loses myData = new Loses();
            //dt = myData.GetLoses(Convert.ToInt32(ddlShowLosesFor.SelectedValue));
            //gvLoses.DataSource = dt;

            //gvLoses.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void gvRecon_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();
            //Recon myData = new Recon();

            //var dateAndTime = DateTime.Now.AddDays(-30);
            //var timeInterval = dateAndTime.Date;
            //if (rblReconType.SelectedValue == "0")
            //{
            //    dt = myData.GetRecon(timeInterval, 0);  // all
            //}
            //else
            //{

            //    dt = myData.GetRecon(timeInterval, 1);//pd
            //}


            //gvRecon.DataSource = dt;

            //gvRecon.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void gvSalvage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();
            //Salvage myData = new Salvage();
            //dt = myData.GetSalvage(Convert.ToInt32(ddlShowSalvageFor.SelectedValue));
            //gvSalvage.DataSource = dt;

            //gvSalvage.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        #endregion

        #region sorting
        protected void gvSearch_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["SearchTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvSearch.DataSource = dt;
                gvSearch.DataBind();

                Session["SearchTable"] = dt;
            }

        }
        protected void gvDemands_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["DemandsTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvDemands.DataSource = Session["DemandsTable"];
                gvDemands.DataBind();

                Session["DemandsTable"] = dt;
            }

        }

       


        protected void gvReserves_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["ReservesTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvReserves.DataSource = Session["ReservesTable"];
                gvReserves.DataBind();

                Session["ReservesTable"] = dt;
            }

        }

        protected void gvDiary_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["DiaryTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvDiary.DataSource = Session["DiaryTable"];
                gvDiary.DataBind();

                Session["DiaryTable"] = dt;
            }

        }

        protected void gvTasks_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["TasksTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvTasks.DataSource = Session["TasksTable"];
                gvTasks.DataBind();

                Session["TasksTable"] = dt;
            }

        }

        protected void gvLoses_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["LosesTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvLoses.DataSource = Session["LosesTable"];
                gvLoses.DataBind();

                Session["LosesTable"] = dt;
            }

        }

        protected void gvRecon_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["ReconTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvRecon.DataSource = Session["ReconTable"];
                gvRecon.DataBind();

                Session["ReconTable"] = dt;
            }

        }

        protected void gvSalvage_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["SalvageTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvSalvage.DataSource = Session["SalvageTable"];
                gvSalvage.DataBind();

                Session["SalvageTable"] = dt;
            }

        }
        #endregion

        protected void btnShowDiaryAll_Click(object sender, EventArgs e)
        {
            //if (DiaryMode == 1)
            //{
                DiaryMode = 0;
                LoadDiaries(false);
            //}
        }

        protected void btnDiaryPastDue_Click(object sender, EventArgs e)
        {
            //if (DiaryMode != 1)
            //{
                DiaryMode = 1;
                LoadDiaries(false);
            //}
        }

        protected void btnSearchName_Click(object sender, EventArgs e)
        {
          
                this.srchCriteria = 2;
            

            //this.srchCriteria = 7;
            bool claimsLoaded = LoadClaims(0, this.txtFirstName.Text.Trim(), this.txtLastName.Text.Trim());
            txtSearch.Focus();
        }

        protected void btnSearch3_Click(object sender, EventArgs e)
        {
            this.srchCriteria = 4;


            //this.srchCriteria = 7;
            bool claimsLoaded = LoadClaims(0, this.txtYear.Text.Trim(), this.txtMake.Text.Trim(),this.txtModel.Text.Trim(), this.txtVIN.Text.Trim());
            txtSearch.Focus();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            btnSearch.Focus();
        }

        protected void gvReserves_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            double Days = 0;
            bool isLocked = false;
            int OverdueItems = 0;
            int WarningItems = 0;
            GridViewRow grv = e.Row;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try  //days open will determine the color of the row
                {
                    TimeSpan myTimeSpan = DateTime.Today.Subtract(Convert.ToDateTime(grv.Cells[3].Text));
                    Days = myTimeSpan.TotalDays;
                }
                catch (Exception ex)
                {
                    Days = 0;
                }
                isLocked = Convert.ToBoolean(grv.Cells[11].Text);
                //if (grv.Cells[11].Text.Trim() == "Reserve_Locked")
                //    isLocked = true;

                if (isLocked)  //set image for loced claims
                {
                     e.Row.Cells[11].BackColor = System.Drawing.Color.Red;
                }

                //if (grv.Cells[8].Text != "")
                //{
                //    e.Row.BackColor = System.Drawing.Color.Salmon;
                //}


                if ((grv.Cells[6].Text.Trim() == "Average") && (grv.Cells[5].Text != "Reserve Closed") &&
                    (grv.Cells[11].Text == "False") && (grv.Cells[12].Text == "False"))
                {
                    if (Days > MedCsXSystem.Settings.ReserveRequirementDays)  //reserve is older than required number of days
                    {
                        if (!isLocked || (isLocked && MedCsXSystem.Settings.ReservesRequiredOnLockedFiles))  //reserve is not locked OR it is locked and requires reserves
                        {
                            //if (row["Policy_No"] != null)
                            //{
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                            if (grv.Cells[8].Text != "")  //claim must have policy # before it is considered overdue
                            {
                                //e.Row.BackColor = System.Drawing.Color.Salmon;
                                e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);  //pumpkin
                                //e.Row.BackColor = System.Drawing.Color.FromArgb(158, 108, 105);   //rust
                                //e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);  //orange
                                e.Row.ForeColor = System.Drawing.Color.White;
                                HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");
                                arEdit.ForeColor = System.Drawing.Color.White;
                                //OverdueItems++;

                            }
<<<<<<< HEAD
=======
=======
                                if (grv.Cells[8].Text != "")  //claim must have policy # before it is considered overdue
                                {
                                e.Row.BackColor = System.Drawing.Color.Salmon;
                                     //OverdueItems++;
                                  
                                }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                            //}
                        }
                        else
                        {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                            e.Row.BackColor = System.Drawing.Color.FromArgb(210, 181, 91); //mustard
                            //e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);  //rust
                            //e.Row.BackColor = System.Drawing.Color.FromArgb(210, 170, 108);  //old gold

                            WarningItems++;

<<<<<<< HEAD
=======
=======
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                            WarningItems++;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        }
                    }
                    else
                    {
                        if (Days > MedCsXSystem.Settings.ReserveWarningDays)  //reserve is older than required warning days
                        {
<<<<<<< HEAD
                            e.Row.BackColor = System.Drawing.Color.FromArgb(210, 181, 91); //mustard
                            HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");

=======
<<<<<<< HEAD
                            e.Row.BackColor = System.Drawing.Color.FromArgb(210, 181, 91); //mustard
                            HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");

=======
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                            WarningItems++;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        }
                    }
                }
            }
        }

        protected void gvAlerts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow grv = e.Row;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (grv.Cells[2].Text == "Yes")
                {
<<<<<<< HEAD
                    e.Row.BackColor = System.Drawing.Color.FromArgb(158, 108, 105);   //rust
=======
<<<<<<< HEAD
                    e.Row.BackColor = System.Drawing.Color.FromArgb(158, 108, 105);   //rust
=======
                    e.Row.BackColor = System.Drawing.Color.Salmon;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    e.Row.ForeColor = System.Drawing.Color.White;
                    HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");
                    arEdit.ForeColor = System.Drawing.Color.White;
                }
               

                if (grv.Cells[4].Text == "1/1/1900 12:00:00 AM")
                {
                    grv.Cells[4].Text = "";
                }

                if (grv.Cells[5].Text == "1/1/1900 12:00:00 AM")
                {
                    grv.Cells[5].Text = "";
                }


            }


           
        }

       

        protected void btnMyDiaryRpt_Click(object sender, EventArgs e)
        {
          
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsMyDiary();

                ShowReport("All Diary Entries for User", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnPastDueRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsMyDiary();

                ShowReport("Past Due Diary Entries For User", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
           
           
        }

        protected void gvAlerts_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["DefaultAlertTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvAlerts.DataSource = dt;
                gvAlerts.DataBind();

                Session["DefaultAlertTable"] = dt;
            }
        }

        protected void ClearMyAlert_Click(object sender, EventArgs e)
        {
            bool alertmessage = false;
            int aid = 0;

            string thisAlertUser = ddlShowAlertsFor.SelectedValue.Trim();
            string[] a = thisAlertUser.Split(' ');
            string fName = a[0];
            string lName = "";
            if (a.Length > 2)
                lName = a[1] + " " + a[2];
            else
                lName = a[1];
            this.alertUserID = mm.GetReserveUserId(fName, lName);

            if ((Convert.ToInt32(alertUserID) == Convert.ToInt32(Session["userID"])) && (rblAlertType.SelectedValue == "1"))
            { 
                for (int i = 0; i < gvAlerts.Rows.Count; i++)
                {
                    GridViewRow row = gvAlerts.Rows[i];
                    bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                    if (isChecked)
                    {
                        if (rblAlertType.SelectedValue == "1")
                        {
                            aid = Convert.ToInt32(gvAlerts.DataKeys[i].Value);
                            Alert thisAlert = new Alert(aid);
                            //if (!(thisAlert.isUrgent && !thisAlert.hasRepliedToAlert))
                                thisAlert.ClearFromMainScreen();  //clear alert that is not urgent and has a reply

                       
                        }
                        else
                        {
                            alertmessage = true;
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You cannot clear other user's alerts.');</script>");
                            //MessageBox.Show("You cannot clear other user's alerts.", "Cannot Clear Alerts", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        }
                    }
                }
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You cannot clear other user's alerts.');</script>");
                
            }
           
<<<<<<< HEAD

            

=======
<<<<<<< HEAD

            

=======

            

>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            LoadAlerts(false);

            DataTable dt = mm.GetMyUrgentAlerts(Convert.ToInt32(Session["userID"]));
            if (dt.Rows.Count > 0)
            {
                string url = "frmAlerts.aspx";
                string sAlert = "window.open('" + url + "', 'popup_windowAlert', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=700, height=300, copyhistory=no, left=400, top=200');";
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "popup_windowAlert", sAlert, true);
            }
        }

        protected void newTask_Click(object sender, EventArgs e)
        {
            Session["referer"] = Request.Url.ToString().Replace("default.aspx", "default.aspx?Ptabindex=3");
            int userTaskID = Convert.ToInt32(gvTasks.DataKeys[gvTasks.SelectedIndex].Value);

            string claimid = Regex.Replace(gvTasks.SelectedRow.Cells[9].Text, "[^0-9]", "");
            string url = "frmNewTask.aspx?mode=2&claim_id=" + claimid + "&user_task_id=" + userTaskID;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "Open", s, true);
        }

        protected void clearTask_Click(object sender, EventArgs e)
        {
            if (gvTasks.SelectedIndex < 0)
                return;

            //if (MessageBox.Show("Are you sure you Want to clear this task?", "Clear Task", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
            //    return;

            mm.UpdateLastActivityTime();

             

            //clear user task
            Session["referer"] = Request.Url.ToString().Replace("default.aspx", "default.aspx?Ptabindex=3");
            int userTaskID = Convert.ToInt32(gvTasks.DataKeys[gvTasks.SelectedIndex].Value);
           
            string claimid = Regex.Replace(gvTasks.SelectedRow.Cells[9].Text, "[^0-9]", "");
            string url = "frmClearTask.aspx?mode=2&claim_id=" + claimid + "&user_task_id=" + userTaskID;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "Open", s, true);
        }

        protected void gvDiary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow grv = e.Row;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (Convert.ToDateTime(grv.Cells[1].Text) < DateTime.Now)
                {
<<<<<<< HEAD
                    
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);  //pumpkin
                    //e.Row.BackColor = System.Drawing.Color.FromArgb(158, 108, 105);   //rust
=======
<<<<<<< HEAD
                    
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);  //pumpkin
                    //e.Row.BackColor = System.Drawing.Color.FromArgb(158, 108, 105);   //rust
=======
                    e.Row.BackColor = System.Drawing.Color.Salmon;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    e.Row.ForeColor = System.Drawing.Color.White;
                    HyperLink arEdit = (HyperLink)grv.FindControl("Hyperlink2");
                    arEdit.ForeColor = System.Drawing.Color.White;
                }


            }

        }

       



       
    }


    //protected void mTab_MenuItemClick(object sender, MenuEventArgs e)
    //{
    //    Page.Validate();
    //    if (Page.IsValid)
    //    {
    //        int index = Int32.Parse(e.Item.Value);
    //        wSteps.ActiveStepIndex = index;
    //    }




    //}
}

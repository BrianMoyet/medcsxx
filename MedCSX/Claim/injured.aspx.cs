﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;

namespace MedCSX.Claim
{
    public partial class injured : System.Web.UI.Page
    {
        private int claim_id;
        private string request_id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;
        Injured myInjured = new Injured();
        Injured updateInjured = new Injured();
        private string mode = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Injured";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            request_id = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");

            mode = Request.QueryString["mode"].ToString();

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            ddlInjuredPerson.SelectedValue = request_id;
            ExtendedDetails(request_id);

            Injured myData = new Injured();
            //myData.GetWitnessInfoByID(id);
            //ddlWitnessPassengerTypes.SelectedValue = myData.witness_passenger_type_id;
            //ddlPersonType.SelectedValue = myData.witness_passenger_type_id;
        }

        protected void PopulateControls()
        {
            DataTable dt = new DataTable();

            Injured myData = new Injured();
            dt = myData.GetInjuredForClaim(claim_id);

            ddlInjuredPerson.DataSource = dt;
            ddlInjuredPerson.DataValueField = dt.Columns[0].ColumnName;
            ddlInjuredPerson.DataTextField = dt.Columns[48].ColumnName;
            ddlInjuredPerson.DataBind();

            DataTable dtInjuredReserves = new DataTable();
            dtInjuredReserves = myData.GetInjuredReservesForClaim(claim_id, 2);
            gvReserves.DataSource = dtInjuredReserves;
            gvReserves.DataBind();
            gvReserves.Visible = true;

            DataTable dtInjuredLiens = new DataTable();
            dtInjuredLiens = myData.GetInjuredLiensForClaim(Convert.ToInt32(request_id));
            gvLiens.DataSource = dtInjuredLiens;
            gvLiens.DataBind();
            gvLiens.Visible = true;

            ModLogic myLogic = new ModLogic();
            dt = myLogic.UsersWhichCanBeAssignedClaims();
            ddlAssignToUser.DataSource = dt;
            ddlAssignToUser.DataValueField = dt.Columns["user_id"].ColumnName;
            ddlAssignToUser.DataTextField = dt.Columns["Last_Name"].ToString();
            ddlAssignToUser.DataBind();

            dt = myLogic.UserGroupsWhichCanBeAssignedClaims();
            ddlAssignToGroup.DataSource = dt;
            ddlAssignToGroup.DataValueField = dt.Columns["user_group_id"].ColumnName;
            ddlAssignToGroup.DataTextField = dt.Columns["Description"].ToString();
            ddlAssignToGroup.DataBind();

            dt = HelperFunctions.getTypeTableRows("Injury_Type", "Description", "1=1");
            ddlInjuryType.DataSource = dt;
            ddlInjuryType.DataValueField = dt.Columns[0].ToString();
            ddlInjuryType.DataTextField = dt.Columns[1].ToString();
            ddlInjuryType.DataBind();

            dt = HelperFunctions.getTypeTableRows("Injury_Cause", "Description", "1=1");
            ddlInjuryCause.DataSource = dt;
            ddlInjuryCause.DataValueField = dt.Columns[0].ColumnName;
            ddlInjuryCause.DataTextField = dt.Columns[1].ToString();
            ddlInjuryCause.DataBind();
        }

        public string FormatPopupInjured(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2Liens" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0,'my_window2Liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Liens" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2Liens" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claim_id + "&witness=" + id + "','my_window2Liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Liens" + id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2PD" + id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0,'my_window2PD" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2PD" + id + ".focus()";

            return tmpString;
        }

        public string FormatPopupReserve(string id, string mode)
        {
            string tmpString = ""; 
            if (mode == "c")
            {
                tmpString = "javascript:my_window2Reserve" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0,'my_window2Reserve" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Reserve" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2Resere" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claim_id + "&resere=" + id + "','my_window2Reserve" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Reserve" + id + ".focus()";
            }

           

            return tmpString;
        }

        public string FormatPopupInjuredBodyPart(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('injuredBodyPart.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0,'my_window2injuredBodyPart" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2injuredBodyPart" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('injuredBodyPart.aspx?mode=" + mode + "&claim_id=" + claim_id + "&injuredBodyPart=" + id + "','my_window2injuredBodyPart" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2injuredBodyPart" + id + ".focus()";
            }



            return tmpString;
        }

        public string FormatPopupLiens(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claim_id + "&liens=0,'my_window2liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2liens" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claim_id + "&liens=" + id + "','my_window2liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2liens" + id + ".focus()";
            }



            return tmpString;
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnInjuredDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnReserveDelete_Click(object sender, EventArgs e)
        {

        }
        protected void btnInjuredBodyPartDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnLiensDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlInjuredPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExtendedDetails(ddlInjuredPerson.SelectedValue);
        }

        public void ExtendedDetails(string id)
        {

            string display = "";

            Claimants myData = new Claimants();
            myData.GetPersonByID(Convert.ToInt32(id));
            display += myData.salutation + " " + myData.first_name + " " + myData.middle_initial + " " + myData.last_name + " " + myData.suffix;
            if ((myData.address1 != "") || (myData.city != "") || (myData.state != "") || (myData.zipcode != ""))
            {
                display += "<br />" + myData.address1 + " " + myData.address2;
                display += "<br />" + myData.city + ", " + myData.state + " " + myData.zipcode;
            }
            if ((myData.home_phone != "") && (CountDigits(myData.home_phone) > 0))
                display += "<br />" + "Home Phone: " + myData.home_phone;
            if ((myData.work_phone != "") && (CountDigits(myData.work_phone) > 0))
                display += "<br />" + "Work Phone: " + myData.work_phone;
            if ((myData.cell_phone != "") && (CountDigits(myData.cell_phone) > 0))
                display += "<br />" + "Cell Phone: " + myData.cell_phone;
            if ((myData.other_contact_phone != "") && (CountDigits(myData.other_contact_phone) > 0))
                display += "<br />" + "Other Contact Phone: " + myData.other_contact_phone;
            if ((myData.fax != "") && (CountDigits(myData.fax) > 0))
                display += "<br />" + "Fax: " + myData.fax;
            if ((myData.email != "") && (myData.email.Length > 0))
                display += "<br />" + "Email: " + myData.email;
            if ((myData.date_of_birth != "1/1/1900 12:00:00 AM") && (myData.date_of_birth.Length > 0))
                display += "<br />" + "Date of Birth: " + myData.date_of_birth;
            if (myData.ssn != "")
                display += "<br />" + "SSN: " + myData.ssn;
            if (myData.drivers_license_no != "")
                display += "<br />" + "Drivers License No: " + myData.drivers_license_no;
            if (myData.sex != "")
                display += "<br />" + "Sex: " + myData.sex;
            if (myData.employer != "")
                display += "<br />" + "Employer: " + myData.employer;
            if (myData.language_id != "")
                display += "<br />" + "Employer: " + HelperFunctions.GetLanguageByID(Convert.ToInt32(myData.language_id));

            lblClaimantDetails.Text = display;
        }

        public int CountDigits(string s)
        {
            int i;
            int count;

            count = 0;
            for (i = 0; i <= s.Length - 1; i++)
            {
                if (IsNumeric(s.Substring(i, 1)))
                    count = count + 1;
            }
            return count;
        }

        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
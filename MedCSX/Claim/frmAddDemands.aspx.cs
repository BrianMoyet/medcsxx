﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAddDemands : System.Web.UI.Page
    {
        public bool okPressed = false;      //was OK button pressed
        private MedCsxLogic.Claim m_Claim = null;
        private int selectedVendorId = 0;   //vendor_Id of selected attorney

        private List<ClaimantGrid> myAttorneyList;
        private List<ReserveGrid> myDemandList;
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_Claim = new MedCsxLogic.Claim(Convert.ToInt32(Request.QueryString["claim_id"]));

            if (!Page.IsPostBack)
            {
                LoadAttorneys();
                LoadDemand();
            }

            referer = Session["referer"].ToString();
        }

        private bool LoadDemand()
        {
            this.grDemands.DataSource = LoadDemand(this.m_Claim.ReservesWithImages());
            this.grDemands.DataBind();
            return true;
        }

        private IEnumerable LoadDemand(List<Hashtable> list)
        {
            myDemandList = new List<ReserveGrid>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    ReserveGrid cg = new ReserveGrid();
                    cg.id = (int)dData["Reserve_Id"];
                    cg.claimant = (string)dData["Claimant"];
                    cg.reserveType = (string)dData["Reserve_Type"];
                    cg.adjCode = "";
                    myDemandList.Add(cg);
                }
                return myDemandList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDemandList;
            }
        }

        private void LoadAttorneys()
        {
            this.cboDemandAttorney.DataSource = LoadAttorneys(Vendor.VendorsOfType(Demand.AttorneyVendorType));
            this.cboDemandAttorney.DataValueField = "claimantId";
            this.cboDemandAttorney.DataTextField = "fullName";
            this.cboDemandAttorney.DataBind();
        }

        private IEnumerable LoadAttorneys(List<Hashtable> list)
        {
            myAttorneyList = new List<ClaimantGrid>();
            try
            {
                foreach (Hashtable aData in list)
                {
                    if (((string)aData["Name"]).Substring(0, 2) != "**")
                    {
                        ClaimantGrid cg = new ClaimantGrid();
                        cg.claimantId = (int)aData["Vendor_Id"];
                        cg.fullName = (string)aData["Name"];
                        cg.streetAddress = (string)aData["Address 1"];
                        cg.Address2 = (string)aData["Address 2"];
                        cg.city = (string)aData["City"];
                        cg.state = (string)aData["State"];
                        cg.postalCode = (string)aData["Zipcode"];
                        cg.phone = (string)aData["Phone"];
                        cg.fax = (string)aData["Fax"];
                        cg.email = (string)aData["Email"];
                        cg.taxId = (string)aData["Tax Id"];
                        cg.imageIdx = (int)aData["Image_Index"];
                        myAttorneyList.Add(cg);
                    }
                }
                return myAttorneyList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAttorneyList;
            }
        }

        protected void llAddAttorney_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString().Replace("tabindex=18", "Ptabindex=8");
            string url = "frmAddEditVendor.aspx?mode=c&vendor_id=0&vendor_type_id=29";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(this.cboDemandAttorney.SelectedValue) == 0)
            {   //the vendor is invalid
                //if (MessageBox.Show("No attorney has been selected.  Are you sure you do not want to select an attorney?", "No Attorney Selected", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
                //    return;

                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>return confirm('No attorney has been selected.  Are you sure you do not want to select an attorney?'));</script>");
            }

            if (this.dpDemandDate.Text == "")
            {   //the vendor is invalid
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a Demand Date.');</script>");
                //MessageBox.Show("Please enter a Demand Date", "Demand Date not entered.", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                return;
            }

            if (this.dpReceivedDate.Text == "")
            {   //the vendor is invalid
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a Received Date.');</script>");
                //MessageBox.Show("Please enter a Received Date", "Received Date not entered.", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                return;
            }

            if (this.dpDueDate.Text == "")
            {   //the vendor is invalid
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a Due Date.');</script>");
                //MessageBox.Show("Please enter a Due Date", "Due Date not entered.", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                return;
            }

            string msg = "";
            for (int i = 0; i < grDemands.Rows.Count; i++)
            {
                int strReserveID = Convert.ToInt32(grDemands.DataKeys[i].Value);

                string adjCode = ((System.Web.UI.WebControls.TextBox)grDemands.Rows[i].FindControl("adjCode")).Text;

                if (adjCode != "")
                {
                    if (Demand.DemandExists(Convert.ToInt32(this.cboDemandAttorney.SelectedValue), strReserveID))
                    {
                        Reserve res = new Reserve(strReserveID);  //get the reserve
                        msg += "A demand already exists for claimant " + res.ClaimantName() + " with reserve type " + res.ReserveType.Description + " and the selected attorney." + Environment.NewLine;
                    }
                }

                if (msg != "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Unable to Create New Demands.');</script>");
                    //MessageBox.Show(msg, "Unable to Create New Demands", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    return;
                }

                if (adjCode != "")
                { 
                    Demand myDemand = new Demand();     //create new demand
                    myDemand.VendorId = this.selectedVendorId;
                    myDemand.ReserveId = strReserveID;
                    myDemand.DemandAmount = double.Parse(adjCode);
                    myDemand.DemandDate = Convert.ToDateTime(this.dpDemandDate.Text);
                    myDemand.ReceivedDate = Convert.ToDateTime(this.dpReceivedDate.Text);
                    myDemand.DueDate = Convert.ToDateTime(this.dpDueDate.Text);
                    myDemand.Update();

                    //fire the received demand event
                    Hashtable parms = new Hashtable();
                    parms.Add("Demand_Amount", myDemand.DemandAmount);

                    if (myDemand.VendorId == 0)
                        parms.Add("Attorney", myDemand.Reserve.Claimant.Name);  //no vendor selected - use claimant
                    else
                        parms.Add("Attorney", myDemand.Vendor.Name);

                    parms.Add("Claimant", myDemand.Reserve.Claimant.Name);
                    parms.Add("Reserve", myDemand.Reserve.ReserveType.Description);
                    parms.Add("Due_Date", myDemand.DueDate.ToShortDateString());
                    myDemand.Reserve.FireEvent((int)modGeneratedEnums.EventType.Received_Demand, parms);

                }

                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
            }
        }


        private int setSelection(int p)
        {
            int idIdx = 0;
            if (p == 0)
                return idIdx;

            foreach (ClaimantGrid c in myAttorneyList)
            {
                if (c.claimantId == p)
                    break;
                idIdx++;
            }
            return idIdx;
        }


       
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmWitnessPassengerDetails : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private string referer = "";
        public bool okPressed = false;     //was OK pressed? Output parameter.
        public int personId = 0;      //output parameter

        private int m_claimId = 0,
            m_PersonId = 0,
            m_WitnessId = 0,
            m_PassengerLocation = 0;
        private WitnessPassenger witnessPassenger;
        private Claimant claimant;
        private ArrayList persons;
        private bool doneLoading = false;
        private MedCsxLogic.Claim m_claim;
        private string titleExt = "";
        private string mode = "";

        private List<witnessType> myWitnessType;
        private List<ClaimantGrid> myPersonList;

        protected void Page_Load(object sender, EventArgs e)
        {
            m_claimId = Convert.ToInt32(Request.QueryString["claim_id"]);
            m_PersonId = Convert.ToInt32(Request.QueryString["person_id"]);
            m_claim = new MedCsxLogic.Claim(m_claimId);
            mode = Request.QueryString["mode"].ToString();
            SetTitle();

            referer = Session["referer"].ToString();

            if (!Page.IsPostBack)
            {
                cboPersonType.DataSource = LoadTypeTableComboBox(cboPersonType, false, "Claim_Type_Id = " + this.Claim.ClaimTypeId);
                cboPersonType.DataValueField = "witnessTypeId";
                cboPersonType.DataTextField = "witnessDescription";
                cboPersonType.DataBind();
                cboPersonType.SelectedIndex = 0;

                if (m_PersonId == 0)
                { 
                    this.Page.Title = "Claim " + Claim.DisplayClaimId + " - Add " + titleExt;
                    cboVehicleSelect_SelectedIndexChanged(null, null);
                }
                else
                {
                    this.Page.Title = "Claim " + Claim.DisplayClaimId + " - Edit " + titleExt;
                    //load property damage details
                    Person p = new Person(m_PersonId);
                    m_WitnessId = p.WitnessPassenger().WitnessPassengerId;
                    witnessPassenger = new WitnessPassenger(m_WitnessId);
                    m_PersonId = witnessPassenger.PersonId;
                    
                    //this.cboPersonType.SelectedIndex = SelectTypeCbo(witnessPassenger.WitnessPassengerTypeId);
                  


                    cboVehicleSelect_SelectedIndexChanged(null, null);


                }

               
                    RefreshPersons();

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                if (Request.QueryString["personid"] != null)
                    this.cboPersonSelect.SelectedValue = Request.QueryString["personid"].ToString();
                else if (Request.QueryString["person_id"] != null)
                     this.cboPersonSelect.SelectedValue = Request.QueryString["person_id"].ToString();
                else
                    this.cboPersonSelect.SelectedValue = m_PersonId.ToString();
<<<<<<< HEAD
=======
=======
                this.cboPersonSelect.SelectedValue = m_PersonId.ToString();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                if (Convert.ToInt32(Request.QueryString["person_id"]) > 0)
                { 
                    RefreshLocation();
                }
            }

<<<<<<< HEAD
            populateFormFromSessionvariables();

            
=======
<<<<<<< HEAD
            populateFormFromSessionvariables();

            
=======
            if (Session["cboPersonType"] != null)
            {
                this.cboPersonType.SelectedValue = Session["cboPersonType"].ToString();
                cboPersonSelect_SelectedIndexChanged(null, null);
                this.cboVehicleSelect.SelectedValue = Session["cboVehicleSelect"].ToString();
                cboVehicleSelect_SelectedIndexChanged(null, null);
                this.cboPersonSelect.SelectedValue = Session["cboPersonSelect"].ToString();
                ResetLocationFromPostback(Convert.ToInt32(Session["lid"]));


                Session["cboPersonType"] = cboPersonType.SelectedValue.ToString();
                Session["cboPersonSelect"] = cboPersonSelect.SelectedValue.ToString();
                Session["cboVehicleSelect"] = cboVehicleSelect.SelectedValue.ToString();
                Session["lid"] = getPassengerLocation();


            }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            if (Request.QueryString["control"] != null)
            {
                switch (Request.QueryString["control"].ToString())
                {
                    case "cboPersonSelect":
                        cboPersonSelect.SelectedValue = Request.QueryString["personid"].ToString();
                        cboPersonSelect_SelectedIndexChanged(null, null);
                        break;
                  
                    case "cboVehicleSelect":
                        cboVehicleSelect.SelectedValue = Request.QueryString["vehicleid"].ToString();
                        cboVehicleSelect_SelectedIndexChanged(null, null);
                        break;
                    default:
                        break;
                }
            }
        }
<<<<<<< HEAD
        
=======
<<<<<<< HEAD
        
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

        public void ResetLocationFromPostback(int lid)
        {
            switch (lid)
            {
                case (int)modGeneratedEnums.WitnessPassengerLocation.BackSeatDrivers:
                    rbWitnessBackDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.BackSeatPassenger:
                    rbWitnessBackPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.BackSeatMiddle:
                    rbWitnessBackCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowDrivers:
                    rbWitness3rdRowDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowPassenger:
                    rbWitness3rdRowPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowMiddle:
                    rbWitness3rdRowCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.FrontSeatMiddle:
                    rbWitnessFrontCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideFront:
                    rbWitnessFront.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideFrontPassenger:
                    rbWitnessFrontRight.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideFrontDriver:
                    rbWitnessFrontLeft.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideSidePassenger:
                    rbWitnessMiddleRight.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideSideDriver:
                    rbWitnessMiddleLeft.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideBackPassenger:
                    rbWitnessBackRight.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideBackDriver:
                    rbWitnessBackLeft.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideBack:
                    rbWitnessBack.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverFront:
                    rbWitnessBackDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedPassengerFront:
                    rbWitnessBackPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterFront:
                    rbWitnessBackCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverMiddle:
                    rbWitness3rdRowDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedPassengerMiddle:
                    rbWitness3rdRowPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterMiddle:
                    rbWitness3rdRowCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverBack:
                    rbWitness4thRowDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedPassengerBack:
                    rbWitness4thRowPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterBack:
                    rbWitness4thRowCenter.Checked = true;
                    break;
                default:        //passenger seat - 0
                    rbWitnessPassenger.Checked = true;
                    break;
            }
        }

        private void RefreshLocation()
        {
            if (m_PersonId == 0)
                return;
            Person p = new Person(m_PersonId);
            WitnessPassenger wp = new WitnessPassenger(p.WitnessPassenger().WitnessPassengerId);
           // WitnessPassenger wp = new WitnessPassenger(Convert.ToInt32(cboPersonSelect.SelectedValue));
            if (wp == null)
                return;


            this.cboVehicleSelect.SelectedIndex = 0;        //car
            if ((wp.WitnessPassengerLocation >= (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowDrivers) &&
                (wp.WitnessPassengerLocation <= (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowMiddle))
                this.cboVehicleSelect.SelectedIndex = 1;    //van
            else if ((wp.WitnessPassengerLocation >= (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverFront) &&
                (wp.WitnessPassengerLocation <= (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterBack))
                this.cboVehicleSelect.SelectedIndex = 2;    //truck

            switch (wp.WitnessPassengerLocation)
            {
                case (int)modGeneratedEnums.WitnessPassengerLocation.BackSeatDrivers:
                    rbWitnessBackDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.BackSeatPassenger:
                    rbWitnessBackPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.BackSeatMiddle:
                    rbWitnessBackCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowDrivers:
                    rbWitness3rdRowDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowPassenger:
                    rbWitness3rdRowPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.ThirdRowMiddle:
                    rbWitness3rdRowCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.FrontSeatMiddle:
                    rbWitnessFrontCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideFront:
                    rbWitnessFront.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideFrontPassenger:
                    rbWitnessFrontRight.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideFrontDriver:
                    rbWitnessFrontLeft.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideSidePassenger:
                    rbWitnessMiddleRight.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideSideDriver:
                    rbWitnessMiddleLeft.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideBackPassenger:
                    rbWitnessBackRight.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideBackDriver:
                    rbWitnessBackLeft.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.OutsideBack:
                    rbWitnessBack.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverFront:
                    rbWitnessBackDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedPassengerFront:
                    rbWitnessBackPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterFront:
                    rbWitnessBackCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverMiddle:
                    rbWitness3rdRowDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedPassengerMiddle:
                    rbWitness3rdRowPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterMiddle:
                    rbWitness3rdRowCenter.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedDriverBack:
                    rbWitness4thRowDrivers.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedPassengerBack:
                    rbWitness4thRowPassenger.Checked = true;
                    break;
                case (int)modGeneratedEnums.WitnessPassengerLocation.TruckBedCenterBack:
                    rbWitness4thRowCenter.Checked = true;
                    break;
                default:        //passenger seat - 0
                    rbWitnessPassenger.Checked = true;
                    break;
            }
        }

        private void RefreshPersons()
        {
          
            this.cboPersonSelect.DataSource = mm.GetMyWitnessPassengerPersons(this.Claim.Id);
            this.cboPersonSelect.DataValueField = "person_id";
            this.cboPersonSelect.DataTextField = "Name";
            this.cboPersonSelect.DataBind();

            DataTable dt = (DataTable)cboPersonSelect.DataSource;

            if (dt.Rows.Count > 0)
            { 
                if (m_WitnessId == 0)
                {
                    //Person p = new Person(m_PersonId);
                    //m_WitnessId = p.WitnessPassenger().WitnessPassengerId;
                    //witnessPassenger = new WitnessPassenger(m_WitnessId);
                    //m_PersonId = witnessPassenger.PersonId;
                    //this.cboPersonSelect.SelectedIndex = 0;
                    //Person p = new Person(Convert.ToInt32(cboPersonSelect.SelectedValue));
                    //m_WitnessId = p.WitnessPassenger().WitnessPassengerId;
                   

                }
                else
                {

                    //TODOOOOOOOOOOOOOOOOOO need to select by person id
                    this.cboPersonSelect.SelectedValue = m_PersonId.ToString();
                }
           


            

                 m_PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                this.txtPersonDetails.Text = new Person(m_PersonId).ExtendedDescription().Replace("\r\n", "<br>");

                //m_PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                //Person p = new Person(m_PersonId);
                //this.txtPersonDetails.Text = p.ExtendedDescription();

                if (cboPersonSelect.SelectedItem.ToString().Trim().Split(' ')[0] == "Unknown")
                    this.btnPersonEdit.Enabled = false;
                else
                    this.btnPersonEdit.Enabled = true;
            }
        }


        private List<witnessType> LoadTypeTableComboBox(DropDownList cb, bool includeZeros, string whereClause)
        {
            myWitnessType = new List<witnessType>();
            try
            {
                foreach (Hashtable wData in TypeTable.getTypeTableRows("Witness_Passenger_Type", whereClause))
                {
                    witnessType wt = new witnessType();
                    wt.witnessTypeId = (int)wData["Id"];
                    wt.imageIdx = (int)wData["Image_Index"];
                    wt.witnessDescription = (string)wData["Description"];
                    if (includeZeros || (wt.witnessTypeId != 0))
                        myWitnessType.Add(wt);
                }
                return myWitnessType;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myWitnessType;
            }
        }

        private void SetTitle()
        {
            this.Page.Title = "Witness / Passenger";
        }

        protected void btnPersonOK_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

               

                if (m_PassengerLocation == -1)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Where the passenger was located has not been selected.');</script>");
                    //MessageBox.Show("Where the passenger was located has not been selected.", "Missing Passenger Location", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    rbWitnessPassenger.Focus();
                    return;
                }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                //if (Convert.ToInt32(Request.QueryString["person_id"]) > 0)
                //    { 
                //        if (witnessPassenger.PersonId != Convert.ToInt32(cboPersonSelect.SelectedValue))
                //        {
                //            if (this.m_claim.ExistingWitnessPassenger(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                //            {
                //                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.');</script>");
                //                //MessageBox.Show(Person.PersonName(m_PersonId) + " is already a Witness / Passenger for this Claim.", "Already Entered", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                //                return;
                //            }
                //        }
                //}


                if (m_PersonId == 0)
                    {
                        if (this.m_claim.ExistingWitnessPassenger(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.');</script>");
                            //MessageBox.Show(Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.", "Already Entered", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                            return;
<<<<<<< HEAD
=======
=======
                

               
                    if (m_PersonId == 0)
                    {
                        if (this.m_claim.ExistingWitnessPassenger(Convert.ToInt32(cboPersonSelect.SelectedValue)))
                        {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.');</script>");
                        //MessageBox.Show(Person.PersonName(Convert.ToInt32(cboPersonSelect.SelectedValue)) + " is already a Witness / Passenger for this Claim.", "Already Entered", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        return;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                        }

                        witnessPassenger = new WitnessPassenger();
                    }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    if (Request.QueryString["personid"] != null)
                    {
                        //Person p2 = new Person(Convert.ToInt32(cboPersonSelect.SelectedValue));
                        //m_WitnessId = p2.WitnessPassenger().WitnessPassengerId;

                        witnessPassenger = new WitnessPassenger();
                       // witnessPassenger.WitnessPassengerId = m_WitnessId;
                        witnessPassenger.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                        witnessPassenger.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
                        witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                        witnessPassenger.WitnessPassengerLocation = getPassengerLocation();
                        this.m_WitnessId = witnessPassenger.Update();
                    }
                    else
                    { 

                        if (Convert.ToInt32(Request.QueryString["person_id"]) > 0)
                        {
                            Person p2 = new Person(Convert.ToInt32(cboPersonSelect.SelectedValue));
                            m_WitnessId = p2.WitnessPassenger().WitnessPassengerId;

                            witnessPassenger = new WitnessPassenger(m_WitnessId);
                            witnessPassenger.WitnessPassengerId = m_WitnessId;
                            witnessPassenger.PersonId =Convert.ToInt32(cboPersonSelect.SelectedValue);
                            witnessPassenger.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
                            witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                            witnessPassenger.WitnessPassengerLocation = getPassengerLocation();
                            this.m_WitnessId = witnessPassenger.Update();
                        }
                        else
                        {
                   

                            witnessPassenger = new WitnessPassenger();
                            witnessPassenger.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                            witnessPassenger.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
                            witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                            witnessPassenger.WitnessPassengerLocation = getPassengerLocation();
                            this.m_WitnessId = witnessPassenger.Update();
                        }

                    }

               
<<<<<<< HEAD
=======
=======
                if (Convert.ToInt32(Request.QueryString["person_id"]) > 0)
                {
                    Person p2 = new Person(Convert.ToInt32(cboPersonSelect.SelectedValue));
                    m_WitnessId = p2.WitnessPassenger().WitnessPassengerId;

                    witnessPassenger = new WitnessPassenger(m_WitnessId);
                    witnessPassenger.WitnessPassengerId = m_WitnessId;
                    witnessPassenger.PersonId =Convert.ToInt32(cboPersonSelect.SelectedValue);
                    witnessPassenger.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
                    witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                    witnessPassenger.WitnessPassengerLocation = getPassengerLocation();
                    this.m_WitnessId = witnessPassenger.Update();
                }
                else
                {
                   

                    witnessPassenger = new WitnessPassenger();
                    witnessPassenger.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                    witnessPassenger.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
                    witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                    witnessPassenger.WitnessPassengerLocation = getPassengerLocation();
                    this.m_WitnessId = witnessPassenger.Update();
                }

                if (witnessPassenger.PersonId != m_PersonId)
                {
                    if (this.m_claim.ExistingWitnessPassenger(m_PersonId))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(m_PersonId) + " is already a Witness / Passenger for this Claim.');</script>");
                        //MessageBox.Show(Person.PersonName(m_PersonId) + " is already a Witness / Passenger for this Claim.", "Already Entered", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                        return;
                    }
                }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                //witnessPassenger = new WitnessPassenger(m_WitnessId);
                //witnessPassenger.PersonId = m_PersonId;
                //witnessPassenger.ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
                //witnessPassenger.WitnessPassengerTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                //witnessPassenger.WitnessPassengerLocation = getPassengerLocation();
                //personId = witnessPassenger.Update();

                //Person p = new Person(m_PersonId);
                //p.PersonTypeId = Convert.ToInt32(cboPersonType.SelectedValue);
                //p.UpdateUnknownPersons();   //add additional unknown person as necessary

                this.okPressed = true;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public int getPassengerLocation()
        {
            int lid = 0;

            if (rbWitnessFrontRight.Checked)
                lid = 9;
            else if (rbWitnessMiddleRight.Checked)
                lid = 11;
            else if (rbWitnessBackRight.Checked)
                lid = 13;
            else if (rbWitnessFront.Checked)
                lid = 9;
            else if (rbWitnessPassenger.Checked)
                lid = 0;
            else if (rbWitnessBackPassenger.Checked)
                lid = 2;
            else if (rbWitness3rdRowPassenger.Checked)
                lid = 5;
            else if (rbWitness4thRowPassenger.Checked)
                lid = 23;
            else if (rbWitnessFrontCenter.Checked)
                lid = 7;
            else if (rbWitnessBackCenter.Checked)
                lid = 3;
            else if (rbWitness3rdRowCenter.Checked)
                lid = 6;
            else if (rbWitness4thRowCenter.Checked)
                lid = 24;
            else if (rbWitnessBackDrivers.Checked)
                lid = 1;
            else if (rbWitness3rdRowDrivers.Checked)
                lid = 4;
            else if (rbWitness4thRowDrivers.Checked)
                lid = 22;
            else if (rbWitnessBack.Checked)
                lid = 15;
            else if (rbWitnessFrontLeft.Checked)
                lid = 10;
            else if (rbWitnessMiddleLeft.Checked)
                lid = 12;
            else if (rbWitnessBackLeft.Checked)
                lid = 14;

            return lid;
        }



        protected void cboPersonSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPersonSelect.SelectedItem.ToString().Trim().Split(' ')[0] == "Unknown")
                this.btnPersonEdit.Enabled = false;
            else
                this.btnPersonEdit.Enabled = true;

            try
            {
                mm.UpdateLastActivityTime();

                if (cboPersonSelect.SelectedIndex == -1)
                    return;

                int tmpWID = Convert.ToInt32(cboPersonSelect.SelectedValue);

                //m_PersonId = new WitnessPassenger(tmpWID).PersonId;
                 
                this.txtPersonDetails.Text = new Person(Convert.ToInt32(cboPersonSelect.SelectedValue)).ExtendedDescription().Replace("\r\n", "<br>");
             
                if (Convert.ToInt32(Request.QueryString["person_id"]) > 0)
                {
                    RefreshLocation();
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboVehicleSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboVehicleSelect.SelectedValue)
            {
                case "VehCar":
                    tblCar.Style.Add("background-image", "url('../content/images/icoredcar2.png')");
                    this.rbWitness3rdRowDrivers.Visible = false;
                    this.rbWitness3rdRowPassenger.Visible = false;
                    this.rbWitness3rdRowCenter.Visible = false;
                    this.rbWitness4thRowDrivers.Visible = false;
                    this.rbWitness4thRowPassenger.Visible = false;
                    this.rbWitness4thRowCenter.Visible = false;
                    this.rbWitness3rdRowPassenger.Checked = false;
                    this.rbWitness3rdRowDrivers.Checked = false;
                    this.rbWitness3rdRowCenter.Checked = false;
                    this.rbWitness4thRowPassenger.Checked = false;
                    this.rbWitness4thRowDrivers.Checked = false;
                    this.rbWitness4thRowCenter.Checked = false;
                    break;
                case "VehVan":
                    tblCar.Style.Add("background-image", "url('../content/images/icoredvantop2.png')");
                    this.rbWitness4thRowDrivers.Visible = false;
                    this.rbWitness4thRowPassenger.Visible = false;
                    this.rbWitness4thRowCenter.Visible = false;
                    this.rbWitness4thRowPassenger.Checked = false;
                    this.rbWitness4thRowDrivers.Checked = false;
                    this.rbWitness4thRowCenter.Checked = false;
                    this.rbWitness3rdRowDrivers.Visible = true;
                    this.rbWitness3rdRowPassenger.Visible = true;
                    break;
                case "VehTruck":
                    tblCar.Style.Add("background-image", "url('../content/images/icotrucktop2.png')");
                    this.rbWitness4thRowDrivers.Visible = true;
                    this.rbWitness4thRowPassenger.Visible = true;
                    this.rbWitness4thRowCenter.Visible = true;
                    this.rbWitness3rdRowDrivers.Visible = true;
                    this.rbWitness3rdRowPassenger.Visible = true;
                    this.rbWitness3rdRowCenter.Visible = true;
                    break;
            }
        }

        protected void btnPersonNew_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString();

<<<<<<< HEAD
            populateSessionVariables();
=======
<<<<<<< HEAD
            populateSessionVariables();
=======
            Session["cboPersonType"] = cboPersonType.SelectedValue.ToString();
            Session["cboPersonSelect"] = cboPersonSelect.SelectedValue.ToString();
            Session["cboVehicleSelect"] = cboVehicleSelect.SelectedValue.ToString();
            Session["lid"] = getPassengerLocation();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


            string url = "frmPersonDetails.aspx?mode=u&person_type_id=" + cboPersonType.SelectedValue + "&claim_id=" + this.m_claimId + "&person_id=0&control=cboPersonSelect";
            string s = "window.open('" + url + "', 'popup_windowp', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowp", s, true);
        }

        protected void btnPersonEdit_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString();

<<<<<<< HEAD
            populateSessionVariables();
=======
<<<<<<< HEAD
            populateSessionVariables();
=======
            Session["cboPersonType"] = cboPersonType.SelectedValue.ToString();
            Session["cboPersonSelect"] = cboPersonSelect.SelectedValue.ToString();
            Session["cboVehicleSelect"] = cboVehicleSelect.SelectedValue.ToString();
            Session["lid"] = getPassengerLocation();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e



            string url = "frmPersonDetails.aspx?mode=u&person_type_id=" + cboPersonType.SelectedValue + "&claim_id=" + this.m_claimId + "&person_id=" + cboPersonSelect.SelectedValue.ToString() + "&control=cboPersonSelect";
            string s = "window.open('" + url + "', 'popup_windowp', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowp", s, true);
        }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        public void populateFormFromSessionvariables()
        {
            try
            {
                string sessionName = "cboPersonType" + this.m_claimId.ToString();

                if (Session[sessionName] != null)
                {
                    this.cboPersonType.SelectedValue = Session["cboPersonType"].ToString();
                    cboPersonSelect_SelectedIndexChanged(null, null);
                    this.cboVehicleSelect.SelectedValue = Session["cboVehicleSelect"].ToString();
                    cboVehicleSelect_SelectedIndexChanged(null, null);
                    this.cboPersonSelect.SelectedValue = Session["cboPersonSelect"].ToString();
                    ResetLocationFromPostback(Convert.ToInt32(Session["lid"]));

                    Session[sessionName] = null;
                    Session["cboPersonType"] = null;
                    Session["cboPersonSelect"] = null;
                    Session["cboVehicleSelect"] = null;
                    Session["lid"] = null;


                }
            }
            catch (Exception ex)
            {
                string sessionName = "cboPersonType" + this.m_claimId.ToString();
                Session[sessionName] = null;
                Session["cboPersonType"] = null;
                Session["cboPersonSelect"] = null;
                Session["cboVehicleSelect"] = null;
                Session["lid"] = null;

                mf.ProcessError(ex);
            }
        }

        protected void populateSessionVariables()
        {
            string sessionName = "cboPersonType" + this.m_claimId.ToString();
            Session[sessionName] = "set";

            Session["cboPersonType"] = cboPersonType.SelectedValue.ToString();
            Session["cboPersonSelect"] = cboPersonSelect.SelectedValue.ToString();
            Session["cboVehicleSelect"] = cboVehicleSelect.SelectedValue.ToString();
            Session["lid"] = getPassengerLocation();
        }


<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        private int SelectTypeCbo(int p)
        {
            int cboIdx = 0;
            foreach (witnessType type in myWitnessType)
            {
                if (type.witnessTypeId == p)
                    break;
                cboIdx++;
            }
            return cboIdx;
        }

        private MedCsxLogic.Claim Claim
        {
            get { return m_claim; }
            set { m_claim = value; }
        }

        class witnessType
        {
            public int witnessTypeId { get; set; }
            public int imageIdx { get; set; }
            public string witnessDescription { get; set; }
        }
    }
}
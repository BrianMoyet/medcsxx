﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;

namespace MedCSX.Claim
{
    public partial class NewClaim : System.Web.UI.Page
    {
        //private static ModMain mm = new ModMain();
        public string selectedPolNo = "";
        public string selectedVerNo = "";
        public bool isExpired = false;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            string tmpsubClaimTypeID = Request.QueryString["subClaimTypeID"].ToString();

            switch (tmpsubClaimTypeID)
            {
                case "1":
                    Page.Title = "Find Policy for New Claim - Auto";
                    break;
                case "4":
                    Page.Title = "Find Policy for New Claim - Commercial Crime";
                    break;
                case "6":
                    Page.Title = "Find Policy for New Claim - Commercial General Liabiltiy";
                    break;
                case "3":
                    Page.Title = "Find Policy for New Claim - Commercial Property Damage";
                    break;
                case "5":
                    Page.Title = "Find Policy for New Claim - Commercial Umbrella";
                    break;

                default:
                    Page.Title = "Find Policy for New Claim - Unknown";
                    break;
            }

                  

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                txtDateOfLoss.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");             
                populateDDLs();
            }
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateLocationsBYCompanyID(ddlCompany.SelectedItem.ToString());
        }

        public void populateLocationsBYCompanyID(string companyId)
        {
            DataTable dt = new DataTable();
            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            dt = myData.GetCompanyLocations(ddlCompany.SelectedItem.ToString());


            ddlLocation.DataSource = dt;
            ddlLocation.DataValueField = dt.Columns[0].ColumnName;
            ddlLocation.DataTextField = dt.Columns[2].ColumnName;
            ddlLocation.DataBind();
        }


        private void populateDDLs()
        {
            DataTable dtState = new DataTable();
            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            dtState = myData.PopulateDDLStates();
            ddlState.DataSource = dtState;
            ddlState.DataValueField = dtState.Columns[0].ColumnName;
            ddlState.DataTextField = dtState.Columns[1].ColumnName;
            ddlState.DataBind();

            DataTable dtCompanies = new DataTable();
            dtCompanies = myData.GetCompanies();
            ddlCompany.DataSource = dtCompanies;
            ddlCompany.DataValueField = dtCompanies.Columns[3].ColumnName;
            ddlCompany.DataTextField = dtCompanies.Columns[1].ColumnName;
            ddlCompany.DataBind();

            ddlCompany.SelectedValue = "KI";
            populateLocationsBYCompanyID(ddlCompany.SelectedItem.ToString());
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateSearchData())
                return;

            DataTable dt = new DataTable();
            PolicyData myData = new PolicyData();


           dt =  myData.FindPolicies(true, this.txtDateOfLoss.Text,
                    this.txtPolicyNo.Text, this.txtFirstName.Text, this.txtLastName.Text,
                    this.txtSSN.Text, this.txtAutoYear.Text,
                    this.txtMake.Text, this.txtModel.Text,
                    this.txtVIN.Text, this.txtAddress.Text,
                    this.txtCity.Text, ddlState.SelectedValue.ToString(),
                    this.txtZip.Text);
            gvPoliciesFound.DataSource = dt;
            gvPoliciesFound.DataBind();
            gvPoliciesFound.Visible = true;

            gvPoliciesFound.SelectedIndex = 0;
        }

        private bool ValidateSearchData()
        {
            //check for date of loss


            if ((this.txtPolicyNo.Text == "") && (this.txtFirstName.Text == "") &&
                (this.txtLastName.Text == "") && (this.txtSSN.Text == "") &&
                (this.txtAutoYear.Text == "") && (this.txtMake.Text == "") &&
                (this.txtModel.Text == "") && (this.txtVIN.Text == "") &&
                (this.txtAddress.Text == "") && (this.txtCity.Text == "") &&
                (Convert.ToInt32(this.ddlCompany.SelectedValue) > 0) && (this.txtZip.Text == ""))
            {
                lblInvalidSearch.Text = "You Must Enter at Least One Search Field Other than the Date of Loss";
                return false;
            }
            return true;    //valid search data
        }

        protected void btnContinueWithPolicy_Click(object sender, EventArgs e)
        {

            //MedCSX.App_Code.Claim myNewClaim = new MedCSX.App_Code.Claim();
            //myNewClaim.PolicyNumber = gvPoliciesFound.SelectedRow.Cells[1].Text;
            //myNewClaim.Version_No = gvPoliciesFound.SelectedRow.Cells[2].Text;
            //myNewClaim.isExpired = isExpired;
            //myNewClaim.DateOfLoss = txtDateOfLoss.Text;
            //myNewClaim.company = ddlCompany.SelectedValue.ToString();
            //myNewClaim.location = ddlLocation.SelectedValue.ToString();
            //myNewClaim.sub_claim_type_id = Request.QueryString["subClaimTypeID"].ToString();
            //Session["myNewClaim"] = myNewClaim;


            PolicyData myData = new PolicyData();
            int dupCount = myData.InsertDuplicate(gvPoliciesFound.SelectedRow.Cells[1].Text, gvPoliciesFound.SelectedRow.Cells[2].Text, 0, txtDateOfLoss.Text, (int)ModGeneratedEnums.PossibleDuplicateClaimMode.Claim_Created);
            if (dupCount > 0)
            {
                DivPossibleDuplicatesFound.Visible = true;
                GetPossibleDuplicates(gvPoliciesFound.SelectedRow.Cells[1].Text, txtDateOfLoss.Text);

                //string url2 = "PossibleDuplicateClaimsFound.aspx?policyNo=" + gvPoliciesFound.SelectedRow.Cells[1].Text + "&DOL=" + txtDateOfLoss.Text;
                //string win2 = "window.open('" + url2 + "','mywindowPosDups', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');";
                //ClientScript.RegisterStartupScript(this.GetType(), "mywindowPosDups", win2, true);

                //if (Session["CancelCreateClaim"].ToString() == "0")
                //    return;
            }




          
        }

        protected void btnContinueWithoutPolicy_Click(object sender, EventArgs e)
        {
            

            int claimId = this.CreateClaimWithoutPolicy(DateTime.Parse(txtDateOfLoss.Text), ddlCompany.SelectedValue.ToString(), Convert.ToInt32(ddlLocation.SelectedValue), Convert.ToInt32(Request.QueryString["subClaimTypeID"]));

            string url = "claim.aspx?claim_id=" + claimId;
            string win = "window.open('" + url + "','mywindow" + claimId.ToString() +"', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');";
            ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

            //fire claim created event
            Hashtable parms = new Hashtable();
            parms.Add("Claim_Id", claimId);

            int successful = ModMain.FileNoteClaimCreatedEventHandler((int)ModGeneratedEnums.EventType.Claim_Created_Without_Policy, parms);


            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);    //close this form
        }

        public int CreateClaimWithoutPolicy(DateTime dateOfLoss, string company, int location, int subClaimTypeID)
        {
            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
           int createdClaimId = myData.CreateClaim("",0,dateOfLoss, location, true, 5, DateTime.Now, subClaimTypeID);


            //update claim with Display_Claim)Id
            myData.GetCompanyLocationAbbrev(location);

            string companyAbbreviation = company;
            string locationAbreviation = myData.locationAbbreviation;
            string tmpclaimid = createdClaimId.ToString("000000");
            string tmpdisplayname = companyAbbreviation + locationAbreviation + tmpclaimid;

          
            myData.UpdateClaimDisplayName(createdClaimId, tmpdisplayname);
            //update claim display name from above

            return createdClaimId;
        }

        public int CreateClaimWithPolicy(string no_policy, decimal version_no, bool _isExpired, DateTime dateOfLoss, string company, int location, int subClaimTypeID)
        {
            //CreateClaimWithPolicy(ByRef policyNumber As String, ByRef versionNumber As String, ByRef isExpired As Boolean, ByRef dateOfLoss As Date, ByRef Company As String, ByRef location As String, ByRef SubClaimTypeID As Integer, ByRef LossLocationNbr As Integer, Optional ByVal LOB As String = "") As Integer
            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            int createdClaimId = myData.CreateClaim(no_policy, version_no, dateOfLoss, location, false, 5, DateTime.Now, subClaimTypeID);


            //update claim with Display_Claim)Id
            myData.GetCompanyLocationAbbrev(location);

            string companyAbbreviation = company;
            string locationAbreviation = myData.locationAbbreviation;
            string tmpclaimid = createdClaimId.ToString("000000");
            string tmpdisplayname = companyAbbreviation + locationAbreviation + tmpclaimid;
            myData.UpdateClaimDisplayName(createdClaimId, tmpdisplayname);

            myData.RefreshPolicyIndividuals(createdClaimId, no_policy, version_no.ToString());
            myData.RefreshPolicyVehicles(createdClaimId, no_policy, version_no.ToString());
            myData.RefreshPolicyLossPayees(createdClaimId, no_policy, version_no.ToString());
            myData.RefreshPolicyCoverages(createdClaimId, version_no.ToString());

            //update claim display name from above

            return createdClaimId;
        }


        protected void gvPoliciesFound_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedPolNo = gvPoliciesFound.SelectedRow.Cells[1].Text;
            selectedVerNo = gvPoliciesFound.SelectedRow.Cells[2].Text;
            btnContinueWithPolicy.Enabled = true;

            populateGVPolicyVehicles(selectedPolNo, selectedVerNo);
            populateGVPolicyIndividuals(selectedPolNo, selectedVerNo);

            ddlCompany.ClearSelection();
            ddlCompany.Items.FindByText(HelperFunctions.getCompanyIDForPolicy(selectedPolNo)).Selected = true;
            populateLocationsBYCompanyID(ddlCompany.SelectedItem.ToString());
        }

        public void populateGVPolicyVehicles(string selectedPolNo, string selectedVerNo)
        {
            DataTable dt = new DataTable();
            PolicyData myData = new PolicyData();
            dt = myData.GetPolicyVehicles(selectedPolNo, selectedVerNo);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string tmp = dt.Columns[2].ColumnName;  //do your code 
                }
            }

            gvPolicyVehicles.DataSource = dt;
            gvPolicyVehicles.DataBind();
            gvPolicyVehicles.Visible = true;

            gvPolicyVehicles.SelectedIndex = 0;
        }

        public void populateGVPolicyIndividuals(string selectedPolNo, string selectedVerNo)
        {
            DataTable dt = new DataTable();
            PolicyData myData = new PolicyData();
            dt = myData.GetPolicyIndividuals(selectedPolNo, selectedVerNo);

            gvPolicyIndividuals.DataSource = dt;
            gvPolicyIndividuals.DataBind();
            gvPolicyIndividuals.Visible = true;

            gvPolicyIndividuals.SelectedIndex = 0;
        }

        //public int InsertRow(Hashtable row, string table)
        //{
        //    /*Insert row into a table.  row is a dictionary of field names/values.
        //     * Return id of newly inserted row.  If there is an insert event 
        //     * registered for the table in the data dictionary, an insert event
        //     * will be raised
        //    */

        //    //int id = db.InsertRow(row, table, MedCsXSystem.UserId);

        //    ////raise insert event if registered for the table
        //    //GenerateTableInsertEvent(table, id);

        //    //return id;
        //}

        //public int CreateClaimWithPolicy(string policyNo, string versionNo, bool expired, DateTime dateOfLoss, string company, string location, int subClaimTypeID, int lossLocationNo, string LOB = "")
        //{
        //    ArrayList c = new ArrayList();
        //    int claimId,
        //        companyId,
        //        companyLocationId;
        //    Hashtable parms = new Hashtable(),
        //        claim = new Hashtable(),
        //        newClaim,
        //        companyRow,
        //        companyLocation;

        //    companyId = ml.getId("Company", company);
        //    companyLocationId = ml.getId("Company_Location", location, "Description", "Company_Id = " + companyId);

        //    //insert claim record
        //    claim.Add("Policy_No", policyNo);
        //    claim.Add("Version_No", versionNo);
        //    claim.Add("Is_Expired_Policy", expired);
        //    claim.Add("Date_Of_Loss", dateOfLoss);
        //    claim.Add("Company_Location_Id", companyLocationId);
        //    claim.Add("Has_Coverage_Issue", false);
        //    claim.Add("Claim_Status_Id", (int)ModGeneratedEnums.ClaimStatus.Claim_Open);
        //    claim.Add("Date_Reported", ml.DBNow());
        //    claim.Add("Sub_Claim_Type_Id", subClaimTypeID);
        //    claim.Add("Loss_Location_Nbr", lossLocationNo);
        //    claimId = ml.InsertRow(claim, "Claim");

        //    //update claim with Display_Claim)Id
        //    claim = ml.getRow("Claim", claimId);
        //    newClaim = ml.copyDictionary(claim);
        //    companyRow = ml.getRow("Company", companyId);
        //    companyLocation = ml.getRow("Company_Location", companyLocationId);

        //    newClaim["Display_Claim_Id"] = (string)companyRow["Abbreviation"] + (string)companyLocation["Abbreviation"] + claimId.ToString("000000");

        //    /*we don't want a file note inserted for this update.
        //     * it will be handled through a manually entered file note later*/
        //    ml.updateRow("Claim", newClaim, claim, null, false);

        //    //refresh policy data
        //    Claim cl = new Claim(claimId);
        //    cl.RefreshPolicyData();

        //    //set default insured vehicle and insured, owner and drivers
        //    mm.UpdateDefaultVehiclePersons(claimId, MedCsXSystem.UserId);

        //    //fire claim created event
        //    parms.Add("Claim_Id", claimId);
        //    parms.Add("Policy_Number", policyNo);
        //    parms.Add("Version_Number", versionNo);
        //    mm.FireEvent((int)ModGeneratedEnums.EventType.Claim_Created_Using_Policy, parms);

        //    return claimId;
        //}

        protected void GetPossibleDuplicates(string policyNo, string dol)
        {
            DataTable dt = new DataTable();
            MedCSX.App_Code.Claim myClaim = new MedCSX.App_Code.Claim();
            dt = myClaim.GetGetPossibleDuplicateClaims(policyNo);


            if (dt.Rows.Count > 0)
            {
                gvPossibleDuplicateClaims.DataSource = dt;
                gvPossibleDuplicateClaims.DataBind();
            }
            else
            {
                Hashtable parms = new Hashtable();

                int claimId = this.CreateClaimWithPolicy(gvPoliciesFound.SelectedRow.Cells[1].Text, Convert.ToDecimal(gvPoliciesFound.SelectedRow.Cells[2].Text), isExpired, DateTime.Parse(txtDateOfLoss.Text), ddlCompany.SelectedValue.ToString(), Convert.ToInt32(ddlLocation.SelectedValue), Convert.ToInt32(Request.QueryString["subClaimTypeID"]));

                ////open then new claim
                //mf.OpenClaim(claimId, 0, 0, 0, true);
                //tmpString = "javascript:my_window2PD" + id + "=window.open('propertyDamage.aspx?mode=" + mode + "&claim_id=" + claim_id + "&property_damage_id=" + id + "','my_window2PD" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2PD" + id + ".focus()";
                string url = "claim.aspx?claim_id=" + claimId;
                string win = "window.open('" + url + "','mywindow" + claimId.ToString() + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');";
                ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

                parms.Add("Claim_Id", claimId);

                int successful = ModMain.FileNoteClaimCreatedEventHandler((int)ModGeneratedEnums.EventType.Claim_Created_Without_Policy, parms);


                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);    //close this form
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Hashtable parms = new Hashtable();

            int claimId = this.CreateClaimWithPolicy(gvPoliciesFound.SelectedRow.Cells[1].Text, Convert.ToDecimal(gvPoliciesFound.SelectedRow.Cells[2].Text), isExpired, DateTime.Parse(txtDateOfLoss.Text), ddlCompany.SelectedValue.ToString(), Convert.ToInt32(ddlLocation.SelectedValue), Convert.ToInt32(Request.QueryString["subClaimTypeID"]));

            ////open then new claim
            //mf.OpenClaim(claimId, 0, 0, 0, true);
            //tmpString = "javascript:my_window2PD" + id + "=window.open('propertyDamage.aspx?mode=" + mode + "&claim_id=" + claim_id + "&property_damage_id=" + id + "','my_window2PD" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2PD" + id + ".focus()";
            string url = "claim.aspx?claim_id=" + claimId;
            string win = "window.open('" + url + "','mywindow" + claimId.ToString() + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');";
            ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);

            parms.Add("Claim_Id", claimId);

            int successful = ModMain.FileNoteClaimCreatedEventHandler((int)ModGeneratedEnums.EventType.Claim_Created_Without_Policy, parms);


            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);    //close this form
        }

        protected void rblActivePolicies_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!ValidateSearchData())
                return;

            if (rblActivePolicies.SelectedValue == "0")
            {
                DataTable dt = new DataTable();
                PolicyData myData = new PolicyData();


                dt = myData.FindPolicies(false, this.txtDateOfLoss.Text,
                         this.txtPolicyNo.Text, this.txtFirstName.Text, this.txtLastName.Text,
                         this.txtSSN.Text, this.txtAutoYear.Text,
                         this.txtMake.Text, this.txtModel.Text,
                         this.txtVIN.Text, this.txtAddress.Text,
                         this.txtCity.Text, ddlState.SelectedValue.ToString(),
                         this.txtZip.Text);
                gvPoliciesFound.DataSource = dt;
                gvPoliciesFound.DataBind();
                gvPoliciesFound.Visible = true;

                gvPoliciesFound.SelectedIndex = 0;
            }
            else
            {
                DataTable dt = new DataTable();
                PolicyData myData = new PolicyData();


                dt = myData.FindPolicies(true, this.txtDateOfLoss.Text,
                         this.txtPolicyNo.Text, this.txtFirstName.Text, this.txtLastName.Text,
                         this.txtSSN.Text, this.txtAutoYear.Text,
                         this.txtMake.Text, this.txtModel.Text,
                         this.txtVIN.Text, this.txtAddress.Text,
                         this.txtCity.Text, ddlState.SelectedValue.ToString(),
                         this.txtZip.Text);
                gvPoliciesFound.DataSource = dt;
                gvPoliciesFound.DataBind();
                gvPoliciesFound.Visible = true;

                gvPoliciesFound.SelectedIndex = 0;
            }
        }
    }
}
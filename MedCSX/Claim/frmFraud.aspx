﻿<%@ Page  Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmFraud.aspx.cs" Inherits="MedCSX.Claim.frmFraud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-10">
              <asp:GridView ID="grFraud" AutoGenerateSelectButton="true" runat="server" DataKeyNames="id"  AutoGenerateColumns="False" CellPadding="4"    ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ShowFooter="true"
               ForeColor="#333333"   width="100%">
               <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
                 
                  
                     <asp:BoundField DataField="NICBDescription" HeaderText="Fraud Type"/>
                   <asp:BoundField DataField="PersonName" HeaderText="Person"/>
                   <asp:BoundField DataField="VehicleName" HeaderText="Vehicle"/>
                  <asp:BoundField DataField="claim_id" HeaderText="Claim"  />
                  <asp:BoundField DataField="description" HeaderText="Description"/>
                 
               </Columns>
             
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                 <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
               <EditRowStyle BackColor="#999999" />
               <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#E9E7E2" />
               <SortedAscendingHeaderStyle BackColor="#506C8C" />
               <SortedDescendingCellStyle BackColor="#FFFDF8" />
               <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            </asp:GridView>
        </div>
        <div class="col-md-2">
            <asp:LinkButton ID="btnAddFraud" runat="server"  class="btn btn-info btn-xs"  OnClick="btnAddFraud_Click">Add</asp:LinkButton>
            <br /><asp:LinkButton ID="btnDeleteFraud"  class="btn btn-info btn-xs" OnClick="btnDeleteFraud_Click"  runat="server">Delete</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right" CausesValidation="false"  Text="Cancel/Close"  OnClientClick="window.close(); return false;"/>
        </div>
    </div>
</asp:Content>

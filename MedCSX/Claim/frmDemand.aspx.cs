﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmDemand : System.Web.UI.Page
    {
        public bool OKClicked = false;
        private int VendorID = 0;
        private int ReserveID = 0;
        private int m_ClaimId = 0;
        private DateTime LastOfferDate = ModMain.DEFAULT_DATE;

        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();

        private List<ReserveGrid> myDemandHistoryList;

        protected void Page_Load(object sender, EventArgs e)
        {
            VendorID = Convert.ToInt32(Request.QueryString["vendor_id"]);
            ReserveID = Convert.ToInt32(Request.QueryString["reserve_id"]);
            m_ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);

            if (!Page.IsPostBack)
            {
                mm.UpdateLastActivityTime();
                LoadGrid();
            }
        }

        protected void LoadGrid()
        {
            this.grDemandHistory.DataSource = LoadDemandOffer(Demand.DemandHistory(VendorID, ReserveID));
            this.grDemandHistory.DataBind();
        }

        private IEnumerable LoadDemandOffer(List<Hashtable> list)
        {
            string s;
            myDemandHistoryList = new List<ReserveGrid>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    ReserveGrid cg = new ReserveGrid();
                    cg.id = (int)dData["ID"];
                    cg.reserveType = (string)dData["Type"];
                    cg.dtOfLoss = (DateTime)dData["Date"];
                    s = (string)dData["Amount"];
                    s = s.Trim().Replace("$", "").Replace(",", "");
                    cg.netLossReserve = double.Parse(s);
                    if (dData["Received_Date"].ToString() != "")
                        cg.dtReported = (DateTime)dData["Received_Date"];
                    if (dData["Due_Date"].ToString() != "")
                        cg.dtDue = (DateTime)dData["Due_Date"];
                    myDemandHistoryList.Add(cg);
                }
                return myDemandHistoryList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDemandHistoryList;
            }
        }

        public bool IsNumeric(object num, decimal output = 0)
        {
            if (num == null)
                return false;  //num is not initialized

            if ((num.GetType() == typeof(string)) || (num.GetType() == typeof(char)))
                return decimal.TryParse(num.ToString().Replace("$", "").Replace(",", ""), out output);  //num is a string - parse to validate
            else if ((num.GetType() == typeof(Int16)) || (num.GetType() == typeof(Int32)) ||
                (num.GetType() == typeof(Int64)) || (num.GetType() == typeof(Single)) ||
                (num.GetType() == typeof(double)) || (num.GetType() == typeof(decimal)))
            {       //num is numeric
                output = (decimal)num;
                return true;
            }
            else
                return false;  //unknown type, not a number
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            txtDemandAmount.Text = txtDemandAmount.Text.Trim().Replace("$", "").Replace(",", "");

            if ((txtDemandAmount.Text == "") || (IsNumeric(txtDemandAmount.Text) == false))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The demand amount is invalid.');</script>");
                //MessageBox.Show("The demand amount is invalid.", "Invalid Demand Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                txtDemandAmount.Focus();
                return;
            }

            if (LastOfferDate > Convert.ToDateTime(dtpDemandDate.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The demand date must be after the last offer date.');</script>");
                //MessageBox.Show("The demand date must be after the last offer date.", "Invalid Demand Date", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                dtpDemandDate.Focus();
                return;
            }


            //save the new demand
            Demand myDemand = new Demand();
            myDemand.VendorId = this.VendorID;
            myDemand.ReserveId = this.ReserveID;
            myDemand.DemandAmount = Convert.ToDouble(txtDemandAmount.Text);
            myDemand.DemandDate = Convert.ToDateTime(dtpDemandDate.Text);
            myDemand.ReceivedDate = Convert.ToDateTime(dtpReceivedDate.Text);
            myDemand.DueDate = Convert.ToDateTime(this.dtpDueDate.Text);
            myDemand.Update();

            //fire the received demand event
            Hashtable parms = new Hashtable();
            parms.Add("Demand_Amount", myDemand.DemandAmount);
            if (myDemand.VendorId == 0)
                parms.Add("Attorney", myDemand.Reserve.Claimant.Name);  //no vendor selected - use claimant
            else
                parms.Add("Attorney", myDemand.Vendor.Name);
            parms.Add("Claimant", myDemand.Reserve.Claimant.Name);
            parms.Add("Reserve", myDemand.Reserve.ReserveType.Description);
            parms.Add("Due_Date", myDemand.DueDate.ToShortDateString());
            myDemand.Reserve.FireEvent((int)modGeneratedEnums.EventType.Received_Demand, parms);

            ViewClaim vc = new ViewClaim();
            vc.LoadDemandSummary();

            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);



        }
    }
}
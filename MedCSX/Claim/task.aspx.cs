﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;

namespace MedCSX.Claim
{
    public partial class task : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Task";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpid = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpid);

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateControls();

                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            Task myTask = new Task();
            myTask.GetTasksByID(id);
            ddlTaskType.SelectedValue = myTask.user_task_type_id;
            ddlClaimant.SelectedValue = myTask.claimant_id;
            ddlReserve.SelectedValue = myTask.reserve_type_id;
            txtClaim.Text = myTask.display_claim_id;
            txtDescription.Text = myTask.task_description;
           
        }

        protected void PopulateControls()
        {
            DataTable dt = new DataTable();
            dt = HelperFunctions.getTypeTableRows("User_Task_Type", "Description", "1=1");
            ddlTaskType.DataSource = dt;
            ddlTaskType.DataValueField = dt.Columns[0].ColumnName;
            ddlTaskType.DataTextField = dt.Columns[1].ColumnName;
            ddlTaskType.DataBind();

            DataTable dtc = new DataTable();
            Claimants myData = new Claimants();
            dtc = myData.GetClaimantsForClaim(claim_id);
            ddlClaimant.DataSource = dtc;
            ddlClaimant.DataValueField = dtc.Columns[0].ColumnName;
            ddlClaimant.DataTextField = dtc.Columns[48].ColumnName;
            ddlClaimant.DataBind();

            DataTable dtr = new DataTable();
            Reserve myDatar = new Reserve();
            dtr = myDatar.GetReserveBySubClaimType(Convert.ToInt32(tmpSub_claim_type_id));
            ddlReserve.DataSource = dtr;
            ddlReserve.DataValueField = dtr.Columns[0].ColumnName;
            ddlReserve.DataTextField = dtr.Columns[1].ColumnName;
            ddlReserve.DataBind();

        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmReserveAssignmentHistory : System.Web.UI.Page
    {
        public bool okPressed = false;  //was OK button pressed?

        private MedCsxLogic.Claim m_Claim = null;
        private Reserve m_Reserve = null;
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private int reserve_id = 0;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            reserve_id = Convert.ToInt32(Request.QueryString["reserve_id"]);
            this.m_Reserve = new Reserve(reserve_id);
            this.lblClaim.Text = m_Reserve.Claim.DisplayClaimId;
            this.lblReserve.Text = m_Reserve.Description();
            this.lblAssignedTo.Text = m_Reserve.AssignedToDescription();

            LoadGrid();
        }

        protected void LoadGrid()
        {
            grHistory.DataSource = mm.GetReserveHistoryAssignment(reserve_id);
            grHistory.DataBind();


        }
    }
}
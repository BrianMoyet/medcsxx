﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmCancelAppraisals : System.Web.UI.Page
    {
        private AppraisalTask m_appraisalTask;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_appraisalTask = new AppraisalTask(Convert.ToInt32(Request.QueryString["appraisal_task_id"]));

            this.lblAppraiserId.Text = this.m_appraisalTask.SceneAccessAppraiser.Vendor.Name;
            this.lblClaimId.Text = this.m_appraisalTask.Claim().DisplayClaimId;
            this.lblVehicleId.Text = this.m_appraisalTask.Vehicle().Name;

            if (Session["referer2"] != null)
            {
                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Are you sure you want to cancel this appraisal?", "Confirm Cancel", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
            //    return;

            if (string.IsNullOrWhiteSpace(this.txtCancellationReason.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reason for Cancellation is a required entry.');</script>");
                //MessageBox.Show("Reason for Cancellation is a required entry.", "Missing Reason", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                Status("Cancellation reason missing.");
                this.txtCancellationReason.Focus();
                return;
            }

            Status("Cancelling Appraisal");     //set status of appraisal to cancelled
            m_appraisalTask.AppraisalTaskStatusId = (int)modGeneratedEnums.AppraisalTaskStatus.Appraisal_Cancelled;
            m_appraisalTask.Update();

            //build message
            string msg = "Appraisal cancelled for " + this.lblVehicleId.Text + Environment.NewLine +
                "Appraiser: " + this.lblAppraiserId.Text + Environment.NewLine +
                "Date assigned: " + this.m_appraisalTask.CreatedDate.ToShortDateString() + Environment.NewLine +
                "Cancelled by: " + ml.CurrentUser.Name + " on " + ml.DBNow().ToShortDateString() + " " + ml.DBNow().ToShortTimeString() + Environment.NewLine +
                "Reason for cancellation: " + this.txtCancellationReason.Text;

            //create file note
            if ((bool)this.ckCreateFileNote.Checked)
            {
                Status("Creating File Note");
                Hashtable parms = new Hashtable();
                parms.Add("FileNoteText", msg);
                this.m_appraisalTask.Claim().FireEvent((int)modGeneratedEnums.EventType.Appraisal_Cancelled, parms);
            }

            //create email
            if ((bool)this.ckEmailAppraiser.Checked)
            {
                Status("Sending Email");
                this.m_appraisalTask.SendAppraisalCancellationEmail(msg);
            }

            Status("Done");

            Session["referer2"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);

            
        }

        private void Status(string p)
        {
            //this.lblStatus.Text = p;
            //this.lblStatus.InvalidateArrange();
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteModal.Master"  CodeBehind="frmAppraisalRequest.aspx.cs" Inherits="MedCSX.Claim.frmAppraisalRequest" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="row">
            <div class="col-md-2">
                <asp:ListView ID="lvTasks" runat="server"  DataKeyNames="id">
                     <ItemTemplate>  
               
                  <%--  <asp:Label ID="id" runat="server"   
                        Text='<%#Eval("Id") %>' />  
                    <br />--%>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImageIdx") %>' Height="40px" runat="server" />
                    <br /><asp:Label ID="Description" runat="server"   
                        Text='<%#Eval("description") %>' />  
                   
                   
               
            </ItemTemplate>  

                </asp:ListView>
                
            </div>
            <div class="col-md-10">
                  <div class="row">
                    <div class="col-md-12">
                        <b>General</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Claim: <asp:Label ID="txtClaimId" runat="server" Text=""></asp:Label>&nbsp;&nbsp;&nbsp; Policy: <asp:Label ID="txtPolicyNo" runat="server" Text=""></asp:Label>
                       
                        <div class="row">
                            <div class="col-md-4">
                                    Type of Claim: 
                            </div>
                            <div class="col-md-8">
                                    <asp:TextBox ID="txtClaimType" runat="server"></asp:TextBox>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-4">
                                   Investigation Type: 
                            </div>
                            <div class="col-md-8">
                                <asp:DropDownList ID="cboInvestigationType" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                   Appraiser: 
                            </div>
                            <div class="col-md-8">
                                <asp:DropDownList ID="cboAppraiser" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12">
                                   Task Description: 
                                <br />
                                <asp:TextBox ID="txtTaskDescrip" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6"> <%--"Third" Column--%>
                        DOL: <asp:Label ID="txtDtLoss" runat="server" Text=""></asp:Label> &nbsp;&nbsp;&nbsp;Deductible: <asp:Label ID="txtDeductible" runat="server" Text="Label"></asp:Label>
                        <br />Special Instructions:
                        <br /><asp:TextBox ID="txtInstructions" runat="server" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        <br />Point of Impact / Loss Description 
                        <br /><asp:TextBox ID="txtLossDescrip" runat="server" TextMode="MultiLine" Width="300px"></asp:TextBox>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12">
                            Initial Estimate: <asp:TextBox ID="txtInitialEst" Text="0" TextMode="Number" step=".01" runat="server"></asp:TextBox>&nbsp;<asp:TextBox ID="dtInitialEst" runat="server" TextMode="DateTimeLocal"></asp:TextBox>&nbsp;&nbsp;Total Estimate: <asp:Label ID="lblTtlEstimate" runat="server"></asp:Label>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-4">
                        <b>Insurance Company</b>
                        <br /><asp:Label ID="txtCompany" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <b>Insured</b>
                        <br /><asp:Label ID="txtInsured" runat="server" Text="Label"></asp:Label>
                        <br />
                        <asp:Button ID="btnEditInsured"  class="btn btn-info btn-xs"  runat="server" Text="Edit Insured" OnClick="btnEditInsured_Click" CausesValidation="false"/>
                    </div>
                    <div class="col-md-4">
                        <b>Claimant</b>
                        <br /><asp:Label ID="txtClaimant" runat="server" Text="Label"></asp:Label>
                         <br />
                        <asp:Button ID="btnEditClaimant"  class="btn btn-info btn-xs" runat="server" Text="Edit Claimant" OnClick="btnEditClaimant_Click" CausesValidation="false" />
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-4">
                        <b>Vehicle</b>
                        <br /><asp:Label ID="txtVehicle" runat="server" Text="Label"></asp:Label>
                         <br />
                        <asp:Button ID="btnEditVehicle" runat="server"  class="btn btn-info btn-xs"  Text="Edit Vehicle" OnClick="btnEditVehicle_Click" CausesValidation="false" />
                    </div>
                    <div class="col-md-4">
                        <b>Location of Vehicle</b>
                        <br /><asp:Label ID="txtVehicleLoc"    runat="server" Text="Label"></asp:Label>
                        <br />
                        <asp:Button ID="btnEditVehicleLoc"  class="btn btn-info btn-xs"  runat="server" Text="Edit Vehicle Location" OnClick="btnEditVehicleLoc_Click" CausesValidation="false" />
                    </div>
                    <div class="col-md-4">
                   
                    </div>
                </div>
            </div>
        </div>
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
    <div class="row">
         <div class="col-md-12">
             <hr />
         </div>
    </div>
<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
     <div class="row">
         <div class="col-md-12">
               
               <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="Submit Request" OnClick="btnOK_Click"/>
               <asp:Button ID="btnResend" runat="server" class="btn btn-info btn-xs"  Text="Resend Request" OnClick="btnResend_Click" /> 
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
  </asp:Content>
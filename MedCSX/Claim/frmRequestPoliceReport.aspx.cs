﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmRequestPoliceReport : System.Web.UI.Page
    {
        #region Class Variables
        private MedCsxLogic.Claim m_Claim = null;
        private string otherDesc = "";      //other report type description
        private int m_vehicleId = 0;        //selected vehicle ID
        int m_ClaimId = 0;

        //List<cboGrid> myAgencyList = null;
        //List<cboGrid> myReportList = null;
        List<VehicleGrid> myVehicleList = null;
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        #endregion

        #region Properties
        public MedCsxLogic.Claim Claim
        {
            get { return this.m_Claim; }
            set { this.m_Claim = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            m_ClaimId = Convert.ToInt32(Request.QueryString["claim_id"]);
           
            this.Claim = new MedCsxLogic.Claim(m_ClaimId);

            this.Page.Title = "Request Police Report";
            LoadComboBoxes();
            //LoadData();
        }

        //private void LoadData()
        //{
        //    int counter = 0;
        //    foreach (Claimant cl in this.Claim.Claimants)
        //    {
        //        if ((cl.PersonId != this.Claim.InsuredPersonId) && (cl.Person.FirstName != "Unknown"))
        //        {
        //            if ((cl.Person.FirstName != this.txtPrimaryFirstName.Text) && (cl.Person.LastName != this.txtPrimaryLastName.Text))
        //            {   //fill in 2nd & 3rd party people
        //                if (counter == 0)
        //                {
        //                    this.txt2ndFirstName.Text = cl.Person.FirstName;
        //                    this.txt2ndLastName.Text = cl.Person.LastName;
        //                    counter++;
        //                }
        //                else if ((cl.Person.FirstName != this.txt2ndFirstName.Text) && (cl.Person.LastName != this.txt2ndLastName.Text))
        //                {
        //                    this.txt3rdFirstName.Text = cl.Person.FirstName;
        //                    this.txt3rdLastName.Text = cl.Person.LastName;
        //                    break;
        //                }
        //            }
        //        }
        //    }

        //    //load loss location 
        //    this.txtLocCity.Text = this.Claim.LossCity;
        //    this.txtLocStreet.Text = this.Claim.LossLocation;
        //    this.txtLocCounty.Text = "";
        //    this.cboLocState.SelectedIndex = this.Claim.LossState_Id;

        //    this.txtReportNumber.Text = this.Claim.PoliceReportNumber;      //load report number

        //    this.otherDesc = (new ChoicePointReportType((int)modGeneratedEnums.ChoicePointReportType.Other_See_Other_Description + 1)).Description;
        //    this.txtReportTypeOther.IsEnabled = false;
        //}

        private void LoadComboBoxes()
        {
            this.cboReportType.DataSource = LoadReportType(MedCsXTable.GetTableContents("Choicepoint_Report_Type"));   //load report types
            this.cboReportType.DataBind();
            this.cboAgencyType.DataSource = LoadReportType(MedCsXTable.GetTableContents("Investigating_Agency_Type"), 1);  //load investigating agency types
            this.cboAgencyType.DataBind();
            mf.LoadStateComboBox(this.cboLocState);     //load states
            LoadVehicles();
        }

        private void LoadVehicles()
        {
            this.cboInsuredVehicle.DataSource = mm.LoadVehicles(this.Claim.VehicleNames, 1);
            this.cboInsuredVehicle.DataTextField = "vName";
            this.cboInsuredVehicle.DataValueField = "vehicleId";
            this.cboInsuredVehicle.DataBind();

        }

        private void RemoveUnknowns(List<VehicleGrid> list)
        {
            List<VehicleGrid> rtn = new List<VehicleGrid>();
            foreach (VehicleGrid v in list)
            {
                if ((v.vName.Length < 7) || ((v.vName.Length >= 7) && (v.vName.Substring(0, 7).ToUpper() != "UNKNOWN")))
                    rtn.Add(v);
            }
            this.myVehicleList = rtn;
        }

        private IEnumerable LoadReportType(List<Hashtable> list, int mode = 0)
        {
            string table = "ChoicePoint_Report_Type";
            List<cboGrid> myTypeList = new List<cboGrid>();

            if (mode == 1)
                table = "Investigating_Agency_Type";
            try
            {
                foreach (Hashtable tData in list)
                {
                    cboGrid cg = new cboGrid();
                    cg.id = (int)tData[table + "_Id"];
                    cg.type = (string)tData[table + "_Code"];
                    cg.description = (string)tData["Description"];
                    cg.imageIdx = (int)tData["Image_Index"];
                    myTypeList.Add(cg);
                }
                return myTypeList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myTypeList;
            }

            //if (mode == 1)
            //{
            //    myAgencyList = myTypeList;
            //    return myAgencyList;
            //}
            //else
            //{
            //    myReportList = myTypeList;
            //    return myReportList;
            //}
        }
    }
}
﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmBICalculations : System.Web.UI.Page
    {
        #region Class Variables
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        private BIEvaluation _BIEvaluation;
        private bool badNum = false;
        public int injuredId = 0;
        private Injured m_injured;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            injuredId = Convert.ToInt32(Request.QueryString["injured_id"]);
            this.m_injured = new Injured(this.injuredId);
            this._BIEvaluation = m_injured.BIEvaluation();

            if (!Page.IsPostBack)
            {
                //try
                //{
                    this.Title = this._BIEvaluation.Claim().DisplayClaimId + " - BI Calculations for " + this._BIEvaluation.Injured().Person().Name;

                    this.txtUnadjLowAmbulance.Text = this._BIEvaluation.AmbulanceBestCase.ToString();
                    this.txtUnadjHighAmbulance.Text = this._BIEvaluation.AmbulanceWorstCase.ToString();
                    this.txtUnadjLowFutureMed.Text = this._BIEvaluation.FutureMedicalBestCase.ToString();
                    this.txtUnadjHighFutureMed.Text = this._BIEvaluation.FutureMedicalWorstCase.ToString();
                    this.txtUnadjLowGenDamages.Text = this._BIEvaluation.GeneralDamagesBestCase.ToString();
                    this.txtUnadjHighGenDamages.Text = this._BIEvaluation.GeneralDamagesWorstCase.ToString();
                    this.txtUnadjLowWageLoss.Text = this._BIEvaluation.WageLossBestCase.ToString();
                    this.txtUnadjHighWageLoss.Text = this._BIEvaluation.WageLossWorstCase.ToString();

                    this.lblTtlBestCaseDiag.Text = this._BIEvaluation.TotalBestCaseDiagnostics.ToString();
                    this.lblTtlBestCaseTreat.Text = this._BIEvaluation.TotalBestCaseTreatments.ToString();
                    this.lblTtlWorstCaseDiag.Text = this._BIEvaluation.TotalWorstCaseDiagnostics.ToString();
                    this.lblTtlWorstCaseTreat.Text = this._BIEvaluation.TotalWorstCaseTreatments.ToString();

                    this.txtGenBestCaseNegligence.Text = this._BIEvaluation.ComparativeBestCase.ToString();
                    this.txtGenWorstCaseNegligence.Text = this._BIEvaluation.ComparativeWorstCase.ToString();
                    this.txtGenDamages.Text = this._BIEvaluation.ProbabilityDamage.ToString();
                    this.txtGenLiability.Text = this._BIEvaluation.ProbabilityLiability.ToString();

                   
                //}
                //catch (Exception ex)
                //{

                //}

                this.DoCalculations();
            }
        }

        private void DoCalculations()
        {
            mm.UpdateLastActivityTime();

            double unadjLow = this.BestCaseTotal();
            double unadjHigh = this.WorstCaseTotal();

            this.lblTtlUnadjLossLow.Text = unadjHigh.ToString();
            this.lblUnadjLossLow.Text = this.lblTtlUnadjLossLow.Text;
            this.lblUnadjLossLow2.Text = this.lblTtlUnadjLossLow.Text;

            this.lblTtlUnadjLossHigh.Text = unadjLow.ToString();
            this.lblUnadjLossHigh.Text = this.lblTtlUnadjLossHigh.Text;

            double unadjExpectValue = ((Convert.ToDouble(lblUnadjLossHigh.Text) - Convert.ToDouble(lblUnadjLossLow.Text)) * (1 - double.Parse(this.txtGenDamages.Text))) + Convert.ToDouble(lblUnadjLossLow2.Text);
            this.lblUnadjExpValue.Text = unadjExpectValue.ToString();
            this.lblUnadjExpectedValue.Text = unadjExpectValue.ToString();
            this.lblUnadjExpectedValue2.Text = this.lblUnadjExpValue.Text;
            this.lblUnadjExpectedValue3.Text = unadjExpectValue.ToString();

            double adjLow = double.Parse(this.txtGenBestCaseNegligence.Text) * unadjExpectValue;
            this.lblAdjLossLow.Text = adjLow.ToString();
            this.lblAdjLossLow2.Text = this.lblAdjLossLow.Text;
            this.lblAdjLossLow3.Text = this.lblAdjLossLow.Text;

            double adjHigh = double.Parse(this.txtGenWorstCaseNegligence.Text) * adjLow;
            this.lblAdjLossHigh.Text = adjHigh.ToString();
            this.lblAdjLossHigh2.Text = this.lblAdjLossHigh.Text;

            double adjExpectValue = (adjHigh - adjLow) * (1 - double.Parse(this.txtGenLiability.Text)) + adjLow;
            this.lblAdjExpValue.Text = adjExpectValue.ToString();
            this.lblAdjExpectedValue.Text = this.lblAdjExpValue.Text;

            this.lblUnadjLossRange.Text = this.lblTtlUnadjLossLow.Text + " - " + this.lblTtlUnadjLossHigh.Text;
            this.lblAdjLossRange.Text = this.lblAdjLossLow.Text + " - " + this.lblAdjLossHigh.Text;
        }

        private double WorstCaseTotal()
        {
            double ret = 0;
            ret += double.Parse(this.txtUnadjLowGenDamages.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(this.txtUnadjLowWageLoss.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(this.txtUnadjLowAmbulance.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(this.txtUnadjLowFutureMed.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(((string)this.lblTtlBestCaseDiag.Text).Replace("$", "").Replace(",", ""));
            ret += double.Parse(((string)this.lblTtlBestCaseTreat.Text).Replace("$", "").Replace(",", ""));
            return ret;
        }

        private double BestCaseTotal()
        {
            double ret = 0;
            ret += double.Parse(this.txtUnadjHighGenDamages.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(this.txtUnadjHighWageLoss.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(this.txtUnadjHighAmbulance.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(this.txtUnadjHighFutureMed.Text.Replace("$", "").Replace(",", ""));
            ret += double.Parse(((string)this.lblTtlWorstCaseDiag.Text).Replace("$", "").Replace(",", ""));
            ret += double.Parse(((string)this.lblTtlWorstCaseTreat.Text).Replace("$", "").Replace(",", ""));
            return ret;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
        }

        protected void txtBox_TextChanged(object sender, EventArgs e)
        {
            string userEntry = ((TextBox)sender).Text;
            bool isNumber = false;
            double thisNumber = 0;

            if (userEntry == "")
                userEntry = "0";
            


            if (modLogic.isNumeric(userEntry))
            {
               

                isNumber = true;
                badNum = false;
                thisNumber = double.Parse(userEntry.Replace("$", "").Replace(",", ""));
            }
            else if (modLogic.isNumeric("0" + userEntry))
            {
                isNumber = true;
                badNum = false;
                thisNumber = double.Parse(userEntry.Replace("$", "").Replace(",", ""));
            }

            if (!isNumber)
            {
                badNum = true;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a valid number.');</script>");
                
                ((TextBox)sender).Focus();
            }
            else if (!badNum)
            {
                ((TextBox)sender).Text = thisNumber.ToString();
                DoCalculations();
            }

           
        }

      

        protected void txtGenDamages_TextChanged(object sender, EventArgs e)
        {
            string userEntry = ((TextBox)sender).Text;
            bool isNumber = false;
            double thisNumber = 0;

            if (userEntry == "")
                userEntry = "0.0";

            if (modLogic.isNumeric(userEntry))
            {
                isNumber = true;
                badNum = false;
                thisNumber = double.Parse(userEntry.Replace("$", "").Replace(",", ""));
            }
            else if (modLogic.isNumeric("0" + userEntry))
            {
                isNumber = true;
                badNum = false;
                thisNumber = double.Parse(userEntry.Replace("$", "").Replace(",", ""));
            }

            if (!isNumber)
            {
                badNum = true;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter a valid number.');</script>");
                ((TextBox)sender).Focus();
            }
            else if (!badNum)
            {
                ((TextBox)sender).Text = thisNumber.ToString("0.00");
                DoCalculations();
            }
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmInjuredDetails : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        public int injuredId = 0;       //Output parameter
        public bool okPressed = false;  //was OK button pressed? Output parameter

        private bool doneLoading = false,   //form done loading?
            m_InItemCheck = false;
        private int claimId = 0,
            personId = 0,
            selectedUserId = 0,
            selectedUserGroupId = 0;
        private Hashtable oldRow = new Hashtable(),
            newRow = new Hashtable(),
            oldCoverage = new Hashtable(),
            newCoverage = new Hashtable();
        private ArrayList persons = new ArrayList(),
            deletedBodyPartsIds = new ArrayList(),
            deletedLeinIds = new ArrayList();
        private MedCsxLogic.Claim m_claim;
        private Injured m_injured;

        private string[] myAssignedUsers;
        private string[] myAssignedUserGroups;
        private List<InjuredReserves> myReserveList;
        private List<InjuryTable> myInjuryCauseList;
        private List<InjuryTable> myInjuryTypeList;
        private List<InjuryTable> myInjuredParts;
        private List<LienData> myLienList;
        private List<ClaimantGrid> myInjuredList;
        public string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            injuredId = Convert.ToInt32(Request.QueryString["injured_id"]);

            this.personId = Convert.ToInt32(Request.QueryString["person_id"]);

            m_claim = new MedCsxLogic.Claim(claimId);

            if (Session["referer2"] != null)
            {

                referer = Session["referer2"].ToString();
                //Session["referer2"] = null;

            }
            else
                referer = Session["referer"].ToString();

            if (injuredId == 0)
            {
                this.m_injured = new Injured();     //create new injured person object
                this.btnLienEval.Visible = false;     
            }
            else
            { 
                this.m_injured = new Injured(this.injuredId);
                this.cboPersonSelect.Enabled = false;
            }
            if (this.injuredId == 0)
                this.Page.Title = "Claim " + this.Claim.DisplayClaimId + " - Add Injured";
            else
                this.Page.Title = "Claim " + this.Claim.DisplayClaimId + " - Edit Injured";

            if (!Page.IsPostBack)
            {


                if (this.injuredId == 0)
                {
                    this.Page.Title = "Claim " + this.Claim.DisplayClaimId + " - Add Injured";
                    btnLienAdd.Enabled = false;
                    btnLienDelete.Enabled = false;
                    btnLienEdit.Enabled = false;
                }
                else
                { 
                    this.Page.Title = "Claim " + this.Claim.DisplayClaimId + " - Edit Injured";
                    btnUpdateReserves.Visible = true;
                }

                //this.LoadAssignTo();

                grReserves.DataSource = LoadReserves(mm.GetAllowedCoverage(this.claimId));
                grReserves.DataBind();
                //grReserves.SelectedIndex = 0;


                //mf.LoadTypeTableComboBox(cboInjuryCause, "Injury_Cause", true);
                //mf.LoadTypeTableComboBox(cboInjuryType, "Injury_Type", true);
                cboInjuryCause.DataSource = TypeTable.getTypeTableRowsDDL("Injury_Cause");
                cboInjuryCause.DataTextField = "description";
                cboInjuryCause.DataValueField = "id";
                cboInjuryCause.DataBind();

                cboInjuryType.DataSource = TypeTable.getTypeTableRowsDDL("Injury_Type");
                cboInjuryType.DataTextField = "description";
                cboInjuryType.DataValueField = "id";
                cboInjuryType.DataBind();

                this.cboInjuryCause.SelectedValue = "0";
                this.cboInjuryType.SelectedValue = "0";


                grInjuryPart.DataSource = TypeTable.getTypeTableRowsDDL("Body_Part");
                grInjuryPart.DataBind();

               

                if (this.injuredId != 0)
                {   //edit mode - load injured details
                    cboInjuryCause.SelectedValue = m_injured.InjuryCauseId.ToString();
                    cboInjuryType.SelectedValue = m_injured.InjuryTypeId.ToString();

                    personId = this.Injured.PersonId;
                    this.txtInjuryDescription.Text = this.Injured.ExtentOfInjury;
                    this.cboInjuryCause.SelectedValue = this.Injured.InjuryCauseId.ToString();
                   this.cboInjuryType.SelectedValue = this.Injured.InjuryTypeId.ToString();

                    
                    LoadParts(this.Injured.InjuredBodyParts());

                    this.DisplayLiens();        //display the liens

                    //select coverages
                    foreach (InjuredCoverage ic in this.Injured.InjuredCoverages())
                    {
                        if (ic.CoverageSpecified)
                        {
                            foreach (InjuredReserves res in myReserveList)
                            {
                                if (ic.ReserveType().Description == res.coverageType)
                                {
                                    res.hasCoverage = true;
                                    res.assignedUser = ic.Reserve.AssignedToDescription();

                                    for (int i = 0; i < grReserves.Rows.Count; i++)
                                    {
                                        if (grReserves.Rows[i].Cells[1].Text == res.coverageType)
                                        {
                                            CheckBox mycheck = ((CheckBox)grReserves.Rows[i].FindControl("cbSelect"));
                                            mycheck.Checked = true;
                                            grReserves.Rows[i].Cells[2].Text = res.assignedUser;
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            foreach (InjuredReserves res in myReserveList)
                            {
                                if (ic.ReserveType().Description == res.coverageType)
                                {
                                    res.hasCoverage = false;
                                    res.assignedUser = "";
                                }
                            }
                        }
                    }

                   

                }

                this.RefreshPersons();      //fill the combo box with the people on this claim
                this.gbInjuryDetails.Visible = true;

                doneLoading = true;
            }


            if (Request.QueryString["control"] != null)
            {
                switch (Request.QueryString["control"].ToString())
                {
                    case "cboPersonSelect":
                        cboPersonSelect.SelectedValue = Request.QueryString["personid"].ToString();
                        cboPersonSelect_SelectedIndexChanged(null, null);
                        break;

                    
                    default:
                        break;
                }
            }
        }

        private void RefreshPersons()
        {
            myInjuredList = mm.LoadClaimants(this.Claim.PersonNamesAndTypes, 2);
            this.cboPersonSelect.DataSource = myInjuredList;
            this.cboPersonSelect.DataTextField = "fullName";
            this.cboPersonSelect.DataValueField = "claimantId";
            this.cboPersonSelect.DataBind();


            if (this.personId == 0)
            {   //no person is selected
                this.cboPersonSelect.SelectedIndex = 0;
                personId = Convert.ToInt32(cboPersonSelect.SelectedValue);
            }
            else
                this.cboPersonSelect.SelectedIndex = SelectCboIndex(this.personId, myInjuredList, 1);

            Person p = new Person(personId);    //create the person object
            this.txtPersonDetails.Text = p.ExtendedDescription().Replace("\r\n", "<br>"); ;

            //don't allow edit of Unknown
            if (this.cboPersonSelect.Text.Trim().Split(' ')[0] == "Unknown")
                this.btnPersonEdit.Enabled = false;
            else
                this.btnPersonEdit.Enabled = true;
        }

        private Injured Injured
        {
            get { return m_injured; }
            set { m_injured = value; }
        }

        private void LoadParts(List<Hashtable> list)
        {
            foreach (Hashtable pData in list)
            {
                    for (int i = 0; i < grInjuryPart.Rows.Count; i++)
                    {
                    string tmp = (string)pData["Description"];
                        if (grInjuryPart.Rows[i].Cells[1].Text == tmp)
                        {
                            CheckBox mycheck = ((CheckBox)grInjuryPart.Rows[i].FindControl("cbSelect"));
                            mycheck.Checked = true;
                           
                        }
                    }

               
                   
            }
        }

        //private IEnumerable LoadParts(List<Hashtable> list)
        //{
        //    myInjuredParts = new List<InjuryTable>();
        //    try
        //    {
        //        foreach (Hashtable pData in list)
        //        {
        //            myInjuredParts.Add(new InjuryTable()
        //            {
        //                description = (string)pData["Description"],
        //                injuryId = (int)pData["ID"]
        //            });
        //        }
        //        return myInjuredParts;
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //        return myInjuredParts;
        //    }
        //}

        private int SelectCboIndex(int p, IEnumerable list, int mode = 0, string s = "")
        {
            switch (mode)
            {
                case 1:
                    List<ClaimantGrid> myClaimants = (List<ClaimantGrid>)list;
                    for (int cboIdx = 0; cboIdx < myClaimants.Count; cboIdx++)
                    {
                        if (p == myClaimants[cboIdx].claimantId)
                            return cboIdx;
                    }
                    break;
                case 2:
                case 3:
                    int userGrpIdx = 0;
                    int userGrpId = 0;
                    foreach (Hashtable ugData in mm.UserGroupsWhichCanBeAssignedClaims())
                    {
                        if (s.Trim() == ((string)ugData["Description"]).Trim())
                        {
                            userGrpId = (int)ugData["User_Group_Id"];
                            break;
                        }
                        userGrpIdx++;
                    }
                    if (mode == 3)
                        return userGrpId;
                    return userGrpIdx;
                case 4:
                case 5:
                    int userIdx = 0;
                    int usrId = 0;
                    foreach (Hashtable uData in mm.UsersWhichCanBeAssignedClaims())
                    {
                        string s2 = ((string)uData["First_Name"]).Trim() + " " + ((string)uData["Last_Name"]).Trim();
                        if (s2.Trim() == s.Trim())
                        {
                            usrId = (int)uData["User_Id"];
                            break;
                        }
                        userIdx++;
                    }
                    if (mode == 5)
                        return usrId;
                    return userIdx;
                default:    //case 0
                    List<InjuryTable> myList = (List<InjuryTable>)list;
                    for (int cboIdx = 0; cboIdx < myList.Count; cboIdx++)
                    {
                        if (p == myList[cboIdx].injuryId)
                            return cboIdx;
                    }
                    break;
            }
            return 0;
        }

        private void DisplayLiens()
        {
            if (this.injuredId != 0)
            {
                this.grLiens.DataSource = LoadLiens(this.m_injured.Liens());
                this.grLiens.DataBind();

                if (grLiens.Rows.Count > 0)
                {
                    grLiens.SelectedIndex = 0;
                }
            }
        }

        private IEnumerable LoadLiens(List<Hashtable> func)
        {
            myLienList = new List<LienData>();
            try
            {
                foreach (Hashtable lData in func)
                {
                    myLienList.Add(new LienData()
                    {
                        lienId = (int)lData["Injured_Lien_Id"],
                        vendorName = (string)lData["Name"],
                        vendorDescription = (string)lData["Description"],
                        lienAmount = (double)(decimal)lData["Amount"],
                        vendorId = (int)lData["Vendor_Id"]
                    });
                }

                //add the old lien system's liens to the list
                if (this.Injured.AttorneyVendorId != 0)
                {
                    Vendor attorneyVendor = new Vendor(this.Injured.AttorneyVendorId);  //get the vendor

                    //create the lien
                    myLienList.Add(new LienData()
                    {
                        lienId = -1,
                        vendorName = attorneyVendor.Name,
                        vendorDescription = attorneyVendor.VendorSubType().Description,
                        lienAmount = this.Injured.MedicalLeinAmount,
                        vendorId = attorneyVendor.Id
                    });
                }
                return myLienList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myLienList;
            }
        }

        private IEnumerable LoadReserves(List<Hashtable> list)
        {
            myReserveList = new List<InjuredReserves>();
            foreach (Hashtable res in list)
            {
                myReserveList.Add(new InjuredReserves()
                {
                    hasCoverage = false,
                    coverageType = (string)res["coverage"],
                    assignedUser = "",
                    assignedUserId = 0
                });
            }
            return myReserveList;
        }

        //private void LoadAssignTo()
        //{
        //    this.cboUser.DataSource = LoadUsers(mm.UsersWhichCanBeAssignedClaims());
        //    this.cboUser.DataBind();
        //    this.cboUserGroup.DataSource = LoadUserGroups(mm.UserGroupsWhichCanBeAssignedClaims());
        //    this.cboUserGroup.DataBind();
        //}

        private IEnumerable LoadUserGroups(List<Hashtable> list)
        {
            var listOfStrings = new List<string>();
            myAssignedUserGroups = listOfStrings.ToArray();
            string s;
            try
            {
                foreach (Hashtable uData in list)
                {
                    listOfStrings.Add(((string)uData["Description"]).Trim());
                }
                myAssignedUserGroups = listOfStrings.ToArray();
                return myAssignedUserGroups;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAssignedUserGroups;
            }
        }

        private IEnumerable LoadUsers(List<Hashtable> list)
        {
            var listOfStrings = new List<string>();
            myAssignedUsers = listOfStrings.ToArray();
            string s;
            try
            {
                foreach (Hashtable uData in list)
                {
                    s = "";
                    s += ((string)uData["First_Name"]).Trim();
                    s += " " + ((string)uData["Last_Name"]).Trim();
                    s += " " + ((string)uData["User_Type"]).Trim();
                    s += " " + ((string)uData["User_Sub_Type"]).Trim();
                    listOfStrings.Add(s);
                }
                myAssignedUsers = listOfStrings.ToArray();
                return myAssignedUsers;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAssignedUsers;
            }
        }

        public string FormatPopupInjured(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2Liens" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claimId + "&id=0,'my_window2Liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Liens" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2Liens" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claimId + "&witness=" + id + "','my_window2Liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Liens" + id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2PD" + id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0,'my_window2PD" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2PD" + id + ".focus()";

            return tmpString;
        }

        public string FormatPopupReserve(string id, string mode)
        {
            string tmpString = ""; 
            if (mode == "c")
            {
                tmpString = "javascript:my_window2Reserve" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claimId + "&id=0,'my_window2Reserve" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Reserve" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2Resere" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claimId + "&resere=" + id + "','my_window2Reserve" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2Reserve" + id + ".focus()";
            }

           

            return tmpString;
        }

        public string FormatPopupInjuredBodyPart(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('injuredBodyPart.aspx?mode=" + mode + "&claim_id=" + claimId + "&id=0,'my_window2injuredBodyPart" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2injuredBodyPart" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('injuredBodyPart.aspx?mode=" + mode + "&claim_id=" + claimId + "&injuredBodyPart=" + id + "','my_window2injuredBodyPart" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2injuredBodyPart" + id + ".focus()";
            }



            return tmpString;
        }

        public string FormatPopupLiens(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claimId + "&liens=0,'my_window2liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2liens" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2InjuredBodyPart" + id + "=window.open('liens.aspx?mode=" + mode + "&claim_id=" + claimId + "&liens=" + id + "','my_window2liens" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no');my_window2liens" + id + ".focus()";
            }



            return tmpString;
        }

       
        protected void btnInjuredBodyPartDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnLiensDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnPersonNew_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();


            Session["referer2"] = Request.Url.ToString();

            string url = "frmPersonDetails.aspx?mode=u&person_type_id=10&claim_id=" + this.claimId + "&person_id=0&control=cboPersonSelect";
            string s = "window.open('" + url + "', 'popup_windowIP', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        }

        protected void btnPersonEdit_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Session["referer2"] = Request.Url.ToString();

            string url = "frmPersonDetails.aspx?mode=u&person_type_id=10&claim_id=" + this.claimId + "&person_id=" + this.cboPersonSelect.SelectedValue + "&control=cboPersonSelect";
            string s = "window.open('" + url + "', 'popup_windowIP', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnInjuredOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();


            //if (this.injuredId == 0)
            //{
            //Injured = new Injured();
            this.personId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                if ((this.Claim.ExistingInjured(this.personId)) && (injuredId == 0))
                {   //injured is already on claim
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + Person.PersonName(personId) + " is already an Injured for this Claim.');</script>");
                    //MessageBox.Show(Person.PersonName(personId) + " is already an Injured for this Claim.", "Injured Already on Claim", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    return;
                }
                //else
                //{   //existing claim
                //    if (this.Injured.PersonId != this.personId)
                //    {   //user has changed person
                //        if (this.Claim.ExistingInjured(this.personId))
                //        {   //injured is already on claim
                //            MessageBox.Show(Person.PersonName(personId) + " is already an Injured for this Claim.", "Injured Already on Claim", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                //            return;
                //        }
                //    }
                //}


                Injured.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                Injured.ClaimId = this.claimId;
                Injured.ExtentOfInjury = this.txtInjuryDescription.Text;
                Injured.InjuryCauseId = Convert.ToInt32(cboInjuryCause.SelectedValue);
                Injured.InjuryTypeId = Convert.ToInt32(cboInjuryType.SelectedValue);
                this.injuredId = Injured.Update();

                //Injured.Id = this.injuredId;
                Injured = null;
                Injured = new Injured(injuredId);
            //}
            //else
            //{ 

            //    //Injured = new Injured(Convert.ToInt32(Request.QueryString["injured_id"]));

            //    Injured.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
            //    Injured.ClaimId = this.claimId;
            //    Injured.ExtentOfInjury = this.txtInjuryDescription.Text;
            //    Injured.InjuryCauseId = Convert.ToInt32(cboInjuryCause.SelectedValue);
            //    Injured.InjuryTypeId = Convert.ToInt32(cboInjuryType.SelectedValue);
       
            //    this.injuredId = Injured.Update();

              
            //}

            Injured.PersonId = Convert.ToInt32(cboPersonSelect.SelectedValue);
            Injured.ClaimId = this.claimId;
            Injured.ExtentOfInjury = this.txtInjuryDescription.Text;
            Injured.InjuryCauseId = Convert.ToInt32(cboInjuryCause.SelectedValue);
            Injured.InjuryTypeId = Convert.ToInt32(cboInjuryType.SelectedValue);

            this.injuredId = Injured.Update();
            //delete the removed injured body parts
            //foreach (int item in this.deletedBodyPartsIds)
            mm.DeleteBodyParts(injuredId);

            //save the injured body parts
            for (int i = 0; i < grInjuryPart.Rows.Count; i++)
            {

                CheckBox mycheck = ((CheckBox)grInjuryPart.Rows[i].FindControl("cbSelect"));

                if (mycheck.Checked)
                {   //add the new body part
                    InjuredBodyPart newPart = new InjuredBodyPart();
                    newPart.InjuredId = this.injuredId;
                    //BodyPart myPart = new BodyPart(Convert.ToInt32(grInjuryPart.DataKeys[i].Value);
                    newPart.BodyPartId = Convert.ToInt32(grInjuryPart.DataKeys[i].Value);
                    newPart.Update();
                }
            }

           
           

            int claimantId = this.Claim.CreateClaimantIfNecessary(Convert.ToInt32(cboPersonSelect.SelectedValue));       //create claimant

            for (int i = 0; i < grReserves.Rows.Count; i++)
            {


                CheckBox mycheck = ((CheckBox)grReserves.Rows[i].FindControl("cbSelect"));

                if (mycheck.Checked)
                {
                    Claimant myC = new Claimant(claimantId);
                    
                        Reserve res = new Reserve(m_claim, grReserves.Rows[i].Cells[1].Text);
                        ReserveType rt = new ReserveType(grReserves.Rows[i].Cells[1].Text);     //get reserve type object

                      

                            //create the injured coverage
                            InjuredCoverage ic = Injured.InjuredCoverageForReserveType(rt.Id);
                            ic.CoverageSpecified = true;
                            ic.Update();

                   

                                //string assignTo = res.AssignedToUser().Name;
                                //string assignTo = "";
                            int userId = 0;
                            //if (res.AssignedToUser().Id != 0)
                            //    userId = res.AssignedToUser().Id;
                            //else
                            //{
                            //    string[] thisUser = res.AssignedToUser().Name.Split(' ');
                            //    userId = mm.GetReserveUserId(thisUser[0], thisUser[1]);
                            //}
                            int userGroupId = 0;
                            Reserve r = new Reserve(ic.ReserveId);
                            r.AssignTo(userId, userGroupId);
                      
                }
            }



            Session["referer2"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);


        }

       

        protected void btnLienAdd_Click(object sender, EventArgs e)
        {
            Session["referer3"] = Request.Url.ToString();

            string url = "frmLiens.aspx?claim_id=" + this.claimId + "&injured_lien_id=0&vendor_id=0&injured_id=" + Request.QueryString["injured_id"].ToString() + "&amount=0";
            string s = "window.open('" + url + "', 'popup_window6', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnLienEdit_Click(object sender, EventArgs e)
        {
            if (grLiens.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a lien to edit.');</script>");
                //MessageBox.Show("Please select a lien to edit.", "No Lien Selected.", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                return;
            }
            Session["referer3"] = Request.Url.ToString();

            string url = "frmLiens.aspx?claim_id=" + this.claimId + "&injured_lien_id=" + grLiens.DataKeys[grLiens.SelectedIndex].Value.ToString() + "&vendor_id=" + grLiens.Rows[grLiens.SelectedIndex].Cells[4].Text + "&injured_id=" + Request.QueryString["injured_id"].ToString() + "&amount=" + grLiens.Rows[grLiens.SelectedIndex].Cells[3].Text.Replace("$","");
            string s = "window.open('" + url + "', 'popup_window6', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnLienDelete_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (grLiens.SelectedIndex < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('TPlease select a Lien to delete.');</script>");
                    //System.Windows.Forms.MessageBox.Show("Please select a Lien to delete.", "No Lien Selected", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                    return;
                }

                int lienID = Convert.ToInt32(grLiens.DataKeys[grLiens.SelectedIndex].Value);

                Injured InjuredDel = new Injured(injuredId);
                InjuredDel.DeleteLien(lienID);

                DisplayLiens();

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnLienEval_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString();

            string url = "frmBIEvaluation.aspx?injured_id=" + injuredId;
            string s = "window.open('" + url + "', 'popup_window5', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=900, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnBodyPartDelete_Click(object sender, EventArgs e)
        {
          
        }

        protected void btnUpdateReserves_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grReserves.Rows.Count; i++)
            {


                CheckBox mycheck = ((CheckBox)grReserves.Rows[i].FindControl("cbSelect"));

                if (mycheck.Checked)
                {
                    Claimant myC = new Claimant(m_claim, injuredId);

                    Reserve res = new Reserve(m_claim, grReserves.Rows[i].Cells[1].Text);
                    ReserveType rt = new ReserveType(grReserves.Rows[i].Cells[1].Text);     //get reserve type object

                    if (!myC.ReserveExists(rt.Id))
                    {


                        //create the injured coverage
                        InjuredCoverage ic = Injured.InjuredCoverageForReserveType(rt.Id);
                        ic.CoverageSpecified = true;
                        ic.Update();



                        //string assignTo = res.AssignedToUser().Name;
                        //string assignTo = "";
                        int userId = 0;
                        //if (res.AssignedToUser().Id != 0)
                        //    userId = res.AssignedToUser().Id;
                        //else
                        //{
                        //    string[] thisUser = res.AssignedToUser().Name.Split(' ');
                        //    userId = mm.GetReserveUserId(thisUser[0], thisUser[1]);
                        //}
                        int userGroupId = 0;
                        Reserve r = new Reserve(ic.ReserveId);
                        r.AssignTo(userId, userGroupId);
                    }
                }
            }
        }

        //protected void btnBodyPartAdd_Click(object sender, EventArgs e)
        //{
        //    mm.UpdateLastActivityTime();

        //    List<Hashtable> parts = TypeTable.getTypeTableRows("Body_Part");
        //    ArrayList newParts = new ArrayList();
        //    foreach (Hashtable eachPart in parts)
        //    {
        //        if (!this.PartInList((string)eachPart["Description"]))
        //        {
        //            cboGrid thisPart = new cboGrid()
        //            {
        //                id = (int)eachPart["Id"],
        //                description = (string)eachPart["Description"]
        //            };
        //            newParts.Add(thisPart);
        //        }
        //    }


        //    Session["referer2"] = Request.Url.ToString();

        //    string url = "frmPickBodyPart.aspx?mode=c&newParts=" + newParts;
        //    string s = "window.open('" + url + "', 'popup_windowIP', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=400, copyhistory=no, left=400, top=200');";
        //    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        //}

        private bool PartInList(string p)
        {
            foreach (InjuryTable parts in myInjuredParts)
            {
                if (parts.description == p)
                    return true;    //the part is in the list
            }
            return false;   //the part is not in the list
        }

        protected void cboPersonSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //get the person description
                this.personId = Convert.ToInt32(cboPersonSelect.SelectedValue);
                Person p = new Person(personId);
                this.txtPersonDetails.Text = p.ExtendedDescription().Replace("\r\n", "<br>");

                //don't allow edit of Unknown
                if (cboPersonSelect.SelectedValue == "Unknown")
                    this.btnPersonEdit.Enabled = false;
                else
                    this.btnPersonEdit.Enabled = true;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public int CountDigits(string s)
        {
            int i;
            int count;

            count = 0;
            for (i = 0; i <= s.Length - 1; i++)
            {
                if (IsNumeric(s.Substring(i, 1)))
                    count = count + 1;
            }
            return count;
        }

        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private MedCsxLogic.Claim Claim
        {
            get { return m_claim; }
            set { m_claim = value; }
        }

        class InjuredReserves
        {
            public bool hasCoverage { get; set; }
            public string coverageType { get; set; }
            public string assignedUser { get; set; }
            public int assignedUserId { get; set; }
        }

        public class InjuryTable
        {
            public int injuryId { get; set; }
            public string description { get; set; }
            public int partId { get; set; }
            public int imageIdx { get; set; }
        }

        class LienData
        {
            public int lienId { get; set; }
            public string vendorName { get; set; }
            public string vendorDescription { get; set; }
            public double lienAmount { get; set; }
            public int vendorId { get; set; }
        }
    }
}
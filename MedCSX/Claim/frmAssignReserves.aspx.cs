﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;
using MedCsxDatabase;
using System.Data;

namespace MedCSX.Claim
{
    public partial class frmAssignReserves : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private static modLogic ml = new modLogic();
<<<<<<< HEAD
        private static clsDatabase db = new clsDatabase();
=======
<<<<<<< HEAD
        private static clsDatabase db = new clsDatabase();
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

        private MedCsxLogic.Claim MyClaim = new MedCsxLogic.Claim();
        private int claim_id = 0;
        private string referer = "";
        public bool frommsg = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            MyClaim = new MedCsxLogic.Claim(claim_id);

            referer = Session["referer"].ToString();

            if (Request.QueryString["mode"] != null)
                frommsg = true;

            if (!Page.IsPostBack)
            {
                LoadReserveAssignments();
                LoadReserves();
            }
        }

        private void LoadReserveAssignments()
        {
            // Load the reserve assignments to the grid

            try
            {
                // Update the last activity time
                mm.UpdateLastActivityTime();

                // Get the reserve assignments
                DataTable rows = mm.GetReserveAssignments();

                grUsers.DataSource = rows;
                grUsers.DataBind();
               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void LoadReserves()
        {
            // Load the reserve assignments to the grid

            try
            {
                // Update the last activity time
                mm.UpdateLastActivityTime();

                // Get the reserve assignments
                DataTable rows = mm.GetReserveAssignmentsForClaim(claim_id);

                //foreach (DataRow row in rows.Rows)
                //{
                //    Response.Write(row[0].ToString() + "-" +  row[1].ToString() + "<br>");

                //}


                    grReserves.DataSource = rows;
                grReserves.DataBind();

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            for (int i = 0; i < grReserves.Rows.Count; i++)
            {
               
               int reserve_id = Convert.ToInt32(grReserves.DataKeys[i].Value);
                //string user = grReserves.Rows[i].Cells[3].Text;
                DropDownList mydd = ((DropDownList)grReserves.Rows[i].FindControl("ddlUsers"));
                string user = mydd.SelectedItem.Text;

                if (user != "")
                {
                    Reserve r = new Reserve(reserve_id);

                    



                    string[] a = user.Split(' ');
                    string fName = a[0];
                    string lName = "";
                    if (a.Length > 2)
                        lName = a[1] + " " + a[2];
                    else
                        lName = a[1];
                    int userid = mm.GetReserveUserId(fName, lName);

                    if (r.AssignedToUser().UserId != userid)
                        r.AssignTo(userid, 0);
                }
            }
            if (frommsg)
            {
                //string url = "ViewClaim.aspx?Ptabindex=7&claim_id=" + this.claim_id;
                //string win = "window.open('" + url + "','my_window" + claim_id.ToString() + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no');window.close()";
                //ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
        }

        protected void grReserves_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                //DataTable rows = mm.GetReserveAssignments();
                DataTable rows = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Active_Users");

                //DataView dv = new DataView(rows);
                //dv.Sort = "Name ASC";
                //rows.DefaultView.Sort = "Name ASC";
<<<<<<< HEAD
=======
=======
                DataTable rows = mm.GetReserveAssignments();

                
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e



                //Find the DropDownList in the Row
                DropDownList ddlUsers = (e.Row.FindControl("ddlUsers") as DropDownList);
                ddlUsers.DataSource = rows;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                ddlUsers.DataTextField = "Name";
                ddlUsers.DataValueField = "Name";
                ddlUsers.DataBind();
                
<<<<<<< HEAD
=======
=======
                ddlUsers.DataTextField = "name";
                ddlUsers.DataValueField = "name";
                ddlUsers.DataBind();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                //Add Default Item in the DropDownList
                ddlUsers.Items.Insert(0, new ListItem(""));

                //Select the user in DropDownList
                string lblUser = (e.Row.FindControl("lblUser") as Label).Text;
                ddlUsers.Items.FindByValue(lblUser).Selected = true;
            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmWireTransfer.aspx.cs" Inherits="MedCSX.Claim.frmWireTransfer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div id="tiWireTransfer" runat="server">
    <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtClaimId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Reserve:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtReserveId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Date of Loss:
        </div>
        <div class="col-md-9">
            <asp:Label ID="dtLoss" runat="server" Text=""></asp:Label>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3">
            Date Sent:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="dpDateSent" TextMode="Date" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Amount:
        </div>
        <div class="col-md-6">
            <asp:TextBox ID="txtWireAmount" OnTextChanged="txtWireAmount_TextChanged" AutoPostBack="true" TextMode="Number" Step=".01" runat="server"></asp:TextBox>
        </div>
          <div class="col-md-3">
              <asp:RadioButton ID="rbLoss" Text="Loss" GroupName="LossExpense" runat="server" /> <asp:RadioButton ID="rbExpense" GroupName="LossExpense" Text="Expense" runat="server" />
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Sent To:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtSendTo"  runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Notes:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtWireNotes" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
    </div>
</div>



<div id="tiWireOverPayment" runat="server" visible="false">
     <div class="row">
        <div class="col-md-3">
            Claim:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtWTOClaimId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Reserve:
        </div>
        <div class="col-md-9">
            <asp:Label ID="txtWTOReserveId" runat="server" Text=""></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Date of Loss:
        </div>
        <div class="col-md-9">
            <asp:Label ID="dtWTOLoss" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
           Wire Transfer:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboWTO" OnTextChanged="cboWTO_TextChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Date Received:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="dpWTODateSent" TextMode="Date" runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Amount:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtWTOAmount" OnTextChanged="txtWTOAmount_TextChanged" AutoPostBack="true"  runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
           Draft No:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtWTODraftNo"  runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Sent To:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtRecFrom"  runat="server"></asp:TextBox>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
           Notes:
        </div>
        <div class="col-md-9">
            <asp:TextBox ID="txtWTONotes"  runat="server"></asp:TextBox>
        </div>
    </div>
</div>


      <div class="row">
             
         <div class="col-md-12">
           <asp:Button ID="btnOK" runat="server" class="btn btn-info btn-xs"  Text="OK" OnClick="btnOK_Click"/>
           <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

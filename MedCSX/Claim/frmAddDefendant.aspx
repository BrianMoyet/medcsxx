﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmAddDefendant.aspx.cs" Inherits="MedCSX.Claim.frmAddDefendant" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-4">
            The Defendant is a a:
        </div>
        <div class="col-md-8">
            <asp:RadioButton ID="rbDefendantPerson" runat="server" GroupName="DefendantType" Text="Person" CausesValidation="false" Checked="true" AutoPostBack="true" OnCheckedChanged="rbDefendantPerson_CheckedChanged"/>
            <asp:RadioButton ID="rbDefendantVendor" runat="server" GroupName="DefendantType" Text="Insurance Company" CausesValidation="false" AutoPostBack="true" OnCheckedChanged="rbDefendantVendor_CheckedChanged"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Person:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboDefendantPerson" runat="server" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="cboDefendantPerson_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Insurance Company:
        </div>
        <div class="col-md-9">
            <asp:DropDownList ID="cboDefendantVendor" runat="server" Enabled="false" CausesValidation="false" AutoPostBack="true"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click" />
              
               <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
    </div>
</div>
</asp:Content>

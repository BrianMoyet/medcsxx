﻿using MedCSX.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class Offer : System.Web.UI.Page
    {
        private int claim_id;
        private int id;
        private string tmpDisplayclaim_id;
        private string tmpSub_claim_type_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            Page.Title = "Claim " + claim.display_claim_id + " - Add Demand";
            tmpSub_claim_type_id = claim.sub_claim_type_id;

            tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
            string tmpID = Regex.Replace(Request.QueryString["id"].ToString(), "[^0-9]", "");
            id = Convert.ToInt32(tmpID);


            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopulateDDLs();


                if (Request.QueryString["mode"].ToString() == "c")
                {
                    btnChange.Visible = true;
                    btnChange.Text = "Add";
                }
                else if (Request.QueryString["mode"].ToString() == "r")
                {
                    btnChange.Visible = false;
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "u")
                {
                    btnDelete.Visible = false;
                    btnChange.Visible = true;
                    btnChange.Text = "Update";
                    GetGeneralInfo();
                }
                else if (Request.QueryString["mode"].ToString() == "d")
                {
                    btnDelete.Visible = true;
                    GetGeneralInfo();
                }
            }
        }

        protected void GetGeneralInfo()
        {
            //MedCSX.App_Code.Litigation myData = new MedCSX.App_Code.Litigation();
            //myData.GetLitigationByID(id);

            //ddlStatus.SelectedValue = myData.litigation_status_id;
            //ddlDefenseAttorney.SelectedValue = myData.defense_attorney_id;
            //ddlPlantiffAttorney.SelectedValue = myData.plaintiff_attorney_id;
            //ddlCourtLocation.SelectedValue = myData.court_location_id;
            //txtDateFiled.Text = Convert.ToDateTime(myData.date_filed).ToString("yyyy-MM-dd");
            //txtDateServed.Text = Convert.ToDateTime(myData.date_served).ToString("yyyy-MM-dd");
            //txtResolutionAmount.Text = string.Format("{0:c}", myData.reslolution_amount);
            //txtComments.Text = myData.comments;



        }

        protected void PopulateDDLs()
        {
            DataTable dt = new DataTable();

            dt = HelperFunctions.getTypeTableRows("Offer_Sent_Type", "offer_sent_type_id", "1=1");
            ddlSentType.DataSource = dt;
            ddlSentType.DataValueField = dt.Columns[0].ColumnName;
            ddlSentType.DataTextField = dt.Columns[1].ColumnName;
            ddlSentType.DataBind();

            //gvDemandOffer.DataSource = dt;
            //gvDemandOffer.DataBind();
            //gvDemandOffer.Visible = true;




        }

        protected void btnChange_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
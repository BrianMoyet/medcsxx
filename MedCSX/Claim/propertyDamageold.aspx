﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="propertyDamageold.aspx.cs" Inherits="MedCSX.Claim.propertyDamage" %>


<!DOCTYPE html>

<html lang="en">
<head runat="server">
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title runat="server"></title>
      <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="../Content/css" />
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon" />
       <style>
             body {
              padding-top: 10px;
              padding-bottom: 10px;
            }

            .sidebar-nav {
              padding: 9px 0;
            }

            .dropdown-menu .sub-menu {
              left: 100%;
              position: absolute;
              top: 0;
              visibility: hidden;
              margin-top: -1px;
            }

            .dropdown-menu li:hover .sub-menu {
              visibility: visible;
            }

            .dropdown:hover .dropdown-menu {
              display: block;
            }

            .nav-tabs .dropdown-menu,
            .nav-pills .dropdown-menu,
            .navbar .dropdown-menu {
              margin-top: 0;
            }

            .navbar .sub-menu:before {
              border-bottom: 7px solid transparent;
              border-left: none;
              border-right: 7px solid rgba(0, 0, 0, 0.2);
              border-top: 7px solid transparent;
              left: -7px;
              top: 10px;
            }

            .navbar .sub-menu:after {
              border-top: 6px solid transparent;
              border-left: none;
              border-right: 6px solid #fff;
              border-bottom: 6px solid transparent;
              left: 10px;
              top: 11px;
              left: -6px;
            }


    </style>
</head>
<body>
<form id="form1" runat="server">
      <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="../Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="../Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="../Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="../Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="../Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="../Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="../Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="../Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
  <div class="container-fluid"  ID="MainContent">
       <div class="row">
            <div class="col-md-2">
              Reserve Type:
            </div>
            <div class="col-md-8">
                   <asp:DropDownList ID="ddlReserveType" runat="server"></asp:DropDownList>
            </div>
       </div>
    <%-- <div class="row">
            <div class="col-md-2">
              Property Type:
            </div>
            <div class="col-md-8">
                   <asp:DropDownList ID="ddlPropertyType" runat="server"></asp:DropDownList>
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              Auto:
            </div>
            <div class="col-md-8">
                   <asp:DropDownList ID="ddlAuto" runat="server"></asp:DropDownList>
            </div>
       </div>
    <div class="row">
            <div class="col-md-2">
              Owner:
            </div>
            <div class="col-md-8">
                   <asp:DropDownList ID="ddlOwner" runat="server"></asp:DropDownList>
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-8">
                   write out
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              Driver:
            </div>
            <div class="col-md-8">
                   <asp:DropDownList ID="ddlDriver" runat="server"></asp:DropDownList>
            </div>
       </div>
      <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-8">
                   write out
            </div>
       </div>
     <div class="row">
            <div class="col-md-2">
              Where Seen:
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtWhereSeen" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                 (Required for Appraisals)
            </div>
       </div>
    <div class="row">
            <div class="col-md-2">
             Describe Damage:
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtDescribeDamage" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
             (Required for Appraisals)
            </div>
       </div>
      <div class="row">
            <div class="col-md-2">
              Estimated Amount:
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtEstimatedAmount" runat="server"></asp:TextBox>
            </div>
       </div>
       <div class="row">
            <div class="col-md-12">
                Other Insurance <hr style='display:inline-block; width:100px;' />
            </div>
       </div>--%>
</div>
</form>
</body>
</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="Offer.aspx.cs" Inherits="MedCSX.Claim.Offer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-md-2">
        Sent Type:
    </div>
    <div class="col-md-10">
        <asp:DropDownList ID="ddlSentType" runat="server"></asp:DropDownList>
    </div>
</div>
 <div class="row">
    <div class="col-md-2">
        Offer Amount:
    </div>
    <div class="col-md-10">
        <asp:TextBox ID="txtOfferAmount" runat="server"></asp:TextBox>
    </div>
</div>
 <div class="row">
    <div class="col-md-2">
       Offer Date:
    </div>
    <div class="col-md-10">
         <asp:TextBox ID="txtOfferDate" runat="server" TextMode="Date"></asp:TextBox>        
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:Button ID="btnChange" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Update"     OnClick="btnChange_Click"  />
        <asp:Button ID="btnDelete" runat="server" Visible="false" class="btn btn-info btn-xs"  Text="Delete"    OnClientClick="return confirm('Are you sure you want to delete this record?');"   OnClick="btnDelete_Click"  />
        <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.opener.location.reload(false); window.close(); return false;" />
    </div>
</div>    
<div>
      <div class="col-md-12">
            <asp:GridView ID="gvDemandOffer" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ShowFooter="true"
                    ForeColor="#333333"   width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                        <asp:TemplateField HeaderText="Demand / Offer" >
                            <ItemTemplate>
                                <asp:Label runat="server" Text=<%# Eval("type")%>></asp:Label>
                            </ItemTemplate>
                               
                         
                            </asp:TemplateField>
                                       
                    <asp:BoundField DataField="date" HeaderText="Date"/>
                    <asp:BoundField DataField="received_date" HeaderText="Received Date" />
                    <asp:BoundField DataField="due_date" HeaderText="Due Date"  />
                    <asp:BoundField DataField="amount" HeaderText="Amount" />
               
            </Columns>
             
              <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    
        </asp:GridView>
        </div>

</div>
</asp:Content>

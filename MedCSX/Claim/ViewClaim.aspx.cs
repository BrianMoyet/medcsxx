﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text.RegularExpressions;
using System.Data;
using System.Drawing;
using System.Collections;
using MedCsxLogic;
using System.Windows.Media.Imaging;
using System.Threading;
using MedCSX.Claim.UserControls;
using MedCsxDatabase;
using System.Windows;
using System.Diagnostics;
using System.IO;
using MedCSX.App_Code;
using System.Configuration;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;


namespace MedCSX.Claim
{
    public partial class ViewClaim : System.Web.UI.Page
    {
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();
        public string referer = "";
      
        #region Class Variables
        private bool claimLocked = false,           //is claim locked?
            reserveLocked = false,                  //is selected reserve locked?
            fileActivityLoading = false,            //is file activity list done loading?
            phoneNumbersLoading = false,            //are phone numbers done loading?
            policyDataLoading = false,              //is the policy data list done loading?
            auditControlsCreated = false,           //have the claim audit controls been created?
            removingTab = false,
            pdReload = false;
        private bool? qSR22 = null;                 //Is claim for SR22 individual?
        private List<Hashtable> _persons = null,    //people related to claim
             _vehicles = null;                      //vehicles related to claim
        private int fileClaimantId = 0,             //file activity claimant selected
             fileReserveId;                         //file activity reserve selected
        private string phoneNumberName = "",        //selected phone number name
            originalTOL = "";                       //original entry to time of loss

        private int _claimId = 0;        //claim ID of this claim

        //claim object for the open claim
        private MedCsxLogic.Claim myClaim = null;
        private bool newSplitData = true;

        //owned vehicle information
        private int ownerPersonId = 0,
            driverPersonId = 0,
            insurePersonId = 0;
        private Vehicle insuredVehicle = null;

        public bool isNew = false;  //new claim?

        public int fileNoteId = 0;  //initial file note to show - if 0, show default screen
        private int fileNoteNum = 0;    //number of file notes displayed in the file activity window

        bool hasUrgentAlerts = false;
        //selected items
        private int _reserveId = 0,
            _transactionId = 0;
        private int fileNoteTypeId = 0,     //selected file note type ID
            taskUserId = 0,
            appraisalTaskId = 0;            //selected appraisal task ID

        private bool doneLoading = true;   //is form done loading?  set to true in Form_Activate event

        private Demand myDemand = null;     //selected demand
        private Litigation myLitigation = null; //selected litigation
        private string _selectedFAType;
        private string _selectedFAClaimant;
        private string _selectedFAReserve;
        private List<Hashtable> DataRows;       //hashtable for pulling in DataGrids
        private DataTable DataRowsDT;  //datatable for pulling into gridviews
        private List<PropertyDamageGrid> myPropertyDamageList;  //ItemsSource for Property Damage Grid
        private List<ClaimantGrid> myWitnessPassengerList;  //ItemsSource for Witness Passenger Grid
        private List<ClaimantGrid> myInjuredList;  //ItemsSource for Injured Grid
        private List<FileNoteGrid> myFileActivityList;  //ItemsSource for File Activity Grid
        private List<ClaimantGrid> myClaimantsList;  //ItemsSource for Claimant Grid
        private List<VehicleGrid> myVehiclesList;  //ItemsSource for Vehicles Grid
        private List<VehicleGrid> myVehiclesListcbo;  //ItemsSource for Vehicles ComboBox
        private List<ClaimantGrid> myOwnerListcbo;  //ItemsSource for Owner ComboBox
        private List<ClaimantGrid> myDriverListcbo;  //ItemsSource for Driver ComboBox
        private List<ReserveGrid> myReservesList;  //ItemsSource for Reserves Grid
        private List<ReserveGrid> myTaskList;      //ItemsSource for Task Grid
        private List<policyGrid> myPolicyList;      //ItemsSource for Policy Grid
        private List<policyGrid> myPolicyNoteList;      //ItemsSource for Policy Notes Grid
        private List<ReserveGrid> myAppraisalList;      //ItemsSource for Appraisal Grid
        private List<SalvageGrid> mySalvageList;        //ItemsSource for Salvage Grid
        private List<ClaimantGrid> myDemandSummaryList;        //ItemsSource for Demand Summary Grid
        private List<ReserveGrid> myDemandHistoryList;        //ItemsSource for Demand History Grid
        private List<LitigationGrid> myLitigationList;        //ItemsSource for Litigation Grid

        private int lockOffset = -2;        //amount of pixel points to move lock picture

        //claim screen previously viewed
        private int previousClaimScreen;

        //IV Comp/Collision assignment
        private int compUserId = 0,
            compUserGroupId = 0,
            collisionUserId = 0,
            collisionUserGroupId = 0;

        //image index of specific images
        private const int ImageIndex_Person_Male = 30,
            ImageIndex_Car = 75;
        //[System.Runtime.InteropServices.Guid("00020970-0000-0000-C000-000000000046")]
        private Microsoft.Office.Interop.Word.Application _wordApp;
        private Microsoft.Office.Interop.Word.Document _wordDoc;

       
        internal void LoadDemandSummary()
        {
          
        }

       
        #endregion

        #region Properties
        public MedCsxLogic.Claim Claim
        {
            get { return myClaim; }
            set { myClaim = value; }
        }

       
        public int reserveId
        {
            get { return _reserveId; }
            set { _reserveId = value; }
        }

        public int transactionId
        {
            get { return _transactionId; }
            set { _transactionId = value; }
        }
        #endregion

        #region Form Initialization
        //public ViewClaim(int claimId)
        //    : base()
        //{
        //    //InitializeComponent();
        //    this._claimId = Convert.ToInt32(Request.QueryString["claim_id"]);

        //    this.LoadClaim();  //load the local variables with the claim data
        //    //if (mm.UserId == null) MedCsXSystem.UserId = 78;
        //    this.Claim.InsertRecentClaim(mm.UserId);    //insert this claim into the recent claims list   
        //}
        #endregion

            

        const string controlID = "MyUserControl";
        static bool createAgain = false;


        //protected void Page_Init(object sender, EventArgs e)
        //{
          
        //    //this._claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
        //    //this.LoadClaim();

        //}

        protected void Page_Load(object sender, EventArgs e)
        {


            this._claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            this.LoadClaim();

            this.Claim.InsertRecentClaim(Convert.ToInt32(Session["userID"]));
            this._transactionId = transactionId;
            this._reserveId = reserveId;
                  
            //Commented out for performance - not needed for web
            //mm.IncrementOpenClaims();

            this.txtUserName.Text = mm.GetUserName(Convert.ToInt32(Session["userID"]));
            
            this.CheckClaimLock();

            if (!Page.IsPostBack)
            {
                this.LoadClaimTypeSpecificData();
                this.InitializeClaimChoices();  //select the initially displayed tab
                this.Claim.InsertRecentClaim(Convert.ToInt32(Session["userID"]));    //insert this claim into the recent claims list   
                this.SyncPolicy();  //sync claim with policy
                this.UpdateLossPayee();
<<<<<<< HEAD

=======
<<<<<<< HEAD

=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            }

            this.EnableDisableReOpen();  //Reopen button if claim is closed

            this.isNew = false;  //don't create startup behavior

            if (ml.DBNow().Subtract(this.Claim.DateReported).Days > 0)
                this.Claim.CompleteSetup();     //complete setup of claim if necessary

            doneLoading = true;
<<<<<<< HEAD

            this.SetTitle();

            if (TabContainer1.ActiveTabIndex == 0)
            { 
                cboGIClDetailDriver_SelectedIndexChanged(null, null);
                cboGIClDetailOwner_SelectedIndexChanged(null, null);
                cboGIClDetailVehicle_SelectedIndexChanged(null, null);

                if (!Page.IsPostBack)
                {
                    if (Session["tbGIClDetailDOL"] != null)
                    {
                        populateFormFromSessionVariables();

                    }


                    if (Request.QueryString["control"] != null)
                    {
                        switch (Request.QueryString["control"].ToString())
                        {
                            case "cboGIClDetailOwner":
                                cboGIClDetailOwner.SelectedValue = Request.QueryString["personid"].ToString();
                                cboGIClDetailOwner_SelectedIndexChanged(null, null);
                                cboGIClDetailOwner.Focus();
                                break;
                            case "cboGIClDetailDriver":
                                cboGIClDetailDriver.SelectedValue = Request.QueryString["personid"].ToString();
                                cboGIClDetailDriver_SelectedIndexChanged(null, null);
                                cboGIClDetailDriver.Focus();
                                break;
                            case "cboGIClDetailVehicle":
                                cboGIClDetailVehicle.SelectedValue = Request.QueryString["vehicleid"].ToString();
                                cboGIClDetailVehicle_SelectedIndexChanged(null, null);
                                cboGIClDetailVehicle.Focus();
                                break;

                            default:
                                break;
                        }
                    }



                }
            }
=======
               
            this.SetTitle();

            cboGIClDetailDriver_SelectedIndexChanged(null, null);
            cboGIClDetailOwner_SelectedIndexChanged(null, null);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            // Commenting out for performance
            //DataTable dt = mm.GetMyUrgentAlerts(Convert.ToInt32(Session["userID"]));
            //if (dt.Rows.Count > 0)
            //{
            //   //urgent.Visible = true;
            //    string url = "frmAlerts.aspx";
            //    string sAlert = "window.open('" + url + "', 'popup_windowAlert', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=700, height=300, copyhistory=no, left=400, top=200').focus();";
            //    ScriptManager.RegisterStartupScript(this.Page, GetType(), "popup_windowAlert", sAlert, true);
            //}

            //this.CheckBeano();

<<<<<<< HEAD
           
=======
            if (!Page.IsPostBack)
            {
                if (Session["tbGIClDetailDOL"] != null)
                {
                    populateFormFromSessionVariables();

                }


                if (Request.QueryString["control"] != null)
                {
                    switch (Request.QueryString["control"].ToString())
                    {
                        case "cboGIClDetailOwner":
                            cboGIClDetailOwner.SelectedValue = Request.QueryString["personid"].ToString();
                            cboGIClDetailOwner_SelectedIndexChanged(null, null);
                            cboGIClDetailOwner.Focus();
                            break;
                        case "cboGIClDetailDriver":
                            cboGIClDetailDriver.SelectedValue = Request.QueryString["personid"].ToString();
                            cboGIClDetailDriver_SelectedIndexChanged(null, null);
                            cboGIClDetailDriver.Focus();
                            break;
                        case "cboGIClDetailVehicle":
                            cboGIClDetailVehicle.SelectedValue = Request.QueryString["vehicleid"].ToString();
                            cboGIClDetailVehicle_SelectedIndexChanged(null, null);
                            cboGIClDetailVehicle.Focus();
                            break;

                        default:
                            break;
                    }
                }
<<<<<<< HEAD


              
            }
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            Page.MaintainScrollPositionOnPostBack = true;

           
        }

        //protected void Page_LoadComplete(object sender, EventArgs e)
        //{
        //    string loc = mf.getIRDocumentLinkByClaim(this.Claim.DisplayClaimId, mm.ClaimsDrawer);
        //    //Response.Redirect(loc);

        //    string url = loc;
        //    string s = "myWindowIR = window.open('" + url + "', 'popup_windowIR', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, width=1, height=1, copyhistory=no, left=0, top=0');myWindowIR.close();";
        //    ClientScript.RegisterStartupScript(this.GetType(), "scriptIR", s, true);

        //    //string i = "myWindowIR.close();";
        //    //ClientScript.RegisterStartupScript(this.GetType(), "scriptIR", i, true);
        //}


        public void populateFormFromSessionVariables()
        {

            try
            {
                string sessionName = "tbGIClDetailDOL" + this._claimId.ToString();

                if (Session[sessionName] != null)
                {

                    tbGIClDetailDOL.Text = Session["tbGIClDetailDOL"].ToString();
                    tbGIClDetailTOL.Text = Session["tbGIClDetailTOL"].ToString();
                    this.tbGIClDetaildtReported.Text = Session["tbGIClDetaildtReported"].ToString();
                    cboGIClDetailatFault.SelectedValue = Session["cboGIClDetailatFault"].ToString();
                    tbGIClDetailLossLoc.Text = Session["tbGIClDetailLossLoc"].ToString();
                    tbGIClDetailLossCity.Text = Session["tbGIClDetailLossCity"].ToString();
                    cboGIClDetailLossState.SelectedValue = Session["cboGIClDetailLossState"].ToString();
                    cboGIClDetailAccidentType.SelectedValue = Session["cboGIClDetailAccidentType"].ToString();
                    tbGIClDetailLossDescr.Text = Session["tbGIClDetailLossDescr"].ToString();
                    tbGIClDetailAuthContact.Text = Session["tbGIClDetailAuthContact"].ToString();
                    tbGIClDetailRptNo.Text = Session["tbGIClDetailRptNo"].ToString();
                    cboGIClDetailRptType.SelectedValue = Session["cboGIClDetailRptType"].ToString();
                    tbGIClDetailRptBy.Text = Session["tbGIClDetailRptBy"].ToString();
                    tbGIClDetailIVDamage.Text = Session["tbGIClDetailIVDamage"].ToString();
                    tbGIClDetailWhere.Text = Session["tbGIClDetailWhere"].ToString();
                    cboGIClDetailVehicle.SelectedValue = Session["cboGIClDetailVehicle"].ToString();
                    cboGIClDetailVehicle_SelectedIndexChanged(null, null);
                    //tbGIClDetailInsured.Text = Session["tbGIClDetailInsured"].ToString();
                    cboGIClDetailOwner.SelectedValue = Session["cboGIClDetailOwner"].ToString();
                    cboGIClDetailOwner_SelectedIndexChanged(null, null);
                    cboGIClDetailDriver.SelectedValue = Session["cboGIClDetailDriver"].ToString();
                    cboGIClDetailDriver_SelectedIndexChanged(null, null);
                    ckbGIClDetailPerm2Use.Checked = (bool)Session["ckbGIClDetailPerm2Use"];
                    cboGIClDetailSR22.Checked = (bool)Session["cboGIClDetailSR22"];
                    ckbGIClDetailIVComp.Checked = (bool)Session["ckbGIClDetailIVComp"];
                    ckbGIClDetailIVColl.Checked = (bool)Session["ckbGIClDetailIVColl"];
                    ckbGIClDetailCoverage.Checked = (bool)Session["ckbGIClDetailCoverage"];
                    ckbGIClDetailLiability.Checked = (bool)Session["ckbGIClDetailLiability"];
                    ckbGIClDetailLrgLoss.Checked = (bool)Session["ckbGIClDetailLrgLoss"];
                    ckbGIClDetailLitigation.Checked = (bool)Session["ckbGIClDetailLitigation"];
                    tbGIClDetailComments.Text = Session["tbGIClDetailComments"].ToString();

                    Session[sessionName] = null;
                    Session["tbGIClDetailDOL"] = null;
                    Session["tbGIClDetailTOL"] = null;
                    Session["tbGIClDetaildtReported"] = null;
                    Session["cboGIClDetailatFault"] = null;
                    Session["tbGIClDetailLossLoc"] = null;
                    Session["tbGIClDetailLossCity"] = null;
                    Session["cboGIClDetailLossState"] = null;
                    Session["cboGIClDetailAccidentType"] = null;
                    Session["tbGIClDetailLossDescr"] = null;
                    Session["tbGIClDetailAuthContact"] = null;
                    Session["tbGIClDetailRptNo"] = null;
                    Session["cboGIClDetailRptType"] = null;
                    Session["tbGIClDetailRptBy"] = null;
                    Session["tbGIClDetailIVDamage"] = null;
                    Session["tbGIClDetailWhere"] = null;
                    Session["cboGIClDetailVehicle"] = null;
                    //Session["tbGIClDetailInsured"] = null;
                    Session["cboGIClDetailOwner"] = null;
                    Session["cboGIClDetailDriver"] = null;
                    Session["ckbGIClDetailPerm2Use"] = null;
                    Session["cboGIClDetailSR22"] = null;
                    Session["ckbGIClDetailIVComp"] = null;
                    Session["ckbGIClDetailIVColl"] = null;
                    Session["ckbGIClDetailCoverage"] = null;
                    Session["ckbGIClDetailLiability"] = null;
                    Session["ckbGIClDetailLrgLoss"] = null;
                    Session["ckbGIClDetailLitigation"] = null;
                    Session["tbGIClDetailComments"] = null;
                }
            }
            catch (Exception ex)
            {
                string sessionName = "tbGIClDetailDOL" + this._claimId.ToString();
                Session[sessionName] = null;
                Session["tbGIClDetailDOL"] = null;
                Session["tbGIClDetailTOL"] = null;
                Session["tbGIClDetaildtReported"] = null;
                Session["cboGIClDetailatFault"] = null;
                Session["tbGIClDetailLossLoc"] = null;
                Session["tbGIClDetailLossCity"] = null;
                Session["cboGIClDetailLossState"] = null;
                Session["cboGIClDetailAccidentType"] = null;
                Session["tbGIClDetailLossDescr"] = null;
                Session["tbGIClDetailAuthContact"] = null;
                Session["tbGIClDetailRptNo"] = null;
                Session["cboGIClDetailRptType"] = null;
                Session["tbGIClDetailRptBy"] = null;
                Session["tbGIClDetailIVDamage"] = null;
                Session["tbGIClDetailWhere"] = null;
                Session["cboGIClDetailVehicle"] = null;
                //Session["tbGIClDetailInsured"] = null;
                Session["cboGIClDetailOwner"] = null;
                Session["cboGIClDetailDriver"] = null;
                Session["ckbGIClDetailPerm2Use"] = null;
                Session["cboGIClDetailSR22"] = null;
                Session["ckbGIClDetailIVComp"] = null;
                Session["ckbGIClDetailIVColl"] = null;
                Session["ckbGIClDetailCoverage"] = null;
                Session["ckbGIClDetailLiability"] = null;
                Session["ckbGIClDetailLrgLoss"] = null;
                Session["ckbGIClDetailLitigation"] = null;
                Session["tbGIClDetailComments"] = null;


                mf.ProcessError(ex);
            }
<<<<<<< HEAD
=======
=======
            }

            Page.MaintainScrollPositionOnPostBack = true;
        }


        public void populateFormFromSessionVariables()
        {
            tbGIClDetailDOL.Text = Session["tbGIClDetailDOL"].ToString();
            tbGIClDetailTOL.Text = Session["tbGIClDetailTOL"].ToString();
            this.tbGIClDetaildtReported.Text = Session["tbGIClDetaildtReported"].ToString();
            cboGIClDetailatFault.SelectedValue = Session["cboGIClDetailatFault"].ToString();
            tbGIClDetailLossLoc.Text = Session["tbGIClDetailLossLoc"].ToString();
            tbGIClDetailLossCity.Text = Session["tbGIClDetailLossCity"].ToString();
            cboGIClDetailLossState.SelectedValue = Session["cboGIClDetailLossState"].ToString();
            cboGIClDetailAccidentType.SelectedValue = Session["cboGIClDetailAccidentType"].ToString();
            tbGIClDetailLossDescr.Text = Session["tbGIClDetailLossDescr"].ToString();
            tbGIClDetailAuthContact.Text = Session["tbGIClDetailAuthContact"].ToString();
            tbGIClDetailRptNo.Text = Session["tbGIClDetailRptNo"].ToString();
            cboGIClDetailRptType.SelectedValue = Session["cboGIClDetailRptType"].ToString();
            tbGIClDetailRptBy.Text = Session["tbGIClDetailRptBy"].ToString();
            tbGIClDetailIVDamage.Text = Session["tbGIClDetailIVDamage"].ToString();
            tbGIClDetailWhere.Text = Session["tbGIClDetailWhere"].ToString();
            cboGIClDetailVehicle.SelectedValue = Session["cboGIClDetailVehicle"].ToString();
            cboGIClDetailVehicle_SelectedIndexChanged(null, null);
            //tbGIClDetailInsured.Text = Session["tbGIClDetailInsured"].ToString();
            cboGIClDetailOwner.SelectedValue = Session["cboGIClDetailOwner"].ToString();
            cboGIClDetailOwner_SelectedIndexChanged(null, null);
            cboGIClDetailDriver.SelectedValue = Session["cboGIClDetailDriver"].ToString();
            cboGIClDetailDriver_SelectedIndexChanged(null, null);
            ckbGIClDetailPerm2Use.Checked = (bool)Session["ckbGIClDetailPerm2Use"];
            cboGIClDetailSR22.Checked = (bool)Session["cboGIClDetailSR22"];
            ckbGIClDetailIVComp.Checked = (bool)Session["ckbGIClDetailIVComp"];
            ckbGIClDetailIVColl.Checked = (bool)Session["ckbGIClDetailIVColl"];
            ckbGIClDetailCoverage.Checked = (bool)Session["ckbGIClDetailCoverage"];
            ckbGIClDetailLiability.Checked = (bool)Session["ckbGIClDetailLiability"];
            ckbGIClDetailLrgLoss.Checked = (bool)Session["ckbGIClDetailLrgLoss"];
            ckbGIClDetailLitigation.Checked = (bool)Session["ckbGIClDetailLitigation"];
            tbGIClDetailComments.Text = Session["tbGIClDetailComments"].ToString();

            Session["tbGIClDetailDOL"] = null;
            Session["tbGIClDetailTOL"] = null;
            Session["tbGIClDetaildtReported"] = null;
            Session["cboGIClDetailatFault"] = null;
            Session["tbGIClDetailLossLoc"] = null;
            Session["tbGIClDetailLossCity"] = null;
            Session["cboGIClDetailLossState"] = null;
            Session["cboGIClDetailAccidentType"] = null;
            Session["tbGIClDetailLossDescr"] = null;
            Session["tbGIClDetailAuthContact"] = null;
            Session["tbGIClDetailRptNo"] = null;
            Session["cboGIClDetailRptType"] = null;
            Session["tbGIClDetailRptBy"] = null;
            Session["tbGIClDetailIVDamage"] = null;
            Session["tbGIClDetailWhere"] = null;
            Session["cboGIClDetailVehicle"] = null;
            //Session["tbGIClDetailInsured"] = null;
            Session["cboGIClDetailOwner"] = null;
            Session["cboGIClDetailDriver"] = null;
            Session["ckbGIClDetailPerm2Use"] = null;
            Session["cboGIClDetailSR22"] = null;
            Session["ckbGIClDetailIVComp"] = null;
            Session["ckbGIClDetailIVColl"] = null;
            Session["ckbGIClDetailCoverage"] = null;
            Session["ckbGIClDetailLiability"] = null;
            Session["ckbGIClDetailLrgLoss"] = null;
            Session["ckbGIClDetailLitigation"] = null;
            Session["tbGIClDetailComments"] = null;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }
       

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            if (Session["activeTab"] != null)
            {
              
                // return;
            }

            Session["activeTab"] = null;
<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            lblSuccess.Visible = false;

            //'Check for unassigned reserves
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //if (this.Claim.HasUnassignedReserves())
            //{
            //    int index = Request.Url.ToString().IndexOf("?");
            //    if (index > 0)
            //        referer = Request.Url.ToString().Substring(0, index);
            //    referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

            //    Session["referer"] = referer;

            //    string url = "frmUnassignedReservesMessage.aspx?mode=c&claim_id=" + _claimId;
            //    string s = "window.open('" + url + "', 'popup_windowurm', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=250, copyhistory=no, left=400, top=400');";
            //    ClientScript.RegisterStartupScript(this.GetType(), "scripturm", s, true);
            //}

                if (this.TabContainer1.ActiveTabIndex == 0)  //General
<<<<<<< HEAD
=======
=======
            if (this.Claim.HasUnassignedReserves())
            {
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer;

                string url = "frmUnassignedReservesMessage.aspx?mode=c&claim_id=" + _claimId;
                string s = "window.open('" + url + "', 'popup_windowurm', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=250, copyhistory=no, left=400, top=400');";
                ClientScript.RegisterStartupScript(this.GetType(), "scripturm", s, true);
            }

            if (this.TabContainer1.ActiveTabIndex == 0)  //General
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                {

                    //if (!Page.IsPostBack)
                    //{ 
                        this.EnableDisableGIControls();
                        this.DisplayClaimData();
                        if (tbGIClDetailDOL.Visible == true)
                            this.tbGIClDetailDOL.Focus();

                        btnGIClDetailNewVehicle.Enabled = true;
<<<<<<< HEAD
                // }

                    Session["activeTab"] = 0;
=======
<<<<<<< HEAD
                // }

                    Session["activeTab"] = 0;
=======
                   // }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }

                if (TabContainer1.ActiveTabIndex == 1)  //property Damage
                {
                    
                    this.DisplayPropertyDamage();
                    this.pnlGI.Visible = false;
                   
                }

                if (TabContainer1.ActiveTabIndex == 2) //Witness Passenger
                {
                    this.pnlGI.Visible = false;
                    this.DisplayWitnessPassenger();
                }

                if (TabContainer1.ActiveTabIndex == 3) //Injured
                {
                    this.pnlGI.Visible = false;
                    this.DisplayInjured();
                }

                if (TabContainer1.ActiveTabIndex == 4) //File Activity
                {
                    this.pnlGI.Visible = false;
                    this.DisplayFileActivity();
                  
                }
                if (TabContainer1.ActiveTabIndex == 5) //claimants
                {
                    this.pnlGI.Visible = false;
                    this.DisplayClaimants();
                }
                if (TabContainer1.ActiveTabIndex == 6) //Vehicles
                {
                    this.pnlGI.Visible = false;
                    this.DisplayVehicles();
 
                }
                if (TabContainer1.ActiveTabIndex == 7) //reserves
                {
                    this.pnlGI.Visible = false;
                    DisplayReserves();
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    //if (this.grReserves.Rows.Count > 0)
                    //{
                    //    grReserves.SelectedIndex = 0;
                    //    grReserves_SelectedIndexChanged(null, null);
                    //}
<<<<<<< HEAD
=======
=======
                    if (this.grReserves.Rows.Count > 0)
                    {
                        grReserves.SelectedIndex = 0;
                        grReserves_SelectedIndexChanged(null, null);
                    }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

                }
                if (TabContainer1.ActiveTabIndex == 8)  //tasks
                {
                    this.pnlGI.Visible = false;
                    this.DisplayTasks();
                }
                if (TabContainer1.ActiveTabIndex == 9)//Policy Data
                {
                    this.pnlGI.Visible = false;
                    this.DisplayPolicyData();
                }
                if (TabContainer1.ActiveTabIndex == 10)  //Appraisals
                {
                    this.pnlGI.Visible = false;
                    this.DisplayAppraisals();
                }
                if (TabContainer1.ActiveTabIndex == 11)  //Documents
                {
                    this.pnlGI.Visible = false;
                    rbAvailableDocs.Checked = true;
                    this.DisplayDocuments();
                }
                if (TabContainer1.ActiveTabIndex == 12)  //Phone numbers
                {
                    this.pnlGI.Visible = false;
                    this.DisplayPhoneNumbers();
                }
                if (TabContainer1.ActiveTabIndex == 13)  //Duplicates
                {
                    this.pnlGI.Visible = false;
                    this.DisplayDuplicateClaims();
                }
                if (TabContainer1.ActiveTabIndex == 14)  //Demands
                {
                    this.pnlGI.Visible = false;
                    this.DisplayDemandOffer();
                }
                if (TabContainer1.ActiveTabIndex == 15)  //Litigation
                {
                    this.pnlGI.Visible = false;
                    this.DisplayLitgation();
                }
                if (TabContainer1.ActiveTabIndex == 16)  //Salvage
                {
                    this.pnlGI.Visible = false;
                     LoadSalvage();
                
                }

                if (TabContainer1.ActiveTabIndex == 17)  //Alerts
                {
                    this.pnlGI.Visible = false;
                    this.DisplayAlerts();
                }
        }

        private void SyncPolicy(bool associatingWithNewPolicy = false)
        {
            string msg = this.Claim.SyncPolicy(associatingWithNewPolicy);

            if (msg == null)
                msg = "";

            this.SetTitle();  //set the form title bar text

            if (!this.doneLoading)  //not done loading form
                return;

            this.CheckClaimLock();  //check if claim is locked - set claimLocked
        }



        private void EnableDisableReserveControls()
        {
            //should not be necessary

            Reserve res = new Reserve(this.reserveId);      //create reserve object

            //enable void reserve if there are no transactions on the reserve line
            if (res.NoNonReserveTransactions == 0)
            {
                if (res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Void)
                { 
                    this.btnResVoid.Enabled = true;
                    this.btnResVoid.ForeColor = System.Drawing.Color.Blue;
                }
            }

            switch (res.ReserveStatusId) 
            {
                case (int)modGeneratedEnums.ReserveStatus.Open_Reserve:  //reserve is open
                    //allow user to change the expense reserves and loss reserves
                    this.btnResChangeExpense.Enabled = true;
                    this.btnResChangeLoss.Enabled = true;
                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.Blue;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve
                    this.btnResClose.Enabled = true;
                    this.btnResClose.ForeColor = System.Drawing.Color.Blue;

                    //reserve is open - don't allow user to reopen
                    this.btnResReOpen.Enabled = false;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.DarkGray;
                    
                    //allow user to view transactions for this reserve
                    this.btnResTransactions.Enabled = true;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;

                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    this.btnResCloseSalvage.Enabled = true;
                    this.btnResCloseSubro.Enabled = true;
                    this.btnResCloseDeduct.Enabled = true;
                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.Blue;

                    //this.btnResCloseSIR.Enabled = true;
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.Enabled = true;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;

                    if (!res.isTotalLoss)
                    {
                        if ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) ||
                            (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) ||
                            (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive) ||
                            (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage))
                        {
                            this.btnResTotalLoss.Enabled = true;      //allow user to set the reserve as total loss
                            this.btnResTotalLoss.ForeColor = System.Drawing.Color.Blue;
                        }
                        else
                        {
                            this.btnResTotalLoss.Enabled = false;
                            this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;
                        }
                    }
                    else
                    {
                        this.btnResTotalLoss.Enabled = false;     //already set to total loss
                        this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;
                    }
                    break;
                case (int)modGeneratedEnums.ReserveStatus.Closed:
                    this.btnResChangeExpense.Enabled = false;
                    this.btnResChangeLoss.Enabled = false;
                    this.btnResClose.Enabled = false;
                    this.btnResTotalLoss.Enabled = false;
                    this.btnResRecon.Enabled = false;

                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResClose.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResRecon.ForeColor = System.Drawing.Color.DarkGray;

                    //allow user to reopen
                    this.btnResReOpen.Enabled = true;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.Blue;

                    //allow user to view transactions for this reserve
                    this.btnResTransactions.Enabled = true;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;

                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    this.btnResCloseSalvage.Enabled = true;
                    this.btnResCloseSubro.Enabled = true;
                    this.btnResCloseDeduct.Enabled = true;

                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.Blue;
                    //this.btnResCloseSIR.Enabled = true;
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSubro.Enabled = true;

                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;
                    break;
                case (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage:
                    this.btnResChangeExpense.Enabled = false;
                    this.btnResChangeLoss.Enabled = false;
                    this.btnResCloseSalvage.Enabled = false;
                    this.btnResTotalLoss.Enabled = false;

                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;

                    //allow user to close reserve
                    this.btnResClose.Enabled = true;
                    this.btnResClose.ForeColor = System.Drawing.Color.Blue;

                    //allow user to reopen
                    this.btnResReOpen.Enabled = true;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.Blue;

                    //allow user to view transactions for this reserve
                    this.btnResTransactions.Enabled = true;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;

                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    this.btnResCloseSubro.Enabled = true;
                    this.btnResCloseDeduct.Enabled = true;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.Blue;
                    //this.btnResCloseSIR.Enabled = true;
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSubro.Enabled = true;

                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;
                    break;
                case (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro:
                    this.btnResChangeExpense.Enabled = false;
                    this.btnResChangeLoss.Enabled = false;
                    this.btnResCloseSubro.Enabled = false;
                    this.btnResTotalLoss.Enabled = false;

                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;

                    //allow user to close reserve
                    this.btnResClose.Enabled = true;
                    this.btnResClose.ForeColor = System.Drawing.Color.Blue;

                    //allow user to reopen
                    this.btnResReOpen.Enabled = true;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.Blue;

                    //allow user to view transactions for this reserve
                    this.btnResTransactions.Enabled = true;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;

                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    this.btnResCloseSalvage.Enabled = true;
                    this.btnResCloseDeduct.Enabled = true;
                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.Blue;
                    //this.btnResCloseSIR.Enabled = true;
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSubro.Enabled = true;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    break;
                case (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Deductibles:
                    this.btnResChangeExpense.Enabled = false;
                    this.btnResChangeLoss.Enabled = false;
                    this.btnResCloseDeduct.Enabled = false;
                    this.btnResTotalLoss.Enabled = false;
                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;

                    //allow user to close reserve
                    this.btnResClose.Enabled = true;
                    this.btnResClose.ForeColor = System.Drawing.Color.DarkBlue;

                    //allow user to reopen
                    this.btnResReOpen.Enabled = true;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.Blue;

                    //allow user to view transactions for this reserve
                    this.btnResTransactions.Enabled = true;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;

                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    this.btnResCloseSalvage.Enabled = true;
                    this.btnResCloseSubro.Enabled = true;
                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.Blue;
                    //this.btnResCloseSIR.Enabled = true;
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSubro.Enabled = true;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;
                    break;
                case (int)modGeneratedEnums.ReserveStatus.Closed_Pending_SIR_Recovery:
                    this.btnResChangeExpense.Enabled = false;
                    this.btnResChangeLoss.Enabled = false;
                    this.btnResTotalLoss.Enabled = false;
                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;

                    //allow user to close reserve
                    this.btnResClose.Enabled = true;
                    this.btnResClose.ForeColor = System.Drawing.Color.Blue;

                    //allow user to reopen
                    this.btnResReOpen.Enabled = true;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.Blue;

                    //allow user to view transactions for this reserve
                    this.btnResTransactions.Enabled = true;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;

                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    this.btnResCloseSalvage.Enabled = true;
                    this.btnResCloseSubro.Enabled = true;
                    this.btnResCloseDeduct.Enabled = true;
                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.Blue;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.Blue;
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSubro.Enabled = true;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;
                    break;
                case (int)modGeneratedEnums.ReserveStatus.Void:
                    this.btnResChangeExpense.Enabled = false;
                    this.btnResChangeLoss.Enabled = false;
                    this.btnResTotalLoss.Enabled = false;
                    this.btnResClose.Enabled = false;
                    this.btnResReOpen.Enabled = false;
                    this.btnResVoid.Enabled = false;
                    this.btnResTransactions.Enabled = false;
                    this.btnResDraft.Enabled = false;
                    this.btnResPendingDraft.Enabled = false;
                    this.btnResCloseSalvage.Enabled = false;
                    this.btnResCloseSubro.Enabled = false;
                    this.btnResCloseDeduct.Enabled = false;
                    this.btnResDeductibles.Enabled = false;
                    this.btnResSalvage.Enabled = false;
                    this.btnResSubro.Enabled = false;
                    this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResClose.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResReOpen.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResTransactions.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResDraft.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResPendingDraft.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResCloseSalvage.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResCloseSubro.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResCloseDeduct.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.DarkGray;
                    this.btnResSubro.ForeColor = System.Drawing.Color.DarkGray;
                    break;
                default:
                    break;



            }

            if (this.Claim.isClosed)
            {
                this.btnResChangeExpense.Enabled = false;
                this.btnResChangeLoss.Enabled = false;
                this.btnResClose.Enabled = false;
                this.btnResReOpen.Enabled = false;
                this.btnResVoid.Enabled = false;
                this.btnResCloseSalvage.Enabled = false;
                this.btnResCloseSubro.Enabled = false;
                this.btnResCloseDeduct.Enabled = false;
                this.btnResTotalLoss.Enabled = false;
                this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResClose.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResReOpen.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResCloseSalvage.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResCloseSubro.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResCloseDeduct.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;

                if (!this.reserveLocked && !this.claimLocked)
                {
                    //allow user to issue draft for this reserve
                    this.btnResDraft.Enabled = true;
                    this.btnResDraft.ForeColor = System.Drawing.Color.Blue;

                    //allow user to close the reserve pending salvage, subro, deductible or SIR recovery
                    //this.btnResSIR.Enabled = true;
                    //this.btnResCreditSIR.Enabled = true;

                    //allow user to recover deductibles or salvage
                    this.btnResDeductibles.Enabled = true;
                    this.btnResSalvage.Enabled = true;
                    this.btnResSubro.Enabled = true;
                    this.btnResDeductibles.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSalvage.ForeColor = System.Drawing.Color.Blue;
                    this.btnResSubro.ForeColor = System.Drawing.Color.Blue;
                }

                
            }

            //enable/disable controls based on selected reserve having locked transaction
            if (res.NoTransactionLocks > 0)
            {
                //this.btnResDraft.Enabled = false;
                this.btnResReOpen.Enabled = false;
                this.btnResVoid.Enabled = false;
                this.btnResDraft.Enabled = false;
                this.btnResCloseSalvage.Enabled = false;
                this.btnResCloseSubro.Enabled = false;
                this.btnResCloseDeduct.Enabled = false;
                this.btnResDeductibles.Enabled = false;
                this.btnResSalvage.Enabled = false;
                this.btnResSubro.Enabled = false;
                this.btnResReOpen.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResDraft.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResCloseSalvage.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResCloseSubro.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResCloseDeduct.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResDeductibles.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResSalvage.ForeColor = System.Drawing.Color.DarkGray;
                this.btnResSubro.ForeColor = System.Drawing.Color.DarkGray;

            }
            MedCsXSystem mySystem = new MedCsXSystem();
            if (mySystem.CurrentUser.isManagement())
            {
                if (DateTime.Now.Year != this.Claim.DateOfLoss.Year)
                { 
                    this.btnResRecon.Enabled = true;      //date of loss year is not the current year
                    this.btnResRecon.ForeColor = System.Drawing.Color.Blue;
                }
                else if (!Functions.IsCurrentQuarter(this.Claim.DateOfLoss))
                { 
                    this.btnResRecon.Enabled = true;      //date of loss year is the current year - not the current quarter
                    this.btnResRecon.ForeColor = System.Drawing.Color.Blue;
                }
                else
                { 
                    this.btnResRecon.Enabled = false;      //date of loss year is the current year and in the current quarter
                    this.btnResRecon.ForeColor = System.Drawing.Color.DarkGray;
                }
            }
            else
            { 
                this.btnResRecon.Enabled = false;      //not a manager
                this.btnResRecon.ForeColor = System.Drawing.Color.DarkGray;
            }
        }

        private void SetTitle()
        {
            //create the claim type description
            string claimTypeDescription = this.Claim.ClaimType.Description,
                subClaimTypeDescription = this.Claim.SubClaimType.Description,
                myDescription = "";

            if (claimTypeDescription == subClaimTypeDescription)
                myDescription = claimTypeDescription;
            else
                myDescription = claimTypeDescription + " " + subClaimTypeDescription;
            //string myClaimStatus = this.Claim.ClaimStatus().ToString()

            string tmpStatus = "";

            switch (this.Claim.ClaimStatusId)
            {
                case 0:
                {
                        tmpStatus = "Undefined";
                        break;
                }
                case 4:
                {
                    tmpStatus = "Claim Open";
                    break;
                }
                case 5:
                {
                    tmpStatus = "Claim Open Without Policy";
                    break;
                }
                case 6:
                    {
                        tmpStatus = "Claim Closed";
                        break;
                    }
                case 7:
                    {
                        tmpStatus = "Claim Reopen";
                        break;
                    }
                default:
                    tmpStatus = "";
                    break;
                 
            }

            this.Page.Title = "Claim " + this.Claim.DisplayClaimId + " - " + myDescription + " - " + tmpStatus;
            if (this.claimLocked)
                this.Page.Title += " *** CLAIM IS LOCKED ***";
        }

        private void InitializeClaimChoices()
        {
            if (Request.QueryString["Ptabindex"] != null)
            {
                TabContainer1.ActiveTabIndex = Convert.ToInt32(Request.QueryString["Ptabindex"]);
                TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
            }
            else if (fileNoteId == 0)
            {
                if (this.isNew == true)
                {
                    TabContainer1.ActiveTabIndex = 0;
                    TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                }
                else
                {
                    //not a new claim, display reserves tab
                    TabContainer1.ActiveTabIndex = 7;
                    TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                }

              
            }
            else
            {   //we're coming from an Alert related to a File Note - display file note
            //    TabContainer1.ActiveTabIndex = 17;
            //    TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);

                //if coming from an Alert related to a Reserve/Transaction Lock - display reserves
                FileNote f = new FileNote(fileNoteId);
                if (f.isForReserveLock || f.isForTransactionLock)
                {
                    reserveId = f.ReserveId;
                    TabContainer1.ActiveTabIndex = 7;
                    TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                }
            }
        }

        private void UpdateLossPayee()
        {
            if (this.insuredVehicle.LossPayeeName1 == "")
                this.txtGIClDetailLossPayee.Text = "";
            else
                this.txtGIClDetailLossPayee.Text = "Loss Payee";
        }

        private void LoadClaimTypeSpecificData()
        {

            try
            {
                if (!Page.IsPostBack)
                { 
                    if (this.Claim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Auto)
                    {
                        //load general info comboboxes
                        mf.LoadTypeTableDDL(this.cboGIClDetailRptType, "Reported_By_Type", true, "Reported_By_Type_Id=0 OR Claim_Type_Id=" + this.Claim.ClaimTypeId.ToString());
                        mf.LoadTypeTableDDL(this.cboGIClDetailAccidentType, "Accident_Type",  true, "Accident_Type_Id=0 OR Claim_Type_Id=" + this.Claim.ClaimTypeId.ToString());
                        mf.LoadTypeTableDDL(this.cboGIClDetailatFault, "At_Fault_type", true);
                        mf.LoadStateDDL(this.cboGIClDetailLossState);
                       // mf.LoadTypeTableComboBox(this.cboGIClDetailLossState, "State", true);
                    }
                    else
                    {
                        //mf.LoadTypeTableComboBox(this.cbGenInfoCompanyRepByType, true, "Reported_By_Type_Id=0 OR Claim_Type_Id=" + this.Claim.ClaimTypeId.ToString());
                        //mf.LoadTypeTableComboBox(this.cbGenInfoCompanyAccidentType, true, "Accident_Type_Id=0 OR Claim_Type_Id=" + this.Claim.ClaimTypeId.ToString());
                        //mf.LoadTypeTableComboBox(this.cbGenInfoCompanyAtFault, true);
                        //mf.LoadStateComboBox(this.cbGenInfoCompanyLossState);
                        mf.LoadTypeTableDDL(this.cbGenInfoCompanyRepByType, "Reported_By_Type", true, "Reported_By_Type_Id=0 OR Claim_Type_Id=" + this.Claim.ClaimTypeId.ToString());
                        mf.LoadTypeTableDDL(this.cbGenInfoCompanyAccidentType, "Accident_Type", true, "Accident_Type_Id=0 OR Claim_Type_Id=" + this.Claim.ClaimTypeId.ToString());
                        mf.LoadTypeTableDDL(this.cbGenInfoCompanyAtFault, "At_Fault_type", true);
                        mf.LoadStateDDL(this.cbGenInfoCompanyLossState);
                    }
                }

                SetTitle();
                DisplayClaimData();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

       

        private void LoadClaim()
        {
            this.Claim = new MedCsxLogic.Claim(_claimId);  //get the claim object
            this.UpdateGeneralFrame();  //update the general info tab with the claim data

            this.insurePersonId = this.Claim.InsuredPersonId;
            //set variables from open claim
            this.driverPersonId = this.Claim.DriverPersonId;
            this.ownerPersonId = this.Claim.OwnerPersonId;
            this.insuredVehicle = this.Claim.InsuredVehicle;

            if (this.isNew)
            {   //reset variables if new claim
                this.Claim.DriverPersonId = 0;
                this.Claim.OwnerPersonId = 0;
                this.Claim.InsuredVehicleId = 0;
            }

            this.CheckFraudClaim();
        }
        private void DisplayPropertyDamage()
        {
            bool propertyLoaded = LoadPropertyDamage();

            if (this.claimLocked || this.Claim.isClosed)
                return;

           
        }

        private bool LoadPropertyDamage()
        {
            gvPropertyDamage.DataSource = null;  
            DataRowsDT = new DataTable();
            DataRowsDT = this.Claim.PropertyDamageRows;

           
            
                gvPropertyDamage.DataSource = DataRowsDT;
                gvPropertyDamage.DataBind();

            if (DataRowsDT.Rows.Count > 0)
            {
                gvPropertyDamage.SelectedIndex = 0;
                pdDescription.Visible = true;
                gvPropertyDamage_SelectedIndexChanged(null, null);

            }
                



                return true;
        }

        private void DisplayWitnessPassenger()
        {
            bool witnessPassengerLoaded = LoadWitnessPassenger();

            if (this.claimLocked || this.Claim.isClosed)
                return;
        }

        private bool LoadWitnessPassenger()
        {
            //grWitnessPassenger.ItemsSource = null;  //disassociate the array with the ItemSource
            //DataRows = new List<Hashtable>();
            DataRowsDT = mm.GetMyWitnessPassenger(this._claimId);

            //grWitnessPassenger.Items.Clear();
           
               gvWitness.DataSource = DataRowsDT;
               gvWitness.DataBind();
               //gvWitness.SelectedIndex = 0;
          
            return true;
        }

        private void DisplayInjured()
        {
            bool injuredLoaded = LoadInjured();

            if (this.claimLocked || this.Claim.isClosed)
                return;
        }

        private bool LoadInjured()
        {
            gvInjured.DataSource = null;  //disassociate the array with the ItemSource
            DataRowsDT = new DataTable();
            DataRowsDT = this.Claim.InjuredRows();

            gvInjured.DataSource = DataRowsDT;
            gvInjured.DataBind();
            //gvInjured.SelectedIndex = 0;
          
            return true;
        }

        private void DisplayFileActivity()
        {
           
            this.LoadFileActivityType();
            this.LoadFileActivityClaimants();       //load claimants into the combo box
            this.LoadFileNotes();       //load file notes in grid
            this.fileActivityLoading = false;       //done loading the file activity list
        }

        private void LoadFileActivityType()
        {
            //clear the combo box
            this.cboFileActivityType.Items.Clear();

           
            //fill the combo box
            mf.LoadTypeIntoComboBox("File_Note_Type", this.cboFileActivityType, "Description");
            this.cboFileActivityType.Items.Insert(0, "All");
            this.cboFileActivityType.SelectedIndex = 0;
        }

        private void LoadFileActivityClaimants()
        {
            //clear the combo box
            this.cboFileActivityClaimant.Items.Clear();
            this.cboFileActivityClaimant.Items.Add("All");  //add "All" as element 0

            //fill the combo box with the claimants
            this.cboFileActivityClaimant.DataSource = Claim.Claimants;
            this.cboFileActivityClaimant.DataValueField = "Id";
            this.cboFileActivityClaimant.DataTextField = "Name";
            this.cboFileActivityClaimant.DataBind();
            cboFileActivityClaimant.Items.Insert(0, new ListItem("All"));

            //foreach (Claimant cl in this.Claim.Claimants)
            //    this.cboFileActivityClaimant.Items.Add(cl.Name);

            this.cboFileActivityClaimant.SelectedIndex = 0;

         
        }
      
        private void LoadFileNotes()
        {
            //get the file notes
           DataTable fileNotes = this.myClaim.GetFileNotes(this.fileNoteTypeId, this.fileClaimantId, this.fileReserveId);
           DataTable newFileNotes = fileNotes.Copy();
            //while (newFileNotes.Rows.Count > 0)
            //{
            //    newFileNotes.Rows[0].Delete();
            //}

            newFileNotes.Clear();

            /*if Show Diary Only is checked, remove non-diary file notes.
             * change 1/1/1900 to blank in Diary Due Date*/
            foreach (DataRow fn in fileNotes.Rows)
            {
                if ((DateTime)fn["Diary_Due_Date"] <= ml.DEFAULT_DATE)
                    fn["Diary_Due_Date"] = ml.DEFAULT_DATE;  //set default date to blank

                if ((bool)chkFileActivityDiariesOnly.Checked)
                {
                    if ((string)fn["File_Note_Type"] == "Diary")
                        newFileNotes.Rows.Add(fn.ItemArray);  //newFileNotes.Rows.Add(fn);
                       
                }
                else
                    newFileNotes.Rows.Add(fn.ItemArray);  //newFileNotes.Rows.Add(fn);
            }
            this.fileNoteNum = newFileNotes.Rows.Count;

            if (tbFileActivitySearch.Text.Length > 0)
            {

                DataTable dt = new DataTable();
                DataTable values = newFileNotes;
                var rows = values.AsEnumerable().Where
                (row => row.Field<string>("File_Note_Text").ToUpper().Contains(tbFileActivitySearch.Text.Trim().ToUpper()));//get the rows where the status is equal to action

                if (rows.Any())
                {
                    newFileNotes = rows.CopyToDataTable<DataRow>();//Copying the rows into the DataTable as DataRow
                }


                //gvFileNotes.DataSource = dt;
                //gvFileNotes.DataBind();

                //Session["FileNotes" + _claimId + "Table"] = dt;
            }



            //populate grid
            gvFileNotes.DataSource = newFileNotes;
            gvFileNotes.DataBind();

            if (gvFileNotes.Rows.Count > 0)
            {

                int nRowIndex = gvFileNotes.Rows.Count - 1;
                int nColumnIndex = 3;

                gvFileNotes.SelectedIndex = nRowIndex;
                gvFileNotes_SelectedIndexChanged(null, null);


            }
            
            Session["FileNotes" + _claimId + "Table"] = newFileNotes;

            //if (hasUrgentAlerts)
            //{
            //    Resort();
            //}

            //this.gvFileNotes.SelectedIndex = 0;

            
            //else
            //    this.SelectFileNoteIdRow(this.fileNoteId);
        }

        //public void Resort()
        //{
        //    DataTable dt = Session["FileNotes" + _claimId + "Table"] as DataTable;
        //    dt.DefaultView.Sort = "Urgent" + " " + "DESC";
        //    grAlerts.DataSource = dt;
        //    grAlerts.DataBind();

        //    Session["FileNotes" + _claimId + "Table"] = dt;
        //}

        private void DisplayClaimants()
        {
            bool claimantsLoaded = LoadClaimants();

            if (this.claimLocked || this.Claim.isClosed)
                return;
        }

        private bool LoadClaimants()
        {
            DataRowsDT = mm.GetMyClaimants(this._claimId);

            gvClaimant.DataSource = null;

            gvClaimant.DataSource = DataRowsDT;
            gvClaimant.DataBind();
           // gvClaimant.SelectedIndex = 0;
           
            return true;
        }

        private void DisplayVehicles()
        {
            bool vehiclesLoaded = LoadVehicles();

            if (this.claimLocked || this.Claim.isClosed)
                return;
        }

        private bool LoadVehicles()
        {
            DataRows = this.Claim.Vehicles;
            if (DataRows.Count > 0)
            {
                gvVehicles.DataSource = mm.LoadVehicles(this.Claim.Vehicles);
                gvVehicles.DataBind();

                //gvVehicles.SelectedIndex = 0;
            }
           
               

           
            return true;
        }

        public void DisplayReserves()
        {
            
            this.DisableReserveControls();      //disable the reserve links

            bool reservesLoaded = this.LoadReserves();

<<<<<<< HEAD
            if ((this.claimLocked || this.Claim.isClosed) && (this.myReservesList.Count > 0))
=======
<<<<<<< HEAD
            if ((this.claimLocked || this.Claim.isClosed) && (this.myReservesList.Count > 0))
=======
            if ((this.claimLocked || this.Claim.isClosed) && this.myReservesList.Count > 0)
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            { 
                this.btnResTransactions.Enabled = true;
                this.btnResTransactions.ForeColor = System.Drawing.Color.Blue;
            }
           
        }

        private bool LoadReserves()
        {

            DataRowsDT = (DataTable)mm.GetReservesByClaim(this._claimId);
<<<<<<< HEAD
            Session["Reserves" + _claimId + "Table"] = DataRowsDT;
=======
<<<<<<< HEAD
            Session["Reserves" + _claimId + "Table"] = DataRowsDT;
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            grReserves.DataSource = LoadReserves(DataRowsDT);
            grReserves.DataBind();
            grReserves.SelectedIndex = -1;
            // grReserves_SelectedIndexChanged(grReserves, EventArgs.Empty);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            try
            {
                string sessionName = "reserve_Id" + this._claimId.ToString();

                if (Session[sessionName] != null)
                {

                    string[] a = Session[sessionName].ToString().Split(' ');
                    //if (Convert.ToInt32(a[1]) == _claimId)
                    //{ 
                        grReserves.SelectedIndex = SetSelection(Convert.ToInt32(Convert.ToInt32(a[0])), 4);
                        grReserves_SelectedIndexChanged(null, null);
                        Session[sessionName] = null;
                    //}
                }
                else
                {
                    grReserves.SelectedIndex = SetSelection(this.reserveId, 4);
                    grReserves_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                grReserves.SelectedIndex = SetSelection(this.reserveId, 4);
                grReserves_SelectedIndexChanged(null, null);
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = null;
                mf.ProcessError(ex);
            }




<<<<<<< HEAD
=======
=======
            
                grReserves.SelectedIndex = SetSelection(this.reserveId, 4);
                grReserves_SelectedIndexChanged(null, null);
           
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            return true;
        }

        private IEnumerable LoadReserves(DataTable DataRows)
        {
            myReservesList = new List<ReserveGrid>();
            try
            {
                foreach (DataRow rData in DataRows.Rows)
                {
                    ReserveGrid r = new ReserveGrid();
                    r.reserveId = (int)rData["Reserve_Id"];
                    r.reserveImage = (int)rData["Image_Index"];
                    r.claimant = (string)rData["Claimant"];
                    r.reserveType = (string)rData["Reserve_Type"];
                    r.reserveStatus = (string)rData["Reserve_Status"];
                    r.reservingInd = (string)rData["Reserving"];
                    r.adjusterName = (string)rData["Assigned_To"];
                    r.dtReported = (DateTime)rData["Date_Open"];
                    r.netLossReserve = (double)((decimal)rData["Net_Loss_Reserve"]);
                    r.netLossPaid = (double)((decimal)rData["Paid_Loss"]);
                    r.netExpenseReserve = (double)((decimal)rData["Net_Expense_Reserve"]);
                    r.netExpensePaid = (double)((decimal)rData["Paid_Expense"]);
                    myReservesList.Add(r);
                }
                return myReservesList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myReservesList;
            }
        }

        private void DisplayTasks()
        {
            try
            {
                bool taskLoaded = LoadTask();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadTask()
        {
            this.gvTasks.DataSource = mm.GetMyTasks(0, this._claimId, 1);
            gvTasks.DataBind();
            this.gvTasks.SelectedIndex = 0;
            return true;
        }

        private void DisplayPolicyData()
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.policyDataLoading = true;
                bool policyLoaded,
                    notesLoaded;

                if (this.Claim.PolicyNo != "")
                {
                    policyLoaded = LoadPolicy();
                    notesLoaded = LoadNotes();
                }

                this.policyDataLoading = false;
                if (gvPolicyData.Rows.Count > 0)
                { 
                    this.gvPolicyData.SelectedIndex = 0;
                    gvPolicyData_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadPolicy()
        {
           
            this.gvPolicyData.DataSource = mm.GetPolicyData(this._claimId);
            this.gvPolicyData.DataBind();

            Session["Policy" + _claimId + "Table"] = gvPolicyData.DataSource as DataTable;

            return true;
        }

        private bool LoadNotes()
        {

            //TODO
            gvPolNotes.DataSource = LoadNotes(mm.GetPolicyNotes(this.Claim.PolicyNo));
            gvPolNotes.DataBind();
            //tiPolNotes.Header = "Notes (" + grPolNotes.Items.Count.ToString() + ")";
            return true;
        }

        private IEnumerable LoadNotes(List<Hashtable> list)
        {
            myPolicyNoteList = new List<policyGrid>();
            try
            {
                foreach (Hashtable nData in list)
                {
                    policyGrid pg = new policyGrid();
                    pg.id = (int)nData["Note_ID"];
                    pg.policyNo = (string)nData["No_Policy"];
                    pg.userId = (string)nData["UserID"];
                    pg.dtEffective = (DateTime)nData["Dt_Created"];
                    pg.transaction = (string)nData["Note"];
                    pg.isWeb = (bool)nData["IsWeb"];
                    myPolicyNoteList.Add(pg);
                }
                return myPolicyNoteList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myPolicyNoteList;
            }
        }

        private void DisplayAppraisals()
        {
            try
            {
                this.gvAppraisals.DataSource = mm.GetAppraisalTasks(this._claimId);
                this.gvAppraisals.DataBind();
                //this.gvAppraisals.SelectedIndex = 0;
               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private IEnumerable LoadAppraisals(List<Hashtable> list)
        {
            myAppraisalList = new List<ReserveGrid>();
            try
            {
                foreach (Hashtable aData in list)
                {
                    ReserveGrid rg = new ReserveGrid();
                    rg.reserveId = (int)aData["appraisal_task_id"];
                    rg.insName = (string)aData["vehicle"];
                    rg.reserveStatus = (string)aData["appraisal_status"];
                    rg.dtReported = (DateTime)aData["request_date"];
                    rg.description = (string)aData["task_description"];
                    rg.reserveType = (string)aData["type_of_investigation_needed"];
                    rg.adjCode = (string)aData["adj_code"];
                    rg.reservingInd = (string)aData["type_of_claim"];
                    rg.claimant = (string)aData["claimant"];
                    rg.instructions = (string)aData["special_instructions"];
                    rg.reserveImage = (int)aData["Image_Index"];
                    myAppraisalList.Add(rg);
                }
                return myAppraisalList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAppraisalList;
            }
        }

        private void DisplayDocuments()
        {
            try
            {
                if (!this.doneLoading)
                    return;

                mm.UpdateLastActivityTime();

                //set available documents count
                //this.rbAvailableDocCount.Text = "Available (" + DocumentType.AvailableDocumentsCount(this.Claim.ClaimTypeId) + ")";

                List<ReserveGrid> docList = new List<ReserveGrid>();
                if ((bool)this.rbAvailableDocs.Checked)
                {   //load available documents
                    this.btnCreateDocument.Visible = true;
                    this.btnOpenDocument.Visible = false;
                    this.btnCancelPending.Visible = false;
                    this.btnMarkDocumentComplete.Visible = false;

                    foreach (Hashtable row in DocumentType.AvailableDocs(this.Claim.ClaimTypeId))
                    {
                        DocumentType dt = new DocumentType((int)row["Document_Type_Id"]);
                        docList.Add(this.CreateDocumentRow(dt));
                    }
                }
                else
                {
                    if ((bool)this.rbPendingDocs.Checked)
                        btnMarkDocumentComplete.Visible = true;
                    else
                        btnMarkDocumentComplete.Visible = false;



                        this.btnCreateDocument.Visible = false;
                    this.btnOpenDocument.Visible = true;

                    if ((bool)this.rbPendingDocs.Checked)
                        this.btnCancelPending.Visible = true;
                    else
                        this.btnCancelPending.Visible = false;
                }

                int pendingCount = 0,
                    completedCount = 0;

                //get documents in an array list
                ArrayList documents = new ArrayList();
                if (this.Claim != null)
                {
                    documents = this.Claim.ChildObjects(typeof(Document));
                    if (documents == null)
                        documents = new ArrayList();
                }

                foreach (Document d in documents)
                {
                    if (d.DocumentStatusId == (int)modGeneratedEnums.DocumentStatus.Printed)
                    {
                        completedCount++;
                        if ((bool)this.rbCompletedDocs.Checked)
                            docList.Add(this.CreateDocumentRow(d));
                    }
                    else if (d.DocumentStatusId == (int)modGeneratedEnums.DocumentStatus.Pending)
                    {
                        pendingCount++;
                        if ((bool)this.rbPendingDocs.Checked)
                            docList.Add(this.CreateDocumentRow(d));
                    }
                }

                this.gvDocuments.DataSource = docList;     //load grid
                this.gvDocuments.DataBind();

                if (gvDocuments.Rows.Count > 0)
                {
                    this.gvDocuments.SelectedIndex = 0;     //select 1st row
                    gvDocuments_SelectedIndexChanged(null, null);
                }
                else
                {
                    lblDocName.Text = "";
                }

                Session["Documents" + _claimId + "Table"] = docList;

                //this.rbCompletedDocs.Content = "Completed (" + completedCount.ToString() + ")";
                //this.rbPendingDocs.Content = "Pending (" + pendingCount.ToString() + ")";
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }


        //private void DisplayDocuments()
        //{
        //    try
        //    {
        //        //if (!this.doneLoading)
        //        //    return;

        //        mm.UpdateLastActivityTime();

        //        //set available documents count
        //        //this.rbAvailableDocs.Text = "Available (" + DocumentType.AvailableDocumentsCount(this.Claim.ClaimTypeId) + ")";

        //        List<ReserveGrid> docList = new List<ReserveGrid>();
        //        if (rblDocuments.SelectedValue == "3")  //availale
        //        {
        //            this.btnCreateDocument.Visible = true;
        //            this.btnOpenDocument.Visible = false;
        //            this.btnCancelPending.Visible = false;

        //            //gvDocuments.DataSource = DocumentType.AvailableDocuments(this.Claim.ClaimTypeId);
        //            //gvDocuments.DataBind();

        //            foreach (Hashtable row in DocumentType.AvailableDocs(this.Claim.ClaimTypeId))
        //            {
        //                DocumentType dt = new DocumentType((int)row["Document_Type_Id"]);
        //                docList.Add(this.CreateDocumentRow(dt));
        //            }

        //            //gvDocuments.DataSource = DocumentType.AvailableDocuments(this.Claim.ClaimTypeId);
        //            //gvDocuments.DataBind();
        //            //foreach (Hashtable row in DocumentType.AvailableDocuments(this.Claim.ClaimTypeId))
        //            //{
        //            //    DocumentType dt = new DocumentType((int)row["Document_Type_Id"]);
        //            //    docList.Add(this.CreateDocumentRow(dt));
        //            //}
        //        }
        //        else if ((rblDocuments.SelectedValue == "2"))  //Complete
        //        {



        //            //if ((bool)this.rbPendingDocs.IsChecked)
        //            //    this.btnCancelPending.Visibility = Visibility.Visible;
        //            //else
        //            //    this.btnCancelPending.Visibility = Visibility.Collapsed;
        //        }

        //        //int pendingCount = 0,
        //        //    completedCount = 0;

        //        ////get documents in an array list
        //        //ArrayList documents = new ArrayList();
        //        //if (this.Claim != null)
        //        //{
        //        //    documents = this.Claim.ChildObjects(typeof(Document));
        //        //    if (documents == null)
        //        //        documents = new ArrayList();
        //        //}

        //        //foreach (Document d in documents)
        //        //{
        //        //    if (d.DocumentStatusId == (int)modGeneratedEnums.DocumentStatus.Printed)
        //        //    {
        //        //        completedCount++;
        //        //        if ((bool)this.rbCompletedDocs.IsChecked)
        //        //            docList.Add(this.CreateDocumentRow(d));
        //        //    }
        //        //    else if (d.DocumentStatusId == (int)modGeneratedEnums.DocumentStatus.Pending)
        //        //    {
        //        //        pendingCount++;
        //        //        if ((bool)this.rbPendingDocs.IsChecked)
        //        //            docList.Add(this.CreateDocumentRow(d));
        //        //    }
        //        //}

        //        //this.grDocuments.ItemsSource = docList;     //load grid
        //        //this.grDocuments.Items.Refresh();
        //        //this.grDocuments.SelectedIndex = 0;     //select 1st row

        //        //this.rbCompletedDocs.Content = "Completed (" + completedCount.ToString() + ")";
        //        //this.rbPendingDocs.Content = "Pending (" + pendingCount.ToString() + ")";
        //    }
        //    catch (Exception ex)
        //    {
        //        mf.ProcessError(ex);
        //    }
        //}

        private ReserveGrid CreateDocumentRow(Document d)
        {
            ReserveGrid rtn = new ReserveGrid();
            rtn.id = d.Id;
            rtn.reserveType = d.DocumentType.Description;
            rtn.reserveImage = d.DocumentType.ImageIndex;

            if (d.Claimant == null)
                rtn.claimant = "";
            else
                rtn.claimant = d.Claimant.Name;

            if (d.DatePrinted != ModMain.DEFAULT_DATE)
                rtn.dtReported = d.DatePrinted;

            return rtn;
        }

        private ReserveGrid CreateDocumentRow(DocumentType dt)
        {
            ReserveGrid rtn = new ReserveGrid();
            rtn.id = dt.Id;
            rtn.reserveType = dt.Description;
            rtn.reserveImage = dt.ImageIndex;
            rtn.claimant = "";

            return rtn;
        }

        private void DisplayPhoneNumbers()
        {
            try
            {
                this.phoneNumbersLoading = true;

                string oldPhoneNumberName = this.phoneNumberName;
                bool phonesLoaded = LoadPhones();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
                this.phoneNumbersLoading = false;
            }
        }

        private bool LoadPhones()
        {
            this.grPhoneNumbers.DataSource = mm.GetPhoneNumbersForClaim(this._claimId);
            this.grPhoneNumbers.DataBind();
            this.grPhoneNumbers.SelectedIndex = 0;
            return true;
        }

        private void DisplayDuplicateClaims()
        {
            try
            {
                mm.UpdateLastActivityTime();

                //get the duplicate claim list
                List<ReserveGrid> dupClaimList = new List<ReserveGrid>();
                foreach (PossibleDuplicateClaim d in this.Claim.PossibleDuplicateClaims())
                {
                    MedCsxLogic.Claim dupClaim = d.DuplicateClaim();
                    ReserveGrid dClaim = new ReserveGrid();
                    dClaim.id = dupClaim.Id;
                    dClaim.adjCode = dupClaim.DisplayClaimId;
                    dClaim.description = d.Description;
                    dClaim.policyNo = dupClaim.PolicyNo;
                    dClaim.dtOfLoss = dupClaim.DateOfLoss;
                    dClaim.reserveImage = d.PossibleDuplicateClaimType().ImageIndex;
                    dupClaimList.Add(dClaim);
                }

                this.gvPossibleDuplicates.DataSource = dupClaimList;
                this.gvPossibleDuplicates.DataBind();

               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        internal void DisplayDemandOffer()
        {
            try
            {
                mm.UpdateLastActivityTime();

                //disable adding demands/offeres unless demand summaries are present
                //this.btnAddDemandHistory.IsEnabled = false;
                //this.btnAddOfferHistory.IsEnabled = false;

                grDemandSummary.DataSource = LoadDemandSummary(this.myClaim.Demands());
                grDemandSummary.DataBind();

                if (grDemandSummary.Rows.Count > 0)
                { 
                    grDemandSummary.SelectedIndex = 0;
                    grDemandSummary_SelectedIndexChanged(null, null);
                }
                //gvDemandHistory.Items.Refresh();

                //if (this.myDemand == null)
                //{
                //    if (gvDemandSummary.Items.Count > 0)
                //        this.gvDemandSummary.SelectedIndex = 0;
                //}
                //else
                //    this.grDemandSummary.SelectedIndex = SetSelection(this.myDemand.Id, 2, this.myDemandSummaryList);


            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        public void LoadDemandSummary(int value)
        {
            MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim();
            grDemandSummary.DataSource = LoadDemandSummary(this.myClaim.Demands());
            grDemandSummary.DataBind();
        }

        private IEnumerable LoadDemandSummary(List<Hashtable> list)
        {
            myDemandSummaryList = new List<ClaimantGrid>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    ClaimantGrid cg = new ClaimantGrid();
                    cg.claimantId = (int)dData["Demand_Id"];
                    cg.personType = (string)dData["Reserve_Type"];
                    cg.payee = (string)dData["Vendor"];
                    cg.fullName = (string)dData["Claimant"];
                    cg.imageIdx = (int)dData["Image_Index"];
                    myDemandSummaryList.Add(cg);
                }
                return myDemandSummaryList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDemandSummaryList;
            }
        }

        private void DisplayDemandOfferHistory()
        {   //display the demand / offer history
            try
            {
                bool demandLoaded = LoadDemandOffer();

                //disable/enable the demand history buttons based on the last activity type
                if (this.grDemandHistory.Rows.Count > 0)
                {
                    Int32 index = grDemandHistory.Rows.Count - 1;
                    //ReserveGrid lastRow = (ReserveGrid)grDemandHistory.Rows[grDemandHistory.Rows.Count - 1];

                    if (grDemandHistory.Rows[index].Cells[0].Text  == "Demand")    //last activity type is demand
                    {   //allow user to add an offer
                        this.btnAddDemandHistory.Visible = false;
                        this.btnAddOfferHistory.Visible = true;
                    }
                    if (grDemandHistory.Rows[index].Cells[0].Text == "Offer")    //last activity type is offer
                    {   //allow user to add an demand
                        this.btnAddDemandHistory.Visible = true;
                        this.btnAddOfferHistory.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadDemandOffer()
        {
            this.grDemandHistory.DataSource = LoadDemandOffer(Demand.DemandHistory(this.myDemand.VendorId, this.myDemand.ReserveId));
            this.grDemandHistory.DataBind();
            return true;
        } 

        private IEnumerable LoadDemandOffer(List<Hashtable> list)
        {
            string s;
            myDemandHistoryList = new List<ReserveGrid>();
            try
            {
                foreach (Hashtable dData in list)
                {
                    ReserveGrid cg = new ReserveGrid();
                    cg.id = (int)dData["ID"];
                    cg.reserveType = (string)dData["Type"];
                    cg.dtOfLoss = (DateTime)dData["Date"];
                    s = (string)dData["Amount"];
                    s = s.Trim().Replace("$", "").Replace(",", "");
                    cg.netLossReserve = double.Parse(s);
                    if (dData["Received_Date"].ToString() != "" )
                        cg.dtReported = (DateTime)dData["Received_Date"];
                    if (dData["Due_Date"].ToString() != "")
                        cg.dtDue = (DateTime)dData["Due_Date"];
                    myDemandHistoryList.Add(cg);
                }
                return myDemandHistoryList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myDemandHistoryList;
            }
        }

        private void DisplayLitgation()
        {
            try
            {
                mm.UpdateLastActivityTime();

                bool litLoaded = LoadLitigation();

                //if (this.myLitigation == null)
                //    this.grLitigation.SelectedIndex = 0;
                //else
                //    this.grLitigation.SelectedIndex = SetSelection(this.myLitigation.Id, 6, myLitigationList);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadLitigation()
        {
            this.gvLitigation.DataSource = LoadLitigation(Litigation.LitigationForClaim(this._claimId));
            this.gvLitigation.DataBind();
            return true;
        }

        private IEnumerable LoadLitigation(List<Hashtable> list)
        {
            myLitigationList = new List<LitigationGrid>();
            try
            {
                foreach (Hashtable lData in list)
                {
                    LitigationGrid cg = new LitigationGrid();
                    cg.id = (int)lData["Litigation_Id"];
                    cg.statusId = (int)lData["Litigation_Status_Id"];
                    cg.claimId = (int)lData["Claim_Id"];
                    cg.defenseAttorneyId = (int)lData["Defense_Attorney_Id"];
                    cg.plaintiffAttorneyId = (int)lData["Plaintiff_Attorney_Id"];
                    cg.courtLocationId = (int)lData["Court_Location_Id"];
                    cg.dtFiled = (DateTime)lData["Date_Filed"];
                    cg.dtServed = (DateTime)lData["Date_Served"];
                    cg.resolution = (string)lData["Resolution_Amount"];
                    cg.comments = (string)lData["Comments"];
                    cg.defenseAttorney = (string)lData["Defense_Attorney"];
                    cg.plaintiffAttorney = (string)lData["Plaintiff_Attorney"];
                    cg.location = (string)lData["Court_Location"];
                    cg.status = (string)lData["Status"];
                    cg.imageIndex = (int)lData["Image_Index"];
                    myLitigationList.Add(cg);
                }
                return myLitigationList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myLitigationList;
            }
        }

        //private void DisplaySalvage()
        //{
        //    bool salvageLoaded = LoadSalvage();

        //    if (this.claimLocked || this.Claim.isClosed)
        //        return;
        //}

        private bool LoadSalvage()
        {
            this.gvSalvage.DataSource = mm.GetSalvageForClaim(this._claimId);
            this.gvSalvage.DataBind();
            this.gvSalvage.SelectedIndex = 0;
            return true;
        }

        private void DisplayAlerts(bool AlwaysDisplayAlerts = false)
        {
            try
            {
                mm.UpdateLastActivityTime();

               

                if (!this.LoadAlerts(true, AlwaysDisplayAlerts))
                    return;

                this.EnableDisableAlertsForClaim();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
               
            }
        }

        private void EnableDisableAlertsForClaim()
        {   //enable the open claim button if there are any alerts
            //TODO
            //this.openClaim.IsEnabled = this.gvAlerts.Items.Count > 0;
        }

        private bool LoadAlerts(bool ignoreDate, bool AlwaysDisplayAlerts)
        {   //pulls Alerts for claim
            DataRowsDT = mm.GetMyAlerts(Convert.ToInt32(Session["userID"]), ml.DEFAULT_DATE, true, this._claimId);
            grAlerts.DataSource = DataRowsDT;
            grAlerts.DataBind();


            Session["Alerts" + _claimId + "Table"] = DataRowsDT;

            List<Hashtable> Row2Keep = new List<Hashtable>();
            foreach (DataRow Row in DataRowsDT.Rows)
            {

                DateTime deferredDate = (DateTime)Row["Deferred_Date"];
                DateTime dateSent = (DateTime)Row["Date_Sent"];
                DateTime respondDate = (DateTime)Row["Date_Responded_To"];
                DateTime modifiedDate = (DateTime)Row["Modified_Date"];
                //TODO
                //Row.Add("date_sent", dateSent);
                //if (deferredDate > dateSent)
                //    Row.Add("date_received", deferredDate);
                //else
                //    Row.Add("date_received", dateSent);

                //if ((string)Row["Urgent"] == "Yes")
                //{
                //    if (!Row.ContainsKey("RowColor"))
                //        Row.Add("RowColor", Colors.Salmon);
                //    else
                //        Row["RowColor"] = Colors.Salmon;
                //    Row2Keep.Add(Row);
                //}

                //if ((deferredDate > dateSent) && (deferredDate > DateTime.Now))
                //    Row.Add("Status", "Deferred");
                //else if (respondDate > dateSent)
                //    Row.Add("Status", "Responded");
                //else
                //    Row.Add("Status", "Sent");
            }
            //if ((Row2Keep.Count > 0) && (this.AlertsToMe.IsChecked == true))
            //    DataRows = Row2Keep;
            //grMyAlerts.Items.Clear();
            //TODO   add to onloaded in gridview
            //btnAlertsOpenClaim.IsEnabled = false;

            //if (DataRows.Count > 0)
            //{   //load grid with Alerts 
            //    //this.grAlerts.ItemsSource = LoadAlerts(DataRows);
            //    //btnAlertsOpenClaim.enabled = true;
            //    grAlerts.SelectedIndex = 0;
            //}


            return true;
            
        }

        private IEnumerable LoadAlerts(List<Hashtable> AlertData)
        {
            List<ReserveGrid> ClaimAlerts = new List<ReserveGrid>();
            try
            {
                foreach (Hashtable row in AlertData)
                {
                    ClaimAlerts.Add(new ReserveGrid()
                    {
                        id = (int)row["Alert_Id"],
                        reserveImage = (int)row["Image_Index"],
                        reservingInd = (string)row["Urgent"],
                        adjusterName = (string)row["Sent_From"],
                        claimant = (string)row["Sent_To"],
                        reserveType = (string)row["File_Note_Type"],
                        description = (string)row["File_Note_Text"],
                        dtOfLoss = (DateTime)row["date_sent"],
                        dtReported = (DateTime)row["date_received"],
                        reserveStatus = (string)row["Status"]
                        //rowColor=(int)row["RowColor"]
                    });
                }
                return ClaimAlerts;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return ClaimAlerts;
            }
        }

        private IEnumerable LoadSalvage(List<Hashtable> list)
        {
            ArrayList usedAppraisals = new ArrayList();
            mySalvageList = new List<SalvageGrid>();
            try
            {
                foreach (Hashtable sData in list)
                {
                    if (!usedAppraisals.Contains((int)sData["Appraisal_Request_Id"]))
                    {
                        SalvageGrid rg = new SalvageGrid();
                        rg.id = (int)sData["Salvage_Id"];
                        rg.vendorId = (int)sData["Vendor_Id"];
                        rg.appraisalRequestId = (int)sData["Appraisal_Request_Id"];
                        rg.appraisalTaskId = (int)sData["Appraisal_Task_Id"];
                        mySalvageList.Add(rg);

                        usedAppraisals.Add(rg.appraisalRequestId);
                    }
                }
                return mySalvageList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return mySalvageList;
            }
        }

        public int SetSelection(int Id, int mode, IEnumerable myList = null)
        {
            int selectIdx = 0;
            if (Id == 0)
                return selectIdx;

            switch (mode)
            {
                case 1: //property damage
                    foreach (PropertyDamageGrid pd in myPropertyDamageList)
                    {
                        if (pd.propertyDamageId == Id)
                            break;
                        selectIdx++;
                    }
                    return selectIdx;
                case 2: //witness/passenger, injured, claimants, demands
                    foreach (ClaimantGrid cg in myList)
                    {
                        if (cg.claimantId == Id)
                            break;
                        selectIdx++;
                    }
                    return selectIdx;
                case 3: //vehicles
                    foreach (VehicleGrid vg in myList)
                    {
                        if (vg.vehicleId == Id)
                            break;
                        selectIdx++;
                    }
                    return selectIdx;
                case 4:
                    foreach (ReserveGrid rg in myReservesList)
                    {
                        if (rg.reserveId == Id)
                            break;
                        selectIdx++;
                    }
                    return selectIdx;
                case 5:
                    foreach (SalvageGrid sg in mySalvageList)
                    {
                        if (sg.id == Id)
                            break;
                        selectIdx++;
                    }
                    return selectIdx;
                case 6:
                    foreach (LitigationGrid lg in myList)
                    {
                        if (lg.id == Id)
                            break;
                        selectIdx++;
                    }
                    return selectIdx;
                default:
                    return selectIdx;
            }
        }

        private void DisplayClaimData()
        {
            try
            {
                switch (this.Claim.ClaimTypeId)
                {
                    case (int)modGeneratedEnums.ClaimType.Auto:

                        tiAuto.Visible = true;
                        tiCompany.Visible = false;
                        //checkboxes
                        this.ckbGIClDetailCoverage.Checked = this.Claim.hasCoverageIssue;
                        this.ckbGIClDetailLiability.Checked = this.Claim.hasDisputedLiability;
                        this.ckbGIClDetailIVComp.Checked = this.Claim.isComp;
                        //this.ckbGIClDetailFoxPro.Checked = this.Claim.isFoxproConversion;
                        this.ckbGIClDetailIVColl.Checked = this.Claim.isCollision;
                        this.ckbGIClDetailPerm2Use.Checked = this.Claim.PermissionToUse;
                        this.ckbGIClDetailLitigation.Checked = this.Claim.isInLitigation();
                        this.ckbGIClDetailLrgLoss.Checked = this.Claim.LargeLoss;

                        //text boxes
                        this.tbGIClDetailIVDamage.Text = this.Claim.VehicleDamage;
                        this.tbGIClDetailWhere.Text = this.Claim.WhereSeen;
                        this.tbGIClDetaildtReported.Text = Convert.ToDateTime(this.Claim.DateReported.ToString()).ToString("yyyy-MM-ddTHH:mm"); //this.Claim.DateReported.ToString("MM/dd/yyyy hh:mm:ss tt");
                        this.tbGIClDetailDOL.Text = Convert.ToDateTime(this.Claim.DateOfLoss.ToString()).ToString("yyyy-MM-dd"); //this.Claim.DateOfLoss.ToShortDateString();
                        this.tbGIClDetailTOL.Text = Convert.ToDateTime(this.Claim.DateOfLoss.ToString()).ToString("HH:mm");  //this.Claim.DateOfLoss.ToShortTimeString();
                        this.originalTOL = this.Claim.DateOfLoss.ToShortTimeString();
                        this.tbGIClDetailLossLoc.Text = this.Claim.LossLocation;
                        this.tbGIClDetailLossCity.Text = this.Claim.LossCity;
                        this.tbGIClDetailAuthContact.Text = this.Claim.AuthorityContacted;
                        this.tbGIClDetailRptNo.Text = this.Claim.PoliceReportNumber;
                        this.tbGIClDetailRptBy.Text = this.Claim.ReportedBy;
                        this.tbGIClDetailLossDescr.Text = this.Claim.LossDescription;
                        this.tbGIClDetailComments.Text = this.Claim.Comments;

                        if (!Page.IsPostBack)
                        {
                            //combo boxes selection
                            this.cboGIClDetailRptType.SelectedValue = this.Claim.ReportedByTypeId.ToString();
                            this.cboGIClDetailAccidentType.SelectedValue = this.Claim.AccidentTypeId.ToString();
                            this.cboGIClDetailatFault.SelectedValue = this.Claim.AtFaultTypeId.ToString();
                            this.cboGIClDetailLossState.SelectedValue = this.Claim.LossState_Id.ToString();
                       
                       
                            this.ReloadPersonComboBoxes();
                            this.ReloadVehicleComboBox();
                            this.UpdatePersonVehicleEditButtons();
                        }

                        DisplayCoverages();
                        //TODO
                        //this.UpdateSR22ComboBox();

                        //Text fields
                        this.txtGIClDetailAgent.Text = this.Claim.AgentDescription.Replace("\r\n", "<br />");
                        this.txtGIClDetailAdjuster.Text = this.Claim.AdjusterDescription.Replace("\r\n", "<br />");
                        this.txtGIClDetailOwnerPh.Text = this.UpdatePhoneNo(ownerPersonId, 1);
                        this.txtGIClDetailDriverPh.Text = this.UpdatePhoneNo(driverPersonId, 2);
                        this.txtGIClDetailInsuredPh.Text = this.UpdatePhoneNo(insurePersonId);
                        break;
                    case (int)modGeneratedEnums.ClaimType.Commercial:
                    case (int)modGeneratedEnums.ClaimType.MJi_Habitational:
                        tiCompany.Visible = true;
                        tiAuto.Visible = false;
                        
                        //load location numbers for this claim's policy
                        LoadLocationNumbers();

                        //checkboxes
                        this.ckGenInfoCompanyCoverage.Checked = this.Claim.hasCoverageIssue;
                        this.ckGenInfoCompanyLiability.Checked = this.Claim.hasDisputedLiability;
                        this.ckGenInfoCompanyLargeLoss.Checked = this.Claim.LargeLoss;
                        this.ckGenInfoCompanyLitigation.Checked = this.Claim.isInLitigation();

                        //textboxes
                        this.txtGenInfoCompanyDOL.Text = this.Claim.DateOfLoss.ToString();
                        this.txtGenInfoCompanyDtReported.Text = this.Claim.DateReported.ToString();
                        this.txtGenInfoCompanyDtNotified.Text = this.Claim.NotificationDate.ToString();
                        this.txtGenInfoCompanyAuthContact.Text = this.Claim.AuthorityContacted;
                        this.txtGenInfoCompanyRptNum.Text = this.Claim.PoliceReportNumber;
                        this.txtGenInfoCompanyRptBy.Text = this.Claim.ReportedBy;
                        this.txtGenInfoCompanyLossAddress.Text = this.Claim.LossLocation;
                        this.txtGenInfoCompanyLossCity.Text = this.Claim.LossCity;
                        this.txtGenInfoCompanyLossDescrip.Text = this.Claim.LossDescription;
                        this.txtGenInfoCompanyComments.Text = this.Claim.Comments;
                        this.txtGenInfoCompanyDed.Text = string.Format("{0:##,###,##0}", this.Claim.Deductible);
                        this.txtGenInfoCompanyLimit.Text = string.Format("{0:##,###,##0}", this.Claim.Limit);

                        //combo boxes
                        this.cbGenInfoCompanyRepByType.SelectedIndex = this.Claim.ReportedByTypeId;
                        this.cbGenInfoCompanyAccidentType.SelectedIndex = this.Claim.AccidentTypeId;
                        this.cbGenInfoCompanyLossLoc.SelectedIndex = this.Claim.LossLocationId;
                        this.cbGenInfoCompanyLossLocNbr.SelectedIndex = this.Claim.LossLocationNbr;
                        this.cbGenInfoCompanyLossState.SelectedIndex = this.Claim.LossState_Id;
                        this.cbGenInfoCompanyAtFault.SelectedIndex = this.Claim.AtFaultTypeId;

                        //label
                        this.txtGenInfoCompanyAgent.Text = this.Claim.AgentDescription;
                        this.txtGenInfoCompanyAgent.ToolTip = this.Claim.AgentDescription;

                        //populate the person combo boxes
                        ReloadPersonComboBoxes();

                        //btnGIClDetailNewVehicle.NavigateUrl = "frmVehicle.aspx?claim_id=" + this._claimId.ToString() + "vehicle_id=0";
                        //btnGIClDetailEditVehicle.NavigateUrl = "frmVehicle.aspx?claim_id=" + this._claimId.ToString() + "vehicle_id=" + this.cboGIClDetailVehicle.SelectedValue.ToString();
                        cboGIClDetailVehicle_SelectedIndexChanged(null, null);
                        UpdatePersonVehicleEditButtons();

                        //reload the policy coverages to the screen
                        DisplayCoverages();
                        break;
                    default:
                        break;
                }

                this.EnableDisableGIControls();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }
      
        private void UpdateOwnerPhoneNo()
        {
            this.txtGIClDetailOwnerPh.Text = new Person(ownerPersonId).HomePhone;
            this.cboGIClDetailOwnerPhone.Enabled = true;
            this.cboGIClDetailOwnerPhone.DataSource = GetPhones(ownerPersonId);
            this.cboGIClDetailOwnerPhone.DataTextField = "id";
            this.cboGIClDetailOwnerPhone.DataValueField = "description";
            this.cboGIClDetailOwnerPhone.DataBind();

            this.cboGIClDetailOwnerPhone.SelectedIndex = 0;
            if (this.cboGIClDetailOwnerPhone.Items.Count == 0)
                this.cboGIClDetailOwnerPhone.Enabled = false;
        }

        private void UpdateDriverPhoneNo()
        {
            this.txtGIClDetailDriverPh.Text = new Person(driverPersonId).HomePhone;
            this.cboGIClDetailDriverPhone.Enabled = true;
            this.cboGIClDetailDriverPhone.DataSource = GetPhones(driverPersonId);
            this.cboGIClDetailDriverPhone.DataTextField = "id";
            this.cboGIClDetailDriverPhone.DataValueField = "description";
            this.cboGIClDetailDriverPhone.DataBind();
            
            this.cboGIClDetailDriverPhone.SelectedIndex = 0;
            if (this.cboGIClDetailDriverPhone.Items.Count == 0)
                this.cboGIClDetailDriverPhone.Enabled = false;
        }

      

        private void EnableDisableGIControls()
        {
            if (this.Claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
            {
                this.btnGIClDetailSave.Enabled = false;
                this.btnGIClDetailEditDriver.Enabled = false;
                this.btnGIClDetailNewDriver.Enabled = false;
                this.btnGIClDetailEditOwner.Enabled = false;
                this.btnGIClDetailNewOwner.Enabled = false;
                this.btnGIClDetailEditInsured.Enabled = false;
                this.btnGIClDetailNewInsured.Enabled = false;
                this.btnGIClDetailEditVehicle.Enabled = false;
                this.btnGIClDetailNewVehicle.Enabled = false;
            }
            else
            {
                this.btnGIClDetailSave.Enabled = true;
                this.btnGIClDetailEditDriver.Enabled = true;
                this.btnGIClDetailNewDriver.Enabled = true;
                this.btnGIClDetailEditOwner.Enabled = true;
                this.btnGIClDetailNewOwner.Enabled = true;
                this.btnGIClDetailEditInsured.Enabled = true;
                this.btnGIClDetailNewInsured.Enabled = true;
                this.btnGIClDetailEditVehicle.Enabled = true;
                this.btnGIClDetailNewVehicle.Enabled = true;

                this.UpdatePersonVehicleEditButtons();

                if (this.Claim.PolicyNo == "")
                {
                    this.btnGIClDetailNewInsured.Enabled = true;
                    this.btnGIClDetailNewVehicle.Enabled = true;
                }
                else
                {
                    this.btnGIClDetailNewInsured.Enabled = false;
                    this.btnGIClDetailNewVehicle.Enabled = false;
                    this.btnGIClDetailNewInsured.Enabled = false;
                    this.btnGIClDetailNewVehicle.Enabled = false;
                }
            }
        }

        private void DisableReserveControls()
        {
            this.btnResTransactions.Enabled = false;
            this.btnResChangeExpense.Enabled = false;
            this.btnResChangeLoss.Enabled = false;
            this.btnResReOpen.Enabled = false;
            this.btnResClose.Enabled = false;
            this.btnResVoid.Enabled = false;
            this.btnResTotalLoss.Enabled = false;
            this.btnLockReserveInfo.Enabled = false;
            this.btnUnlockReserve.Enabled = false;
            this.btnResSalvage.Enabled = false;
            this.btnResSubro.Enabled = false;
            this.btnResDeductibles.Enabled = false;
            this.btnResCloseSalvage.Enabled = false;
            this.btnResCloseSubro.Enabled = false;
            this.btnResCloseDeduct.Enabled = false;

            this.btnResTransactions.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResChangeExpense.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResChangeLoss.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResReOpen.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResClose.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResVoid.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResTotalLoss.ForeColor = System.Drawing.Color.DarkGray;
            //this.btnLockReserveInfo.ForeColor = System.Drawing.Color.White;
            //this.btnUnlockReserve.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResSalvage.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResSubro.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResDeductibles.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResCloseSalvage.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResCloseSubro.ForeColor = System.Drawing.Color.DarkGray;
            this.btnResCloseDeduct.ForeColor = System.Drawing.Color.DarkGray;

        }

      

        private void DisplayCoverages()
        {
            try
            {
                switch (this.Claim.ClaimTypeId)
                {
                    case (int)modGeneratedEnums.ClaimType.Auto:
                        if (this.Claim.PolicyNo == "")
                            this.gbGIClDetails.Text = "Not Assigned to a Policy ";
                        else
                            this.gbGIClDetails.Text = "Policy " + this.Claim.PolicyNo + ", Ver " + this.Claim.VersionNo.ToString("###.#") + ", " + this.Claim.PolicyDates;

                        //populate lables
                        this.GIPolicyBI.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Auto_Bodily_Injury);
                        this.GIPolicyPD.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Property_Damage);
                        this.GIPolicyMP.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Medical_Payments);
                        this.GIPolicyUM.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Uninsured_Motorist);
                        this.GIPolicyUIM.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Underinsured_Motorist);
                        this.GIPolicyCompD.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Comprehensive);
                        this.GIPolicyCollD.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Collision);
                        this.GIPolicyPIPMed.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Medical);
                        this.GIPolicyPIPReh.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Rehab);
                        this.GIPolicyPIPWage.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Wages);
                        this.GIPolicyPIPES.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Essential_Services);
                        this.GIPolicyPIPFun.Text = this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Funeral);
                        break;
                    default:
                        if (string.IsNullOrWhiteSpace(this.Claim.PolicyNo))
                        {   //no policy number
                            this.gbGenInfoCompanyPolicy.Text = "Not Assigned to a Policy";
                        }
                        //else
                        //{   //display policy information
                        //    PopulateCompanyHabitatLiabilityGrid();
                        //}
                        break;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void LoadLocationNumbers()
        {
            // fill the location number combo box with all the location numbers 

            if (this.Claim == null)
                return;
            else if (string.IsNullOrWhiteSpace(this.Claim.PolicyNo))
                return;

            //get the location numbers
            List<Hashtable> locationOptions = mm.getPolicyLocation(this.Claim.PolicyNo, this.Claim.DateOfLoss);

            MakeDisplayMember(ref this.cbGenInfoCompanyLossLocNbr, ref locationOptions);

            //load the locations to the combobox
            //this.cbGenInfoCompanyLossLocNbr.ItemsSource = locationOptions;
        }

        private void MakeDisplayMember(ref DropDownList cb, ref List<Hashtable> list)
        {
            string[] cbTag1 = "Tag=Loc;Columns=Loc,storename,address,City,statename,Zip".Split(';');
            string myTag = cbTag1[0].Substring(cbTag1[0].IndexOf('=') + 1, cbTag1[0].Length - (cbTag1[0].IndexOf('=') + 1));
            string[] cbCols = cbTag1[1].Substring(cbTag1[1].IndexOf('=') + 1, cbTag1[1].Length - (cbTag1[1].IndexOf('=') + 1)).Split(',');

            foreach (Hashtable ht in list)
            {
                string dispMem = "";
                foreach (string col in cbCols)
                {
                    if (!string.IsNullOrWhiteSpace(ht[col].ToString()))
                        dispMem += ht[col].ToString() + " ";
                }
                ht.Add("description", dispMem.Trim());
                ht.Add("id", ht["Loc"]);
            }

            mf.LoadTypeTable(cb, list);
        }

        private string UpdatePhoneNo(int personId, int mode = 0)
        {

            switch (mode)
            {
                case 1:     //Owner
                    this.cboGIClDetailOwnerPhone.Enabled = true;
                    this.cboGIClDetailOwnerPhone.DataSource = this.GetPhones(personId);
                    this.cboGIClDetailOwnerPhone.DataTextField = "description";
                    this.cboGIClDetailOwnerPhone.DataValueField = "id";
                    this.cboGIClDetailOwnerPhone.DataBind();
                   
                    if (this.cboGIClDetailOwnerPhone.Items.Count == 0)
                        this.cboGIClDetailOwnerPhone.Enabled = false;
                    else
                        this.cboGIClDetailOwnerPhone.SelectedIndex = 0;
                    break;
                case 2:     //Driver
                    this.cboGIClDetailDriverPhone.Enabled = true;
                    this.cboGIClDetailDriverPhone.DataSource = this.GetPhones(personId);
                    this.cboGIClDetailDriverPhone.DataTextField = "description";
                    this.cboGIClDetailDriverPhone.DataValueField = "id";
                    this.cboGIClDetailDriverPhone.DataBind();
                    
                    if (this.cboGIClDetailDriverPhone.Items.Count == 0)
                        this.cboGIClDetailDriverPhone.Enabled = false;
                    else
                        this.cboGIClDetailDriverPhone.SelectedIndex = 0;
                    break;
                default:    //Insured
                    this.cboGIClDetailInsuredPhone.DataSource = this.GetPhones(personId);
                    this.cboGIClDetailInsuredPhone.DataTextField = "description";
                    this.cboGIClDetailInsuredPhone.DataValueField = "id";
                    this.cboGIClDetailInsuredPhone.DataBind();
                    
                    if (this.cboGIClDetailInsuredPhone.Items.Count == 0)
                        this.cboGIClDetailInsuredPhone.Enabled = false;
                    else
                        this.cboGIClDetailInsuredPhone.SelectedIndex = 0;
                    break;
            }


            return new Person(personId).HomePhone;
        }

        private IEnumerable GetPhones(int personId)
        {
            Person p = new Person(personId);
            List<cboGrid> myList = new List<cboGrid>();

            if (p.HomePhone.Replace("(", "").Replace(")", "").Replace("_", "").Replace("-", "").Trim() != "")
            {
                myList.Add(new cboGrid()
                {
                    id = 1,
                    description = "Home"
                });
            }
            if (p.WorkPhone.Replace("(", "").Replace(")", "").Replace("_", "").Replace("-", "").Trim() != "")
            {
                myList.Add(new cboGrid()
                {
                    id = 2,
                    description = "Work"
                });
            }
            if (p.CellPhone.Replace("(", "").Replace(")", "").Replace("_", "").Replace("-", "").Trim() != "")
            {
                myList.Add(new cboGrid()
                {
                    id = 3,
                    description = "Cell"
                });
            }
            if (p.OtherContactPhone.Replace("(", "").Replace(")", "").Replace("_", "").Replace("-", "").Trim() != "")
            {
                myList.Add(new cboGrid()
                {
                    id = 4,
                    description = "Other"
                });
            }
            if (p.Fax.Replace("(", "").Replace(")", "").Replace("_", "").Replace("-", "").Trim() != "")
            {
                myList.Add(new cboGrid()
                {
                    id = 5,
                    description = "Fax"
                });
            }

            return myList;
        }
        //TODO
        //private void UpdateSR22ComboBox()
        //{
        //    this.cboGIClDetailSR22.ItemsSource = LoadSR22();
        //    if (mm.GetSR22Policy(this._claimId))
        //        this.cboGIClDetailSR22.SelectedIndex = 1;
        //    else
        //        this.cboGIClDetailSR22.SelectedIndex = 0;
        //}

       
        private void ReloadPersonComboBoxes()
        {
            try
            {
                if (!doneLoading)
                    return;

               
                _persons = this.Claim.PersonDescriptions;

                if (this.Claim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Auto)
                {   //auto claim
                    this.cboGIClDetailOwner.DataSource = mm.LoadClaimants(this.Claim.PersonNamesAndTypes, 1);
                    this.cboGIClDetailOwner.DataTextField = "fullName";
                    this.cboGIClDetailOwner.DataValueField = "claimantId";
                    this.cboGIClDetailOwner.DataBind();

                    this.cboGIClDetailDriver.DataSource = mm.LoadClaimants(this.Claim.PersonNamesAndTypes, 2);
                    this.cboGIClDetailDriver.DataTextField = "fullName";
                    this.cboGIClDetailDriver.DataValueField = "claimantId";
                    cboGIClDetailDriver.DataBind();
                    //string whatsthis = this.cboGIClDetailDriver.DisplayMemberPath;

                    //select driver and owner

                    //this.SetComboBoxSelectedIndex(this.cboGIClDetailOwner, "Owner");
                    try
                    {
                        if (this.Claim.OwnerPersonId.ToString() != "0")
                            this.cboGIClDetailOwner.SelectedValue = this.Claim.OwnerPersonId.ToString();
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        if (this.Claim.DriverPersonId.ToString() != "0")
                            this.cboGIClDetailDriver.SelectedValue = this.Claim.DriverPersonId.ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                       this.tbGIClDetailInsured.Text = mm.GetPersonDescription(this.insurePersonId, false);
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        lbGIClDetailInsured.Text = new Person(this.insurePersonId).Description.Replace("\r\n", "<br>");
                    }
                    catch (Exception ex)
                    { }
                }
                else
                {   //commercial claim
                    //set the insured person description
                    this.txtGenInfoCompanyInsured.Text = mm.GetPersonDescription(this.insurePersonId);
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
              
            }
        }
        public void ReloadVehicleComboBox()
        {
            try
            {
              
                this._vehicles = this.Claim.Vehicles;  //get the claim related vehicles
                //mf.LoadPersonComboBox(this.cboGIClDetailVehicle, this.Claim.VehicleNames, 1, this.insuredVehicle.Id);
                this.cboGIClDetailVehicle.DataSource = mm.LoadVehicles(this.Claim.VehicleNames, 1);
                this.cboGIClDetailVehicle.DataTextField = "vName";
                this.cboGIClDetailVehicle.DataValueField = "vehicleId";
                this.cboGIClDetailVehicle.DataBind();

                try
                {
                    if (this.Claim.InsuredVehicleId.ToString() != "0")
                    { 
                        if (!Page.IsPostBack)
                            this.cboGIClDetailVehicle.SelectedValue = Claim.InsuredVehicleId.ToString();
                        //cboGIClDetailVehicle_SelectedIndexChanged(null, null);
                    }
                }
                catch (Exception ex)
                { }

               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
              
            }
        }
        private void UpdatePersonVehicleEditButtons()
        {
            switch (this.Claim.ClaimTypeId)
            {
                case (int)modGeneratedEnums.ClaimType.Auto:
                    //enable buttons
                    this.btnGIClDetailEditDriver.Enabled = true;
                    this.btnGIClDetailEditOwner.Enabled = true;
                    this.btnGIClDetailEditVehicle.Enabled = true;

                    //driver
                    if (this.cboGIClDetailDriver.SelectedIndex < 0)
                        this.btnGIClDetailEditDriver.Enabled = false;  //driver not selected, do not edit
                    else
                    {
                        if (this.cboGIClDetailDriver.Text == "Parked and Unoccupied")
                            this.btnGIClDetailEditDriver.Enabled = false;  //no driver, do not edit

                        if (this.cboGIClDetailDriver.Text.Length >= 7)
                        {
                            if (this.cboGIClDetailDriver.Text.Substring(0, 7) == "Unknown")
                                this.btnGIClDetailEditDriver.Enabled = false;  //unknow driver, do not edit
                        }
                    }

                    //owner
                    if (this.cboGIClDetailOwner.SelectedIndex < 0)
                        this.btnGIClDetailEditOwner.Enabled = false;  //owner not selected, do not edit
                    else
                    {
                        if (this.cboGIClDetailOwner.Text == "")
                           //TODO
                            // this.cboGIClDetailOwner.Text = ((ClaimantGrid)this.cboGIClDetailOwner.SelectedValue).nameAddressType;

                        if (this.cboGIClDetailOwner.Text.Length >= 7)
                        {
                            if (this.cboGIClDetailOwner.Text.Substring(0, 7) == "Unknown")
                                this.btnGIClDetailEditOwner.Enabled = false;  //unknow owner, do not edit
                        }
                    }

                    //vehicle
                    if (this.cboGIClDetailVehicle.SelectedIndex < 0)
                        this.btnGIClDetailEditVehicle.Enabled = false;  //vehicle not selected, do not edit
                    else
                    {
                        if (this.cboGIClDetailVehicle.Text.Length >= 7)
                        {
                            if (this.cboGIClDetailVehicle.Text.Substring(0, 7) == "Unknown")
                                this.btnGIClDetailEditVehicle.Enabled = false;  //unknow vehicle, do not edit
                        }
                    }
                    break;
                default:
                    this.btnGenInfoCompanyInsuredEdit.Enabled = true;
                    break;
            }
        }

        private void SetComboBoxSelectedIndex(DropDownList cb, string idxString)
        {
            string tmpIdxString = idxString;
            int thisIdx = 0;

            for (int i = 0; i < 4; i++)
            {
                thisIdx = 0;
                foreach (object thisItem in cb.Items)
                {
                    string thatItem = "";
                    if (thisItem.GetType() == typeof(ClaimantGrid))
                        thatItem = ((ClaimantGrid)thisItem).nameAddressType;
                    else if (thisItem.GetType() == typeof(VehicleGrid))
                        thatItem = ((VehicleGrid)thisItem).vName;

                    if (thatItem.Substring(thatItem.Length - tmpIdxString.Length, tmpIdxString.Length) == tmpIdxString)
                    {
                        //TODO
                        //cb.SelectedIndex = cb.Items.IndexOf(thatItem);
                        if (cb.SelectedIndex == -1)
                            cb.SelectedIndex = thisIdx;
                        break;
                    }
                    thisIdx++;
                }
                if (cb.SelectedIndex != -1)
                    break;
                switch (i)
                {
                    case 1:
                        tmpIdxString = "Named Insured";
                        break;
                    case 2:
                        tmpIdxString = "Insured";
                        break;
                    case 3:
                        tmpIdxString = "Owner";
                        break;
                    default: //case 0:
                        tmpIdxString = "Driver";
                        break;
                }
            }
        }

        private void CheckFraudClaim()
        {
            List<Hashtable> list;
            bool fraudPerson = false;
            bool fraudVehicle = false;

            if (this.insurePersonId != 0)
            {
                Person p = new Person(this.insurePersonId);
                if (!string.IsNullOrWhiteSpace(p.DriversLicenseNo))
                {
                    list = mm.GetNICBPersons("", p.LastName, "", "", "", "", p.DateOfBirth, "", p.DriversLicenseNo);
                    if (list.Count > 0)
                    {
                        foreach (Hashtable f in list)
                        {
                            if ((bool)f["Flag_Fraud"])
                            {
                                fraudPerson = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (this.insuredVehicle != null)
            {
                if (!string.IsNullOrWhiteSpace(this.insuredVehicle.VIN))
                {
                    list = mm.GetNICBVehicles(this.insuredVehicle.VIN);
                    if (list.Count > 0)
                    {
                        foreach (Hashtable f in list)
                        {
                            if ((bool)f["Flag_Fraud"])
                            {
                                fraudVehicle = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void UpdateGeneralFrame()
        {
            //Policy
            if (this.Claim.PolicyNo == "")
            {
                this.lblPolicyNo.Text = "No Policy Assigned";
                this.lblPolicyNo.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lblPolicyNo.Text = this.Claim.PolicyNo + ", Ver " + this.Claim.VersionNo.ToString("###.#") + ", " + this.Claim.PolicyDates;
                //this.lblPolicyNo.Foreground = this.ClDOL.Foreground;
            }

            //populate gbGeneral
            this.lblDateOfLoss.Text = this.Claim.DateOfLoss.ToShortDateString();
<<<<<<< HEAD
            this.lblTimeOfloss.Text = this.Claim.DateOfLoss.ToShortTimeString();
=======
<<<<<<< HEAD
            this.lblTimeOfloss.Text = this.Claim.DateOfLoss.ToShortTimeString();
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            this.lblLossDescription.Text = Truncate(this.Claim.LossDescription, 170);
            this.lblInsuredName.Text = this.Claim.InsuredPerson.Name;
            this.lblClaim.Text = this.Claim.DisplayClaimId;
            switch (this.myClaim.ClaimTypeId)
            {
                case (int)modGeneratedEnums.ClaimType.Auto:
                    this.lblInsuredVehicle.Text = this.Claim.InsuredVehicle.Name;
                    break;
                default:
                    break;
            }
        }

        private string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length) + "...";
            }
            return source;
        }

        public string GetState(int id)
        {
            string tmpString = mm.GetStateForId(id);


            return tmpString;
        }

        private void CheckClaimLock()
        {
            this.claimLocked = this.Claim.FirstLockId() != 0;
            this.EnableDisableAnnoyingLock();
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            if (this.claimLocked)
            {
                imgLocked.Visible = true;
            }
            else
            {
                imgLocked.Visible = false;
            }
<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        private void EnableDisableAnnoyingLock()
        {
            if (this.claimLocked)
            {
                imgLocked.Visible = true;
<<<<<<< HEAD
                LockedPanel.Style["background-color"] = "#D21306";
                LockedPanel.Style["color"] = "#ffffff";
=======
                LockedPanel.Style["background-color"] = "#fa8072";
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                //if (!Page.IsPostBack)
                //{ 
                //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Claim Is Locked!');</script>");
                //}
                //gvLockReasons.DataBind();
                //gvLockReasons.Visible = true;
            }
            else
            {
                LockedPanel.Style["background-color"] = "#f0f0f0";
<<<<<<< HEAD
                LockedPanel.Style["color"] = "blue";

            }
=======

            }
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            if (this.Claim.HasUnassignedReserves())
            {
                //int index = Request.Url.ToString().IndexOf("?");
                //if (index > 0)
                //    referer = Request.Url.ToString().Substring(0, index);
                //referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                //Session["referer"] = referer;

                //string url = "frmUnassignedReservesMessage.aspx?mode=c&claim_id=" + _claimId;
                //string s = "window.open('" + url + "', 'popup_windowurm', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=250, copyhistory=no, left=400, top=400');";
                //ClientScript.RegisterStartupScript(this.GetType(), "scripturm", s, true);
                lblUnassignedReserves.Visible = true;
            }
            else
            {
                lblUnassignedReserves.Visible = false;
            }

           
<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        private void UpdateInsuredPhoneNo()
        {
            this.txtGIClDetailInsuredPh.Text = new Person(insurePersonId).HomePhone;
        }

        public class PropertyDamageGrid
        {
            public int propertyDamageId { get; set; }
            public int propertyDamageImage { get; set; }
            public string propertyType { get; set; }
            public string pdReserveType { get; set; }
            public string pdPropertyDescription { get; set; }
            public string pdEstimatedDamage { get; set; }
            public string pdWhereSeen { get; set; }
            public string pdOtherInsurance { get; set; }
            public string pdOtherInsPolicyNo { get; set; }
            public BitmapImage imageIdx
            {
                get { return modLogic.ImageList(propertyDamageImage); }
            }
        }
        class FileNoteGrid
        {
            public int fileNoteId { get; set; }
            public int fileNoteImage { get; set; }
            public DateTime dtCreated { get; set; }
            public string fileNoteType { get; set; }
            public string diaryType { get; set; }
            public string cleared { get; set; }
            public string dtDiaryDue { get; set; }
            public string userId { get; set; }
            public string claimantId { get; set; }
            public string reserveId { get; set; }
            public string description { get; set; }
            public BitmapImage imageIdx
            {
                get { return modLogic.ImageList(fileNoteImage); }
            }
        }
        class policyGrid
        {
            public int id { get; set; }
            public string policyNo { get; set; }
            public double versionNo { get; set; }
            public DateTime dtEffective { get; set; }
            public DateTime dtExpired { get; set; }
            public string transaction { get; set; }
            public string endorsementReason { get; set; }
            public int vehicleNum { get; set; }
            public int namedInsuredNum { get; set; }
            public int addDriversNum { get; set; }
            public int exclDriversNum { get; set; }
            public int ownerId { get; set; }
            public int driverId { get; set; }
            public int namedInsuredId { get; set; }
            public int insVehicleId { get; set; }
            public int rowColor { get; set; }
            public bool isWeb { get; set; }
            public string userId { get; set; }
            public BitmapImage ownerImage
            {
                get { return modLogic.ImageList(ownerId); }
            }
            public BitmapImage driverImage
            {
                get { return modLogic.ImageList(driverId); }
            }
            public BitmapImage insVehicleImage
            {
                get { return modLogic.ImageList(insVehicleId); }
            }
            public BitmapImage namedInsuredImage
            {
                get { return modLogic.ImageList(namedInsuredId); }
            }

        }
        class SalvageGrid
        {
            public int id { get; set; }
            public int image_index { get; set; }
            public int vehicleId { get; set; }
            public int vendorId { get; set; }
            public int appraisalRequestId { get; set; }
            public int appraisalTaskId { get; set; }
            public bool salvageComplete { get; set; }
            public double totalSalvageCharge
            {
                get
                {
                    if (id == 0)
                        return 0;

                    Salvage sal = new Salvage(id);
                    double storage = sal.StorageCharges;
                    double tow = sal.Towing_Charges;
                    double yard = sal.SalvageYardCharges;
                    return storage + tow + yard;
                }
            }
            public string vendorName
            {
                get
                {
                    return new Vendor(vendorId).Name;
                }
            }
            public string vehicleName { get { return new Vehicle(vehicleId).ShortName; } }
            public string vendorLocation
            {
                get
                {
                    Vendor v = new Vendor(vendorId);
                    string loc = v.Address1 + " " + v.City + ", " + v.State.Abbreviation;
                    return loc;
                }
            }
            public string vendorPhone { get { return new Vendor(vendorId).Phone; } }
        }
        class LitigationGrid
        {
            public int id { get; set; }
            public int statusId { get; set; }
            public int claimId { get; set; }
            public int defenseAttorneyId { get; set; }
            public int plaintiffAttorneyId { get; set; }
            public int courtLocationId { get; set; }
            public DateTime dtFiled { get; set; }
            public DateTime dtServed { get; set; }
            public string resolution { get; set; }
            public string comments { get; set; }
            public string defenseAttorney { get; set; }
            public string plaintiffAttorney { get; set; }
            public string location { get; set; }
            public string status { get; set; }
            public string name { get; set; }
            public int imageIndex { get; set; }
            public BitmapImage imageIdx
            {
                get { return modLogic.ImageList(imageIndex); }
            }

        }

        #region formatpopups
        public string FormatPopupAlerts(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('alert.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('alert.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }

        public string FormatPopupPickAppraisal()
        {
            string tmpString = "";
            tmpString = "javascript:my_window2" + _claimId + "=window.open('PickAppraisalProperties.aspx?mode=u&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }

        public string FormatPopupSalvage(string id, string pvehicle_id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmAddEditSalvage.aspx?mode=" + mode + "&claim_id=" + _claimId + "&salvage_id=0&vehicle_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmAddEditSalvage.aspx?mode=" + mode + "&claim_id=" + _claimId + "&salvage_id=" + id + "&vehicle_id=" + pvehicle_id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupDemands(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                //tmpString = "javascript:my_window2" + _claimId + "=window.open('DemandsAdd.aspx?mode=" + mode + "&_claimId=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
                tmpString = "javascript:my_window2" + _claimId + "=window.open('DemandsAdd.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('DemandsAdd.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }

        public string FormatPopupDemandOffer(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('offer.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('offer.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupDemandDemand(string id, string mode)
        {

           
            return "";
        }
        public string FormatPopupLitigation(string id, string mode)
        {

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=15";

            Session["referer"] = referer;

            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmLitigation.aspx?mode=" + mode + "&claim_id=" + _claimId + "&litigation_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmLitigation.aspx?mode=" + mode + "&claim_id=" + _claimId + "&litigation_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupPossibleDuplicates(string id, string mode)
        {
            string tmpClaimId = Regex.Replace(id, "[^0-9]", "");
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window" + id + "=window.open('viewClaim.aspx?tabindex=0&claim_id=" + tmpClaimId + "','my_window" + tmpClaimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" + tmpClaimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window" + id + "=window.open('viewClaim.aspx?tabindex=0&claim_id=" + tmpClaimId + "','my_window" + tmpClaimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" + tmpClaimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupAppraisals(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmAppraisalRequest.aspx?mode=" + mode + "&claim_id=" + _claimId + "&appraisal_task_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmAppraisalRequest.aspx?mode=" + mode + "&claim_id=" + _claimId + "&appraisal_task_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupReserves(string id, string mode)
        {
        
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmTransactions.aspx?mode=" + mode + "&claim_id=" + _claimId + "&reserve_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmTransactions.aspx?mode=" + mode + "&claim_id=" + _claimId + "&reserve_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupVehicle(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmVehicles.aspx?mode=" + mode + "&claim_id=" + _claimId + "&vehicle_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmVehicles.aspx?mode=" + mode + "&claim_id=" + _claimId + "&vehicle_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupFileNote(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&_claimId=<%=_claimId%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('fileNote.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('fileNote.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupReport(string reportLink)
        {

            string tmpString = "javascript:my_windowReport=window.open('" + reportLink + _claimId + "','my_windowReport','toolbar=no, location=yes, titlebar=yes, linkbar=no, directories=no, status=yes, menubar=yes, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2Report.focus()";

            return tmpString;
        }
        public string FormatPopupPD(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&_claimId=<%=_claimId%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmPropertyDamage.aspx?mode=" + mode + "&claim_Id=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmPropertyDamage.aspx?mode=" + mode + "&claim_Id=" + _claimId + "&property_damage_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=300, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupWitness(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&_claimId=<%=_claimId%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmWitnessPassengerDetails.aspx?mode=c&claim_id=" + _claimId + "&person_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmWitnessPassengerDetails.aspx?mode=" + mode  + "&claim_id=" + _claimId + "&person_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupInjured(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&_claimId=<%=_claimId%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmInjuredDetails.aspx?mode=" + mode + "&claim_id=" + _claimId + "&injured_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmInjuredDetails.aspx?mode=" + mode + "&claim_id=" + _claimId + "&injured_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupPhone(string id, string mode, string type, string personId, string vendorId)
        {
            string tmpString = "";
            if ((mode == "u") || (mode == "r"))
            {
                    mm.UpdateLastActivityTime();



                    //phoneGrid mySelection = (phoneGrid)grPhoneNumbers.SelectedItem;
                    //if (mySelection == null)
                    //    return "";

                    switch (type)
                    {
                        case "Agent":
                        break;
                        case "Adjuster":
                             break;
                        case "Manual Entry":

                            tmpString = "javascript:my_window2" + _claimId + "=window.open('frmManualPhoneNumber.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
                            break;
                        default:
                            if (vendorId != "0")
                            {
                                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmAddEditVendor.aspx?mode=" + mode + "&claim_id=" + _claimId + "&vendor_Id=" + vendorId + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
                            }
                            else if (personId != "0")
                            {
                                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmPersonDetails.aspx?mode=" + mode + "&claim_id=" + _claimId + "&person_id=" + personId + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
                            }
                            break;
                    }
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmManualPhoneNumber.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            return tmpString;

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";


        }

        public string FormatPopupPerson(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&_claimId=<%=_claimId%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('persondetails.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('persondetails.aspx?mode=" + mode + "&claim_id=" + _claimId + "&id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupClaimant(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&_claimId=<%=_claimId%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmClaimantDetails.aspx?mode=1&claim_id=" + _claimId + "&person_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmClaimantDetails.aspx?mode=1&claim_id=" + _claimId + "&person_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        public string FormatPopupTasks(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + _claimId + "=window.open('frmNewTask.aspx?mode=" + mode + "&claim_id=" + _claimId + "&task_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=500, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }
            else
            {
                tmpString = "return confirm('Are you sure you want to clear this task?');javascript:my_window2" + _claimId + "=window.open('frmNewTask.aspx?mode=" + mode + "&claim_id=" + _claimId + "&task_id=" + id + "','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=500, copyhistory=no, left=400, top=200');my_window2" + _claimId + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + _claimId + "=window.open('propertyDamage.aspx?mode=c&_claimId=" + _claimId + "&property_damage_id=0','my_window2" + _claimId + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + _claimId + ".focus()";

            return tmpString;
        }
        #endregion

        #region sorting
        protected void gvFileNotes_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            DataTable dt = Session["FileNotes" + _claimId + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvFileNotes.DataSource = Session["FileNotes" + _claimId + "Table"];
                gvFileNotes.DataBind();

                Session["FileNotes" + _claimId + "Table"] = dt;
            }

        }
        protected void gvPossibleDuplicates_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["PossibleDuplicates" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvPossibleDuplicates.DataSource = Session["PossibleDuplicates" + claim_id + "Table"];
            //    gvPossibleDuplicates.DataBind();

            //    Session["PossibleDuplicates" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvLitigation_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Litigation" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvLitigation.DataSource = Session["Litigation" + claim_id + "Table"];
            //    gvLitigation.DataBind();

            //    Session["Litigation" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvSalvage_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Salvage" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvSalvage.DataSource = Session["Salvage" + claim_id + "Table"];
            //    gvSalvage.DataBind();

            //    Session["Salvage" + claim_id + "Table"] = dt;
            //}

        }
       
        protected void gvAppraisals_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Appraisals" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvAppraisals.DataSource = Session["Appraisals" + claim_id + "Table"];
            //    gvAppraisals.DataBind();

            //    Session["Appraisals" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvPhoneNumbers_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["PhoneNumber" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvPhoneNumbers.DataSource = Session["PhoneNumber" + claim_id + "Table"];
            //    gvPhoneNumbers.DataBind();

            //    Session["PhoneNumber" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvTasks_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Tasks" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvTasks.DataSource = Session["Tasks" + claim_id + "Table"];
            //    gvTasks.DataBind();

            //    Session["Tasks" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvClaimant_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Claimant" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvClaimant.DataSource = Session["Claimant" + claim_id + "Table"];
            //    gvClaimant.DataBind();

            //    Session["Claimant" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvInjured_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Injured" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvInjured.DataSource = Session["Injured" + claim_id + "Table"];
            //    gvInjured.DataBind();

            //    Session["Injured" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvWitness_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Witness" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvWitness.DataSource = Session["Witness" + claim_id + "Table"];
            //    gvWitness.DataBind();

            //    Session["Witness" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvDemands_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Demands" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvDemands.DataSource = Session["Demands" + claim_id + "Table"];
            //    gvDemands.DataBind();

            //    Session["Demands" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvPropertyDamage_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["PropertyDamage" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvPropertyDamage.DataSource = Session["PropertyDamage" + claim_id + "Table"];
            //    gvPropertyDamage.DataBind();

            //    Session["PropertyDamage" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvReserves_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            DataTable dt = Session["Reserves" + _claimId + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                grReserves.DataSource = Session["Reserves" + _claimId + "Table"];
                grReserves.DataBind();

                Session["Reserves" + _claimId + "Table"] = dt;
            }
<<<<<<< HEAD
=======
=======
            //DataTable dt = Session["Reserves" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    //Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvReserves.DataSource = Session["Reserves" + claim_id + "Table"];
            //    gvReserves.DataBind();

            //    Session["Reserves" + claim_id + "Table"] = dt;
            //}
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

        }
        protected void gvVehicles_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            //DataTable dt = Session["Vehicles" + claim_id + "Table"] as DataTable;

            //if (dt != null)
            //{

            //    Sort the data.
            //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            //    gvVehicles.DataSource = Session["Vehicles" + claim_id + "Table"];
            //    gvVehicles.DataBind();

            //    Session["Vehicles" + claim_id + "Table"] = dt;
            //}

        }
        protected void gvAlerts_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////Retrieve the table from the session object.
            DataTable dt = Session["Alerts" + _claimId + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                grAlerts.DataSource = dt;
                grAlerts.DataBind();

                Session["Alerts" + _claimId + "Table"] = dt;
            }

        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }
        #endregion

        #region paging
        protected void gvPossibleDuplicates_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //DuplicateClaims myData = new DuplicateClaims();
            //dt = myData.GetPossibleDuplicatesForClaim(claim_id);
            //gvPossibleDuplicates.DataSource = dt;
            //gvPossibleDuplicates.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvLitigation_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Litigation myData = new Litigation();
            //dt = myData.GetLitigationForClaim(claim_id);
            //gvLitigation.DataSource = dt;
            //gvLitigation.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvSalvage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Salvage myData = new Salvage();
            //dt = myData.GetSalvageForClaim(claim_id);
            //gvSalvage.DataSource = dt;
            //gvSalvage.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Documents myData = new Documents();
            //dt = myData.GetDocuments_By_ClaimId(claim_id);
            //gvDocuments.DataSource = dt;
            //gvDocuments.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvAppraisals_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Appraisals myData = new Appraisals();
            //dt = myData.GetAppraisalsForClaim(claim_id);
            //gvAppraisals.DataSource = dt;
            //gvAppraisals.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvPhoneNumbers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            //dt = myData.GetPhoneNumbersForClaim(claim_id);
            //gvPhoneNumbers.DataSource = dt;

            //gvPhoneNumbers.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvTasks_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Task myData = new Task();
            //dt = myData.GetTasksForClaim(claim_id);
            //gvTasks.DataSource = dt;

            //gvTasks.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvClaimant_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Claimants myData = new Claimants();
            //dt = myData.GetClaimantsForClaim(claim_id);
            //gvClaimant.DataSource = dt;

            //gvClaimant.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvInjured_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Injured myData = new Injured();
            //dt = myData.GetInjuredForClaim(claim_id);
            //gvInjured.DataSource = dt;

            //gvInjured.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void chkFileActivityDiariesOnly_CheckedChanged(object sender, EventArgs e)
        {
            LoadFileNotes();
            //if (chkFileActivityDiariesOnly.Checked)
            //{
            //    DataTable selectedTable = gvFileNotes.DataSource as DataTable;

            //    DataTable dt = selectedTable.AsEnumerable()
            //                    .Where(r => r.Field<string>("diary_type").Length > 0)
            //                    .CopyToDataTable();
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();

               
            //}
            //else
            //{
            //    LoadFileNotes();
            //}
        }

        protected void cboFileActivityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.doneLoading)  //don't load before form is done loading
                    return;

                mm.UpdateLastActivityTime();
                if (this.cboFileActivityClaimant.SelectedValue == "All")
                    this.fileClaimantId = 0;
                else
                    this.fileClaimantId = Convert.ToInt32(this.cboFileActivityClaimant.SelectedValue);

                if (this.cboFileActivityType.SelectedValue == "All")
                    this.fileNoteTypeId = 0;
                else
                    this.fileNoteTypeId = Convert.ToInt32(this.cboFileActivityType.SelectedValue);

                try
                {
                    if (cboFileActivityReserve.SelectedItem.ToString() == "All")
                        this.fileReserveId = 0;
                    else
                        this.fileReserveId = (new Claimant(fileClaimantId)).ReserveId(ml.getId("Reserve_Type", cboFileActivityReserve.SelectedItem.ToString()));
                }
                catch (Exception ex)
                {
                    this.fileReserveId = 0;
                }


                this.LoadFileNotes();   //reload file notes grid
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void cboFileActivityClaimant_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.doneLoading)  //don't load before form is done loading
                    return;

                mm.UpdateLastActivityTime();

                if (this.cboFileActivityClaimant.SelectedValue == "All")
                    this.fileClaimantId = 0;
                else
                   this.fileClaimantId = Convert.ToInt32(this.cboFileActivityClaimant.SelectedValue);

                if (this.cboFileActivityType.SelectedValue == "All")
                    this.fileNoteTypeId = 0;
                else
                    this.fileNoteTypeId = Convert.ToInt32(this.cboFileActivityType.SelectedValue);

                try
                {
                    if (cboFileActivityReserve.SelectedItem.ToString() == "All")
                        this.fileReserveId = 0;
                    else
                        this.fileReserveId = (new Claimant(fileClaimantId)).ReserveId(ml.getId("Reserve_Type", cboFileActivityReserve.SelectedItem.ToString()));
                }
                catch (Exception ex)
                {
                    this.fileReserveId = 0;
                }



                LoadFileActivityReserves();
                this.LoadFileNotes();   //reload file notes grid
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void LoadFileActivityReserves()
        {
            this.fileActivityLoading = true;
            mf.LoadComboBoxFromStoredProc("usp_Get_Reserve_Names_For_Claimant", this.cboFileActivityReserve, "Reserve_Name", this.fileClaimantId);

            this.cboFileActivityReserve.Items.Insert(0, new ListItem("All"));     //insert "All" as element 0
            this.cboFileActivityReserve.SelectedIndex = 0;
            this.fileActivityLoading = false;

            this.fileReserveId = 0;     //reset the file note reserve ID selected
        }

        protected void btnFileActivitySearch_Click(object sender, EventArgs e)
        {
            LoadFileNotes();
            //if (tbFileActivitySearch.Text.Length > 0)
            //{
               
            //    DataTable dt = new DataTable();
            //    DataTable values = gvFileNotes.DataSource as DataTable;
            //    var rows = values.AsEnumerable().Where
            //    (row => row.Field<string>("File_Note_Text").ToUpper().Contains(tbFileActivitySearch.Text.Trim().ToUpper()));//get the rows where the status is equal to action

            //    if (rows.Any())
            //    {
            //        dt = rows.CopyToDataTable<DataRow>();//Copying the rows into the DataTable as DataRow
            //    }


            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();

            //    Session["FileNotes" + _claimId + "Table"] = dt;
            //}
            //else
            //{
            //    LoadFileNotes();
            //}
        }

        protected void cboFileActivityReserve_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboFileActivityClaimant.SelectedValue == "All")
                this.fileClaimantId = 0;
            else
                this.fileClaimantId = Convert.ToInt32(this.cboFileActivityClaimant.SelectedValue);

            if (this.cboFileActivityType.SelectedValue == "All")
                this.fileNoteTypeId = 0;
            else
                this.fileNoteTypeId = Convert.ToInt32(this.cboFileActivityType.SelectedValue);

            if (cboFileActivityReserve.SelectedItem.ToString() == "All")
                this.fileReserveId = 0;
            else
                this.fileReserveId = (new Claimant(fileClaimantId)).ReserveId(ml.getId("Reserve_Type", cboFileActivityReserve.SelectedItem.ToString()));


            LoadFileNotes();
        }

        protected void gvWitness_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Witness myData = new Witness();
            //dt = myData.GetWitnessForClaim(claim_id);
            //gvWitness.DataSource = dt;

            //gvWitness.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvDemands_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Demands myData = new Demands();
            //dt = myData.GetDemandsByClaimID(claim_id);
            //gvDemands.DataSource = dt;

            //gvAlerts.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvAlerts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dtAlert = new DataTable();

            //Alert myAlert = new Alert();
            //dtAlert = myAlert.GetAlertsForClaim(claim_id);
            //gvAlerts.DataSource = dtAlert;

            //gvAlerts.PageIndex = e.NewPageIndex;
            //DataBind();
        }
        protected void gvPropertyDamage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //PropertyDamage myData = new PropertyDamage();
            //dt = myData.GetPropertyDamagesForClaim(claim_id);
            //gvPropertyDamage.DataSource = dt;

            //gvPropertyDamage.PageIndex = e.NewPageIndex;
            //DataBind();
        }

       

        protected void btnGIClDetailSave_Click(object sender, EventArgs e)
        {
            this.SaveGeneralInfo(true);
        }

        public void populateSessionHolders()
        {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            try
            {
                string sessionName = "tbGIClDetailDOL" + this._claimId.ToString();
                Session[sessionName] = "set";
                Session["tbGIClDetailDOL"] = tbGIClDetailDOL.Text;
                Session["tbGIClDetailTOL"] = tbGIClDetailTOL.Text;
                Session["tbGIClDetaildtReported"] = this.tbGIClDetaildtReported.Text;
                Session["cboGIClDetailatFault"] = cboGIClDetailatFault.SelectedValue.ToString();
                Session["tbGIClDetailLossLoc"] = tbGIClDetailLossLoc.Text;
                Session["tbGIClDetailLossCity"] = tbGIClDetailLossCity.Text;
                Session["cboGIClDetailLossState"] = cboGIClDetailLossState.SelectedValue.ToString();
                Session["cboGIClDetailAccidentType"] = cboGIClDetailAccidentType.SelectedValue.ToString();
                Session["tbGIClDetailLossDescr"] = tbGIClDetailLossDescr.Text;
                Session["tbGIClDetailAuthContact"] = tbGIClDetailAuthContact.Text;
                Session["tbGIClDetailRptNo"] = tbGIClDetailRptNo.Text;
                Session["cboGIClDetailRptType"] = cboGIClDetailRptType.SelectedValue.ToString();
                Session["tbGIClDetailRptBy"] = tbGIClDetailRptBy.Text;
                Session["tbGIClDetailIVDamage"] = tbGIClDetailIVDamage.Text;
                Session["tbGIClDetailWhere"] = tbGIClDetailWhere.Text;
                Session["cboGIClDetailVehicle"] = cboGIClDetailVehicle.SelectedValue.ToString();
                //Session["tbGIClDetailInsured"] = tbGIClDetailInsured.Text;
                Session["cboGIClDetailOwner"] = cboGIClDetailOwner.SelectedValue.ToString();
                Session["cboGIClDetailDriver"] = cboGIClDetailDriver.SelectedValue.ToString();
                Session["ckbGIClDetailPerm2Use"] = (bool)ckbGIClDetailPerm2Use.Checked;
                Session["cboGIClDetailSR22"] = (bool)cboGIClDetailSR22.Checked;
                Session["ckbGIClDetailIVComp"] = (bool)ckbGIClDetailIVComp.Checked;
                Session["ckbGIClDetailIVColl"] = (bool)ckbGIClDetailIVColl.Checked;
                Session["ckbGIClDetailCoverage"] = (bool)ckbGIClDetailCoverage.Checked;
                Session["ckbGIClDetailLiability"] = (bool)ckbGIClDetailLiability.Checked;
                Session["ckbGIClDetailLrgLoss"] = (bool)ckbGIClDetailLrgLoss.Checked;
                Session["ckbGIClDetailLitigation"] = (bool)ckbGIClDetailLitigation.Checked;
                Session["tbGIClDetailComments"] = tbGIClDetailComments.Text;

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
<<<<<<< HEAD
=======
=======
            Session["tbGIClDetailDOL"] = tbGIClDetailDOL.Text;
            Session["tbGIClDetailTOL"] = tbGIClDetailTOL.Text;
            Session["tbGIClDetaildtReported"] = this.tbGIClDetaildtReported.Text;
            Session["cboGIClDetailatFault"] = cboGIClDetailatFault.SelectedValue.ToString();
            Session["tbGIClDetailLossLoc"] = tbGIClDetailLossLoc.Text;
            Session["tbGIClDetailLossCity"] = tbGIClDetailLossCity.Text;
            Session["cboGIClDetailLossState"] = cboGIClDetailLossState.SelectedValue.ToString();
            Session["cboGIClDetailAccidentType"] = cboGIClDetailAccidentType.SelectedValue.ToString();
            Session["tbGIClDetailLossDescr"] = tbGIClDetailLossDescr.Text;
            Session["tbGIClDetailAuthContact"] = tbGIClDetailAuthContact.Text;
            Session["tbGIClDetailRptNo"] = tbGIClDetailRptNo.Text;
            Session["cboGIClDetailRptType"] = cboGIClDetailRptType.SelectedValue.ToString(); 
            Session["tbGIClDetailRptBy"] = tbGIClDetailRptBy.Text;
            Session["tbGIClDetailIVDamage"] = tbGIClDetailIVDamage.Text;
            Session["tbGIClDetailWhere"] = tbGIClDetailWhere.Text;
            Session["cboGIClDetailVehicle"] = cboGIClDetailVehicle.SelectedValue.ToString();
            //Session["tbGIClDetailInsured"] = tbGIClDetailInsured.Text;
            Session["cboGIClDetailOwner"] = cboGIClDetailOwner.SelectedValue.ToString();
            Session["cboGIClDetailDriver"] = cboGIClDetailDriver.SelectedValue.ToString();
            Session["ckbGIClDetailPerm2Use"] = (bool)ckbGIClDetailPerm2Use.Checked;
            Session["cboGIClDetailSR22"] = (bool)cboGIClDetailSR22.Checked;
            Session["ckbGIClDetailIVComp"] = (bool)ckbGIClDetailIVComp.Checked;
            Session["ckbGIClDetailIVColl"] = (bool)ckbGIClDetailIVColl.Checked;
            Session["ckbGIClDetailCoverage"] = (bool)ckbGIClDetailCoverage.Checked;
            Session["ckbGIClDetailLiability"] = (bool)ckbGIClDetailLiability.Checked;
            Session["ckbGIClDetailLrgLoss"] = (bool)ckbGIClDetailLrgLoss.Checked;
            Session["ckbGIClDetailLitigation"] = (bool)ckbGIClDetailLitigation.Checked;
            Session["tbGIClDetailComments"] = tbGIClDetailComments.Text;


>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }


        protected void btnGIClDetailNewVehicle_Click(object sender, EventArgs e)
        {
            populateSessionHolders();

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

            Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3"); 

            string url = "frmVehicles.aspx?mode=c&claim_id=" + _claimId + "&vehicle_id=0&control=cboGIClDetailVehicle";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
           
        }

        protected void btnGIClDetailEditVehicle_Click(object sender, EventArgs e)
        {
            populateSessionHolders();

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

            Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");

            string url = "frmVehicles.aspx?mode=u&claim_id=" + _claimId + "&vehicle_id=" + this.cboGIClDetailVehicle.SelectedValue + "&control=cboGIClDetailVehicle";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnGIClDetailEditVehicle_Click1(object sender, EventArgs e)
        {
            populateSessionHolders();

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");
                string url = "frmVehicles.aspx?mode=u&claim_id=" + _claimId + "&vehicle_id=" + this.cboGIClDetailVehicle.SelectedValue + "&control=cboGIClDetailVehicle";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            
        }

        protected void btnGIClDetailNewInsured_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");

                string url = "frmPersonDetails.aspx?mode=c&person_type_id=1&claim_id=" + _claimId + "&person_id=0";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            
        }

        protected void btnGIClDetailEditInsured_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");

                string url = "frmPersonDetails.aspx?mode=u&person_type_id=1&claim_id=" + _claimId + "&person_id=" + this.insurePersonId;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
           
        }

        protected void btnGIClDetailNewOwner_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");

                string url = "frmPersonDetails.aspx?mode=u&person_type_id=7&claim_id=" + _claimId + "&person_id=0&control=cboGIClDetailOwner";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            
        }

        protected void btnGIClDetailEditOwner_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");

                string url = "frmPersonDetails.aspx?mode=u&person_type_id=7&claim_id=" + _claimId + "&person_id=" + this.cboGIClDetailOwner.SelectedValue + "&control=cboGIClDetailOwner";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            
        }

        protected void btnGIClDetailNewDriver_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");
                string url = "frmPersonDetails.aspx?mode=u&person_type_id=8&claim_id=" + _claimId + "&person_id=0&control=cboGIClDetailDriver";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            
        }
        
        protected void btnGIClDetailEditDriver_Click(object sender, EventArgs e)
        {
            populateSessionHolders();
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=0";

                Session["referer"] = referer.Replace("&control=cboGIClDetailVehicle", "").Replace("&control=cboGIClDetailOwner", "").Replace("&control=tbGIClDetailInsured", "").Replace("&control=cboGIClDetailDriver", "").Replace("&personid", "&pastperson").Replace("&pastperson", "&pastperson2").Replace("&pastperson2", "&pastperson3");

                string url = "frmPersonDetails.aspx?mode=u&person_type_id=8&claim_id=" + _claimId + "&person_id=" + this.cboGIClDetailDriver.SelectedValue + "&control=cboGIClDetailDriver";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
          
        }

      

        private bool SavePreviousScreenChanges()
        {
            
            if (this.Claim.isNotClosed && this.doneLoading)
            {

                switch (previousClaimScreen)
                {
                    case 0:  //General Information

                        if ((string.IsNullOrWhiteSpace(this.tbGIClDetailTOL.Text) || (Convert.ToDateTime(this.tbGIClDetailTOL.Text).ToShortTimeString() == ml.DEFAULT_DATE.ToShortTimeString())))
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Time of loss is a required entry.');</script>");
                            
                            //MessageBox.Show("Time of loss is a required entry.", "Missing Information", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                            //this.clPages.SelectedIndex = this.clPages.Items.IndexOf(clGeneral);
                            TabContainer1.ActiveTabIndex = 0;
                            TabContainer1_ActiveTabChanged(TabContainer1, EventArgs.Empty);
                           
                            this.tbGIClDetailTOL.Focus();
                            return false;
                        }

                        this.PopulateClaim();
                        //if (this.Claim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Auto)
                        //{
                        //    if ((this.Claim.isChanged) && (this.TabContainer1.ActiveTabIndex == 0))
                        //    {
                        //        switch (MessageBox.Show("General Claim Data has Changed.  Save Changes?", "Save Changes", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes))
                        //        {
                        //            case MessageBoxResult.Yes:
                        //                if (!this.SaveGeneralInfo())
                        //                {   //didn't save info
                        //                    this.TabContainer1.ActiveTabIndex = 0;
                        //                    this.TabContainer1_ActiveTabChanged(null, null);
                        //                    return false;
                        //                }
                        //                return true;
                        //            case MessageBoxResult.No:
                        //                //lose changes
                        //                this.LoadClaim();
                        //                break;
                        //            case MessageBoxResult.Cancel:
                        //                break;
                        //        }
                        //    }
                        //}
                        break;
                    default:
                        break;
                }
            }
            return true;
        }

        protected void tsbAssigAppraiser_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //if (!this.SavePreviousScreenChanges())
                //    return;
                mm.UpdateLastActivityTime();
                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(Not Authorized to Modify Claim Data.');</script>");

                    return;
                }

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=10";

                Session["referer"] = referer;

                string url = "frmPicker.aspx?mode=5&claim_id=" + _claimId + "";
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=300, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);


                //mf.AssignAppraiser(this._claimId);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

      

        private void addControls(int n)
        {
            //loop the amount of controls and add them to the placeholder
            for (int i = 0; i < n; i++)
            {
                this.pnlGI.Visible = true;
                //this.mainForm.Visible = false;
                VehicleCtrl control = (VehicleCtrl)LoadControl("UserControls/VehicleCtrl.ascx");
                control.myClaimId = this._claimId;
                control.vehicleId = Convert.ToInt32(cboGIClDetailVehicle.SelectedValue);
                pnlGI.Controls.Add(control);
            }

            //save the control count into a viewstate
            ViewState["controlCount"] = pnlGI.Controls.Count;
        }

        protected void gvReserves_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dtReserve = new DataTable();

            //Reserve myReserve = new Reserve();
            //dtReserve = myReserve.GetReserves_By_ClaimId(claim_id);
            //gvReserves.DataSource = dtReserve;

            //gvReserves.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void lbCloseClaim_Click(object sender, EventArgs e)
        {
            CloseClaim();
            this.SetTitle();
        }

        protected void tsbIRClaim_Click(object sender, ImageClickEventArgs e)
        {


          //  int exitCode;
          //  string arguments = @"/c """"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"" http://www.google.de""";
          //  string arguments = @"/c """"C:\Program Files (x86)\\ImageRight\\Clients\imageright.desktop.exe"" http://www.google.de""";
          //  Prepare the process to run
          // ProcessStartInfo start = new ProcessStartInfo();
          //  Enter in the command line arguments, everything you would enter after the executable name itself
          //  start.Arguments = arguments;

          //  start.UseShellExecute = false;
          //  start.RedirectStandardOutput = true;
          //  Enter the executable to run, including the complete path
          //  start.FileName = @"C:\Windows\system32\cmd.exe";
          //  Do you want to show a console window?
          //  start.WindowStyle = ProcessWindowStyle.Hidden;
          //  start.CreateNoWindow = true;

          //  Run the external process &wait for it to finish

          //Process proc = Process.Start(start);
          //  using (Process proc = Process.Start(start))
          //  {
          //      string output = proc.StandardOutput.ReadToEnd();
          //      proc.WaitForExit();
          //      // Retrieve the app's exit code
          //      exitCode = proc.ExitCode;
          //  }
            string loc = mf.getIRDocumentLinkByClaim(this.Claim.DisplayClaimId, mm.ClaimsDrawer);
            Response.Redirect(loc);

<<<<<<< HEAD


=======
<<<<<<< HEAD


=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            //Response.Write("<script>");
            //Response.Write("window.open('" + loc + "','_blank')");
            //Response.Write("</script>");

            ////string redirect = "<script>window.open('" + loc + "');</script>";
            ////Response.Write(redirect);
            //////mainForm.Focus();
            //string mywin = "my_window" + Claim.DisplayClaimId + ".focus();";
            //ClientScript.RegisterStartupScript(GetType(), "setfocus", mywin, true);


        }

        protected void tsbIRPolicy_Click(object sender, ImageClickEventArgs e)
        {
            if (this.Claim.PolicyNo == "")
            {

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This Claim isn't Associated with a Policy Yet.');</script>");
                //System.Windows.MessageBox.Show("This Claim isn't Associated with a Policy Yet", "No Policy on Claim",
                //       System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                return;
            }


            if (myClaim.ClaimTypeId != (int)modGeneratedEnums.ClaimType.Auto)
            {
                if ((myClaim.CompanyLocationId == (int)modGeneratedEnums.Company.Trisura_Commercial_Liability) || (myClaim.CompanyLocationId == (int)modGeneratedEnums.Company.Trisura_Commercial_Property))
                {
                    try
                    {
                        if (mf.getIRDocumentsByClaim(Claim.PolicyNo, mm.PolicyDrawer_MJIBrokerageCompanion) == null)
                        {
                            string loc = mf.getIRDocumentLinkByClaim(Claim.DisplayClaimId, mm.PolicyDrawer_MJIBrokerageCompanion, Claim.PolicyNo);
                            Response.Redirect(loc);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (mf.getIRDocumentsByClaim(Claim.PolicyNo, mm.PolicyDrawer_AllOtherCompanion) == null)
                        {
                            string loc = mf.getIRDocumentLinkByClaim(Claim.DisplayClaimId, mm.PolicyDrawer_AllOtherCompanion, Claim.PolicyNo);
                            Response.Redirect(loc);
                        }
                    }
                }
                else
                {
                    string loc = mf.getIRDocumentLinkByClaim(Claim.DisplayClaimId, mm.PolicyDrawer_MJIBrokerageCompanion, Claim.PolicyNo);
                    Response.Redirect(loc);
                }
            }
            else
            {
                string loc = mf.getIRDocumentLinkByClaim(Claim.DisplayClaimId, mm.PolicyDrawer, Claim.PolicyNo);
                Response.Redirect(loc);
            }




           // mf.getIRDocumentByClaim(this.Claim.DisplayClaimId, mm.PolicyDrawer);
        }

        protected void tsbClmCloseReopen_Click(object sender, ImageClickEventArgs e)
        {
            //try
            //{
                mm.UpdateLastActivityTime();

                //TODO
                //if (!this.SavePreviousScreenChanges())
                //    return;

                if (this.Claim.isClosed)
                    this.ReopenClaim();
                else
                    this.CloseClaim();

            this.SetTitle();

            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
            //SetTitle();
        }

        protected void tsbClmLocks_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!this.SavePreviousScreenChanges())
                    return;

                this.ShowClaimLocks();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void ShowClaimLocks()
        {
            try
            {
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=07";

                Session["referer"] = referer;

                mm.UpdateLastActivityTime();
                string url = "frmLockingInfo.aspx?mode=c&transaction_id=" + _claimId + "&lock_type_id=" + (int)modGeneratedEnums.RestrictionType.Claim;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                this.CheckClaimLock();          //check if the claim is locked
                this.ClaimScreenClicked();      //reload the data to the screen
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void tsbNewPolicy_Click(object sender, EventArgs e)
        {
            mnuAssocClaim2Policy_Click(null, null);
        }

        protected void tsbPoliceRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                    return;

                ////check for existing police report for claim
                //int nbr = db.GetIntFromStoredProcedure("usp_No_ChoicePoint_Requests_For_Claim",this._claimId);
                //if (nbr > 0)
                //{
                //    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Warning - ChoicePoint reports have already been ordered for this claim.  Do you wish to proceed?", true);
                //    //TODO
                //    //if (System.Windows.MessageBox.Show("Warning - ChoicePoint reports have already been ordered for this claim.  Do you wish to proceed?",
                //    //    "Reports Already Requested", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                //    return;
                //}

                string url = "https://www.carfaxforclaims.com/";
                string s = "window.open('" + url + "', 'popup_windowpr', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                //frmRequestPoliceReport f = new frmRequestPoliceReport(claim);
                //f.Show();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void gvVehicles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //DataTable dt = new DataTable();

            //Vehicle myData = new Vehicle();
            //dt = myData.GetVehicles_By_ClaimId(claim_id);
            //gvVehicles.DataSource = dt;

            //gvVehicles.PageIndex = e.NewPageIndex;
            //DataBind();
        }

        protected void tsbAssigReserves_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!this.SavePreviousScreenChanges())
                    return;

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;

                string url = "frmAssignReserves.aspx?claim_id=" + _claimId;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=800, copyhistory=no, left=400, top=100');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void mnuAssocClaim2Policy_Click(object sender, EventArgs e)
        {
            try
            {

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                    return;

                mm.UpdateLastActivityTime();

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;

                string url = "frmFindPolicy.aspx?mode=assign&date_of_loss=" + this.Claim.DateOfLoss + "&SubClaimTypeId=" + this.Claim.SubClaimTypeId + "&CompanyLocationId=" + this.Claim.CompanyLocationId + "&claim_id=" + this._claimId ;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=800, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        //public bool AssociateClaimWithNewPolicy(int claimId)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
        //            return false;

        //        MedCsxLogic.Claim claim = new MedCsxLogic.Claim(claimId);
        //        frmFindPolicy f = new frmFindPolicy(claim.DateOfLoss, claim.SubClaimTypeId, claim.CompanyLocationId);
        //        f.ShowDialog();

        //        if (f.okPressed)
        //        {
        //            claim.AssociateWithNewPolicy(f.policyNo, f.versionNo, f.isExpired);

        //            claim.SyncPolicy(true);
        //            claim.UpdateNamedInsured();
        //            string SaveDisplayClaimId = claim.DisplayClaimId;
        //            claim.UpdateCompanyLocationId(f.cboPolicyCompany.Text);

        //            //write ImageRight FUP File
        //            ImageRightFupFile fup = new ImageRightFupFile(claimId, SaveDisplayClaimId);
        //            fup.WriteFile();

        //            //write ImageRight AFUP File
        //            ImageRightAFupFile afup = new ImageRightAFupFile(claimId);
        //            afup.WriteFile();

        //            return true;
        //        }
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //        return false;
        //    }
        //}

        protected void mnuClaimReOpen_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                this.ReopenClaim();     //reopen this claim
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

      

        public void IssueDraft(int reserveId, List<Hashtable> CommercialPolicyValues = null, int claimId = 0)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                //if (!MicrFontsExist())
                //    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Issue_Draft))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Issue Drafts.');</script>");
                    return;
                }
               

                Reserve res = new Reserve(reserveId);
                if (res.Claim.maxSupplementalPaymentReached)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments. ');</script>");
                    //System.Windows.MessageBox.Show("The Maximum Number of Supplemental Payments have been Made Against This Claim.  " +
                    //    "It Must be Reopened to allow Payments.", "Max Supplemental Payments for Claim",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return;
                }

                if (res.maxSupplementalPaymentsReached)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('The Maximum Number of Supplemental Payments have been Made Against This Claim. It Must be Reopened to allow Payments. ');</script>");
                    //System.Windows.MessageBox.Show("The Maximum Number of Supplemental Payments have been Made Against This Reserve.  " +
                    //   "It Must be Reopened to allow Payments.", "Max Supplemental Payments for Reserve",
                    //   System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return;
                }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = reserveId + " " + this._claimId;

<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;

                string url = "frmDraft.aspx?mode=0&reserve_id=" + reserveId;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=800, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
                //frmDraft f = new frmDraft(reserveId);//, CommercialPolicyValues, claimId);
                //f.Show();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResSalvage_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value);
            
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Receive_Salvage))
                    return;

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;

<<<<<<< HEAD
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = reserveId + " " + this._claimId;
=======
<<<<<<< HEAD
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = reserveId + " " + this._claimId;
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


                string url = "frmReceiveSalvage.aspx?mode=0&reserve_id=" + this.reserveId;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=400, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResSubro_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value); this.reserveId = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value);
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Receive_Subro))
                {
                     Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Receive Subro.');</script>");
                     return;
                }

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = reserveId + " " + this._claimId;

<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                string url = "frmReceiveSubro.aspx?reserve_id=" + this.reserveId + "&mode=" + (int)SalvageSubro.Subro;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=400, copyhistory=no, left=500, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResDeductibles_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value);
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Recover_Deductibles) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Recover Deductibles.');</script>");
                return;
            }

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

            Session["referer"] = referer;

            string url = "frmRecoverDeductibles.aspx?reserve_id=" + this.reserveId;
            string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=300, height=300, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
        }

        protected void btnResSIR_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

            Session["referer"] = referer;


        }

        protected void btnResCreditSIR_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

            Session["referer"] = referer;
        }

        protected void btnResChangeLoss_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Loss_Reserve))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to change Loss Reserve.');</script>");
                return;
            }

            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            Reserve myReserve = new Reserve(reserveId);

            if (myReserve.ReserveStatusId == 2)
                ResChangeLoss.Visible = true;
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Unable to complete.  Reserve is closed.');</script>");
            
        }

        protected void btnResChangeExpense_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Expense_Reserve))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to change Expense Reserve.');</script>");
                return;
            }
                

            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            Reserve myReserve = new Reserve(reserveId);

            if (myReserve.ReserveStatusId == 2)
                ResChangeExpense.Visible = true;
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Unable to complete.  Reserve is closed.');</script>");

           
        }

        protected void btnResReOpen_Click(object sender, EventArgs e)
        {
            try
            {
                reserveId = Convert.ToInt32(grReserves.SelectedValue);
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Reopen_Reserve))
                    return;

                if (new Reserve(reserveId).ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This Reserve is Already Opened. ');</script>");
                    //System.Windows.MessageBox.Show("This Reserve is Already Opened", "Reserve Already Opened", System.Windows.MessageBoxButton.OK,
                    // System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    //Response.Write("<script>alert('This Reserve is Already Opened');</script>");
                    return;
                }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = reserveId + " " + this._claimId;

<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                string url = "frmReopenReserve.aspx?mode=" + (int)SalvageSubro.Reserve_Reopen + "&reserve_id=" + grReserves.SelectedValue.ToString();
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                //frmSmallWindow f = new frmSmallWindow(reserveId, (int)SalvageSubro.Reserve_Reopen);
                //f.ShowDialog();
                //return f.okPressed;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return;
            }
            //TODO Refresh grid
            //if (mf.ReopenReserve(this.reserveId))
            //    this.DisplayReserves();
        }

        protected void btnResClose_Click(object sender, EventArgs e)
        {




            CloseReserve(this.reserveId);
           
        }

        public void CloseReserve(int reserveId, bool totalLossFlag = false)
        {
            try
            {
                reserveId = Convert.ToInt32(grReserves.SelectedValue);
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Reserve))
                    return;

                Reserve res = new Reserve(reserveId);
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This Reserve is Already Closed. ');</script>");
                    return;
                }

               
                //commented out so reserve can be closed with unprinted drafts
                //if (res.getNoUnprintedDrafts() > 0)
                //{
                //    string url = "frmPicker2.aspx?mode=0&reserve_id=" + grReserves.SelectedValue.ToString();
                //    string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
                //    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                   
                //    return;
                //}

                //if APD, Comp, or Collision, ask if Total Loss.  Yes - Close Pending Salvage instead.
                if ((res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage) &&
                    ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) ||
                    (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive) ||
                    (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) ||
                    (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
                {
                    if (totalLossFlag)
                    {
                        res.isTotalLoss = true;
                        res.Update();
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reserve Will be Closed Pending Salvage.');</script>");
                        //System.Windows.MessageBox.Show("Reserve Will be Closed Pending Salvage", "Pending Salvage", System.Windows.MessageBoxButton.OK,
                        //    System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.OK);
                        
                        res.ClosePendingSalvage();
                        bool reservesLoaded = this.LoadReserves();
                        return;
                    }
                }

                //int denialReasonTypeId = 0;
                if ((res.PaidLoss == 0) && (paidLoss.Visible == false)) //if there are no loss payments, ask for denial reason
                {
                    //string url = "frmDenialReason.aspx?mode=1&void_reason_table=Denial_Reason_Type&reserve_id=" + reserveId;
                    //string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
                    //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                    ////TODO
                    //frmSmallWindow f = new frmSmallWindow("Denial_Reason_Type", 1);
                    //f.ShowDialog();
                    //if (!f.okPressed)
                    //    return;

                    paidLoss.Visible = true;

                    btnResClose.Enabled = false;
                    this.cboDenialReason.DataSource = TypeTable.getTypeTableRowsDDL("Denial_Reason_Type");
                    this.cboDenialReason.DataValueField = "Id";
                    this.cboDenialReason.DataTextField = "Description";
                    this.cboDenialReason.DataBind();

                    //int voidreason
                   // this.cboDenialReason.SelectedValue = modGeneratedEnums.DraftVoidReason.Printing_Problem.ToString();
                    return;
                }
                //else
                //{
                res.Close(0); //close the reserve
                bool reservesLoaded2 = this.LoadReserves();

                //}
                
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        

        protected void btnResCloseSubro_Click(object sender, EventArgs e)
        {
            string msg = "";
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            if (mf.ClosePendingSubro(this.reserveId, ref msg))
            { 
                DisplayReserves();
                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }
            }

            if (msg.Length > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
            }
        }

        protected void btnResCloseDeduct_Click(object sender, EventArgs e)
        {
            string msg = "";
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            if (mf.ClosePendingDeductibles(this.reserveId, ref msg))
            {
                DisplayReserves();
                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }
            }

            if (msg.Length > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
            }
        }

        //protected void btnResCloseSIR_Click(object sender, EventArgs e)
        //{
        //    this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
        //    if (mf.ClosePendingSIRRecovery(this.reserveId))
        //        DisplayReserves();
        //}

        protected void btnResVoid_Click(object sender, EventArgs e)
        {
            string msg = "";
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            if (mf.VoidReserve(this.reserveId, ref msg))
            {
                DisplayReserves();
                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }

            }

            if (msg.Length > 1)
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
        }

        protected void btnResReassign_Click(object sender, EventArgs e)
        {
            try
            {
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                      
                string sessionName = "reserve_id" + this._claimId;
                Session[sessionName] = reserveId + " " + this._claimId;
               

                

<<<<<<< HEAD
=======
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                string url = "frmAssignReserve.aspx?mode=c&reserve_id=" + this.reserveId ;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=400, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResHistory_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            string url = "frmReserveAssignmentHistory.aspx?reserve_id=" + reserveId + "&mode=" + (int)wireXfer.Transfer;
            string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
        }

        protected void btnResWireTransfer_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            string url = "frmWireTransfer.aspx?reserve_id=" + reserveId + "&mode=" + (int)wireXfer.Transfer;
            string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=600, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);

         
        }

        protected void btnResWireOverpayment_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            string url = "frmWireTransfer.aspx?reserve_id=" + reserveId + "&mode=" + (int)wireXfer.Overpayment;
            string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=600, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
        }

        protected void btnResPendingDraft_Click(object sender, EventArgs e)
        {
            try
            {
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                mm.UpdateLastActivityTime();

                if (reserveId == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Pending_Drafts))
                    return;      //user not authorized

                int index = Request.Url.ToString().IndexOf("?");
                if (index > 0)
                    referer = Request.Url.ToString().Substring(0, index);
                referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

                Session["referer"] = referer;


                string url = "frmDraft.aspx?mode=1&reserve_id=" + reserveId;
                string s2 = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=800, copyhistory=no, left=300, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResTotalLoss_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            //if (MessageBox.Show("Are you sure you want to set this Reserve as a Total Loss?", "Set Reserve As Total Loss", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel) == MessageBoxResult.OK)
            mf.SetReserveAsTotalLoss(this.reserveId);

            DisplayReserves();

            foreach (GridViewRow gvRow in grReserves.Rows)
            {
                if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                {
                    grReserves.SelectedIndex = gvRow.DataItemIndex;
                    break;
                }
            }
        }

        protected void btnResRecon_Click(object sender, EventArgs e)
        {

          

            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            Reserve myReserve = new Reserve(reserveId);

            if (myReserve.ReserveStatusId == 2)
                resrecon.Visible = true;
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Unable to complete.  Reserve is closed.');</script>");
        }

        protected void btnUnlockReserve_Click(object sender, EventArgs e)
        {
            this.btnUnlockReserve_MouseUp(null, null);
        }

        private void btnUnlockReserve_MouseUp(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);

            if (this.reserveId == 0)
                return;

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

            Session["referer"] = referer;

            string url = "frmLockingInfo.aspx?mode=c&transaction_id=" + this.reserveId + "&lock_type_id=" + modGeneratedEnums.RestrictionType.Reserve.ToString();
            string s = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            //this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            //if (this.reserveId == 0)
            //    return;

            //Reserve res = new Reserve(this.reserveId);      //create reserve object

            //int reserveLockId = res.FirstLockId;
            ////TODO
            ////mf.UnlockReserveLock(reserveLockId);        //unlock the reserve

            //this.RefreshReserveLock();      //refresh the reserve's lock information
            //this.DisplayReserves();         //refresh the reserve grid
        }

        protected void btnLockReserveInfo_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
          
            if (this.reserveId == 0)
                return;

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

            Session["referer"] = referer;

            string url = "frmLockingInfo.aspx?mode=c&transaction_id=" + this.reserveId + "&lock_type_id=" + modGeneratedEnums.RestrictionType.Reserve.ToString();
            string s = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            
        }

        protected void grReserves_SelectedIndexChanged(object sender, EventArgs e)
        {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            try
            {
                //int tmpreserveid = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value);
                if (grReserves.Rows.Count > 0)
                {
                    if (grReserves.SelectedIndex > -1)
                    { 
                        this.reserveId = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value);
                        Reserve res = new Reserve(this.reserveId);


                        this.lblNetReserveLoss.Text = grReserves.SelectedRow.Cells[5].Text;
                        this.lblNetReserveExp.Text = grReserves.SelectedRow.Cells[7].Text;
                        this.lblReserveLossPaid.Text = grReserves.SelectedRow.Cells[6].Text;
                        this.lblReserveExpPaid.Text = grReserves.SelectedRow.Cells[8].Text;
                        this.lblReserveCoverage.Text = grReserves.SelectedRow.Cells[1].Text; ;
                        this.lblReserveClaimant.Text = res.Claimant.Person.ExtendedDescription().Replace("\r\n", "<br />");
                        this.lblReserveAdjuster.Text = grReserves.SelectedRow.Cells[4].Text;
                        this.lblReserveLimit.Text = this.Claim.CoverageDescriptions(res.ReserveType.PolicyCoverageTypeId);

                        this.btnLockReserveInfo.Enabled = true;       //let user view lock info
                        this.RefreshReserveLock();  //refresh locking info

                        this.EnableDisableReserveControls();
                    }
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
<<<<<<< HEAD
=======
=======
            //int tmpreserveid = Convert.ToInt32(grReserves.DataKeys[grReserves.SelectedIndex].Value);
           if (grReserves.Rows.Count > 0)
           { 
                this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
                Reserve res = new Reserve(this.reserveId);
           

                this.lblNetReserveLoss.Text = grReserves.SelectedRow.Cells[5].Text; 
                this.lblNetReserveExp.Text = grReserves.SelectedRow.Cells[7].Text;
                this.lblReserveLossPaid.Text = grReserves.SelectedRow.Cells[6].Text;
                this.lblReserveExpPaid.Text = grReserves.SelectedRow.Cells[8].Text;
                this.lblReserveCoverage.Text = grReserves.SelectedRow.Cells[1].Text; ;
                this.lblReserveClaimant.Text = res.Claimant.Person.ExtendedDescription().Replace("\r\n","<br />");
                this.lblReserveAdjuster.Text = grReserves.SelectedRow.Cells[4].Text;
                this.lblReserveLimit.Text = this.Claim.CoverageDescriptions(res.ReserveType.PolicyCoverageTypeId);

                this.btnLockReserveInfo.Enabled = true;       //let user view lock info
                this.RefreshReserveLock();  //refresh locking info
           
                this.EnableDisableReserveControls();
           }
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        private void RefreshReserveLock()
        {
            try
            {
                Reserve res = new Reserve(this.reserveId);      //create a reserve object
                int reserveLockId = res.FirstLockId;

                if (reserveLockId == 0)
                {   //reserve is not locked
                    this.reserveLocked = false;
                    this.gbReserveLockedHeader.Text = "Reserve Not Locked";
                    this.gbReserveLockedHeader.ForeColor = System.Drawing.Color.Black;
                 
                    this.btnUnlockReserve.Enabled = false;
                    

                    this.lblReserveLockReason.Text = "";
                    this.lblReserveLockDate.Text = "";
                }
                else
                {   //reserve is locked
                    this.reserveLocked = true;
                    this.gbReserveLockedHeader.Text = "*** RESERVE LOCKED ***";
                    this.gbReserveLockedHeader.ForeColor = System.Drawing.Color.Red;
                    this.btnUnlockReserve.Enabled = true;
                   

                    //get lock reason and date
                    ReserveLock resLock = new ReserveLock(reserveLockId);
                    EventType et = resLock.LockFileNote().EventType();
                    this.lblReserveLockReason.Text = et.Description;
                    this.lblReserveLockDate.Text = resLock.LockDate.ToString("MM/dd/yyyy hh:mm:ss tt");
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResTransactions_Click(object sender, EventArgs e)
        {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);

            string sessionName = "reserve_id" + this._claimId;
            Session[sessionName] = reserveId + " " + this._claimId;
            
                int index = Request.Url.ToString().IndexOf("?");
<<<<<<< HEAD
=======
=======

            int index = Request.Url.ToString().IndexOf("?");
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

<<<<<<< HEAD
            
=======
<<<<<<< HEAD
            
=======
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            Session["referer"] = referer;

            try
            {
                mm.UpdateLastActivityTime();

                if (!this.btnResTransactions.Enabled)
                    return;     //function is disabled

                if (this.grReserves.SelectedValue == null)
                    return;     //no selected reserves

                string url = "frmTransactions.aspx?mode=c&claim_id=" + _claimId + "&reserve_id=" + grReserves.SelectedValue.ToString();
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResDraft_Click(object sender, EventArgs e)
        {
           
            Session["referer"] = referer;
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            IssueDraft(this.reserveId);
        }

      

        protected void mnuClaimClose_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                this.CloseClaim();      //close this claim
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnResCloseSalvage_Click(object sender, EventArgs e)
        {
            Session["referer"] = referer;
            string msg = "";
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            if (mf.ClosePendingSalvage(this.reserveId, ref msg))
            {
                DisplayReserves();
                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }
            }

            if (msg.Length > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
            }
        }

        protected void mnuExit_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

      
        protected void grAlerts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow grv = e.Row;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (grv.Cells[0].Text == "Yes")
                {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
                    e.Row.ForeColor = System.Drawing.Color.White;


<<<<<<< HEAD
=======
=======
                    e.Row.BackColor = System.Drawing.Color.Salmon;
                  
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                }
            }
        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            //rptParms = 
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }

        private Dictionary<string, string> GetReportParmsClaimID()
        {
          

            Dictionary<string, string> newparms = new Dictionary<string, string>();

            newparms.Add("claim_id", this._claimId.ToString());

            return newparms;
        }

        protected void hprNormalReport_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();

                rptParms.Add("claim", this._claimId.ToString());

                ShowReport("File Activity Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fFile+Activity+Report&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
            ////mf.ShowFileActivityReport(this._claimId);
        }

        protected void btnFileActivityDetailRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();

                rptParms.Add("claim", this._claimId.ToString());

                ShowReport("Detailed File Activity Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fDetailed+File+Activity+Report&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
            //mf.ShowFileActivityReportDetailed(this._claimId);
        }

        protected void btnFileActivitySummaryRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();

                rptParms.Add("claim", this._claimId.ToString());

                ShowReport("Summary File Activity Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fDetailed+File+Activity+Report&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
            //mf.ShowFileActivityReportSummary(this._claimId);
        }

        protected void btnFileActivityFileNotes_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();

                rptParms.Add("claim", this._claimId.ToString());

                ShowReport("User Entered File Notes", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fUser+Entered+File+Notes&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
            //mf.ShowUserEnteredFileNotesReport(this._claimId);
        }

        protected void clSummRpts_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsClaimID();

                ShowReport("Claim Summary", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fClaim+Summary&rs:Command=Render&claim_id=" + this._claimId;
            //ShowReport(url);

            //ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fClaim+Summary&rs:Command=Render&claim_id=" + claimId);
            //mf.ShowClaimSummaryReport(this._claimId);
        }

        protected void clActRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();

                rptParms.Add("claim", this._claimId.ToString());
                //rptParms = GetReportParmsClaimID();

                ShowReport("File Activity Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fFile+Activity+Report&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
           //mf.ShowFileActivityReport(this._claimId);
        }

        protected void clActDetailRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();

                rptParms.Add("claim", this._claimId.ToString());
                //rptParms = GetReportParmsClaimID();

                ShowReport("Detailed File Activity Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fDetailed+File+Activity+Report&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
            //mf.ShowFileActivityReportSummary(this._claimId);
            
        }

        protected void clActSummRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();
                rptParms.Add("claim", this._claimId.ToString());
                
                ShowReport("Detailed File Activity Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fDetailed+File+Activity+Report&rs:Command=Render&claim=" + this._claimId;
            //ShowReport(url);
            //mf.ShowFileActivityReportSummary(this._claimId);
        }

        protected void clDraftRpt_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms = new Dictionary<string, string>();
                rptParms.Add("claim_id", this._claimId.ToString());

                ShowReport("Draft Report For Claim", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            //string url = "http://reports.medjames.com/reportserver?%2fClaims%2fDraft+Report+For+Claim&rs:Command=Render&claim_id=" + this._claimId;
            //ShowReport(url);
            //mf.ShowDraftReport(this._claimId);
        }

        protected void tbGIClDetailIVDamage_TextChanged(object sender, EventArgs e)
        {
            if (tbGIClDetailLossDescr.Text.Length < 1)
                tbGIClDetailLossDescr.Text = tbGIClDetailIVDamage.Text;
        }

        protected void grDemandSummary_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //mm.UpdateLastActivityTime();

                ////this.btnAddDemandHistory.IsEnabled = false;
                ////this.btnAddOfferHistory.IsEnabled = false;

                //ClaimantGrid mySelection = (ClaimantGrid)grDemandSummary.SelectedItem;
                //if (mySelection == null)
                //{
                //    this.myDemand = null;
                //    return;
                //}

                ////set the selected demand
                //this.myDemand = new Demand(mySelection.claimantId);

                this.myDemand = new Demand((int)grDemandSummary.SelectedDataKey.Value);

                DisplayDemandOfferHistory();    //display the demand / offer history
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnAddNewDemand_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=14";

            Session["referer"] = referer;

            string url = "frmAddDemands.aspx?mode=u&claim_id=" + _claimId;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=500, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        public void ShowReport(string url)
        {
            //url += " + --new- window";
            try
            {
                mm.UpdateLastActivityTime();
                
                string s = "window.open('" + url + "', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=900, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
               
               
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnAddOfferHistory_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=14";

            Session["referer"] = referer;

            this.myDemand = new Demand((int)grDemandSummary.SelectedDataKey.Value);
            string url = "frmOffer.aspx?mode=u&claim_id=" + this._claimId + "&vendor_id=" + myDemand.VendorId + "&reserve_id=" + this.myDemand.ReserveId;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=500, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnAddDemandHistory_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=14";

            Session["referer"] = referer;
            if (grDemandSummary.SelectedIndex < 0)
                return;

            this.myDemand = new Demand((int)grDemandSummary.SelectedDataKey.Value);
            string url = "frmDemand.aspx?mode=u&claim_id=" + this._claimId + "&vendor_id=" + myDemand.VendorId + "&reserve_id=" + this.myDemand.ReserveId;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=500, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnNewTask_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=8";

            Session["referer"] = referer;

            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data) == false)
                return;
            
            string url = "frmNewTask.aspx?mode=2&claim_id=" + _claimId;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnClearTask_Click(object sender, EventArgs e)
        {
            if (gvTasks.SelectedIndex < 0)
                return;

            //if (MessageBox.Show("Are you sure you Want to clear this task?", "Clear Task", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
            //    return;


            mm.UpdateLastActivityTime();



            //clear user task
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=8";

            Session["referer"] = referer;

            int userTaskID = Convert.ToInt32(gvTasks.DataKeys[gvTasks.SelectedIndex].Value);
            string url = "frmClearTask.aspx?mode=2&claim_id=" + _claimId + "&user_task_id=" + userTaskID;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

 
        }

        protected void btnVehiclesAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=6";

            Session["referer"] = referer;

            string url = "frmVehicles.aspx?mode=c&claim_id=" + _claimId + "&vehicle_id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnVehiclesUpdate_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=6";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvVehicles.DataKeys[index].Value);

            string url = "frmVehicles.aspx?mode=u&claim_id=" + _claimId + "&vehicle_id=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnWitnessView_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=2";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvWitness.DataKeys[index].Value);


            string url = "frmWitnessPassengerDetails.aspx?mode=r&claim_id=" + _claimId + "&person_id=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnWitnessAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=2";

            Session["referer"] = referer;
            
            string url = "frmWitnessPassengerDetails.aspx?mode=c&claim_id=" + _claimId + "&person_id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnWitnessUpdate_Click(object sender, EventArgs e)
        {

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=2";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvWitness.DataKeys[index].Value);

            string url = "frmWitnessPassengerDetails.aspx?mode=u&claim_id=" + _claimId + "&person_id=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnPhoneAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=12";

            Session["referer"] = referer;

            string url = "frmManualPhoneNumber.aspx?mode=u&claim_id=" + _claimId + "&phone_number_id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=300, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnPhoneUpdate_Click(object sender, EventArgs e)
        {

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=12";

            Session["referer"] = referer;

            

           

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index2 = gridViewRow.RowIndex;
            int val = Convert.ToInt32(grPhoneNumbers.DataKeys[index2].Value);

            string type2 = grPhoneNumbers.Rows[index2].Cells[0].Text;

            string url = url = "frmManualPhoneNumber.aspx?mode=u&claim_id=" + _claimId + "&phone_number_id=" + val;

            switch (type2)
            {
                case "Agent":
                    break;
                case "Adjuster":
                    break;
                case "Manual Entry":

                    url = "frmManualPhoneNumber.aspx?mode=u&claim_id=" + _claimId + "&phone_number_id=" + val;
                    break;
                default:
                    if (grPhoneNumbers.Rows[index2].Cells[8].Text.Trim() != "0")
                    {
                        url = "frmAddEditVendor.aspx?mode=u&claim_id=" + _claimId + "&vendor_Id=" + grPhoneNumbers.Rows[index2].Cells[8].Text;
                    }
                    else if (grPhoneNumbers.Rows[index2].Cells[7].Text.Trim() != "0")
                    {
                        url = "frmPersonDetails.aspx?mode=u&claim_id=" + _claimId + "&person_id=" + grPhoneNumbers.Rows[index2].Cells[7].Text;
                    }
                    break;
            }

            
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnSalvageAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=16";

            Session["referer"] = referer;
        }

        protected void btnSalvageUpdate_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=16";

            Session["referer"] = referer;
        }

        protected void btnInjuredView_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=3";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvInjured.DataKeys[index].Value);

            string url = "frmInjuredDetails.aspx?mode=u&claim_id=" + _claimId + "&injured_id=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=650, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnInjuredAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=3";

            Session["referer"] = referer;

            //GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            //index = gridViewRow.RowIndex;
            //int val = Convert.ToInt32(gvInjured.DataKeys[index].Value);

            string url = "frmInjuredDetails.aspx?mode=u&claim_id=" + _claimId + "&injured_id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=650, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnInjuredUpdate_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=3";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvInjured.DataKeys[index].Value);
            
            string url = "frmInjuredDetails.aspx?mode=u&claim_id=" + _claimId + "&injured_id=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=650, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnVehiclesView_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=6";

            Session["referer"] = referer;

            string url = "frmVehicles.aspx?mode=r&claim_id=" + _claimId + "&vehicle_id=" + Convert.ToInt32(gvVehicles.DataKeys[gvVehicles.SelectedIndex].Value);
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnPDView_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=1";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvPropertyDamage.DataKeys[index].Value);

            string url = "frmPropertyDamage.aspx?mode=r&claim_Id=" + _claimId + "&property_damage_id=" + val;
            string s = "window.open('" + url + "', 'popup_windowPD', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200')";
            ClientScript.RegisterStartupScript(this.GetType(), "my_window2" + _claimId, s, true);
        }

        protected void btnPDAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=1";

            Session["referer"] = referer;

            string url = "frmPropertyDamage.aspx?mode=c&claim_Id=" + _claimId + "&property_damage_id=0";
            string s = "window.open('" + url + "', 'popup_windowPD', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200')";
            ClientScript.RegisterStartupScript(this.GetType(), "my_window2" + _claimId, s, true);
        }

        protected void btnPDUpdate_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=1";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvPropertyDamage.DataKeys[index].Value);

            string url = "frmPropertyDamage.aspx?mode=u&claim_Id=" + _claimId + "&property_damage_id=" + val;
            string s = "window.open('" + url + "', 'popup_windowPD', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200')";
            ClientScript.RegisterStartupScript(this.GetType(), "my_window2" + _claimId, s, true);
        }

        protected void gvFileNotes_Sorting1(object sender, GridViewSortEventArgs e)
        {

        }

        protected void tbFileActivitySearch_TextChanged(object sender, EventArgs e)
        {
            btnFileActivitySearch.Focus();
        }

        protected void btnFileActivityFileNote_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                return;

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=4";

            Session["referer"] = referer;

            string url = "frmNewFileNote.aspx?create_new_diary=false&claim_id=" + _claimId;
            string s = "window.open('" + url + "', 'popup_windowFN', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=620, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        }

        protected void btnFileActivityNewDiary_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                return;

            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=4";

            Session["referer"] = referer;

            string url = "frmNewFileNote.aspx?create_new_diary=true&claim_id=" + _claimId;
            string s = "window.open('" + url + "', 'popup_windowFN', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=620, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnFileActivityClearDiary_Click(object sender, EventArgs e)  //called from file activity
        {
            mm.UpdateLastActivityTime();
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                return;

            if (gvFileNotes.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Select the diary to clear.');</script>");
                return;
            }

            int aid = Convert.ToInt32(gvFileNotes.DataKeys[gvFileNotes.SelectedIndex].Value);

            FileNote f = new FileNote(aid);    //create file note object
            if (f.DiaryForUser(ml.CurrentUser) != null)
            {
                if (!f.DiaryType().AllowManualClear)
                {   //diary can't be cleared manually
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('This diary cannot be manually cleared.  The system will clear it automatically when the required conditions are met.');</script>");
                    //MessageBox.Show("This diary cannot be manually cleared.  The system will clear it automatically when the required conditions are met.", "Diary Cannot Be Cleared Manually", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

                    //fire diary clear attempt event
                    ScriptParms sp = new ScriptParms();
                    sp.GenerateParms(f);
                    Hashtable parms = sp.Parms;
                    parms.Add("Adjuster", ml.CurrentUser.Name);
                    EventType et = new EventType((int)modGeneratedEnums.EventType.Diary_Clear_Attempt);
                    et.FireEvent(parms);

                    return;
                }
                //clear the diary entry
                f.ClearDiaryEntry();

               // this.DiaryClearedText = "Yes";  //update cleared column in grid
            }

            DisplayFileActivity();

            //ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='/claim/';", true);

            
        }

        protected void btnFileActivityClearAllAlerts_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            //clear the alerts
            foreach (Alert a in this.Claim.Alerts())
                a.Clear();

            //ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='default.aspx';this.focus();", true);

            DisplayFileActivity();
            
        }

        protected void ClearMyAlert_Click(object sender, EventArgs e)
        {
            int aid = 0;
            for (int i = 0; i < grAlerts.Rows.Count; i++)
            {
                GridViewRow row = grAlerts.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {
                    
                        aid = Convert.ToInt32(grAlerts.DataKeys[i].Value);
                        Alert thisAlert = new Alert(aid);
                        if (!(thisAlert.isUrgent && !thisAlert.hasRepliedToAlert))
                            thisAlert.Clear();  //clear alert that is not urgent and has a reply

                }
            }

            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='/claim/';this.focus();", true);

            this.DisplayAlerts();

        }

        protected void btnFileActivityClearAlert_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(gvFileNotes.SelectedIndex) == -1)
            { 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select alert to clear.');</script>");
                    return;
            }
            mm.UpdateLastActivityTime();

            //create new file note and alert
            FileNote f = new FileNote(Convert.ToInt32(gvFileNotes.DataKeys[gvFileNotes.SelectedIndex].Value));
            Alert a = f.Alert();

            if (a != null)
            {
                //if (!(a.isUrgent && !a.hasRepliedToAlert))
                //{ 
                    a.Clear();  //clear the alert

                //DisplayFileActivity();
                


                ClientScript.RegisterStartupScript(this.GetType(), "reload", "window.top.location.href='default.aspx'", true);

                string url = "frmAlerts.aspx";
                string sAlert = "window.open('" + url + "', 'popup_windowAlert', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=700, height=300, copyhistory=no, left=400, top=200').focus();";
               // ClientScript.RegisterStartupScript(this.GetType(), "popup_windowAlert", sAlert, true);
               


                string strRedirect = "viewClaim.aspx?Ptabindex=4&claim_id=" + _claimId;
                Response.Redirect(strRedirect);

                //if (gvFileNotes.Rows.Count > 0)
                //gvFileNotes.SelectedIndex = 0;
                //}
            }

           
        }

        protected void btnClaimantView_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=5";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvClaimant.DataKeys[index].Value);

            string url = "frmClaimantDetails.aspx?mode=1&claim_id=" + _claimId + "&person_id=" + val;
            string s = "window.open('" + url + "', 'popup_windowClaimant', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=400, top=200')";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowClaimant" + _claimId, s, true);
        }

        protected void btnClaimantAdd_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=5";

            Session["referer"] = referer;

            //GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            //index = gridViewRow.RowIndex;
            //int val = Convert.ToInt32(gvClaimant.DataKeys[index].Value);

            string url = "frmClaimantDetails.aspx?mode=1&claim_id=" + _claimId + "&person_id=0";
            string s = "window.open('" + url + "', 'popup_windowClaimant', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=300, copyhistory=no, left=400, top=200')";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowClaimant" + _claimId, s, true);
        }

        protected void btnClaimantUpdate_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=5";

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvClaimant.DataKeys[index].Value);

            string url = "frmClaimantDetails.aspx?mode=1&claim_id=" + _claimId + "&person_id=" + val;
            string s = "window.open('" + url + "', 'popup_windowClaimant', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=300, copyhistory=no, left=400, top=200')";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_windowClaimant" + _claimId, s, true);
        }

        protected void gvFileNotes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow grv = e.Row;
<<<<<<< HEAD

           


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int val = Convert.ToInt32(gvFileNotes.DataKeys[e.Row.RowIndex].Values[0]);
                FileNote myNote = new FileNote(val);

              

                if (grv.Cells[4].Text == "No") //* && (grv.Cells[5].Text == mm.GetUserName(Convert.ToInt32(Session["userID"]))))*/
                {
                    //e.Row.BackColor = System.Drawing.Color.Salmon;
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
=======
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (grv.Cells[4].Text == "No")
                {
<<<<<<< HEAD
                    //e.Row.BackColor = System.Drawing.Color.Salmon;
                    e.Row.BackColor = System.Drawing.Color.FromArgb(236, 133, 64);
=======
                    e.Row.BackColor = System.Drawing.Color.Salmon;
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    e.Row.ForeColor = System.Drawing.Color.White;
                  

                }

                if (grv.Cells[5].Text == "1/1/1900 12:00:00 AM")
                {
                    grv.Cells[5].Text = "";
                }

<<<<<<< HEAD
                if (myNote.Alert() != null)
                {
                    if (myNote.Alert().ClearedFromMainScreen == false)
                    {
                        e.Row.BackColor = System.Drawing.Color.FromArgb(210, 19, 6);
                        e.Row.ForeColor = System.Drawing.Color.White;

                        gvFileNotes.SelectedIndex = e.Row.RowIndex;
                        gvFileNotes_SelectedIndexChanged(null, null);
                    }
                }

                // DataTable dt = mm.IsCheckUrgentAlert(Convert.ToInt32(gvFileNotes.DataKeys[e.Row.RowIndex].Values[0]), Convert.ToInt32(Session["userID"]));
                ////Hashtable ht =  ml.getRow("Alert", Convert.ToInt32(gvFileNotes.DataKeys[e.Row.RowIndex].Values[0]));
                // if (dt.Rows.Count > 0)
                // {
                //     e.Row.BackColor = System.Drawing.Color.Salmon;
                //     e.Row.ForeColor = System.Drawing.Color.White;
                // }

=======
               // DataTable dt = mm.IsCheckUrgentAlert(Convert.ToInt32(gvFileNotes.DataKeys[e.Row.RowIndex].Values[0]), Convert.ToInt32(Session["userID"]));
               ////Hashtable ht =  ml.getRow("Alert", Convert.ToInt32(gvFileNotes.DataKeys[e.Row.RowIndex].Values[0]));
               // if (dt.Rows.Count > 0)
               // {
               //     e.Row.BackColor = System.Drawing.Color.Salmon;
               //     e.Row.ForeColor = System.Drawing.Color.White;
               // }
               
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


            }
        }

        protected void gvFileNotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(gvFileNotes.DataKeys[gvFileNotes.SelectedIndex].Value);
            FileNote myFN = new FileNote(id);
            //.Replace("\r\n", "<br />")
            this.tbFileActivityFileNote.Text = myFN.FileNoteText; //.Replace("\r\n", "<br />");

            //gvFileNotes.Rows[gvFileNotes.SelectedIndex].Cells[0].Focus();
        }

        protected void btnCreateDocument_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                int mySelection = Convert.ToInt32(gvDocuments.DataKeys[gvDocuments.SelectedIndex].Value);



                string url = "frmPickDocumentFields.aspx?mode=c&claim_id=" + _claimId + "&document_type_id=" + mySelection;
                string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=500, copyhistory=no, left=400, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnCancelPending_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!(bool)this.rbPendingDocs.Checked)
                    return;

                int mySelection = Convert.ToInt32(gvDocuments.DataKeys[gvDocuments.SelectedIndex].Value);

                Document m_document = new Document(mySelection); //create a new document object

                //get the document details
                string[] documentDescription = m_document.TemplateFileName.Split('\\');
                string docFileName = documentDescription[documentDescription.GetUpperBound(0)];
                string[] documentFileDescription = docFileName.Split('.');
                string myDate = m_document.CreatedDate.ToString("MMddyyyy hhmmss");

                if (!m_document.ManuallyAdded)
                {   //document was added by the system
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You cannot cancel this document because it was added by the system.');</script>");
                    //MessageBox.Show("You cannot cancel this document because it was added by the system.", "System Added Document", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }

                //if (MessageBox.Show("Are you sure you want to cancel the selected document?", "Confirm Cancellation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                //    return;

                m_document.DocumentStatusId = (int)modGeneratedEnums.DocumentStatus.Cancelled;      //set the document status ID

                //delete the pending document
                string doc2Kill = documentFileDescription[0] + "-" + m_document.ClaimId + "-" + myDate + "-" + mySelection.ToString() + ".docx";
                string KillDocument = System.IO.Path.Combine(mm.DOCUMENT_DIR, doc2Kill);
                m_document.DeleteRow();
                //System.IO.File.Delete(KillDocument);

                DisplayDocuments();     //redisplay the documents
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        //protected void btnOpenDocument_Click(object sender, EventArgs e)
        //{

        //    if (gvDocuments.SelectedValue == null)
        //        return;

        //    int mySelection = Convert.ToInt32(gvDocuments.DataKeys[gvDocuments.SelectedIndex].Value);

        //    MedCsxLogic.Document m_document = new MedCsxLogic.Document(mySelection); //create a new document object from the document ID


        //    //get the document details
        //    string[] documentDescription = m_document.TemplateFileName.Split('\\');
        //    string docFileName = documentDescription[documentDescription.GetUpperBound(0)];
        //    string[] documentFileDescription = docFileName.Split('.');
        //    string myDate = m_document.CreatedDate.ToString("MMddyyyy hhmmss");

        //    //create the document filename
        //    string filepath = mm.DOCUMENT_DIR + documentFileDescription[0] + "-" + m_document.ClaimId + "-" + myDate + "-" + mySelection.ToString() + ".doc";

        //    using (DocumentFormat.OpenXml.Packaging.WordprocessingDocument
        //        wordDocument = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(filepath, false)) ;

        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + filepath + "','_blank')", true);

        //}

        protected void btnOpenDocument_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            //ReserveGrid mySelection = (ReserveGrid)gvDocuments.SelectedRow;
            //ReserveGrid mySelection = 
            //(from GridViewRow cada in gvDocuments.SelectedRow.ToString() select cada.DataItem)
            //.Select(x => (ReserveGrid)x).ToList(); ;
            //if (mySelection == null)
            //    return;

            //if (mySelection.id == 0)
            //    return;

            if (gvDocuments.SelectedValue == null)
                return;


            int mySelection = Convert.ToInt32(gvDocuments.DataKeys[gvDocuments.SelectedIndex].Value);

            Document m_document = new Document(mySelection); //create a new document object from the document ID

            //get the document details
            string[] documentDescription = m_document.TemplateFileName.Split('\\');
            string docFileName = documentDescription[documentDescription.GetUpperBound(0)];
            string[] documentFileDescription = docFileName.Split('.');
            string myDate = m_document.CreatedDate.ToString("MMddyyyy hhmmss");

            //create the document filename
            //string fileName = Server.MapPath(mm.DOCUMENT_DIR) + documentFileDescription[0] + "-" + m_document.ClaimId + "-" + myDate + "-" + mySelection.ToString() + ".doc";
            string fileName = Server.MapPath(mm.DOCUMENT_DIR) + "Completed Documents\\" + documentFileDescription[0] + "-" + m_document.ClaimId + "-" + myDate + "-" + mySelection.ToString() + ".doc";
            FileInfo myDoc = new FileInfo(fileName);


            if (myDoc.Exists)
            {
                DisplayDoc(myDoc.Name, myDoc.FullName, myDoc.Length.ToString());
            }
            else
            {
                fileName = Server.MapPath(mm.DOCUMENT_DIR) + "Completed Documents\\" + documentFileDescription[0] + "-" + m_document.ClaimId + "-" + myDate + "-" + mySelection.ToString() + ".docx";
                FileInfo myDocX = new FileInfo(fileName);
                if (myDocX.Exists)
                {
                    DisplayDoc(myDocX.Name, myDocX.FullName, myDocX.Length.ToString());
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Document Not Found. " + myDoc.FullName.Replace("\\", "\\\\") + "******" + myDocX.FullName.Replace("\\", "\\\\") + "');</script>");
                }
            }






        }

        protected void DisplayDoc(string docName, string docFullName, string docLength)
        {
            Response.Clear();
            Response.ContentType = "Application/msword";
            Response.AddHeader("content-disposition", "attachment;filename=" + docName);
            Response.AddHeader("Content-Length", docLength);
            Response.ContentType = "application/octet-stream";

            Response.WriteFile(docFullName);

            Response.End();
        }

        protected void rbPendingDocs_CheckedChanged(object sender, EventArgs e)
        {
            DisplayDocuments();
        }

        protected void rbCompletedDocs_CheckedChanged(object sender, EventArgs e)
        {
            DisplayDocuments();
        }

        protected void rbAvailableDocs_CheckedChanged(object sender, EventArgs e)
        {
            DisplayDocuments();
        }

        protected void btnAppraisalsView_Click(object sender, EventArgs e)
        {
            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=10";

            Session["referer"] = referer;
        }

        protected void TabContainer2_ActiveTabChanged(object sender, EventArgs e)
        {
            double verNo = 0.00;
            if (this.TabContainer1.ActiveTabIndex == 0)  //Vehicles
            {
                if (gvPolicyData.SelectedIndex < 0)
                    return;
                verNo = Convert.ToDouble(gvPolicyData.SelectedRow.Cells[6].Text);

                bool vehiclesLoaded = LoadPolicyVehicles(verNo);
            }  
            if (this.TabContainer1.ActiveTabIndex == 0)  //Individuals
            {
                    if (gvPolicyData.SelectedIndex < 0)
                        return;
                    verNo = Convert.ToDouble(gvPolicyData.SelectedRow.Cells[6].Text);

               
                bool individualsLoaded = LoadPolicyInd(verNo);
            }
            if (this.TabContainer1.ActiveTabIndex == 0)  //Notes
            {
                LoadNotes();
            }

        }

        private bool LoadPolicyVehicles(double version_no)
        {
            this.grPolVehicles.DataSource = mm.LoadVehicles(mm.GetPolicyVehicles(this.Claim.PolicyNo, version_no.ToString(), 1), 2);
            this.grPolVehicles.DataBind();
            //this.tiPolVehicles.Header = "Vehicles (" + this.grPolVehicles.Items.Count.ToString() + ")";
            return true;
        }

        protected void gvPolicyData_SelectedIndexChanged(object sender, EventArgs e)
        {
            double verNo = Convert.ToDouble(gvPolicyData.SelectedRow.Cells[6].Text);

            bool vehiclesLoaded = LoadPolicyVehicles(verNo);
            bool individualsLoaded = LoadPolicyInd(verNo);
        }

        private bool LoadPolicyInd(double version_no)
        {
            this.grPolIndividuals.DataSource = mm.LoadClaimants(mm.GetPolicyIndividuals(this.Claim.PolicyNo, version_no.ToString(), 1), 3);
            this.grPolIndividuals.DataBind();
            //this.tiPolIndividuals.Header = "Individuals (" + this.grPolIndividuals.Items.Count.ToString() + ")";
            return true;
        }

        protected void gvPolicyData_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = Session["Policy" + _claimId + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvPolicyData.DataSource = dt;
                gvPolicyData.DataBind();

                Session["Policy" + _claimId + "Table"] = dt;
            }
        }

        protected void gvDocuments_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Documents" + _claimId + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvDocuments.DataSource = dt;
                gvDocuments.DataBind();

                Session["Documents" + _claimId + "Table"] = dt;
            }
        }

        protected void btnAddEditSIU_Click(object sender, EventArgs e)
        {
            try
            {
                if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Users))
                {
                    Session["referer"] = Request.Url.ToString();
                    string url3 = "frmfraud.aspx?mode=99&claim_id=" + _claimId + "&perpetrator=0";
                    string s3 = "window.open('" + url3 + "', 'popup_windowFraud', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
                    ClientScript.RegisterStartupScript(this.GetType(), "popup_windowFraud", s3, true);
                }
                else
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You do not have authorization to add/edit SIU database.');</script>");
//                MessageBox.Show("You do not have authorization to add/edit SIU database.", "Not Authorized");
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

       

        protected void btnResCloseTotalLoss_Click(object sender, EventArgs e)
        {
            CloseReserve(this.reserveId, true);
        }

        protected void btnContinueResRecon_Click_Click(object sender, EventArgs e)
        {
            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            string msg = "";
            mf.ReconReserve(this.reserveId, double.Parse(txtresreconamount.Text), ref msg);
            if (msg.Length > 1)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Unable to complete" + msg + "');</script>");
            }
            else
            {
                DisplayReserves();
                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }

                resrecon.Visible = false;
            }

           
        }

        protected void btnCancelContinueResRecon_Click(object sender, EventArgs e)
        {
            resrecon.Visible = false;
        }

        protected void btnContinueResChangeLoss_Click(object sender, EventArgs e)
        {
            reserveId = Convert.ToInt32(grReserves.SelectedValue);


            if (ChangeNetLossReserve(this.reserveId))
            {
                this.DisplayReserves();

                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }
            }

            txtResChangeLoss.Text = "";
            ResChangeLoss.Visible = false;
        }

        public bool ChangeNetLossReserve(int reserveId,  List<Hashtable> CommercialPolicyValues = null)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return false;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Loss_Reserve))
                    return false;

                if (new Reserve(reserveId).Claim.PolicyNo == "")  //check for policy number on claim
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You can't set loss reserves on a claim without a policy number assigned.  " +
                    "It causes serious problems with Personal Lines accounting.  Please wait until a policy " +
                    "has been assigned to this claim.');</script>");
                    return false;
                }

                double s = double.Parse(txtResChangeLoss.Text);

                double newReserve = s;
                if (newReserve < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reserves Cannot be Set to a Negative Amount.');</script>");
      
                    return false;
                }
                else if (newReserve == 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('To Set a Reserve to Zero, the Reserve Must Be Closed. Please Close the reserve first.');</script>");
                    return false;
                }
                else
                    return ChangeNetLossReserve(reserveId, newReserve, CommercialPolicyValues);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + ex.ToString() + "');</script>");
                
                return false;
            }
        }

        public bool ChangeNetLossReserve(int reserveId, double newAmount, List<Hashtable> CommercialPolicyValues = null)
        {
            string noLockPart = ".  Proceeding Will Place the Reserve in a Locked State Until Approved by your Supervisor.";
            string lockedPart = ".  This would normally add a lock to the reserve, but since it's alread locked, the system will just create a note instead.";


            Reserve res = new Reserve(reserveId);
            double oldAmount,  //old net reserve amount
                perPerson = 0,  //Per-Person coverage
                perAccident = 0,  //Per-Accident coverage
                previousPayments = 0,  //Previous payments on reserve line
                userAuthority = 0,  //User's financial authority for reserve line
                amountChange = 0;  //the change from the old to new amounts
            Hashtable eventTypeIds = new Hashtable(),  //Event type IDs for firing events.  Keyed by Event_Type_Id, value is parms dictionary
                parms = new Hashtable();  //parms dictionary passed to event handling scripts
            int periodTypeId = 0,  //Per-Person period type
                periods = 0,  //Per-Person number of periods
                reserveTypeId = 0,  //reserve type ID
                claimId = res.ClaimId;  //Claim ID for reserve
            bool deductible = false; //Reserve has deductible amount (comp or collision)
            MedCsxLogic.Claim resClaim = new MedCsxLogic.Claim(claimId);  //Claim associated with reserve line

            //get per-Person and per-Accident coverages
            PolicyCoverage pc = res.PolicyCoverage();
            if (pc != null)
            {
                perPerson = pc.PerPersonCoverage;
                periodTypeId = pc.PerPersonPeriodTypeId;
                periods = pc.PerPersonNoPeriods;
                perAccident = pc.PerAccidentCoverage;
                deductible = pc.hasDeductible;
            }

            oldAmount = res.NetLossReserve;
            amountChange = newAmount - oldAmount;

            if (newAmount < oldAmount)
            {
                if (eventTypeIds.Count == 0)
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                return true;
            }

            if ((resClaim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Commercial) && (resClaim.PolicyNo != ""))
            {
                return false;  //commercial not supported
            }

            reserveTypeId = res.ReserveTypeId;
            previousPayments = res.TotalLossPayments;
            userAuthority = res.UserAuthority();

            if (newAmount + previousPayments > userAuthority)
            {
                string reasonPart = "The Amount Entered of " + newAmount.ToString("c") + " Plus Previous Payments of "
                    + previousPayments.ToString("c") + " Exceeds Your Financial Authority of " + userAuthority.ToString("c");
                string msg = reasonPart + ". Proceeding Will Place the Reserve in a Locked State Until Approved by your Supervisor.";

                if (res.isLocked)
                    msg = reasonPart + ".  This would normally add a lock to the reserve, but since it's alread locked, the system will just create a note instead.";


                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                parms.Add("Previous_Payments", previousPayments);
                parms.Add("User_Authority", userAuthority);
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed_Above_Adjuster_Authority, parms);
            }

            //if comp/collision, there is no coverage limit so we don't need to check for coverage
            if (deductible)
            {
                if (eventTypeIds.Count == 0)
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                return true;
            }

            //if manually unlocked for policy not in force for date of loss, don't check coverage limits - adjuster authority limits are good enough
            if (resClaim.PolicyManuallyInForce)
            {
                if (eventTypeIds.Count == 0)
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                return true;
            }

            if (resClaim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Auto)
            {
                if (perPerson == 0 & perAccident == 0) //check for no coverage
                {
                    string reasonPart = "There is No Coverage for this Reserve";
                    string msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;


                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
                   

                    PolicyCoverageType pct = res.PolicyCoverageType();
                    if (pct.hasPerPersonCoverage)
                    {
                        parms.Add("Previous_Payments", previousPayments);
                        parms.Add("Per_Person_Coverage", perPerson);
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                    }
                    else if (pct.hasPerAccidentCoverage)
                    {
                        parms.Add("Previous_Payments", previousPayments);
                        parms.Add("Per_Accident_Coverage", perAccident);
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, parms);
                    }
                    if (eventTypeIds.Count == 0)
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                    res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                    return true;
                }

                //check for amount exceeding per-Person coverage (Flat)
                if ((perPerson > 0) && (periodTypeId == (int)modGeneratedEnums.PeriodType.Flat) && (newAmount + previousPayments > perPerson))
                {
                    string reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Person Coverage of " + perPerson.ToString("c");
                    string msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("Per_Person_Coverage", perPerson);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                }

                //check for amount exceeding per-Person coverage (Periodic)
                if ((perPerson > 0) && (periodTypeId != (int)modGeneratedEnums.PeriodType.Flat) && (newAmount + previousPayments > perPerson))
                {
                    string reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Person Coverage of " + res.CoverageDescription;
                    string msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("Per_Person_Coverage", perPerson * periods);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                }

                //check for amount exceeding per-Accident coverage
                if ((perPerson > 0) &&
                    (newAmount - oldAmount + res.Claim.LossForCoverageType(res.ReserveTypeId) +
                    res.Claim.NetLossReserveForCoverageType(res.ReserveTypeId) > perAccident))
                {
                    string reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Accident Coverage of " + perAccident.ToString("c");
                    string msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;


                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                    parms.Add("Reserve_Amount", previousPayments);
                    parms.Add("Per_Accident_Coverage", perAccident);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, parms);
                }
            }
            if (eventTypeIds.Count == 0)
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
            res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
            return true;
        }

        protected void btnCancelResChangeLoss_Click(object sender, EventArgs e)
        {
            ResChangeLoss.Visible = false;
        }

        protected void btnContinueResChangeExpense_Click(object sender, EventArgs e)
        {
            reserveId = Convert.ToInt32(grReserves.SelectedValue);
            if (ChangeNetExpenseReserve(this.reserveId))
            {
                this.DisplayReserves();

                foreach (GridViewRow gvRow in grReserves.Rows)
                {
                    if ((int)grReserves.DataKeys[gvRow.DataItemIndex].Value == reserveId)
                    {
                        grReserves.SelectedIndex = gvRow.DataItemIndex;
                        break;
                    }
                }
            }

            txtResChangeExpense.Text = "";
            ResChangeExpense.Visible = false;
        }

        public bool ChangeNetExpenseReserve(int reserveId, List<Hashtable> CommercialPolicyValues = null)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return false;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Expense_Reserve))
                    return false;

               
                Reserve res = new Reserve(reserveId);
                bool isDone = false;

                double s = double.Parse(txtResChangeExpense.Text);
              

                return ChangeNetExpenseReserve(reserveId, s, CommercialPolicyValues);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + ex.ToString() + "');</script>");
                return false;
            }
        }

        public bool ChangeNetExpenseReserve(int reserveId, double newAmount, List<Hashtable> CommercialPolicyValues = null)
        {

            string noLockPart = ".  Proceeding Will Place the Reserve in a Locked State Until Approved by your Supervisor.";
            string lockedPart = ".  This would normally add a lock to the reserve, but since it's alread locked, the system will just create a note instead.";



            Reserve res = new Reserve(reserveId);
            double oldAmount,  //old net reserve amount
                    previousPayments = 0,  //Previous payments on reserve line
                    userAuthority = 0,  //User's financial authority for reserve line
                    amountChange = 0;  //the change from the old to new amounts
            Hashtable eventTypeIds = new Hashtable(),  //Event type IDs for firing events.  Keyed by Event_Type_Id, value is parms dictionary
                parms = new Hashtable();  //parms dictionary passed to event handling scripts
            MedCsxLogic.Claim resClaim = new MedCsxLogic.Claim(res.ClaimId);  //Claim associated with reserve line

            oldAmount = res.NetLossReserve;
            amountChange = newAmount - oldAmount;

            if (newAmount >= oldAmount)
            {
                if ((resClaim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Commercial) && (resClaim.PolicyNo != ""))
                    return false;  //commercial not supported

                previousPayments = res.TotalLossPayments;
                userAuthority = res.UserAuthority();

                if (newAmount + previousPayments > userAuthority)
                {
                    string reasonPart = "The Amount Entered of " + newAmount.ToString("c") + " Plus Previous Payments of "
                        + previousPayments.ToString("c") + " Exceeds Your Financial Authority of " + userAuthority.ToString("c");
                    string msg = reasonPart + noLockPart;

                    if (res.isLocked)
                        msg = reasonPart + lockedPart;


                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");

                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("User_Authority", userAuthority);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Expense_Reserve_Changed_Above_Adjuster_Authority, parms);
                }
            }

            if (eventTypeIds.Count == 0)
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Expense_Reserve_Changed, parms);
            res.ChangeNetExpenseReserve(newAmount, eventTypeIds);
            return true;
        }

        protected void btnCancelResChangeExpense_Load(object sender, EventArgs e)
        {
            ResChangeExpense.Visible = false;
        }

        protected void btnBIEvaluation_Click(object sender, EventArgs e)
        {
            //int index = Request.Url.ToString().IndexOf("?");
            //if (index > 0)
            //    referer = Request.Url.ToString().Substring(0, index);
            //referer += "?claim_id=" + this._claimId + "&Ptabindex=3";

            //Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(gvInjured.DataKeys[index].Value);

            string url = "frmBIEvaluation.aspx?injured_id=" + val;
            string s = "window.open('" + url + "', 'popup_window5', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=900, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void cboGIClDetailVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboGIClDetailVehicle.SelectedItem == null)
                return;
            mm.UpdateLastActivityTime();

            //set the insured vehicle
            this.insuredVehicle = new Vehicle(Convert.ToInt32(cboGIClDetailVehicle.SelectedValue));

            this.UpdateLossPayee();     //update the loss payee field
            this.UpdatePersonVehicleEditButtons();  //update the vehicle edit buttons
            this.DisplayCoverages();    //update policy coverages displayed
            lbGIClDetailVehicle.Text = insuredVehicle.LongName.Replace("\r\n","<br>"); 
        }

       

        protected void cboGIClDetailatFault_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Claim.AtFaultDateSet = ml.DBNow();
        }

        protected void cboGIClDetailDriver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboGIClDetailDriver.SelectedValue != "")
                lblGIClDetailDriver.Text = new Person(Convert.ToInt32(cboGIClDetailDriver.SelectedValue)).Description.Replace("\r\n", "<br>");

            //this.driverPersonId = Convert.ToInt32(cboGIClDetailDriver.SelectedValue);
            //UpdateDriverPhoneNo();
            //UpdatePersonVehicleEditButtons();

        }

        protected void gvPropertyDamage_SelectedIndexChanged(object sender, EventArgs e)
        {

            int mySelection = Convert.ToInt32(gvPropertyDamage.DataKeys[gvPropertyDamage.SelectedIndex].Value);
           
                    PropertyDamage pd = new PropertyDamage(mySelection);
                    PDpropType.Text = pd.PropertyType().Description.ToString();
                    PDpropDescr.Text = pd.VehicleName;
                    PDdamageDescr.Text = pd.DamageDescription;
                    PDloc.Text = pd.WhereSeen;
                    PDestAmt.Text = gvPropertyDamage.SelectedRow.Cells[4].Text;
                    PDowner.Text = pd.OwnerPerson().ExtendedDescription().Replace("\r\n", "<br />");
                    PDdriver.Text = pd.DriverPerson().ExtendedDescription().Replace("\r\n", "<br />");

                    if (((string)pd.OtherInsuranceCompany != "") && ((string)pd.OtherInsurancePolicyNo != ""))
                        PDotherIns.Text = (string)pd.OtherInsuranceCompany;
                    else if (pd.OtherInsuranceCompany != "")
                        PDotherIns.Text = pd.OtherInsuranceCompany;
                    else
                        PDotherIns.Text = "";

                   
              
        }


        #endregion

        #region selectedindexchanging
        protected void ddlShowFileNotesByType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable();

            //FileNotes myData = new FileNotes();
            //dt = myData.GetFileNotes(claim_id, Convert.ToInt32(ddlShowFileNotesByType.SelectedValue), 0, 0, 0);

            //dt.DefaultView.Sort = "created_date Desc";
            //gvFileNotes.DataSource = dt;
            //gvFileNotes.DataBind();
            //gvFileNotes.Visible = true;
            //Session["FileNotes" + claim_id + "Table"] = dt;
        }

        protected void mnuClaimLocks_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (!this.SavePreviousScreenChanges())
                    return;

                this.ShowClaimLocks();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void gvDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDocName.Text = gvDocuments.SelectedRow.Cells[1].Text;
        }

        protected void btnMarkDocumentComplete_Click(object sender, EventArgs e)
        {
            int documentID = Convert.ToInt32(gvDocuments.DataKeys[gvDocuments.SelectedIndex].Value);

            Hashtable htDocuments = new Hashtable();
            htDocuments.Add("Document_Id", documentID.ToString());
            htDocuments.Add("Document_Status_Id", (int)modGeneratedEnums.DocumentStatus.Printed);
            htDocuments.Add("Date_Printed", DateTime.Now.ToString());
            ml.updateRow("Document", htDocuments);

            DisplayDocuments();
            
        }

        protected void cboGIClDetailOwner_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboGIClDetailOwner.SelectedValue != "")
                lblGIClDetailOwner.Text = new Person(Convert.ToInt32(cboGIClDetailOwner.SelectedValue)).Description.Replace("\r\n", "<br>");
        }

        //protected void btnClearAllTasks_Click(object sender, EventArgs e)
        //{
        //    if (this.Claim.OutstandingTasks().Count > 0)
        //    {
        //        this.Claim.OutstandingTasks().Clear();
        //        this.Claim.Update();
        //        LoadTask();   
        //    }

        //}

        protected void ddlShowFileNotesForClaimant_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable();

            //FileNotes myData = new FileNotes();
            //dt = myData.GetFileNotes(claim_id, 0, Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue), 0, 0);

            //dt.DefaultView.Sort = "created_date Desc";
            //gvFileNotes.DataSource = dt;
            //gvFileNotes.DataBind();
            //gvFileNotes.Visible = true;



            //Session["FileNotes" + claim_id + "Table"] = dt;

            //Reserve myReserves = new Reserve();
            //dt = myReserves.usp_Get_Reserve_Names_For_Claimant(Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue));
            //DataRow newRowR = dt.NewRow();
            //newRowR["reserve_name"] = "All";

            //dt.Rows.InsertAt(newRowR, 0);

            //ddlShowFileNotesForReserve.DataSource = dt;
            //ddlShowFileNotesForReserve.DataBind();
            //ddlShowFileNotesForReserve.Visible = true;
        }

        protected void lbDenialReason_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboDenialReason.SelectedValue) == 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There are no loss payments for this reserve.  Please Select a denial reason and hit continue.');</script>");
                return;
            }

            this.reserveId = Convert.ToInt32(grReserves.SelectedValue);
            Reserve res = new Reserve(this.reserveId);
            int denialReasonTypeId = Convert.ToInt32(cboDenialReason.SelectedValue);
            res.Close(denialReasonTypeId); //close the reserve
                                           //    bool reservesLoaded = this.LoadReserves();

            paidLoss.Visible = false;
            bool reservesLoaded = this.LoadReserves();
        }

        protected void lbCancelPaidLoss_Click(object sender, EventArgs e)
        {
            paidLoss.Visible = false;
            btnResClose.Enabled = true;

        }

        protected void cboGIClDetailInsuredPhone_SelectedIndexChanged(object sender, EventArgs e)
        {
            Person p = new Person(this.Claim.InsuredPersonId);

            switch (cboGIClDetailInsuredPhone.SelectedItem.ToString())
            {
                case "Work":
                    txtGIClDetailInsuredPh.Text = p.WorkPhone;
                    break;
                case "Cell":
                    txtGIClDetailInsuredPh.Text = p.CellPhone;
                    break;
                case "Other":
                    txtGIClDetailInsuredPh.Text = p.OtherContactPhone;
                    break;
                case "Fax":
                    txtGIClDetailInsuredPh.Text = p.Fax;
                    break;
                default:
                    txtGIClDetailInsuredPh.Text = p.HomePhone;
                    break;

            }
                
            
        }

        protected void cboGIClDetailOwnerPhone_SelectedIndexChanged(object sender, EventArgs e)
        {
            Person p = new Person(Convert.ToInt32(cboGIClDetailOwner.SelectedValue));

            switch (cboGIClDetailOwnerPhone.SelectedItem.ToString())
            {
                case "Work":
                    txtGIClDetailOwnerPh.Text = p.WorkPhone;
                    break;
                case "Cell":
                    txtGIClDetailOwnerPh.Text = p.CellPhone;
                    break;
                case "Other":
                    txtGIClDetailOwnerPh.Text = p.OtherContactPhone;
                    break;
                case "Fax":
                    txtGIClDetailOwnerPh.Text = p.Fax;
                    break;
                default:
                    txtGIClDetailOwnerPh.Text = p.HomePhone;
                    break;

            }

        }

        protected void cboGIClDetailDriverPhone_SelectedIndexChanged(object sender, EventArgs e)
        {
            Person p = new Person(Convert.ToInt32(cboGIClDetailDriver.SelectedValue));

            switch (cboGIClDetailDriverPhone.SelectedItem.ToString())
            {
                case "Work":
                    txtGIClDetailDriverPh.Text = p.WorkPhone;
                    break;
                case "Cell":
                    txtGIClDetailDriverPh.Text = p.CellPhone;
                    break;
                case "Other":
                    txtGIClDetailDriverPh.Text = p.OtherContactPhone;
                    break;
                case "Fax":
                    txtGIClDetailDriverPh.Text = p.Fax;
                    break;
                default:
                    txtGIClDetailDriverPh.Text = p.HomePhone;
                    break;

            }
        }

        protected void ddlShowFileNotesForReserve_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlShowFileNotesForReserve.SelectedValue == "All")
            //{
            //    DataTable dt = new DataTable();

            //    FileNotes myData = new FileNotes();
            //    dt = myData.GetFileNotes(claim_id, 0, 0, Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue), 0);

            //    dt.DefaultView.Sort = "created_date Desc";
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();
            //    gvFileNotes.Visible = true;



            //    Session["FileNotes" + claim_id + "Table"] = dt;
            //}
            //else
            //{
            //    DataTable selectedTable = Session["FileNotes" + claim_id + "Table"] as DataTable;

            //    DataTable dt = selectedTable.AsEnumerable()
            //                    .Where(r => r.Field<string>("reserve").Contains(ddlShowFileNotesForReserve.SelectedValue))
            //                    .CopyToDataTable();
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();

            //    Session["FileNotes" + claim_id + "Table"] = dt;
            //}






        }
        #endregion

        #region checkedchanged
        protected void chkDiariesOnly_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkDiariesOnly.Checked)
            //{
            //    DataTable selectedTable = Session["FileNotes" + claim_id + "Table"] as DataTable;

            //    DataTable dt = selectedTable.AsEnumerable()
            //                    .Where(r => r.Field<string>("diary_type").Length > 0)
            //                    .CopyToDataTable();
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();

            //    Session["FileNotes" + claim_id + "Table"] = dt;
            //}
            //else
            //{
            //    DataTable dt = new DataTable();

            //    FileNotes myData = new FileNotes();
            //    dt = myData.GetFileNotes(claim_id, 0, 0, 0, 0);

            //    dt.DefaultView.Sort = "created_date Desc";
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();
            //    gvFileNotes.Visible = true;

            //    Session["FileNotes" + claim_id + "Table"] = dt;
            //}

        }
        #endregion

        #region buttonclickevents
        protected void btnSearchFileNotes_Click(object sender, EventArgs e)
        {
            //if (txtSearchFileNote.Text.Length > 0)
            //{
            //    DataTable selectedTable = Session["FileNotes" + claim_id + "Table"] as DataTable;

            //    DataTable dt = selectedTable.AsEnumerable()
            //                    .Where(r => r.Field<string>("file_note_text").Contains(txtSearchFileNote.Text))
            //                    .CopyToDataTable();
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();

            //    Session["FileNotes" + claim_id + "Table"] = dt;
            //}
            //else
            //{
            //    DataTable dt = new DataTable();

            //    FileNotes myData = new FileNotes();
            //    dt = myData.GetFileNotes(claim_id, 0, 0, 0, 0);

            //    dt.DefaultView.Sort = "created_date Desc";
            //    gvFileNotes.DataSource = dt;
            //    gvFileNotes.DataBind();
            //    gvFileNotes.Visible = true;

            //    Session["FileNotes" + claim_id + "Table"] = dt;
            //}
        }

       

      
        #endregion

        #region selectedindexchanged
        protected void gvDemands_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int myVendorId = Convert.ToInt32(gvDemands.SelectedRow.Cells[5].Text);
            //int myReserveId = Convert.ToInt32(gvDemands.SelectedRow.Cells[6].Text);

            //populateDemandHistory(myVendorId, myReserveId);
        }
        protected void rblDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayDocuments();
        }
        #endregion

        private bool SaveGeneralInfo(bool prompt = false)
        {
           
           
            mm.UpdateLastActivityTime();

            string msg = "";
            if (!this.Claim.ValidateVIN(this.insuredVehicle))
            {
                if (this.Claim.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Insured Vehicle has Invalid VIN.');</script>");

          

            }


            int claimantId,
                newReserveId;

            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                return false;  //user wasn't authorized to perform operation

            if (!this.ValidDateReported())
                return false;  //invalid date

            //check required fields
            if ((string.IsNullOrWhiteSpace(this.tbGIClDetailTOL.Text) || (Convert.ToDateTime(this.tbGIClDetailTOL.Text).ToShortTimeString() == ml.DEFAULT_DATE.ToShortTimeString())))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Time of loss is a required entry.');</script>");
                this.tbGIClDetailTOL.Focus();
                return false;
            }

                if (this.cboGIClDetailRptType.SelectedValue == "0")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reported By Must be Selected.');</script>");
                    this.cboGIClDetailRptType.Focus();
                    return false;
                }
                else if (this.tbGIClDetailRptBy.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reported By Must be Entered.');</script>");
                    this.tbGIClDetailRptBy.Focus();
                    return false;
                }
                else if (this.tbGIClDetailLossLoc.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss Location Must be Entered.');</script>");
                    this.tbGIClDetailLossLoc.Focus();
                    return false;
                }
                else if (this.tbGIClDetailLossCity.Text.Trim() == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss City Must be Entered.');</script>");
                    this.tbGIClDetailLossCity.Focus();
                    return false;
                }
                else if (this.cboGIClDetailLossState.SelectedIndex < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Loss State Must be Selected.');</script>");
                    this.cboGIClDetailLossState.Focus();
                    return false;
                }
                else if (this.cboGIClDetailatFault.SelectedIndex < 0)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Fault of Driver Must be Selected.');</script>");
                    this.cboGIClDetailatFault.Focus();
                    return false;
                }
                
                this.PopulateClaim();

                if (this.Claim.isCollision || this.Claim.isComp)
                {
                    if (this.tbGIClDetailWhere.Text.Trim() == "")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Where IV Seen Must be Entered if Comp or Collision.');</script>");
                        this.tbGIClDetailWhere.Focus();
                        return false;
                    }
                }

                //save general claim data
                Hashtable oldRow = this.Claim.oldRow;
                
                this.Claim.Update();        //comp/collision lines created automatically
                

            //if DOL, IV, or Driver has changed, resync with policy
            if ((this.Claim.DateOfLoss != (DateTime)oldRow["Date_Of_Loss"]) ||
                    (this.Claim.InsuredVehicleId != (int)oldRow["Insured_Vehicle_Id"]) ||
                    (this.Claim.DriverPersonId != (int)oldRow["Driver_Person_Id"]))
            {
                this.SyncPolicy();
                this.DisplayCoverages();

                bool dc = mf.CheckForPossibleDuplicateClaims(this.Claim, PossibleDuplicateClaimMode.Date_Of_Loss_Changed);

                if (dc)
                {
                    string url = "frmPossibleDuplicateClaims.aspx?mode=c&policy_no=" + this.Claim.PolicyNo + "&DoL=" + tbGIClDetailDOL.Text + "&claimID=" + this.Claim.ClaimId;
                    string s = "window.open('" + url + "', 'popup_windowdup', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=250, copyhistory=no, left=200, top=200');";
                    ClientScript.RegisterStartupScript(this.GetType(), "scriptdup", s, true);
                }
            }

                //Sends notice for No Loss statement through IR
                if (this.Claim.PolicyNo.Substring(1, 2) == "NV")
                {
                    try
                    {
                        if ((this.Claim.NoLossSent == ml.DEFAULT_DATE) && ((ml.DBNow() - this.Claim.CreatedDate).Days == 0))
                        {
                            //isFirstSave = True
                            SetNoLossTask();
                        }
                        else if (this.Claim.DateOfLoss != (DateTime)oldRow["Date_Of_Loss"])
                        {
                            SetNoLossTask(1);
                        }
                    }
                    catch (Exception ex)
                    {
                        SetNoLossTask();
                    }

                }


            //create comp/collision reserve lines if checked
            if ((bool)this.ckbGIClDetailIVComp.Checked)
            {
                claimantId = this.Claim.CreateClaimantIfNecessary(this.Claim.InsuredPersonId);
                Claimant claimant = new Claimant(claimantId);
                newReserveId = claimant.CreateReserveIfNecessary((int)modGeneratedEnums.ReserveType.Comprehensive);

                //assign user/usergroup
                Reserve res = new Reserve(newReserveId);
                res.AssignTo(this.compUserId, this.compUserGroupId);

                if (this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Comprehensive) == "None")
                    res.FireEvent((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
            }

                if ((bool)this.ckbGIClDetailIVColl.Checked)
                {
                    claimantId = this.Claim.CreateClaimantIfNecessary(this.Claim.InsuredPersonId);
                    Claimant claimant = new Claimant(claimantId);
                    newReserveId = claimant.CreateReserveIfNecessary((int)modGeneratedEnums.ReserveType.Collision);

                    //assign user/usergroup
                    Reserve res = new Reserve(newReserveId);
                    res.AssignTo(this.compUserId, this.compUserGroupId);

                    if (this.Claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Collision) == "None")
                        res.FireEvent((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
                }

            if (prompt)
            {
                lblSuccess.Visible = true;
                lblSuccess.ForeColor = System.Drawing.Color.Red;
                lblSuccess.Text = "Claim Saved";
            }
           

            this.UpdateGeneralFrame();
                return true;
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //    return false;
            //}
            //finally
            //{
               
            //}
        }

        private void SetNoLossTask(int mode = 0)
        {
            //'Add record to IRTask.NoLossReview
            this.Claim.SetNoLossTask(mode);

        }
       
    

        private bool ValidDateReported()
        {
            if (!ml.isDate(this.tbGIClDetaildtReported.Text.Trim()))
            {
                Response.Write("<script>alert('You Must Enter a Valid Date Reported.');</script>");

                if (this.tbGIClDetaildtReported.Visible == true)
                    this.tbGIClDetaildtReported.Focus();
                return false;
            }
            else if ((DateTime.Parse(this.tbGIClDetaildtReported.Text.Trim())).Year < 1900)
            {
                Response.Write("<script>alert('You Must Enter a Valid Date Reported.');</script>");
                if (this.tbGIClDetaildtReported.Visible == true)
                    this.tbGIClDetaildtReported.Focus();
                return false;
            }
            return true;
        }

        private void PopulateClaim()
        {
            switch (this.Claim.ClaimTypeId)
            {
                case (int)modGeneratedEnums.ClaimType.Auto:
                    //checkboxes
                    this.Claim.hasCoverageIssue = (bool)this.ckbGIClDetailCoverage.Checked;
                    this.Claim.hasDisputedLiability = (bool)this.ckbGIClDetailLiability.Checked;
                    this.Claim.isComp = (bool)this.ckbGIClDetailIVComp.Checked;
                    this.Claim.isCollision = (bool)this.ckbGIClDetailIVColl.Checked;
                    this.Claim.isFoxproConversion = false;
                    this.Claim.PermissionToUse = (bool)this.ckbGIClDetailPerm2Use.Checked;
                    this.Claim.LargeLoss = (bool)this.ckbGIClDetailLrgLoss.Checked;

                    //textboxes
                    this.Claim.VehicleDamage = this.tbGIClDetailIVDamage.Text.Trim();
                    this.Claim.WhereSeen = this.tbGIClDetailWhere.Text.Trim();
                    this.Claim.AuthorityContacted = this.tbGIClDetailAuthContact.Text.Trim();
                    this.Claim.PoliceReportNumber = this.tbGIClDetailRptNo.Text.Trim();
                    this.Claim.ReportedBy = this.tbGIClDetailRptBy.Text.Trim();
                    this.Claim.Comments = this.tbGIClDetailComments.Text.Trim();
                    this.Claim.LossLocation = this.tbGIClDetailLossLoc.Text.Trim();
                    this.Claim.LossCity = this.tbGIClDetailLossCity.Text.Trim();
                    this.Claim.LossDescription = this.tbGIClDetailLossDescr.Text.Trim();
                    if (ml.isDate(this.tbGIClDetailDOL.Text.Trim()))
                    {
                        if (IsValidTime(this.tbGIClDetailTOL.Text.Trim()))
                        {
                            string dtLossTime = this.tbGIClDetailDOL.Text.Trim() + " " + this.tbGIClDetailTOL.Text.Trim();
                            this.Claim.DateOfLoss = DateTime.Parse(dtLossTime);
                        }
                    }

                    if (ml.isDate(this.tbGIClDetaildtReported.Text.Trim()))
                        this.Claim.DateReported = DateTime.Parse(this.tbGIClDetaildtReported.Text.Trim());

                    //combo boxes
                    this.Claim.LossState_Id = Convert.ToInt32(this.cboGIClDetailLossState.SelectedValue);
                    this.Claim.ReportedByTypeId = Convert.ToInt32(this.cboGIClDetailRptType.SelectedValue);
                    this.Claim.AccidentTypeId = Convert.ToInt32(this.cboGIClDetailAccidentType.SelectedValue);

                    this.Claim.AtFaultTypeId = Convert.ToInt32(this.cboGIClDetailatFault.SelectedValue);
                   
                    this.Claim.hasSR22 = (bool)cboGIClDetailSR22.Checked;


                    //claim values
                    this.Claim.OwnerPersonId = Convert.ToInt32(cboGIClDetailOwner.SelectedValue);
                    this.Claim.DriverPersonId = Convert.ToInt32(cboGIClDetailDriver.SelectedValue);
                    this.Claim.InsuredPersonId = this.insurePersonId;
                    this.Claim.InsuredVehicleId = Convert.ToInt32(cboGIClDetailVehicle.SelectedValue);
                    break;

                default:
                    break;
            }
        }

        private bool IsValidTime(string p)
        {
            DateTime dt;
            return DateTime.TryParse(p, out dt);
        }


        private void ClosePrompt()
        {
            string msgPrompt = "<b>Open Reseves</b><br>"; ;

            foreach (Reserve res in this.Claim.Reserves())
            {
                //if ((res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve) && (res.ReserveTypeId != (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage)
                //    && ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive)
                //    || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                {
                    msgPrompt += "Open Reserve: " + res.ReserveType + "<br>";

                }
            }


            if (Claim.OutstandingTasks().Count > 0)
            {
                msgPrompt += "<br><br><b>Unclosed Tasks</b><br>";
                foreach (UserTask t in Claim.OutstandingTasks())
                {

                    msgPrompt += "Task:" + t.UserTaskType().Description;

                }
            }

            //check for open appraisals
            List<Hashtable> OpenAppraisals = Claim.OpenAppraisals();
            if (OpenAppraisals.Count > 0)
            {
                msgPrompt += "<br><br><b>Open Appraisals</b><br>";
                ////send an alert to the appraisers so they know the claim is being closed

                //create a list of unique appraiser IDs
                List<int> uniqueIDs = new List<int>();
                foreach (Hashtable appraisal in OpenAppraisals)
                {

                    if (!uniqueIDs.Contains((int)appraisal["appraiser_id"]))
                        uniqueIDs.Add((int)appraisal["appraiser_id"]);  //add unique appraiser ID to the list
                }

                //foreach (int appraiserID in uniqueIDs)
                //{
                //    if (appraiserID != 0)
                //    {
                //        //the appraiser ID is valid
                //        //Alert claimClosedAlert = new Alert();
                //        //int noteID = db.GetIntFromStoredProcedure("usp_Get_Claim_Closed_File_Note_ID", claim.Id);

                //        //if (noteID != 0)  //file note ID is valid
                //        //    SendAlert(noteID, 0, appraiserID); //send alert to the appraiser
                //    }
                //}
            }

            //send alert to all of the uncleared diary owners that are not the person closing the claim
            List<Hashtable> unclearedDiaries = Claim.UnclearedDiaryList;
            if (unclearedDiaries.Count > 0)
            {
                msgPrompt += "<br><br><b>Uncleared Diaries</b><br>";
                DataTable dt = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Uncleared_Diary_Entries_With_Images", Claim.ClaimId);

                foreach (DataRow row in dt.Rows)
                {
                    msgPrompt += row["Diary_Type"].ToString() + " Sent To: " + row["sent_to"].ToString() + " Sent From: " + row["sent_from"].ToString() + " Due on: " + row["diary_due_date"].ToString();


                }
            }


        }


        private void CloseClaim()
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Claim))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Close Claim.');</script>");
                return;
            }


            foreach (Reserve res in this.Claim.Reserves())
            {
                //if ((res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve) && (res.ReserveTypeId != (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage)
                //    && ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive)
                //    || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) || (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                {
                    //MouseNormal();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('There are open reserves for this claim, please close them prior to closing claim.');</script>");

                    return;

                }
            }


            int index = Request.Url.ToString().IndexOf("?");
            if (index > 0)
                referer = Request.Url.ToString().Substring(0, index);
            referer += "?claim_id=" + this._claimId + "&Ptabindex=7";

            Session["referer"] = referer;

            string url = "frmCloseClaim.aspx?claim_id=" + _claimId;
            string s = "window.open('" + url + "', 'popup_window_close_claim', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=700, height=600, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "popup_window_close_claim", s, true);

            //string msg = "";
            //if (mf.CloseClaim(this.Claim, ref msg))
            //{
            //    this.tsbClmCloseReopen.ImageUrl = "../content/Images/ClaimOpenClaim.png";
            //    this.tsbClmCloseReopen.ToolTip = "Reopen Claim";
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Claim Closed Successfully.');</script>");
            //    ////ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);   //able to close the claim
            //}
            //else
            //{
            //    this.DisplayReserves();     //unable to close this claim - go to reserves
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + msg + "');</script>");
            //}
        }

        

        private void ReopenClaim()
        {
            mf.ReopenClaim(_claimId);   //reopen the claim

            //refresh data
            this.LoadClaim();
            this.EnableDisableReOpen();
            this.SyncPolicy();
            this.DisplayFileActivity();
            this.ClaimScreenClicked();
            tsbClmCloseReopen.ImageUrl = "../content/Images/claimcloseclaim.png";
            this.tsbClmCloseReopen.ToolTip = "Close Claim";
        }

        private int CurrentClaimScreen
        {
            get { return this.TabContainer1.ActiveTabIndex; }
        }

        private void ClaimScreenClicked()
        {
            //TODO
            try
            {
                this.UpdateGeneralFrame();

                if (previousClaimScreen < 0)
                    previousClaimScreen = this.TabContainer1.ActiveTabIndex;
                else
                {
                    if (previousClaimScreen == CurrentClaimScreen)
                    {
                        if (CurrentClaimScreen != 0)
                            this.LoadClaim();  //load the claim data
                        this.SetTitle();    //set the form text for claim
                        this.EnableDisableClaimControls();  //enable/disable claim controls
                        if (previousClaimScreen == 0)
                            this.EnableDisableGIControls();
                        return;
                    }
                }
               

                previousClaimScreen = CurrentClaimScreen;
                if (!this.SavePreviousScreenChanges())
                    return;

                this.LoadClaimScreen();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
            finally
            {
              
            }
        }

        private void LoadClaimScreen()
        {
            try
            {
                this.EnableDisableClaimControls();  //enable/disable the correct claim controls
                this.SetTitle();    //set the form text for claim

                switch (this.CurrentClaimScreen)
                {
                    case 0:     //general info
                        this.EnableDisableGIControls();
                        this.DisplayClaimData();
                        if (tbGIClDetailDOL.Visible == true)
                            this.tbGIClDetailDOL.Focus();
                        break;
                    case 1:     //property damage
                        this.DisplayPropertyDamage();
                        break;
                    case 2:     //Witness/Passenger
                        this.DisplayWitnessPassenger();
                        break;
                    case 3:     //Injured
                        this.DisplayInjured();
                        break;
                    case 4:     //file activity
                        this.DisplayFileActivity();
                        break;
                    case 5:     //claimants
                        this.DisplayClaimants();
                        break;
                    case 6:     //vehicles
                        this.DisplayVehicles();
                        break;
                    case 7:     //reserves
                        this.DisplayReserves();
                        break;
                    case 8:     //tasks
                        this.DisplayTasks();
                        break;
                    case 9:     //policy data
                        this.DisplayPolicyData();
                        break;
                    case 10:     //appraisals
                        this.DisplayAppraisals();
                        break;
                    case 11:
                        //this.DisplaySalvage();
                        break;
                    case 12:     //claim audit
                    //this.DisplayClaimAudit();
                    case 13:     //documents
                        this.DisplayDocuments();
                        break;
                    case 14:     //phone numbers
                        this.DisplayPhoneNumbers();
                        break;
                    case 15:     //possible duplicates
                        this.DisplayDuplicateClaims();
                        break;
                    case 16:     //demand/offer
                        this.DisplayDemandOffer();
                        break;
                    case 17:     //litigation
                        this.DisplayLitgation();
                        break;
                    case 21:    //Alerts
                        this.DisplayAlerts();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private void EnableDisableClaimControls()
        {
            if (this.Claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
            {
                //toolbar controls
                this.tsbAssigReserves.Enabled = false;
                //this.tsbAssigAppraiser.IsEnabled = false;
                //this.tsbNewPolicy.IsEnabled = false;
                //this.tsbPoliceRpt.IsEnabled = false;
                //this.tsbClmLocks.IsEnabled = false;
                this.tsbClmCloseReopen.Enabled = true;
                //this.tsbClmReopen.IsEnabled = true;

                //set image and text to Reopen
                this.tsbClmCloseReopen.ImageUrl = "../content/Images/ClaimOpenClaim.png";
                tsbClmCloseReopen.ToolTip = "Reopen Claim";
                //this.lblCloseReopen.Content = "Reopen Claim";

                //menu items
                this.mnuAssocClaim2Policy.Enabled = false;
                this.mnuExit.Enabled = false;
                this.mnuClaimReOpen.Enabled = true;
                this.mnuClaimClose.Enabled = false;
                //this.mnuClaimLocks.IsEnabled = false;

                //buttons
                //this.btnClaimantAdd.Enabled = false;
                //this.btnClaimantEdit.IsEnabled = false;
                //this.btnVehiclesAdd.IsEnabled = false;
                ////this.btnVehiclesEdit.IsEnabled = false;
                //this.btnPropertyDamageAdd.IsEnabled = false;
                //this.btnPropertyDamageEdit.IsEnabled = false;
                //this.btnWitnessPassengerAdd.IsEnabled = false;
                //this.btnWitnessPassengerEdit.IsEnabled = false;
                //this.btnInjuredAdd.IsEnabled = false;
                //this.btnInjuredEdit.IsEnabled = false;
                //this.btnAddNewDemand.IsEnabled = false;
                //this.btnAddDemandHistory.IsEnabled = false;
                //this.btnAddOfferHistory.IsEnabled = false;
                //this.btnLitigationAdd.IsEnabled = false;
                //this.btnLitigationEdit.IsEnabled = false;
            }
            else
            {
                //toolbar controls
                this.tsbAssigReserves.Enabled = true;
                //this.tsbAssigAppraiser.IsEnabled = true;
                //this.tsbNewPolicy.IsEnabled = true;
                //this.tsbPoliceRpt.IsEnabled = true;
                //this.tsbClmLocks.IsEnabled = true;
                //this.tsbClmCloseReopen.IsEnabled = true;
                //this.tsbClmReopen.IsEnabled = false;

                //set image and text to Close
                //this.tsbClmCloseReopen.ImageUrl = "../content/Images/claimcloseclaim.png";
                //tsbClmCloseReopen.ToolTip = "Close Claim";
                //this.lblCloseReopen..Content = "Close Claim";

                //menu items
                this.mnuAssocClaim2Policy.Enabled = true;
                this.mnuExit.Enabled = true;
                this.mnuClaimReOpen.Enabled = false;
                this.mnuClaimClose.Enabled = true;
                //this.mnuClaimLocks.IsEnabled = true;

                //buttons
                //this.btnClaimantAdd.Enabled = true;
                //this.btnClaimantEdit.Enabled = true;
                //this.btnVehiclesAdd.Enabled = true;
                //this.btnVehiclesEdit.Enabled = true;
                //this.btnPropertyDamageAdd.Enabled = true;
                //this.btnPropertyDamageEdit.Enabled = true;
                //this.btnWitnessPassengerAdd.Enabled = true;
                //this.btnWitnessPassengerEdit.Enabled = true;
                //this.btnInjuredAdd.Enabled = true;
                //this.btnInjuredEdit.Enabled = true;
                //this.btnAddNewDemand.Enabled = true;
                //this.btnAddDemandHistory.Enabled = true;
                //this.btnAddOfferHistory.Enabled = true;
                //this.btnLitigationAdd.IsEnabled = true;
                //this.btnLitigationEdit.IsEnabled = true;
            }
        }
        private void EnableDisableReOpen()
        {
            if (this.Claim.isClosed)
            {
                //TODO
                //mnuExit.IsEnabled = false;
                tsbClmCloseReopen.Enabled = true;
                //this.imgCloseReopen.Source = new BitmapImage(new Uri("../../Forms/Images/mnuFileOpen.png", UriKind.Relative));
                //this.lblCloseReopen.Content = "Reopen Claim";
                tsbClmCloseReopen.ImageUrl = "../content/Images/ClaimOpenClaim.png";
                this.tsbClmCloseReopen.ToolTip = "Reopen Claim";
            }
            else
            {
                //mnuExit.IsEnabled = true;
                tsbClmCloseReopen.Enabled = true;
                tsbClmCloseReopen.ImageUrl = "../content/Images/claimcloseclaim.png";
                this.tsbClmCloseReopen.ToolTip = "Close Claim";
                this.SetTitle();
                //this.imgCloseReopen.Source = new BitmapImage(new Uri("../../Forms/Images/icoClose.png", UriKind.Relative));
                //this.lblCloseReopen.Content = "Close Claim";
            }
        }

        class phoneGrid
        {
            public int id { get; set; }
            public int image_index { get; set; }
            public int claimId { get; set; }
            public int personId { get; set; }
            public int vendorId { get; set; }
            public bool autoRefresh { get; set; }
            public string fullName { get; set; }
            public string type { get; set; }
            public string homePhone { get; set; }
            public string workPhone { get; set; }
            public string cellPhone { get; set; }
            public string fax { get; set; }
            public string email { get; set; }
            public string notes { get; set; }
            public BitmapImage imageIdx
            {
                get
                {
                    return modLogic.ImageList(image_index);
                }
            }
        }
    }
}
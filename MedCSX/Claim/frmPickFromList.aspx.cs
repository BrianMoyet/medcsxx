﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmPickFromList : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static ModMain mm = new ModMain();
        private MedCsxLogic.Claim m_claim;
        private int claimId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            claimId = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
            m_claim = new MedCsxLogic.Claim(claimId);
            ArrayList reserveDescriptions = m_claim.ReserveDescriptions();

            if (!Page.IsPostBack)
            {
                LoadData(reserveDescriptions);
            }

        }

        protected void LoadData(ArrayList reserveDescriptions)
        {
            grPick.DataSource = reserveDescriptions;
            grPick.DataBind();
        }

        protected void cmdOk_Click(object sender, EventArgs e)
        {
            if (grPick.SelectedIndex > -1)
            {
                string cid = grPick.SelectedRow.Cells[1].Text;

                Reserve r = new Reserve(m_claim, cid);

                ClientScript.RegisterStartupScript(this.GetType(), "close", "this.opener.window.close();", true);

                string url = "frmPendingDraft.aspx?reserve_id=" + r.ReserveId;
                string w = "window.open('" + url + "', 'popup_window3', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=800, copyhistory=no, left=500, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", w, true);

          
                //ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a reserve.');</script>");
                //MessageBox.Show("Please select a reserve.", "Pick a Reserve", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmAddEditVendor : System.Web.UI.Page
    {
        public Vendor vendor = null;
        public bool okPressed = false;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private List<VendorTypeGrid> myVendorTypeList;
        private List<VendorTypeGrid> myVendorSubTypeList;
        private bool inValid = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.vendor = new Vendor();
            this.vendor = new Vendor(Convert.ToInt32(Request.QueryString["vendor_Id"]));
            
            //try
            //{
                mm.UpdateLastActivityTime();

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Vendor_Tax_Id_Required))
                    this.chkVendorTaxIdRequired.Enabled = false;
                if (!Page.IsPostBack)
                { 
                    bool typeLoaded = LoadTypes();
                    //mf.LoadStateComboBox(this.cboVendorState);
                    mf.LoadStateDDL(this.cboVendorState);

                if (Convert.ToInt32(Request.QueryString["vendor_Id"]) == 0)
                    //if (vendor.isAdd)
                    {   //adding vendor
                        this.Page.Title = "Add Vendor";
                       
                    }
                    else
                    {   //editing vendor
                        LoadVendor();
                        this.Page.Title = "Edit Vendor";
                        //this.cboVendorType.SelectedValue = vendor.VendorSubType().VendorTypeId.ToString(); //vendor.VendorTypeId.ToString();// Request.QueryString["vendor_type_Id"].ToString();
                        //string temp = vendor.VendorSubTypeId.ToString();
                        //this.cboVendorSubType.SelectedValue = vendor.VendorSubTypeId.ToString();
                        //this.chkVendorTaxIdRequired.Checked = true;

                }
                }
            //}
            //catch (Exception ex)
            //{
            //    mf.ProcessError(ex);
            //}
        }

        private int setSelection(int p, int mode = 0)
        {
            int idIdx = 0;
            switch (mode)
            {
                case 1:
                    foreach (VendorTypeGrid v in myVendorSubTypeList)
                    {
                        if (v.id == p)
                            break;
                        idIdx++;
                    }
                    return idIdx;
                default:
                    foreach (VendorTypeGrid v in myVendorTypeList)
                    {
                        if (v.id == p)
                            break;
                        idIdx++;
                    }
                    return idIdx;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
           

            

            if (vendor.isAdd)
            {
                if (mm.VendorExists(this.txtVendorName.Text))
                {
                   lblMessageBox.Text = "Vendor " + this.txtVendorName.Text + " Already Exists in the Database.";
                    this.txtVendorName.Focus();
                    return;
                }

                if ((this.txtVendorTaxId.Text != "") && mm.VendorExists(this.txtVendorTaxId.Text, 1))
                {
                    lblMessageBox.Text = "Tax Id " + this.txtVendorTaxId.Text + " Already Exists in the Database.";
                    this.txtVendorTaxId.Focus();
                    return;
                }

                if ((chkVendorTaxIdRequired.Checked) && (txtVendorTaxId.Text.Length < 10 ))
                {
                    lblMessageBox.Text = "Tax Id is required if Tax Id Is Required checkbox is checked.";
                    lblMessageBox.Visible = true;
                    txtVendorTaxId.Focus();
                    return;
                }

                //insert vendor
                LoadVendor(1);
                vendor.Update();
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Vendor Added.');</script>");
            }
            else
            {
                if ((chkVendorTaxIdRequired.Checked) && (txtVendorTaxId.Text.Length < 10))
                {
                    lblMessageBox.Text = "Tax Id is required if Tax Id Is Required checkbox is checked.";
                    lblMessageBox.Visible = true;
                    txtVendorTaxId.Focus();
                    return;
                }

                LoadVendor(1);
                vendor.Update();
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Vendor Updated.');</script>");
            }

            if (Session["referer2"] != null)
            {
                string tmpreferrer = Session["referer2"].ToString();
                Session["referer2"] = null;
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + tmpreferrer + "';window.close();", true);
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);

        }

        private void LoadVendor(int mode = 0)
        {
            try
            {
                switch (mode)
                {
                    case 1:
                        vendor.VendorSubTypeId = Convert.ToInt32(this.cboVendorSubType.SelectedValue);

                        vendor.Name = this.txtVendorName.Text;
                        vendor.Address1 = this.txtVendorAddress1.Text;
                        vendor.Address2 = this.txtVendorAddress2.Text;
                        vendor.City = this.txtVendorCity.Text;
                        vendor.StateId = Convert.ToInt32(this.cboVendorState.SelectedValue);
                        vendor.Zipcode = this.txtVendorZipcode.Text;
                        vendor.Phone = this.txtVendorPhone.Text;
                        vendor.Fax = this.txtVendorFax.Text;
                        vendor.Email = this.txtVendorEmail.Text;
                        vendor.TaxIdRequired = (bool)this.chkVendorTaxIdRequired.Checked;
                        vendor.Requires1099 = (bool)this.chkVendor1099Required.Checked;
                        vendor.TaxId = this.txtVendorTaxId.Text;
                        break;
                    default:
                        if (vendor.Id == 0)
                        {
                            lblMessageBox.Visible = true;
                            lblMessageBox.Text = "Vendor is not able to be edited";
                            btnOK.Enabled = false;

                        }

                        //TODO
                        this.cboVendorType.SelectedValue = vendor.VendorSubType().VendorTypeId.ToString();
                        cboVendorType_SelectedIndexChanged(null, null);
                        this.cboVendorSubType.SelectedValue = vendor.VendorSubTypeId.ToString();

                        this.txtVendorName.Text = vendor.Name;
                        this.txtVendorAddress1.Text = vendor.Address1;
                        this.txtVendorAddress2.Text = vendor.Address2;
                        this.txtVendorCity.Text = vendor.City;
                        this.cboVendorState.SelectedValue = vendor.StateId.ToString();
                        this.txtVendorZipcode.Text = vendor.Zipcode;
                        this.txtVendorPhone.Text = vendor.Phone;
                        this.txtVendorFax.Text = vendor.Fax;
                        this.txtVendorEmail.Text = vendor.Email;
                        this.chkVendorTaxIdRequired.Checked = vendor.TaxIdRequired;
                        this.chkVendor1099Required.Checked = vendor.Requires1099;
                        this.txtVendorTaxId.Text = vendor.TaxId;
                        break;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private bool LoadTypes()
        {
            this.cboVendorType.DataSource = mm.GetVendorTypes();
            this.cboVendorType.DataTextField = "Description";
            this.cboVendorType.DataValueField = "vendor_type_id";
            this.cboVendorType.DataBind();

            this.cboVendorSubType.DataSource = mm.GetVendorSubTypes(this.cboVendorType.SelectedItem.ToString());
            this.cboVendorSubType.DataTextField = "description";
            this.cboVendorSubType.DataValueField = "vendor_sub_type_Id";
            this.cboVendorSubType.DataBind();
            return true;
        }

        private IEnumerable LoadTypes(List<Hashtable> list, int mode = 0)
        {
            List<VendorTypeGrid> myList = new List<VendorTypeGrid>();
            try
            {
                foreach (Hashtable vData in list)
                {
                    myList.Add(new VendorTypeGrid()
                    {
                        id = (int)vData["Vendor_Type_Id"],
                        imageIdx = (int)vData["Image_Index"],
                        description = (string)vData["Description"]
                    });
                }
                switch (mode)
                {
                    case 1:
                        myVendorSubTypeList = myList;
                        return myVendorSubTypeList;
                    default:
                        myVendorTypeList = myList;
                        return myVendorTypeList;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myList;
            }
        }

        class VendorTypeGrid
        {
            public int id { get; set; }
            public int imageIdx { get; set; }
            public string description { get; set; }
        }

        protected void cboVendorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboVendorSubType.DataSource = mm.GetVendorSubTypes(this.cboVendorType.SelectedItem.ToString());
            this.cboVendorSubType.DataTextField = "Description";
            this.cboVendorSubType.DataValueField = "vendor_sub_type_Id";
            this.cboVendorSubType.DataBind();
        }

        protected void txtVendorZipcode_TextChanged(object sender, EventArgs e)
        {
            ZipCodeLookUp lookup = new ZipCodeLookUp((sender as TextBox).Text);
            if (!string.IsNullOrWhiteSpace(lookup.zCity) && !string.IsNullOrWhiteSpace(lookup.zState))
            {
                txtVendorCity.Text = lookup.zCity;
                cboVendorState.SelectedValue = null;
                cboVendorState.Items.FindByText((string)lookup.zState).Selected = true; // lookup.zState;
                txtVendorCity.Focus();
            }
            else
                txtVendorZipcode.Focus();
        }

        protected void txtVendorTaxId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string formattedstr =  String.Format("{0:##-#######}", Convert.ToInt64(txtVendorTaxId.Text.Replace("-", "")));
                if (formattedstr.Length > 10)
                { 
                    lblvendorTaxIDMessage.Visible = true;
                    txtVendorTaxId.Focus();
                }
                else
                { 
                    lblvendorTaxIDMessage.Visible = false;
                    txtVendorTaxId.Text = formattedstr;
                }
            }
            catch (Exception ex)
            {
                lblvendorTaxIDMessage.Visible = true;
                txtVendorTaxId.Focus();
            }
        }

        protected void txtVendorPhone_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.txtVendorPhone.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.txtVendorPhone.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < txtVendorPhone.Text.Length; i++)
            {
                if (Char.IsDigit(txtVendorPhone.Text[i]))
                    p += txtVendorPhone.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    txtVendorPhone.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    txtVendorPhone.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

        }

        protected void txtVendorFax_TextChanged(object sender, EventArgs e)
        {
            string phNum = (sender as TextBox).Text;
            bool validPh = false;
            if (!string.IsNullOrWhiteSpace(phNum))
            {
                this.txtVendorFax.Text = mm.BuildPhoneNum(phNum, ref validPh);
                if (!validPh)
                {
                    this.txtVendorFax.Focus();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Phone number must be numeric.  Please enter a valid phone number.');</script>");
                    return;
                    //MessageBox.Show("Phone number must be numeric.  Please enter a valid phone number", "Invalid Phone Number", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }

            string p = string.Empty;
            Int64 val;
            for (int i = 0; i < txtVendorFax.Text.Length; i++)
            {
                if (Char.IsDigit(txtVendorFax.Text[i]))
                    p += txtVendorFax.Text[i];
            }

            if (p.Length > 0)
            {
                val = Int64.Parse(p);
                if (val.ToString().Length < 11)
                {
                    txtVendorFax.Text = String.Format("{0:(###) ###-####}", val);
                }
                else
                {
                    //tbPhoneOther.Text = String.Format("{0:(###) ###-#### x}{1}", val.ToString().Substring(0, 10), val.ToString().Substring(10));

                    const string formatPattern = @"(\d{3})(\d{3})(\d{4})(\d{2})";

                    txtVendorFax.Text = Regex.Replace(val.ToString(), formatPattern, "($1) $2-$3 x$4");
                }
            }

        }


    }
}
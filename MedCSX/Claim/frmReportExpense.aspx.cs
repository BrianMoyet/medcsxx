﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Claim
{
    public partial class frmReportExpense : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "Application/pdf";
            Response.BinaryWrite((byte[])Session["binaryDataExpense"]);
            Response.End();

            Session["binaryDataExpense"] = null;
        }
    }
}
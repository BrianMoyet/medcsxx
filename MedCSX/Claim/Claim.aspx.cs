﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCSX.App_Code;
using System.Text.RegularExpressions;
using System.Data;
using System.Drawing;
using System.Collections;

namespace MedCSX.Claim
{
    public partial class Claim1 : System.Web.UI.Page
    {
        public int claim_id;
        public string sub_claim_type_id;
        public string claim_status_id;
        public string company_location_id;
        MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
        MedCSX.App_Code.Claim oc = null;


        protected void Page_Load(object sender, EventArgs e)
        {
        
            string tmpDisplayclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");

            claim_id = Convert.ToInt32(Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", ""));
           

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetClaimTitleInfo(claim_id);
            sub_claim_type_id = claim.sub_claim_type_id.ToString();

            Page.Title = "Claim " + claim.display_claim_id + " - " + claim.sub_Claim_Type_Description + " - " + claim.claim_status_description;

            if (Session["user_id"] != null)
            { }
            else
            {
                Response.Redirect("../default.aspx");
            }
            if (!Page.IsPostBack)
            {
                oc = new MedCSX.App_Code.Claim();
                oc.GetClaimGeneralInfo(claim_id);

                Session["oc"] = oc;
                

                lblSuccess.Text = "";
                MedCSX.SiteClaimMaster yourMaster = (MedCSX.SiteClaimMaster)this.Master;
                //yourMaster.Page.Title = "Claim " + Request.QueryString["claim_id"].ToString();
                //string tmpclaim_id = Regex.Replace(Request.QueryString["claim_id"].ToString(), "[^0-9]", "");
                //int claim_id = Convert.ToInt32(tmpclaim_id);
                //Page.Title = "Claim " + Request.QueryString["claim_id"].ToString();

                PopulateControls(claim_id);
                GetClaimGeneralInfo(claim_id);
               
                GetPolicyCoverageAmounts(claim_id);
                getIRPolicyLink();

                //TabContainer1_ActiveTabChanged(TabContainer1, null);
                //TabContainer1_ActiveTabChanged(TabContainer1, null);
                TabContainer1.ActiveTabIndex = Convert.ToInt32(Request.QueryString["tabindex"]);
                CheckClaimLocks(claim_id);
               

            }
        }

        //private void SyncPolicy(bool associatingWithNewPolicy = false, string timer = "")
        //{
        //    // Sync Claim with Policy
        //    bool doneLoading = true;
        //    timer += "Enter SyncPolicy " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;
        //    string msg = "";
        //    //if (doneLoading == false)
        //    //{
        //    //    frmStatus f = new frmStatus("Please Wait");
        //    //    f.Show();
        //    //    f.Caption = "Searching SIU Database...";
        //    //    // Sync Policy Vehicles, Persons, and Coverages.  Display any problems to the user.
        //    //    msg = this.Claim.SyncPolicy(associatingWithNewPolicy, doneLoading, timer);
        //    //    f.Close();
        //    //}
        //    //else
        //    MedCSX.App_Code.Claim syncClaim = new MedCSX.App_Code.Claim();
        //    msg = syncClaim.SyncPolicy(associatingWithNewPolicy, doneLoading, timer);
            
        //    //timer += "Exit Claim.SyncPolicy " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

        //    //if (msg == null)
        //    //    msg = "";

        //    // Set the form title bar text
        //    //this.SetTitle();
        //    timer += "Set Title " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

        //    //if (msg.Trim() != "")
        //    //    // Sync is not ok - show sync problems to the user
        //    //    MessageBox.Show(msg, "Syncronizing Claim with Personal Lines", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

        //    //if (this.doneLoading == false)
        //    //    // Not done loading form
        //    //    return;

        //    // Check if claim is locked (set claimLocked variable)
        //    CheckClaimLocks(claim_id);
        //    timer += "Claim Lock Again " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;
        //}




        public void getIRPolicyLink()
        {
          
        }

        public string FormatPopupAlerts(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('alert.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('alert.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }

        public string FormatPopupPickAppraisal()
        {
            string tmpString = "";
            tmpString = "javascript:my_window2" + claim_id + "=window.open('PickAppraisalProperties.aspx?mode=u&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
           
            return tmpString;
        }

        public string FormatPopupSalvage(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('salvage.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('salvage.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupDemands(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                //tmpString = "javascript:my_window2" + claim_id + "=window.open('DemandsAdd.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
                  tmpString = "javascript:my_window2" + claim_id + "=window.open('DemandsAdd.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('DemandsAdd.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }

        public string FormatPopupDemandOffer(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('offer.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('offer.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupDemandDemand(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('DemandAdd.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('DemandAdd.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupLitigation(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('litigation.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('litigation.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupPossibleDuplicates(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window" + id + "=window.open('claim.aspx?tabindex=0&claim_id=" + id + "','my_window" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" + id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window" + id + "=window.open('claim.aspx?tabindex=0&claim_id=" + id + "','my_window" + id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, copyhistory=no, left=300, top=100');my_window" + id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupAppraisals(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('appraisals.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('appraisals.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupReserves(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('reserves.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" +claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('reserves.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" +claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupVehicle(string id, string mode)
        {
            string tmpString = ""; 
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('vehicles.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('vehicles.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupFileNote(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('fileNote.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('fileNote.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupReport(string reportLink)
        {
          
           string tmpString = "javascript:my_windowReport=window.open('" + reportLink + claim_id + "','my_windowReport','toolbar=no, location=yes, titlebar=yes, linkbar=no, directories=no, status=yes, menubar=yes, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2Report.focus()";
          
            return tmpString;
        }
        public string FormatPopupPD(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=" + mode + "&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            { 
                tmpString = "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=" + mode + "&claim_id=" + claim_id + "&property_damage_id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=300, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupWitness(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('witness.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('witness.aspx?mode=" + mode + "&claim_id=" + claim_id + "&witness=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupInjured(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('injured.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('injured.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupPhone(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('phone.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('phone.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }

        public string FormatPopupPerson(string id, string mode)
        {
            string tmpString = ""; //javascript:my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "=window.open('propertyDamage.aspx?mode=r&claim_id=<%=claim_id%>&property_damage_id=" + DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=400, copyhistory=no, left=200, top=200');my_window2PD" +  DataBinder.Eval(Container.DataItem,"property_damage_id").ToString() + ".focus()"
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('persondetails.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('persondetails.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupClaimant(string id, string mode)
        {
            string tmpString = ""; 
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('claimant.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                //tmpString = "javascript:my_window2" + claim_id + "=window.open('claimant.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
                tmpString = "javascript:my_window2" + claim_id + "=window.open('claimant.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }
        public string FormatPopupTasks(string id, string mode)
        {
            string tmpString = "";
            if (mode == "c")
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('task.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }
            else
            {
                tmpString = "javascript:my_window2" + claim_id + "=window.open('task.aspx?mode=" + mode + "&claim_id=" + claim_id + "&id=" + id + "','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";
            }

            // HyperLink5.NavigateUrl= "javascript:my_window2" + claim_id + "=window.open('propertyDamage.aspx?mode=c&claim_id=" + claim_id + "&property_damage_id=0','my_window2" + claim_id + "','toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');my_window2" + claim_id + ".focus()";

            return tmpString;
        }

        public string GetState(string id)
        {
            string tmpString = HelperFunctions.getStateByID(id);
          

            return tmpString;
        }
        private void CheckClaimLocks(int _claim_id)
        {
            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            DataTable LockedReasons = claim.CheckClaimLocks(_claim_id);
            if (claim.isLocked)
            {
                imgLocked.Visible = true;
                LockedPanel.Style["background-color"] = "#fa8072";
                gvLockReasons.DataSource = LockedReasons;
                gvLockReasons.DataBind();
                gvLockReasons.Visible = true;
            }
            else
            {
                LockedPanel.Style["background-color"] = "#f0f0f0";
            }
        }
        protected void GetClaimGeneralInfo(int _claim_id)
        {
           
            claim.GetClaimGeneralInfo(_claim_id);

            lblClaim.Text = claim.display_claim_id;
            lblPolicyNo.Text = claim.PolicyNumber + ", Ver " + claim.Version_No;
            lblInsuredName.Text = claim.NameInsured;
            lblInsuredVehicle.Text = claim.InsuredVehicle;
            lblLossDescription.Text = HelperFunctions.Truncate(claim.LossDescription, 80);
            lblDateOfLoss.Text = claim.DateOfLoss;
            ddlIVDriverAtFault.SelectedValue = claim.at_fault_type_id;

            txtLossDescription.Text = claim.LossDescription;
            txtDateOfLoss.Text = Convert.ToDateTime(claim.DateOfLoss).ToString("yyyy-MM-dd");
            txtTimeOfLoss.Text = Convert.ToDateTime(claim.DateOfLoss).ToString("HH:mm");
            txtDateReported.Text = Convert.ToDateTime(claim.date_reported).ToString("yyyy-MM-ddTHH:mm");
            ddlInsuredVehicle.SelectedValue = claim.insured_vehicle_id;
            ddlDriver.SelectedValue = claim.driver_person_id;
            ddlOwner.SelectedValue = claim.owner_person_id;
            txtIVDamage.Text = claim.vehicle_damage;
            txtWhereIVSeen.Text = claim.where_seen;
            txtLossLocation.Text = claim.loss_location;
            txtLossCity.Text = claim.loss_city;
            ddlLossState.SelectedValue = claim.loss_state_id;
            ddlReportedByType.SelectedValue = claim.reported_by_type_id;
            ddlAccidentType.SelectedValue = claim.accident_type_id;
            txtAuthContacted.Text = claim.authority_contacted;
            txtReportNum.Text = claim.police_report_number;
            txtComments.Text = claim.comments;
            txtReportedBy.Text = claim.reported_by;

            if (claim.permission_to_use == "True")
                cbPermToUse.Checked = true;
            if (claim.hasSR22 == "True")
                cbSR22.Checked = true;
            if (claim.is_comp == "True")
                cbIVComp.Checked = true;
            if (claim.is_collision == "True")
                cbIVCollision.Checked = true;
            if (claim.has_coverage_issue == "True")
                cbCoverageIssue.Checked = true;
            if (claim.has_disputed_liability == "True")
                cbDisputedLiability.Checked = true;
            if (claim.large_loss == "True")
                cbLargeLoss.Checked = true;
            if (claim.is_foxpro_conversion == "True")
            {
                cbFoxPro.Checked = true;
                lblFoxPro.Visible = true;
                lblFoxPro.Text = claim.foxpro_claim_no;
            }
            
            
            agentInfo.InnerText = HelperFunctions.GetAgentForClaim(claim_id.ToString());
            adjusterInfo.InnerText = HelperFunctions.GetAdjusterInfo(claim.adjuster_id);

            PopulateInsuredPhones();
            PopulateOwnerPhones();
            PopulateDriverPhones();

            claim.InsertRecentClaim(Convert.ToInt32(Session["user_id"]), _claim_id);

            //claim.SyncPolicy(false);
        }

        



        protected void PopulateControls(int _claim_id)
        {
            DataTable dt = new DataTable();

            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            dt = myData.PopulateDDLClaimPersons(_claim_id);
            
            ddlInsuredPerson.DataSource = dt;
            ddlInsuredPerson.DataValueField = dt.Columns[0].ColumnName;
            ddlInsuredPerson.DataTextField = dt.Columns[1].ColumnName;
            ddlInsuredPerson.DataBind();

            ddlOwner.DataSource = dt;
            ddlOwner.DataValueField = dt.Columns[0].ColumnName;
            ddlOwner.DataTextField = dt.Columns[1].ColumnName;
            ddlOwner.DataBind();

            ddlDriver.DataSource = dt;
            ddlDriver.DataValueField = dt.Columns[0].ColumnName;
            ddlDriver.DataTextField = dt.Columns[1].ColumnName;
            ddlDriver.DataBind();

            dt = myData.PopulateDDLStates();
            ddlLossState.DataSource = dt;
            ddlLossState.DataValueField = dt.Columns[0].ColumnName;
            ddlLossState.DataTextField = dt.Columns[1].ColumnName;
            ddlLossState.DataBind();

            dt = myData.PopulateDDLAccidentTypes(1);
            ddlAccidentType.DataSource = dt;
            ddlAccidentType.DataValueField = dt.Columns[0].ColumnName;
            ddlAccidentType.DataTextField = dt.Columns[1].ColumnName;
            ddlAccidentType.DataBind();

            dt = myData.PopulateDDLReportedByTypes(1);
            ddlReportedByType.DataSource = dt;
            ddlReportedByType.DataValueField = dt.Columns[0].ColumnName;
            ddlReportedByType.DataTextField = dt.Columns[1].ColumnName;
            ddlReportedByType.DataBind();

            dt = myData.PopulateDDLAtFaultTypes();
            ddlIVDriverAtFault.DataSource = dt;
            ddlIVDriverAtFault.DataValueField = dt.Columns[0].ColumnName;
            ddlIVDriverAtFault.DataTextField = dt.Columns[1].ColumnName;
            ddlIVDriverAtFault.DataBind();

            Vehicle myVehicle = new Vehicle();
            dt = myVehicle.GetVehicles_By_ClaimId(_claim_id);

            ddlInsuredVehicle.DataSource = dt;
            ddlInsuredVehicle.DataValueField = dt.Columns[0].ColumnName;
            ddlInsuredVehicle.DataTextField = dt.Columns[38].ColumnName;
            ddlInsuredVehicle.DataBind();

            DataTable dtState = new DataTable();
            MedCSX.App_Code.Claim myD = new MedCSX.App_Code.Claim();
            dtState = myD.PopulateDDLStates();
            ddlLossState.DataSource = dtState;
            ddlLossState.DataValueField = dtState.Columns[0].ColumnName;
            ddlLossState.DataTextField = dtState.Columns[1].ColumnName;
            ddlLossState.DataBind();

            //file notes
            dt = HelperFunctions.getTypeTableRows("File_Note_Type", "Description", "1=1");

            DataRow newRow = dt.NewRow();
            newRow["Id"] = 0;
            newRow["Description"] = "All";
            dt.Rows.InsertAt(newRow, 0);

            ddlShowFileNotesByType.DataSource = dt;
            ddlShowFileNotesByType.DataValueField = dt.Columns[0].ColumnName;
            ddlShowFileNotesByType.DataTextField = dt.Columns[1].ColumnName;
            ddlShowFileNotesByType.DataBind();


            Claimants myClaimant = new Claimants();
            dt = myClaimant.GetClaimantsAndIDsForClaim(claim_id);
            DataRow newRowC = dt.NewRow();
            newRowC["claimant_id"] = 0;
            newRowC["Person"] = "All";
            dt.Rows.InsertAt(newRowC, 0);

            ddlShowFileNotesForClaimant.DataSource = dt;
            ddlShowFileNotesForClaimant.DataValueField = dt.Columns[0].ColumnName;
            ddlShowFileNotesForClaimant.DataTextField = dt.Columns[2].ColumnName;
            ddlShowFileNotesForClaimant.DataBind();

            Reserve myReserves = new Reserve();
            dt = myReserves.usp_Get_Reserve_Names_For_Claimant(Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue));
            DataRow newRowR = dt.NewRow();
            newRowR["reserve_name"] = "All";
            dt.Rows.InsertAt(newRowR, 0);
            ddlShowFileNotesForReserve.DataValueField = dt.Columns[0].ColumnName;
            ddlShowFileNotesForReserve.DataTextField = dt.Columns[0].ColumnName;
            ddlShowFileNotesForReserve.DataBind();

            hprNormalReport.NavigateUrl = "http://reports.medjames.com/reportserver?%2fClaims%2fFile+Activity+Report&rs:Command=Render&claim=" + claim_id;
            hprSummaryReport.NavigateUrl = "http://reports.medjames.com/reportserver?%2fClaims%2fDetailed+File+Activity+Report&rs:Command=Render&claim=" + claim_id;
            hprDetailedReport.NavigateUrl = "http://reports.medjames.com/reportserver?%2fClaims%2fSummary+File+Activity+Report&rs:Command=Render&claim=" + claim_id;
            hprUserEnteredNotes.NavigateUrl = "http://reports.medjames.com/reportserver?%2fClaims%2fUser+Entered+File+Notes&rs:Command=Render&claim=" + claim_id;
        }

        protected void ddlShowFileNotesByType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            FileNotes myData = new FileNotes();
            dt = myData.GetFileNotes(claim_id,Convert.ToInt32(ddlShowFileNotesByType.SelectedValue), 0, 0, 0);

            dt.DefaultView.Sort = "created_date Desc";
            gvFileNotes.DataSource = dt;
            gvFileNotes.DataBind();
            gvFileNotes.Visible = true;
            Session["FileNotes" + claim_id + "Table"] = dt;
        }

        protected void ddlShowFileNotesForClaimant_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            FileNotes myData = new FileNotes();
            dt = myData.GetFileNotes(claim_id, 0, Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue), 0, 0);

            dt.DefaultView.Sort = "created_date Desc";
            gvFileNotes.DataSource = dt;
            gvFileNotes.DataBind();
            gvFileNotes.Visible = true;



            Session["FileNotes" + claim_id + "Table"] = dt;

            Reserve myReserves = new Reserve();
            dt = myReserves.usp_Get_Reserve_Names_For_Claimant(Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue));
            DataRow newRowR = dt.NewRow();
            newRowR["reserve_name"] = "All";
          
            dt.Rows.InsertAt(newRowR, 0);

            ddlShowFileNotesForReserve.DataSource = dt;
            ddlShowFileNotesForReserve.DataBind();
            ddlShowFileNotesForReserve.Visible = true;
        }

        protected void ddlShowFileNotesForReserve_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlShowFileNotesForReserve.SelectedValue == "All")
            {
                DataTable dt = new DataTable();

                FileNotes myData = new FileNotes();
                dt = myData.GetFileNotes(claim_id, 0, 0, Convert.ToInt32(ddlShowFileNotesForClaimant.SelectedValue), 0);

                dt.DefaultView.Sort = "created_date Desc";
                gvFileNotes.DataSource = dt;
                gvFileNotes.DataBind();
                gvFileNotes.Visible = true;



                Session["FileNotes" + claim_id + "Table"] = dt;
            }
            else
            {
                DataTable selectedTable = Session["FileNotes" + claim_id + "Table"] as DataTable;

                DataTable dt = selectedTable.AsEnumerable()
                                .Where(r => r.Field<string>("reserve").Contains(ddlShowFileNotesForReserve.SelectedValue))
                                .CopyToDataTable();
                gvFileNotes.DataSource = dt;
                gvFileNotes.DataBind();

                Session["FileNotes" + claim_id + "Table"] = dt;
            }

           



           
        }

        protected void GetPolicyCoverageAmounts(int _claim_id)
        {

            MedCSX.App_Code.Claim claim = new MedCSX.App_Code.Claim();
            claim.GetPolicyCoverageAmounts(_claim_id);
            if (claim.ClaimFound)
            {

                lblBI.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.AutoBodilyInjuryPerPerson))) + "/" + HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.AutoBodilyInjury)));
                lblPropertyDamage.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.PropertyDamage)));
                lblMedPay.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.MedicalPayments)));
                lblUM.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.UM_UIM)));

                lblUIM.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.UninsuredMotorist)));
                lblCompDed.Text = "";
                lblCollDed.Text = "";
                lblPIPEssServices.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.PIPEssentialServices)));

                lblPIPMed.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.PIPMedical)));
                lblPIPRehab.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.PIPMedical)));
                lblPIPWages.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.PIPWages)));
                lblPIPFuneral.Text = HelperFunctions.FormatKNumber(Math.Truncate(Convert.ToDecimal(claim.PIPFuneral)));
            }
            else
            {

            }
        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {


            try
            {
                if (TabContainer1.ActiveTabIndex == 0)  //General
                {

                }

                if (TabContainer1.ActiveTabIndex == 1)  //property Damage
                {
                    DataTable dt = new DataTable();

                    PropertyDamage mymyData = new PropertyDamage();
                    dt = mymyData.GetPropertyDamagesForClaim(claim_id);

                    gvPropertyDamage.DataSource = dt;
                    gvPropertyDamage.DataBind();
                    gvPropertyDamage.Visible = true;

                    Session["PropertyDamage" + claim_id + "Table"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 2) //Witness Passenger
                {
                    DataTable dt = new DataTable();

                    Witness myData = new Witness();
                    dt = myData.GetWitnessForClaim(claim_id);

                    gvWitness.DataSource = dt;
                    gvWitness.DataBind();
                    gvWitness.Visible = true;

                    Session["Witness" + claim_id + "Table"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 3) //Injured
                {
                    DataTable dt = new DataTable();

                    Injured myData = new Injured();
                    dt = myData.GetInjuredForClaim(claim_id);

                    gvInjured.DataSource = dt;
                    gvInjured.DataBind();
                    gvInjured.Visible = true;

                    Session["Injured" + claim_id + "Table"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 4) //File Activity
                {
                    DataTable dt = new DataTable();

                    FileNotes myData = new FileNotes();
                    dt = myData.GetFileNotes(claim_id, 0, 0, 0, 0);

                    dt.DefaultView.Sort = "created_date Desc";
                    gvFileNotes.DataSource = dt;
                    gvFileNotes.DataBind();
                    gvFileNotes.Visible = true;

                   

                    Session["FileNotes" + claim_id + "Table"] = dt;
                }
                if (TabContainer1.ActiveTabIndex == 5) //claimants
                {
                    DataTable dt = new DataTable();

                    Claimants myData = new Claimants();
                    dt = myData.GetClaimantsForClaim(claim_id);

                    gvClaimant.DataSource = dt;
                    gvClaimant.DataBind();
                    gvClaimant.Visible = true;

                    Session["Claimant" + claim_id + "Table"] = dt;
                }
                if (TabContainer1.ActiveTabIndex == 6) //Vehicles
                {
                    DataTable dt = new DataTable();

                    Vehicle myData = new Vehicle();
                    dt = myData.GetVehicles_By_ClaimId(claim_id);

                    gvVehicles.DataSource = dt;
                    gvVehicles.DataBind();
                    gvVehicles.Visible = true;

                    Session["Vehicles" + claim_id + "Table"] = dt;
                }
                if (TabContainer1.ActiveTabIndex == 7) //reserves
                {
                    DataTable dtReserve = new DataTable();

                    Reserve myReserve = new Reserve();
                    dtReserve = myReserve.GetReserves_By_ClaimId(claim_id);

                    gvReserves.DataSource = dtReserve;
                    gvReserves.DataBind();
                    gvReserves.Visible = true;

                    Session["Reserves" + claim_id + "Table"] = dtReserve;
                }
                if (TabContainer1.ActiveTabIndex == 8)  //tasks
                {
                    DataTable dt = new DataTable();

                    Task myData = new Task();
                    dt = myData.GetTasksForClaim(claim_id);

                    gvTasks.DataSource = dt;
                    gvTasks.DataBind();
                    gvTasks.Visible = true;

                    Session["Tasks" + claim_id + "Table"] = dt;
                }
                if (TabContainer1.ActiveTabIndex == 9)//Policy Data
                {
                    DataTable dt = new DataTable();

                    PolicyData myData = new PolicyData();
                    dt = myData.GetPolicyDataForClaim(claim_id);
                    gvPolicyData.DataSource = dt;
                    DataBind();


                }
                if (TabContainer1.ActiveTabIndex == 10)  //Appraisals
                {
                    DataTable dt = new DataTable();

                    Appraisals myData = new Appraisals();
                    dt = myData.GetAppraisalsForClaim(claim_id);
                    gvAppraisals.DataSource = dt;
                    DataBind();

                    Session["Appraisals" + claim_id + "Table"] = dt;

                }
                if (TabContainer1.ActiveTabIndex == 11)  //Documents
                {
                    DataTable dt = new DataTable();

                    Documents myData = new Documents();
                    dt = myData.GetDocuments_By_ClaimId(claim_id);
                    gvDocuments.DataSource = dt;
                    DataBind();

                    Session["Documents" + claim_id + "Table"] = dt;

                }
                if (TabContainer1.ActiveTabIndex == 12)  //Phone numbers
                {
                    DataTable dt = new DataTable();

                    MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
                    dt = myData.GetPhoneNumbersForClaim(claim_id);

                    gvPhoneNumbers.DataSource = dt;
                    gvPhoneNumbers.DataBind();
                    gvPhoneNumbers.Visible = true;

                    Session["PhoneNumber" + claim_id + "Table"] = dt;


                }
                if (TabContainer1.ActiveTabIndex == 13)  //Duplicates
                {
                    DataTable dt = new DataTable();

                    DuplicateClaims myData = new DuplicateClaims();
                    dt = myData.GetPossibleDuplicatesForClaim(claim_id);
                    gvPossibleDuplicates.DataSource = dt;
                    gvPossibleDuplicates.DataBind();

                    Session["PossibleDuplicates" + claim_id + "Table"] = dt;
                }
                if (TabContainer1.ActiveTabIndex == 14)  //Demands
                {
                    DataTable dt = new DataTable();

                    Demands myData = new Demands();
                    dt = myData.GetDemandsByClaimID(claim_id);

                    gvDemands.DataSource = dt;
                    gvDemands.DataBind();
                    gvDemands.Visible = true;

                    Session["Demands" + claim_id + "Table"] = dt;

                }
                if (TabContainer1.ActiveTabIndex == 15)  //Litigation
                {
                    DataTable dt = new DataTable();

                    Litigation myData = new Litigation();
                    dt = myData.GetLitigationForClaim(claim_id);

                    gvLitigation.DataSource = dt;
                    gvLitigation.DataBind();
                    gvLitigation.Visible = true;

                    Session["Litigation" + claim_id + "Table"] = dt;

                }
                if (TabContainer1.ActiveTabIndex == 16)  //Salvage
                {
                    DataTable dt = new DataTable();

                    Salvage myData = new Salvage();
                    dt = myData.GetSalvageForClaim(claim_id);

                    gvSalvage.DataSource = dt;
                    gvSalvage.DataBind();
                    gvSalvage.Visible = true;

                    Session["Salvage" + claim_id + "Table"] = dt;
                }

                if (TabContainer1.ActiveTabIndex == 17)  //Alerts
                {
                    DataTable dt = new DataTable();

                    Alert myData = new Alert();
                    dt = myData.GetAlertsForClaim(claim_id);

                    gvAlerts.DataSource = dt;
                    gvAlerts.DataBind();
                    gvAlerts.Visible = true;

                    Session["Alerts" + claim_id + "Table"] = dt;
                }
                //if (TabContainer1.ActiveTabIndex == 18) //Alerts
                //{
                  
                //}
            }

            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

      

        protected void rblDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dtAlert = new DataTable();
            //Alert myAlert = new Alert();


            //if (rblAlertType.SelectedValue == "1")
            //{
            //    dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 1);  // to me
            //}
            //else
            //{
            //    dtAlert = myAlert.GetAlerts(Convert.ToInt32(ddlShowAlertsFor.SelectedValue), 2); //from me
            //}

            //gvAlerts.DataSource = dtAlert;
            //gvAlerts.DataBind();
            //gvAlerts.Visible = true;

            //Session["AlertTable"] = dtAlert;
        }



        #region paging
        protected void gvPossibleDuplicates_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            DuplicateClaims myData = new DuplicateClaims();
            dt = myData.GetPossibleDuplicatesForClaim(claim_id);
            gvPossibleDuplicates.DataSource = dt;
            gvPossibleDuplicates.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvLitigation_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Litigation myData = new Litigation();
            dt = myData.GetLitigationForClaim(claim_id);
            gvLitigation.DataSource = dt;
            gvLitigation.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvSalvage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Salvage myData = new Salvage();
            dt = myData.GetSalvageForClaim(claim_id);
            gvSalvage.DataSource = dt;
            gvSalvage.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Documents myData = new Documents();
            dt = myData.GetDocuments_By_ClaimId(claim_id);
            gvDocuments.DataSource = dt;
            gvDocuments.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvAppraisals_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Appraisals myData = new Appraisals();
            dt = myData.GetAppraisalsForClaim(claim_id);
            gvAppraisals.DataSource = dt;
            gvAppraisals.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvPhoneNumbers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            MedCSX.App_Code.Claim myData = new MedCSX.App_Code.Claim();
            dt = myData.GetPhoneNumbersForClaim(claim_id);
            gvPhoneNumbers.DataSource = dt;

            gvPhoneNumbers.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvTasks_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Task myData = new Task();
            dt = myData.GetTasksForClaim(claim_id);
            gvTasks.DataSource = dt;

            gvTasks.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvClaimant_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Claimants myData = new Claimants();
            dt = myData.GetClaimantsForClaim(claim_id);
            gvClaimant.DataSource = dt;

            gvClaimant.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvInjured_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Injured myData = new Injured();
            dt = myData.GetInjuredForClaim(claim_id);
            gvInjured.DataSource = dt;

            gvInjured.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvWitness_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Witness myData = new Witness();
            dt = myData.GetWitnessForClaim(claim_id);
            gvWitness.DataSource = dt;

            gvWitness.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvDemands_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Demands myData = new Demands();
            dt = myData.GetDemandsByClaimID(claim_id);
            gvDemands.DataSource = dt;

            gvAlerts.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvAlerts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dtAlert = new DataTable();

            Alert myAlert = new Alert();
            dtAlert = myAlert.GetAlertsForClaim(claim_id);
            gvAlerts.DataSource = dtAlert;

            gvAlerts.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvPropertyDamage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            PropertyDamage myData = new PropertyDamage();
            dt = myData.GetPropertyDamagesForClaim(claim_id);
            gvPropertyDamage.DataSource = dt;

            gvPropertyDamage.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvReserves_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dtReserve = new DataTable();

            Reserve myReserve = new Reserve();
            dtReserve = myReserve.GetReserves_By_ClaimId(claim_id);
            gvReserves.DataSource = dtReserve;

            gvReserves.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvVehicles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            Vehicle myData = new Vehicle();
            dt = myData.GetVehicles_By_ClaimId(claim_id);
            gvVehicles.DataSource = dt;

            gvVehicles.PageIndex = e.NewPageIndex;
            DataBind();
        }
        protected void gvFileNotes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            DataTable dt = new DataTable();

            FileNotes myData = new FileNotes();
            dt = myData.GetFileNotes(claim_id,0,0,0,0);
            gvFileNotes.DataSource = dt;

            gvFileNotes.PageIndex = e.NewPageIndex;
            DataBind();
        }

        #endregion

        #region sorting
        protected void gvFileNotes_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["FileNotes" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvFileNotes.DataSource = Session["FileNotes" + claim_id + "Table"];
                gvFileNotes.DataBind();

                Session["FileNotes" + claim_id + "Table"] = dt;
            }

        }
        protected void gvPossibleDuplicates_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["PossibleDuplicates" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvPossibleDuplicates.DataSource = Session["PossibleDuplicates" + claim_id + "Table"];
                gvPossibleDuplicates.DataBind();

                Session["PossibleDuplicates" + claim_id + "Table"] = dt;
            }

        }
        protected void gvLitigation_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Litigation" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvLitigation.DataSource = Session["Litigation" + claim_id + "Table"];
                gvLitigation.DataBind();

                Session["Litigation" + claim_id + "Table"] = dt;
            }

        }
        protected void gvSalvage_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Salvage" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvSalvage.DataSource = Session["Salvage" + claim_id + "Table"];
                gvSalvage.DataBind();

                Session["Salvage" + claim_id + "Table"] = dt;
            }

        }
        protected void gvDocuments_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Documents" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvDocuments.DataSource = Session["Documents" + claim_id + "Table"];
                gvDocuments.DataBind();

                Session["Documents" + claim_id + "Table"] = dt;
            }

        }
        protected void gvAppraisals_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Appraisals" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvAppraisals.DataSource = Session["Appraisals" + claim_id + "Table"];
                gvAppraisals.DataBind();

                Session["Appraisals" + claim_id + "Table"] = dt;
            }

        }
        protected void gvPhoneNumbers_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["PhoneNumber" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvPhoneNumbers.DataSource = Session["PhoneNumber" + claim_id + "Table"];
                gvPhoneNumbers.DataBind();

                Session["PhoneNumber" + claim_id + "Table"] = dt;
            }

        }
        protected void gvTasks_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Tasks" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvTasks.DataSource = Session["Tasks" + claim_id + "Table"];
                gvTasks.DataBind();

                Session["Tasks" + claim_id + "Table"] = dt;
            }

        }
        protected void gvClaimant_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Claimant" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvClaimant.DataSource = Session["Claimant" + claim_id + "Table"];
                gvClaimant.DataBind();

                Session["Claimant" + claim_id + "Table"] = dt;
            }

        }
        protected void gvInjured_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Injured" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvInjured.DataSource = Session["Injured" + claim_id + "Table"];
                gvInjured.DataBind();

                Session["Injured" + claim_id + "Table"] = dt;
            }

        }
        protected void gvWitness_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Witness" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvWitness.DataSource = Session["Witness" + claim_id + "Table"];
                gvWitness.DataBind();

                Session["Witness" + claim_id + "Table"] = dt;
            }

        }
        protected void gvDemands_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Demands" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvDemands.DataSource = Session["Demands" + claim_id + "Table"];
                gvDemands.DataBind();

                Session["Demands" + claim_id + "Table"] = dt;
            }

        }
        protected void gvPropertyDamage_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["PropertyDamage" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvPropertyDamage.DataSource = Session["PropertyDamage" + claim_id + "Table"];
                gvPropertyDamage.DataBind();

                Session["PropertyDamage" + claim_id + "Table"] = dt;
            }

        }
        protected void gvReserves_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Reserves" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvReserves.DataSource = Session["Reserves" + claim_id + "Table"];
                gvReserves.DataBind();

                Session["Reserves" + claim_id + "Table"] = dt;
            }

        }
        protected void gvVehicles_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Vehicles" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvVehicles.DataSource = Session["Vehicles" + claim_id + "Table"];
                gvVehicles.DataBind();

                Session["Vehicles" + claim_id + "Table"] = dt;
            }

        }
        protected void gvAlerts_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["Alerts" + claim_id + "Table"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                gvAlerts.DataSource = Session["Alerts" + claim_id + "Table"];
                gvAlerts.DataBind();

                Session["Alerts" + claim_id + "Table"] = dt;
            }

        }
        #endregion

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void btnPDDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnWitnessDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnInjuredDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnPhoneDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnClaimantDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnTasksDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnFileNoteDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnReservesDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnAppraisalsDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnPossibleDuplicatesDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnLitigationDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnAlertsDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnDemandsDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnDemandOfferDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnSalvageDelete_Click(object sender, EventArgs e)
        {

        }

        protected void chkDiariesOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDiariesOnly.Checked)
            {
                DataTable selectedTable = Session["FileNotes" + claim_id + "Table"] as DataTable;

                DataTable dt = selectedTable.AsEnumerable()
                                .Where(r => r.Field<string>("diary_type").Length > 0)
                                .CopyToDataTable();
                gvFileNotes.DataSource = dt;
                gvFileNotes.DataBind();

                Session["FileNotes" + claim_id + "Table"] = dt;
            }
            else
            {
                DataTable dt = new DataTable();

                FileNotes myData = new FileNotes();
                dt = myData.GetFileNotes(claim_id, 0, 0, 0, 0);

                dt.DefaultView.Sort = "created_date Desc";
                gvFileNotes.DataSource = dt;
                gvFileNotes.DataBind();
                gvFileNotes.Visible = true;

                Session["FileNotes" + claim_id + "Table"] = dt;
            }

        }

        protected void btnSearchFileNotes_Click(object sender, EventArgs e)
        {
            if (txtSearchFileNote.Text.Length > 0)
            {
                DataTable selectedTable = Session["FileNotes" + claim_id + "Table"] as DataTable;

                DataTable dt = selectedTable.AsEnumerable()
                                .Where(r => r.Field<string>("file_note_text").Contains(txtSearchFileNote.Text))
                                .CopyToDataTable();
                gvFileNotes.DataSource = dt;
                gvFileNotes.DataBind();

                Session["FileNotes" + claim_id + "Table"] = dt;
            }
            else
            {
                DataTable dt = new DataTable();

                FileNotes myData = new FileNotes();
                dt = myData.GetFileNotes(claim_id, 0, 0, 0, 0);

                dt.DefaultView.Sort = "created_date Desc";
                gvFileNotes.DataSource = dt;
                gvFileNotes.DataBind();
                gvFileNotes.Visible = true;

                Session["FileNotes" + claim_id + "Table"] = dt;
            }
        }

        protected void gvDemands_SelectedIndexChanged(object sender, EventArgs e)
        {
            int myVendorId = Convert.ToInt32(gvDemands.SelectedRow.Cells[5].Text);
            int myReserveId = Convert.ToInt32(gvDemands.SelectedRow.Cells[6].Text);

            populateDemandHistory(myVendorId, myReserveId);
        }

        protected void populateDemandHistory(int vendorId, int reserveid)
        {
            DataTable dt = new DataTable();
            Demands myData = new Demands();
            dt = myData.GetDemandHistoryByClaimID(vendorId, reserveid);

            gvDemandOffer.DataSource = dt;
            gvDemandOffer.DataBind();
            gvDemandOffer.Visible = true;
        }

       

        protected void imgIRPolicy_Click(object sender, ImageClickEventArgs e)
        {
            if (lblPolicyNo.Text.Length > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('This Claim isn't Associated with a Policy Yet');", true);
            }
            else
            {
                if (sub_claim_type_id == ModGeneratedEnums.ClaimType.Auto.ToString())
                {
                    if (company_location_id == ModGeneratedEnums.Company.Trisura_Commercial_Liability.ToString() || company_location_id == ModGeneratedEnums.Company.Trisura_Commercial_Property.ToString())
                    {
                        try
                        {
                            //if (getIRDocumentsByClaim(Me.Claim.PolicyNo, ModMain.PolicyDrawer_MJIBrokerageCompanion))
                            //{

                            //}
                        }
                        catch
                        {

                        }
                    }
                }
                else
                {

                }
            }
        }

        protected void imgAssignAppraiser_Click(object sender, ImageClickEventArgs e)
        {
            string url = "PickAppraisalProperties.aspx?mode=u&claim_id=" + claim_id + "&id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void imgAssignReserves_Click(object sender, ImageClickEventArgs e)
        {
            string url = "AssignReserves.aspx?mode=u&claim_id=" + claim_id + "&id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void imgPoliceReport_Click(object sender, ImageClickEventArgs e)
        {
            bool reportExists = HelperFunctions.PoliceReportOrdered(claim_id);
          

            if (reportExists)
            {

                ScriptManager.RegisterStartupScript(this, this.GetType(), "myconfirm", "Warning - ChoicePoint reports have already been ordered for this claim.  Are you sure you want to proceed?", true);
            }
            else
            {
                string url = "RequestPoliceReport.aspx?mode=u&claim_id=" + claim_id + "&id=0";
                string s2 = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            
        }

        public void OnConfirm(object sender, EventArgs e)
        {
            bool reportExists = HelperFunctions.PoliceReportOrdered(claim_id);

            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                string url = "RequestPoliceReport.aspx?mode=u&claim_id=" + claim_id + "&id=0";
                string s2 = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s2, true);
            }
            else
            {
                
            }
        }

        protected void btnOwnerAdd_Click(object sender, EventArgs e)
        {
            string url = "personDetails.aspx?mode=c&person_type_id=7&claim_id=" + claim_id + "&id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnDriverAdd_Click(object sender, EventArgs e)
        {
            string url = "personDetails.aspx?mode=c&person_type_id=8&claim_id=" + claim_id + "&id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveGeneralInfo();
        }

        public void SaveGeneralInfo()
        {
            MedCSX.App_Code.Claim updateClaim = new MedCSX.App_Code.Claim();
            updateClaim.ClaimID = claim_id.ToString();
             DateTime newDateTime = Convert.ToDateTime(txtDateOfLoss.Text).Add(TimeSpan.Parse(txtTimeOfLoss.Text));
            updateClaim.date_reported = txtDateReported.Text;
            updateClaim.DateOfLoss = newDateTime.ToString();
            updateClaim.at_fault_type_id = ddlIVDriverAtFault.SelectedValue;
            updateClaim.insured_person_id = ddlInsuredPerson.SelectedValue;
            updateClaim.insured_vehicle_id = ddlInsuredVehicle.SelectedValue;
            updateClaim.owner_person_id = ddlOwner.SelectedValue;
            updateClaim.driver_person_id = ddlDriver.SelectedValue;
            updateClaim.vehicle_damage = txtIVDamage.Text;
            updateClaim.where_seen = txtWhereIVSeen.Text;
            updateClaim.loss_location = txtLossLocation.Text;
            updateClaim.loss_city = txtLossCity.Text;
            updateClaim.loss_state_id = ddlLossState.SelectedValue;
            updateClaim.accident_type_id = ddlAccidentType.SelectedValue;
            updateClaim.LossDescription = txtLossDescription.Text;
            updateClaim.authority_contacted = txtAuthContacted.Text;
            updateClaim.police_report_number = txtReportNum.Text;
            updateClaim.reported_by = txtReportedBy.Text;
            updateClaim.reported_by_type_id = ddlReportedByType.SelectedValue;
            updateClaim.comments = txtComments.Text;
            updateClaim.permission_to_use = Convert.ToInt32(cbPermToUse.Checked).ToString();
            updateClaim.is_comp = Convert.ToInt32(cbIVComp.Checked).ToString();
            updateClaim.has_coverage_issue = Convert.ToInt32(cbCoverageIssue.Checked).ToString();
            updateClaim.has_disputed_liability = Convert.ToInt32(cbDisputedLiability.Checked).ToString();
            updateClaim.large_loss = Convert.ToInt32(cbLargeLoss.Checked).ToString();
            updateClaim.is_foxpro_conversion = Convert.ToInt32(cbFoxPro.Checked).ToString();
            updateClaim.is_collision = Convert.ToInt32(cbIVCollision.Checked).ToString();
            updateClaim.hasSR22 = Convert.ToInt32(cbSR22.Checked).ToString();
            updateClaim.Modified_By = Session["user_id"].ToString();
            updateClaim.Modified_Date = HelperFunctions.GetCurrentDate();
            updateClaim.claim_status_id = claim.claim_status_id;

            int success = updateClaim.Update();
            //if (success == 1)
            //    lblSuccess.Text = "Changes Saved";
            //else
            //{ 
            //    lblSuccess.ForeColor = System.Drawing.Color.Red;
            //    lblSuccess.Text = "There was an error updating the claim";
            //}

            Hashtable parms = new Hashtable();
            parms.Add("Claim_ID", claim_id);
            oc = (MedCSX.App_Code.Claim)Session["oc"];

            updateClaim.claim_status_id = oc.claim_status_id;
            int successful = ModMain.ClaimUpdatedEventHandler((int)ModGeneratedEnums.EventType.Claim_Updated, parms, oc, updateClaim);

            lblSuccess.Text = "Changes Saved";
        }

        protected void btnOwnerUpdate_Click(object sender, EventArgs e)
        {
            string url = "personDetails.aspx?mode=u&person_type_id=7&claim_id=" + claim_id + "&id=" + this.ddlOwner.SelectedValue;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnDriverUpdate_Click(object sender, EventArgs e)
        {
            string url = "personDetails.aspx?mode=u&person_type_id=8&claim_id=" + claim_id + "&id=" + this.ddlDriver.SelectedValue;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnVehicleUpdate_Click(object sender, EventArgs e)
        {
            string url = "vehicles.aspx?mode=u&claim_id=" + claim_id + "&id=" + this.ddlInsuredVehicle.SelectedValue;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void btnVehicleAdd_Click(object sender, EventArgs e)
        {
            string url = "vehicles.aspx?mode=c&claim_id=" + claim_id + "&id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void ddlInsuredPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateInsuredPhones();
            
        }

        protected void ddlOwnerPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateOwnerPhones();
         
        }

        protected void ddlDriverPhoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateDriverPhones();
          
        }

        protected void ddlInsuredPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateInsuredPhones();
        }

        public void PopulateInsuredPhones()
        {
            Person myPerson = new Person();
            myPerson.GetPersonByPersonID(Convert.ToInt32(ddlInsuredPerson.SelectedValue));

            if (!Page.IsPostBack)
            {
                DataTable dtInsuredPhone = new DataTable();
                dtInsuredPhone.Columns.Add("Id");
                dtInsuredPhone.Columns.Add("Description");
                DataRow newRow = dtInsuredPhone.NewRow();
                DataRow newRow2 = dtInsuredPhone.NewRow();
                if (myPerson.home_phone.Length > 0)
                {
                    newRow["Id"] = "Home";
                    newRow["Description"] = "Home";
                    dtInsuredPhone.Rows.InsertAt(newRow, 0);
                }
                if ((myPerson.cell_phone.Length > 0) && (!myPerson.cell_phone.Contains("(   )    -")))
                {
                    newRow2["Id"] = "Cell";
                    newRow2["Description"] = "Cell";
                    dtInsuredPhone.Rows.InsertAt(newRow2, 2);
                }
                ddlInsuredPhoneType.DataSource = dtInsuredPhone;
                ddlInsuredPhoneType.DataValueField = dtInsuredPhone.Columns[0].ColumnName;
                ddlInsuredPhoneType.DataTextField = dtInsuredPhone.Columns[1].ColumnName;
                ddlInsuredPhoneType.DataBind();
            }

            if (ddlInsuredPhoneType.SelectedValue == "Home")
                lblInsuredPhone.Text = myPerson.home_phone;
            else
                lblInsuredPhone.Text = myPerson.cell_phone;


        }

        public void PopulateOwnerPhones()
        {
            Person myOwnerPerson = new Person();
            myOwnerPerson.GetPersonByPersonID(Convert.ToInt32(ddlOwner.SelectedValue));
            if (!Page.IsPostBack)
            {

                DataTable dtOwnerPhone = new DataTable();
                dtOwnerPhone.Columns.Add("Id");
                dtOwnerPhone.Columns.Add("Description");
                DataRow newRow3 = dtOwnerPhone.NewRow();
                DataRow newRow4 = dtOwnerPhone.NewRow();
                if (myOwnerPerson.home_phone.Length > 0)
                {
                    newRow3["Id"] = "Home";
                    newRow3["Description"] = "Home";
                    dtOwnerPhone.Rows.InsertAt(newRow3, 0);
                }
                if ((myOwnerPerson.cell_phone.Length > 0) && (!myOwnerPerson.cell_phone.Contains("(   )    -")))
                {
                    newRow4["Id"] = "Cell";
                    newRow4["Description"] = "Cell";
                    dtOwnerPhone.Rows.InsertAt(newRow4, 2);
                }
                ddlOwnerPhoneType.DataSource = dtOwnerPhone;
                ddlOwnerPhoneType.DataValueField = dtOwnerPhone.Columns[0].ColumnName;
                ddlOwnerPhoneType.DataTextField = dtOwnerPhone.Columns[1].ColumnName;
                ddlOwnerPhoneType.DataBind();
            }
            if (ddlOwnerPhoneType.SelectedValue == "Home")
                lblOwnerPhone.Text = myOwnerPerson.home_phone;
            else
                lblOwnerPhone.Text = myOwnerPerson.cell_phone;


        }

        public void PopulateDriverPhones()
        {
            Person myDriverPerson = new Person();
            myDriverPerson.GetPersonByPersonID(Convert.ToInt32(ddlDriver.SelectedValue));
            if (!Page.IsPostBack)
            {
                DataTable dtDriverPhone = new DataTable();
                dtDriverPhone.Columns.Add("Id");
                dtDriverPhone.Columns.Add("Description");
                DataRow newRow5 = dtDriverPhone.NewRow();
                DataRow newRow6 = dtDriverPhone.NewRow();
                if (myDriverPerson.home_phone.Length > 0)
                {
                    newRow5["Id"] = "Home";
                    newRow5["Description"] = "Home";
                    dtDriverPhone.Rows.InsertAt(newRow5, 0);
                }
                if ((myDriverPerson.cell_phone.Length > 0) && (!myDriverPerson.cell_phone.Contains("(   )    -")))
                {
                    newRow6["Id"] = "Cell";
                    newRow6["Description"] = "Cell";
                    dtDriverPhone.Rows.InsertAt(newRow6, 2);
                }
                ddlDriverPhoneType.DataSource = dtDriverPhone;
                ddlDriverPhoneType.DataValueField = dtDriverPhone.Columns[0].ColumnName;
                ddlDriverPhoneType.DataTextField = dtDriverPhone.Columns[1].ColumnName;
                ddlDriverPhoneType.DataBind();
            }
            if (ddlDriverPhoneType.SelectedValue == "Home")
                lblDriverPhone.Text = myDriverPerson.home_phone;
            else
                lblDriverPhone.Text = myDriverPerson.cell_phone;
        }

        protected void ddlOwner_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateOwnerPhones();
        }

        protected void ddlDriver_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateDriverPhones();
        }
    }


}
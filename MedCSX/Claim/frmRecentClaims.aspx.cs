﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;

namespace MedCSX.Claim
{
    public partial class frmRecentClaims : System.Web.UI.Page
    {
        private ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = mm.LoadRecentClaims(Convert.ToInt32(Session["userID"]));
            ddlRecentClaims.DataSource = dt;
            ddlRecentClaims.DataBind();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }
    }
}
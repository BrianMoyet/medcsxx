﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmConfigureDocument : System.Web.UI.Page
    {
        DocumentType MyDocumentType = new DocumentType();
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Request.QueryString["document_id"]) > 0)
                this.MyDocumentType = new DocumentType(Convert.ToInt32(Request.QueryString["document_id"]));
            else
                this.MyDocumentType = new DocumentType();

            if (!Page.IsPostBack)
                LoadData();
        }

        private void LoadData()
        {

            mf.LoadTypeTableDDL(this.cboEventType, "Event_Type", true);
            mf.LoadTypeTableDDL(this.cboClaimType, "Claim_Type", true);
            mf.LoadComboBoxFromStoredProc("usp_Stored_Procedures_Without_Parameters", this.cboStoredProcedure, "name");
            cboStoredProcedure.Items.Insert(0, new ListItem("", ""));

            if (Convert.ToInt32(Request.QueryString["document_id"]) > 0)
            { 
                txtDescription.Text = MyDocumentType.Description;
                lblCurrentFile.Text = MyDocumentType.FileName;
                

                if (MyDocumentType.Stored_Procedure_Name != "")
                    cboStoredProcedure.Items.FindByText((string)MyDocumentType.Stored_Procedure_Name).Selected = true;

                if (MyDocumentType.EventType.Description != "")
                    cboEventType.Items.FindByText((string)MyDocumentType.EventType.Description).Selected = true;

                if (MyDocumentType.ClaimType.Description != "")
                    cboClaimType.Items.FindByText((string)MyDocumentType.ClaimType.Description).Selected = true;
            }

            chkActive.Checked = (bool)MyDocumentType.Active;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (txtFileName.FileName.Length > 3)
            {
                lblCurrentFile.Text = txtFileName.FileName;
                MyDocumentType.EnumName = ml.makeEnum(lblCurrentFile.Text);
            }

            if (lblCurrentFile.Text.Length < 3)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You must select a document template.');</script>");
                return;
            }

            MyDocumentType.Description = txtDescription.Text;
            MyDocumentType.FileName = lblCurrentFile.Text;
            MyDocumentType.Stored_Procedure_Name = cboStoredProcedure.SelectedValue.ToString();
            MyDocumentType.EventTypeId = Convert.ToInt32(cboEventType.SelectedValue);
            MyDocumentType.ClaimTypeId = Convert.ToInt32(cboClaimType.SelectedValue);
            MyDocumentType.Active = (bool)chkActive.Checked;

            if (MyDocumentType.EnumName.Length < 3)
                MyDocumentType.EnumName = ml.makeEnum(lblCurrentFile.Text);

            MyDocumentType.Update();

            string tmpreferrer = Session["refererAdmin"].ToString();
            Session["refererAdmin"] = null;
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + tmpreferrer + "';window.close();", true);
        }

        //protected void txtFileName_DataBinding(object sender, EventArgs e)
        //{
        //    lblCurrentFile.Text = txtFileName.FileName;
        //}
    }
}
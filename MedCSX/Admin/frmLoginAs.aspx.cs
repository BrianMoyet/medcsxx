﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmLoginAs : System.Web.UI.Page
    {
        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Log_In_As))
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
            //    return;
            //}

            if (!Page.IsPostBack)
            {
                populateUsers();
            }


        }

        private void populateUsers()
        {
            ArrayList userList = MedCsXSystem.UserNames();
            gvUsers.DataSource = userList;
            gvUsers.DataBind();
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (gvUsers.SelectedIndex < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Select a user.');</script>");
                return;
            }

            if (txtPassword.Text.Trim() == mm.MASTER_PASSWORD)
            {
                int id = MedCsxLogic.User.UserIdForName(gvUsers.SelectedRow.Cells[1].Text);
                
                MedCsxLogic.User.LogonUser();
                MedCsXSystem mySystem = new MedCsXSystem();
                Session["userID"] = id;
                mySystem.UserId = id;
                

                ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='../claim/default.aspx';window.close();", true);
                

            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Incorrect Master Password.');</script>");
                txtPassword.Focus();
            }
              
        }
    }
}
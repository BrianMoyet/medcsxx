﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmEditFileNoteTypes : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Type_Tables) == false)
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);

            if (!Page.IsPostBack)
            {
                LoadFileNoteTypes();
            }
        }

        private void LoadFileNoteTypes()
        {
            this.grFileNoteTypes.DataSource = mm.GetFileNoteTypes();
            grFileNoteTypes.DataBind();
            grFileNoteTypes.SelectedIndex = 0;
            grFileNoteTypes_SelectedIndexChanged(null, null);
        }

        protected void grFileNoteTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.grEventTypes.DataSource = mm.GetFileNoteEventTypes(Convert.ToInt32(grFileNoteTypes.DataKeys[grFileNoteTypes.SelectedIndex].Value));
            grEventTypes.DataBind();

            this.grClearingTasks.DataSource = mm.GetFileNoteClearingTasks(Convert.ToInt32(grFileNoteTypes.DataKeys[grFileNoteTypes.SelectedIndex].Value));
            grClearingTasks.DataBind();
        }
    }
}
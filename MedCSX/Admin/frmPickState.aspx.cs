﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxDatabase;
using MedCsxLogic;

namespace MedCSX.Admin
{
    public partial class frmPickState : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadStates();
            }
        }

        protected void LoadStates()
        {
            lstVendorTypes.DataSource = TypeTable.getTypeTableRowsDDL("State", "1 = 1");
            lstVendorTypes.DataBind();
            //lstVendorTypes.SelectedIndex = 0;
            //lstVendorTypes_SelectedIndexChanged(null, null);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            SceneAccessAppraiser MyAppraiser = new SceneAccessAppraiser(Convert.ToInt32(Request.QueryString["appraiserid"]));
            MyAppraiser.AddState(lstVendorTypes.SelectedRow.Cells[1].Text);
            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer2"].ToString() + "';window.close();", true);
        }
    }
}
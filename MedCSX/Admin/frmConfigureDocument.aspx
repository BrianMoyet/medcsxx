﻿<%@ Page Title="Configure Document" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmConfigureDocument.aspx.cs" Inherits="MedCSX.Admin.frmConfigureDocument" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <b>Configure Document Types</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Document Name
        </div>
         <div class="col-md-9">
             <asp:TextBox ID="txtDescription" Width="250px" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Word Template
        </div>
         <div class="col-md-9">
             <asp:FileUpload ID="txtFileName"  Width="250px"  runat="server" />  <asp:Label ID="lblCurrentFile" runat="server"></asp:Label>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Event
        </div>
         <div class="col-md-9">
             <asp:DropDownList ID="cboEventType" runat="server"></asp:DropDownList>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Stored Procedure
        </div>
         <div class="col-md-9">
             <asp:DropDownList ID="cboStoredProcedure" runat="server">
                 <asp:ListItem Value=""></asp:ListItem>
             </asp:DropDownList>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            Claim Type
        </div>
         <div class="col-md-9">
             <asp:DropDownList ID="cboClaimType" runat="server"></asp:DropDownList>
        </div>
    </div>
         <div class="row">
        <div class="col-md-3">
            Active
        </div>
         <div class="col-md-9">
             <asp:CheckBox ID="chkActive" runat="server" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Button ID="btnOK" runat="server"  class="btn btn-info btn-xs"  Text="OK"  OnClick="btnOK_Click" />
            <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-xs pull-right"  Text="Cancel/Close"    OnClientClick="window.close(); return false;" />
        </div>
    </div>
</asp:Content>

﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmLogonHistory : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();


        protected void Page_Load(object sender, EventArgs e)
        {
            grLogonHistory.DataSource = db.ExecuteStoredProcedureReturnDataTable ("usp_Get_Logon_History", Convert.ToInt32(Request.QueryString["userId"]));
            grLogonHistory.DataBind();
        }
    }
}
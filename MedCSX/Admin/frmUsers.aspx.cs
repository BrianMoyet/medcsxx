﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmUsers : System.Web.UI.Page
    {
        private int              //selected User ID
           userReserveTypeId = 0,                     //selected User reserve type ID
           genIterator = 0,
           sigIterator = 0,
           grpIterator = 0,
           AuthIterator = 0,
           AttIterator = 0,
           CoIterator = 0,
           alertIterator = 0,
           UsrIterator = 0;

        private bool loadingUser = true,              //is user being loaded?
            isLoading = false;                        //is form loading?
        private string oldValue = "";                 //previous value of text field - used for undo
        private User thisUser,
            saveUser;

        private long AllowanceForUser = 0;
        private List<UserGrid> myUserList;
        private List<cboGrid> myStatusList;
        private List<cboGrid> myPrinterList;
        private List<cboGrid> myTypeList;
        private List<cboGrid> mySupervisorList;
        private List<cboGrid> mySubTypeList;
        private List<cboGrid> myAlertList;
        private List<cboGrid> myOutlookStatus;
        private List<cboGrid> myOutlookImportance;
        private List<chkGrid> myLanguageList;
        private List<chkGrid> myGroupList;
        private List<chkGrid> myCompanyList;
        private List<reserveGrid> myAuthorityList;
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Users))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                return;
            }

            if (!Page.IsPostBack)
            {
                userCalendar.SelectedDate = DateTime.Now;
                LoadUsers();
             
                TabContainer1_ActiveTabChanged(null, null);

            }
        }
        private void LoadUsers()
        {
            LoadUsers(mm.GetActiveUsers(4));
            DataTable dt = db.ExecuteStoredProcedureReturnDataTable("usp_Get_All_Subordinate_Users", Convert.ToInt32(HttpContext.Current.Session["userID"]));
            grUsers.DataSource = dt;
            grUsers.DataBind();

            Session["grUsersAdminTable"] = dt;


        }

        protected void grUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblUserName.Text = grUsers.SelectedRow.Cells[2].Text + " " + grUsers.SelectedRow.Cells[3].Text;
            //selectedUserId = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);

            User thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            //this.TabContainer1_ActiveTabChanged(null, null);
            LoadAll();
            //EnableDisableAttorneyTab(thisUser.UserType().Description);
        }

        private IEnumerable LoadUsers(List<Hashtable> list)
        {
            myUserList = new List<UserGrid>();
            try
            {
                foreach (Hashtable uData in list)
                {
                    UserGrid g = new UserGrid();
                    g.id = (int)uData["user_id"];
                    g.fName = (string)uData["first_name"];
                    g.lName = (string)uData["last_name"];
                    g.userType = (string)uData["user_type"];
                    g.userStatus = (string)uData["user_status"];
                    g.imageIdx = 1;
                    if (g.id == 10)
                        g.imageIdx = 4;
                    myUserList.Add(g);
                }
                return myUserList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myUserList;
            }
        }

        //private void EnableDisableAttorneyTab(string p)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();

        //        if ((p.ToUpper().IndexOf("ATTORNEY") > 0) || (p.ToUpper() == "ATTORNEY"))
        //        {
        //            this.TabPanel6.Enabled = true;
        //            //this.txtAttorneyVendor.IsEnabled = true;
        //            //this.btnPickVendor.IsEnabled = true;
        //        }
        //        else
        //        {
        //            this.TabPanel6.Enabled = false;
        //            //this.txtAttorneyVendor.IsEnabled = false;
        //            //this.btnPickVendor.IsEnabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected void btnUsersAdd_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Hashtable newUser = new Hashtable();
                newUser["User_Status_Id"] = (int)modGeneratedEnums.UserStatus.Active;
                newUser["User_Type_Id"] = (int)modGeneratedEnums.UserType.Adjuster;
                int user_id = ml.InsertRow(newUser, "User");

                LoadUsers();
                if (grUsers.Rows.Count > 0)
                { 
                    this.grUsers.SelectedIndex = 0;  // setSelection(user_id);
                    grUsers_SelectedIndexChanged(null, null);
                }

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('New user created with Id: " + user_id.ToString() + ". Please complete details.');</script>");
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnUsersUpdate_Click(object sender, EventArgs e)
        {

        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
            //if (this.TabContainer1.ActiveTabIndex == 0)  //General
            //{
            //    LoadGeneral();
            //}

            //if (TabContainer1.ActiveTabIndex == 1)  //alerts
            //{

            
            //}

            //if (TabContainer1.ActiveTabIndex == 2) //signature
            //{
            //    LoadSignature();
            //}

            //if (TabContainer1.ActiveTabIndex == 3) //groups
            //{
            //    LoadGroups();
            //    SelectGroupsForUser();
            //}

            //if (TabContainer1.ActiveTabIndex == 4) //authority
            //{
            //    LoadAuthority();
            //}
            //if (TabContainer1.ActiveTabIndex == 5) //attorney
            //{
               
            //}
            //if (TabContainer1.ActiveTabIndex == 6) //comapny authority
            //{
               
            //}
            //if (TabContainer1.ActiveTabIndex == 7) //allowed authority
            //{
              
            //}

            LoadAll();

        }

        public void LoadAll()
        {
            LoadGeneral();

            LoadAlerts();

            LoadSignature();

            LoadGroups();
          
            LoadAuthority();

            LoadCompany();

            MarkAllowances_byIsUserAuthorized();

            //LoadAllowedAuthority();
        }

        private void LoadAlerts()
        {
            try
            {
                alertIterator++;
                DateTime d;
                if (this.userCalendar.SelectedDate != null)
                    d = (DateTime)this.userCalendar.SelectedDate;
                else
                {
                    d = DateTime.Now;
                    this.userCalendar.SelectedDate = d;
                }

      
                thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

                this.lblUserActiveAlerts.Text = "Active Alerts Before " + d.ToString("MMM dd, yyyy") + ":";
                this.userActiveAlerts.Text = thisUser.NbrAlertsBefore(d).ToString();
                this.lblUserActiveDiaries.Text = "Active Diaries Before " + d.ToString("MMM dd, yyyy") + ":";
                this.userActiveDiaries.Text = thisUser.NbrDiariesBefore(d).ToString();

                this.chkForceDiary.Checked = thisUser.ForceDiary;
                this.chkForceDiaryByUserType.Checked = thisUser.UseForceDiaryFromUserType;

                this.chkGracePeriodByUserType.Checked = thisUser.UseDiaryGracePeriodFromUserType;
                this.txtDiaryGracePeriod.Text = thisUser.DiaryGracePeriod.ToString();

                if (thisUser.UseForceDiaryFromUserType)
                    this.chkForceDiaryByUserType.Enabled = false;
                else
                    this.chkForceDiaryByUserType.Enabled = true;

                if (thisUser.UseDiaryGracePeriodFromUserType)
                    this.pnlDiaryGracePeriod.Visible = false;
                else
                    this.pnlDiaryGracePeriod.Visible = true;
                this.chkGracePeriodByUserType.Enabled = true;

                this.cboAlertPreference.DataSource = LoadCBO(4);
                this.cboAlertPreference.DataValueField = "id";
                this.cboAlertPreference.DataTextField = "description";
                this.cboAlertPreference.DataBind();

                this.cboAlertPreference.SelectedIndex = setSelection(thisUser.AlertPreferenceId, myAlertList);

                this.cboOutgoingAlert.DataSource = LoadCBO(4);
                this.cboOutgoingAlert.DataValueField = "id";
                this.cboOutgoingAlert.DataTextField = "description";
                this.cboOutgoingAlert.DataBind();

                this.cboOutgoingAlert.SelectedIndex = setSelection(thisUser.OutgoingAlertPreferenceId, myAlertList);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }
        //private void chkAllowances_Checked(object sender, EventArgs e)
        //{

        //    CheckBox thisBox = (CheckBox)sender;
        //    //int thisAllowance = int.Parse((string)thisBox.Tag);
        //    int thisAllowance = 0;

        //    switch (thisBox.ID)
        //    {
        //        case "chkEditUser":
        //            thisAllowance = 0;
        //            break;
        //        case "chkCloseClaim":
        //            thisAllowance = 1;

        //            break;

        //        default:
        //            break;
        //    }

        //            if ((bool)thisBox.Checked)
        //        FlagsHelper.Set(ref this.AllowanceForUser, (int)thisAllowance);
        //    else
        //        FlagsHelper.UnSet(ref this.AllowanceForUser, (int)thisAllowance);
        //    thisUser.UserAllowance = this.AllowanceForUser;
        //    thisUser.Update();
        //}

        private void chkSuper_Unchecked(object sender, EventArgs e)
        {
            AllowanceForUser = 0;
            thisUser.UserAllowance = AllowanceForUser;
            thisUser.Update();

            UnMarkAllowances();
            MarkAllowances_byIsUserAuthorized();
        }

        private void UnMarkAllowances()
        {
            //foreach (CheckBox element in chkAuthorityGrid.Children.OfType<CheckBox>())
            //{
            //    element.Checked = false;
            //}
        }

        private void MarkAllowances_byUserAllowance()
        {
            this.chkEditUser.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_Users);
            this.chkCloseClaim.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Close_Claim);
            this.chkEditTypeTable.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_Type_Tables);
            this.chkEditVendor.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_Vendors);
            this.chkEditUserGroup.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_User_Groups);
            this.chkConfigNotes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Configure_Alerts_Diary_File_Notes);
            this.chkEditSettings.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_Global_Settings);
            this.chkOpenLog.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Open_Error_Log);
            this.chkEditDictionary.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_Data_Dictionary);
            this.chkVendorAssoc.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Vendor_Type_Sub_Type_Associations);
            this.chkLogUser.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Logged_In_Users);
            this.chkKillUser.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Kill_Users);
            this.chkEventView.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Event_Viewer);
            this.chkAddReserve.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Add_Reserve);
            this.chkChgLossRes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Change_Loss_Reserve);
            this.chkChgExpRes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Change_Expense_Reserve);
            this.chkReOpenRes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Reopen_Reserve);
            this.chkCloseRes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Close_Reserve);
            this.chkClsPendSalv.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Close_Pending_Salvage);
            this.chkClsPendSub.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Close_Pending_Subro);
            this.chkIssueDraft.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Issue_Draft);
            this.chkRecSubro.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Receive_Subro);
            this.chkRecSal.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Receive_Salvage);
            this.chkVoidDrft.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Void_Draft);
            this.chkVdSubro.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Void_Subro);
            this.chkVdSal.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Void_Salvage);
            this.chkModifyClaim.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Modify_Claim_Data);
            this.chkVoidRes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Void_Reserve);
            this.chkReissDraft.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Reissue_Draft);
            this.chkReissSubro.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Reissue_Subro);
            this.chkReissSalv.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Reissue_Salvage);
            this.chkReopenClaim.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Reopen_Claim);
            this.chkChgVendTax.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Change_Vendor_Tax_Id_Required);
            this.chkSendMsg.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Send_Message);
            this.chkStaffMetrix.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Staffing_Metrics);
            this.chkRecoverRpt.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Recovery_Amounts_Report);
            this.chkEditTrans.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Edit_Transaction_Type_Conversion);
            this.chkPrintFoxPro.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Print_Foxpro_Drafts);
            this.chkAcctRpt.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Accounting_Reports);
            this.chkManLock.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Manually_Lock_Claim);
            this.chkSetAvgRes.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Set_Average_Reserves);
            this.chkSetIcon.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Set_Type_Table_Icons);
            this.chkSQLLog.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.SQL_Logging);
            this.chkBulkAssign.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Bulk_Assign_Claims);
            this.chkUnVoidDraft.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Unvoid_Draft);
            this.chkAuditClaim.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Audit_Claims);
            this.chkLogInAs.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Log_In_As);
            this.chkResendAppr.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Resend_Appraisal_Requests);
            this.chkCompleteAppr.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Mark_Appraisals_as_Completed);
            this.chkCancelAppr.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Cancel_Appraisal);
            this.chkChgDocStatus.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Change_Document_Status);
            this.chkNewDoc.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Create_New_Document);
            this.chkOpenDoc.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Open_Document);
            this.chkPrintQueue.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Print_Draft_Queue);
            this.chkViewAppr.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.View_Appraiser_Data);
            this.chkWireTrans.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Wire_Transfer);
            this.chkPendDraft.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Pending_Drafts);
            this.chkResendEmail.Checked = FlagsHelper.IsSet((long)AllowanceForUser, (int)Task_Allowed.Resend_Appraisal_Emails);
        }


        private void MarkAllowances_byIsUserAuthorized()
        {
            int selectedUserId = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);
            this.chkEditUser.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_Users, selectedUserId);
            this.chkCloseClaim.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Close_Claim, selectedUserId);
            this.chkEditTypeTable.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_Type_Tables, selectedUserId);
            this.chkEditVendor.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_Vendors, selectedUserId);
            this.chkEditUserGroup.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_User_Groups, selectedUserId);
            this.chkConfigNotes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Configure_Alerts_Diary_File_Notes, selectedUserId);
            this.chkEditSettings.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_Global_Settings, selectedUserId);
            this.chkOpenLog.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Open_Error_Log, selectedUserId);
            this.chkEditDictionary.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_Data_Dictionary, selectedUserId);
            this.chkVendorAssoc.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Vendor_Type_Sub_Type_Associations, selectedUserId);
            this.chkLogUser.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Logged_In_Users, selectedUserId);
            this.chkKillUser.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Kill_Users, selectedUserId);
            this.chkEventView.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Event_Viewer, selectedUserId);
            this.chkAddReserve.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Add_Reserve, selectedUserId);
            this.chkChgLossRes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Change_Loss_Reserve, selectedUserId);
            this.chkChgExpRes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Change_Expense_Reserve, selectedUserId);
            this.chkReOpenRes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Reopen_Reserve, selectedUserId);
            this.chkCloseRes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Close_Reserve, selectedUserId);
            this.chkClsPendSalv.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Close_Pending_Salvage, selectedUserId);
            this.chkClsPendSub.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Close_Pending_Subro, selectedUserId);
            this.chkIssueDraft.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Issue_Draft, selectedUserId);
            this.chkRecSubro.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Receive_Subro, selectedUserId);
            this.chkRecSal.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Receive_Salvage, selectedUserId);
            this.chkVoidDrft.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Void_Draft, selectedUserId);
            this.chkVdSubro.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Void_Subro, selectedUserId);
            this.chkVdSal.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Void_Salvage, selectedUserId);
            this.chkModifyClaim.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Modify_Claim_Data, selectedUserId);
            this.chkVoidRes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Void_Reserve, selectedUserId);
            this.chkReissDraft.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Reissue_Draft, selectedUserId);
            this.chkReissSubro.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Reissue_Subro, selectedUserId);
            this.chkReissSalv.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Reissue_Salvage, selectedUserId);
            this.chkReopenClaim.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Reopen_Claim, selectedUserId);
            this.chkChgVendTax.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Change_Vendor_Tax_Id_Required, selectedUserId);
            this.chkSendMsg.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Send_Message, selectedUserId);
            this.chkStaffMetrix.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Staffing_Metrics, selectedUserId);
            this.chkRecoverRpt.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Recovery_Amounts_Report, selectedUserId);
            this.chkEditTrans.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Edit_Transaction_Type_Conversion, selectedUserId);
            this.chkPrintFoxPro.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Print_Foxpro_Drafts, selectedUserId);
            this.chkAcctRpt.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Accounting_Reports, selectedUserId);
            this.chkManLock.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Manually_Lock_Claim, selectedUserId);
            this.chkSetAvgRes.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Set_Average_Reserves, selectedUserId);
            this.chkSetIcon.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Set_Type_Table_Icons, selectedUserId);
            this.chkSQLLog.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.SQL_Logging, selectedUserId);
            this.chkBulkAssign.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Bulk_Assign_Claims, selectedUserId);
            this.chkUnVoidDraft.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Unvoid_Draft, selectedUserId);
            this.chkAuditClaim.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Audit_Claims, selectedUserId);
            this.chkLogInAs.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Log_In_As, selectedUserId);
            this.chkResendAppr.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Resend_Appraisal_Requests, selectedUserId);
            this.chkCompleteAppr.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Mark_Appraisals_as_Completed, selectedUserId);
            this.chkCancelAppr.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Cancel_Appraisal, selectedUserId);
            this.chkChgDocStatus.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Change_Document_Status, selectedUserId);
            this.chkNewDoc.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Create_New_Document, selectedUserId);
            this.chkOpenDoc.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Open_Document, selectedUserId);
            this.chkPrintQueue.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Print_Draft_Queue, selectedUserId);
            this.chkViewAppr.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.View_Appraiser_Data, selectedUserId);
            this.chkWireTrans.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Wire_Transfer, selectedUserId);
            this.chkPendDraft.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Pending_Drafts, selectedUserId);
            this.chkResendEmail.Checked = mm.IsUserAuthorizedForTask((int)modGeneratedEnums.Task.Resend_Appraisal_Emails, selectedUserId);
        }

            //private void LoadAllowedAuthority()
            //{
            //    this.UsrIterator++;
            //    if ((thisUser == null) || (thisUser.Id != selectedUserId))
            //        thisUser = new User(selectedUserId);

            //    MarkAllowances_byIsUserAuthorized();
            //}

        private void LoadCompany()
        {
            this.CoIterator++;
            if ((thisUser == null) || (thisUser.Id != Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
                thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

            this.grUserCompany.DataSource = Company.getCompanies();
            //this.grUserCompany.DataSource = LoadCompany(MedCsxLogic.User.CompanyAuthority(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)));
            this.grUserCompany.DataBind();

            SelectCompanyAuthorityForUser();

            //SelectCompanyAuthorityForUser(MedCsxLogic.User.CompanyAuthority(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)));
        }

        

        private IEnumerable LoadCompany(List<Hashtable> list)
        {
            this.myCompanyList = new List<chkGrid>();
            try
            {
                foreach (Hashtable cData in list)
                {
                    chkGrid gr = new chkGrid();
                    gr.id = (int)cData["Company_Id"];
                    gr.imageIdx = (int)cData["Image_Index"];
                    gr.description = (string)cData["Description"];
                    //gr.Checked = (bool)cData["Open_Allowed"];
                    gr.authorityId = (int)cData["User_Company_Authority_Id"];
                    myCompanyList.Add(gr);
                }
                return myCompanyList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myCompanyList;
            }
        }

        private void LoadSignature()
        {
            this.sigIterator++;
            if ((thisUser == null) || (thisUser.Id != Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
                thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

            string sigFile = thisUser.SignatureFileName.ToUpper().Replace(@"G:\CLAIMS\SIGNATURES\", "../signatures/");
            txtSignatureFile.Text = sigFile;
            this.txtSignatureFileName.ImageUrl = sigFile;

        //g:\claims\signatures\shermanb.bmp


        }

        private void LoadGeneral()
        {
            lblpwchanged.Visible = false;

            this.isLoading = true;
            this.cboUserStatus.DataSource = LoadCBO(1);
            this.cboUserStatus.DataValueField = "id";
            this.cboUserStatus.DataTextField = "description";
            this.cboUserStatus.DataBind();

            this.cboUserType.DataSource = LoadCBO(2);
            this.cboUserType.DataValueField = "id";
            this.cboUserType.DataTextField = "description";
            this.cboUserType.DataBind();

            this.cboUserClassification.DataSource = LoadCBO(3);
            this.cboUserClassification.DataValueField = "id";
            this.cboUserClassification.DataTextField = "description";
            this.cboUserClassification.DataBind();

            this.cboDefaultPrinter.DataSource = LoadCBO(5);
            this.cboDefaultPrinter.DataValueField = "id";
            this.cboDefaultPrinter.DataTextField = "description";
            this.cboDefaultPrinter.DataBind();

            this.cboUserSupervisor.DataSource = RefreshSupervisors();
            this.cboUserSupervisor.DataValueField = "id";
            this.cboUserSupervisor.DataTextField = "description";
            this.cboUserSupervisor.DataBind();

            this.grUserLanguage.DataSource = LoadLanguages();
           
            this.grUserLanguage.DataBind();

            if (!Page.IsPostBack)
                grUsers.SelectedIndex = 0;

            if (grUsers.Rows.Count > 0)
            { 
                if (Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value) != 0)
                {
                    genIterator++;
                    thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
                    this.txtUserId.Text = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value).ToString();
                    this.txtUserFName.Text = thisUser.FirstName;
                    this.txtUserLName.Text = thisUser.LastName;
                    this.txtUserLogon.Text = thisUser.LoginId;
                    this.txtUserExperience.Text = thisUser.DateClaimExperienceStarted.ToShortDateString();
                    this.txtUserPhone.Text = thisUser.PhoneExt;
                    this.txtUserFax.Text = thisUser.DirectFax;
                    this.txtUserEmail.Text = thisUser.EmailAddress;
                    this.chkUserAssignedClaims.Checked = (bool)thisUser.CanBeAssignedClaims;

                    SetGeneralComboBoxes();

                    SelectLanguages();
                }
            }
            this.isLoading = false;
        }

        private void SelectLanguages()
        {
            for (int i = 0; i < grUserLanguage.Rows.Count; i++)
            {
                foreach (Hashtable a in mm.GetLanguagesForUser(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
                {
                    if ((string)a["Language"] == grUserLanguage.Rows[i].Cells[1].Text)
                    { 
                        CheckBox mycheck = ((CheckBox)grUserLanguage.Rows[i].FindControl("cbSelect"));
                        mycheck.Checked = true;
                    }
                }
            }


            //foreach (chkGrid c in myLanguageList)
            //    c.Checked = false;        //uncheck all languages

            //foreach (Hashtable a in mm.GetLanguagesForUser(selectedUserId))
            //{
            //    foreach (chkGrid c in myLanguageList)
            //    {
            //        if ((string)a["Language"] == c.description)
            //        {
            //            c.Checked = true;
            //            break;
            //        }
            //    }
            //}
        }

        private void SetGeneralComboBoxes()
        {
            try
            {
                this.cboUserStatus.SelectedIndex = setSelection(thisUser.UserStatusId, myStatusList);

                this.cboUserSupervisor.SelectedIndex = setSelection(thisUser.SupervisorId, mySupervisorList);

                this.cboUserType.SelectedIndex = setSelection(thisUser.UserTypeId, myTypeList);

                this.cboDefaultPrinter.SelectedIndex = setSelection(thisUser.UserDefaultDraftPrinterId, myPrinterList);

                if (thisUser.UserTypeId == (int)modGeneratedEnums.UserType.Adjuster)
                    this.cboUserClassification.SelectedIndex = setSelection(thisUser.UserSubTypeId, mySubTypeList);
                else
                    this.cboUserClassification.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
               
            }
        }

        private IEnumerable LoadLanguages()
        {
            myLanguageList = new List<chkGrid>();
            try
            {
                foreach (Hashtable lData in mm.GetLanguages())
                {
                    chkGrid l = new chkGrid();
                    l.id = (int)lData["Language_Id"];
                    l.imageIdx = (int)lData["Image_Index"];
                    l.description = (string)lData["Description"];
                    myLanguageList.Add(l);
                }
                return myLanguageList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myLanguageList;
            }
        }

        private IEnumerable LoadCBO(int mode)
        {
            List<Hashtable> list;
            List<cboGrid> myList = new List<cboGrid>();
            switch (mode)
            {
                case 1:     //user status
                    list = TypeTable.getTypeTableRows("User_Status");
                    break;
                case 2:     //user type
                    list = TypeTable.getTypeTableRows("User_Type");
                    break;
                case 3:     //user sub type
                    list = TypeTable.getTypeTableRows("User_Sub_Type");
                    break;
                case 4:
                    list = TypeTable.getTypeTableRows("Alert_Preference");
                    break;
                case 5:
                    list = TypeTable.getDefaultPrinterTypeTableRows("User_Default_Printer");
                    break;
                default:
                    list = new List<Hashtable>();
                    break;
            }

            try
            {
               
                    foreach (Hashtable h in list)
                    {
                        cboGrid c = new cboGrid();
                        c.id = (int)h["Id"];
                        c.imageIdx = (int)h["Image_Index"];
                        c.description = (string)h["Description"];
                        myList.Add(c);
                    }
               

            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }

            switch (mode)
            {
                case 1:     //user status
                    myStatusList = myList;
                    return myStatusList;
                case 2:     //user type
                    myTypeList = myList;
                    return myTypeList;
                case 3:     //user sub type
                    mySubTypeList = myList;
                    return mySubTypeList;
                case 4:
                    myAlertList = myList;
                    return myAlertList;
                case 5:
                    myPrinterList = myList;
                    return myPrinterList;
                default:
                    return myList;
            }
        }

        protected void grAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
          
                try
                {
                    mm.UpdateLastActivityTime();

                    if (grAuthority.SelectedIndex == 0)
                        return;

                    int mySelection = Convert.ToInt32(grAuthority.DataKeys[grAuthority.SelectedIndex].Value);



                    RefreshSupervisors();
                    this.userReserveTypeId = mySelection;
                    this.txtFinancialAuthority.Text = grAuthority.SelectedRow.Cells[2].Text;
                    cboUserReserveSupervisor.SelectedIndex = setSelection(Convert.ToInt32(grAuthority.SelectedRow.Cells[4].Text), mySupervisorList);
                }
                catch (Exception ex)
                {
                    mf.ProcessError(ex);
                }
           
        }

        private int setSelection(int p, List<cboGrid> myList = null)
        {
            int idx = 0;
            if (p == 0)
                return idx;
            if (myList == null)
            {
                foreach (UserGrid g in myUserList)
                {
                    if (p == g.id)
                        break;
                    idx++;
                }
            }
            else
            {
                foreach (cboGrid c in myList)
                {
                    if (p == c.id)
                        break;
                    idx++;
                }
            }
            return idx;
        }

        protected void btnSelectAllGroups_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grUserGroup.Rows.Count; i++)
            {
               
                CheckBox mycheck = ((CheckBox)grUserGroup.Rows[i].FindControl("cbSelect"));
                mycheck.Checked = true;
                
            }
        }

        protected void btnSelectNoGroups_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grUserGroup.Rows.Count; i++)
            {

                CheckBox mycheck = ((CheckBox)grUserGroup.Rows[i].FindControl("cbSelect"));
                mycheck.Checked = false;

            }
        }

        private void LoadGroups()
        {
            this.grpIterator++;
            if ((thisUser == null) || (thisUser.Id != Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
                thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

            this.grUserGroup.DataSource = LoadGroups(mm.GetActiveUsers(1));
            this.grUserGroup.DataBind();
 
            if (grUserGroup.Rows.Count > 0)
            { 
                SelectGroupsForUser();
            }
            //this.grUserGroup.Items.Refresh();

            //SelectGroupsForUser();
        }

        protected void btnSelectAllCo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grUserCompany.Rows.Count; i++)
            {

                CheckBox mycheck = ((CheckBox)grUserCompany.Rows[i].FindControl("cbSelect"));
                mycheck.Checked = true;

            }
        }

        protected void btnSelectNoCo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grUserCompany.Rows.Count; i++)
            {

                CheckBox mycheck = ((CheckBox)grUserCompany.Rows[i].FindControl("cbSelect"));
                mycheck.Checked = false;

            }
        }

        private void SelectGroupsForUser()
        {
            if (grUserGroup.Rows.Count > 0)
            { 
                for (int i = 0; i < grUserGroup.Rows.Count; i++)
                {
                    foreach (Hashtable a in db.ExecuteStoredProcedureReturnHashList("usp_Get_User_Groups_For_User", Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
                    {
                        string tmp = (string)a["User_Group"];
                        if ((string)a["User_Group"] == grUserGroup.Rows[i].Cells[1].Text)
                        {
                            CheckBox mycheck = ((CheckBox)grUserGroup.Rows[i].FindControl("cbSelect"));
                            mycheck.Checked = true;
                        }
                    }
                }
            }


        }

        //private void SelectCompanyAuthorityForUser()
        //{
        //    for (int i = 0; i < grUserCompany.Rows.Count; i++)
        //    {
        //        foreach (Hashtable a in db.ExecuteStoredProcedureReturnHashList("usp_Get_User_Groups_For_User", Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
        //        {
        //            string tmp = (string)a["User_Group"];
        //            if ((string)a["User_Group"] == grUserCompany.Rows[i].Cells[1].Text)
        //            {
        //                CheckBox mycheck = ((CheckBox)grUserCompany.Rows[i].FindControl("cbSelect"));
        //                mycheck.Checked = true;
        //            }
        //        }
        //    }


        //}

        private void SelectCompanyAuthorityForUser()
        {
            DataTable dt = db.ExecuteStoredProcedure("usp_Get_User_Company_Authority", Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            //thisUser =  new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            for (int i = 0; i < grUserCompany.Rows.Count; i++)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if ((bool)row["open_allowed"] == true)
                    { 
                        int tmp1 = (int)row["company_id"];
                        int tmp2 = Convert.ToInt32(grUserCompany.DataKeys[i].Value);
                        if (tmp1  == tmp2)
                        {
                            CheckBox mycheck = ((CheckBox)grUserCompany.Rows[i].FindControl("cbSelect"));
                            mycheck.Checked = true;
                        }
                    }
                }
            }

        }

        private int setAllowance(string id)
        {
            int thisAllowance = 0;

            switch (id)
            {
                case "chkEditUser":
                    thisAllowance = 0;
                    break;
                case "chkCloseClaim":
                    thisAllowance = 1;
                    break;
                case "chkEditTypeTable":
                    thisAllowance = 2;
                    break;
                case "chkEditVendor":
                    thisAllowance = 3;
                    break;
                case "chkEditUserGroup":
                    thisAllowance = 4;
                    break;
                case "chkConfigNotes":
                    thisAllowance = 5;
                    break;
                case "chkEditSettings":
                    thisAllowance = 6;
                    break;
                case "chkOpenLog":
                    thisAllowance = 7;
                    break;
                case "chkEditDictionary":
                    thisAllowance = 8;
                    break;
                case "chkVendorAssoc":
                    thisAllowance = 9;
                    break;
                case "chkLogUser":
                    thisAllowance = 10;
                    break;
                case "chkKillUser":
                    thisAllowance = 11;
                    break;
                case "chkEventView":
                    thisAllowance = 12;
                    break;
                case "chkAddReserve":
                    thisAllowance = 13;
                    break;
                case "chkChgLossRes":
                    thisAllowance = 14;
                    break;
                case "chkChgExpRes":
                    thisAllowance = 15;
                    break;
                case "chkReOpenRes":
                    thisAllowance = 16;
                    break;
                case "chkCloseRes":
                    thisAllowance = 17;
                    break;
                case "chkClsPendSalv":
                    thisAllowance = 18;
                    break;
                case "chkClsPendSub":
                    thisAllowance = 19;
                    break;
                case "chkIssueDraft":
                    thisAllowance = 20;
                    break;
                case "chkRecSubro":
                    thisAllowance = 21;
                    break;
                case "chkRecSal":
                    thisAllowance = 22;
                    break;
                case "chkVoidDrft":
                    thisAllowance = 23;
                    break;
                case "chkVdSubro":
                    thisAllowance = 24;
                    break;
                case "chkVdSal":
                    thisAllowance = 25;
                    break;
                case "chkModifyClaim":
                    thisAllowance = 26;
                    break;
                case "chkVoidRes":
                    thisAllowance = 27;
                    break;
                case "chkReissDraft":
                    thisAllowance = 28;
                    break;
                case "chkReissSubro":
                    thisAllowance = 29;
                    break;
                case "chkReissSalv":
                    thisAllowance = 30;
                    break;
                case "chkReopenClaim":
                    thisAllowance = 31;
                    break;
                case "chkChgVendTax":
                    thisAllowance = 32;
                    break;
                case "chkSendMsg":
                    thisAllowance = 33;
                    break;
                case "chkStaffMetrix":
                    thisAllowance = 34;
                    break;
                case "chkRecoverRpt":
                    thisAllowance = 35;
                    break;
                case "chkEditTrans":
                    thisAllowance = 36;
                    break;
                case "chkPrintFoxPro":
                    thisAllowance = 37;
                    break;
                case "chkAcctRpt":
                    thisAllowance = 38;
                    break;
                case "chkManLock":
                    thisAllowance = 39;
                    break;
                case "chkSetAvgRes":
                    thisAllowance = 40;
                    break;
                case "chkSetIcon":
                    thisAllowance = 41;
                    break;
                case "chkSQLLog":
                    thisAllowance = 42;
                    break;
                case "chkBulkAssign":
                    thisAllowance = 43;
                    break;
                case "chkUnVoidDraft":
                    thisAllowance = 44;
                    break;
                case "chkAuditClaim":
                    thisAllowance = 45;
                    break;
                case "chkLogInAs":
                    thisAllowance = 46;
                    break;
                case "chkResendAppr":
                    thisAllowance = 47;
                    break;
                case "chkCompleteAppr":
                    thisAllowance = 48;
                    break;
                case "chkCancelAppr":
                    thisAllowance = 49;
                    break;
                case "chkChgDocStatus":
                    thisAllowance = 50;
                    break;
                case "chkNewDoc":
                    thisAllowance = 51;
                    break;
                case "chkOpenDoc":
                    thisAllowance = 52;
                    break;
                case "chkPrintQueue":
                    thisAllowance = 53;
                    break;
                case "chkViewAppr":
                    thisAllowance = 54;
                    break;
                case "chkWireTrans":
                    thisAllowance = 55;
                    break;
                case "chkPendDraft":
                    thisAllowance = 56;
                    break;
                case "chkResendEmail":
                    thisAllowance = 57;
                    break;
                case "chkSuper":
                    thisAllowance = 58;
                    break;



                default:
                    break;
            }

            return thisAllowance;
        }

        protected void chkEditUser_CheckedChanged(object sender, EventArgs e)
        {
            

            CheckBox thisBox = (CheckBox)sender;

            int thisAllowance = 0;

            thisAllowance = setAllowance(thisBox.ID.ToString());


            thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            if ((bool)thisBox.Checked)
                FlagsHelper.Set(ref this.AllowanceForUser, (int)thisAllowance);
            else
                FlagsHelper.UnSet(ref this.AllowanceForUser, (int)thisAllowance);
            thisUser.UserAllowance = this.AllowanceForUser;
            thisUser.Update();
           
            //int thisAllowance = int.Parse((string)thisBox.Tag);
         
        }

        protected void chkSuper_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkSuper.Checked == true)
            //{

            //    CheckBox thisBox = (CheckBox)sender;

            //    int allChecked = 58;
            //    long maxAllowance = 0;
            //    for (int i = 0; i < allChecked; i++)
            //        maxAllowance += (long)System.Math.Pow(2, i);
            //    AllowanceForUser = maxAllowance;
            //    thisUser.UserAllowance = AllowanceForUser;
            //    MarkAllowances_byUserAllowance();
            //}
            //else
            //{
            //    AllowanceForUser = 0;
            //    thisUser.UserAllowance = AllowanceForUser;
            //    thisUser.Update();

            //    UnMarkAllowances();
            //    MarkAllowances_byIsUserAuthorized();
            //}
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void grUsers_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = Session["grUsersAdminTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                grUsers.DataSource = dt;
                grUsers.DataBind();

                Session["grUsersAdminTable"] = dt;
            }
        }

        protected void userCalendar_SelectionChanged(object sender, EventArgs e)
        {
            LoadAlerts();
        }

        protected void chkForceDiaryByUserType_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if ((bool)chkForceDiaryByUserType.Checked)
                    this.chkForceDiary.Enabled = false;
                else
                    this.chkForceDiary.Enabled = true;

                thisUser.UseForceDiaryFromUserType = (bool)chkForceDiaryByUserType.Checked;
                thisUser.Update();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void chkForceDiary_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                thisUser.ForceDiary = (bool)this.chkForceDiary.Checked;
                thisUser.Update();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void chkGracePeriodByUserType_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                this.pnlDiaryGracePeriod.Visible = (bool)this.chkGracePeriodByUserType.Checked == false;
                this.chkGracePeriodByUserType.Visible = true;

                thisUser.UseDiaryGracePeriodFromUserType = (bool)this.chkGracePeriodByUserType.Checked;
                thisUser.Update();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void txtDiaryGracePeriod_TextChanged(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (this.txtDiaryGracePeriod.Text == "")
                    return;

                if (!modLogic.isNumeric(this.txtDiaryGracePeriod.Text))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please Enter a Valid Diary Grace Period.');</script>");
                    this.txtDiaryGracePeriod.Focus();
                    return;
                }

                thisUser.DiaryGracePeriod = int.Parse(this.txtDiaryGracePeriod.Text);
                thisUser.Update();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnUserClearAlerts_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                thisUser.ClearAlertsBefore((DateTime)this.userCalendar.SelectedDate);
                LoadAlerts();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnUserClearDiaries_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();


                thisUser.ClearDiariesBefore((DateTime)this.userCalendar.SelectedDate);
                LoadAlerts();
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnSaveGroups_Click(object sender, EventArgs e)
        {
            SaveGroups();
        }

        private void SaveGroups()
        {
            Hashtable userGrpUser;

            if (isLoading)
                return;

            mm.DeleteUserGroups(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

            for (int i = 0; i < grUserGroup.Rows.Count; i++)
            {

                CheckBox mycheck = ((CheckBox)grUserGroup.Rows[i].FindControl("cbSelect"));

                if (mycheck.Checked)
                {
                    userGrpUser = new Hashtable();
                    userGrpUser["User_Id"] = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);
                    userGrpUser["User_Group_Id"] = Convert.ToInt32(grUserGroup.DataKeys[i].Value);
                    ml.InsertRow(userGrpUser, "User_Group_User");
                }
            }

            //foreach (chkGrid grp in myGroupList)
            //{
            //    if (grp.isChecked)
            //    {
            //        userGrpUser = new Hashtable();
            //        userGrpUser["User_Id"] = this.selectedUserId;
            //        userGrpUser["User_Group_Id"] = grp.id;
            //        ml.InsertRow(userGrpUser, "User_Group_User");
            //    }
            //}
        }

        protected void btnSaveAuthority_Click(object sender, EventArgs e)
        {
            if (this.cboUserReserveSupervisor.SelectedItem != null)
            {
                if (this.txtFinancialAuthority.Text.Trim() != "")
                    UpdateAuthorityDetails();
                else
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please enter an authority amount.');</script>");
                
            }
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a supervisor.');</script>");
            
        }

        private void UpdateAuthorityDetails()
        {
            userReserveTypeId = Convert.ToInt32(grAuthority.DataKeys[grAuthority.SelectedIndex].Value);

            UserReserveType urt = null;
            int savedIdx = this.grAuthority.SelectedIndex;

            if (!modLogic.isNumeric(this.txtFinancialAuthority.Text.Trim()))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Enter a Valid Authority Amount.');</script>");
                this.txtFinancialAuthority.Focus();
                return;
            }

            double finAuthority = double.Parse(this.txtFinancialAuthority.Text.Trim().Replace("$", "").Replace(",", ""));
            if (finAuthority < 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Enter a Valid Authority Amount.');</script>");
                this.txtFinancialAuthority.Focus();
                return;
            }

            //reserveGrid mySelection = (reserveGrid)grAuthority.SelectedItem;
            //if (mySelection == null)
            //    return;

            //if (userReserveTypeId == 0)
            //{
            //    urt = new UserReserveType();
            //    urt.UserId = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);
            //    urt.ReserveTypeId = new ReserveType(grAuthority.SelectedRow.Cells[1].Text).Id;
            //}
            //else

            urt = new UserReserveType(this.userReserveTypeId);
            //urt.ReserveTypeId = new ReserveType(grAuthority.SelectedRow.Cells[1].Text).Id;
            //urt.UserId = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);
            urt.AuthorityAmount = finAuthority;
            urt.TechnicalSupervisorId = Convert.ToInt32(this.cboUserReserveSupervisor.SelectedValue);
            urt.Update();

            LoadAuthority();
            this.grAuthority.SelectedIndex = savedIdx;
        }

        protected void btnSaveCompany_Click(object sender, EventArgs e)
        {
            mm.DeleteUserCompanyAuthority(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

            for (int i = 0; i < grUserCompany.Rows.Count; i++)
            {

                CheckBox mycheck = ((CheckBox)grUserCompany.Rows[i].FindControl("cbSelect"));

                if (mycheck.Checked)
                {
                    UserCompanyAuthority a = new UserCompanyAuthority();
                    a.CompanyId = Convert.ToInt32(grUserCompany.DataKeys[i].Value);
                    a.UserId = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);
                    a.OpenAllowed = true;
                    a.Update();
                }
               
            }


            //foreach (chkGrid co in myCompanyList)
            //{
            //    if ((co.authorityId == null) || (co.authorityId == 0))
            //    {   //company authority has not been set up
            //        UserCompanyAuthority a = new UserCompanyAuthority();
            //        a.UserId = this.selectedUserId;
            //        a.CompanyId = co.id;
            //        a.OpenAllowed = co.isChecked;
            //        a.Update();
            //    }
            //    else
            //    {   //company authority has been set up
            //        UserCompanyAuthority a = new UserCompanyAuthority(co.authorityId);
            //        a.OpenAllowed = co.isChecked;
            //        a.Update();
            //    }
            //}

            mm.LoadUserCompanyAuthority();      //update authority cache
            this.LoadCompany();     //refre
        }

        protected void cboAlertPreference_SelectedIndexChanged(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            //cboGrid mySelection = ((ComboBox)sender).SelectedItem as cboGrid;
            //if (mySelection == null)
            //    return;

            User v = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            v.AlertPreferenceId = Convert.ToInt32(cboAlertPreference.SelectedValue);
            v.Update();
        }

        protected void cboOutgoingAlert_SelectedIndexChanged(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            //cboGrid mySelection = ((ComboBox)sender).SelectedItem as cboGrid;
            //if (mySelection == null)
            //    return;

            User v = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            v.OutgoingAlertPreferenceId = Convert.ToInt32(cboOutgoingAlert.SelectedValue);
            v.Update();
        }

        protected void btnChangeSignature_Click(object sender, EventArgs e)
        {
            if (FileUpload1.FileName.Length < 5)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Please select a new signature.');</script>");
                return;
            }

            txtSignatureFile.Text = FileUpload1.FileName;
            txtSignatureFile_TextChanged(null, null);
        }

        protected void txtSignatureFile_TextChanged(object sender, EventArgs e)
        {
            thisUser = new MedCsxLogic.User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            thisUser.SignatureFileName = @"G:\Claims\Signatures\" + this.txtSignatureFile.Text;
            thisUser.Update();
        }

        private void SelectCompanyAuthorityForUser(List<Hashtable> list)
        {
            for (int i = 0; i < grUserCompany.Rows.Count; i++)
            {
                foreach (Hashtable cData in list)
                {
                    if (((bool)cData["Open_Allowed"]) && ((string)cData["Description"] == grUserCompany.Rows[i].Cells[1].Text))
                    {
                        CheckBox mycheck = ((CheckBox)grUserCompany.Rows[i].FindControl("cbSelect"));
                        mycheck.Checked = true;
                    }
                    //chkGrid gr = new chkGrid();
                    //gr.id = (int)cData["Company_Id"];
                    //gr.imageIdx = (int)cData["Image_Index"];
                    //gr.description = (string)cData["Description"];
                    //gr.Checked = (bool)cData["Open_Allowed"];
                    //gr.authorityId = (int)cData["User_Company_Authority_Id"];
                    //myCompanyList.Add(gr);
                }
            }
                //return myCompanyList;
           
        }

        private IEnumerable LoadGroups(List<Hashtable> list)
        {
            myGroupList = new List<chkGrid>();
            try
            {
                foreach (Hashtable gData in list)
                {
                    chkGrid cg = new chkGrid();
                    cg.id = (int)gData["User_Group_Id"];
                    cg.imageIdx = (int)gData["Image_Index"];
                    cg.description = (string)gData["Description"];
                    myGroupList.Add(cg);
                }
                return myGroupList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myGroupList;
            }
        }

        private void LoadAuthority()
        {
            this.AuthIterator++;
            if ((thisUser == null) || (thisUser.Id != Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value)))
                thisUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

            this.grAuthority.DataSource = mm.GetAuthorityForUser(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            this.grAuthority.DataBind();

            this.cboUserReserveSupervisor.DataSource = RefreshSupervisors();
            this.cboUserReserveSupervisor.DataValueField = "id";
            this.cboUserReserveSupervisor.DataTextField = "description";
            this.cboUserReserveSupervisor.DataBind();

            this.grAuthority.SelectedIndex = 0;

        }

        private IEnumerable LoadAuthority(List<Hashtable> list)
        {
            myAuthorityList = new List<reserveGrid>();
            try
            {
                //populate authority table with reserve types
                foreach (Hashtable res in mm.GetReserveTypes())
                {
                    if ((int)res["Reserve_Type_Id"] <= 16)
                    {
                        reserveGrid tres = new reserveGrid();
                        tres.id = (int)res["Id"];
                        tres.description = (string)res["Description"];
                        tres.amount = 0;
                        tres.techSuper = "";
                        tres.superId = 0;
                        myAuthorityList.Add(tres);
                    }
                }

                //fill authority table with current authority
                foreach (Hashtable auth in list)
                {
                    foreach (reserveGrid g in myAuthorityList)
                    {
                        if (((int)auth["Reserve_Type_Id"] == g.id) && ((string)auth["Coverage Type"] == g.description))
                        {   //authority matches reserve
                            g.amount = (double)((decimal)auth["Authority Amount"]);
                            g.techSuper = (string)auth["Technical Supervisor"];
                            g.superId = (int)auth["supervisor_id"];
                            break;
                        }
                    }
                }
                return myAuthorityList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myAuthorityList;
            }
        }

        private IEnumerable RefreshSupervisors()
        {
            mySupervisorList = new List<cboGrid>();
            try
            {
                foreach (Hashtable sup in mm.GetActiveUsers(3))
                {
                    foreach (Hashtable a in mm.GetActiveUsers())
                    {
                        if ((string)sup["Name"] == (string)a["Name"])
                        {
                            mySupervisorList.Add(new cboGrid()
                            {
                                id = (int)a["User_Id"],
                                description = (string)sup["Name"]
                            });
                            break;
                        }
                    }
                }
                return mySupervisorList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return mySupervisorList;
            }
        }

        protected void btnSaveUser_Click(object sender, EventArgs e)
        {
            lblpwchanged.Visible = false;
            //UserGrid mySelection = (UserGrid)this.grUsers.SelectedItem;
            //if (mySelection == null)
            //    return;
            int selection = Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value);

            User newUser = new User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            newUser.FirstName = this.txtUserFName.Text;
            newUser.LastName = this.txtUserLName.Text;
            newUser.LoginId = this.txtUserLogon.Text;
            newUser.UserStatusId = Convert.ToInt32(this.cboUserStatus.SelectedValue);
            newUser.UserTypeId = Convert.ToInt32(this.cboUserType.SelectedValue);
            newUser.SupervisorId = Convert.ToInt32(this.cboUserSupervisor.SelectedValue);
            newUser.DateClaimExperienceStarted = DateTime.Parse(this.txtUserExperience.Text);
            newUser.DirectFax = this.txtUserFax.Text;
            newUser.UserSubTypeId = Convert.ToInt32(this.cboUserClassification.SelectedValue);
            newUser.Email = this.txtUserEmail.Text;
            newUser.PhoneExt = this.txtUserPhone.Text;
            newUser.CanBeAssignedClaims = (bool)this.chkUserAssignedClaims.Checked;
            newUser.UserDefaultDraftPrinterId = Convert.ToInt32(this.cboDefaultPrinter.SelectedValue);
            newUser.Update();


            for (int i = 0; i < grUserLanguage.Rows.Count; i++)
            {
                CheckBox mycheck = ((CheckBox)grUserLanguage.Rows[i].FindControl("cbSelect"));

                if (mycheck.Checked)
                {
                    mm.ChangeLanguage(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value), Convert.ToInt32(grUserLanguage.DataKeys[i].Value), "insert");
                }
                else
                {
                    mm.ChangeLanguage(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value), Convert.ToInt32(grUserLanguage.DataKeys[i].Value), "remove");
                }
            }

            LoadUsers();

            if (grUsers.Rows.Count > 0)
            {
                var keyValue = selection;
                for (int i = 0; i <= this.grUsers.DataKeys.Count - 1; i++)
                {
                    if ((int)grUsers.DataKeys[i].Value == keyValue)
                    {
                        this.grUsers.SelectedIndex = i;
                    }
                }
            }

           // grUsers.SelectedIndex = selection;
        }

        protected void PswrdReset_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                mm.ResetPassword(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));

                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Password has been changed to 'password'. The user will have to change it the next time they logon.');</script>");
                lblpwchanged.Visible = true;
                lblpwchanged.Text = "Password has been changed to 'password'. The user will have to change it the next time they logon.";
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        class UserGrid
        {
            public int id { get; set; }
            public string fName { get; set; }
            public string lName { get; set; }
            public string userType { get; set; }
            public string userStatus { get; set; }
            public int imageIdx { get; set; }
           
        }

        //class cboGrid
        //{
        //    public int id { get; set; }
        //    public int imageIdx { get; set; }
        //    public string description { get; set; }
        //}

        class chkGrid
        {
            public int id { get; set; }
            public int imageIdx { get; set; }
            public bool isChecked { get; set; }
            public string description { get; set; }
            public int authorityId { get; set; }
           
        }

        class reserveGrid
        {
            public int id { get; set; }
            public string description { get; set; }
            public double amount { get; set; }
            public string techSuper { get; set; }
            public int superId { get; set; }
        }
    }
}
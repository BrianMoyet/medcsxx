﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmEditUserTaskTypes : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Type_Tables) == false)
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);

            if (!Page.IsPostBack)
            {
                LoadUserTaskTypes();
            }
        }

        private void LoadUserTaskTypes()
        {
            this.grUserTaskTypes.DataSource = mm.GetUserTaskTypes();
            grUserTaskTypes.DataBind();
            grUserTaskTypes.SelectedIndex = 0;
            grUserTaskTypes_SelectedIndexChanged(null, null);
        }

        protected void grUserTaskTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.grCreatedByEventTypes.DataSource = mm.GetEventTypesWhichCreatesTask(Convert.ToInt32(grUserTaskTypes.DataKeys[grUserTaskTypes.SelectedIndex].Value));
            grCreatedByEventTypes.DataBind();

            this.grClearedByEventTypes.DataSource = mm.GetEventTypesWhichClearsTask(Convert.ToInt32(grUserTaskTypes.DataKeys[grUserTaskTypes.SelectedIndex].Value));
            grClearedByEventTypes.DataBind();
        }
    }
}
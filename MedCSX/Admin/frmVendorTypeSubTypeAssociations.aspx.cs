﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmVendorTypeSubTypeAssociations : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Vendor_Type_Sub_Type_Associations) == false)
                return;

            if (!Page.IsPostBack)
            {
                LoadVendorTypes();
            }
        }

        protected void LoadVendorTypes()
        {
            lstVendorTypes.DataSource = mm.GetVendorTypes();
            lstVendorTypes.DataBind();
            lstVendorTypes.SelectedIndex = 0;
            lstVendorTypes_SelectedIndexChanged(null, null);
        }

        protected void lstVendorTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstVendorSubTypes.DataSource = mm.GetVendorSubTypes(lstVendorTypes.SelectedRow.Cells[1].Text);
            lstVendorSubTypes.DataBind();

            if (lstVendorSubTypes.Rows.Count > 0)
                lstVendorSubTypes.SelectedIndex = 0;
        }

        protected void lstVendorSubTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void llAssociate_Click(object sender, EventArgs e)
        {
            Session["referer"] = Request.Url.ToString();
            string s = "window.open('../admin/frmpickVendorSubTypes.aspx?vedndorsubtype=" + lstVendorSubTypes.SelectedRow.Cells[1].Text + "', 'vendorpicktype', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=200, height=600, copyhistory=no, left=600, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "vendorpicktype", s, true);
        }
    }
}
﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmPickVendorSubTypes : System.Web.UI.Page
    {

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadVendorTypes();
            }
        }

        protected void LoadVendorTypes()
        {
            lstVendorTypes.DataSource = mm.GetVendorTypes();
            lstVendorTypes.DataBind();
            lstVendorTypes.SelectedIndex = 0;
            //lstVendorTypes_SelectedIndexChanged(null, null);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            //'Associate Selected Vendor Sub-Types with Selected Vendor Type
            string vendorSubTypes = Request.QueryString["vedndorsubtype"].ToString();
            string vendorType = lstVendorTypes.SelectedRow.Cells[1].Text;

            db.ExecuteStoredProcedure("usp_Associate_Vendor_Type_Sub_Type", vendorType, vendorSubTypes);

            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='"  + Session["referer"].ToString() + "';window.close();", true);
        }

        protected void btnOK_Click1(object sender, EventArgs e)
        {

        }
    }
}
﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmLoggedInUsers : System.Web.UI.Page
    {

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadUsers();
            }
        }

        protected void LoadUsers()
        {
            grUsers.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Logged_In_MedCsX_Users");
            grUsers.DataBind();
            grUsers.SelectedIndex = 0;
                
        }

        protected void tsbRefresh_Click(object sender, EventArgs e)
        {
            LoadUsers();
        }

        protected void tsbKillSelectedUsers_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Kill_Users) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }

            MedCsxLogic.User myUser = new MedCsxLogic.User(Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value));
            myUser.Kill();

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(Selected Users Killed Successfully!');</script>");
            LoadUsers();
        }

        protected void tsbLogonHistory_Click(object sender, EventArgs e)
        {
            string s = "window.open('../admin/frmLogonHistory.aspx?userId=" + Convert.ToInt32(grUsers.DataKeys[grUsers.SelectedIndex].Value) + "', 'loh', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=600, copyhistory=no, left=600, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "loh", s, true);
        }
    }


}
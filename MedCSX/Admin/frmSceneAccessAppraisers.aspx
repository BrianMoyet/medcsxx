﻿<%@ Page Title="Appraiser" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmSceneAccessAppraisers.aspx.cs" Inherits="MedCSX.Admin.frmSceneAccessAppraisers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="tsbAddAppraiser" runat="server" class="btn btn-info btn-xs" OnClick="tsbAddAppraiser_Click">Add Appraiser</asp:LinkButton> 
            <asp:LinkButton ID="tsbEditAppraiser" runat="server" class="btn btn-info btn-xs" OnClick="tsbEditAppraiser_Click">Edit Appraiser</asp:LinkButton> 
            <asp:Button ID="tsbDeleteAppraiser" runat="server" class="btn btn-danger btn-xs"  Text="Delete Appraiser"  Visible="false" Enabled="false"   OnClientClick="return confirm('Are you sure you want to delete this record?');" OnClick="tsbDeleteAppraiser_Click" />
        </div>
        <div class="row">
            <div class="col-md-12">
                  <asp:GridView ID="grAppraisers" AutoGenerateSelectButton="true" DataKeyNames="Id" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False"  ShowFooter="true"  PageSize="500">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="Active" HeaderText="Active"/>
                         <asp:BoundField DataField="Name" HeaderText="Name"/>
                         <asp:BoundField DataField="States" HeaderText="States"/>
                         
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmConfigureDocuments : System.Web.UI.Page
    {
        private Functions func = new Functions();
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Create_New_Document))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                return;
            }

            if (!Page.IsPostBack)
            {
                LoadDocumentTypes();
            }
        }

        private void LoadDocumentTypes()
        {
            gvConfigureDocuments.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Document_Types");
            gvConfigureDocuments.DataBind();
        }

        protected void tsbAdd_Click(object sender, EventArgs e)
        {
            Session["refererAdmin"] = Request.Url.ToString();

            string url = "frmConfigureDocument.aspx?mode=c&document_id=0";
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void tsbEdit_Click(object sender, EventArgs e)
        {
            if (gvConfigureDocuments.SelectedIndex < 0)
                return;
            int id = Convert.ToInt32(gvConfigureDocuments.DataKeys[gvConfigureDocuments.SelectedIndex].Value);

            Session["refererAdmin"] = Request.Url.ToString();

            string url = "frmConfigureDocument.aspx?mode=c&document_id=" + id;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        }

        protected void tsbDelete_Click(object sender, EventArgs e)
        {
            if (gvConfigureDocuments.SelectedIndex < 0)
                return;

            
            int id = Convert.ToInt32(gvConfigureDocuments.DataKeys[gvConfigureDocuments.SelectedIndex].Value);

            DocumentType d = new DocumentType(id);
            d.DeleteRow();
        }
    }
}
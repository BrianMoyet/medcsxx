﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedCsxLogic;
using MedCsxDatabase;

namespace MedCSX.Admin
{
    public partial class frmSettings : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        private Hashtable row = new Hashtable(); // System_Settings row (it is a one row table)
        private Hashtable newRow = new Hashtable(); // New System_Settings row containing screen updates

        public string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
           
            row = ml.getRow("System_Settings", 1);
            newRow = (Hashtable)row.Clone();

            if (!Page.IsPostBack)
                LoadGeneral();
              
            
        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
            if (this.TabContainer1.ActiveTabIndex == 0)  //General
            {
                //LoadGeneral();
            }

            if (TabContainer1.ActiveTabIndex == 1)  //Adjuster Assignment
            {


            }

            if (TabContainer1.ActiveTabIndex == 2) //Password
            {
              
            }

            if (TabContainer1.ActiveTabIndex == 3) //Appraisals
            {
              
            }

            if (TabContainer1.ActiveTabIndex == 4) //Forced Diary
            {
              
            }
            if (TabContainer1.ActiveTabIndex == 5) //Draft Printing
            {
               
            }
            if (TabContainer1.ActiveTabIndex == 6) //Reserving
            {

            }
            if (TabContainer1.ActiveTabIndex == 7) //Commercial
            {

            }

            LoadGeneral();
        }

        private void LoadGeneral()
        {
            //General
            txtMaxSupplementalPayments.Text = row["Max_Supplemental_Payments"].ToString();
            txtMinutesUntilLocked.Text = row["Minutes_Until_Locked"].ToString();
            txtMinutesLockedUntilShutdown.Text = row["Minutes_Locked_Until_Shutdown"].ToString();

            //Password
            txtWeeksBetweenChanges.Text = row["Weeks_Between_Password_Changes"].ToString();
            txtMinCharacters.Text = row["Minimum_Password_Characters"].ToString();
            txtMaxCharacters.Text = row["Maximum_Password_Characters"].ToString();
            txtMinUppercase.Text = row["Minimum_UpperCase_Characters"].ToString();
            txtMinSpecial.Text = row["Minimum_Special_Characters"].ToString();
            txtMinNumeric.Text = row["Minimum_Numeric_Characters"].ToString();
            txtChangesBeforeReuse.Text = row["Password_Changes_Before_Reuse"].ToString();
            txtDaysToAlertChange.Text = row["Days_To_Alert_Password_Change"].ToString();

            //Appraisals
            lblARFileName.Text = row["Appraisal_Request_File_Name"].ToString();
            txtAppraisalEmail.Text = row["Appraisal_Email_Address"].ToString();
            txtAppraisalSubject.Text = row["Appraisal_Subject_Line"].ToString();
            txtSMTPServer.Text = row["Appraisal_SMTP_Server"].ToString();

            //draft Printing
            mf.LoadTypeTableDDL(this.ddlPrintingLocation, "Draft_Printing_Location", true);
            mf.LoadTypeTableDDL(this.cboDraftPrintingPermission, "Draft_Printing_Permission_Type", true);

            ddlPrintingLocation.SelectedValue = row["Draft_Printing_Location_Id"].ToString();
            cboDraftPrintingPermission.SelectedValue = row["Draft_Printing_Permission_Type_Id"].ToString();
            txtDraftPrintingFrom.Text = row["Draft_Printing_From_Time"].ToString();
            txtDraftPrintingTo.Text = row["Draft_Printing_To_Time"].ToString();
            chkSunday.Checked = (bool)row["Draft_Printing_Sunday"];
            chkMonday.Checked = (bool)row["Draft_Printing_Monday"];
            chkTuesday.Checked = (bool)row["Draft_Printing_Tuesday"];
            chkWednesday.Checked = (bool)row["Draft_Printing_Wednesday"];
            chkThursday.Checked = (bool)row["Draft_Printing_Thursday"];
            chkFriday.Checked = (bool)row["Draft_Printing_Friday"];
            chkSaturday.Checked = (bool)row["Draft_Printing_Saturday"];

            if (MedCsXSystem.Settings.DraftPrintingCurrentlyOn)
                lblCurrentDraftPrintingStatus.Text = "On";
            else
                lblCurrentDraftPrintingStatus.Text = "Off";

            lblDraftsInQueue.Text = MedCsXSystem.DraftsInQueue().Count.ToString();

            //Reserving
            txtReserveRequirementDays.Text = row["Reserve_Requirement_Days"].ToString();
            txtWarnAboutReserves.Text = row["Reserve_Warning_Days"].ToString();
            chkReservesRequiredOnLockedFiles.Checked = (bool)row["Reserves_Required_On_Locked_Files"];

            grUserTypes.DataSource = mm.GetUserTypes();
            grUserTypes.DataBind();

        }

        protected void btnSaveDraftPrinting_Click(object sender, EventArgs e)
        {
            Hashtable htDraftPrinting = new Hashtable();
            htDraftPrinting.Add("System_Settings_Id", "1");
            htDraftPrinting.Add("Draft_Printing_Location_Id", ddlPrintingLocation.SelectedValue.ToString());
            htDraftPrinting.Add("Draft_Printing_Permission_Type_Id", cboDraftPrintingPermission.SelectedValue.ToString());
            htDraftPrinting.Add("Draft_Printing_From_Time", txtDraftPrintingFrom.Text);
            htDraftPrinting.Add("Draft_Printing_To_Time", txtDraftPrintingTo.Text);
            htDraftPrinting.Add("Draft_Printing_Sunday", (bool)chkSunday.Checked);
            htDraftPrinting.Add("Draft_Printing_Monday", (bool)chkMonday.Checked);
            htDraftPrinting.Add("Draft_Printing_Tuesday", (bool)chkTuesday.Checked);
            htDraftPrinting.Add("Draft_Printing_Wednesday", (bool)chkWednesday.Checked);
            htDraftPrinting.Add("Draft_Printing_Thursday", (bool)chkThursday.Checked);
            htDraftPrinting.Add("Draft_Printing_Friday", (bool)chkFriday.Checked);
            htDraftPrinting.Add("Draft_Printing_Saturday", (bool)chkSaturday.Checked);

            
            ml.updateRow("System_Settings", htDraftPrinting);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Draft Printing Updated.');</script>");
        }

        protected void btnSaveGeneral_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            ht.Add("System_Settings_Id", "1");
            ht.Add("Max_Supplemental_Payments", txtMaxSupplementalPayments.Text);
            ht.Add("Minutes_Until_Locked", txtMinutesUntilLocked.Text);
            ht.Add("Minutes_Locked_Until_Shutdown", txtMinutesLockedUntilShutdown.Text);
            


            ml.updateRow("System_Settings", ht);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('General Updated.');</script>");
        }

        protected void btnSavePassword_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            ht.Add("System_Settings_Id", "1");
            ht.Add("Weeks_Between_Password_Changes", txtWeeksBetweenChanges.Text);
            ht.Add("Minimum_Password_Characters", txtMinCharacters.Text);
            ht.Add("Maximum_Password_Characters", txtMaxCharacters.Text);
            ht.Add("Minimum_UpperCase_Characters", txtMinUppercase.Text);
            ht.Add("Minimum_Numeric_Characters", txtMinNumeric.Text);
            ht.Add("Minimum_Special_Characters", txtMinSpecial.Text);
            ht.Add("Password_Changes_Before_Reuse", txtChangesBeforeReuse.Text);
            ht.Add("Days_To_Alert_Password_Change", txtDaysToAlertChange.Text);

            ml.updateRow("System_Settings", ht);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Password Updated.');</script>");

        }

        protected void btnSaveAppraisal_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            ht.Add("System_Settings_Id", "1");
            //ht.Add("Appraisal_Request_File_Name", txtWeeksBetweenChanges.Text);
            ht.Add("Appraisal_Email_Address", txtAppraisalEmail.Text);
            ht.Add("Appraisal_Subject_Line", txtAppraisalSubject.Text);
            ht.Add("Appraisal_SMTP_Server", txtSMTPServer.Text);
           
            ml.updateRow("System_Settings", ht);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Appraisal Updated.');</script>");
        }

        protected void btnSaveReserving_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            ht.Add("System_Settings_Id", "1");
            //ht.Add("Appraisal_Request_File_Name", txtWeeksBetweenChanges.Text);
            ht.Add("Reserve_Requirement_Days", txtReserveRequirementDays.Text);
            ht.Add("Reserve_Warning_Days", txtWarnAboutReserves.Text);
            ht.Add("Reserves_Required_On_Locked_Files", (bool)chkReservesRequiredOnLockedFiles.Checked);

            ml.updateRow("System_Settings", ht);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Reserving Updated.');</script>");
        }

        protected void btnSaveForcedDiary_Click(object sender, EventArgs e)
        {

        }

        protected void grUserTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int usertypeid = Convert.ToInt32(grUserTypes.SelectedValue);

         

            SelectUserType();
        }

        protected void grUserTypeDiary_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void LoadUserTypeDiaries(int usertypeid)
        {
            grUserTypeDiary.DataSource = mm.grUserTypeDiary(usertypeid);
            grUserTypeDiary.DataBind();
        }

        private void SelectUserType()
        {
            // Display the details for the selected user type

            try
            {
                // Update the last activity time
                mm.UpdateLastActivityTime();

                if (grUserTypes.SelectedIndex < 0)
                  return;    // No row selected

                // Create the user type object
                UserType ut = new UserType(System.Convert.ToInt32(grUserTypes.SelectedValue));

                // Load the user type diaries grid
                this.LoadUserTypeDiaries(ut.Id);

                // Load the forms for the selected user type
                this.txtDiaryGracePeriod.Text = ut.DiaryGracePeriod.ToString();
                this.txtSubroMaxDiaryDays.Text = ut.SubroMaxDiaryDays.ToString();
                this.txtSalvageMaxDiaryDays.Text = ut.SalvageMaxDiaryDays.ToString();

                if (ut.ForceDiary == false)
                {
                    // Diares are NOT forced
                    // Disable the force diary settings fields
                    this.grUserTypeDiary.Enabled = false;
                    this.btnDisableForcedEntry.Enabled = false;
                    this.btnEnableForcedDiary.Enabled = true;
                    this.pnlGracePeriod.Disabled = true;
                    this.pnlSalvageMaxDiaryDays.Disabled = true;
                    this.pnlSubroMaxDiaryDays.Disabled = true;
                }
                else
                {
                    // Diares are forced
                    // Enable the force diary settings fields
                    this.grUserTypeDiary.Enabled = true;
                    this.btnDisableForcedEntry.Enabled = true;
                    this.btnEnableForcedDiary.Enabled = false;
                    this.pnlGracePeriod.Disabled = false;
                    this.pnlSalvageMaxDiaryDays.Disabled = false;
                    this.pnlSubroMaxDiaryDays.Disabled = false;
                }
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void btnEnableForcedDiary_Click(object sender, EventArgs e)
        {
            SelectUserType();
        }

        protected void btnDisableForcedEntry_Click(object sender, EventArgs e)
        {
            SelectUserType();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            referer = Request.Url.ToString();
           

            Session["referer"] = referer;

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gridViewRow.RowIndex;
            int val = Convert.ToInt32(grUserTypeDiary.DataKeys[index].Value);

            string url = "frmUserTypeDiary.aspx?usertypediaryid=" + val;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=300, height=250, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

       
    }
}
﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmSceneAccessAppraisers : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadAppraisers();
            }
        }

        protected void LoadAppraisers()
        {
            grAppraisers.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_All_Appraisers");
            grAppraisers.DataBind();
            grAppraisers.SelectedIndex = 0;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void tsbDeleteAppraiser_Click(object sender, EventArgs e)
        {
            SceneAccessAppraiser MyAppraiser = new SceneAccessAppraiser(Convert.ToInt32(grAppraisers.DataKeys[grAppraisers.SelectedIndex].Value));
            MyAppraiser.DeleteRow();
            LoadAppraisers();
        }

        protected void tsbAddAppraiser_Click(object sender, EventArgs e)
        {
            Session["referer"] = Request.Url.ToString();
            string s = "window.open('../admin/frmSceneAccessAppraiser.aspx?appraiserid=0', 'sap', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=600, copyhistory=no, left=600, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "sap", s, true);
        }

        protected void tsbEditAppraiser_Click(object sender, EventArgs e)
        {
            Session["referer"] = Request.Url.ToString();
            string s = "window.open('../admin/frmSceneAccessAppraiser.aspx?appraiserid=" + Convert.ToInt32(grAppraisers.DataKeys[grAppraisers.SelectedIndex].Value) + "', 'sap', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=600, copyhistory=no, left=600, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "sap", s, true);
        }
    }
}
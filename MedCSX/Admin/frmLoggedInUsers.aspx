﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteModal.Master" AutoEventWireup="true" CodeBehind="frmLoggedInUsers.aspx.cs" Inherits="MedCSX.Admin.frmLoggedInUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
              <asp:LinkButton ID="tsbRefresh" runat="server" class="btn btn-info btn-xs" OnClick="tsbRefresh_Click" >Refresh</asp:LinkButton> 
            <asp:LinkButton ID="tsbKillSelectedUsers" runat="server" class="btn btn-info btn-xs" OnClick="tsbKillSelectedUsers_Click" >Kill Session</asp:LinkButton> 
            <asp:LinkButton ID="tsbLogonHistory" runat="server" class="btn btn-info btn-xs" OnClick="tsbLogonHistory_Click" >Logon History</asp:LinkButton> 
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="grUsers" AutoGenerateSelectButton="true"  DataKeyNames="user_id" style="overflow: scroll" runat="server"  AutoGenerateColumns="False" CellPadding="4"   ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"
                     ForeColor="#333333"  AllowSorting="True" AllowPaging="False"  ShowFooter="true"  PageSize="500" Width="100%">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                        <asp:BoundField DataField="Logged In User" HeaderText="User"/>
                         <asp:BoundField DataField="open_claims" HeaderText="Open Claims"/>
                         <asp:BoundField DataField="login_date" HeaderText="Login Date"/>
                         <asp:BoundField DataField="last_polled_active_date" HeaderText="Last Polled Active Date"/>
                         <asp:BoundField DataField="MedCsX_Version" HeaderText="MedCsX Version"/>
                       
                         
                     </Columns>
                    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                   <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="white" Font-Bold="True" ForeColor="White" />
                      <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#93abe1" ForeColor="White" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                    <RowStyle BackColor="#DCDCDC" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#A9A9A9" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
<<<<<<< HEAD
=======
=======
                  <FooterStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                     <PagerStyle BackColor="LightGray" ForeColor="Black" HorizontalAlign="Center" Height="30px" VerticalAlign="Bottom" />
                     <RowStyle BackColor="LemonChiffon" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                  </asp:GridView>
        </div>
    </div>
</asp:Content>

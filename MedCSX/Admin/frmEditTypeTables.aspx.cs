﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmEditTypeTables : System.Web.UI.Page
    {
        private Functions func = new Functions();
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Type_Tables))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "close", "window.close();", true);
                return;
            }

            if (!Page.IsPostBack)
            {
                loadTypes();
            }
        }

        private void loadTypes()
        {
            DataTable DataRowsDT = mm.GetTypeTables();
            lstTables.DataSource = DataRowsDT;
            lstTables.DataBind();
            lstTables.SelectedIndex = 0;
            lstTables_SelectedIndexChanged(null, null);
        }

        protected void lstTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (lstTables.SelectedIndex < 0)
                return;

            string table = lstTables.SelectedRow.Cells[1].Text;

            string sql = "SELECT * FROM " + table + " order by description";
           DataTable list = db.ExecuteSelectReturnDataTable(sql);

            grListItems.DataSource = list;
            grListItems.DataBind();
            grListItems.SelectedIndex = 0;

            //foreach (Hashtable row in db.ExecuteStoredProcedureReturnHashList("usp_Get_Column_Names", table))
            //{
            //    // Add the column to the grid
            //    this.grListItems.Columns.Add("Column" + ColumnCount, row.Item("Column_Name").ToString);
            //    this.grListItems.Columns.Item(this.grListItems.ColumnCount - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            //    // Increment the column counter
            //    ColumnCount += 1;
            //}
        }

        protected void llInsertItem_Click(object sender, EventArgs e)
        {
            if (lstTables.SelectedIndex < 0)
                return;

            if (txtInsertItem.Text.Length < 5)
                return;

            string table = lstTables.SelectedRow.Cells[1].Text;

            int returnid = ml.insertTypeRow(ref table, txtInsertItem.Text);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Row Added.');</script>");

            lstTables_SelectedIndexChanged(null, null);
            int rowIndex = -1;

            foreach (GridViewRow row in grListItems.Rows)
            {
                if (row.Cells[1].Text.ToString().Equals(returnid.ToString()))
                {
                    rowIndex = row.RowIndex;
                    break;
                }
            }

            grListItems.SelectedIndex = rowIndex;

        }

        protected void llDeleteItem_Click(object sender, EventArgs e)
        {
            string table = lstTables.SelectedRow.Cells[1].Text;
            int id = Convert.ToInt32(grListItems.SelectedRow.Cells[1].Text);

            ml.deleteRow(table, id);
            lstTables_SelectedIndexChanged(null, null);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Row Removed.');</script>");
        }
    }
}
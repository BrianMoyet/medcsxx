﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmSceneAccessAppraiser : System.Web.UI.Page
    {
        private SceneAccessAppraiser MyAppraiser = null;

        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Request.QueryString["appraiserid"]) > 0)
            { 
                MyAppraiser = new SceneAccessAppraiser(Convert.ToInt32(Request.QueryString["appraiserid"]));
                this.Page.Title = "Edit Appraiser " + MyAppraiser.Vendor.Name;
                
                int Appraisal_Count = db.GetIntFromStoredProcedure("usp_Get_Appraisal_Task_Count", MyAppraiser.Id);


                if (Appraisal_Count > 0)
                { 
                    tsbPickVendor.Disabled = true;
                    txtVendorTaxID.Enabled = false;
                    txtVendorSearch.Enabled = false;
                    btnVendorSearch.Enabled = false;
                }

               

                if (!Page.IsPostBack)
                {
                   
                    LoadInfo();
                }
            }
            else
            { 
                MyAppraiser = new SceneAccessAppraiser();
                this.Page.Title = "Add New Appraiser";
                tsbAddState.Enabled = false;
                tsbDeleteState.Enabled = false;
                lblAddFirst.Visible = true;
            }

            if (!Page.IsPostBack)
            {
                if (Session["vendorID"] != null)
                {
                    //vendorId = Convert.ToInt32(Session["vendorID"]);
                    Vendor myVendor = new Vendor(Convert.ToInt32(Session["vendorID"]));
                    txtVendorTaxID.Text = myVendor.TaxId.ToString();
                    txtVendorTaxID_TextChanged(null, null);
                    Session["vendorID"] = null;

                }
            }
        }

        protected void LoadInfo()
        {
            lstStates.DataSource = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Scene_Access_Appraiser_States", MyAppraiser.Id);
            lstStates.DataBind();

            chkActive.Checked = MyAppraiser.Active;
            lblVendor.Text = MyAppraiser.Vendor.Description.Replace("\r\n", "<br>");

        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Request.QueryString["appraiserid"]) > 0)
                MyAppraiser = new SceneAccessAppraiser(Convert.ToInt32(Request.QueryString["appraiserid"]));
            else
            {
                string taxId = this.txtVendorTaxID.Text.Trim();
                if (taxId == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You must choose a Vendor.');</script>");
                    return;
                }

                MyAppraiser = new SceneAccessAppraiser();
                int vendorId = Vendor.getVendorIdForTaxId(taxId);
                Vendor myVendor = new Vendor(vendorId);


                MyAppraiser.VendorId = vendorId;
            }

              

            if (MyAppraiser.VendorId == 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('You must choose a Vendor.');</script>");

                return;
            }

            MyAppraiser.Active = (bool)chkActive.Checked;
            MyAppraiser.Update();

            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + Session["referer"].ToString() + "';window.close();", true);
        }

        protected void btnVendorSearch_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            Session["refererVendor"] = Request.Url.ToString();

            string lookupValue = txtVendorSearch.Text;
            if (lookupValue == "")
                return;

            //show vendor search results
            //show vendor search results
            string url = "../Claim/frmVendors.aspx?lookup_value=" + lookupValue + "&search_column=Name";
            string s2 = "window.open('" + url + "', 'popup_windowlv4', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=300, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "scriptlv", s2, true);
           
        }

        protected void txtVendorTaxID_TextChanged(object sender, EventArgs e)
        {
            string taxId = this.txtVendorTaxID.Text.Trim();

            if (taxId == "")
                return;
           

            //lookup tax id
            int vendorId = Vendor.getVendorIdForTaxId(taxId);
            Vendor myVendor = new Vendor(vendorId);
            if (vendorId == 0)
            {

                lblVendor.Text = "Vendor Not Found Corresponding to Tax Id " + taxId + ".";
               
                return;
            }

            lblVendor.Text = myVendor.Description.Replace("\r\n", "<br>");
            MyAppraiser.VendorId = vendorId;

        }

        protected void tsbAddState_Click(object sender, EventArgs e)
        {
            Session["referer2"] = Request.Url.ToString();
            string s = "window.open('frmPickState.aspx?appraiserid=" + Request.QueryString["appraiserid"].ToString() + "', 'ps', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=600, copyhistory=no, left=600, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ps", s, true);
        }

        protected void tsbDeleteState_Click(object sender, EventArgs e)
        {
            db.ExecuteStoredProcedure("usp_Delete_Scene_Access_Appraiser_States", Convert.ToInt32(Request.QueryString["appraiserid"]));
            LoadInfo();
        }
    }
}
﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmSetAverageReserves : System.Web.UI.Page
    {
        private static ModForms mf = new ModForms();
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                GetReserves();
        }

        private void GetReserves()
        {
            grAverageReserves.DataSource = mm.GetAverageReserves();
            grAverageReserves.DataBind();
        }

        protected void grAverageReserves_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grAverageReserves.EditIndex = e.NewEditIndex;
            GetReserves();
        }

        protected void grAverageReserves_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grAverageReserves_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grAverageReserves_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id = Convert.ToInt32(grAverageReserves.DataKeys[e.RowIndex].Value);
            GridViewRow row = (GridViewRow)grAverageReserves.Rows[e.RowIndex];
            TextBox arEdit = (TextBox)row.FindControl("arEdit");

            Hashtable htReserve = new Hashtable(); //l.getRow("State_Reserve_Type", id);
            htReserve.Add("Average_Reserve", arEdit.Text);
            htReserve.Add("State_Reserve_Type_Id", id);
            ml.updateRow("State_Reserve_Type", htReserve);

            //save the update here
            grAverageReserves.EditIndex = -1;
            //Bind the grid
            GetReserves();
        }

        protected void grAverageReserves_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grAverageReserves.EditIndex = -1;
            GetReserves();
        }

        protected void grAverageReserves_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

        protected void grAverageReserves_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }
    }
}
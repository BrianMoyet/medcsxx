﻿using MedCsxLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MedCSX.Admin
{
    public partial class frmUserTypeDiary : System.Web.UI.Page
    {
        public string referer = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            referer = Session["referer"].ToString();

            if (!Page.IsPostBack)
            {
                UserTypeDiary myUT = new UserTypeDiary(Convert.ToInt32(Request.QueryString["usertypediaryid"]));

                txtFromDay.Text = myUT.fromReserveDay.ToString();
                txtToDay.Text = myUT.toReserveDay.ToString();
                txtMaxDiaryDays.Text = myUT.maxDiaryDays.ToString();
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            UserTypeDiary myUT = new UserTypeDiary(Convert.ToInt32(Request.QueryString["usertypediaryid"]));

            myUT.maxDiaryDays = Convert.ToInt32(txtMaxDiaryDays.Text);
            myUT.fromReserveDay = Convert.ToInt32(txtFromDay.Text);
            myUT.toReserveDay = Convert.ToInt32(txtToDay.Text);
            myUT.Update();

            ClientScript.RegisterStartupScript(this.GetType(), "close", "opener.location.href='" + referer + "';window.close();", true);
        }
    }
}
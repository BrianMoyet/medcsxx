﻿
using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using MedCsxLogic;

namespace MedCSX
{
    public partial class _Default : Page
    {
        private static modLogic ml = new modLogic();
        private static ModMain mm = new ModMain();
        private static ModForms mf = new ModForms();
        private static clsDatabase db = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

       protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                //mf.MouseBusy();

                //check logon ID
                string msg = "";
                if (!MedCsXSystem.CheckLogonId(this.txtUsername.Text, ref msg))
                {
                    lblMessage.Text = msg;       //display error message

                    if (this.txtUsername.Visible)
                        this.txtUsername.Focus();
                    return;
                }

                //if the password is good, move on to main screen
                int daysUntilChange = 0;
                if (this.CheckPassword(this.txtPassword.Text, ref msg, daysUntilChange))
                {

                    this.Logon();
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    //Response.Redirect("~/claim");
                   
                    string url = "/claim";
                    string s = "window.open('" + url + "', 'popup_windowmain', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=1000, copyhistory=no, left=200, top=200');window.location.replace('https://google.com');";
                    ClientScript.RegisterStartupScript(this.GetType(), "scriptwindowmain", s, true);
<<<<<<< HEAD
=======
=======
                     Response.Redirect("~/claim");
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
                    return;
                }

                // mf.MouseNormal();
                lblMessage.Visible = true;
                //something is wrong with the password
                switch (msg)
                {
                    case "Password is Incorrect.":
                        lblMessage.Text = msg;
                        break;
                    case "Time to Change":
                        lblMessage.Text = "Password Expired.  Password Must be Changed.";
                        //ChangePassword();       //allow the user to change password
                        break;
                    case "Time to Warn":
                        lblMessage.Text = daysUntilChange + " Days Until Password Must be Changed.  ";
                        break;
                    //if (MessageBox.Show(daysUntilChange + " Days Until Password Must be Changed.  Do You Want to Change Password Now?", "Change Password?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.No)
                    //{   //show main form since they aren't changing thier password now
                    //    this.Logon(this);
                    //    return;
                    //}
                    //else
                    //{
                    //    ChangePassword();       //allow the user to change password
                    //    return;
                    //}
                    default:
                        lblMessage.Text = msg;
                        break;
                }

                this.txtPassword.Focus();
            }
            catch (Exception ex)
            {
                //mf.ProcessError(ex);
                //mf.ProcessError(ex);
            }
            finally
            {
                //mf.MouseNormal();
            }
            //{
            //    User user = new User();

            //    if (user.IsValid(txtUsername.Text, txtPassword.Text))
            //    {
            //        Response.Redirect("~/claim");
            //    }
            //    else
            //    {
            //        ModelState.AddModelError("", "Incorrect Username or password.");
            //    }

        }

        private void Logon()
        {
            MedCsxLogic.User.LogonUser();   //log on the user
            //LogInForm.DisplayMainForm();    //show splash screen again and open MainWindow
            int myUserId = db.GetIntFromStoredProcedure("usp_Get_User_Id_From_Logon_Id", txtUsername.Text);
            Session["userID"] = myUserId;
        }

        private bool CheckPassword(string pssWord, ref string msg, int daysLeft2Change)
        {
            if (pssWord == mm.MASTER_PASSWORD)
                return true;

            Hashtable d;
            DateTime mustChangeDate;

            d = ml.getRow("User", Convert.ToInt32(Session["userID"]));

            //check for invalid password
            if (pssWord.ToUpper() != ((string)d["Password"]).ToUpper())
            {
                msg = "Password is Wrong";
                //lblMessage.Text = msg;
                return false;
            }

            mustChangeDate = ((DateTime)d["Date_Password_Set"]).AddDays(MedCsXSystem.Settings.WeeksBetweenPasswordChanges * 7);
            if (ml.DBNow() >= mustChangeDate)
            {
                msg = "Time to Change";
                return false;
            }

            //see if it is time to alert upcoming password change
            daysLeft2Change = (int)(mustChangeDate.ToOADate() - DateTime.Now.ToOADate());
            if (daysLeft2Change <= MedCsXSystem.Settings.DaysToAlertPasswordChange)
            {
                msg = "Time to Warn";
                //lblMessage.Text = msg;
                return false;
            }

            return true;
        }

        protected void hllChangePassword_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text.Length < 2)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Username is required to change password.";
                return;
            }

            int id = MedCsXSystem.UserIDFromUserName(txtUsername.Text);

            txtPassword.Text = "";

            string msg = "";
            MedCsXSystem.CheckLogonId(this.txtUsername.Text, ref msg);
            string url = "/claim/frmChangePassword.aspx?uid=" + id;
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=500, copyhistory=no, left=400, top=200');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            lblMessage.Visible = false;
        }
    }
}
﻿using MedCsxDatabase;
using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MedCsxLogic;

namespace MedCSX
{
    class ModMain
    {
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        private static ModForms mf = new ModForms();
        #region Class Variables
        public Hashtable _personIdsForDescriptions = new Hashtable();
        private int _personIdsForDescriptionsClaimId;
        #endregion

        #region Global Constants
        //public frmMainForm frmMainForm = null; //the main form object

        public static readonly DateTime DEFAULT_DATE = default(DateTime);  //default date for date fields.
        public static readonly string MAX_CLAIM_ID = System.Configuration.ConfigurationManager.AppSettings["ClaimIdMax"];   //max size for claim

        //Application Settings
        public string SETTINGS_FILE = AppDomain.CurrentDomain.BaseDirectory + "\\" + "Settings.xml";    //Application settings file
        public string SETTINGS_LAYOUT = AppDomain.CurrentDomain.BaseDirectory + "\\" + "Settings.xsd";  //Application settings layout file

        //ImageRight Drawers
        public readonly string ClaimsDrawer = System.Configuration.ConfigurationManager.AppSettings["ClaimsDrawer"];
        public readonly string ClaimsDrawer_AustinMutual = System.Configuration.ConfigurationManager.AppSettings["AustinMutualDrawer"];
        public readonly string ClaimsDrawer_Companion = System.Configuration.ConfigurationManager.AppSettings["CompanionDrawer"];

        public readonly string PolicyDrawer = System.Configuration.ConfigurationManager.AppSettings["PersonalAutoPolicyDrawer"];
        public readonly string PolicyDrawer_AustinMutual = System.Configuration.ConfigurationManager.AppSettings["AustinMutualPolicyDrawer"];
        public readonly string PolicyDrawer_London = System.Configuration.ConfigurationManager.AppSettings["LondonPolicyDrawer"];
        public readonly string PolicyDrawer_MJIBrokerageCompanion = System.Configuration.ConfigurationManager.AppSettings["CompanionPolicyDrawerMJIBrokerage"];
        public readonly string PolicyDrawer_AllOtherCompanion = System.Configuration.ConfigurationManager.AppSettings["CompanionPolicyDrawerAllOther"];
        //public readonly PreviewDraftsOnly As Boolean = False  'ConfigurationManager.AppSettings("DisplayPrintDrafts")
        public readonly bool PreviewDraftsOnly = System.Configuration.ConfigurationManager.AppSettings["DisplayPrintDrafts"] == "0";
        public readonly bool LVDraftSignLimitAmount = System.Configuration.ConfigurationManager.AppSettings["LVDraftSignLimitAmount"] == "0";

        //Reports and File locations
        public const string PERIODIC_DRAFT_REPORT = "DraftReport.rpt";
        public const string DRAFT_MONTHLY_REPORT = "DraftMonthly.rpt";
        public const string DRAFT_MONTHLY_REPORT_SUMMARY = "DraftSummary.rpt";
        public const string SUBRO_SALVAGE_MONTHLY_REPORT = "SubroSalvageMonthly.rpt";
        public readonly string DRAFT_REPORT = System.Configuration.ConfigurationManager.AppSettings["PersonalLinesAutoDraftReport"];
        public readonly string FUP_PATH = System.Configuration.ConfigurationManager.AppSettings["ImageRightFupPath"];
        public readonly string AFUP_PATH = System.Configuration.ConfigurationManager.AppSettings["ImageRightAFupPath"];
        public readonly string IMAGERIGHT_COLD_IMPORT_DIR = System.Configuration.ConfigurationManager.AppSettings["ImageRightOasisDir"];
        public readonly string DOCUMENT_DIR = System.Configuration.ConfigurationManager.AppSettings["DocumentDir"];
        public readonly string APPRAISAL_TEMPLATE = System.Configuration.ConfigurationManager.AppSettings["AppraisalTemplate"];

        //Other description for SceneAccess Appraisal Requests
        public readonly string OTHER_DESC = "Other (Scene Photos, etc.)";

        //the master password that can be used to log in as any person
        public string MASTER_PASSWORD = "copssuck";

        public bool FireClaimEvent(int claimId, int eventTypeId, Hashtable parms = null)
        {
            MedCsxLogic.Claim claim = new MedCsxLogic.Claim(claimId);
            return claim.FireEvent(eventTypeId, parms);
        }

        //Holiday dates
        public DateTime Date_NewYearsDay = new DateTime(2021, 1, 1);
        public DateTime Date_MartinLutherKingJRDay = new DateTime(2021, 1, 18);
        public DateTime Date_GroundhogDay = new DateTime(2021, 2, 2);
        public DateTime Date_ValentinesDay = new DateTime(2021, 2, 14);
        public DateTime Date_PresidentsDay = new DateTime(2021, 2, 15);
        public DateTime Date_AprilFoolsDay = new DateTime(2021, 4, 1);
        public DateTime Date_ArborDay = new DateTime(2021, 4, 30);
        public DateTime Date_CincoDeMayo = new DateTime(2021, 5, 5);
        public DateTime Date_MothersDay = new DateTime(2021, 5, 9);
        public DateTime Date_MemorialDay = new DateTime(2021, 5, 31);
        public DateTime Date_FlagDay = new DateTime(2021, 6, 14);
        public DateTime Date_FathersDay = new DateTime(2021, 6, 20);
        public DateTime Date_FourthOfJuly = new DateTime(2021, 7, 4);
        public DateTime Date_LaborDay = new DateTime(2021, 9, 6);
        public DateTime Date_PatriotDay = new DateTime(2021, 9, 11);
        public DateTime Date_ColumbusDay = new DateTime(2021, 10, 11);
        public DateTime Date_Halloween = new DateTime(2021, 10, 31);
        public DateTime Date_VeteransDay = new DateTime(2021, 11, 11);
        public DateTime Date_Thanksgiving = new DateTime(2021, 11, 25);
        public DateTime Date_Christmas = new DateTime(2020, 12, 25);
        public DateTime Date_NewYearsEve = new DateTime(2021, 12, 31);

        public string[] ApplicationDevelopers = { "Brian Moyet" };
        #endregion

        #region Global Variables
        public DateTime lastActivityTime = DEFAULT_DATE;
        public ArrayList urgentAlerts = new ArrayList();
        public Hashtable ClaimWindows = new Hashtable(); //indexed by claim number

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        //public string draftPrinter = "\\\\moose\\PL Auto - M3560";
        //public string draftPrinterKC = "\\\\moose\\PL Auto - M3560";   //development
        //public string draftPrinterLV = "\\\\moose\\HP LaserJet (IT East)";   //development
        public string draftPrinter = "\\\\moose\\HP Draft Key Home Office";
        public string draftPrinterKC = "\\\\moose\\HP Draft Key Home Office";
        public string draftPrinterLV = "\\\\moose\\HP Draft Key LV";
<<<<<<< HEAD
=======
=======
        public string draftPrinter = "\\\\moose\\PL Auto - M3560";
        public string draftPrinterKC = "\\\\moose\\PL Auto - M3560";   //development
        public string draftPrinterLV = "\\\\moose\\HP LaserJet (IT East)";   //development
        //public string draftPrinter = "\\\\moose\\HP Draft Key Home Office";
        //public string draftPrinterKC = "\\\\moose\\HP Draft Key Home Office";
        //public string draftPrinterLV = "\\\\moose\\HP Draft Key LV";
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e


        public int minutesUntilLocked = 0;
        public int minutesLockedUntilShutdown = 0;
        public Dictionary<int, object> AppSettings = new Dictionary<int, object>(); //Application settings
        public SortedList<int, bool> UserCompanyAuthority = new SortedList<int, bool>(); //User's company authority
        public double calculatorMemory = 0;
        #endregion

        #region Window Messages (WndProc Messages)
        public enum WindowMessages
        {
            WM_ACTIVATE

        }
        #endregion

        #region Application Settings
        public enum AppSettingsNames : int
        {
            MainFormWidth = 0,
            MainFormHeight = 1,
            MainFormTop = 2,
            MainFormLeft = 3,
            RecordingDevice = 4,
            RecordingDeviceLine = 5
        };

        public void GetApplicationSettings()
        {
            AppSettings.Clear();  //clear the application settings
            if (System.IO.File.Exists(SETTINGS_FILE))  //the settings file exists, get the settings
            {
                DataSet SettingsData = new DataSet();
                SettingsData.ReadXml(SETTINGS_FILE); //get the application settings

                DataTable SettingsTable = SettingsData.Tables[0]; //get the settings table
                DataRow SettingsValues = SettingsTable.Rows[0]; //get the settings row

                foreach (AppSettingsNames foo in Enum.GetValues(typeof(AppSettingsNames)))
                    AppSettings.Add((int)foo, SettingsValues[(int)foo]); //add this settting to the application settings hashtable
            }
            else  //Settings file doesn't exist - create the settings file
            {
                DataSet SettingsLayout = new DataSet();
                SettingsLayout.ReadXmlSchema(SETTINGS_LAYOUT);  //get the settings layout

                DataTable SettingsTable = SettingsLayout.Tables[0]; //get the settings table

                object[] SettingsRow = new object[SettingsTable.Columns.Count];  //initialize the settings
                for (int i = 0; i < SettingsTable.Columns.Count; i++)
                {
                    DataColumn Column = SettingsTable.Columns[i];  //get the column
                    if ((Column.DataType == typeof(int)) || (Column.DataType == typeof(Int64)))
                        SettingsRow[i] = 0; //integer
                    else if (Column.DataType == typeof(double))
                        SettingsRow[i] = 0.0; //double
                    else if (Column.DataType == typeof(string))
                        SettingsRow[i] = "";  //string
                    else
                        SettingsRow[i] = "";  //unknow type

                    AppSettings.Add(i, SettingsRow[i]);
                }
                SettingsTable.Rows.Add(SettingsRow); //add the settings row to the table
                SettingsLayout.WriteXml(SETTINGS_FILE);  //create the settings file
            }
        }

        public void SaveApplicationSettings()
        {
            //create the application settings data table and data set
            DataSet SettingsData = new DataSet();
            DataTable SettingsTable = new DataTable();

            //create the settings row
            object[] SettingsRow = new object[AppSettings.Keys.Count - 1];
            for (int i = 0; i < AppSettings.Keys.Count; i++)
            {
                SettingsRow[i] = AppSettings[i]; //add the settings to the settings row
                SettingsTable.Columns.Add(); //add the column to the table
            }
            SettingsTable.Rows.Add(SettingsRow);  //add the settings row to the data table
            SettingsData.Tables.Add(SettingsTable);  //add the data table to the data set
            SettingsData.WriteXml(SETTINGS_FILE);  //write the settings to the file
        }
        #endregion

        #region User Company Authority
        /// <summary>
        /// Save a local copy of the user's Company authority
        /// </summary>
        public void LoadUserCompanyAuthority()
        {
        //    UserCompanyAuthority.Clear();  //clear the current authority
            
        //    //TODO*
        //    //foreach (Hashtable company in User.CompanyAuthority(UserId))
        //    foreach (Hashtable company in User.CompanyAuthority(UserId))
        //            UserCompanyAuthority.Add((int)company["Company_Id"], (bool)company["Open_Allowed"]);  //add the authority to the list
        }

        /// <summary>
        /// Is the user allowed to open this company type?
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool ComanyAllowedForUser(int companyId)
        {
            if (UserCompanyAuthority.ContainsKey(companyId))
                return UserCompanyAuthority[companyId];  //return the authority
            else
                return false;  //the authority doesn't exist, not allowed to open the Company type
        }
        #endregion

        #region Claim Search Methods
        /// <summary>
        /// Search open claims whose creation date is not later than noDays ago
        /// </summary>
        /// <param name="noDays">Number of days</param>
        /// <returns>ArrayList of rows to be displayed in a Claims List View</returns>
        //public List<Hashtable> SearchClaimsByDate(int noDays, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Date", noDays);
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Date_Ordered", noDays, filter);
        //}14

        public DataTable SearchClaimsByDate(int noDays, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Date", noDays);
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Date_Ordered", noDays, filter);
        }

        public DataTable LoadFraud(int claim_id, int vendor_id, int person_id, int vehicle_id)
        {
           
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Fraud_Claims_For_Id", claim_id, vendor_id, person_id, vehicle_id);
        }

        public DataTable LoadRecentClaims(int user_id)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Recent_Claims", user_id);
        }

        /// <summary>
        /// Search claims by first and last name (using Like)
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns>ArrayList of rows to be displayed in a Claims List View</returns>
        //public List<Hashtable> SearchClaimsByName(string firstName, string lastName, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Name", firstName.Trim(), lastName.Trim());
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Name_Ordered", firstName.Trim(), lastName.Trim(), filter);
        //}

        public DataTable SearchClaimsByName(string firstName, string lastName, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Name", firstName.Trim(), lastName.Trim());
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Name_Ordered", firstName.Trim(), lastName.Trim(), filter);
        }

        /// <summary>
        /// Search open claims for all open claims
        /// </summary>
        /// <returns>ArrayList of rows to be displayed in a Claims List View</returns>
        //public List<Hashtable> SearchClaimsByAllPending(string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_All_Pending");
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_All_Pending_Ordered", filter);
        //}

        public DataTable SearchClaimsByAllPending(string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_All_Pending");
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_All_Pending_Ordered", filter);
        }

        //public List<Hashtable> SearchClaimsByAllLockedPending(string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_All_Locked_Pending");
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_All_Locked_Pending_Ordered", filter);
        //}

        public DataTable SearchClaimsByAllLockedPending(string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_All_Locked_Pending");
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_All_Locked_Pending_Ordered", filter);
        }

        //public List<Hashtable> SearchClaimsByClaimNo(string claimNo, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Claim_No", claimNo.Trim());
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Claim_No_Ordered", claimNo.Trim(), filter);
        //}

        public DataTable SearchClaimsByClaimNo(string claimNo, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Claim_No", claimNo.Trim());
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Claim_No_Ordered", claimNo.Trim(), filter);
        }

        //public List<Hashtable> SearchClaimsByDraftNo(string draftNo, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Draft_No", draftNo.Trim());
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Draft_No_Ordered", draftNo.Trim(), filter);
        //}

        public DataTable SearchClaimsByDraftNo(string draftNo, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Draft_No", draftNo.Trim());
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Draft_No_Ordered", draftNo.Trim(), filter);
        }

        //public List<Hashtable> SearchClaimsByPolicyNo(string policyNo, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Policy_No", policyNo.Trim());
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Policy_No_Ordered", policyNo.Trim(), filter);
        //}
        public DataTable SearchClaimsByPolicyNo(string policyNo, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Policy_No", policyNo.Trim());
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Policy_No_Ordered", policyNo.Trim(), filter);
        }

        //public List<Hashtable> SearchClaimsByVehicle(string yearMade, string make, string model, string vin, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Vehicle", yearMade.Trim(), make.Trim(), model.Trim(), vin.Trim());
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Vehicle_Ordered", yearMade.Trim(), make.Trim(), model.Trim(), vin.Trim(), filter);
        //}

        public DataTable SearchClaimsByVehicle(string yearMade, string make, string model, string vin, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Vehicle", yearMade.Trim(), make.Trim(), model.Trim(), vin.Trim());
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Vehicle_Ordered", yearMade.Trim(), make.Trim(), model.Trim(), vin.Trim(), filter);
        }

        //public List<Hashtable> SearchClaimsByDateOfLoss(string dateOfLoss, string filter = null)
        //{
        //    if (filter == null)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Date_Of_Loss", dateOfLoss.Trim());
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Date_Of_Loss_Ordered", dateOfLoss.Trim(), filter);
        //}

        public DataTable SearchClaimsByDateOfLoss(string dateOfLoss, string filter = null)
        {
            if (filter == null)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Date_Of_Loss", dateOfLoss.Trim());
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Date_Of_Loss_Ordered", dateOfLoss.Trim(), filter);
        }

        //public List<Hashtable> SearchRecordsByPhoneNumber(string phoneNumber, int mode = 0)
        //{
        //    if (mode == 0)      //search claims
        //    {
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_PhoneNumber", phoneNumber);
        //    }
        //    //search phone records
        //    return db.ExecuteStoredProcedureReturnHashList("usp_Search_Phone_Records_By_PhoneNumber", phoneNumber);
        //}

        public DataTable SearchRecordsByPhoneNumber(string phoneNumber, int mode = 0)
        {
            if (mode == 0)      //search claims
            {
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_PhoneNumber", phoneNumber);
            }
            //search phone records
            return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Phone_Records_By_PhoneNumber", phoneNumber);
        }

        internal List<Hashtable> getPolicyLocation(string p, DateTime dateTime)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Commercial_Policy_Locations", p, dateTime.ToString());
        }

        //internal IEnumerable SearchClaimsByAddress(string streetAddress = "", string city = "", string stateId = "", string filter = "")
        //{
        //    if (string.IsNullOrWhiteSpace(filter))
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Address", streetAddress, city, stateId);
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Address_Ordered", streetAddress, city, stateId, filter);
        //}

        internal DataTable SearchClaimsByAddress(string streetAddress = "", string city = "", string stateId = "", string filter = "")
        {
            if (string.IsNullOrWhiteSpace(filter))
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Address", streetAddress, city, stateId);
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Address_Ordered", streetAddress, city, stateId, filter);
        }

        //internal IEnumerable SearchClaimsByEmail(string emailAddress, string filter = "")
        //{
        //    if (string.IsNullOrWhiteSpace(filter))
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Email", emailAddress);
        //    else
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Search_Claims_By_Email_Ordered", emailAddress, filter);
        //}

        internal DataTable SearchClaimsByEmail(string emailAddress, string filter = "")
        {
            if (string.IsNullOrWhiteSpace(filter))
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Email", emailAddress);
            else
                return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Claims_By_Email_Ordered", emailAddress, filter);
        }

        public string phoneStrip(string p_input)
        {
            string phNum = p_input.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "").Replace("_", "").Replace("x", "").Replace(" ", "");
            string p;

            if (phNum.Length > 10)
                p = "(" + phNum.Substring(0, 3) + ")" + phNum.Substring(3, 3) + "-" + phNum.Substring(6, 4) + "x" + phNum.Substring(10, phNum.Length - 10);
            else if (phNum.Length == 10)
                p = "(" + phNum.Substring(0, 3) + ")" + phNum.Substring(3, 3) + "-" + phNum.Substring(6, 4);
            else
                p = phNum;

            return p;
        }

        public string BuildPhoneNum(string p_input, ref bool isValid)
        {
            string phNum = p_input.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "").Replace("_", "").Replace("x", "").Replace(" ", "");
            string p;
            double testDbl = 0;

            if (!double.TryParse(phNum, out testDbl))
            {
                string[] dig = p_input.Split(' ');
                string p2;
                if (dig.Length > 2)
                {
                    p2 = BuildPhoneNum(dig[0] + " " + dig[1], ref isValid);
                    if (isValid)
                    {
                        for (int i = 2; i < dig.Length; i++)
                            p2 += " " + dig[i];
                        return p2;
                    }
                }
                else
                {
                    isValid = false;
                    return p_input;
                }
            }

            if (phNum.Length > 10)
                p = "(" + phNum.Substring(0, 3) + ")" + phNum.Substring(3, 3) + "-" + phNum.Substring(6, 4) + "x" + phNum.Substring(10, phNum.Length - 10);
            else if (phNum.Length == 10)
                p = "(" + phNum.Substring(0, 3) + ")" + phNum.Substring(3, 3) + "-" + phNum.Substring(6, 4);
            else
                p = phNum;

            isValid = true;
            return p;
        }

        public bool PolicyMask(string p)
        {
            //validates that policy number follows mask.  
            //this does not include policies from American Sterling
            //Mask: XXX9999999

            if (p.Length > 10)      //policy is not to be greater than 10 characters
                return false;

            char[] s = p.ToCharArray();
            string t = "";

            for (int i = 0; i < s.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        if (s[i] != 'K')    //first letter is K - eliminating American Sterling
                            return false;
                        t += s[i];
                        break;
                    case 1:
                        char[] range = { 'A', 'F', 'K', 'N', 'O' };
                        bool inrange = false;
                        for (int j = 0; j < range.Length; j++)
                        {
                            if (s[i] == range[j])
                            {
                                inrange = true;
                                break;
                            }
                        }
                        if (!inrange)
                            return false;

                        t += s[i];
                        break;
                    case 2:
                        t += s[i];
                        string[] range2 = { "KAL", "KAZ", "KFA", "KFP", "KKS", "KOK", "KNV" };      //first 3 characters
                        bool inrange2 = false;
                        for (int j = 0; j < range2.Length; j++)
                        {
                            if (t == range2[j])
                            {
                                inrange2 = true;
                                break;
                            }
                        }
                        if (!inrange2)
                            return false;
                        break;
                    default:
                        int testInt = 0;
                        if (!int.TryParse(s[i].ToString(), out testInt))        //remaining characters are numeric
                            return false;

                        t += s[i];
                        break;
                }
            }
            return true;
        }

        public List<Hashtable> GetSearchResult(int claimId, string searched)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_File_Note_Search_Results_With_Images", claimId, searched);
        }

        public DataTable SearchVendors(string search_column, string search_value)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Search_Vendors", search_column, search_value);
        }

        public List<Hashtable> SearchVendors(string search_column, string search_value, int dummy)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Search_Vendors", search_column, search_value);
        }

        public DataTable GetVendorsBySubType(string subType)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Vendors", subType);
        }

        public DataTable GetFraudClaims(int vendor_id, int person_id, int vehicle_id, int claim_id)
        {
            return db.ExecuteStoredProcedure("usp_Get_Fraud_Claims_For_Id", claim_id, vendor_id, person_id, vehicle_id);
        }

        public List<Hashtable> GetNICBvendors(string tax_id = "", string vendor_name = "", string street_address = "", string phone_number = "", string fax_number = "", string email = "")
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Search_Vendors_Fraud", tax_id, vendor_name, street_address, phone_number, fax_number, email);
        }

        public List<Hashtable> GetNICBPersons(string first_name = "", string last_name = "", string street_address = "", string phone_number = "", string fax_number = "", string email_address = "", DateTime date_of_birth = default(DateTime), string ssn = "", string drivers_license = "")
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", first_name, last_name, street_address, phone_number, fax_number, email_address, date_of_birth, ssn, drivers_license);
        }

        public List<Hashtable> GetNICBVehicles(string vin = "", string year_made = "", string manufacturer = "", string model = "", string license_no = "")
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Search_Vehicles_Fraud", vin, year_made, manufacturer, model, license_no);
        }

        public bool CheckForPossibleDuplicateDrafts(int reserve_id, int vendor_id, double dollar)
        {
            List<Hashtable> duplicates = new List<Hashtable>();
            duplicates = db.ExecuteStoredProcedureReturnHashList("usp_Get_Duplicate_Drafts", reserve_id, vendor_id, dollar);
            return duplicates.Count > 0;
        }

        public List<Hashtable> GetUnPrintedDrafts(int myId, int mode)
        {
            switch (mode)
            {
                case 1:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Unprinted_Drafts_For_Claim_With_Images", myId);
                default:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Unprinted_Drafts_For_Reserve_With_Images", myId);
            }
        }
        #endregion

        #region Validation Methods
        public bool ValidSSN(string SSN)
        {
            string s = "";
            string[] a;

            s = SSN.Trim();
            if (s == "")
                return false;

            if ((s.Length == 9) && (ml.CountDigits(s) == 9))
            {
                if (s.Substring(3, 2) == "00")
                    return false;
                return true;
            }

            a = s.Split('-');
            if (a.Length - 1 != 2)
                return false;

            if (!modLogic.isNumeric(a[0]) || !modLogic.isNumeric(a[1]) || !modLogic.isNumeric(a[2]) ||
                (a[0].Length != 3) || (a[1].Length != 2) || (a[2].Length != 4) ||
                (a[1] == "00"))
                return false;
            return true;
        }

        public bool ValidEmailAddress(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                string[] a;
                a = email.Split('@');
                if (a.Length - 1 != 1)  //check for only 1 @ - email@domain.ext
                    return false;

                if (a[0].Length != a[0].Replace(" ", string.Empty).Length)  //check for spaces in email
                    return false;

                a = a[1].Split('.');
                if (a.Length - 1 == 0) //check for 1 . in domain.ext
                    return false;

                if (a[1].Length - 1 != 2) //check for length of ext
                    return false;
                return true;
            }
        }

        public bool ValidTaxId(string taxId)
        {
            string s = "";
            string[] a;

            s = taxId.Trim();
            if ((s == "") || (s.Replace("-", string.Empty).Length != 9))
                return false;

            a = s.Split('-');
            switch (a.Length - 1)
            {
                case 1:
                    if ((!modLogic.isNumeric(a[0])) && (!modLogic.isNumeric(a[1])))
                        return false;

                    if (a[0].Length != 2)
                        return false;

                    if (a[1].Length != 7)
                        return false;
                    return true;
                case 2:
                    return ValidSSN(taxId);
                default:
                    return false; //bad tax id
            }
        }

        public bool ValidZipcode(string zipCode)
        {
            string s = "";
            int count;

            s = zipCode.Trim();
            count = ml.CountDigits(s);
            if ((count != 5) && (count != 9))  //domestic postal codes
                return false;
            return true;
        }

        public bool ValidState(string state)
        {
            DataTable c2 = db.ExecuteStoredProcedure("usp_Valid_State", state);
            DataRow d = c2.Rows[0];
            if ((int)d["NbrStates"] == 0)
                return false;
            return true;
        }

        public bool ValidateColumn(string table, string col, string value, string msg)
        {
            DataTable c = db.ExecuteStoredProcedure("usp_Get_Data_Dictionary_Column", table.Replace("Tag=", string.Empty), col.Replace("Columns=", string.Empty));
            if (c.Rows.Count == 0)
                return true;  //Tag line is not in Table;Column format

            DataRow d = c.Rows[0];
            string colType;
            int fieldLength;
            if ((bool)d["Is_Required"])  //check for required field
            {
                if ((value.Trim() == "") || (value.ToUpper().Trim() == "UNDEFINED"))
                {
                    msg = d["Display_Name"] + " is a Required Field.";
                    return false;
                }
            }

            colType = (string)d["Column_Type"];
            if ((colType == "char") || (colType == "varchar") || (colType == "text"))
            {
                fieldLength = (int)d["Column_Length"];
                if (value.Length > fieldLength)
                {
                    msg = d["Display_Name"] + " Cannot be Greater than " + fieldLength + " Characters Long.";
                    return false;
                }
            }

            if (value.Length != 0)
            {
                switch ((int)d["Validation_Type_Id"])
                {
                    case (int)modGeneratedEnums.ValidationType.Email_Address:
                        if (!ValidEmailAddress(value))
                        {
                            msg = "Email Address is not Valid.";
                            return false;
                        }
                        break;
                    case (int)modGeneratedEnums.ValidationType.Tax_Id:
                        if (!ValidTaxId(value))
                        {
                            msg = "Tax Id is not Valid.";
                            return false;
                        }
                        break;
                    case (int)modGeneratedEnums.ValidationType.SSN:
                        if (!ValidSSN(value))
                        {
                            msg = "Social Security Number is not Valid.";
                            return false;
                        }
                        break;
                    case (int)modGeneratedEnums.ValidationType.State:
                        if (!ValidState(value))
                        {
                            msg = "State is not Valid.";
                            return false;
                        }
                        break;
                    case (int)modGeneratedEnums.ValidationType.Zipcode:
                        if (!ValidZipcode(value))
                        {
                            msg = "Zipcode is not Valid.";
                            return false;
                        }
                        break;
                    case (int)modGeneratedEnums.ValidationType.Undefined:
                    case (int)modGeneratedEnums.ValidationType.Phone_Number:
                        return true;
                    default:
                        msg = "Unknow Validation Type Id: " + d["Validation_Type_Id"];
                        return false;
                }
            }
            return true;
        }
        #endregion

        #region User Id and Name Methods
        public int UserId
        {
            get {
                MedCsXSystem mySystem = new MedCsXSystem();
                return mySystem.UserId; }
        }

        public void ResetPassword(int user_id_to_change)
        {
            db.ExecuteStoredProcedure("usp_Set_Password", user_id_to_change, "password", UserId);
            ml.updateDateColumn("User", user_id_to_change, "Date_Password_Set", ml.DEFAULT_DATE);
        }

        public bool UserIsAdmin(int id = 0)
        {
            Hashtable user;
            if (id == 0)
                id = UserId;

            user = ml.getRow("User", id);
            return (int)user["User_Type_Id"] == (int)modGeneratedEnums.UserType.Administrator;
        }

        public List<Hashtable> UsersWhichCanBeAssignedClaims()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Users_Which_Can_Be_Assigned_Claims");
        }

        public List<Hashtable> UserGroupsWhichCanBeAssignedClaims()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_User_Groups_Which_Can_Be_Assigned_Claims");
        }

        public List<Hashtable> GetActiveUsers(int mode = 0)
        {
            string sql = "select distinct u.[user_id], u.first_name, u.last_name, ut.description as user_type, us.description as user_status";
            sql += " from [user] u, user_type ut, user_status us";
            sql += " where u.user_type_id = ut.user_type_id and u.user_status_id = us.user_status_id and u.[user_id] <> 0";
<<<<<<< HEAD
            sql += " and u.First_Name<>'' order by u.first_name, u.last_name";
=======
<<<<<<< HEAD
            sql += " and u.First_Name<>'' order by u.first_name, u.last_name";
=======
            sql += " and u.First_Name<>'' order by u.last_name, u.first_name";
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            switch (mode)
            {
                case 1:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_User_Groups");
                case 2:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_All_Subordinate_Users", UserId);
                case 3:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Supervisors");
                case 4:
                    return db.ExecuteSelectReturnHashList(sql);
                case 5:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_User_Groups_For_User", UserId);
                default:
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Active_Users");
            }
        }

        public void DeleteUserGroups(int user_id)
        {
            db.ExecuteStoredProcedure("usp_Delete_User_Groups_For_User", user_id);
        }

        public int GetReserveUserId(string firstName, string lastName)
        {
            return db.GetIntFromStoredProcedure("usp_Get_User_Id_For_Name", firstName, lastName);
        }

        public DataTable GetReserveAssignments()
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Reserve_Assignments");
        }

        public DataTable GetReserveAssignmentsForClaim(int claim_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Reserve_Assignments_For_Claim", claim_id);
        }

        public string GetUserName(int userId)
        {
            return db.GetStringFromStoredProcedure("usp_Get_User_Name", userId);
        }

        public int GetLanguageId(string language)
        {
            int ret = 2;        //default is English
            IEnumerable lData = GetLanguages(); //db.ExecuteStoredProcedureReturnHashList("usp_Get_Languages");
            foreach (Hashtable langRecord in lData)
            {
                if (language == (string)langRecord["Description"])
                {
                    ret = (int)langRecord["Language_Id"];
                    break;
                }
            }
            return ret;
        }

        public int LanguageCBOIndex(int languageId)
        {
            int ret = 0;
            // IEnumerable lData = db.ExecuteStoredProcedureReturnHashList("usp_Get_Languages");
            IEnumerable lData = GetLanguages();
            foreach (Hashtable langRecord in lData)
            {
                if (languageId == (int)langRecord["Language_Id"])
                    break;
                ret++;
            }
            return ret;
        }

        public List<Hashtable> GetLanguages()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Languages");
        }

        public List<Hashtable> GetLanguagesForUser(int user_id)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Languages_For_User", user_id);
        }

        public void ChangeLanguage(int user_id, int language_id, string action)
        {
            switch (action)
            {
                case "insert":
                    db.ExecuteStoredProcedure("usp_Insert_User_Language", user_id, language_id);
                    break;
                case "remove":
                    db.ExecuteStoredProcedure("usp_Delete_User_Language", user_id, language_id);
                    break;
                default:
                    break;
            }
        }

        public List<Hashtable> GetAddresses(int claimId)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Addresses_For_Claim", claimId);
        }

        public DataTable GetAddressesDT(int claimId)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Addresses_For_Claim", claimId);
        }

        public List<ClaimantGrid> LoadClaimants(List<Hashtable> cData, int mode = 0)
        {
            List<ClaimantGrid> myClaimantList = new List<ClaimantGrid>(); ;
            try
            {
                switch (mode)
                {
                    case 1: //owner cbo
                        foreach (Hashtable myData in cData)
                        {
                            if ((string)myData["Name"] != "Parked and Unoccupied")
                            {
                                //myOwnerListcbo.Add(new ClaimantGrid()
                                //{
                                ClaimantGrid cg = new ClaimantGrid();
                                cg.claimantId = (int)myData["Person_Id"];
                                cg.personType = (string)myData["Type"];
                                cg.imageIdx = getImageIndex((int)myData["Image_Index"], cg.personType);
                                cg.fullName = (string)myData["Name"];
                                cg.streetAddress = (string)myData["Address1"];
                                cg.city = (string)myData["City"];
                                cg.state = (string)myData["State"];
                                cg.postalCode = (string)myData["Zipcode"];
                                //});
                                myClaimantList.Add(cg);
                            }
                        }
                        break;
                    case 2: //driver cbo
                        foreach (Hashtable myData in cData)
                        {
                            //myDriverListcbo.Add(new ClaimantGrid()
                            //{
                            ClaimantGrid cg = new ClaimantGrid();
                            cg.claimantId = (int)myData["Person_Id"];
                            cg.personType = (string)myData["Type"];
                            cg.imageIdx = getImageIndex((int)myData["Image_Index"], cg.personType);
                            cg.fullName = (string)myData["Name"];
                            cg.streetAddress = (string)myData["Address1"];
                            cg.city = (string)myData["City"];
                            cg.state = (string)myData["State"];
                            cg.postalCode = (string)myData["Zipcode"];
                            //});
                            myClaimantList.Add(cg);
                        }
                        break;
                    case 3:     //Policy grid
                        foreach (Hashtable myData in cData)
                        {
                            ClaimantGrid cg = new ClaimantGrid();
                            cg.claimantId = (int)myData["No_Account"];
                            cg.personType = (string)myData["Type"];
                            cg.fullName = (string)myData["Name"];
                            cg.streetAddress = (string)myData["Address1"];
                            cg.Address2 = (string)myData["Address2"];
                            cg.city = (string)myData["City"];
                            cg.state = (string)myData["Name_State"];
                            cg.postalCode = (string)myData["Cd_Zip"];
                            cg.cAge = Convert.ToInt32((byte)myData["Age"]);
                            cg.cSex = (string)myData["Sex"];
                            cg.MaritalStatus = (string)myData["Stat_Martl"];
                            cg.cDOB = ((DateTime)myData["Dt_Birth"]).ToShortDateString();
                            cg.taxId = (string)myData["No_SocSec"];
                            cg.clCounter = Convert.ToInt32((byte)myData["No_Indiv"]);
                            myClaimantList.Add(cg);
                        }
                        break;
                    case 4:     //vendor grid
                        foreach (Hashtable myData in cData)
                        {
                            if (((string)myData["Name"]).Trim().Substring(0, 2) != "**")
                            {
                                ClaimantGrid cg = new ClaimantGrid();
                                cg.claimantId = (int)myData["Vendor_Id"];
                                cg.fullName = (string)myData["Name"];
                                cg.streetAddress = (string)myData["Address 1"];
                                cg.Address2 = (string)myData["Address 2"];
                                cg.city = (string)myData["City"];
                                cg.state = (string)myData["State"];
                                cg.postalCode = (string)myData["Zipcode"];
                                cg.phone = (string)myData["Phone"];
                                cg.fax = (string)myData["Fax"];
                                cg.email = (string)myData["Email"];
                                cg.taxId = (string)myData["Tax Id"];
                                cg.imageIdx = (int)myData["Image_Index"];
                                myClaimantList.Add(cg);
                            }
                        }
                        break;
                    case 5:     //Witness Passenger
                        foreach (Hashtable wp in cData)
                        {
                            ClaimantGrid cg = new ClaimantGrid();
                            cg.claimantId = (int)wp["Person_Id"];
                            cg.witnessId = (int)wp["Witness_Passenger_Id"];
                            cg.fullName = (string)wp["Person"];
                            cg.streetAddress = (string)wp["Address"];
                            cg.city = (string)wp["City"];
                            cg.state = (string)wp["Abbreviation"];
                            cg.postalCode = (string)wp["Zipcode"];
                            cg.personType = (string)wp["Description"];
                            cg.imageIdx = (int)wp["Image_Index"];
                            myClaimantList.Add(cg);
                        }
                        break;
                    default:    //Claimant grid
                        foreach (Hashtable myData in cData)
                        {
                            myClaimantList.Add(new ClaimantGrid()
                            {
                                claimantId = (int)myData["Claimant_Id"],
                                imageIdx = (int)myData["Image_Index"],
                                personType = (string)myData["Person_Type"],
                                fullName = (string)myData["Person"],
                                streetAddress = (string)myData["Address1"],
                                city = (string)myData["City"],
                                state = (string)myData["Abbreviation"],
                                postalCode = (string)myData["Zipcode"]
                            });
                        }
                        break;
                }
                return myClaimantList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return new List<ClaimantGrid>();
            }
        }

        public List<ReserveGrid> LoadReserves(List<Hashtable> rData, int mode = 0)
        {
            List<ReserveGrid> myReserveList = new List<ReserveGrid>();
            try
            {
                //GetReservesByClaim
                foreach (Hashtable res in rData)
                {
                    ReserveGrid cg = new ReserveGrid();
                    cg.reserveId = (int)res["Reserve_Id"];
                    cg.claimant = (string)res["Claimant"];
                    cg.reserveType = (string)res["Reserve_Type"];
                    cg.reserveStatus = (string)res["Reserve_Status"];
                    cg.grossLossReserve = (double)((decimal)res["Gross_Loss_Reserve"]);
                    cg.netLossPaid = (double)((decimal)res["Paid_Loss"]);
                    cg.netLossReserve = (double)((decimal)res["Net_Loss_Reserve"]);
                    cg.grossExpenseReserve = (double)((decimal)res["Gross_Expense_Reserve"]);
                    cg.netExpensePaid = (double)((decimal)res["Paid_Expense"]);
                    cg.netExpenseReserve = (double)((decimal)res["Net_Expense_Reserve"]);
                    cg.dtReported = (DateTime)res["Date_Open"];
                    cg.reservingInd = (string)res["Reserving"];
                    cg.reserveImage = (int)res["Image_Index"];
                    cg.adjusterName = (string)res["Assigned_To"];
                    cg.id = (int)res["Id"];
                    myReserveList.Add(cg);
                }
                return myReserveList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myReserveList;
            }
        }

        public List<Hashtable> GetDoctors(int stateId)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Doctors", stateId);
        }

        private int getImageIndex(int p1, string p2)
        {
            if (p1 != 0)
                return p1;

            switch (p2)
            {
                case "Excluded Driver":
                    return 13;
                case "Injured":
                    return 28;
                case "Owner":
                case "Insured":
                case "Named Insured":
                case "Claimant":
                case "Passenger - IV":
                case "Passenger - CV":
                    return 30;
                case "Witness":
                    return 56;
                case "Driver":
                case "Additional Driver":
                    return 75;
                default:
                    return 30;
            }
        }

        public List<Hashtable> GetLocks(int mode, int claim_id)
        {
            switch (mode)
            {
                case 1: //Claims
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Locks_With_Images", claim_id);
                case 2: //Reserves
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Reserve_Locks_With_Images", claim_id);
                case 3: //Transactons
                    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Transaction_Locks_With_Images", claim_id);
                default:
                    return new List<Hashtable>();
                    break;
            }
        }
        #endregion

        #region Event Firing Methods
        public void FireEvent(int eventTypeId, Hashtable parms)
        {
            EventType et = new EventType(eventTypeId);
            et.FireEvent(parms);
        }

        //public bool FireClaimEvent(int claimId, int eventTypeId, Hashtable parms = null)
        //{
        //    MedCsxLogic.Claim claim = new MedCsxLogic.Claim(claimId);
        //    return claim.FireEvent(eventTypeId, parms);
        //}

        public bool FireReserveEvent(int reserveId, int eventTypeId)
        {
            Reserve res = new Reserve(reserveId);
            return res.FireEvent(eventTypeId);
        }

        public bool FireTransactionEvent(int transactionId, int eventTypeId)
        {
            Transaction t = new Transaction(transactionId);
            return t.FireEvent(eventTypeId);
        }

        #endregion

        #region Alert Methods
        //public List<Hashtable> GetMyAlerts(int alertUserId, DateTime alertStartDate, bool getDeferred = false, int claim_id = 0)
        //{
        //    if (claim_id == 0)
        //        return db.ExecuteStoredProcedureReturnHashList("usp_Get_My_Alerts_After", alertUserId, alertStartDate);
        //    return db.ExecuteStoredProcedureReturnHashList("usp_Get_Alerts_For_Claim", claim_id);
        //}
        public DataTable GetMyAlerts(int alertUserId, DateTime alertStartDate, bool getDeferred = false, int claim_id = 0)
        {
            if (claim_id == 0)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_Alerts_After", alertUserId, alertStartDate);
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Alerts_For_Claim", claim_id);
        }

        public DataTable GetMyUrgentAlerts(int alertUserId)
        {
            
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_Urgent_Alerts", alertUserId);
        }

        public DataTable GetMyStartupAlerts(DateTime alertStartDate, bool getDeferred = false)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_Alerts_After", UserId, alertStartDate);
        }

        public List<Hashtable> GetMyUrgenAlerts()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_My_Urgent_Alerts", UserId);
        }

        public DataTable GetAlertsToMe(int alertUserId)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_Alerts_Sent", alertUserId);
        }
        #endregion

        #region Reserve Methods
        //public List<Hashtable> GetMyReserves(int reserveUserId)
        //{
        //    return db.ExecuteStoredProcedureReturnHashList("usp_Get_My_Reserves", reserveUserId);
        //}

        public DataTable GetMyReserves(int reserveUserId)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_Reserves", reserveUserId);
        }

        public List<Hashtable> GetReservesWithoutNotes(int userId)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_My_Reserves_Without_Notes", userId);
        }

        public List<Hashtable> GetReservesHashByClaim(int claimId)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Reserves_With_Images", claimId);
        }
        public DataTable GetReservesByClaim(int claimId)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Reserves_With_Images", claimId);
        }

        public DataTable GetReserveNamesForClaimant(int claimantId)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Reserve_Names_For_Claimant", claimantId);
        }

        public List<Hashtable> GetReserveTypes()
        {
            List<Hashtable> activeReserves = new List<Hashtable>();
            foreach (Hashtable ht in db.ExecuteStoredProcedureReturnHashList("usp_Get_Reserve_Types"))
            {
                if ((bool)ht["IsActive"])
                    activeReserves.Add(ht);
            }
            return activeReserves;
        }

        public DataTable GetAuthorityForUser(int user_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Authorities_For_User", user_id);
        }
        #endregion

        #region Diary Methods
        public DataTable GetMyDiaries(int diaryUserId, int mode = 1)
        {
            if (mode == 1) //past due diaries
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Diary_Past_Due", diaryUserId, DateTime.Now);
            else if (mode == 2)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Uncleared_Diary_Entries_With_Images", diaryUserId);
            else  //all diaries
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Diary_All", diaryUserId);
        }
        #endregion

        #region Task Methods
        //public List<Hashtable> GetMyTasks(int taskUserId, int claim_id = 0, int mode = 0)
        //{
        //    switch (mode)
        //    {
        //        case 1:
        //            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Tasks_For_Claim_And_User", claim_id, taskUserId);
        //        default:        //mode = 0
        //            return db.ExecuteStoredProcedureReturnHashList("usp_Get_My_Tasks", taskUserId);
        //    }
        //}

        public DataTable GetMyTasks(int taskUserId, int claim_id = 0, int mode = 0)
        {
            switch (mode)
            {
                case 1:
                    return db.ExecuteStoredProcedureReturnDataTable ("usp_Get_Tasks_For_Claim_And_User", claim_id, taskUserId);
                default:        //mode = 0
                    return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_Tasks", taskUserId);
            }
        }

        public DataTable GetMyDemands(int taskUserId, int claim_id = 0, int mode = 0)
        {
            switch (mode)
            {
                case 1:
                    return db.ExecuteStoredProcedureReturnDataTable("usp_MedCsX_Get_MY_Demands", claim_id, taskUserId);
                default:        //mode = 0
                    return db.ExecuteStoredProcedureReturnDataTable("usp_MedCsX_Get_MY_Demands", taskUserId);
            }
        }

        public List<Hashtable> GetNumTasks(int task_id, int claim_id, int claimant_id, int reserve_id)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_No_User_Tasks", task_id, claim_id, claimant_id, reserve_id);
        }

        public int GetAppraisalTasks(int claim_id, int vehicle_id)
        {
            return db.GetIntFromStoredProcedure("usp_Appraisal_Task_Exists", claim_id, vehicle_id);
        }

        public DataTable GetAppraisalTasks(int claim_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Appraisal_Tasks_For_Claim", claim_id);
        }

        public void CompleteAppraisalTasks()
        {
            //db.ExecuteStoredProcedure("usp_Set_Appraisal_Tasks_Completed");
        }

        public void CompleteAppraisals(int appraisal_task_id)
        {
            db.ExecuteStoredProcedure("usp_Set_Appraisal_To_Completed", appraisal_task_id);
        }

        public DataTable GetOpenAppraisals(int appraiser_id = 0)
        {
            if (appraiser_id == 0)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Open_Appraisal_Totals");
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Open_Appraisals_For_Appraiser", appraiser_id);
        }

        public DataTable GetDraftQueue()
        {
          
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Drafts_In_Queue");
        }

        public DataTable GetDraftQueueKC()
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Drafts_In_Queue_By_Default_Printer", 1);
        }

        public DataTable GetDraftQueueLV()
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Drafts_In_Queue_By_Default_Printer", 2);
        }

        public int Get_Draft_Print_Creator(int DpId)
        {

            return db.GetIntFromStoredProcedure("usp_get_draft_Print_creator", DpId);
        }

        public DataTable GetPendingDrafts()
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Pending_Drafts");
        }

        public DataTable GetReserveHistoryAssignment(int reserve_id)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Reserve_Assignment_History", reserve_id);
        }

        public DataTable GetFileNoteTypes()
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_File_Note_Types");
        }

        public DataTable GetUserTaskTypes()
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_User_Task_Types");
        }

        public DataTable GetUserTypes()
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_User_Types");
        }

        public DataTable grUserTypeDiary(int id)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_User_Type_Diaries_For_User_Type", id );
        }

        public DataTable GetFileNoteEventTypes(int FileNoteTypeId)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Event_Types_Which_Create_File_Note", FileNoteTypeId);
        }

        public DataTable GetEventTypesWhichCreatesTask(int userTaskTypeId)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Event_Types_Which_Creates_Task", userTaskTypeId);
        }

        public DataTable GetEventTypesWhichClearsTask(int userTaskTypeId)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Event_Types_Which_Clears_Task", userTaskTypeId);
        }

        public DataTable GetFileNoteClearingTasks(int FileNoteTypeId)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_User_Task_Types_Which_Create_File_Note", FileNoteTypeId);
        }

        public List<Hashtable> GetAppraisers()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Scene_Access_Appraisers");
        }

        public AppraisalResponse GetAppraisalResponseForTask(int appraisal_Task_id)
        {
            int appraisal_response_id = db.GetIntFromStoredProcedure("usp_Get_Appraisal_Response_Id_For_Task_Id", appraisal_Task_id);

            if (appraisal_response_id == 0)
                return new AppraisalResponse();
            return new AppraisalResponse(appraisal_response_id);
        }

        public double GetTotalSupplementalForResponse(int appraisal_Response_id)
        {
            string sql = "SELECT SUM(Supplemental_Amount) as Sum_Supplemental FROM Appraisal_Supplemental WHERE Appraisal_Response_Id=" + appraisal_Response_id;
            DataTable result = db.ExecuteSelect(sql);

            foreach (DataRow dRow in result.Rows)
            {
                double sumThis;
                object wii = dRow.ItemArray[0];
                if (!double.TryParse(dRow.ItemArray[0].ToString(), out sumThis))
                {
                    return 0;
                }
                return sumThis;
            }
            return 0;
        }

        public List<Hashtable> GetSupplementalForResponse(int appraisal_Response_id)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Supplemental_Appraisal_For_Response_Id", appraisal_Response_id);
        }
        #endregion

        public List<Hashtable> Get3rdPartySites(int user_id, int mode = 0)
        {
            if (mode == 0)
                return db.ExecuteStoredProcedureReturnHashList("usp_Get_Sites_For_User_Id", user_id);

            string sql = "SELECT Site_Id FROM User_3rd_Party_Sites";
            return db.ExecuteSelectReturnHashList(sql);
        }

        #region Total Loss Methods
        public DataTable GetMyTtlLosses(int tlUserId)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_My_TotalLosses", tlUserId);
        }
        #endregion

        #region Recon Methods
        public DataTable GetMyRecon(DateTime whichQtr, int mode)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Recon_Reserves", whichQtr, mode);
        }

        public List<Hashtable> GetUpdateCounts(int userId, DateTime qtr)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Counts_Recon", userId, qtr);
        }
        #endregion

        #region Transaction Methods
        public string GetSubroSalvageVoidReason(int subro_salvage_void_reason_id)
        {
            string sql = "SELECT Description FROM Subro_Salvage_Void_Reason WHERE Subro_Salvage_Void_Reason_Id = " + subro_salvage_void_reason_id;
            return (string)db.ExecuteSelectReturnValue(sql);
        }

        public DataTable GetSalvageForClaim(int claim_id)
        {
            //string sql = "SELECT s.Vendor_Id, s.Salvage_Id, at.Appraisal_Request_Id, at.Appraisal_Task_Id FROM claim cl";
            //sql += " INNER JOIN File_Note fn on fn.Claim_Id = cl.Claim_Id";
            //sql += " INNER JOIN [Transaction] tr on tr.Claim_Id=cl.Claim_Id";
            //sql += " LEFT JOIN Salvage s on s.Salvage_Id = tr.Salvage_Id";
            //sql += " LEFT JOIN Appraisal_Task at on at.Claim_Number=cl.Display_Claim_Id";
            //sql += " WHERE fn.File_Note_Type_Id in (20,22,133,165) AND cl.Claim_Id='" + claim_id.ToString() + "'";
            //return db.ExecuteSelectReturnHashList(sql);
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Salvage_For_Claim_Id", claim_id);
        }

        public List<Hashtable> GetSalvageHashForClaim(int claim_id)
        {
            //string sql = "SELECT s.Vendor_Id, s.Salvage_Id, at.Appraisal_Request_Id, at.Appraisal_Task_Id FROM claim cl";
            //sql += " INNER JOIN File_Note fn on fn.Claim_Id = cl.Claim_Id";
            //sql += " INNER JOIN [Transaction] tr on tr.Claim_Id=cl.Claim_Id";
            //sql += " LEFT JOIN Salvage s on s.Salvage_Id = tr.Salvage_Id";
            //sql += " LEFT JOIN Appraisal_Task at on at.Claim_Number=cl.Display_Claim_Id";
            //sql += " WHERE fn.File_Note_Type_Id in (20,22,133,165) AND cl.Claim_Id='" + claim_id.ToString() + "'";
            //return db.ExecuteSelectReturnHashList(sql);
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Salvage_For_Claim_Id", claim_id);
        }

        public DataTable GetMySalvage(int user_id)
        {
            //string sql = "SELECT tr.Claim_Id, tr.Claimant_Id, s.Salvage_Id, s.Vendor_Id, v.Vehicle_Id, at.Appraisal_Request_Id, at.Appraisal_Task_Id, s.Completed_Date FROM Salvage s";
            //sql += " INNER JOIN [Transaction] tr ON s.Salvage_Id=tr.Salvage_Id";
            //sql += " INNER JOIN Vehicle v ON tr.Claim_Id=v.Claim_Id";
            //sql += " INNER JOIN Claim cl ON tr.Claim_Id=cl.Claim_Id";
            //sql += " INNER JOIN Appraisal_Task at ON cl.Display_Claim_Id=at.Claim_Number";
            //sql += " WHERE tr.[User_Id] = " + user_id.ToString() + " and s.Is_Void<>'1' and (s.Storage_Charges>0 or s.Salvage_Yard_Charges>0)";
            //return db.ExecuteSelectReturnHashList(sql);
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Salvage_For_User_Id", user_id);

        }

        public bool GetSR22Policy(int claim_id)
        {
            //string sql = "SELECT c.Insured_Person_Id FROM Claim c, PersonalLines.dbo.Pol_FinResponse f";
            //sql += " where c.Policy_No = f.No_Policy and f.No_Version = c.Version_No and f.Cd_Filing='22'";
            //sql += " and c.Claim_Id=" + claim_id.ToString();

            return (db.ExecuteStoredProcedureReturnHashList("usp_Get_SR22_For_Claim_Id", claim_id)).Count > 0;
        }
        #endregion


        #region Claim Methods
        public void IncrementOpenClaims()
        {
            db.ExecuteStoredProcedure("usp_Increment_Open_Claims", UserId);
        }

        public int CheckBeanoClaim(string policyNo)
        {
            return db.GetIntFromStoredProcedure("usp_Check_For_Prev_Beno_Claim", policyNo);
        }

        public DataTable GetMyWitnessPassengerDDL(int claimId)
        {
            //TODO
            //return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Witness_Passengers_With_Images", claimId);
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Claim_Persons", claimId);
        }
        public DataTable GetMyWitnessPassenger(int claimId)
        {
           
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Witness_Passengers_With_Images", claimId);
            
        }

        public DataTable IsCheckUrgentAlert(int file_note_id, int user_id)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Is_Urgent_alert", file_note_id, user_id);

        }

        public DataTable GetMyWitnessPassengerPersons(int claimId)
        {

            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Claim_Persons", claimId);

        }

        public Hashtable GetInjuredCoverages(int injured)
        {
            IEnumerable coverage = db.ExecuteStoredProcedureReturnHashList("usp_Get_Injured_Coverages", injured);
            Hashtable retTable = new Hashtable();
            foreach (Hashtable table in coverage)
                retTable.Add(table["Description"], table);

            return retTable;
        }

        public string CoverageDescription(string abbreviation)
        {
            string sql = "select DESCRIPT from PersonalLines.dbo._COVERAGE plc where plc.COVERAGE=" + abbreviation;
            sql += " and plc.REPORT='COV'";

            DataTable covDescription = db.ExecuteSelect(sql);

            foreach (DataRow ret in covDescription.Rows)
                return (string)ret.ItemArray[0];
            return "";
        }

        public DataTable GetOverLappingPolicies(string policy_no, DateTime date_of_loss)
        {
            return db.ExecuteStoredProcedure("usp_Get_Policies_InForce", date_of_loss, policy_no);
        }
        public DataTable GetMyClaimants(int claimId, int mode = 0)
        {
            if (mode == 0)
                return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Claimants", claimId);
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Claimants_For_Claim", claimId);
        }

        public DataTable GetTypeTables()
        {
           return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Type_Tables");
        }

        public List<Hashtable> GetRecentClaims()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Recent_Claims", UserId);
        }

        public int InsertDuplicate(string policyNo, string versionNo, int claimId, string dateOfLoss, int possibleDuplicationClaimMode)
        {
            return db.GetIntFromStoredProcedure("usp_Insert_Possible_Duplicates", policyNo, versionNo, claimId, dateOfLoss, possibleDuplicationClaimMode);
        }

        public void UpdateDefaultVehiclePersons(int claimId, int userId)
        {
            db.ExecuteStoredProcedure("usp_Update_Default_Vehicle_Persons", claimId, userId);
        }

        public List<Hashtable> GetPropertyTypes()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Property_Types");
        }

        public List<Hashtable> GetAllowedCoverage(int claim_id)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Allowable_Injured_Coverages", claim_id);
        }

        public void DeleteBodyParts(int injuredId)
        {
            db.ExecuteStoredProcedure("usp_Delete_Injured_Body_Part", injuredId);
        }
        public void DeleteUserCompanyAuthority(int userId)
        {
            db.ExecuteStoredProcedure("usp_Delete_Company_Authority", userId);
        }

        public void DeleteLitigationReserves(int litigationId)
        {
            db.ExecuteStoredProcedure("usp_Delete_Litigation_Reserves", litigationId);
        }
        #endregion

        #region Misc Methods
        public DateTime GetLogoutDate()
        {
            return ml.CurrentUser.LogoutDate;
        }

        public List<Hashtable> GetClaimsWithoutNotes()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claims_Without_Adjuster_Entered_Notes", Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public DataTable GetVendorTypes()
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Vendor_Types");
        }

        public DataTable GetVendorSubTypes(string vendorType)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Vendor_Sub_Types", vendorType);
        }

        public bool VendorExists(string vendor, int mode = 0)
        {
            switch (mode)
            {
                case 1:
                    return db.GetBoolFromStoredProcedure("usp_Vendor_Tax_Id_Exists", vendor);
                default: //case 0:
                    return db.GetBoolFromStoredProcedure("usp_Vendor_Name_Exists", vendor);
            }
        }

        public int GetVendorIdForTaxId(string tax_id)
        {
            return db.GetIntFromStoredProcedure("usp_Get_Vendor_Id_For_Tax_Id", tax_id);
        }

        public List<Hashtable> GetStates()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_States");
        }

        public int GetStateId(string stateAbbr)
        {
            return db.GetIntFromStoredProcedure("usp_Get_State_Id_For_Abbr", stateAbbr);
        }

        public string GetStateForId(int stateId)
        {
            return db.GetStringFromStoredProcedure("usp_Get_State", stateId);
        }

        public void UpdateLastPolledActiveDate()
        {
            db.ExecuteStoredProcedure("usp_Update_Last_Polled_Active_Date", UserId);
        }

        public DateTime GetLastPolledActiveDate()
        {
            Hashtable user;
            user = ml.getRow("User", UserId);
            return (DateTime)user["Last_Polled_Active_Date"];
        }

        public DateTime DefaultDeferredDate()
        {
            int yy = ml.DBNow().Year;
            int mm = ml.DBNow().Month;
            int dd = ml.DBNow().Day;
            DateTime defaultDate = new DateTime(yy, mm, dd, 6, 0, 0);
            return defaultDate.AddDays(1);
        }

        public void LoadSettings()
        {
            draftPrinter = MedCsXSystem.Settings.DraftPrinter;
            minutesUntilLocked = MedCsXSystem.Settings.MinutesUntilLocked;
            minutesLockedUntilShutdown = MedCsXSystem.Settings.MinutesLockedUntilShutdown;
        }

        public void UpdateLastActivityTime()
        {
            lastActivityTime = ml.DBNow();
        }

        public void ExitSystem()
        {
            User.LogoffUser();
        }

        public int ExtractClaimId(string s)
        {
            string s2 = "";
            for (short i = 0; i < s.Length; i++)
            {
                if (modLogic.isNumeric(s.Substring(i, 1)))
                    s2 += s.Substring(i, 1);
            }
            if (s2 == "")
                s2 = "0";
            return int.Parse(s2);
        }

       

        public bool CheckUserAuthorization(int taskId)
        {
            bool b = IsUserAuthorizedForTask(taskId);
           //TODO   check if authorized
            //if (!b)
            //{
            //    frmNotAuthorized f = new frmNotAuthorized(taskId);
            //    f.ShowDialog();
            //}
            return b;
        }

        public bool IsUserAuthorizedForTask(int taskId, int user_id = 0)
        {
            bool userAuthorized = true;
            if (user_id == 0)
                user_id = Convert.ToInt32(HttpContext.Current.Session["userID"]);

            bool isAuthorized = db.GetBoolFromStoredProcedure("usp_Is_User_Authorized_For_Task", user_id, taskId);
            if (!isAuthorized)
            {
                User v = new User(user_id);
                long userAllowed = v.UserAllowance;
                long myTaxId = 0;
                switch (taskId)
                {
                    case 3:
                        myTaxId = taskId - 3;
                        break;
                    case 5:
                        myTaxId = taskId - 4;
                        break;
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        myTaxId = taskId - 5;
                        break;
                    default:
                        myTaxId = taskId - 6;
                        break;
                }
                userAuthorized = FlagsHelper.IsSet((long)userAllowed, myTaxId);
            }
            return userAuthorized;
        }

        public string GetPersonDescription(int id, bool isLondon = false)
        {
            return ml.myScriptFunctions.getPersonDescription(id, isLondon);
        }

        public int GetPersonIdForDescription(string description, int claimId)
        {
            if (_personIdsForDescriptionsClaimId != claimId)
            {
                _personIdsForDescriptionsClaimId = claimId;
                _personIdsForDescriptions = new Hashtable();
            }

            if (_personIdsForDescriptions.Contains(description))
                return (int)_personIdsForDescriptions[description];

            List<Hashtable> c = db.ExecuteStoredProcedureReturnHashList("usp_Get_Person_Id_For_Description", description, claimId);
            if (c.Count == 0)
                return 0;
            Hashtable d = (Hashtable)ml.firstItem(c);

            _personIdsForDescriptions.Add(description, d["Person_Id"]);
            return (int)d["Person_Id"];
        }

        public string YesNo(bool b)
        {
            return b ? "Yes" : "No";
        }

        public string Zeros(short nbr)
        {
            string s = "";
            if (nbr > 1)
            {
                for (short i = 1; i < nbr; i++)
                    s += "0";
            }
            return s;
        }

        public string WordWrap(string s, int charPerLine)
        {
            if (s.Length <= charPerLine)
                return s;

            string[] a;
            string s2 = "";
            string currLine = "";

            a = s.Split(' ');
            for (int i = 0; i < a.Length; i++)
            {
                if ((currLine + a[i]).Length > charPerLine)
                {
                    if (s2 == "")
                    {
                        if (i == 0)
                            s2 = a[0];
                        else
                            s2 = currLine;
                    }
                    else
                    {
                        s2 += Environment.NewLine + currLine;
                        currLine = a[i];
                    }
                }
                else
                    currLine += a[i] + " ";
            }
            s2 += Environment.NewLine + currLine;
            return s2;
        }

        public void GetUrgenAlerts()
        {
            urgentAlerts.Clear();
            foreach (Hashtable row in GetMyUrgenAlerts())
                urgentAlerts.Add(new Alert((int)row["alert_id"]));
        }

        public List<Hashtable> GetActiveLocks(int claim_id)
        {
            List<Hashtable> activeLocks = new List<Hashtable>();
            foreach (Hashtable lData in db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Locks_With_Images", claim_id))
            {
                if (((string)lData["locked"]).ToUpper() == "YES")
                    activeLocks.Add(lData);
            }
            return activeLocks;
        }

        public bool WriteEnumsFile(string msg)
        {
            string table;
            IEnumerable rows;

            //create file text
            string FileText = "";
            string FileName = "GeneratedEnums.cs";

            //start the module text
            //usings
            FileText += "using System;" + Environment.NewLine +
                "using System.Collections.Generic;" + Environment.NewLine +
                "using System.Linq;" + Environment.NewLine +
                "using System.Text;" + Environment.NewLine +
                "using System.Threading.Tasks;" + Environment.NewLine + Environment.NewLine;
            //namespace
            FileText += "namespace MedCSX" + Environment.NewLine + "{" + Environment.NewLine;
            FileText += "public static class modGeneratedEnums" + Environment.NewLine + "{" + Environment.NewLine;

            //process each type table
            foreach (Hashtable d in db.ExecuteStoredProcedureReturnHashList("usp_Get_Type_Tables"))
            {
                table = (string)d["table_name"];
                rows = MedCsXTable.GetTableContents(table);

                //get unique rows which have an enum defined
                ArrayList rows2 = new ArrayList();
                Hashtable enumsAdded = new Hashtable();
                foreach (Hashtable row in rows)
                {
                    string enumName = ((string)row["Enum_Name"]).Trim();
                    if ((enumName != string.Empty) && (!enumsAdded.ContainsKey(enumName)))
                    {
                        rows2.Add(row);
                        enumsAdded.Add(enumName, 1);
                    }
                }

                if (rows2.Count > 0)
                {
                    //write enum name
                    FileText += Environment.NewLine + "public enum " + table.Replace("_", string.Empty) + " : int" + Environment.NewLine + "{";
                    int comma = 0;
                    foreach (Hashtable row in rows2)
                    {
                        if (comma != 0)
                            FileText += ",";
                        comma++;
                        FileText += Environment.NewLine + "    " + ((string)row["Enum_Name"]).Trim() + " = " + (string)row[table + "_Id"];
                    }
                    FileText += Environment.NewLine + "};" + Environment.NewLine;
                }
            }
            FileText += Environment.NewLine + "}";

            //create the file

            Functions.WriteFile(FileName, FileText);

            msg = FileName + " Successfully Written.";
            return true;
        }

        public void UpdateReportParameter(string parameterName, int value)
        {
            db.ExecuteStoredProcedure("usp_Update_Report_Parmeter_Int", parameterName, value);
        }

        public short UppercaseCount(string s)
        {
            short count = 0;
            string c = "";
            for (int i = 0; i < s.Length; i++)
            {
                c = s.Substring(i, 1);
                if (!modLogic.isNumeric(c) && (c == c.ToUpper()))
                    count++;
            }
            return count;
        }

        public short SpecialCount(string s)
        {
            short count = 0;
            string c = "";
            int a = 0;
            for (int i = 0; i < s.Length; i++)
            {
                c = s.Substring(i, 1);
                a = Convert.ToInt32(c.ToUpper());

                if (!modLogic.isNumeric(c))
                {
                    if ((a < 65) || (a > 90))
                        count++;
                }
            }
            return count;
        }

        public short NumericCount(string s)
        {
            short count = 0;
            string c = "";
            for (int i = 0; i < s.Length; i++)
            {
                c = s.Substring(i, 1);
                if (modLogic.isNumeric(c))
                    count++;
            }
            return count;
        }

        public double CurrencyToDouble(string s)
        {
            string s2 = s.Trim().Replace("$", string.Empty).Replace(" ", string.Empty).Replace(",", string.Empty);
            if (s2 == "")
                s2 = "0";
            return double.Parse(s2);
        }

        public DataTable GetSubClaimTypes(int isActive)
        {
            return db.ExecuteStoredProcedure("usp_Get_Sub_Claim_Types", isActive);
        }

        public DataTable GetDiagnosticTypes()
        {
            return db.ExecuteStoredProcedure("usp_Get_Diagnostic_Types");
        }

        public int GetCompanyId(int companyLocId)
        {
            return db.GetIntFromStoredProcedure("usp_Get_Company_Id", companyLocId);
        }

        public List<Hashtable> GetCompanyLocations(string companyId)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Company_Locations", companyId);
        }

        public List<Hashtable> GetPolicyIndividuals(string policyNo, string versionNo, int mode = 0)
        {
            if (mode == 0)
                return db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Individuals", policyNo, versionNo);
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Version_Individuals", policyNo, versionNo);
        }

        public List<Hashtable> GetPolicyVehicles(string policyNo, string versionNo, int mode = 0)
        {
            if (mode == 0)
                return db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Vehicles", policyNo, versionNo);
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Version_Vehicles", policyNo, versionNo);
        }

        public DataTable GetPolicyData(int claim_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Policy_Data", claim_id);
        }

        public DataTable GetAverageReserves()
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Average_Reserves");
        }

        public DataTable GetBIDiagnostics(int bi_evalution_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_BI_Diagnostic_Report", bi_evalution_id);
        }

        public DataTable GetBITreatments(int bi_evalution_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_BI_Treatment_Report", bi_evalution_id);
        }

        public List<Hashtable> GetPolicyEndorsementChanges(string policy_no, decimal version_no)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Endorsement_Changes", policy_no, version_no);
        }

        public List<Hashtable> GetPolicyNotes(string policy_no)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Notes", policy_no);
        }

        public void SearchForPolicies(bool expired, string dol, string policyNo, string firstN, string lastN, string ssn, string vYear, string vMake, string vModel, string vVIN, string streetAddress, string pCity, string pState, string pZip)
        {
            IEnumerable activePolicies = ml.FindPolicies(expired, dol, policyNo, firstN, lastN, ssn, vYear, vMake, vModel, vVIN, streetAddress, pCity, pState, pZip);
        }

        public DataTable GetPhoneNumbersForClaim(int claim_id)
        {
            return db.ExecuteStoredProcedureReturnDataTable("usp_Populate_Phone_Numbers", claim_id);
        }

        public List<Hashtable> GetUnsavedFileNotes(int user_id)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Unsaved_File_Notes", user_id);
        }

        public object ExecuteSelectReturnValue(string sql)
        {
            return db.ExecuteSelectReturnValue(sql);
        }

        public string BuildPhoneNumber(string ph, ref bool isValid, int mode = 0)
        {
            string p = ph.Replace("(", "").Replace(")", "").Replace("-", "").Replace("_", "").Replace(".", "").Replace(" ", "");
            double testInt = 0;
            isValid = false;

            if (!double.TryParse(p, out testInt))
            {
                string[] dig = ph.Split(' ');
                string p2;
                if (dig.Length > 2)
                {
                    p2 = BuildPhoneNumber(dig[0] + " " + dig[1], ref isValid, mode);
                    if (isValid)
                    {
                        for (int i = 2; i < dig.Length; i++)
                            p2 += " " + dig[i];
                        return p2;
                    }
                }
                else if (dig[1].Length > 7)
                {
                    string mask = "(999) 999-9999";
                    int splitter = mask.Length;
                    string p3 = ph.Substring(0, splitter) + " " + ph.Substring(splitter, ph.Length - splitter);
                    string[] dig2 = p3.Split(' ');
                    if (dig2.Length > 2)
                    {
                        p2 = BuildPhoneNumber(dig2[0] + " " + dig2[1], ref isValid, mode);
                        if (isValid)
                        {
                            for (int i = 2; i < dig2.Length; i++)
                                p2 += " " + dig2[i];
                            return p2;
                        }
                    }
                    else
                    {
                        isValid = false;
                        return ph;
                    }
                }
            }

            string phNum = "";
            if (mode == 0)
            {
                switch (p.Length)
                {
                    case 0:
                    case 1:
                    case 2:
                        phNum = p;
                        break;
                    case 3:
                        ph = string.Format("({0})", p);
                        break;
                    case 4:
                    case 5:
                    case 6:
                        phNum = string.Format("({0}) {1}-", p.Substring(0, 3), p.Substring(3, Math.Max(0, p.Length - 3)));
                        break;
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        phNum = string.Format("({0}) {1}-{2}", p.Substring(0, 3), p.Substring(3, 3), p.Substring(6, Math.Max(0, p.Length - 6)));
                        break;
                    default:
                        phNum = string.Format("({0}) {1}-{2}x{3}", p.Substring(0, 3), p.Substring(3, 3), p.Substring(6, 4), p.Substring(10, Math.Max(0, p.Length - 10)));
                        break;
                }
            }
            else
            {
                switch (p.Length)
                {
                    case 0:
                    case 1:
                    case 2:
                        phNum = p;
                        break;
                    case 3:
                        ph = string.Format("{0}", p);
                        break;
                    case 4:
                    case 5:
                    case 6:
                        phNum = string.Format("{0}-{1}-", p.Substring(0, 3), p.Substring(3, Math.Max(0, p.Length - 3)));
                        break;
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        phNum = string.Format("{0}-{1}-{2}", p.Substring(0, 3), p.Substring(3, 3), p.Substring(6, Math.Max(0, p.Length - 6)));
                        break;
                    default:
                        phNum = string.Format("{0}-{1}-{2}x{3}", p.Substring(0, 3), p.Substring(3, 3), p.Substring(6, 4), p.Substring(10, Math.Max(0, p.Length - 10)));
                        break;
                }
            }
            isValid = true;
            return phNum;
        }


        #endregion

        #region Vehicle Methods
        public List<VehicleGrid> LoadVehicles(IEnumerable vData, int mode = 0)
        {
            int counter = 0;
            int testInt;
            List<VehicleGrid> myVehiclesList = new List<VehicleGrid>();
            try
            {
                foreach (Hashtable veh in vData)
                {
                    VehicleGrid vg = new VehicleGrid();
                    switch (mode)
                    {
                        case 1:
                            vg.vehicleId = (int)veh["Vehicle_Id"];
                            vg.vName = (string)veh["Name"];
                            vg.vType = (string)veh["Vehicle_Type"];
                            vg.vShortName = (string)veh["Vehicle_Short_Name"];
                            break;
                        case 2:     //policy grid
                            vg.vehicleId = Convert.ToInt32((byte)veh["No_Vehicle"]);
                            vg.vYear = int.Parse((string)veh["Veh_YR"]);
                            vg.vMake = (string)veh["Veh_Mnfc"];
                            vg.vModel = (string)veh["Veh_Model"];
                            vg.vVIN = (string)veh["Veh_VIN"];
                            break;
                        default:
                            vg.vehicleId = (int)veh["Vehicle_Id"];
                            vg.claimId = (int)veh["Claim_Id"];
                            vg.vMake = (string)veh["Manufacturer"];
                            if ((vg.vMake.Length < 7) || ((vg.vMake.Length >= 7) && (vg.vMake.Substring(0, 7) != "Unknown")))
                            {
                                vg.policyId = (int)veh["Policy_Vehicle_Id"];
                                if (int.TryParse((string)veh["Year_Made"], out testInt))
                                    vg.vYear = testInt;
                                else
                                    vg.vYear = 0;
                                vg.vModel = (string)veh["Model"];
                                vg.vLicenseNo = (string)veh["License_Plate_No"];
                                vg.vVIN = (string)veh["VIN"];
                                vg.vColor = (string)veh["Color"];
                                vg.vState = GetStateForId((int)veh["License_Plate_State_Id"]);
                            }
                            break;
                    }
                    myVehiclesList.Add(vg);
                }
                return myVehiclesList;
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
                return myVehiclesList;
            }
        }

        public bool VIN_Checksum(string VIN)
        {
            char[] vin = VIN.ToUpper().ToCharArray();

            if (vin.Length != 17)
            {
                //TODO*
                //System.Windows.MessageBox.Show("VIN is not valid.", "Invalid VIN", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                return false;
            }
            int vValue = 0;
            int remainder;
            int sumProduct = 0;
            string ckSum;
            char pos9 = ' ';
            for (int pos = 0; pos < vin.Length; pos++)
            {
                int product = 0;

                if (pos == 8)
                    pos9 = vin[pos];
                else
                {
                    if (!int.TryParse(vin[pos].ToString(), out vValue))
                        vValue = Transliterate(vin[pos]);

                    product = vValue * VIN_Weight(pos + 1);
                }
                sumProduct += product;
            }
            remainder = sumProduct % 11;

            ckSum = remainder == 10 ? "X" : remainder.ToString();
            return ckSum == pos9.ToString();
        }

        private int VIN_Weight(int p)
        {
            switch (p)
            {
                case 1:
                case 11:
                    return 8;
                case 2:
                case 12:
                    return 7;
                case 3:
                case 13:
                    return 6;
                case 4:
                case 14:
                    return 5;
                case 5:
                case 15:
                    return 4;
                case 6:
                case 16:
                    return 3;
                case 7:
                case 17:
                    return 2;
                case 8:
                    return 10;
                case 10:
                    return 9;
                default:
                    return 0;
            }
        }

        private int Transliterate(char p)
        {
            switch (p)
            {
                case 'A':
                case 'J':
                    return 1;
                case 'B':
                case 'K':
                case 'S':
                    return 2;
                case 'C':
                case 'L':
                case 'T':
                    return 3;
                case 'D':
                case 'M':
                case 'U':
                    return 4;
                case 'E':
                case 'N':
                case 'V':
                    return 5;
                case 'F':
                case 'W':
                    return 6;
                case 'G':
                case 'P':
                case 'X':
                    return 7;
                case 'H':
                case 'Y':
                    return 8;
                case 'R':
                case 'Z':
                    return 9;
                default:
                    //TODO*
                    //System.Windows.MessageBox.Show("The VIN has invalid character " + p.ToString() + ".", "Invalid VIN", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return 0;
            }
        }

        public int GetVehicleIdForVIN(string vin, int claim_id)
        {
            return db.GetIntFromStoredProcedure("usp_Get_Vehicle_Id_For_VIN", vin, claim_id);
        }
        #endregion


        internal DataTable getCompanyLimits(int claim_id)
        {
            string sql = "SELECT lv.*, p.PPSPolicyEffectiveDate, p.PPSPolicyExpirationDate";
            sql += " From GAAS01.dbo.MJI_Habitat_Policy p";
            sql += " inner join Claims.dbo.Claim c on p.PolicyNumber = c.Policy_No";
            sql += " inner join GAAS01.dbo.MJI_Habitat_Transaction_Header th on p.HabitatPolicyKey = th.HabitatPolicyKey";
            sql += " inner join GAAS01.dbo.MJI_Habitat_Transaction_Location tl On tl.HabitatTransactionHeaderKey = th.HabitatTransactionHeaderKey";
            sql += " inner join GAAS01.dbo.MJI_Habitat_Location_Values lv on lv.HabitatLocationValuesKey = tl.HabitatLocationValuesKey";
            sql += " where c.Claim_Id = '" + claim_id.ToString() + "'";
            sql += " And c.Loss_Location_Nbr = tl.HabitatLocationKey";

            return db.ExecuteSelect(sql);
        }


        internal DataTable GetClosedAppraisals()
        {
            string sql = "SELECT a.created_date, c.display_claim_id, dbo.get_vehicle_name(a.vehicle_id) as vehicle, ";
            sql += "2 as ImageIndex, a.Task_Description, a.vehicle_id, c.claim_id, a.appraisal_task_id, ";
            sql += "dbo.get_user_display_name(c.adjuster_id) as Adjuster, a.Appraiser_Response_Date ";
            sql += "FROM vendor v, scene_access_appraiser s, appraisal_task a, appraisal_request ar, claim c ";
            sql += "WHERE v.vendor_id=s.vendor_id ";
            sql += "AND a.appraisal_request_id = ar.appraisal_request_id ";
            sql += "AND s.scene_access_appraiser_id = A.scene_access_appraiser_id ";
            sql += "AND a.appraisal_task_status_id = 3 "; //closed, cancelled 
            sql += "AND ar.claim_id = c.claim_id ";
            sql += "AND c.claim_status_id = 6 "; //closed
            sql += "order by a.Created_Date desc";

            return db.ExecuteSelect(sql);
        }
    }
}
﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/SiteUnsecured.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MedCSX._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
       <div class="jumbotron">
    <h1>MedCsX</h1>
</div>

    <div class="row">
    <div class="col-md-8">
        <section id="loginForm">
            
               
                <hr />
             <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="text-danger" />
          
                <div class="form-group">
                     <div class="col-md-10">
                   Username:
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Username is required." class="text-danger" ControlToValidate="txtUsername" EnableViewState="False" ></asp:RequiredFieldValidator>
                         </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                     <div class="col-md-10">
                    Password:
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Password is required." class="text-danger" ControlToValidate="txtPassword" EnableViewState="False" ></asp:RequiredFieldValidator>--%>
                         </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                     <div class="col-md-10">
                        <asp:Button ID="btnLogin" runat="server" class="btn btn-info btn-xs"  Text="Login" OnClick="btnLogin_Click" />
                         
                    &nbsp;<asp:Label ID="lblMessage" ForeColor="Red" runat="server"></asp:Label>
                          &nbsp;
                        <asp:LinkButton ID="hllChangePassword" OnClick="hllChangePassword_Click" CausesValidation="true" runat="server">Change Password</asp:LinkButton>
                         
                    </div>
                </div>

                
           
        </section>
    </div>
    <div class="col-md-4">
       

    </div>
</div>

</asp:Content>

﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MedCsxLogic;
using MedCsxDatabase;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.IO;
using System.Drawing.Printing;
using System.Web.Services.Protocols;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using System.Windows;
using MedJames.ServiceWrappers;
using System.Web.UI;

namespace MedCSX
{
    public class ModForms
    {
        private static modLogic ml = new modLogic();
        private static clsDatabase db = new clsDatabase();
        private static ModMain mm = new ModMain();
       
        private string msg = string.Empty;
        private string reasonPart = string.Empty;
        private string noLockPart = ".  The Reserve in a Locked State Until Approved by your Supervisor.";
        private string lockedPart = ".  This would normally add a lock to the reserve, but since it's alread locked, the system will just create a note instead.";
        private int m_Claim_Id;
        //private ImageList ilTypeTables = null;
        //private ImageList ilStates = null;
        private List<int> AllItemImages = new List<int>();
        private List<int> AllItemIDs = new List<int>();
        private List<Hashtable> AllItemsWithIds = new List<Hashtable>();

        static string svcEndPoint = System.Configuration.ConfigurationManager.AppSettings["serviceEndPoint"];
        string IRuser = ConfigurationManager.AppSettings["serviceUserName"];
        string IRpswd = ConfigurationManager.AppSettings["servicePassword"];
        string IRinstance = ConfigurationManager.AppSettings["serviceInstance"];
        MedJames.ServiceWrappers.ImageRight.Implementation.AuthenticationService authService = new MedJames.ServiceWrappers.ImageRight.Implementation.AuthenticationService(svcEndPoint);
        MedJames.ServiceWrappers.ImageRight.Implementation.DrawerService drService = new MedJames.ServiceWrappers.ImageRight.Implementation.DrawerService(svcEndPoint);
        MedJames.ServiceWrappers.ImageRight.Implementation.FileService fileService = new MedJames.ServiceWrappers.ImageRight.Implementation.FileService(svcEndPoint);

        MedJames.ServiceWrappers.ImageRight.Interfaces.ISessionToken sessionToken;
        MedJames.ServiceWrappers.ImageRight.Implementation.DocumentService docService = new MedJames.ServiceWrappers.ImageRight.Implementation.DocumentService(svcEndPoint);
        MedJames.ServiceWrappers.ImageRight.Implementation.DocumentTypeService docTypeService = new MedJames.ServiceWrappers.ImageRight.Implementation.DocumentTypeService(svcEndPoint);
        MedJames.ServiceWrappers.ImageRight.Implementation.PageService pgService = new MedJames.ServiceWrappers.ImageRight.Implementation.PageService(svcEndPoint);


         public void DisplayMap(string address, string city, string state, string zip)
       {
           try
           {
               mm.UpdateLastActivityTime();

               //open the map in the web brower
               //Process.Start("http://www.google.com/maps?q=" + address.Trim().Replace(" ", "+") + city.Trim().Replace(" ", "+") + state.Trim().Replace(" ", "+") + zip.Trim().Replace(" ", "+") + "+USA");
           }
           catch (Exception ex)
           {
               ProcessError(ex);
           }
       }

       
        public void UnlockClaimLock(int claimLockId, string fn, ref string msg)
        {
            //try
            //{
                mm.UpdateLastActivityTime();

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                {
                    msg = "Not authorized to modify Claim Data";
                        return;
                }

                 MedCsxLogic.ClaimLock cl = new ClaimLock(claimLockId);

                //check for authority to unlock - user must be in chain of command above assigned adjuster
                if (!ml.CurrentUser.isManagement())
                {
                    msg = "You must be a supervisor or above to clear this lock.";
                    return;
                }

                //get required text
                string s = "Claim " + cl.Claim().DisplayClaimId;
                s += " Unlocked By " + ml.CurrentUser.Name;
                s += Environment.NewLine + fn;

                //unlock claim
                cl.Unlock(s);


            //}
            //catch (Exception ex)
            //{
            //    ProcessError(ex);
            //}
        }

        ///// <summary>
        ///// Unlock reserve associated with reserveLockId
        ///// </summary>
        ///// <param name="reserveLockId">Reserve_Lock_Id from Reserve_Lock table</param>
        public void UnlockReserveLock(int reserveLockId, string fn, ref string msg)
        {
            try
            {
               
                string s; //required text

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
                    return;

                MedCsxLogic.ReserveLock rl = new ReserveLock(reserveLockId);

                //check for authority to unlock - user must be in chain of command above assigned adjuster
                if (!ml.CurrentUser.isManagement())
                {
                    msg = "You must be a supervisor or above to clear this lock.";
                    //System.Windows.MessageBox.Show("You must be a supervisor or above to clear this lock.", "Insufficient Authority",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return;
                }

                MedCsxLogic.Reserve res = rl.Reserve();

                //check for financial authority to unlock reserve
                if (res.ReserveType.UserAuthority() < rl.LockFileNote().AuthorityNeededToUnlock)
                {
                    msg = "You do not have sufficient financial authority to unlock this reserve.";
                    //System.Windows.MessageBox.Show("You do not have sufficient financial authority to unlock this reserve.", "Insufficient Financial Authority",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return;
                }

                //get required text
                s = res.ReserveType.Description + "Reserve For " + res.ClaimantName();
                s += " Unlocked By " + ml.CurrentUser.Name;

                //prompt for file note text
                //string f = Microsoft.VisualBasic.Interaction.InputBox("Enter File Note Text", "File Note Text");
                //if (f == "")
                //    return;

                s += Environment.NewLine + fn;

                //unlock claim
                rl.Unlock(s);
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }
        internal IEnumerable getIRDocumentsByClaim(string policyNumber, string policyDrawer = "CLMS", string docTypeCode = "ADJR", string name = "Document Via Wrapper 1", string description = "Document Via Wrapper 1", bool includeContent = true)
        {
            MedJames.ServiceWrappers.ImageRight.Interfaces.IDrawer drawer = null;
            MedJames.ServiceWrappers.ImageRight.Model.File mFile;
            IEnumerable<MedJames.ServiceWrappers.ImageRight.Interfaces.IDocument> documents;
            bool foundit = false;

            authService.Authenticate(IRuser, IRpswd, IRinstance);   //set up session information
            sessionToken = authService.SessionToken;
            foreach (MedJames.ServiceWrappers.ImageRight.Interfaces.IDrawer thisDrawer in drService.GetDrawers(sessionToken, false))
            {
                if (thisDrawer.Name == policyDrawer && !foundit)
                {
                    drawer = thisDrawer;
                    foundit = true;
                    break;
                }
            }

            mFile = fileService.GetFile(sessionToken, drawer, policyNumber, includeContent, false) as MedJames.ServiceWrappers.ImageRight.Model.File;
            documents = docService.GetDocuments(sessionToken, drawer, policyNumber);
            return documents;
        }

        public void ShowBIReport(int biEvaluationId)
        {
            try
            {
                ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fBodily+Injury+Evaluation+Report&rs:Command=Render&bi_evaluation_id=" + biEvaluationId);
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }

        internal IEnumerable getIRPagesByDoc(long docRefId)
        {
            List<MedJames.ServiceWrappers.ImageRight.Interfaces.IPage> pages;
            authService.Authenticate(IRuser, IRpswd, IRinstance);
            sessionToken = authService.SessionToken;

            pages = pgService.GetPages(sessionToken, docRefId, true);
            return pages;
        }

        public string DetermineClaimsImageRightDrawer(int p)
        {
            return mm.ClaimsDrawer;
        }

        //public bool ReconReserve(int reserve_id)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();

        //        if (reserve_id == 0)
        //            return false;

        //        string s;
        //        Reserve res = new Reserve(reserve_id);
        //        bool goodEntry = false;

        //        while (!goodEntry)
        //        {
        //            s = Microsoft.VisualBasic.Interaction.InputBox("Enter New Recon Reserve Amount.", "Recon Reserve");

        //            if (s == "")
        //                return false;

        //            if (!modLogic.isNumeric(s))
        //                System.Windows.MessageBox.Show("Recon Reserve Amount Must be Numeric", "Invalid Amount", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
        //            else if (double.Parse(s) > res.NetLossReserve)
        //                System.Windows.MessageBox.Show("Recon Reserve Amount cannot be greater than the Net Loss Reserve.", "Recon Amount", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
        //            else
        //            {
        //                goodEntry = true;
        //                return (ReconReserve(reserve_id, double.Parse(s)));
        //            }
        //        }
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //        return false;
        //    }
        //}

        public bool VoidReserve(int reserveId, ref string msg)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return false;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Void_Reserve))
                {
                    msg = "Not Authorized";
                    return false;
                }
                

                Reserve res = new Reserve(reserveId);
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Void)
                {
                    //System.Windows.MessageBox.Show("This Reserve Has Already Been Voided", "Reserve Already Voided",
                    //System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
                    msg = "This Reserve Has Already Been Voided";
                    return false;
                }

                //if (System.Windows.MessageBox.Show("Are You Sure You Want to Void This Reserve?", "Void Reserve",
                //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                //    return false;

                if (!res.canVoid)
                {
                    msg = "Reserve Cannot be Voided Because it has Transactions Posted Against it.";
                    //System.Windows.MessageBox.Show("Reserve Cannot be Voided Because it has Transactions Posted Against it.",
                    //     "Cannot Void Reserve", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return false;
                }

                res.Void();
                return true;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return false;
            }
        }

        public bool ClosePendingSalvage(int reserveId, ref string msg)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return false;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Pending_Salvage))
                    return false;

                Reserve res = new Reserve(reserveId);
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage)
                {
                    msg = "This Reserve is Already Closed Pending Salvage";
                    //System.Windows.MessageBox.Show("This Reserve is Already Closed Pending Salvage", "Reserve Already Closed Pending Salvage",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
                    return false;
                }

                //if (System.Windows.MessageBox.Show("Are You Sure You Want to Close Pending Salvage?", "Close Pending Salvage",
                //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                //    return false;

                res.ClosePendingSalvage();
                return true;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return false;
            }
        }

        //public bool ClosePendingSIRRecovery(int reserveId)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        if (reserveId == 0)
        //            return false;

        //        if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Pending_SIR_Recovery))
        //            return false;

        //        Reserve res = new Reserve(reserveId);
        //        if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_SIR_Recovery)
        //        {
        //            System.Windows.MessageBox.Show("This Reserve is Already Closed Pending SIR Recovery", "Reserve Already Closed Pending SIR Recovery",
        //                System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
        //            return false;
        //        }

        //        if (System.Windows.MessageBox.Show("Are You Sure You Want to Close Pending SIR Recovery?", "Close Pending SIR Recovery",
        //            System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
        //            return false;

        //        res.ClosePendingSIRRecovery();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //        return false;
        //    }
        //}

        public bool ClosePendingDeductibles(int reserveId, ref string msg)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return false;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Pending_Deductibles))
                    return false;

                Reserve res = new Reserve(reserveId);
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Deductibles)
                {
                    msg = "This Reserve is Already Closed Pending Deductibles";
                    //System.Windows.MessageBox.Show("This Reserve is Already Closed Pending Deductibles", "Reserve Already Closed Pending Deductibles",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
                    return false;
                }

                //if (System.Windows.MessageBox.Show("Are You Sure You Want to Close Pending Deductibles?", "Close Pending Deductibles",
                //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                //    return false;

                res.ClosePendingDeductibles();
                return true;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return false;
            }
        }

        public bool ReconReserve(int reserve_id, double p, ref string msg)
        {
            Reserve res = new Reserve(reserve_id);
            double oldAmount = 0,
                dblDifference = 0;
            int recon_id = 0;
            DataTable dsRecon = null;
            Recon Recon = null;
            oldAmount = res.NetLossReserve;
            dblDifference = oldAmount - p;

            Hashtable eventTypeIds = new Hashtable(),       //event type IDs for firing events
                parms;                                      //parameters for events
            double perPerson = 0,                         //per-person coverage
                perAccident = 0,                          //per-accident coverage
                previousPayments,                      //previous payments on reserve line
                userAuthority;                           //user's financial authority for reserve line
            int periodTypeId = 0,                         //per-person period type
                periods = 0,                              //number of per-person periods
                reserveTypeId,                          //reserve type ID
                _claim_id;                              //claim ID for reserve
            bool deductible = false;                    //reserve has deductible? (comp/collision)

            _claim_id = res.ClaimId;
            reserveTypeId = res.ReserveTypeId;

            PolicyCoverage pc = res.PolicyCoverage();
            if (pc != null)
            {
                perPerson = pc.PerPersonCoverage;
                periodTypeId = pc.PerPersonPeriodTypeId;
                periods = pc.PerPersonNoPeriods;
                perAccident = pc.PerAccidentCoverage;
                deductible = pc.hasDeductible;
            }

            if (p >= oldAmount)
            {
                //amount exceeding authority
                previousPayments = res.TotalLossPayments;
                userAuthority = res.UserAuthority();

                if (p + previousPayments > userAuthority)
                {
                    reasonPart = "The Amount Entered of " + p.ToString("c") + " Plus Previous Payments of " + previousPayments.ToString("c") + " Exceeds Your Financial Authority of " + userAuthority.ToString("c");
                    msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;

                    //if (System.Windows.MessageBox.Show(msg, "Loss Reserve Exceeds Authority", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                        //return false;

                    parms = new Hashtable();
                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("User_Authority", userAuthority);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed_Above_Adjuster_Authority, parms);
                }

                if (!deductible)
                {   //has coverage limits
                    if (!res.Claim.PolicyManuallyInForce)
                    {   //not manually unlocked
                        if ((perPerson == 0) && (perAccident == 0))
                        {
                            reasonPart = "There is No Coverage for this Reserve";
                            msg = reasonPart + noLockPart;
                            if (res.isLocked)
                                msg = reasonPart + lockedPart;

                            //if (System.Windows.MessageBox.Show(msg, "No Coverage for Reserve", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                                //return false;

                            PolicyCoverageType pct = res.PolicyCoverageType();
                            if (pct.hasPerPersonCoverage)
                            {
                                parms = new Hashtable();
                                parms.Add("Previous_Payments", previousPayments);
                                parms.Add("Per_Person_Coverage", perPerson);
                                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                            }
                            else if (pct.hasPerAccidentCoverage)
                            {
                                parms = new Hashtable();
                                parms.Add("Previous_Payments", previousPayments);
                                parms.Add("Per_Accident_Coverage", perAccident);
                                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, parms);
                            }
                        }
                        else
                        {
                            if ((perPerson > 0) && (periodTypeId == (int)modGeneratedEnums.PeriodType.Flat) && (p + previousPayments > perPerson))
                            {   //amount exceeding per-person coverage (Flat)
                                reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Person Coverage of " + perPerson.ToString("c");
                                msg = reasonPart + noLockPart;
                                if (res.isLocked)
                                    msg = reasonPart + lockedPart;

                                //if (System.Windows.MessageBox.Show(msg, "Amount Exceeding Per-Person Coverage (Flat)", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                                    //return false;

                                parms = new Hashtable();
                                parms.Add("Previous_Payments", previousPayments);
                                parms.Add("Per_Person_Coverage", perPerson);
                                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                            }

                            if ((perPerson > 0) && (periodTypeId != (int)modGeneratedEnums.PeriodType.Flat) && (p + previousPayments > (perPerson * periods)))
                            {   //amount exceeding per-person coverage (Periodic)
                                reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Person Coverage of " + res.CoverageDescription;
                                msg = reasonPart + noLockPart;
                                if (res.isLocked)
                                    msg = reasonPart + lockedPart;

                                //if (System.Windows.MessageBox.Show(msg, "Amount Exceeding Per-Person Coverage (Periodic)", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                                    //return false;

                                parms = new Hashtable();
                                parms.Add("Previous_Payments", previousPayments);
                                parms.Add("Per_Person_Coverage", perPerson * periods);
                                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                            }

                            if ((perAccident > 0) && (p - oldAmount + res.Claim.LossForCoverageType(reserveTypeId) + res.Claim.NetLossReserveForCoverageType(reserveTypeId) > perAccident))
                            {
                                reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Accident Coverage of " + perAccident.ToString("c");
                                msg = reasonPart + noLockPart;
                                if (res.isLocked)
                                    msg = reasonPart + lockedPart;

                                //if (System.Windows.MessageBox.Show(msg, "Amount Exceeding Per-Accident Coverage", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                                    //return false;

                                parms = new Hashtable();
                                parms.Add("Reserve_Amount", p);
                                parms.Add("Per_Accident_Coverage", perAccident);
                                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, parms);
                            }
                        }
                    }
                }
            }

            if (eventTypeIds.Count == 0)
            {
                parms = new Hashtable();
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
            }

            res.ChangeNetLossReserve(ref p, eventTypeIds);
            dsRecon = res.getReconRows(reserve_id, res.ClaimId);
            if (dsRecon.Rows.Count > 0)
            {
                recon_id = (int)dsRecon.Rows[0]["Recon_Id"];
                Recon = new Recon(recon_id);
                Recon.ReserveAtRecon = oldAmount;
                Recon.ReserveAfterRecon = p;
                Recon.ReserveDifference = dblDifference;
                Recon.Update();
            }
            else
            {
                Recon = new Recon();
                Recon.ReserveAtRecon = oldAmount;
                Recon.ReserveAfterRecon = p;
                Recon.ReserveDifference = dblDifference;
                Recon.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                Recon.ReserveId = reserve_id;
                Recon.ClaimId = res.ClaimId;
                Recon.Update();
            }
            return true;
        }

        public void SetReserveAsTotalLoss(int p)
        {
            try
            {
                mm.UpdateLastActivityTime();

                if (p == 0)
                    return;

                //update reserve
                Reserve res = new Reserve(p);
                res.isTotalLoss = true;
                res.Update();
                res = null;

               
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }

        public bool ClosePendingSubro(int reserveId, ref string msg)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return false;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Pending_Subro))
                    return false;

                Reserve res = new Reserve(reserveId);
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro)
                {
                    msg = "This Reserve is Already Closed Pending Subro";
                    //System.Windows.MessageBox.Show("This Reserve is Already Closed Pending Subro", "Reserve Already Closed Pending Subro",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
                    return false;
                }

                //if (System.Windows.MessageBox.Show("Are You Sure You Want to Close Pending Subro?", "Close Pending Subro",
                //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                //    return false;

                res.ClosePendingSubro();
                return true;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return false;
            }
        }

        //public void CloseReserve(int reserveId)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        if (reserveId == 0)
        //            return;

        //        if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Reserve))
        //            return;

        //        Reserve res = new Reserve(reserveId);
        //        //if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed)
        //        //{
        //        //    System.Windows.MessageBox.Show("This Reserve is Already Closed", "Reserve Already Closed",
        //        //         System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
        //        //    return;
        //        //}

        //        //if (System.Windows.MessageBox.Show("Are You Sure You Want to Close this Reserve?", "Close Reserve",
        //        //   System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
        //        //    return;

        //        if (res.getNoUnprintedDrafts() > 0)
        //        {
        //            //TODO
        //            //frmPicker2 fd = new frmPicker2(reserveId);
        //            //fd.Show();
        //            return;
        //        }

        //        //if APD, Comp, or Collision, ask if Total Loss.  Yes - Close Pending Salvage instead.
        //        if ((res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage) &&
        //            ((res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Collision) ||
        //            (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comprehensive) ||
        //            (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Comp_Collision) ||
        //            (res.ReserveTypeId == (int)modGeneratedEnums.ReserveType.Property_Damage)))
        //        {
        //            if (System.Windows.MessageBox.Show("Is this Reserve a Total Loss?", "Total Loss", System.Windows.MessageBoxButton.YesNo,
        //                System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.Yes)
        //            {
        //                res.isTotalLoss = true;
        //                res.Update();
        //                System.Windows.MessageBox.Show("Reserve Will be Closed Pending Salvage", "Pending Salvage", System.Windows.MessageBoxButton.OK,
        //                    System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.OK);
        //                res.ClosePendingSalvage();
        //                return;
        //            }
        //        }

        //        int denialReasonTypeId = 0;
        //        if (res.PaidLoss == 0)  //if there are no loss payments, ask for denial reason
        //        {
        //            //TODO
        //            //frmSmallWindow f = new frmSmallWindow("Denial_Reason_Type", 1);
        //            //f.ShowDialog();
        //            //if (!f.okPressed)
        //            //    return;

        //            //denialReasonTypeId = f.voidReasonId;
        //        }

        //        res.Close(denialReasonTypeId); //close the reserve
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}

        public void PrintDraft(int draftId, bool isCheckRequestOnly)
        {
            //update the last activity time
            mm.UpdateLastActivityTime();

            //insert Print_Draft row
            int draftPrintId = Draft.InsertDraftPrint(draftId);

            //update draft
            Draft draft = new Draft(draftId);
            draft.PrintDate = ml.DBNow();
            m_Claim_Id = draft.ClaimId;

            //if (MedCsXSystem.Settings.DraftPrintingCurrentlyOn)
            //{
            //    draft.InDraftQueue = 0; //false
            //    draft.ActualPrintDate = ml.DBNow();
            //    draft.Update();

            //    try
            //    {
            //        if (!isCheckRequestOnly)
            //        {
            //            //print draft on draft printer
            //            string msg = "";
            //            if (PrintDraftForDraftPrintId(draftPrintId, ref msg))
            //            {
            //                draft.isPrinted = true;
            //                draft.Update();
            //            }
                        
            //        }
            //        else
            //        {
            //            string checkRequestReport = ConfigurationManager.AppSettings["CheckRequestReportUrl"];
            //            ShowReport(String.Format("{0}{1}", checkRequestReport, draftPrintId));  //London?
            //            draft.isPrinted = true;
            //        }
            //    }
            //    catch (Exception ex)
            //    {


            //        string msg = ml.CurrentUser.Name + " - Error occurred while printing the draft.  The draft (draftPrintId = " +
            //             draftPrintId.ToString() + ") " + Environment.NewLine +
            //             Environment.NewLine + "Error Information:" + Environment.NewLine + ex.ToString() +
            //             Environment.NewLine + Environment.NewLine + ex.Message.ToString();

            //        MedCsxObject.SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, "bmoyet@medjames.com", "bmoyet@medjames.com",
            // "Error Printing Checks", msg, false);

            //        //error occurred while printing - put the draft into the draft queue
            //        draft.InDraftQueue = 1;
            //        draft.Update();
            //    }
            //}
            //else
            //{
                draft.InDraftQueue = 1;
                draft.Update();
            //}
        }

        private void SendEmail(string appraisalSMTPServer, string appraisalEmailAddress, object email, object p1, object p2, bool v)
        {
            throw new NotImplementedException();
        }

        public bool MicrFontsExist()
        {
            try
            {
                if (System.IO.File.Exists("c:/windows/fonts/Micr.TTF"))
                    return true;

                if (System.IO.File.Exists("c:/winnt/fonts/Micr.TTF"))
                    return true;
                //HttpResponse.Write("<script>alert('MICR Fonts are not installed on this machine.  They must be installed in order to print drafts.');</script>");

                //System.Windows.MessageBox.Show("MICR Fonts are not installed on this machine.  They must be installed in order to print drafts",
                //    "MICR Fonts Not Installed", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.OK);
                return false;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return true;
            }
        }

        public void ShowTransactionReport(int reserveId)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (reserveId == 0)
                    return;
                ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fDraft+Transaction+Detail&rs:Command=Render&reserveid=" + reserveId);
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }



        public void ShowReport(string url)
        {
            //url += " + --new- window";
            try
            {

                mm.UpdateLastActivityTime();
                Process.Start(url, "new window");  //open url in browser
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }

        public void ProcessError(Exception e)
        {
            //Update the last activity time
            mm.UpdateLastActivityTime();
            //MouseNormal(); //Set the cursor back to normal
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ////Display error
            ////WebMsgBox.Show("MedCsX Error: " + e.ToString());
            //page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert(''MecCsX Error: " + e.ToString() + "');</script>");
            ////page.RegisterClientScriptBlock("msgBox1", "<script>alert('MecCsX Error: " + e.ToString() + "');</script>");
            //System.Windows.MessageBox.Show(e.ToString(), "MecCsX Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error, System.Windows.MessageBoxResult.OK);
            //string msg = "Error occurred while printing the draft.  The draft (draftPrintId = " +
            //            draftPrintId.ToString() + ") has been placed in the draft queue." + Environment.NewLine +
            //            Environment.NewLine + "Error Information:" + Environment.NewLine + ex.ToString() +
            //            Environment.NewLine + Environment.NewLine + ex.Message.ToString();

            MedCsxObject.SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, "bmoyet@medjames.com", "bmoyet@medjames.com", ml.CurrentUser.Name + " - Error: ", e.ToString(), false);
            //throw e;
        }
        public bool CheckForPossibleDuplicateClaims(MedCsxLogic.Claim claim, PossibleDuplicateClaimMode mode)
        {
            if (claim.checkForPossibleDuplicateClaims(mode) > 0)
            {
                return true;
            }
            return false;
        }

       

        //public void ShowClaimSummaryReport(int claimId)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        MedCsxLogic.Claim claim = new MedCsxLogic.Claim(claimId);
        //        claim.populateClaimReport();
        //        ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fClaim+Summary&rs:Command=Render&claim_id=" + claimId);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}

        //public void ShowFileActivityReportDetailed(int claimId)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fDetailed+File+Activity+Report&rs:Command=Render&claim=" + claimId);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}

        public void ShowAllDiaryEntriesForUserReport(int userId)
        {
            //ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fAll+Diary+Entries+For+User&rs:Command=Render&user_id=" + userId);
            ShowReport(System.Configuration.ConfigurationManager.AppSettings["ReportServerURL"] + "All%20Diary%20Entries%20for%20User?user_id=" + userId);
        }

        public void ShowPastDueDiaryEntriesForUserReport(int userId)
        {
            ShowReport(System.Configuration.ConfigurationManager.AppSettings["ReportServerURL"] + "Past%20Due%20Diary%20Entries%20For%20User?user_id=" + userId);
        }

        public void ShowPendingSalvageReport()
        {
            ShowReport("http://reports.medjames.com/Reports/Pages/Report.aspx?ItemPath=%2fClaims%2fMedCsx+Pending+Salvage+Report");
        }

        public void ShowPendingPoliceReports()
        {
            ShowReport("http://reports.medjames.com/Reports/Pages/Report.aspx?ItemPath=%2fClaims%2fPending+Police+Reports");
        }

        public void ShowPendingReservesPerAdjusterReport()
        {
            ShowReport("http://reports.medjames.com/Reports/Pages/Report.aspx?ItemPath=%2fClaims%2fPending+Reserves+by+Adjuster");
        }

        //public void ShowFileActivityReport(int claimId)
        //{
        //    try
        //    {
        //        ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fFile+Activity+Report&rs:Command=Render&claim=" + claimId);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}

        //public void ShowFileActivityReportSummary(int claimId)
        //{
        //    try
        //    {
        //        ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fSummary+File+Activity+Report&rs:Command=Render&claim=" + claimId);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}

        //public void ShowUserEnteredFileNotesReport(int claimId)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fUser+Entered+File+Notes&rs:Command=Render&claim=" + claimId);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}

        //public void ShowDraftReport(int claimId)
        //{
        //    ShowReport("http://reports.medjames.com/reportserver?%2fClaims%2fDraft+Report+For+Claim&rs:Command=Render&claim_id=" + claimId);
        //}

        public void getIRDocumentByClaim(string display_claim_id, string drawer)
        {
            MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(mm.ExtractClaimId(display_claim_id));

            string imageRightLoc = @"C:\Program Files (x86)\ImageRight\Clients";
            string irL = "IRLINKER /ACTION:OND /LOCATIONID:-1 /LOCATIONNAME: /DRAWER:";
            irL += drawer;
            irL += " /FILETYPE:" + drawer;
            irL += " /FILENUMBER:" + display_claim_id;
            irL += " /FILENAME:" + (char)34 + myClaim.InsuredPerson.LastName + ", " + myClaim.InsuredPerson.FirstName + (char)34;
            //string IRcmd = imageRightLoc + irL;
            IRProcess(imageRightLoc, irL);
        }

        public string getIRDocumentLinkByClaim(string display_claim_id, string drawer)
        {
            MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(mm.ExtractClaimId(display_claim_id));

            //string imageRightLoc = @"C:\Program Files (x86)\ImageRight\Clients";
            string irL = "ir://LocationId:-1&Drawer:";
            irL += drawer;
            irL += "&FileType:" + drawer;
            irL += "&FileNumber:" + display_claim_id;
            irL += "&FileName:" + myClaim.InsuredPerson.LastName + ", " + myClaim.InsuredPerson.FirstName;
            irL += "&Action:O";
            //string IRcmd = imageRightLoc + irL;
            //IRProcess(imageRightLoc, irL);
            return irL;
        }

        public void getIRDocumentByClaim(string display_claim_id, string drawer, string policy)
        {
            MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(mm.ExtractClaimId(display_claim_id));

            string imageRightLoc = @"C:\Program Files (x86)\ImageRight\Clients";
            string irL = "IRLINKER /ACTION:OND /LOCATIONID:-1 /LOCATIONNAME: /DRAWER:";
            irL += drawer;
            irL += " /FILETYPE:" + drawer;
            irL += " /FILENUMBER:" + policy;
            irL += " /FILENAME:" + (char)34 + myClaim.InsuredPerson.LastName + ", " + myClaim.InsuredPerson.FirstName + (char)34;
            //string IRcmd = imageRightLoc + irL;
            IRProcess(imageRightLoc, irL);
        }

        public string getIRDocumentLinkByClaim(string display_claim_id, string drawer, string policy)
        {
            MedCsxLogic.Claim myClaim = new MedCsxLogic.Claim(mm.ExtractClaimId(display_claim_id));

            //string imageRightLoc = @"C:\Program Files (x86)\ImageRight\Clients";
            string irL = "ir://ACTION:OND&LOCATIONID:-1&LOCATIONNAME:&DRAWER:";
            irL += drawer;
            irL += "&FILETYPE:" + drawer;
            irL += "&FILENUMBER:" + policy;
            irL += "&FILENAME:" + myClaim.InsuredPerson.LastName + ", " + myClaim.InsuredPerson.FirstName;
            //string IRcmd = imageRightLoc + irL;
            //IRProcess(imageRightLoc, irL);

            return irL;
        }

        private void IRProcess(string workingDir, string IRcmd)
        {
            System.Diagnostics.Process process = new Process();
            System.Diagnostics.ProcessStartInfo startInfo = new ProcessStartInfo();

            startInfo.CreateNoWindow = true;
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;

            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + IRcmd;
            //startInfo.Arguments = IRcmd;
            //string stdArg = "/C " + IRcmd;


            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.WorkingDirectory = workingDir;
            process.StartInfo = startInfo;
            var stdOutput = new StringBuilder();
            process.OutputDataReceived += (sender, args) => stdOutput.AppendLine(args.Data);

            string stdError = null;
            try
            {
                process.Start();
                stdError = process.StandardOutput.ReadToEnd();
                process.WaitForExit(2000);
            }
            catch (Exception ex)
            {
                //ProcessError(ex);
                throw new Exception("OS error while executing " + Format("cmd.exe", IRcmd) + ": " + ex.Message, ex);
            }

            if (process.ExitCode != 0)
            {
                var message = new StringBuilder();
                if (!string.IsNullOrEmpty(stdError))
                    message.AppendLine(stdError);

                if (stdOutput.Length != 0)
                {
                    message.AppendLine("Std output:");
                    message.AppendLine(stdOutput.ToString());
                }

                throw new Exception(Format("cmd.exe", IRcmd) + " finished with exit code = " + process.ExitCode + ": " + message);
            }

            if (!process.HasExited)
            {
                process.CloseMainWindow();
                process.Kill();
            }
        }

        private string Format(string p, string IRcmd)
        {
            return "'" + p +
                ((string.IsNullOrEmpty(IRcmd)) ? string.Empty : " " + IRcmd) +
                "'";
        }

        public bool CloseClaim(MedCsxLogic.Claim claim, ref string msg1)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
                {

                    //System.Windows.MessageBox.Show("This Claim is Already Closed", "Claim Already Closed",
                    //     MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK); // MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Claim))
                    return false;

                //confirm close

                //if (System.Windows.MessageBox.Show("Are you sure you want to close the claim?", "Close Claim",
                //    MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No, System.Windows.MessageBoxOptions.None) == MessageBoxResult.No) //MessageBoxDefaultButton.Button2) == DialogResult.No)
                //    return false;

                //MouseBusy(); //set the cursor to busy

                //check for uncleared diary entries
                List<Hashtable> unclearedDiaries = claim.UnclearedDiaryList;
                if (unclearedDiaries.Count > 0)
                {
                    msg1 = "You can not close this claim! Uncleared Diary entries.";
                    return false;
                    //MouseNormal();  //set cursor to normal

                    //            //display the uncleared  diaries
                    //            frmUnclearedDiary f = new frmUnclearedDiary(claim.Id);
                    //            f.ShowDialog();

                    //            if (!f.okPressed)
                    //                return false;
                }

                //check for unprinted drafts
                if (claim.NoUnprintedDraftsForClaim > 0)
                {
                    msg1 = "You can not close this claim! Unprinted Drafts for this Claim.";
                    return false;
                    //MouseNormal();
                    //            frmUnprintedDrafts f = new frmUnprintedDrafts(claim.Id, 0);
                    //            f.Show();
                    //            return false;
                }

                //check for non-closed reserve lines
                string msg = "";
                foreach (Reserve res in claim.Reserves())
                {
                    if ((res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Closed) & (res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Void))
                        msg += res.Description() + Environment.NewLine;
                }

                if (msg != "")
                {
                    msg = "Closing the Claim will Close All of These Reserve Lines.  Are You Sure You Want to Close this Claim?" +
                        Environment.NewLine + Environment.NewLine + msg;
                    //MouseNormal();

                    //if (System.Windows.MessageBox.Show(msg, "Reserve Lines Not Closed", MessageBoxButton.YesNo,
                    //    MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No) //MessageBoxDefaultButton.Button2) == DialogResult.No)
                    //    return false;
                }

                /*Ask about any APD, Comp, or Collision reserve lines which are open.  If it is a total loss,
                 * put in Pending Salvage and don't close the claim.  */


                //Check for reserves without loss payment and ask for denial reason
                foreach (Reserve res in claim.Reserves())
                {
                    if ((res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Closed) && (res.ReserveStatusId != (int)modGeneratedEnums.ReserveStatus.Void))
                    {
                        if (res.PaidLoss == 0)
                        {
                            //frmDenialReason f = new frmDenialReason();
                            //                    f.reserveId = res.Id;
                            //                    f.ShowDialog();

                            //                    if (!f.okPressed)
                            //                        return false;

                            //                    res.Close(f.denialReasonTypeId);
                        }
                        else
                            res.Close();
                    }
                }

                //check for outstanding tasks
                if (claim.OutstandingTasks().Count > 0)
                {
                    //msg1 = "You can not close this claim! There are outstanding tasks.";
                    //return false;
                    //frmOutstandingTasks f = new frmOutstandingTasks(claim);
                    //            f.ShowDialog();

                    //            if (!f.CloseClaim)
                    //                return false;

                    claim.FireEvent((int)modGeneratedEnums.EventType.Claim_Closed_With_Outstanding_Tasks);
                }

                //check for open appraisals
                List<Hashtable> OpenAppraisals = claim.OpenAppraisals();
                if (OpenAppraisals.Count > 0)
                {
                    //send an alert to the appraisers so they know the claim is being closed

                    //create a list of unique appraiser IDs
                    List<int> uniqueIDs = new List<int>();
                    foreach (Hashtable appraisal in OpenAppraisals)
                    {
                        if (!uniqueIDs.Contains((int)appraisal["appraiser_id"]))
                            uniqueIDs.Add((int)appraisal["appraiser_id"]);  //add unique appraiser ID to the list
                    }

                    foreach (int appraiserID in uniqueIDs)
                    {
                        if (appraiserID != 0)
                        {
                            //the appraiser ID is valid
                            Alert claimClosedAlert = new Alert();
                            int noteID = db.GetIntFromStoredProcedure("usp_Get_Claim_Closed_File_Note_ID", claim.Id);

                            if (noteID != 0)  //file note ID is valid
                                SendAlert(noteID, 0, appraiserID); //send alert to the appraiser
                        }
                    }
                }

                //send alert to all of the uncleared diary owners that are not the person closing the claim
                List<int> userIDs = new List<int>();
                int sentToUserId = 0;
                foreach (Hashtable unclearedDiary in unclearedDiaries)
                {
                    //get the user the diary was set for
                    sentToUserId = (int)unclearedDiary["sent_from_user_id"];
                    if (sentToUserId != Convert.ToInt32(HttpContext.Current.Session["userID"]))  //the diary owner is not the current user
                    {
                        if (!userIDs.Contains(sentToUserId))  //this user hasn't been notified yet
                        {
                            userIDs.Add(sentToUserId);

                            //let the diary owner know the claim is being closed
                            int noteID = db.GetIntFromStoredProcedure("usp_Get_Claim_Closed_File_Note_ID", claim.Id);

                            if (noteID != 0)  //file note ID is valid
                                SendAlert(noteID, 0, sentToUserId); //send alert to the diary owner
                        }
                    }
                }
                claim.Close();  //close the claim

                //display success message
                //System.Windows.MessageBox.Show("Claim Closed Successfully", "Claim Closed", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK); //MessageBoxDefaultButton.Button1);

                return true;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return false;
            }
            finally
            {
                //set the cursor back to normal
            }
        }

        public bool CloseClaimWithWarnings(MedCsxLogic.Claim claim, ref string msg1)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
                {

                    //System.Windows.MessageBox.Show("This Claim is Already Closed", "Claim Already Closed",
                    //     MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK); // MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Close_Claim))
                    return false;

               

                //check for uncleared diary entries
                List<Hashtable> unclearedDiaries = claim.UnclearedDiaryList;
                //send alert to all of the uncleared diary owners that are not the person closing the claim
                List<int> userIDs = new List<int>();
                int sentToUserId = 0;
                foreach (Hashtable unclearedDiary in unclearedDiaries)
                {
                    //get the user the diary was set for
                    sentToUserId = (int)unclearedDiary["sent_from_user_id"];
                    if (sentToUserId != Convert.ToInt32(HttpContext.Current.Session["userID"]))  //the diary owner is not the current user
                    {
                        if (!userIDs.Contains(sentToUserId))  //this user hasn't been notified yet
                        {
                            userIDs.Add(sentToUserId);

                            //let the diary owner know the claim is being closed
                            int noteID = db.GetIntFromStoredProcedure("usp_Get_Claim_Closed_File_Note_ID", claim.Id);

                            if (noteID != 0)  //file note ID is valid
                                SendAlert(noteID, 0, sentToUserId); //send alert to the diary owner
                        }
                    }
                }

                ////check for unprinted drafts
                //if (claim.NoUnprintedDraftsForClaim > 0)
                //{
                //    msg1 = "You can not close this claim! Unprinted Drafts for this Claim.";
                //    return false;
                //    //MouseNormal();
                //    //            frmUnprintedDrafts f = new frmUnprintedDrafts(claim.Id, 0);
                //    //            f.Show();
                //    //            return false;
                //}

               


                //check for outstanding tasks
                if (claim.OutstandingTasks().Count > 0)
                {
                    claim.FireEvent((int)modGeneratedEnums.EventType.Claim_Closed_With_Outstanding_Tasks);
                }

                //check for open appraisals
                List<Hashtable> OpenAppraisals = claim.OpenAppraisals();
                if (OpenAppraisals.Count > 0)
                {
                    //send an alert to the appraisers so they know the claim is being closed

                    //create a list of unique appraiser IDs
                    List<int> uniqueIDs = new List<int>();
                    foreach (Hashtable appraisal in OpenAppraisals)
                    {
                        if (!uniqueIDs.Contains((int)appraisal["appraiser_id"]))
                            uniqueIDs.Add((int)appraisal["appraiser_id"]);  //add unique appraiser ID to the list
                    }

                    foreach (int appraiserID in uniqueIDs)
                    {
                        if (appraiserID != 0)
                        {
                            //the appraiser ID is valid
                            Alert claimClosedAlert = new Alert();
                            int noteID = db.GetIntFromStoredProcedure("usp_Get_Claim_Closed_File_Note_ID", claim.Id);

                            if (noteID != 0)  //file note ID is valid
                                SendAlert(noteID, 0, appraiserID); //send alert to the appraiser
                        }
                    }
                }

                //send alert to all of the uncleared diary owners that are not the person closing the claim
               
                foreach (Hashtable unclearedDiary in unclearedDiaries)
                {
                    //get the user the diary was set for
                    sentToUserId = (int)unclearedDiary["sent_from_user_id"];
                    if (sentToUserId != Convert.ToInt32(HttpContext.Current.Session["userID"]))  //the diary owner is not the current user
                    {
                        if (!userIDs.Contains(sentToUserId))  //this user hasn't been notified yet
                        {
                            userIDs.Add(sentToUserId);

                            //let the diary owner know the claim is being closed
                            int noteID = db.GetIntFromStoredProcedure("usp_Get_Claim_Closed_File_Note_ID", claim.Id);

                            if (noteID != 0)  //file note ID is valid
                                SendAlert(noteID, 0, sentToUserId); //send alert to the diary owner
                        }
                    }
                }
                claim.Close();  //close the claim

                //display success message
                //System.Windows.MessageBox.Show("Claim Closed Successfully", "Claim Closed", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK); //MessageBoxDefaultButton.Button1);

                return true;
            }
            catch (Exception ex)
            {
                ProcessError(ex);
                return false;
            }
            finally
            {
                //set the cursor back to normal
            }
        }

        public void RequestPoliceReport(MedCsxLogic.Claim claim)
        {
            //try
            //{
            //    mm.UpdateLastActivityTime();
            //    if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Modify_Claim_Data))
            //        return;

            //    //check for existing police report for claim
            //    int nbr = db.GetIntFromStoredProcedure("usp_No_ChoicePoint_Requests_For_Claim", claim.Id);
            //    if (nbr > 0)
            //    {
            //        //TODO
            //        //if (System.Windows.MessageBox.Show("Warning - ChoicePoint reports have already been ordered for this claim.  Do you wish to proceed?",
            //        //    "Reports Already Requested", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
            //            return;
            //    }

            //    string url = "frmAppraisalRequest.aspx?mode=c&claim_id=" + _claimId + "&vehicle_id=0";
            //    string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=600, copyhistory=no, left=200, top=200');";
            //    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            //    //frmRequestPoliceReport f = new frmRequestPoliceReport(claim);
            //    //f.Show();
            //}
            //catch (Exception ex)
            //{
            //    ProcessError(ex);
            //}
        }

        //public bool ChangeNetLossReserve(int reserveId, List<Hashtable> CommercialPolicyValues = null)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        if (reserveId == 0)
        //            return false;

        //        if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Loss_Reserve))
        //            return false;

        //        if (new Reserve(reserveId).Claim.PolicyNo == "")  //check for policy number on claim
        //        {
        //            System.Windows.MessageBox.Show("You can't set loss reserves on a claim without a policy number assigned.  " +
        //                "It causes serious problems with Personal Lines accounting.  Please wait until a policy " +
        //                "has been assigned to this claim.", "Can't Set Reserves Without Policy", System.Windows.MessageBoxButton.OK,
        //                System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
        //            return false;
        //        }

        //        string s = "";
        //        bool isNumber = false;
        //        while (!isNumber)
        //        {
        //            s = Microsoft.VisualBasic.Interaction.InputBox("Enter New Net Loss Reserve Amount", "Change Net Loss Reserve");

        //            if (s == "") //cancel pressed or nothing entered
        //                return false;

        //            isNumber = modLogic.isNumeric(s);
        //            if (!isNumber)
        //                System.Windows.MessageBox.Show("Net Loss Reserve Amount Must be Numeric", "Invalid Amount",
        //                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
        //        }

        //        double newReserve = double.Parse(s.Trim().Replace("$", "").Replace(",", ""));
        //        if (newReserve < 0)
        //        {
        //            System.Windows.MessageBox.Show("Reserves Cannot be Set to a Negative Amount.", "Invalid Reserve", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
        //            return false;
        //        }
        //        else if (newReserve == 0)
        //        {
        //            if (System.Windows.MessageBox.Show("To Set a Reserve to Zero, the Reserve Must Be Closed.  Do You Want to Close This Reserve?", "Zero Reserve", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
        //                return false;
        //            else
        //            {
        //                CloseReserve(reserveId);
        //                return true;
        //            }
        //        }
        //        else
        //            return ChangeNetLossReserve(reserveId, newReserve, CommercialPolicyValues);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //        return false;
        //    }
        //}

        //public bool ChangeNetExpenseReserve(int reserveId, List<Hashtable> CommercialPolicyValues = null)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        if (reserveId == 0)
        //            return false;

        //        if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Change_Expense_Reserve))
        //            return false;

        //        string s = "";
        //        Reserve res = new Reserve(reserveId);
        //        bool isDone = false;

        //        while (!isDone)
        //        {
        //            s = Microsoft.VisualBasic.Interaction.InputBox("Enter New Net Expense Reserve Amount.", "Change Net Expense Reserve");
        //            if (s == "")  //cancel pressed or nothing entered
        //                return false;

        //            isDone = modLogic.isNumeric(s);
        //            if (!modLogic.isNumeric(s))
        //                System.Windows.MessageBox.Show("Net Expense Reserve Amount Must be Numeric", "Invalid Amount",
        //                    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
        //        }

        //        return ChangeNetExpenseReserve(reserveId, double.Parse(s), CommercialPolicyValues);
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //        return false;
        //    }
        //}

        public void SendAlert(int fileNoteId, int fromUserId, int toUserId, bool isDeferred = false, DateTime deferredDate = default(DateTime), bool isUrgent = false)
        {
            User toUser = new User(toUserId);  //create a user object for the To User

            if (toUser.AlertPreferenceId != (int)modGeneratedEnums.AlertPreference.None)  //user should have alert preference
            {
                //create the alert
                Alert a = new Alert();
                a.FileNoteId = fileNoteId;
                a.SentFromUserId = fromUserId;
                a.SentToUserId = toUserId;
                a.DateSent = ml.DBNow();
                a.ShowAlert = true;
                a.isDeferred = isDeferred;

                User fromUser = new User(fromUserId); //create a user object for the From User

                if (fromUser.isAboveUser(toUser))  //can only be urgent if the From user is above the To user OR the From user is management and the To user isn't
                    a.isUrgent = isUrgent;

                if (isDeferred)
                    a.DeferredDate = deferredDate;  //alert is deferred

                a.Update();  //insert the alert in the database

                if (toUser.AlertPreferenceId == (int)modGeneratedEnums.AlertPreference.Email)
                    a.SendAlertByEmail();  //send alert by email
            }

            //update the file note text to say that an alert was sent to this user
            FileNote f = new FileNote(fileNoteId);
            f.FileNoteText += Environment.NewLine + "Alert Sent To: " + toUser.Name;
            f.Update();
        }

        public bool ChangeNetLossReserve(int reserveId, double newAmount, ref string msg, List<Hashtable> CommercialPolicyValues = null)
        {
            Reserve res = new Reserve(reserveId);
            double oldAmount,  //old net reserve amount
                perPerson = 0,  //Per-Person coverage
                perAccident = 0,  //Per-Accident coverage
                previousPayments = 0,  //Previous payments on reserve line
                userAuthority = 0,  //User's financial authority for reserve line
                amountChange = 0;  //the change from the old to new amounts
            Hashtable eventTypeIds = new Hashtable(),  //Event type IDs for firing events.  Keyed by Event_Type_Id, value is parms dictionary
                parms = new Hashtable();  //parms dictionary passed to event handling scripts
            int periodTypeId = 0,  //Per-Person period type
                periods = 0,  //Per-Person number of periods
                reserveTypeId = 0,  //reserve type ID
                claimId = res.ClaimId;  //Claim ID for reserve
            bool deductible = false; //Reserve has deductible amount (comp or collision)
            MedCsxLogic.Claim resClaim = new MedCsxLogic.Claim(claimId);  //Claim associated with reserve line

            //get per-Person and per-Accident coverages
            PolicyCoverage pc = res.PolicyCoverage();
            if (pc != null)
            {
                perPerson = pc.PerPersonCoverage;
                periodTypeId = pc.PerPersonPeriodTypeId;
                periods = pc.PerPersonNoPeriods;
                perAccident = pc.PerAccidentCoverage;
                deductible = pc.hasDeductible;
            }

            oldAmount = res.NetLossReserve;
            amountChange = newAmount - oldAmount;

            if (newAmount < oldAmount)
            {
                if (eventTypeIds.Count == 0)
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                return true;
            }

            if ((resClaim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Commercial) && (resClaim.PolicyNo != ""))
            {
                return false;  //commercial not supported
            }

            reserveTypeId = res.ReserveTypeId;
            previousPayments = res.TotalLossPayments;
            userAuthority = res.UserAuthority();

            if (newAmount + previousPayments > userAuthority)
            {
                reasonPart = "The Amount Entered of " + newAmount.ToString("c") + " Plus Previous Payments of "
                    + previousPayments.ToString("c") + " Exceeds Your Financial Authority of " + userAuthority.ToString("c");
                msg = reasonPart + noLockPart;

                if (res.isLocked)
                    msg = reasonPart + lockedPart;

               
                //if (System.Windows.MessageBox.Show(msg, "Loss Reserve Exceeds Authority",
                //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                    //msg = "Loss Reserve Exceeds Authority. ";
               
                //return false;

                parms.Add("Previous_Payments", previousPayments);
                parms.Add("User_Authority", userAuthority);
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed_Above_Adjuster_Authority, parms);
            }

            //if comp/collision, there is no coverage limit so we don't need to check for coverage
            if (deductible)
            {
                if (eventTypeIds.Count == 0)
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                return true;
            }

            //if manually unlocked for policy not in force for date of loss, don't check coverage limits - adjuster authority limits are good enough
            if (resClaim.PolicyManuallyInForce)
            {
                if (eventTypeIds.Count == 0)
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                return true;
            }

            if (resClaim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Auto)
            {
                if (perPerson == 0 & perAccident == 0) //check for no coverage
                {
                    reasonPart = "There is No Coverage for this Reserve";
                    msg = reasonPart + noLockPart;
                    if (res.isLocked)
                    { 
                        msg = reasonPart + lockedPart;
                        
                    }

                    //return false;

                    PolicyCoverageType pct = res.PolicyCoverageType();
                    if (pct.hasPerPersonCoverage)
                    {
                        parms.Add("Previous_Payments", previousPayments);
                        parms.Add("Per_Person_Coverage", perPerson);
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                    }
                    else if (pct.hasPerAccidentCoverage)
                    {
                        parms.Add("Previous_Payments", previousPayments);
                        parms.Add("Per_Accident_Coverage", perAccident);
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, parms);
                    }
                    if (eventTypeIds.Count == 0)
                        eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
                    res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
                    return true;
                }

                //check for amount exceeding per-Person coverage (Flat)
                if ((perPerson > 0) && (periodTypeId == (int)modGeneratedEnums.PeriodType.Flat) && (newAmount + previousPayments > perPerson))
                {
                    reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Person Coverage of " + perPerson.ToString("c");
                    msg = reasonPart + noLockPart;
                    if (res.isLocked)
                    { 
                        msg = reasonPart + lockedPart;
                      
                    }
                    //return false;

                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("Per_Person_Coverage", perPerson);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                }

                //check for amount exceeding per-Person coverage (Periodic)
                if ((perPerson > 0) && (periodTypeId != (int)modGeneratedEnums.PeriodType.Flat) && (newAmount + previousPayments > perPerson))
                {
                    reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Person Coverage of " + res.CoverageDescription;
                    msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;

                    //if (System.Windows.MessageBox.Show(msg, "Amount Exceeding Per-Person Coverage (Periodic)",
                    //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                    //return false;

                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("Per_Person_Coverage", perPerson * periods);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, parms);
                }

                //check for amount exceeding per-Accident coverage
                if ((perPerson > 0) &&
                    (newAmount - oldAmount + res.Claim.LossForCoverageType(res.ReserveTypeId) +
                    res.Claim.NetLossReserveForCoverageType(res.ReserveTypeId) > perAccident))
                {
                    reasonPart = "Amount Entered Will Cause the Reserve to Exceed Per-Accident Coverage of " + perAccident.ToString("c");
                    msg = reasonPart + noLockPart;
                    if (res.isLocked)
                        msg = reasonPart + lockedPart;


                    //if (System.Windows.MessageBox.Show(msg, "Amount Exceeding Per-Accident Coverage",
                    //    System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                     
                    //return false;

                    parms.Add("Reserve_Amount", previousPayments);
                    parms.Add("Per_Accident_Coverage", perAccident);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, parms);
                }
            }
            if (eventTypeIds.Count == 0)
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Loss_Reserve_Changed, parms);
            res.ChangeNetLossReserve(ref newAmount, eventTypeIds);
            return true;
        }

        public bool ChangeNetExpenseReserve(int reserveId, double newAmount,  ref string msg, List<Hashtable> CommercialPolicyValues = null)
        {
            Reserve res = new Reserve(reserveId);
            double oldAmount,  //old net reserve amount
                    previousPayments = 0,  //Previous payments on reserve line
                    userAuthority = 0,  //User's financial authority for reserve line
                    amountChange = 0;  //the change from the old to new amounts
            Hashtable eventTypeIds = new Hashtable(),  //Event type IDs for firing events.  Keyed by Event_Type_Id, value is parms dictionary
                parms = new Hashtable();  //parms dictionary passed to event handling scripts
            MedCsxLogic.Claim resClaim = new MedCsxLogic.Claim(res.ClaimId);  //Claim associated with reserve line

            oldAmount = res.NetExpenseReserve;
            amountChange = newAmount - oldAmount;

            if (newAmount >= oldAmount)
            {
                if ((resClaim.ClaimTypeId == (int)modGeneratedEnums.ClaimType.Commercial) && (resClaim.PolicyNo != ""))
                    return false;  //commercial not supported

                previousPayments = res.TotalLossPayments;
                userAuthority = res.UserAuthority();

                if (newAmount + previousPayments > userAuthority)
                {
                    reasonPart = "The Amount Entered of " + newAmount.ToString("c") + " Plus Previous Payments of "
                        + previousPayments.ToString("c") + " Exceeds Your Financial Authority of " + userAuthority.ToString("c");
                    msg = reasonPart + noLockPart;

                    if (res.isLocked)
                        msg = reasonPart + lockedPart;


                    //if (System.Windows.MessageBox.Show(msg, "Expense Reserve Exceeds Authority",
                    //     System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, System.Windows.MessageBoxResult.No) == System.Windows.MessageBoxResult.No)
                    //return false;

                    parms.Add("Previous_Payments", previousPayments);
                    parms.Add("User_Authority", userAuthority);
                    eventTypeIds.Add((int)modGeneratedEnums.EventType.Expense_Reserve_Changed_Above_Adjuster_Authority, parms);
                }
            }

            if (eventTypeIds.Count == 0)
                eventTypeIds.Add((int)modGeneratedEnums.EventType.Expense_Reserve_Changed, parms);
            res.ChangeNetExpenseReserve(newAmount, eventTypeIds);
            return true;
        }

        //[DataSysDescription("Allows user to download the PDF version of the report and returns the first page in bytes.")]
        public static Byte[] DownloadPDFFirstPage(string reportURL, Hashtable reportParameters)
        {
            try
            {
                PrintReportingServices printMyReport = new PrintReportingServices();
                return printMyReport.ReturnFirstPage_PDF(reportURL, reportParameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private class PrintReportingServices
        {   //class for printing report service reports
            private com.medjames.reports.ReportingService rs;
            private Byte[][] m_renderedReport;
            private MemoryStream m_currentPageStream;
            private System.Drawing.Image m_docImage = null;
            private int m_numberOfPages;
            private int m_currentPrintingPage,
                m_lastPrintingPage;

            public PrintReportingServices()
            {   //create proxy object and authenicate
                rs = new com.medjames.reports.ReportingService();
                rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }

            public Byte[] ReturnFirstPage_PDF(string reportPath, Hashtable reportParameters)
            {
                string deviceInfo = null,
                    format = "PDF",
                    encoding = "",
                    mimeType = "";
                Byte[] firstPage = null;
                com.medjames.reports.Warning[] warnings = null;
                com.medjames.reports.ParameterValue[] reportHistoryParameters = null;
                com.medjames.reports.ParameterValue[] Params = null;
                string[] streamIds = null;

                foreach (string key in reportParameters.Keys)
                {   //add report parameters to parameters object

                    //create parameter from hashtable item
                    com.medjames.reports.ParameterValue param = new com.medjames.reports.ParameterValue();
                    param.Name = key;
                    if (reportParameters[key].GetType() == typeof(string))
                        param.Value = (string)reportParameters[key];
                    else if (reportParameters[key].GetType() == typeof(int))
                        param.Value = ((int)reportParameters[key]).ToString();

                    //resize parameter array
                    if (Params == null)
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, 1);
                    else
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, Params.Length + 1);

                    Params[Params.Length - 1] = param;  //add parameter to parameter array
                }

                //saving report to PDF file
                deviceInfo = "<DeviceInfo></DeviceInfo>";

                //execute report and get page count
                try
                {
                    //renders the first page of the report and returns stream IDs for subsequent pages
                    firstPage = rs.Render(reportPath, format, null, deviceInfo, Params, null, null,
                        out encoding, out mimeType, out reportHistoryParameters, out warnings, out streamIds);
                    return firstPage;
                }
                catch (SoapException ex)
                {
                    ModForms mf = new ModForms();
                    mf.ProcessError(ex);
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Byte[][] RenderReport(string reportPath, Hashtable reportParameters, string reportName = "")
            {
                string deviceInfo = null,
                    format = "PDF",
                    encoding = "",
                    mimeType = "";
                Byte[] firstPage = null;
                Byte[][] pages = null;
                com.medjames.reports.Warning[] warnings = null;
                com.medjames.reports.ParameterValue[] reportHistoryParameters = null;
                com.medjames.reports.ParameterValue[] Params = null;
                string[] streamIds = null;

                foreach (string key in reportParameters.Keys)
                {   //add report parameters to parameters object

                    //create parameter from hashtable item
                    com.medjames.reports.ParameterValue param = new com.medjames.reports.ParameterValue();
                    param.Name = key;
                    if (reportParameters[key].GetType() == typeof(string))
                        param.Value = (string)reportParameters[key];
                    else if (reportParameters[key].GetType() == typeof(int))
                        param.Value = ((int)reportParameters[key]).ToString();

                    //resize parameter array
                    if (Params == null)
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, 0);
                    else
                        Array.Resize<com.medjames.reports.ParameterValue>(ref Params, Params.Length);

                    Params[Params.Length] = param;  //add parameter to parameter array
                }

                if (reportName == "")
                {   //print report as image
                    format = "IMAGE";
                    deviceInfo = "<DeviceInfo><OutputFormat>TIFF</OutputFormat><ColorDepth>32</ColorDepth><PageHeight>11in</PageHeight><PageWidth>8.5in</PageWidth><MarginBottom>0.44in</MarginBottom></DeviceInfo>";
                }
                else
                {   //saving report to PDF file
                    deviceInfo = "<DeviceInfo></DeviceInfo>";
                }

                //execute report and get page count
                try
                {
                    //renders the first page of the report and returns stream IDs for subsequent pages
                    firstPage = rs.Render(reportPath, format, null, deviceInfo, Params, null, null,
                        out encoding, out mimeType, out reportHistoryParameters, out warnings, out streamIds);

                    if (reportName != "")
                    {   //write the document to PDF file
                        FileStream fs = new FileStream(reportName + "-1.pdf", FileMode.Create);
                        fs.Write(firstPage, 0, firstPage.Length);
                        fs.Close();
                    }

                    //total number of pages is 1 + number of stream IDs
                    m_numberOfPages = streamIds.Length + 1;
                    //pages = new Byte[m_numberOfPages-1][] { };
                    pages = new Byte[][] { };

                    //first page has been rendered
                    pages[0] = firstPage;
                    for (int pageIndex = 1; pageIndex < m_numberOfPages; pageIndex++)
                    {
                        if (reportName == "")
                        {   //print report as image
                            deviceInfo = "<DeviceInfo><OutputFormat>TIFF</OutputFormat><ColorDepth>32</ColorDepth><PageHeight>11in</PageHeight><PageWidth>8.5in</PageWidth><MarginBottom>0.44in</MarginBottom><StartPage>" + pageIndex + 1 + "</StartPage></DeviceInfo>";
                        }

                        pages[pageIndex] = rs.Render(reportPath, format, null, deviceInfo, Params, null, null,
                        out encoding, out mimeType, out reportHistoryParameters, out warnings, out streamIds);

                        if (reportName != "")
                        {   //write the document to PDF file
                            FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("DraftPrints\\") + reportName + "-" + pageIndex + 1 + ".pdf", FileMode.Create);
                            fs.Write(pages[pageIndex], 0, pages[pageIndex].Length);
                            fs.Close();
                        }
                    }
                }
                catch (SoapException ex)
                {
                    ModForms mf = new ModForms();
                    mf.ProcessError(ex);
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return pages;
            }

            public bool PrintReport(string reportURL, string printerName, Hashtable reportParameters)
            {
                this.RenderedReport = this.RenderReport(reportURL, reportParameters);

                try
                {   //wait for the report to completely render
                    if (m_numberOfPages < 1)
                        return false;

                    PrinterSettings printerSettings = new PrinterSettings();
                    printerSettings.MaximumPage = m_numberOfPages;
                    printerSettings.MinimumPage = 1;
                    printerSettings.PrintRange = PrintRange.SomePages;
                    printerSettings.FromPage = 1;
                    printerSettings.ToPage = m_numberOfPages;
                    printerSettings.PrinterName = printerName;
                    printerSettings.PrintToFile = true;

                    PrintDocument pd = new PrintDocument();
                    m_currentPrintingPage = 1;
                    m_lastPrintingPage = m_numberOfPages;
                    pd.PrinterSettings = printerSettings;
                    pd.DefaultPageSettings.Landscape = false;   //always print in portrait

                    pd.PrintPage += pd_PrintPage;
                    pd.Print();
                }
                catch (Exception ex)
                {
                    throw ex;
                   
                    //MessageBox.Show(ex.ToString());
                  
                }
                return true;
            }

            void pd_PrintPage(object sender, PrintPageEventArgs e)
            {
                e.HasMorePages = false;
                if ((m_currentPrintingPage <= m_lastPrintingPage) && MoveToPage(m_currentPrintingPage))
                {
                    ReportDrawPage(e.Graphics);     //draw page
                    if (System.Threading.Interlocked.Increment(ref m_currentPrintingPage) <= m_lastPrintingPage)
                        e.HasMorePages = true;      //print another page if there are more pages
                }
            }

            private bool MoveToPage(int m_currentPrintingPage)
            {
                if (this.RenderedReport[m_currentPrintingPage - 1] == null)
                    return false;       //current page doesn't exist in array list

                m_currentPageStream = new MemoryStream(this.RenderedReport[m_currentPrintingPage - 1]);     //set current page stream to the rendered page
                m_currentPageStream.Position = 0;   //set position to start

                if (m_docImage != null)
                {
                    m_docImage.Dispose();
                    m_docImage = null;
                }
                m_docImage = System.Drawing.Image.FromStream(m_currentPageStream);
                return true;
            }

            private void ReportDrawPage(Graphics graphics)
            {
                if ((m_currentPageStream == null) || (m_currentPageStream.Length == 0) || (m_docImage == null))
                    return;
                lock (this)
                {
                    System.Drawing.Point destPoint = new System.Drawing.Point(0, 0);
                    graphics.DrawImage(m_docImage, destPoint);
                }
            }

            public Byte[][] RenderedReport
            {
                get { return m_renderedReport; }
                set { m_renderedReport = value; }
            }
        }

      

        public bool PrintDraftForDraftPrintId(int draftPrintId,  ref string msg)
        {
            bool ret = true;
            try
            {
                //set mouse to be busy
               

                //create the new draft print object
                DraftPrint dp = new DraftPrint(draftPrintId);

                //create the PDF document
                /// need ABCpdf
                Byte[] PDFDocument;

                //create the parameters hashtable
                Hashtable Params = new Hashtable();
                Params.Add("Draft_Id", dp.DraftId);

                //PDFDocument.Read(Functions.DownloadPDFFirstPage(mm.DRAFT_REPORT, Params));
                PDFDocument = Functions.DownloadPDFFirstPage(mm.DRAFT_REPORT, Params);
                ////print the document
                ////pgrint draft using ABCpdf DLL
                //PrintPDF printDoc = new PrintPDF(PDFDocument, mm.draftPrinter);

                if (mm.PreviewDraftsOnly)
                {
                    //printDoc.PrintPreview(); //display the page
                    System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("DraftPrints\\") + "Draft" + dp.DraftId.ToString() + ".pdf", PDFDocument);
                }
                else
                {
                    //printDoc.Print();
                    ret = Functions.PrintDraft(dp.DraftId.ToString(), PDFDocument);
                }
            }
            catch (Exception ex)
            {
                msg = "Error occurred while printing the draft.  The draft (draftPrintId = " +
                            draftPrintId.ToString() + ") has been placed in the draft queue." + Environment.NewLine +
                            Environment.NewLine + "Error Information:" + Environment.NewLine + ex.ToString() +
                            Environment.NewLine + Environment.NewLine + ex.Message.ToString();
                
                ret = false;
                ProcessError(ex); //display error with line number
                throw ex;
            }
            finally
            {
                //set mouse to normal
                //MouseNormal();
            }
            return ret;
        }

        public bool PrintDraftForDraftPrintId(int draftPrintId, ref string msg, bool overlimit, string draftprinter = "")
        {
            bool ret = true;
            try
            {
                //set mouse to be busy


                //create the new draft print object
                DraftPrint dp = new DraftPrint(draftPrintId);

                //create the PDF document
                /// need ABCpdf
                Byte[] PDFDocument;

                //create the parameters hashtable
                Hashtable Params = new Hashtable();
                Params.Add("Draft_Id", dp.DraftId);

                //PDFDocument.Read(Functions.DownloadPDFFirstPage(mm.DRAFT_REPORT, Params));
                PDFDocument = Functions.DownloadPDFFirstPage(mm.DRAFT_REPORT, Params);
                ////print the document
                ////pgrint draft using ABCpdf DLL
                //PrintPDF printDoc = new PrintPDF(PDFDocument, mm.draftPrinter);

                if (mm.PreviewDraftsOnly)
                {
                    //printDoc.PrintPreview(); //display the page
                    System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("DraftPrints\\") + "Draft" + dp.DraftId.ToString() + ".pdf", PDFDocument);
                }
                else
                {
                    //printDoc.Print();
                    ret = Functions.PrintDraftQueue(dp.DraftId.ToString(), PDFDocument, overlimit, draftprinter);
                }
            }
            catch (Exception ex)
            {
                msg = "Error occurred while printing the draft.  The draft (draftPrintId = " +
                            draftPrintId.ToString() + ") has been placed in the draft queue." + Environment.NewLine +
                            Environment.NewLine + "Error Information:" + Environment.NewLine + ex.ToString() +
                            Environment.NewLine + Environment.NewLine + ex.Message.ToString();

                ret = false;
                ProcessError(ex); //display error with line number
                throw ex;
            }
            finally
            {
                //set mouse to normal
                //MouseNormal();
            }
            return ret;
        }



        public void LoadTypeIntoComboBox(string table, DropDownList cb, string field = "Description", string whereClause = "1=1", bool setTextToFirstItem = true)
        {
            try
            {
                cb.Items.Clear();  //clear out the combo box rows
                DataTable _typeRows = new DataTable(); //by sql string
                string sql = "SELECT " + table + "_Id as Id, description FROM " + table +
                    " WHERE " + table + "_Id <> 0 and " + whereClause + " order by " + field;

                _typeRows = db.ExecuteSelectReturnDataTable(sql,1);

                cb.DataSource = _typeRows;
                cb.DataValueField = _typeRows.Columns[0].ColumnName;
                cb.DataTextField = _typeRows.Columns[1].ColumnName;
                cb.DataBind();

                //List<Hashtable> Rows = (List<Hashtable>)_typeRows[sql];
                //foreach (Hashtable row in Rows)
                //    cb.Items.Add(row[field].ToString());

                //if (Rows.Count > 0 & setTextToFirstItem)
                //    cb.Text = (string)((Hashtable)ml.firstItem(Rows))[field];
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }

        public void LoadComboBoxFromStoredProc(string procName, DropDownList cb, string field = "Description", object parm1 = null)
        {
            DataTable c;

            cb.Items.Clear();
            if (parm1 != null)
                c = db.ExecuteStoredProcedure(procName, parm1);
            else
                c = db.ExecuteStoredProcedure(procName);


            cb.DataSource = c;
            cb.DataValueField = c.Columns[0].ColumnName;
            cb.DataTextField = field;
            cb.DataBind();

            //foreach (DataRow v in c.Rows)
            //{
            //    cb.Items.Add(v[field]);
            //}
        }

        //public void IssueDraft(int reserveId, List<Hashtable> CommercialPolicyValues = null, int claimId = 0)
        //{
        //    try
        //    {
        //        mm.UpdateLastActivityTime();
        //        if (reserveId == 0)
        //            return;

        //        //if (!MicrFontsExist())
        //        //    return;

        //        if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Issue_Draft))
        //            return;

        //        Reserve res = new Reserve(reserveId);

        //        //TODO
        //        //if (res.Claim.maxSupplementalPaymentReached)
        //        //{
                    
        //        //   System.Windows.MessageBox.Show("The Maximum Number of Supplemental Payments have been Made Against This Claim.  " +
        //        //        "It Must be Reopened to allow Payments.", "Max Supplemental Payments for Claim",
        //        //        System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
        //        //    return;
        //        //}

        //        //if (res.maxSupplementalPaymentsReached)
        //        //{
        //        //    System.Windows.MessageBox.Show("The Maximum Number of Supplemental Payments have been Made Against This Reserve.  " +
        //        //        "It Must be Reopened to allow Payments.", "Max Supplemental Payments for Reserve",
        //        //        System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
        //        //    return;
        //        //}

        //        frmDraft f = new frmDraft(reserveId);//, CommercialPolicyValues, claimId);
        //        f.Show();
        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessError(ex);
        //    }
        //}


        public void ReopenClaim(int claimId)
        {
            try
            {
                mm.UpdateLastActivityTime();
                if (claimId == 0)
                    return;

                if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Reopen_Claim))
                    return;

                MedCsxLogic.Claim claim = new MedCsxLogic.Claim(claimId);
                if ((claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Open)
                    || (claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Reopened))
                {
                   
                    //System.Windows.MessageBox.Show("This Claim is Already Open", "Claim Already Open",
                    //    System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation, System.Windows.MessageBoxResult.OK);
                    return;
                }

                claim.Reopen();
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
        }



        public void LoadTypeTableComboBox(DropDownList cb, string Tag, bool includeZeroValue = false, string whereClause = "1=1")
        {
            LoadTypeTable(cb, TypeTable.getTypeTableRows(Tag, whereClause), includeZeroValue);
        }




        public void LoadTypeTable(DropDownList cb, List<Hashtable> rows, bool includeZeroValue = false)
        {
            for (int i = 0; i < rows.Count; i++)
            {
                Hashtable thisRow = rows[i];
                this.ConvertHashtableKeysToLowercase(thisRow);

                if (includeZeroValue || ((int)thisRow["id"] != 0))
                {
                    cb.Items.Add((string)thisRow["description"]);
                    AllItemsWithIds.Add(thisRow);
                }
            }
        }

        public void LoadTypeTableDDL(DropDownList cb, string Tag, bool includeZeroValue = true, string whereClause = "1=1")
        {
            LoadTypeTableDDL(cb, TypeTable.getTypeTableRowsDDL(Tag, whereClause), includeZeroValue);
        }


        public void LoadTypeTableDDL(DropDownList cb, DataTable rows, bool includeZeroValue = false)
        {
            cb.DataSource = rows;
            cb.DataTextField = "description";
            cb.DataValueField = "id";
            cb.DataBind();
        }

        public void LoadStateComboBox(DropDownList cb, bool includeZeroValue = true, string whereClause = "1=1")
        {
            if (cb == null)
                return;
            string sql = "SELECT State_Id, Abbreviation FROM State";
            if (!includeZeroValue)
                sql += " WHERE State_Id <> 0";
            List<Hashtable> hStates = db.ExecuteSelectReturnHashList(sql);
            //ExecuteStoredProcedureReturnHashList("Get_States2");
            for (int i = 0; i < hStates.Count; i++)
            {
                Hashtable thisRow = hStates[i];
                this.ConvertHashtableKeysToLowercase(thisRow);
                cb.Items.Add((string)thisRow["abbreviation"]);
            }
        }

        public void LoadStateDDL(DropDownList cb, bool includeZeroValue = true, string whereClause = "1=1")
        {
           
            if (cb == null)
                return;
            string sql = "SELECT State_Id, Abbreviation FROM State";
            if (!includeZeroValue)
                sql += " WHERE State_Id <> 0 order by Abbreviation";
            DataTable hStates = db.ExecuteSelectReturnDataTable(sql, 1);
            //ExecuteStoredProcedureReturnHashList("Get_States2");

            cb.DataSource = hStates;
            cb.DataValueField = "state_id";
            cb.DataTextField = "abbreviation";
            cb.DataBind();
        }

        public void ConvertHashtableKeysToLowercase(Hashtable data)
        {
            bool allKeysConverted = false;
            while (!allKeysConverted)
            {
                string lowerCaseKey = "",
                    missingKey = "";
                allKeysConverted = true;

                foreach (string key in data.Keys)
                {
                    lowerCaseKey = key.ToLower().Replace(" ", "_");
                    if (lowerCaseKey != key)
                    {
                        missingKey = key;
                        allKeysConverted = false;
                        break;
                    }
                }

                if (!allKeysConverted)
                {
                    if (!data.ContainsKey(lowerCaseKey))
                        data.Add(lowerCaseKey, data[missingKey]);
                    else
                        data[lowerCaseKey] = data[missingKey];
                    data.Remove(missingKey);
                }
            }
        }
    }
}
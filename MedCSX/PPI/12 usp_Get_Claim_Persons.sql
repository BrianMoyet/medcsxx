USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Claim_Persons]    Script Date: 3/17/2021 3:43:55 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/* Get all persons related to a claim (insured, owner, driver, prop damage owner & driver, etc. */

ALTER PROCEDURE [dbo].[usp_Get_Claim_Persons] 
@claim_id foreign_key
AS

OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;



/* return person rows */
select distinct 
 Person_Id, Claim_Id, p.Person_Type_Id, Policy_Individual_Id, 
dbo.fn_Decrypt(Salutation) as Salutation, 
Last_Name, 
Middle_Initial, 
First_Name, 
Suffix, 
dbo.fn_Decrypt(Address1) as Address1, 
dbo.fn_Decrypt(Address2) as Address2, 
dbo.fn_Decrypt(City) as City, 
Convert(int, CONVERT(varchar, dbo.fn_Decrypt(p.State_Id))) as State_Id, 
dbo.fn_Decrypt(Zipcode) as Zipcode, 
dbo.fn_Decrypt(Home_Phone) as Home_Phone, 
dbo.fn_Decrypt(Work_Phone) as Work_Phone, 
dbo.fn_Decrypt(Cell_Phone) as Cell_Phone, 
dbo.fn_Decrypt(Other_Contact_Phone) as Other_Contact_Phone, 
dbo.fn_Decrypt(Fax) as Fax, 
dbo.fn_Decrypt(Email) as Email, 
Convert(datetime ,CONVERT(varchar, DecryptByKey(Date_Of_Birth))) AS 'Date_Of_Birth', 
dbo.fn_Decrypt(SSN) AS 'SSN', 
dbo.fn_Decrypt(Drivers_License_No) as Drivers_License_No, 
dbo.fn_Decrypt(Sex) as Sex, 
dbo.fn_Decrypt(Employer) as Employer, 
Convert(int, CONVERT(varchar, dbo.fn_Decrypt(Language_Id)))  as Language_Id, 
p.Created_By, p.Modified_By, p.Created_Date, p.Modified_Date, Is_Company,
dbo.fn_Decrypt(Comments) as Comments, 
Flag_Fraud, 
dbo.fn_Decrypt(Other_Insurance_Policy_No) as Other_Insurance_Policy_No, 
dbo.fn_Decrypt(Other_Insurance_Company) as Other_Insurance_Company, 
dbo.fn_Decrypt(Other_Insurance_Agent_Name) as Other_Insurance_Agent_Name, 
dbo.fn_Decrypt(Other_Insurance_Agent_Address) as Other_Insurance_Agent_Address, 
dbo.fn_Decrypt(Other_Insurance_Agent_City) as Other_Insurance_Agent_City, 
dbo.fn_Decrypt(Other_Insurance_Agent_State_Id) as Other_Insurance_Agent_State_Id, 
dbo.fn_Decrypt(Other_Insurance_Agent_ZipCode) as Other_Insurance_Agent_ZipCode, 
dbo.fn_Decrypt(Other_Insurance_Agent_Phone) as Other_Insurance_Agent_Phone, 
dbo.fn_Decrypt(Other_Insurance_Agent_Fax) as Other_Insurance_Agent_Fax, 
dbo.fn_Decrypt(Other_Insurance_Agent_Email) as Other_Insurance_Agent_Email, 
dbo.fn_Decrypt(Other_Insurance_Claim_No) as Other_Insurance_Claim_No, 
Has_Other_Insurance
, s.abbreviation as [State], pt.Image_Index, dbo.get_person_type(person_id) as Type, dbo.get_person_name(p.person_id) as Name
from person p, state s, person_type pt
where p.person_id <> 0 and s.state_id = dbo.fn_decrypt(p.state_id) and
p.person_type_id = pt.person_type_id and 
--dbo.get_person_name(p.person_id) <> 'Parked and Unoccupied' and
claim_id = @claim_id

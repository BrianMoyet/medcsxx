USE [Claims_Encrypt]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_Decrypt]    Script Date: 3/9/2021 3:57:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_Decrypt] (@encryptedValue VARBINARY(256))
RETURNS VARCHAR(128)
AS
BEGIN;
    RETURN CONVERT(VARCHAR(128),DECRYPTBYKEYAUTOCERT(CERT_ID('Certificate_Encrypt'), NULL, @encryptedValue));
END
GO


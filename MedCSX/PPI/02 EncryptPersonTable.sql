
OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;
GO


UPDATE [Person]
        SET  
		 Address1_Enc = dbo.fn_Encrypt(Address1), 
		 Address2_Enc = dbo.fn_Encrypt(Address2), 
         City_Enc = dbo.fn_Encrypt(City), 
		 State_Id_Enc = dbo.fn_Encrypt(State_Id), 
		 Zipcode_Enc = dbo.fn_Encrypt(Zipcode), 
		 Home_Phone_Enc = dbo.fn_Encrypt(Home_Phone), 
		 Work_Phone_Enc =dbo.fn_Encrypt(Work_Phone), 
		 Cell_Phone_Enc =dbo.fn_Encrypt(Cell_Phone), 
		 Other_Contact_Phone_Enc =dbo.fn_Encrypt(Other_Contact_Phone), 
		 Fax_Enc =dbo.fn_Encrypt(Fax), 
		 Email_Enc =dbo.fn_Encrypt(Email), 
		 Date_Of_Birth_Enc =dbo.fn_Encrypt(Date_Of_Birth), 
		 SSN_Enc =dbo.fn_Encrypt(SSN), 
		 Drivers_License_No_Enc =dbo.fn_Encrypt(Drivers_License_No), 
         Sex_Enc =dbo.fn_Encrypt(Sex), 
		 Employer_Enc =dbo.fn_Encrypt(Employer), 
		 Language_Id_Enc =dbo.fn_Encrypt(Language_Id), 
		 Comments_Enc =dbo.fn_Encrypt(Comments), 
		 Other_Insurance_Policy_No_Enc = dbo.fn_Encrypt(Other_Insurance_Policy_No), 
		 Other_Insurance_Company_Enc = dbo.fn_Encrypt(Other_Insurance_Company), 
		 Other_Insurance_Agent_Name_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_Name), 
		 Other_Insurance_Agent_Address_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_Address), 
         Other_Insurance_Agent_City_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_City), 
		 Other_Insurance_Agent_State_Id_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_State_Id), 
		 Other_Insurance_Agent_ZipCode_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_ZipCode), 
		 Other_Insurance_Agent_Phone_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_Phone), 
		 Other_Insurance_Agent_Fax_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_Fax), 
         Other_Insurance_Agent_Email_Enc = dbo.fn_Encrypt(Other_Insurance_Agent_Email), 
		 Other_Insurance_Claim_No_Enc =  dbo.fn_Encrypt(Other_Insurance_Claim_No),
		 Salutation_enc = dbo.fn_Encrypt(Salutation)
        FROM [Person]

        GO
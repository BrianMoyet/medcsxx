/*
   Thursday, March 11, 202111:08:40 AM
   User: 
   Server: qasql4
   Database: Claims_Encrypt
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT FK_Person__States
GO
ALTER TABLE dbo.State SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.State', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.State', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.State', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT FK_Person_Language
GO
ALTER TABLE dbo.Language SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Language', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Language', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Language', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
EXECUTE sp_unbindefault N'dbo.Person.Salutation'
GO
EXECUTE sp_unbindefault N'dbo.Person.Address1'
GO
EXECUTE sp_unbindefault N'dbo.Person.Address2'
GO
EXECUTE sp_unbindefault N'dbo.Person.City'
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT DF_Person_State_Id
GO
EXECUTE sp_unbindefault N'dbo.Person.Zipcode'
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT DF_Person_Home_Phone
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT DF_Person_Work_Phone
GO
EXECUTE sp_unbindefault N'dbo.Person.Cell_Phone'
GO
EXECUTE sp_unbindefault N'dbo.Person.Other_Contact_Phone'
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT DF_Person_Fax_Phone
GO
EXECUTE sp_unbindefault N'dbo.Person.Email'
GO
EXECUTE sp_unbindefault N'dbo.Person.Date_Of_Birth'
GO
EXECUTE sp_unbindefault N'dbo.Person.SSN'
GO
EXECUTE sp_unbindefault N'dbo.Person.Drivers_License_No'
GO
EXECUTE sp_unbindefault N'dbo.Person.Sex'
GO
EXECUTE sp_unbindefault N'dbo.Person.Employer'
GO
EXECUTE sp_unbindefault N'dbo.Person.Language_Id'
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT DF__Person__Comments__4E2426E1
GO
ALTER TABLE dbo.Person
	DROP CONSTRAINT DF__Person__Other_In__0297EF20
GO
DROP INDEX IX_Person_2 ON dbo.Person
GO
ALTER TABLE dbo.Person
	DROP COLUMN Salutation, Address1, Address2, City, State_Id, Zipcode, Home_Phone, Work_Phone, Cell_Phone, Other_Contact_Phone, Fax, Email, Date_Of_Birth, SSN, Drivers_License_No, Sex, Employer, Language_Id, Comments, Other_Insurance_Policy_No, Other_Insurance_Company, Other_Insurance_Agent_Name, Other_Insurance_Agent_Address, Other_Insurance_Agent_City, Other_Insurance_Agent_State_Id, Other_Insurance_Agent_ZipCode, Other_Insurance_Agent_Phone, Other_Insurance_Agent_Fax, Other_Insurance_Agent_Email, Other_Insurance_Claim_No
GO
EXECUTE sp_rename N'dbo.Person.Address1_Enc', N'Tmp_Address1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Address2_Enc', N'Tmp_Address2_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.City_Enc', N'Tmp_City_2', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.State_Id_Enc', N'Tmp_State_Id_3', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Zipcode_Enc', N'Tmp_Zipcode_4', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Home_Phone_Enc', N'Tmp_Home_Phone_5', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Work_Phone_Enc', N'Tmp_Work_Phone_6', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Cell_Phone_Enc', N'Tmp_Cell_Phone_7', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Contact_Phone_Enc', N'Tmp_Other_Contact_Phone_8', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Fax_Enc', N'Tmp_Fax_9', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Email_Enc', N'Tmp_Email_10', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Date_Of_Birth_Enc', N'Tmp_Date_Of_Birth_11', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.SSN_Enc', N'Tmp_SSN_12', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Drivers_License_No_Enc', N'Tmp_Drivers_License_No_13', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Sex_Enc', N'Tmp_Sex_14', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Employer_Enc', N'Tmp_Employer_15', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Language_Id_Enc', N'Tmp_Language_Id_16', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Comments_Enc', N'Tmp_Comments_17', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Policy_No_Enc', N'Tmp_Other_Insurance_Policy_No_18', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Company_Enc', N'Tmp_Other_Insurance_Company_19', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_Name_Enc', N'Tmp_Other_Insurance_Agent_Name_20', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_Address_Enc', N'Tmp_Other_Insurance_Agent_Address_21', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_City_Enc', N'Tmp_Other_Insurance_Agent_City_22', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_State_Id_Enc', N'Tmp_Other_Insurance_Agent_State_Id_23', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_ZipCode_Enc', N'Tmp_Other_Insurance_Agent_ZipCode_24', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_Phone_Enc', N'Tmp_Other_Insurance_Agent_Phone_25', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_Fax_Enc', N'Tmp_Other_Insurance_Agent_Fax_26', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Agent_Email_Enc', N'Tmp_Other_Insurance_Agent_Email_27', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Other_Insurance_Claim_No_Enc', N'Tmp_Other_Insurance_Claim_No_28', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Salutation_Enc', N'Tmp_Salutation_29', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Address1', N'Address1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Address2_1', N'Address2', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_City_2', N'City', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_State_Id_3', N'State_Id', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Zipcode_4', N'Zipcode', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Home_Phone_5', N'Home_Phone', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Work_Phone_6', N'Work_Phone', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Cell_Phone_7', N'Cell_Phone', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Contact_Phone_8', N'Other_Contact_Phone', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Fax_9', N'Fax', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Email_10', N'Email', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Date_Of_Birth_11', N'Date_Of_Birth', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_SSN_12', N'SSN', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Drivers_License_No_13', N'Drivers_License_No', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Sex_14', N'Sex', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Employer_15', N'Employer', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Language_Id_16', N'Language_Id', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Comments_17', N'Comments', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Policy_No_18', N'Other_Insurance_Policy_No', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Company_19', N'Other_Insurance_Company', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_Name_20', N'Other_Insurance_Agent_Name', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_Address_21', N'Other_Insurance_Agent_Address', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_City_22', N'Other_Insurance_Agent_City', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_State_Id_23', N'Other_Insurance_Agent_State_Id', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_ZipCode_24', N'Other_Insurance_Agent_ZipCode', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_Phone_25', N'Other_Insurance_Agent_Phone', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_Fax_26', N'Other_Insurance_Agent_Fax', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Agent_Email_27', N'Other_Insurance_Agent_Email', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Other_Insurance_Claim_No_28', N'Other_Insurance_Claim_No', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Person.Tmp_Salutation_29', N'Salutation', 'COLUMN' 
GO
ALTER TABLE dbo.Person SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Person', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Person', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Person', 'Object', 'CONTROL') as Contr_Per 
USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Fraudulent]    Script Date: 3/11/2021 4:19:15 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


ALTER  PROCEDURE [dbo].[usp_Get_Fraudulent]
@claimid foreign_key
AS
Begin

OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;

if OBJECT_ID('tempdb.dbo.#claimParms', 'U') is not null
	drop table #claimParms 

if OBJECT_ID('tempdb.dbo.#FoundFraudulent', 'U') is not null
	drop table #FoundFraudulent 

if OBJECT_ID('tempdb.dbo.#vehicleParms', 'U') is not null
	drop table #vehicleParms 

--Returned table for persons, vendors, and vehicles found fraudulent
create table #FoundFraudulent(Person_id int, Vendor_id int, Vehicle_Id int, Claim_id int )
--table to hold persons and vendors from claim
create table #claimParms(firstName nvarchar(64), lastName nvarchar(32), address1 nvarchar(64),
					 	address2 nvarchar(64), zipCode nvarchar(16), phoneNum1 nvarchar(16), 
						phoneNum2 nvarchar(16), email nvarchar(64), dateOfBirth datetime, 
						TaxId nvarchar(16), driversLic nvarchar(32))
--table to hold vehicles from claim
create table #vehicleParms(VIN nvarchar(32), vehYear nvarchar(8), vehMake nvarchar(32),
						vehModel nvarchar(32), vehLicense varchar(16))

--insert persons into #claimParms from claim so that blanks are not present
insert #claimParms 
select p.First_Name, p.Last_Name, 

case when dbo.fn_Decrypt(p.Address1) = '' then '-'
	when Substring(Upper(dbo.fn_Decrypt(p.Address1)),1,3) = 'UNK' then '-'
	else dbo.fn_Decrypt(p.Address1)
end as Address1,
dbo.fn_Decrypt(p.Address2),
case when dbo.fn_Decrypt(p.Zipcode) = '' then '-'
	when Substring(Upper(dbo.fn_Decrypt(p.Zipcode)),1,3) = 'UNK' then '-'
	else dbo.fn_Decrypt(p.Zipcode)
end as zipCode,
case when dbo.fn_Decrypt(p.Cell_Phone) <> '' and dbo.fn_Decrypt(p.Cell_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Cell_Phone) <> '(   )    -     ' then
	SubString(dbo.fn_Decrypt(p.Cell_Phone), 1, 14)
	when dbo.fn_Decrypt(p.Home_Phone) <> '' and dbo.fn_Decrypt(p.Home_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Home_Phone)  <> '(   )    -     ' then
	SubString(dbo.fn_Decrypt(p.Home_Phone), 1, 14)
	when dbo.fn_Decrypt(p.Other_Contact_Phone) <> '' and dbo.fn_Decrypt(p.Other_Contact_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Other_Contact_Phone)  <> '(   )    -     ' then
	SubString(dbo.fn_Decrypt(p.Other_Contact_Phone), 1, 14)
	else '-' 
end as Phone1, 
case when dbo.fn_Decrypt(p.Cell_Phone) <> '' and dbo.fn_Decrypt(p.Cell_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Cell_Phone) <> '(   )    -     ' then
	case 
		when dbo.fn_Decrypt(p.Home_Phone) <> '' and dbo.fn_Decrypt(p.Home_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Home_Phone)  <> '(   )    -     ' then
		SubString(dbo.fn_Decrypt(p.Home_Phone), 1, 14)
		when dbo.fn_Decrypt(p.Other_Contact_Phone) <> '' and dbo.fn_Decrypt(p.Other_Contact_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Other_Contact_Phone)  <> '(   )    -     ' then
		SubString(dbo.fn_Decrypt(p.Other_Contact_Phone), 1, 14)
		else '-'
	end
	when dbo.fn_Decrypt(p.Home_Phone) <> '' and dbo.fn_Decrypt(p.Home_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Home_Phone)  <> '(   )    -     ' then
	case 
		when dbo.fn_Decrypt(p.Other_Contact_Phone) <> '' and dbo.fn_Decrypt(p.Other_Contact_Phone) <> '(___) ___-____' and dbo.fn_Decrypt(p.Other_Contact_Phone)  <> '(   )    -     ' then
		SubString(dbo.fn_Decrypt(p.Other_Contact_Phone), 1, 14)
		else '-'
	end
end as Phone2,
case when dbo.fn_Decrypt(p.Email) <> '' then dbo.fn_Decrypt(p.Email) 
	else '-'
end as Email, Convert(datetime ,CONVERT(varchar, 
                         dbo.fn_Decrypt(date_of_birth))) AS 'Date_Of_Birth', 
case when dbo.fn_Decrypt(p.SSN) <> '' then dbo.fn_Decrypt(p.SSN) 
	else '-'
end as SSN, 
case when dbo.fn_Decrypt(p.Drivers_License_No) <> '' then dbo.fn_Decrypt(p.Drivers_License_No)
	else '-'
end as Drivers_License_No
from Person p, Claim c
where c.Claim_Id=@claimId 
and p.Person_Id in (c.Owner_Person_Id, c.Driver_Person_Id, c.Insured_Person_Id)

--insert vendors into #claimParms from claim so that blanks are not present
insert #claimParms 
select v.Name, '-',
case when v.Address1 = '' then '-'
	when Substring(Upper(v.Address1),1,3) = 'UNK' then '-'
	else v.Address1
end as Address1, 
v.Address2,
case when v.Zipcode = '' then '-'
	when Substring(Upper(v.Zipcode),1,3) = 'UNK' then '-'
	else v.Zipcode
end as zipCode,
case when v.Phone <> '' and v.Phone <> '(___) ___-____' and v.Phone <> '(   )    -     ' then
	SubString(v.Phone, 1, 14)
	else '-' 
end as Phone1, 
case when v.Fax <> '' and v.Fax <> '(___) ___-____' and v.Fax <> '(   )    -     ' then
	SubString(v.Fax, 1, 14)
	else '-'
end as Phone2,
case when v.Email <> '' then v.Email 
	else '-'
end as Email, '01-02-1900', v.Tax_Id,'-'
from Vendor v, [Transaction] t, Draft d
where t.Claim_Id = @claimId 
and t.Draft_Id = d.Draft_Id 
and d.Vendor_Id = v.Vendor_Id 
and v.Name <> ''

--remove duplicates
;with CTE as (
	select *,
	Row_Number() over (partition by PhoneNum1 order by PhoneNum1) as Rn
	from #claimParms )
Delete from CTE where Rn>1

--insert vehicles into #vehicleParms from claim so that blanks are not present
insert #vehicleParms
select 
case when VIN = '' then '-'
	 when Substring(Upper(VIN),1,3) = 'UNK' then '-'
	else VIN
end as VIN,
case when dbo.No_Digits(Year_Made) = len(Year_Made) then Year_Made 
	else '-'
end as vehicleYear,
case when Manufacturer = '' then '-'
	 when Substring(Upper(Manufacturer),1,3) = 'UNK' then '-'
	else Manufacturer 
end as vehicleMake,
case when Model = '' then '-'
	 when Substring(Upper(Model),1,3) = 'UNK' then '-'
	else Model 
end as vehicleModel,
case when License_Plate_No = '' then '-'
	 when Substring(Upper(License_Plate_No),1,3) = 'UNK' then '-'
	else License_Plate_No
end as vehicleLicense
from Vehicle 
where Claim_Id = @claimId 
and Substring(Upper(Manufacturer),1,3) <> 'UNK'

--search persons table against #claimsParms for fraudulent match
insert #FoundFraudulent (
Person_id, 
Claim_id)
select p.Person_Id, p.Claim_Id
from Person p
where p.Flag_Fraud = 1
and (Exists(select firstName, lastName
			from #claimParms 
			where Upper(p.First_Name) like '%' + Upper(firstName) +'%'
			and Upper(p.Last_Name) like '%' + Upper(lastName))
	and (Exists (select Address1,Address2,Zipcode
				from #claimParms 
				where Upper(dbo.fn_Decrypt(p.Address1)) like '%' + Upper(address1) + '%'
				and Upper(dbo.fn_Decrypt(p.Address2)) like '%' + Upper(address2) + '%'
				and dbo.fn_Decrypt(p.Zipcode) = zipCode))
	or (dbo.fn_Decrypt(p.Cell_Phone) in (select phoneNum1 from #claimParms)
	or dbo.fn_Decrypt(p.Home_Phone) in (select phoneNum1 from #claimParms)
	or dbo.fn_Decrypt(p.Other_Contact_Phone) in (select phoneNum1 from #claimParms)
	or dbo.fn_Decrypt(p.Cell_Phone) in (select phoneNum2 from #claimParms)
	or dbo.fn_Decrypt(p.Home_Phone) in (select phoneNum2 from #claimParms)
	or dbo.fn_Decrypt(p.Other_Contact_Phone) in (select phoneNum2 from #claimParms))
	or dbo.fn_Decrypt(p.Drivers_License_No) in (select driversLic from #claimParms)
	or dbo.fn_Decrypt(p.SSN) in (select SSN from #claimParms)
	or (Convert(Varchar(10), Convert(datetime ,CONVERT(varchar, 
                         dbo.fn_Decrypt(p.date_of_birth))),103) <> '01-01-1900'
		and Convert(Varchar(10), Convert(datetime ,CONVERT(varchar, 
                         dbo.fn_Decrypt(p.date_of_birth))),103) in (select Convert(Varchar(10),dateOfBirth,103) from #claimParms))
	)

--search vehicle table against #vehicleParms for fraudulent match
insert #FoundFraudulent (
Vehicle_Id,
Claim_id)
select v.Vehicle_Id, v.Claim_Id 
from Vehicle v
where v.Flag_Fraud = 1
and (Upper(v.VIN) in (select Upper(VIN) from #vehicleParms)
or Upper(v.License_Plate_No) in (select Upper(vehLicense) from #vehicleParms)
or (Upper(v.Manufacturer) in (select Upper(vehMake) from #vehicleParms)
	and Upper(v.Model) in (select Upper(vehModel) from #vehicleParms)
	and v.Year_Made in (select vehYear from #vehicleParms)))

--search vendor table against #claimsParms for fraudulent match
insert #FoundFraudulent (
Vendor_id)
select v.Vendor_Id
from Vendor v
where v.Flag_Fraud = 1
and (v.Tax_Id <> '' and v.Tax_Id in (select TaxId from #claimParms)
or Exists (select Address1,Address2,Zipcode
	from #claimParms 
	where Upper(v.Address1) like '%' + Upper(address1) + '%'
	and Upper(v.Address2) like '%' + Upper(address2) + '%'
	and v.Zipcode = zipCode)
or v.Phone in (select phoneNum1 from #claimParms)
or v.Phone in (select phoneNum2 from #claimParms)
or v.Fax in (select phoneNum1 from #claimParms)
or v.Fax in (select phoneNum2 from #claimParms)
or v.Email in (select email from #claimParms))


select * from #FoundFraudulent 

if OBJECT_ID('tempdb.dbo.#claimParms', 'U') is not null
	drop table #claimParms 

if OBJECT_ID('tempdb.dbo.#FoundFraudulent', 'U') is not null
	drop table #FoundFraudulent 

if OBJECT_ID('tempdb.dbo.#vehicleParms', 'U') is not null
	drop table #vehicleParms 

End
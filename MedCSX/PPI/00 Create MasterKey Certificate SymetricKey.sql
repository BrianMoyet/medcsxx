--Create Master Key
CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'N(XLtwN]/vW"2A5tD~!t-\';



--Create self signed certificate
USE Claims;
GO
CREATE CERTIFICATE Certificate_Encrypt WITH SUBJECT = 'Encrypt Data';
GO

--create symetric key
CREATE SYMMETRIC KEY SymKey_Encrypt WITH ALGORITHM = AES_128 ENCRYPTION BY CERTIFICATE Certificate_Encrypt;
USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Refresh_Policy_Individuals]    Script Date: 3/17/2021 4:07:56 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Refresh Policy Individuals for a given Policy and Version Number. 

ALTER PROCEDURE [dbo].[usp_Refresh_Policy_Individuals]
@claim_id as foreign_key,
@policy_no as personal_lines_policy_no,
@version_no as personal_lines_version_no,
@user_id foreign_key
AS
declare @cnt integer

OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;

-- Only Refresh if Policy Individual info has changed
if (select count(*) 
from personallines.dbo.pol_individuals 
where no_policy = @policy_no and no_version = @version_no) <>  
(select count(*) from personallines.dbo.pol_individuals p, policy_individual c
where c.no_policy = @policy_no and c.no_version = @version_no
and c.claim_id = @claim_id
and p.no_policy = @policy_no and p.no_version = @version_no
and p.no_indiv=c.no_indiv and p.indivtype=c.indivtype
and p.name_salu=c.name_salu
and p.name_first=c.name_first
and p.name_midin=c.name_midin
and p.name_last=c.name_last
and p.name_suffx=c.name_suffx
and p.address1=c.address1
and p.address2=c.address2
and p.city=c.city
and p.name_state=c.name_state
and p.cd_zip=c.cd_zip
and p.dt_birth=c.dt_birth
and dbo.fn_extract_phone_number(p.phone_home) = dbo.fn_extract_phone_number(c.phone_home)
and dbo.fn_extract_phone_number(p.phone_bus) = dbo.fn_extract_phone_number(c.phone_bus))
begin

-- Clean up from an Aborted Open Claim Attempt
update person set claim_id = @claim_id where person_id in
(select person_id from person where policy_individual_id in 
(select policy_individual_id from policy_individual where claim_id=@claim_id))

/* Set Person_Type_Id and Policy_Individual Id to Zero in Person table for Claim (they will be refreshed 
later from Personal Lines.  This is to prevent FK violations during the refresh. */
update person set person_type_id = 0, policy_individual_id = 0 where claim_id = @claim_id

/* Delete Existing New_Policy_Individual Rows for Claim */
delete from policy_individual where claim_id = @claim_id

/* Insert rows into Policy_Individual from Personal Lines */
insert into policy_individual (claim_id, 
No_Policy,
No_Version,
No_Indiv,
IndivType,
No_Account,
Name_Salu,
Name_First,
Name_Midin,
Name_Last,
Name_Suffx,
No_SocSec,
Dt_Birth,
Age,
Sex,
Stat_Martl,
Address1,
Address2,
City,
County_Code,
Name_State,
Cd_Zip,
Employer,
Phone_Bus,
Phone_Bext,
Phone_Home,
No_Claim,
GeneralComment,
RelToApplicant,
created_by, modified_by)
select @claim_id, *, @user_id, @user_id 
from personallines.dbo.pol_individuals
where no_policy = @policy_no and no_version = @version_no

/* Fix wierd ssn and phone number formatting from personal lines */

update policy_individual set no_socsec='' 
where no_socsec='___-__-____' and claim_id=@claim_id

update policy_individual set phone_home='' 
where phone_home='(___)-___-____' and claim_id=@claim_id

update policy_individual set phone_bus='' 
where phone_bus='(___)-___-____' and claim_id=@claim_id

update policy_individual set phone_home= replace(phone_home, ')-', ') ')
where claim_id=@claim_id

update policy_individual set phone_bus= replace(phone_bus, ')-', ') ')
where claim_id=@claim_id

update policy_individual set phone_home= replace(phone_home, '(___)', '')
where claim_id=@claim_id

-- Fix addresses in Policy_Individual from Pol_GeneralInfo or InsuredItem_Address
declare @version decimal(4,1)
declare @address1 varchar(100), @address2 varchar(100), @city varchar(100), @state varchar(2), @zip varchar(20), @isGaragingAddress bit
declare @locatedHere bit

select @version = max(no_version) from personallines.dbo.pol_generalinfo where no_policy = @policy_no
and personallines.dbo.fn_Is_Even_Version(no_version) = 1

select @address1 = address1, @address2 = address2, @city = name_city, @state = name_state, @zip = cd_zip, @locatedHere = locatedhere
from personallines.dbo.pol_generalinfo where no_policy = @policy_no and no_version = @version

if @locatedhere = 0
begin
	set @isGaragingAddress = 1
	select @address1 = address1, @address2 = isnull(address2, ''), @city = name_city, @state = cd_state, @zip = cd_zip 
	from personallines.dbo.insureditem_address
	where no_policy = @policy_no and no_version = @version and address1 <> ''
end
else
	set @isGaragingAddress = 0

update Policy_Individual set address1 = @address1, address2 = @address2, city = @city, name_state = @state, cd_zip = @zip, is_garaging_address = @isGaragingAddress
where claim_id = @claim_id

/* Insert New Persons from Policy_Individual to Person table for the claim */
insert into person (claim_id, policy_individual_id, person_type_id, salutation, last_name, middle_initial, first_name, suffix,
address1, address2, city, state_id, zipcode, home_phone, work_phone, date_of_birth,
ssn, sex, employer, language_id, created_by, modified_by)
select p.claim_id, p.policy_individual_id, pt.person_type_id, dbo.fn_Encrypt(name_salu), name_last, name_midin, name_first, name_suffx,
dbo.fn_Encrypt(address1), dbo.fn_Encrypt(address2), dbo.fn_Encrypt(city), dbo.fn_Encrypt(s.state_id), dbo.fn_Encrypt(cd_zip), dbo.fn_Encrypt(phone_home), dbo.fn_Encrypt(phone_bus), dbo.fn_Encrypt(dt_birth),
dbo.fn_Encrypt(no_socsec), dbo.fn_Encrypt(sex), dbo.fn_Encrypt(employer), 2 /* english */, @user_id, @user_id
from policy_individual p, state s, personallines.dbo._indivtype i, person_type pt
where s.abbreviation = p.name_state
and p.claim_id = @claim_id
and i.itype = p.indivtype and pt.description = i.itdescription
and dbo.get_policy_individual_name (policy_individual_id) not in
(select dbo.get_person_name (person_id) from person where claim_id = @claim_id)

/* Update Person table from Policy_Individual where the Names match */
update person set policy_individual_id = p.policy_individual_id, 
person_type_id = pt.person_type_id, 
/*salutation = p.name_salu ,                  
last_name =  p.name_last, 
middle_initial = p.name_midin, 
first_name = p.name_first, 
suffix = p.name_suffx,
address1 = p.address1, 
address2 = p.address2, 
city = p.city, 
state_id = s.state_id, 
zipcode = p.cd_zip, 
home_phone = p.phone_home, 
work_phone = p.phone_bus, 
ssn = p.no_socsec, 
sex = p.sex, 
employer = p.employer, */
language_id = 2 /* english */, 
modified_by = p.modified_by
from person ps, policy_individual p, state s, personallines.dbo._indivtype i, person_type pt
where s.abbreviation = p.name_state
and ps.claim_id = p.claim_id
and p.claim_id = @claim_id
and i.itype = p.indivtype and pt.description = i.itdescription
and dbo.get_policy_individual_name (p.policy_individual_id) = dbo.get_person_name (ps.person_id)
--and ps.date_of_birth = p.dt_birth

/* Insert 'Unknown' person if not already in Person table for Claim */
select @cnt = count(*) from person where first_name = 'Unknown' and claim_id = @claim_id
if @cnt = 0
begin
	insert into person (claim_id, first_name) values (@claim_id, 'Unknown')
end

/* Insert 'Parked & Unoccupied' person if not already in Person table for Claim */
select @cnt = count(*) from person where first_name = 'Parked and Unoccupied' and claim_id = @claim_id
if @cnt = 0
begin
	insert into person (claim_id, first_name) values (@claim_id, 'Parked and Unoccupied')
end

/* Update Drivers License No from Pol_Drivers */
update person 
set drivers_license_no = dbo.fn_Encrypt(d.cd_state + ' ' + d.no_drv_lic)
from person p, personallines.dbo.pol_drivers d, policy_individual i
where p.claim_id = @claim_id
and p.policy_individual_id = i.policy_individual_id
and i.no_policy = d.no_policy 
and i.no_version = d.no_version
and i.no_indiv = d.no_indiv
and p.drivers_license_no = ''

end
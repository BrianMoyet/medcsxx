USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Witness_Passengers_With_Images]    Script Date: 3/11/2021 4:01:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Get_Witness_Passengers_With_Images] @claim_id foreign_key AS 

select 
w.Witness_Passenger_Id, 
wt.Description, 
dbo.get_person_name(w.person_id) as [Person], 
dbo.fn_decrypt(p.address1) + ' ' +  dbo.fn_decrypt(p.Address2) as Address, 
DecryptByKey(City) as City, 
Abbreviation, 
DecryptByKey(Zipcode) as Zipcode, 
wt.Image_Index, 
w.Person_Id 
from witness_passenger w, 
witness_passenger_type wt, 
person p, 
state s 
where w.witness_passenger_type_id = wt.witness_passenger_type_id and w.claim_id = @claim_id and w.person_id = p.person_id and  DecryptByKey(p.state_id) = s.state_id
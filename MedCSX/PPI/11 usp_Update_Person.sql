USE [Claims_Encrypt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Update_Person]    Script Date: 3/17/2021 4:45:05 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER   PROCEDURE [dbo].[usp_Update_Person] 
@person_id primary_key,
@claim_id int,
@person_type_id int,
@policy_individual_id int,
@salutation salutation,
@last_name last_name,
@middle_initial middle_initial,
@first_name first_name,
@suffix suffix,
@address1 address,
@address2 address,
@city city,
@state_id int,
@zipcode zipcode,
@home_phone phone_number,
@work_phone phone_number,
@cell_phone phone_number,
@other_contact_phone phone_number,
@fax phone_number,
@email email,
@date_of_birth date_with_default,
@ssn ssn,
@drivers_license_no drivers_license_no,
@sex sex,
@employer employer,
@language_id int,
@is_company bit,
@user_id created_by,
@addComment varchar(max) = '',
@fraudInd bit = 0,
@hasOtherIns bit = 0,
@OtherInsCompany varchar(64) = '',
@OtherInsPolicyNo varchar(32) = '',
@OtherInsClaimNo varchar(32) = '',
@OtherInsAgent varchar(64) = '',
@OtherInsAgentAddress address = '',
@OtherInsAgentCity city = '',
@OtherInsAgentStateId int = 0,
@OtherInsAgentZip zipcode = '',
@OtherInsAgentPhone phone_number = '',
@OtherInsAgentFax phone_number = '',
@OtherInsAgentEmail email = ''
AS
OPEN SYMMETRIC KEY SymKey_Encrypt
        DECRYPTION BY CERTIFICATE Certificate_Encrypt;

UPDATE Person
SET claim_id = @claim_id, 
person_type_id = @person_type_id, 
policy_individual_id = @policy_individual_id,
salutation =  dbo.fn_Encrypt(@salutation), 
last_name = @last_name, 
middle_initial = @middle_initial, 
first_name = @first_name,
suffix = @suffix, 
address1 =  dbo.fn_Encrypt(@address1), 
address2 =  dbo.fn_Encrypt(@address2), 
city =  dbo.fn_Encrypt(@city),
state_id =  dbo.fn_Encrypt(@state_id), 
zipcode =  dbo.fn_Encrypt(@zipcode), 
home_phone =  dbo.fn_Encrypt(@home_phone), 
work_phone =  dbo.fn_Encrypt(@work_phone), 
cell_phone =  dbo.fn_Encrypt(@cell_phone), 
other_contact_phone =  dbo.fn_Encrypt(@other_contact_phone), 
fax =  dbo.fn_Encrypt(@fax), 
email =  dbo.fn_Encrypt(@email), 
date_of_birth =  dbo.fn_Encrypt(@date_of_birth), 
SSN = dbo.fn_Encrypt(@ssn), 
drivers_license_no =  dbo.fn_Encrypt(@drivers_license_no), 
sex =  dbo.fn_Encrypt(@sex), 
employer =  dbo.fn_Encrypt(@employer),
language_id =  dbo.fn_Encrypt(@language_id), 
is_company =  @is_company, 
Comments =  dbo.fn_Encrypt(@addComment),
Flag_Fraud = @fraudInd, Has_Other_Insurance = @hasOtherIns, 
Other_Insurance_Company =  dbo.fn_Encrypt(@OtherInsCompany), 
Other_Insurance_Policy_No =  dbo.fn_Encrypt(@OtherInsPolicyNo), 
Other_Insurance_Claim_No =  dbo.fn_Encrypt(@OtherInsClaimNo), 
Other_Insurance_Agent_Name =  dbo.fn_Encrypt(@OtherInsAgent), 
Other_Insurance_Agent_Address =  dbo.fn_Encrypt(@OtherInsAgentAddress), 
Other_Insurance_Agent_City =  dbo.fn_Encrypt(@OtherInsAgentCity), 
Other_Insurance_Agent_State_Id =  dbo.fn_Encrypt(@OtherInsAgentStateId),
Other_Insurance_Agent_ZipCode =  dbo.fn_Encrypt(@OtherInsAgentZip), 
Other_Insurance_Agent_Phone =  dbo.fn_Encrypt(@OtherInsAgentPhone), 
Other_Insurance_Agent_Fax =  dbo.fn_Encrypt(@OtherInsAgentFax), 
Other_Insurance_Agent_Email =  dbo.fn_Encrypt(@OtherInsAgentEmail),
modified_by = @user_id, modified_date = getdate()
WHERE person_id = @person_id

	CLOSE SYMMETRIC KEY SymKey_Encrypt;



USE [Claims_Encrypt]
GO

/****** Object:  UserDefinedFunction [dbo].[Get_Person_Name]    Script Date: 3/11/2021 1:16:51 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/* Return a Person Row Formatted as a Displayable Name, such as "Mr David A Smith Jr"  */

ALTER  FUNCTION [dbo].[Get_Person_Name] (@person_id foreign_key)
RETURNS person_name  AS  
BEGIN 
	declare @person_name description


	if @person_id = 0 
	begin
		set @person_name = ''
	end
	else
	begin
		/* Build Displayable Name */
		select @person_name = ltrim(rtrim((dbo.fn_Decrypt(salutation) + ' ' + first_name + ' ' + middle_initial +
			 ' ' + case last_name when '_' then '' else last_name end + ' ' + suffix)))
		from person WITH(NOLOCK)
		where person_id = @person_id	

		/* Eliminate excessive embedded spaces */
		set @person_name = replace (@person_name, '  ', ' ')
		set @person_name = replace (@person_name, '  ', ' ')
		set @person_name = replace (@person_name, '  ', ' ')		
		set @person_name = replace (@person_name, '  ', ' ')
	end

	return @person_name
END









GO



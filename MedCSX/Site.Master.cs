﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using MedCSX.App_Code;
using MedCsxDatabase;
using MedCsxLogic;

namespace MedCSX
{
    public partial class SiteMaster : MasterPage
    {

        private ModForms mf = new ModForms();
        private ModMain mm = new ModMain();
        private static clsDatabase db2 = new clsDatabase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null)
            {
                //string ymp = Session["userID"].ToString();
                if (Convert.ToInt32(Session["userID"]) < 1)
                    Response.Redirect("NotLoggedIn.html");
            }
            else
            {
                Response.Redirect("NotLoggedIn.html");
            }

            string sql = "SELECT        COUNT(*) AS count FROM Draft AS d INNER JOIN  Claim AS c ON dbo.Claim_Id_For_Draft_Id(d.Draft_Id) = c.Claim_Id WHERE(d.In_Draft_Queue <> 0)";
            DataTable list = db2.ExecuteSelectReturnDataTable(sql);

            //tsbDraftQueue.Text = "Draft Queue (" + list.Rows[0][0].ToString() + ")";

            //if (Convert.ToInt32(list.Rows[0][0]) > 1)
            //    tsbDraftQueue.ForeColor = System.Drawing.Color.Red;

            sql = "SELECT        COUNT(*) AS count FROM Draft AS d INNER JOIN  Claim AS c ON dbo.Claim_Id_For_Draft_Id(d.Draft_Id) = c.Claim_Id INNER JOIN                          [User] ON d.Created_By = [User].User_Id WHERE(d.In_Draft_Queue <> 0) AND([User].User_Default_Draft_Printer_Id = 1)";
            list = db2.ExecuteSelectReturnDataTable(sql);

            tsbDraftKC.Text = "KC Draft Queue (" + list.Rows[0][0].ToString() + ")";

            if (Convert.ToInt32(list.Rows[0][0]) > 1)
                tsbDraftKC.ForeColor = System.Drawing.Color.Red;

            sql = "SELECT        COUNT(*) AS count FROM Draft AS d INNER JOIN  Claim AS c ON dbo.Claim_Id_For_Draft_Id(d.Draft_Id) = c.Claim_Id INNER JOIN                          [User] ON d.Created_By = [User].User_Id WHERE(d.In_Draft_Queue <> 0) AND([User].User_Default_Draft_Printer_Id = 2)";
            list = db2.ExecuteSelectReturnDataTable(sql);

            tsbDraftLV.Text = "LV Draft Queue (" + list.Rows[0][0].ToString() + ")";

            if (Convert.ToInt32(list.Rows[0][0]) > 1)
                tsbDraftLV.ForeColor = System.Drawing.Color.Red;


        }

        protected void btnOpenClaim_Click(object sender, EventArgs e)
        {
            if (txtClaimNo.Text.Trim().Length < 1)
                return;
            //int clmNotFound = 0;
            if (!(new MedCsxLogic.Claim()).Exists(Convert.ToInt32(txtClaimNo.Text.Trim()))) 
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Claim Number " + txtClaimNo.Text.Trim() + " Not Found');</script>");
                //MessageBox.Show("Claim Number " + txtClaimNo.Text.Trim() + " Not Found", "Claim Not Found", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                txtClaimNo.Text = "";
                return;
            }
            
            

            MedCsxLogic.Claim myPClaim = new MedCsxLogic.Claim(Convert.ToInt32(txtClaimNo.Text.Trim()));
            string url = "ViewClaim.aspx?tabindex=18&claim_id=" + this.txtClaimNo.Text;
            string win = "window.open('" + url + "','my_window" + myPClaim.ClaimId + "', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=900, copyhistory=no, left=400, top=100').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
            txtClaimNo.Text = "";
        }

        protected void tsbOpenAppraisals_Click(object sender, EventArgs e)
        {
            string url = "frmOpenAppraisals.aspx";
            string win = "window.open('" + url + "','mywindowAppraisals', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=500, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
        }

        protected void tsbDraftQueue_Click(object sender, EventArgs e)
        {
            string url = "frmDraftQueue.aspx";
            string win = "window.open('" + url + "','mywindowDraftQue', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
        }

        protected void tsbPendingDrafts_Click(object sender, EventArgs e)
        {
            string url = "frmPendingDrafts.aspx";
            string win = "window.open('" + url + "','mywindowDraftQue', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=400, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
        }

        protected void mnuOpenAppraisals_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                string url = "frmOpenAppraisals.aspx";
                string win = "window.open('" + url + "','mywindowAppraisals', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=500, copyhistory=no, left=500, top=200').focus();";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('" + ex.ToString() + "');</script>");

               
            }
        }

        protected void mnuMyDiaryAll_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            mf.ShowAllDiaryEntriesForUserReport(Convert.ToInt32(Session["userID"]));
        }

        protected void mnuMyDiaryPastDue_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            mf.ShowPastDueDiaryEntriesForUserReport(Convert.ToInt32(Session["userID"]));
        }

        protected void mnuPendingSalvage_Click(object sender, EventArgs e)
        {
            mf.ShowPendingSalvageReport();
        }

        protected void mnuPendingPoliceReports_Click(object sender, EventArgs e)
        {
            mf.ShowPendingPoliceReports();
        }

        protected void mnuRecentlyPrintedDrafts_Click(object sender, EventArgs e)
        {

        }

        protected void mnuRecoveryAmountsPerAdjuster_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Recovery_Amounts_Report) == false)
                return;

            mf.ShowReport("http://reports.medjames.com/Reports/Pages/Report.aspx?ItemPath=%2fClaims%2fRecovery+Amounts+per+Adjuster");
        }

        protected void mnuPendingReservesPerAdjuster_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            mf.ShowPendingReservesPerAdjusterReport();
        }

        protected void mnuFirstContactRatesPerAdjuster_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            mf.ShowReport("http://reports.medjames.com/Reports/Pages/Report.aspx?ItemPath=%2fClaims%2fMedCSx+First+Contact+Report");
        }

        protected void mnuPendingClaimsbyDate_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();


            mf.ShowReport("http://reports.medjames.com/Reports/Pages/Report.aspx?ItemPath=%2fClaims%2fPending+Claims+by+Date");
        }

        protected void mnuPendingReserveTypesPerAdjuster_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsNull();

                //Dictionary<string, string> rptParms = new Dictionary<string, string>();
                //rptParms.Add("textbox1", "");

                ShowReport("Pending Reserve Types by Adjuster", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void mnuClaimSetupTimes_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsNull();

                ShowReport("Claim Setup Times", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void mnuZeroReserveLines_Click(object sender, EventArgs e)
        {
            string url = "frmZeroReserveLines.aspx";
            string win = "window.open('" + url + "','mywindowAppraisals', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
        }

        protected void mnuReservesWithoutAdjusterFileNotes_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsNull();

                ShowReport("Reserves Without Adjuster File Notes", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void mnuOpenClaimsWithoutOpenReserves_Click(object sender, EventArgs e)
        {
            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsNull();

                ShowReport("Open Claims Without Open Reserves", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void mnuUnprintedDrafts_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Print_Draft_Queue) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Unprinted Drafts.');</script>");
                return;
            }

            string url = "frmAllUnprintedDrafts.aspx";
            string win = "window.open('" + url + "','mywindowAppraisals', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=500, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwin", win, true);
        }

        public void ShowReport(string url)
        {
            //url += " + --new- window";
            try
            {
                mm.UpdateLastActivityTime();

                string s = "window.open('" + url + "', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=600, copyhistory=no, left=400, top=200');";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);


            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        private Dictionary<string, string> GetReportParmsNull()
        {
          
            Dictionary<string, string> newparms = new Dictionary<string, string>();

            //newparms.Add("Title", "");

            return newparms;
        }

        private Dictionary<string, string> GetReportParmsSubro()
        {
            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            Dictionary<string, string> newparms = new Dictionary<string, string>();

            newparms.Add("Date1", firstDayOfMonth.ToShortDateString());
            newparms.Add("Date2", lastDayOfMonth.ToShortDateString());

            return newparms;
        }

        protected void mnuDailyDraftReport_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Accounting_Reports) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Accounting Reports');</script>");
                return;
            }

            string url = "frmPeriodicReport.aspx?period=d";
            ShowReport(url);
          
        }

        public void ShowReport(string reportName, Dictionary<string, string> rptParms)
        {
            byte[] bytes;
            //Dictionary<string, string> rptParms;
            ReportDocument rptDocument = new ReportDocument();
            System.Net.NetworkCredential networkUser;
            ReportService rsNew;

            networkUser = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReportServerUser"], ConfigurationManager.AppSettings["ReportServerPassword"], ConfigurationManager.AppSettings["ReportServer"]);
            rsNew = new ReportService(networkUser, ConfigurationManager.AppSettings["ReportingServicesEndPoint"], ConfigurationManager.AppSettings["ReportingServicesExecutionEndPoint"]);
            rptDocument.Path = ConfigurationManager.AppSettings["ReportPath"];
            rptDocument.Name = reportName; // "All Diary Entries for User";
            bytes = rsNew.GetByFormat(rptDocument, rptParms);

            Session["binaryData"] = bytes;
            //Response.Redirect("frmReport.aspx");
            string url = "frmReport.aspx";
            string s = "window.open('" + url + "', 'popup_windowReport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=995, height=900, copyhistory=no, left=200, top=0');";
            //ClientScript.RegisterStartupScript(this.GetType(), "popup_windowReport", s, true);
            ScriptManager.RegisterStartupScript(this, GetType(), "popup_windowReport", s, true);

        }

        protected void mnuMonthlyDraftReport_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Accounting_Reports) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Accounting Reports');</script>");
                return;
            }

            string url = "frmPeriodicReportMonthly.aspx?period=m";
            ShowReport(url);
           
        }

        protected void mnuMonthlyDraftReportSummary_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Accounting_Reports) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Accounting Reports');</script>");
                return;
            }

            string url = "frmPeriodicReportMonthly.aspx?period=m";
            ShowReport(url);
        }

        protected void mnuMonthlySubroSalvageReport_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Accounting_Reports) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized for Accounting Reports');</script>");
                return;
            }

            try
            {
                mm.UpdateLastActivityTime();

                Dictionary<string, string> rptParms;
                rptParms = GetReportParmsSubro();

                ShowReport("MedCsx Monthly Subro Salvage Report", rptParms);
            }
            catch (Exception ex)
            {
                mf.ProcessError(ex);
            }
        }

        protected void mnuEditVendors_Click(object sender, EventArgs e)
        {
            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Vendors))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Manage Users');</script>");
                return;
            }

           

            string s = "window.open('frmVendors.aspx?lookup_value=aa&search_column=Name', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=900, height=900, copyhistory=no, left=400, top=200');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuEditUsers_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Users))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Manage Users');</script>");
                return;
            }


            string s = "window.open('../admin/frmUsers.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1250, height=925, copyhistory=no, left=10, top=10');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);


        }

        protected void hllChangePassword_Click(object sender, EventArgs e)
        {
            string url = "/claim/frmChangePassword.aspx?uid=" + Convert.ToInt32(HttpContext.Current.Session["userID"]);
            string s = "window.open('" + url + "', 'popup_window', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=500, height=500, copyhistory=no, left=400, top=200');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }

        protected void mnuEditTypeTables_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Type_Tables))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Edit Type Tables');</script>");
                return;
            }


            string s = "window.open('../admin/frmEditTypeTables.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=800, copyhistory=no, left=10, top=10');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);

        }

        protected void mnuLogInAs_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Log_In_As))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }


            string s = "window.open('../admin/frmLoginAs.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=400, height=500, copyhistory=no, left=400, top=200');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuConfigureDocuments_Click(object sender, EventArgs e)
        {
            mm.UpdateLastActivityTime();

            if (!mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Create_New_Document))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized to Create Documents');</script>");
                return;
            }

            string s = "window.open('../admin/frmConfigureDocuments.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=700, copyhistory=no, left=400, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuEditFileNoteTypes_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Type_Tables) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }


            string s = "window.open('../admin/frmEditFileNoteTypes.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=700, copyhistory=no, left=400, top=200');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuEditUserTaskTypes_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Type_Tables) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }


            string s = "window.open('../admin/frmEditUserTaskTypes.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1100, height=700, copyhistory=no, left=400, top=200');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuSetAverageReserves_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Set_Average_Reserves) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }


            string s = "window.open('../admin/frmSetAverageReserves.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=800, copyhistory=no, left=400, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuGlobalSettings_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Edit_Global_Settings) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }

            string s = "window.open('../admin/frmSettings.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=800, copyhistory=no, left=400, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuVendorTypeSubTypeAssociations_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Vendor_Type_Sub_Type_Associations) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }

            string s = "window.open('../admin/frmVendorTypeSubTypeAssociations.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=600, copyhistory=no, left=400, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuEditSceneAccessAppraisers_Click(object sender, EventArgs e)
        {
            string s = "window.open('../admin/frmSceneAccessAppraisers.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=600, height=800, copyhistory=no, left=400, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);
        }

        protected void mnuLoggedInUsers_Click(object sender, EventArgs e)
        {
            if (mm.CheckUserAuthorization((int)modGeneratedEnums.Task.Logged_In_Users) == false)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "msgBox1", "<script>alert('Not Authorized!');</script>");
                return;
            }

            string s = "window.open('../admin/frmLoggedInUsers.aspx', 'popup_windowreport', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, copyhistory=no, left=400, top=100');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "reportscript", s, true);

        }

        protected void tsbDraftKC_Click(object sender, EventArgs e)
        {
            string url = "frmUnprintedDraftsKC.aspx";
            string win = "window.open('" + url + "','mywindowDraftQueKC', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwinKC", win, true);
        }

        protected void tsbDraftLV_Click(object sender, EventArgs e)
        {
            string url = "frmUnprintedDraftsLV.aspx";
            string win = "window.open('" + url + "','mywindowDraftQueLV', 'toolbar=no, location=no, titlebar=no, linkbar=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, copyhistory=no, left=500, top=200').focus();";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "newwinLV", win, true);
        }
        //protected void DisplayRecentClaims()
        //{
        //    DataTable dt = mm.LoadRecentClaims(mm.UserId);
        //    ddlRecentClaims.DataSource = dt;
        //    ddlRecentClaims.DataBind();

        //}

        //protected void lbRecentClaims_Click(object sender, EventArgs e)
        //{
        //    DisplayRecentClaims();
        //}
    }
}
Key Insurance Company
P.O. Box 201
Overland Park, KS 66201

#DATE#

#APPRAISER_FIRST_NAME#,

Please complete the assignment for the following claim.  
When you are done, reply to this email, attaching all related photos and documents.  
You must submit this assignment to us by replying to this email. 
You must not change the return email address or the subject line in any way. 

The property being inspected should be appropriately photographed commensurate with 
professional appraisal standards. Photographing potential points of impact with a yard stick 
in the photo should be done. Prior damage should be carefully documented. In 
addition, at a minimum the following nine photographs should always be included in your inspection. 
Direct photos of the front, sides and rear of the vehicle. These photographs should be 
framed that the entire side of the car fills the area of the picture, but shows the entire side.
 Direct photos of each corner of the vehicle should be taken. A photograph of the 
interior showing the front passenger area including any locations where air bags could have been deployed. 

When in doubt, photograph it. We would prefer to have 10 pictures we do not need as 
opposed to one picture that we do need.

All photos must be returned at the resolution that they were taken. Metadata should never be altered.
Do not shrink, compress or in any other way alter the original photograph. Always send the original 
photographs. You may reference particular photos in your comments. In special cases we may need photos
printed to a document for the purposes of adding captions. We will address those special cases as they 
arise.

Finally, and importantly please take a photo of the title and/or registration if it is available. 
If you contact the claimant to set a time for the appraisal, please remind the claimant to have that
with them if they are going to be present. Otherwise, remind them to send a copy to the adjuster.

Your acceptence of this assignment constitutes your agreement to comply with the above. At its 
discretion Key will not accept any fee bill that does not comply with the above directions.

Thank you,

#ADJUSTER_NAME#, Adjuster
Phone: (913) 663-5500 x#ADJUSTER_EXT#

 
Assignment  Info
----------------------------------------------------------------
 
Appraiser: #APPRAISER_NAME#
Insured or Claimant: #INSURED_OR_CLAIMANT#
Task Description: #TASK_DESCRIPTION#

Special Instructions: #SPECIAL_INSTRUCTIONS#


Vehicle Info
----------------------------------------------------------------
 
Vehicle: #VEHICLE_NAME#
VIN: #VIN#
Color: #VEHICLE_COLOR#
License Plate State: #LICENSE_PLATE_STATE#
License Plate: #LICENSE_PLATE#
Location Contact: 
	#VEHICLE_LOCATION_CONTACT#

Location: 
	#VEHICLE_LOCATION#
 
Damage Description: #DAMAGE_DESCRIPTION#
Where Seen: #WHERE_SEEN#
Estimated Damage Amount: #ESTIMATED_DAMAGE_AMOUNT#


Claim Info
----------------------------------------------------------------
 
Claim Number: #CLAIM_NUMBER#
Policy Number: #POLICY_NUMBER#
Date of Loss: #DATE_OF_LOSS#
Deductible: #DEDUCTIBLE#
Type of Claim: #TYPE_OF_CLAIM#
Loss Description: #LOSS_DESCRIPTION#

Insured: #INSURED_INFO

Insured Vehicle: #INSURED_VEHICLE#

Claimant: #CLAIMANT_INFO#


ImageRight Info - Do Not Change
----------------------------------------------------------------
DRAWER: CLMS
FILENO: #CLAIM_NUMBER#
PACKAGETYPE: 30800
DOCTYPE: APPR
FLOW: 20
STEP: 67
TASKPRIORITY: 5

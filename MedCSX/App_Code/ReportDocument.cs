﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class ReportDocument
    {
        private string _path;
        private string _name;

        public string Path
        {
            get
            {
                return (_path);
            }
            set
            {
                _path = value;
            }
        }

        public string Name
        {
            get
            {
                return (_name);
            }
            set
            {
                _name = value;
            }
        }
    }
}
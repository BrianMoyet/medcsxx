﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace MedCSX.App_Code
{
    public class Recon
    {
        public DataTable GetRecon(DateTime _recon_date, int _property_damage)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Recon_Reserves", cn);
                cmd.Parameters.AddWithValue("@recon_date", _recon_date);
                cmd.Parameters.AddWithValue("@Property_Damage", _property_damage);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

    }
}
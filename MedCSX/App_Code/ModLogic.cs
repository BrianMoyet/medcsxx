﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class ModLogic
    {

        public DataTable UsersWhichCanBeAssignedClaims()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Users_Which_Can_Be_Assigned_Claims", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable UserGroupsWhichCanBeAssignedClaims()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_User_Groups_Which_Can_Be_Assigned_Claims", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }
    }
}
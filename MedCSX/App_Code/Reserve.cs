﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Reserve
    {
        public string claimant_name { get; set; }
        public string reserve_type { get; set; }
        public string claimant_id { get; set; }
        public string claim_id { get; set; }
        public string net_loss_reserve { get; set; }
        public string net_expense_reserve { get; set; }
        public string paid_loss { get; set; }
        public string paid_expense { get; set; }
        public string policy_coverage_type_id { get; set; }
        public string version_no { get; set; }
       




        public DataTable GetReserves(int _userID)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd =  new SqlCommand("Get_My_Reserves2", cn);
                cmd.Parameters.AddWithValue("@user_id", _userID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public DataTable GetReserves_By_ClaimId(int _claim_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Reserves_By_ClaimId", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }


            return dtReserveTable;
        }

        public DataTable GetReserves_With_Images_By_ClaimId(int _claim_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Reserves_With_Images", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }


            return dtReserveTable;
        }

        public DataTable Get_Reserve_Assignments_For_Claim(int _claim_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Reserve_Assignments_For_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }


            return dtReserveTable;
        }

        public DataTable GeTransactions_By_ReserveId(int _reserve_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Transactions_For_Reserve", cn);
                cmd.Parameters.AddWithValue("@reserve_id", _reserve_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public DataTable GetReserveTypes()
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Reserve_Types", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public DataTable GetReserveBySubClaimType(int sub_claim_type_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Reserve_Types_by_Sub_Claim_Type_Id", cn);
                cmd.Parameters.AddWithValue("@sub_claim_type_id", sub_claim_type_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public DataTable usp_Get_Reserve_Names_For_Claimant(int claimant_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Reserve_Names_For_Claimant", cn);
                cmd.Parameters.AddWithValue("@claimant_id", claimant_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public void GetReserveByID(int _reserve_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_GetReserve_Details_By_ID", cn);
                cmd.Parameters.AddWithValue("@reserve_id", _reserve_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    claimant_name = reader["claimant_name"].ToString();
                    reserve_type = reader["reserve_type"].ToString();
                    claimant_id = reader["claimant_id"].ToString();
                    claim_id = reader["claim_id"].ToString();
                    net_loss_reserve = reader["net_loss_reserve"].ToString();
                    net_expense_reserve = reader["net_expense_reserve"].ToString();
                    paid_loss = reader["paid_loss"].ToString();
                    paid_expense = reader["paid_expense"].ToString();
                    policy_coverage_type_id = reader["policy_coverage_type_id"].ToString();
                    version_no = reader["version_no"].ToString();
                }

            }


        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using MedCsxDatabase;
using MedCsxLogic;

namespace MedCSX.App_Code
{
    public class Claim
    {
        public string mode { get; set; }
        public bool isExpired { get; set; }
        public string claim_status_id { get; set; }   
        public bool is_expired_policy { get; set; }
        public bool isLocked { get; set; }
        public string company { get; set; }
        public string location { get; set; }
        public string ClaimID { get; set; }
        public string display_claim_id { get; set; }
        public string ClaimNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string Version_No { get; set; }
        public string DateOfLoss { get; set; }
        //public string DateReported { get; set; }
        public string NameInsured { get; set; }
        public bool ClaimFound { get; set; }
        public string InsuredVehicle { get; set; }
        public string LossDescription { get; set; }



        public string AutoBodilyInjury { get; set; }
        public string AutoBodilyInjuryPerPerson { get; set; }
        public string Collision { get; set; }
        public string Comp_Coll { get; set; }
        public string Comprehensive { get; set; }
        public string Glass { get; set; }
        public string MedicalPayments { get; set; }
        public string NoCoverage { get; set; }
        public string PersonalInjuryProtection { get; set; }
        public string PIPEssentialServices { get; set; }
        public string PIPFuneral { get; set; }
        public string PIPMedical { get; set; }
        public string PIPRehab { get; set; }
        public string PIPWages { get; set; }
        public string PropertyDamage { get; set; }
        public string UM_UIM { get; set; }
        public string UM_UIMPerPerson { get; set; }
        public string UnderinsuredMotorist { get; set; }
        public string UninsuredMotorist { get; set; }

        public string adjuster_id { get; set; }
        public string insured_person_id { get; set; }
        public string insured_vehicle_id { get; set; }
        public string owner_person_id { get; set; }
        public string driver_person_id { get; set; }
        public string where_seen { get; set; }
        public string loss_location { get; set; }
        public string loss_city { get; set; }
        public string loss_state_id { get; set; }
        public string authority_contacted { get; set; }
        public string police_report_number { get; set; }
        public string reported_by { get; set; }
        public string reported_by_type_id { get; set; }
        public string comments { get; set; }
        public string date_reported { get; set; }
        public string accident_type_id { get; set; }
        public string at_fault_type_id { get; set; }
        public string has_coverage_issue { get; set; }
        public string has_disputed_liability { get; set; }
        public string permission_to_use { get; set; }
        public string is_comp { get; set; }
        public string is_collision { get; set; }
        public string large_loss { get; set; }
        public string hasSR22 { get; set; }
        public string is_foxpro_conversion { get; set; }
        public string foxpro_claim_no { get; set; }
        public string vehicle_damage { get; set; }
        public string locationAbbreviation { get; set; }
        public string sub_Claim_Type_Description { get; set; }
        public string claim_status_description { get; set; }
        public string sub_claim_type_id { get; set; }
        public string company_location_id { get; set; }
        public string Modified_By { get; set; }
        public string Modified_Date { get; set; }
        public string Created_By { get; set; }
        public string Created_Date { get; set; }
        public int ClaimTypeId { get; set; }
        private Hashtable _coverageDescriptions { get; set; } /* TODO ERROR: Skipped SkippedTokensTrivia */
        public delegate bool DelegateBool2(int int1, Hashtable ht1);
        public bool PolicyExists { get; set; }

        //public Claim(int id)
        //{
        //    GetClaimGeneralInfo(id);
        //}

        //public string AgentDescription
        //{
        //    get
        //    {
        //        // Return Agent Description for a Claim

        //        Hashtable agent = null/* TODO Change to default(_) if this is not a reference type */;
        //        string s = "";

        //        List<Hashtable> rows = db.ExecuteStoredProcedureReturnHashList("usp_Get_Agent_For_Claim", this.Id);
        //        if (rows.Count() == 0)
        //            return "";

        //        agent = rows.Item[0];
        //        s = System.Convert.ToString(agent.Item("Name_Firm1"));

        //        if (System.Convert.ToString(agent.Item("Name_Firm2")) != "")
        //            s = s + Environment.NewLine + System.Convert.ToString(agent.Item("Name_Firm2"));

        //        if (System.Convert.ToString(agent.Item("Address1")) != "")
        //            s = s + Environment.NewLine + System.Convert.ToString(agent.Item("Address1"));

        //        if (System.Convert.ToString(agent.Item("Address2")) != "")
        //            s = s + Environment.NewLine + System.Convert.ToString(agent.Item("Address2"));

        //        s = s + Environment.NewLine + System.Convert.ToString(agent.Item("Name_City")) + ", " + System.Convert.ToString(agent.Item("Name_State")) + " " + System.Convert.ToString(agent.Item("Cd_zip"));

        //        if (System.Convert.ToString(agent.Item("Phone_Bus")) != "")
        //            s = s + Environment.NewLine + "Phone: " + System.Convert.ToString(agent.Item("Phone_Bus"));

        //        if (System.Convert.ToString(agent.Item("Phone_Fax")) != "")
        //            s = s + Environment.NewLine + "Fax: " + System.Convert.ToString(agent.Item("Phone_Fax"));

        //        return s;
        //    }
        //}

        //public string AdjusterDescription
        //{
        //    get
        //    {
        //        string s = "";
        //        User thisAdjuster = new User();
        //        thisAdjuster.   this.adjuster_id;

        //        s = thisAdjuster.Name;

        //        if (string.IsNullOrWhiteSpace(thisAdjuster.PhoneExt) == false)
        //            s += Environment.NewLine + "Ph: x " + thisAdjuster.PhoneExt;
        //        if (string.IsNullOrWhiteSpace(thisAdjuster.DirectFax) == false)
        //            s += Environment.NewLine + "Fax: " + thisAdjuster.DirectFax;
        //        if (string.IsNullOrWhiteSpace(thisAdjuster.Email) == false)
        //            s += Environment.NewLine + "Email: " + thisAdjuster.Email;
        //        return s;
        //    }
        //}


        public int Update()
        {
            int successful = 0;
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Update_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_id", Convert.ToInt32(ClaimID));
                cmd.Parameters.AddWithValue("@date_reported", Convert.ToDateTime(date_reported));
                cmd.Parameters.AddWithValue("@Date_Of_Loss", Convert.ToDateTime(DateOfLoss));
                cmd.Parameters.AddWithValue("@at_fault_type_id", Convert.ToInt32(at_fault_type_id));
                cmd.Parameters.AddWithValue("@insured_person_id", Convert.ToInt32(insured_person_id));
                cmd.Parameters.AddWithValue("@insured_vehicle_id", Convert.ToInt32(insured_vehicle_id));
                cmd.Parameters.AddWithValue("@owner_person_id", Convert.ToInt32(owner_person_id));
                cmd.Parameters.AddWithValue("@driver_person_id", Convert.ToInt32(driver_person_id));
                cmd.Parameters.AddWithValue("@vehicle_damage", vehicle_damage);
                cmd.Parameters.AddWithValue("@where_seen", where_seen);
                cmd.Parameters.AddWithValue("@loss_location", loss_location);
                cmd.Parameters.AddWithValue("@loss_city", loss_city);
                cmd.Parameters.AddWithValue("@loss_state_id", Convert.ToInt32(loss_state_id));
                cmd.Parameters.AddWithValue("@accident_type_id", Convert.ToInt32(accident_type_id));
                cmd.Parameters.AddWithValue("@Loss_Description", LossDescription);
                cmd.Parameters.AddWithValue("@authority_contacted", authority_contacted);
                cmd.Parameters.AddWithValue("@police_report_number", police_report_number);
                cmd.Parameters.AddWithValue("@reported_by", reported_by);
                cmd.Parameters.AddWithValue("@reported_by_type_id", Convert.ToInt32(reported_by_type_id));
                cmd.Parameters.AddWithValue("@comments", comments);
                cmd.Parameters.AddWithValue("@permission_to_use", Convert.ToInt32(permission_to_use));
                cmd.Parameters.AddWithValue("@is_comp", Convert.ToInt32(is_comp));
                cmd.Parameters.AddWithValue("@has_coverage_issue", Convert.ToInt32(has_coverage_issue));
                cmd.Parameters.AddWithValue("@has_disputed_liability", Convert.ToInt32(has_disputed_liability));
                cmd.Parameters.AddWithValue("@large_loss", Convert.ToInt32(large_loss));
                cmd.Parameters.AddWithValue("@is_foxpro_conversion", Convert.ToInt32(is_foxpro_conversion));
                cmd.Parameters.AddWithValue("@is_collision", Convert.ToInt32(is_collision));
                cmd.Parameters.AddWithValue("@hasSR22", Convert.ToInt32(hasSR22));
                cmd.Parameters.AddWithValue("@Modified_By", Convert.ToInt32(Modified_By));
                cmd.Parameters.AddWithValue("@Modified_Date", Convert.ToDateTime(Modified_Date));

                cmd.CommandType = CommandType.StoredProcedure;
                successful = (int)cmd.ExecuteNonQuery();


            }



            return successful;
        }

        public DataTable CheckClaimLocks(int _ClaimID)
                {
                    isLocked = false;
                    DataTable dt = new DataTable();

                    using (var cn = new ClaimsDB().GetOpenConnection())
                    {
                        var cmd = new SqlCommand("Get_Claim_Locks", cn);
                        cmd.Parameters.AddWithValue("@claim_ID", _ClaimID);
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(dt);
                        }

                    }
                    if (dt.Rows.Count > 0)
                        isLocked = true;
                    return dt;
                }

        public void GetClaimTitleInfo(int _ClaimID)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Claim_Title", cn);
                cmd.Parameters.AddWithValue("@claim_ID", _ClaimID);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                  
                    display_claim_id = reader["display_claim_id"].ToString();
                    sub_Claim_Type_Description = reader["ClaimType_description"].ToString();
                    claim_status_description = reader["Claim_Status_Description"].ToString();
                    sub_claim_type_id = reader["sub_claim_type_id"].ToString();
                }
            }
        }

        public void InsertRecentClaim(int user_id, int claim_id)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Insert_Recent_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_id", claim_id);
                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }
        }

        public string SyncPolicy(bool associatingWithNewPolicy = false)
        {
            //synchronize the claim with the policy

            //switch (this.ClaimId)
            //{
            //    case (int)modGeneratedEnums.ClaimType.Auto:
            //sync claim with PLA.  Return any problems as an error message string.
            _coverageDescriptions = null;
            string msg = "";
            int insPersonId = 0;
            int drivPersonId = 0;
            Hashtable coverages = null;
            Hashtable coverage = null;
            string versionNo = "";
            DateTime dateOfLoss;

            //get insured vehicle, person and drive IDs
            Vehicle insVehicle = this.InsuredVehicle;
            insPersonId = this.InsuredPersonId;
            drivPersonId = this.DriverPersonId;
            Person drivPerson = this.DriverPerson;
            Person insPerson = this.InsuredPerson;

            if (this.PolicyNo.Trim() == "")
                return msg;

            if (!this.PolicyExists)
            {
                if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Not_Found))
                    msg += "Policy " + this.PolicyNo + " Not found in Personal Lines" + Environment.NewLine;

                //if policy isn't found in PLA, we don't need the other PLA related locks to be set (NSF, Date of Loss not covered,etc.).
                //they can be unlocked
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Now_Found);
                msg += this.ClearCancellationEvents();
                return msg;
            }
            else
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Not_Found, (int)modGeneratedEnums.EventType.Policy_Now_Found))
                    msg += "Policy " + this.PolicyNo + " is Now Found in Personal Lines" + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_Found);
                this.ClearEvent((int)modGeneratedEnums.EventType.Claim_Created_Without_Policy);
            }

            //check version number
            dateOfLoss = this.DateOfLoss;
            versionNo = db.GetStringFromStoredProcedure("usp_Get_Personal_Lines_Version_No", this.PolicyNo, dateOfLoss);

            if (Convert.ToDouble(versionNo) == -1)
            {
                //Did not receive good version number from PLA and the policy is not supposed to be expired
                if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
                    msg += "Policy " + this.PolicyNo + " Not in Force For Date of Loss " + dateOfLoss + Environment.NewLine;

                this.isExpiredPolicy = true;
                this.UpdateNamedInsured();

                //if policy is not in force for date of claim - NSF Cancellation, Pro-Rate, etc. do not apply and can be unlocked
                msg += this.ClearCancellationEvents();
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss);
            }
            else
            {
                //Received a good version number from PLA
                if (this.isExpiredPolicy)
                {
                    //if the claim used to belong to an expired policy, it is now in force
                    if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
                        msg += "Policy " + this.PolicyNo + " is Now in Force for the Date of Loss " + this.DateOfLoss + Environment.NewLine;
                    this.isExpiredPolicy = false;
                    this.Update();
                }
                else
                {
                    //See if the PLA version number changed
                    if ((Convert.ToDouble(versionNo) != this.VersionNo) && (Convert.ToDouble(versionNo) > 0))
                    {
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Personal_Lines_Version_No_Changed))
                            msg += "Personal Lines Version Number Changed from " + this.VersionNo + " To " + versionNo + Environment.NewLine;
                        this.VersionNo = Convert.ToDouble(versionNo);
                        this.Update();
                    }
                    else if (Convert.ToDouble(versionNo) == 0)
                        versionNo = this.VersionNo.ToString();
                }
                //check date of loss and transaction code
                PolGeneralInfo genInfo = new PolGeneralInfo(this.PolicyNo, versionNo);

                if (genInfo.dtExp <= dateOfLoss)
                {
                    if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
                        msg += "Policy " + this.PolicyNo + " Not in Force For Date of Loss " + dateOfLoss + Environment.NewLine;
                    msg += this.ClearCancellationEvents();
                }
                else
                {
                    //Policy is in force for the date of claim
                    if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss, (int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
                        msg += "Policy " + this.PolicyNo + " Now in Force for Date of Loss " + dateOfLoss + Environment.NewLine;
                    this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);

                    //check transaction codes
                    /**** trying old policy
                    switch (genInfo.cdTrx)
                    {
                        case 30:
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation))
                                msg += "Short Rate Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                            this.ClearEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation_No_Longer_Applies);
                            break;
                        case 32:
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation))
                                msg += "Pro-Rate Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                            this.ClearEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation_No_Longer_Applies);
                            break;
                        case 34:
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Flat_Cancellation))
                                msg += "Flat Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                            this.ClearEvent((int)modGeneratedEnums.EventType.Flat_Cancellation_No_Longer_Applies);
                            break;
                        case 35:
                            if (this.FireEvent((int)modGeneratedEnums.EventType.NSF_Cancellation))
                                msg += "NSF Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                            this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_No_Longer_Applies);
                            break;
                        default:
                            //Policy is good - remove short-rate, flat, NSF locks and alerts
                            msg += this.ClearCancellationEvents();
                            break;
                    }
                     ***/
                }
            }
            //check NSF check register in PLA for NSF.  This is a different check than looking for a Transaction code of 35 in Pol_GeneralInfo

            if (db.GetBoolFromStoredProcedure("usp_Is_Policy_Nsf", this.PolicyNo))
            {
                if (this.FireEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_NSF_Check_Register_))
                    msg += "NSF Cancellation (NSF Check Register) on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_NSF_Check_Register_No_Longer_Applies);
            }

            //check Pol_Control Work in Progress Code
            versionNo = this.VersionNo.ToString();
            PolControl polCtrl = new PolControl(this.PolicyNo, versionNo);

            if (polCtrl.row.Count > 0)
            {
                switch (polCtrl.CdWorkInprog.Trim().ToUpper())
                {
                    case "CHNIP":  //Change to Policy in Progress
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress))
                            msg += "Change To Policy in Progress: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Change_To_Policy_In_Progress_No_Longer_Applies);
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress);
                        break;
                    case "HOLD":  //Holding Bad Data See Mel
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel))
                            msg += "Holding Bad Data See Mel: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel_No_Longer_Applies);
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel);
                        break;
                    case "PYERR":  //Payment Posting Error
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Payment_Posting_Error))
                            msg += "Payment Posting Error: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Payment_Posting_Error_No_Longer_Applies);
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Payment_Posting_Error);
                        break;
                    case "WATFR":  //Waiting Final Rating
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Waiting_Final_Rating))
                            msg += "Waiting Final Rating: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Waiting_Final_Rating_No_Longer_Applies);
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Waiting_Final_Rating);
                        break;
                    case "WATNB":  //Waiting for New Business
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Waiting_For_New_Business))
                            msg += "Waiting For New Business: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Waiting_For_New_Business_No_Longer_Applies);
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Waiting_For_New_Business);
                        break;
                    default:
                        //Clear all WIP related events
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, 0);
                        break;
                }
            }
            else
            {
                //There is no corresponding row in Pol_Control - Clear all WIP related events
                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, 0);
            }

            //read current policy coverages into dictionary of Policy_Coverage rows (indexed by Policy_Coverage_Type_Id)
            coverages = new Hashtable();
            foreach (Hashtable row in db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Coverages_For_Claim", this.Id))
                coverages.Add((int)row["Policy_Coverage_Type_Id"], row);

            //refresh policy data
            this.RefreshPolicyData();

            //Check for problems

            //check if insured vehicle is currently on policy
            if (insVehicle.PolicyVehicleId == 0)
            {
                //insured vehicle is no longer on policy
                if (this.FireEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy))
                    msg += "Insured Vehicle " + insVehicle.Name + "is Not on the Policy." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy);
            }
            else
            {
                //insured vehicle is on policy
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy, (int)modGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy))
                    msg += "Insured Vehicle " + insVehicle.Name + " is Now on the Policy." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy);
            }

            //check if insured person is currently on policy
            if (insPerson.PersonTypeId != 1)
            {
                //insured persion is no longer on policy
                if (!associatingWithNewPolicy)
                {
                    if (this.FireEvent((int)modGeneratedEnums.EventType.Insured_Not_On_Policy))
                        msg += "Insured " + insPerson.Name + "is Not on the Policy." + Environment.NewLine;
                    this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Now_On_Policy);
                }
            }
            else
            {
                //insured person is on policy
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Insured_Not_On_Policy, (int)modGeneratedEnums.EventType.Insured_Now_On_Policy))
                    msg += "Insured " + insPerson.Name + " is Now on the Policy." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Not_On_Policy);
            }

            //check if driver is currently excluded on policy
            if (drivPerson.PersonTypeId == (int)modGeneratedEnums.PersonType.Excluded_Driver)
            {
                //driver has changed to be an excluded driver
                if (this.FireEvent((int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver))
                    msg += "Driver " + drivPerson.Name + "is an Excluded Driver." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore);
            }
            else
            {
                //driver is not an excluded driver
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore, (int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver))
                    msg += "Driver " + drivPerson.Name + " is not an Excluded Driver Anymore." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver);
            }

            //check if driver is not listed on policy
            if (((drivPerson.PersonTypeId < 1) || (drivPerson.PersonTypeId > 6)) && (drivPerson.FirstName != "Parked and Unoccupied"))
            {
                //driver is not on policy
                if (this.FireEvent((int)modGeneratedEnums.EventType.Driver_Not_On_Policy))
                    msg += "Driver " + drivPerson.Name + "is Not on the Policy." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Now_On_Policy);
            }
            else
            {
                //driver is on policy
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Driver_Not_On_Policy, (int)modGeneratedEnums.EventType.Driver_Now_On_Policy))
                    msg += "Driver " + drivPerson.Name + " is Now on the Policy." + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Not_On_Policy);
            }

            //check policy coverages
            int policyCoverageTypeId;

            foreach (PolicyCoverage newCoverage in this.PolicyCoverages)
            {
                policyCoverageTypeId = newCoverage.PolicyCoverageTypeId;
                if (!coverages.Contains(policyCoverageTypeId))
                {
                    //check for collision coverage now on policy
                    if (newCoverage.isCollision && newCoverage.hasCoverage)
                    {
                        foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Collision))
                        {
                            if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Collision_Coverage_Now_Exists_on_Policy))
                                msg += "Collision Coverage Now Exists on Policy." + Environment.NewLine;
                            res.ClearEvent((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
                        }

                        if (!this.ValidateVIN(this.InsuredVehicle))
                        {
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                        }
                    }

                    //check for comprehensive coverage now on the policy
                    if (newCoverage.isComprehensive && newCoverage.hasCoverage)
                    {
                        foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Comprehensive))
                        {
                            if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Comp_Coverage_Now_Exists_on_Policy))
                                msg += "Comprehensive Coverage Now Exists on Policy." + Environment.NewLine;
                            res.ClearEvent((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
                        }
                        if (!this.ValidateVIN(this.InsuredVehicle))
                        {
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                        }
                    }
                }
                else
                {
                    //see if coverage has increased/decreased
                    coverage = (Hashtable)coverages[policyCoverageTypeId];

                    //check per person coverage
                    if ((bool)coverage["Has_Per_Person_Coverage"])
                    {
                        if (!newCoverage.hasPerPersonCoverage)
                            msg += this.PerPersonCoverageDecreased(policyCoverageTypeId);
                        else
                        {
                            if (Convert.ToDouble(coverage["Per_Person_Coverage"]) > newCoverage.PerPersonCoverage)
                                msg += this.PerPersonCoverageDecreased(policyCoverageTypeId);
                            else
                            {
                                if (Convert.ToDouble(coverage["Per_Person_Coverage"]) < newCoverage.PerPersonCoverage)
                                    msg += this.PerPersonCoverageIncreased(policyCoverageTypeId);
                            }
                        }
                    }
                    else
                    {
                        if (newCoverage.hasPerPersonCoverage)
                            msg += this.PerPersonCoverageIncreased(policyCoverageTypeId);
                    }

                    //check per accident coverage
                    if ((bool)coverage["Has_Per_Accident_Coverage"])
                    {
                        if (!newCoverage.hasPerAccidentCoverage)
                            msg += this.PerAccidentCoverageDecreased(policyCoverageTypeId);
                        else
                        {
                            if (Convert.ToDouble(coverage["Per_Accident_Coverage"]) > newCoverage.PerAccidentCoverage)
                                msg += this.PerAccidentCoverageDecreased(policyCoverageTypeId);
                            else
                            {
                                if (Convert.ToDouble(coverage["Per_Accident_Coverage"]) < newCoverage.PerAccidentCoverage)
                                    msg += this.PerAccidentCoverageIncreased(policyCoverageTypeId);
                            }
                        }
                    }
                    else
                    {
                        if (newCoverage.hasPerAccidentCoverage)
                            msg += this.PerAccidentCoverageIncreased(policyCoverageTypeId);
                    }

                    //check for collision coverage now on policy
                    if (newCoverage.isCollision && newCoverage.hasCoverage)
                    {
                        foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Collision))
                        {
                            if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Collision_Coverage_Now_Exists_on_Policy))
                                msg += "Collision Coverage Now Exists on Policy." + Environment.NewLine;
                            res.ClearEvent((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
                        }

                        if (!this.ValidateVIN(this.InsuredVehicle))
                        {
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                        }
                    }

                    //check for comprehensive coverage now on the policy
                    if (newCoverage.isComprehensive && newCoverage.hasCoverage)
                    {
                        foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Comprehensive))
                        {
                            if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Comp_Coverage_Now_Exists_on_Policy))
                                msg += "Comprehensive Coverage Now Exists on Policy." + Environment.NewLine;
                            res.ClearEvent((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
                        }

                        if (!this.ValidateVIN(this.InsuredVehicle))
                        {
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                        }
                    }
                }
            }

            Hashtable parms = new Hashtable();
            int thisFraudId = CheckFraud();
            if (thisFraudId > 0)
            {
                parms.Add("Claim_Id", this.ClaimId);
                switch (m_FraudTypeId)
                {
                    case 1:     //vendor
                        parms.Add("Vendor_Id", thisFraudId);
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Vendor_On_Fraud_List))
                            msg += "Vendor " + new Vendor(thisFraudId).Name + " On Fraud List" + Environment.NewLine;
                        break;
                    case 2:     //person
                        parms.Add("Person_Id", thisFraudId);
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Person_On_Fraud_List))
                            msg += new Person(thisFraudId).Name + " On Fraud List" + Environment.NewLine;
                        break;
                    case 3:     //vehicle
                        parms.Add("Vehicle_Id", thisFraudId);
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Vehicle_On_Fraud_List))
                            msg += "Vehicle " + new Vehicle(thisFraudId).ShortName + " On Fraud List" + Environment.NewLine;
                        break;
                }
            }

            return msg;
            //    default:
            //        return "";
            //}
        }

        private int CheckFraud()
        {
            try
            {
                List<Hashtable> search = db.ExecuteStoredProcedureReturnHashList("usp_Get_Fraudulent", this.ClaimId);
                Person p;
                string ph;
                if (search.Count > 0)
                {
                    if (this.OwnerPersonId != 0)
                    {
                        //p = this.OwnerPerson;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_Id"].ToString()))
                            {
                                if (this.OwnerPersonId == (int)item["Person_Id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return OwnerPersonId;
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //{
                            //    this.m_FraudTypeId = 2;
                            //    return (int)item["Person_Id"];
                            //}
                        }
                        //    }
                        //}
                    }

                    if (this.DriverPersonId != 0)
                    {
                        //p = this.DriverPerson;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_Id"].ToString()))
                            {
                                if (this.DriverPersonId == (int)item["Person_Id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return DriverPersonId;
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //  {
                            //      this.m_FraudTypeId = 2;
                            //      return (int)item["Person_Id"];
                            //  }
                        }
                        //    }
                        //}
                    }

                    if (this.InsuredPersonId != 0)
                    {
                        //p = this.InsuredPerson;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_Id"].ToString()))
                            {
                                if (this.InsuredPersonId == (int)item["Person_Id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return InsuredPersonId;
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //{
                            //    this.m_FraudTypeId = 2;
                            //    return (int)item["Person_Id"];
                            //}
                        }
                        //    }
                        //}
                    }

                    foreach (Claimant c in this.Claimants)
                    {
                        //p = c.Person;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_Id"].ToString()))
                            {
                                if (c.PersonId == (int)item["Person_Id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return c.PersonId;
                                }
                            }
                            // if ((bool)item["Flag_Fraud"])
                            //{
                            //    this.m_FraudTypeId = 2;
                            //    return (int)item["Person_Id"];
                            //}
                        }
                        //    }
                        //}
                    }

                    foreach (Hashtable h in this.Vehicles)
                    {
                        //Vehicle v = new Vehicle((int)h["Vehicle_Id"]);
                        //if (v.Manufacturer.ToUpper() != "UNKNOWN")
                        //{
                        //    if (v.VIN != "")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Vehicles_Fraud", v.VIN);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Vehicle_Id"].ToString()))
                            {
                                if ((int)h["Vehicle_Id"] == (int)item["Vehicle_Id"])
                                {
                                    this.m_FraudTypeId = 3;
                                    return (int)item["Vehicle_Id"];
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //          {
                            //              this.m_FraudTypeId = 3;
                            //              return (int)item["Vehicle_Id"];
                            //          }
                        }
                        //    }
                        //}
                    }

                    foreach (Transaction trx in this.Transactions)
                    {
                        if (trx.Draft != null)
                        {
                            Vendor v = trx.Draft.Vendor();
                            //if (!string.IsNullOrWhiteSpace(v.TaxId))
                            //{
                            //    search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Vendors_Fraud", v.TaxId);
                            foreach (Hashtable item in search)
                            {
                                if (!string.IsNullOrWhiteSpace(item["Vehicle_Id"].ToString()))
                                {
                                    if (v.Id == (int)item["Vendor_Id"])
                                    {
                                        this.m_FraudTypeId = 1;
                                        return (int)item["Vendor_Id"];
                                    }
                                }
                                //if ((bool)item["Flag_Fraud"])
                                //      {
                                //          this.m_FraudTypeId = 1;
                                //          return (int)item["Vendor_Id"];
                                //      }
                            }
                            //}
                        }
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string SyncPolicy(bool associatingWithNewPolicy = false)
        //{
        //    //synchronize the claim with the policy

        //    //switch (this.ClaimId)
        //    //{
        //    //    case (int)ModGeneratedEnums.ClaimType.Auto:
        //    //sync claim with PLA.  Return any problems as an error message string.
        //    _coverageDescriptions = null;
        //    string msg = "";
        //    int insPersonId = 0;
        //    int drivPersonId = 0;
        //    Hashtable coverages = null;
        //    Hashtable coverage = null;
        //    string versionNo = "";
        //    DateTime dateOfLoss;

        //    //get insured vehicle, person and drive IDs
        //    Vehicle insVehicle = new Vehicle();
        //    insVehicle.GetVehicleByID(Convert.ToInt32(insured_vehicle_id));
        //    //Vehicle insVehicle = this.InsuredVehicle;
        //    Person insPerson = new Person();
        //    insPerson.GetPersonByPersonID(Convert.ToInt32(insured_person_id));
        //    //insPersonId = this.InsuredPersonId;
        //    //drivPersonId = this.DriverPersonId;
        //    Person drivPerson = new Person();
        //    drivPerson.GetPersonByPersonID(Convert.ToInt32(driver_person_id));
        //    //Person insPerson = this.InsuredPerson;

        //    if (this.PolicyNumber.Trim() == "")
        //        return msg;

        //    if (!this.PolicyExists)
        //    {
        //        if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Not_Found))
        //            msg += "Policy " + this.PolicyNumber + " Not found in Personal Lines" + Environment.NewLine;

        //        //if policy isn't found in PLA, we don't need the other PLA related locks to be set (NSF, Date of Loss not covered,etc.).
        //        //they can be unlocked
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Now_Found);
        //        msg += this.ClearCancellationEvents();
        //        return msg;
        //    }
        //    else
        //    {
        //        if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Policy_Not_Found, (int)ModGeneratedEnums.EventType.Policy_Now_Found))
        //            msg += "Policy " + this.PolicyNumber + " is Now Found in Personal Lines" + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Not_Found);
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Claim_Created_Without_Policy);
        //    }

        //    //check version number
        //    dateOfLoss = Convert.ToDateTime(this.DateOfLoss);
        //    versionNo = GetVersionNo(this.PolicyNumber, dateOfLoss);


        //    if (Convert.ToDouble(versionNo) == -1)
        //    {
        //        //Did not receive good version number from PLA and the policy is not supposed to be expired
        //        if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
        //            msg += "Policy " + this.PolicyNumber + " Not in Force For Date of Loss " + dateOfLoss + Environment.NewLine;

        //        this.is_expired_policy = true;
        //        this.UpdateNamedInsured();

        //        //if policy is not in force for date of claim - NSF Cancellation, Pro-Rate, etc. do not apply and can be unlocked
        //        msg += this.ClearCancellationEvents();
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss);
        //    }
        //    else
        //    {
        //        //Received a good version number from PLA
        //        if (this.is_expired_policy)
        //        {
        //            //if the claim used to belong to an expired policy, it is now in force
        //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
        //                msg += "Policy " + this.PolicyNumber + " is Now in Force for the Date of Loss " + this.DateOfLoss + Environment.NewLine;
        //            this.is_expired_policy = false;
        //            this.Update();
        //        }
        //        else
        //        {
        //            //See if the PLA version number changed
        //            if ((Convert.ToDouble(versionNo) != Convert.ToDouble(this.Version_No)) && (Convert.ToDouble(versionNo) > 0))
        //            {
        //                if (this.FireEvent((int)ModGeneratedEnums.EventType.Personal_Lines_Version_No_Changed))
        //                    msg += "Personal Lines Version Number Changed from " + this.Version_No + " To " + versionNo + Environment.NewLine;
        //                this.Version_No = versionNo.ToString();
        //                this.Update();
        //            }
        //            else if (Convert.ToDouble(versionNo) == 0)
        //                versionNo = this.Version_No.ToString();
        //        }
        //        //check date of loss and transaction code
        //        PolicyData genInfo = new PolicyData();
        //            genInfo.PolGeneralInfo(this.PolicyNumber, versionNo);

        //        if (genInfo.dt_Exp <= dateOfLoss)
        //        {
        //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
        //                msg += "Policy " + this.PolicyNumber + " Not in Force For Date of Loss " + dateOfLoss + Environment.NewLine;
        //            msg += this.ClearCancellationEvents();
        //        }
        //        else
        //        {
        //            //Policy is in force for the date of claim
        //            if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss, (int)ModGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
        //                msg += "Policy " + this.PolicyNumber + " Now in Force for Date of Loss " + dateOfLoss + Environment.NewLine;
        //            this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);


        //        }
        //    }
        //    //check NSF check register in PLA for NSF.  This is a different check than looking for a Transaction code of 35 in Pol_GeneralInfo

        //    if (HelperFunctions.GetBoolFromStoredProcedure("usp_Is_Policy_Nsf", this.PolicyNumber))
        //    {
        //        if (this.FireEvent((int)ModGeneratedEnums.EventType.NSF_Cancellation_NSF_Check_Register_))
        //            msg += "NSF Cancellation (NSF Check Register) on Policy " + this.PolicyNumber + " Version " + versionNo + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.NSF_Cancellation_NSF_Check_Register_No_Longer_Applies);
        //    }

        //    //check Pol_Control Work in Progress Code
        //    versionNo = this.Version_No;
        //    //PolControl polCtrl = new PolControl(this.PolicyNo, versionNo);

        //    //if (polCtrl.row.Count > 0)
        //    //{
        //    //    switch (polCtrl.CdWorkInprog.Trim().ToUpper())
        //    //    {
        //    //        case "CHNIP":  //Change to Policy in Progress
        //    //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Change_To_Policy_in_Progress))
        //    //                msg += "Change To Policy in Progress: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
        //    //            this.ClearEvent((int)ModGeneratedEnums.EventType.Change_To_Policy_In_Progress_No_Longer_Applies);
        //    //            msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)ModGeneratedEnums.EventType.Change_To_Policy_in_Progress);
        //    //            break;
        //    //        case "HOLD":  //Holding Bad Data See Mel
        //    //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Holding_Bad_Data_See_Mel))
        //    //                msg += "Holding Bad Data See Mel: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
        //    //            this.ClearEvent((int)ModGeneratedEnums.EventType.Holding_Bad_Data_See_Mel_No_Longer_Applies);
        //    //            msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)ModGeneratedEnums.EventType.Holding_Bad_Data_See_Mel);
        //    //            break;
        //    //        case "PYERR":  //Payment Posting Error
        //    //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Payment_Posting_Error))
        //    //                msg += "Payment Posting Error: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
        //    //            this.ClearEvent((int)ModGeneratedEnums.EventType.Payment_Posting_Error_No_Longer_Applies);
        //    //            msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)ModGeneratedEnums.EventType.Payment_Posting_Error);
        //    //            break;
        //    //        case "WATFR":  //Waiting Final Rating
        //    //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Waiting_Final_Rating))
        //    //                msg += "Waiting Final Rating: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
        //    //            this.ClearEvent((int)ModGeneratedEnums.EventType.Waiting_Final_Rating_No_Longer_Applies);
        //    //            msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)ModGeneratedEnums.EventType.Waiting_Final_Rating);
        //    //            break;
        //    //        case "WATNB":  //Waiting for New Business
        //    //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Waiting_For_New_Business))
        //    //                msg += "Waiting For New Business: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
        //    //            this.ClearEvent((int)ModGeneratedEnums.EventType.Waiting_For_New_Business_No_Longer_Applies);
        //    //            msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)ModGeneratedEnums.EventType.Waiting_For_New_Business);
        //    //            break;
        //    //        default:
        //    //            //Clear all WIP related events
        //    //            msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, 0);
        //    //            break;
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    //There is no corresponding row in Pol_Control - Clear all WIP related events
        //    //    msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, 0);
        //    //}

        //    //read current policy coverages into dictionary of Policy_Coverage rows (indexed by Policy_Coverage_Type_Id)
        //    coverages = new Hashtable();
        //    foreach (Hashtable row in db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Coverages_For_Claim", this.Id))
        //        coverages.Add((int)row["Policy_Coverage_Type_Id"], row);

        //    //refresh policy data
        //    this.RefreshPolicyData();

        //    //Check for problems

        //    //check if insured vehicle is currently on policy
        //    if (insVehicle.PolicyVehicleId == 0)
        //    {
        //        //insured vehicle is no longer on policy
        //        if (this.FireEvent((int)ModGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy))
        //            msg += "Insured Vehicle " + insVehicle.Name + "is Not on the Policy." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy);
        //    }
        //    else
        //    {
        //        //insured vehicle is on policy
        //        if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy, (int)ModGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy))
        //            msg += "Insured Vehicle " + insVehicle.Name + " is Now on the Policy." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy);
        //    }

        //    //check if insured person is currently on policy
        //    if (insPerson.PersonTypeId != 1)
        //    {
        //        //insured persion is no longer on policy
        //        if (!associatingWithNewPolicy)
        //        {
        //            if (this.FireEvent((int)ModGeneratedEnums.EventType.Insured_Not_On_Policy))
        //                msg += "Insured " + insPerson.Name + "is Not on the Policy." + Environment.NewLine;
        //            this.ClearEvent((int)ModGeneratedEnums.EventType.Insured_Now_On_Policy);
        //        }
        //    }
        //    else
        //    {
        //        //insured person is on policy
        //        if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Insured_Not_On_Policy, (int)ModGeneratedEnums.EventType.Insured_Now_On_Policy))
        //            msg += "Insured " + insPerson.Name + " is Now on the Policy." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Insured_Not_On_Policy);
        //    }

        //    //check if driver is currently excluded on policy
        //    if (drivPerson.PersonTypeId == (int)ModGeneratedEnums.PersonType.Excluded_Driver)
        //    {
        //        //driver has changed to be an excluded driver
        //        if (this.FireEvent((int)ModGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver))
        //            msg += "Driver " + drivPerson.Name + "is an Excluded Driver." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore);
        //    }
        //    else
        //    {
        //        //driver is not an excluded driver
        //        if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore, (int)ModGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver))
        //            msg += "Driver " + drivPerson.Name + " is not an Excluded Driver Anymore." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver);
        //    }

        //    //check if driver is not listed on policy
        //    if (((drivPerson.PersonTypeId < 1) || (drivPerson.PersonTypeId > 6)) && (drivPerson.FirstName != "Parked and Unoccupied"))
        //    {
        //        //driver is not on policy
        //        if (this.FireEvent((int)ModGeneratedEnums.EventType.Driver_Not_On_Policy))
        //            msg += "Driver " + drivPerson.Name + "is Not on the Policy." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Driver_Now_On_Policy);
        //    }
        //    else
        //    {
        //        //driver is on policy
        //        if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Driver_Not_On_Policy, (int)ModGeneratedEnums.EventType.Driver_Now_On_Policy))
        //            msg += "Driver " + drivPerson.Name + " is Now on the Policy." + Environment.NewLine;
        //        this.ClearEvent((int)ModGeneratedEnums.EventType.Driver_Not_On_Policy);
        //    }

        //    //check policy coverages
        //    int policyCoverageTypeId;

        //    foreach (PolicyCoverage newCoverage in this.PolicyCoverages)
        //    {
        //        policyCoverageTypeId = newCoverage.PolicyCoverageTypeId;
        //        if (!coverages.Contains(policyCoverageTypeId))
        //        {
        //            //check for collision coverage now on policy
        //            if (newCoverage.isCollision && newCoverage.hasCoverage)
        //            {
        //                foreach (Reserve res in this.ReservesForReserveType((int)ModGeneratedEnums.ReserveType.Collision))
        //                {
        //                    if (res.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage, (int)ModGeneratedEnums.EventType.Collision_Coverage_Now_Exists_on_Policy))
        //                        msg += "Collision Coverage Now Exists on Policy." + Environment.NewLine;
        //                    res.ClearEvent((int)ModGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
        //                }

        //                if (!this.ValidateVIN(this.InsuredVehicle))
        //                {
        //                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Manual_Claim_Lock))
        //                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
        //                }
        //            }

        //            //check for comprehensive coverage now on the policy
        //            if (newCoverage.isComprehensive && newCoverage.hasCoverage)
        //            {
        //                foreach (Reserve res in this.ReservesForReserveType((int)ModGeneratedEnums.ReserveType.Comprehensive))
        //                {
        //                    if (res.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage, (int)ModGeneratedEnums.EventType.Comp_Coverage_Now_Exists_on_Policy))
        //                        msg += "Comprehensive Coverage Now Exists on Policy." + Environment.NewLine;
        //                    res.ClearEvent((int)ModGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
        //                }
        //                if (!this.ValidateVIN(this.InsuredVehicle))
        //                {
        //                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Manual_Claim_Lock))
        //                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //see if coverage has increased/decreased
        //            coverage = (Hashtable)coverages[policyCoverageTypeId];

        //            //check per person coverage
        //            if ((bool)coverage["Has_Per_Person_Coverage"])
        //            {
        //                if (!newCoverage.hasPerPersonCoverage)
        //                    msg += this.PerPersonCoverageDecreased(policyCoverageTypeId);
        //                else
        //                {
        //                    if (Convert.ToDouble(coverage["Per_Person_Coverage"]) > newCoverage.PerPersonCoverage)
        //                        msg += this.PerPersonCoverageDecreased(policyCoverageTypeId);
        //                    else
        //                    {
        //                        if (Convert.ToDouble(coverage["Per_Person_Coverage"]) < newCoverage.PerPersonCoverage)
        //                            msg += this.PerPersonCoverageIncreased(policyCoverageTypeId);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (newCoverage.hasPerPersonCoverage)
        //                    msg += this.PerPersonCoverageIncreased(policyCoverageTypeId);
        //            }

        //            //check per accident coverage
        //            if ((bool)coverage["Has_Per_Accident_Coverage"])
        //            {
        //                if (!newCoverage.hasPerAccidentCoverage)
        //                    msg += this.PerAccidentCoverageDecreased(policyCoverageTypeId);
        //                else
        //                {
        //                    if (Convert.ToDouble(coverage["Per_Accident_Coverage"]) > newCoverage.PerAccidentCoverage)
        //                        msg += this.PerAccidentCoverageDecreased(policyCoverageTypeId);
        //                    else
        //                    {
        //                        if (Convert.ToDouble(coverage["Per_Accident_Coverage"]) < newCoverage.PerAccidentCoverage)
        //                            msg += this.PerAccidentCoverageIncreased(policyCoverageTypeId);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (newCoverage.hasPerAccidentCoverage)
        //                    msg += this.PerAccidentCoverageIncreased(policyCoverageTypeId);
        //            }

        //            //check for collision coverage now on policy
        //            if (newCoverage.isCollision && newCoverage.hasCoverage)
        //            {
        //                foreach (Reserve res in this.ReservesForReserveType((int)ModGeneratedEnums.ReserveType.Collision))
        //                {
        //                    if (res.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage, (int)ModGeneratedEnums.EventType.Collision_Coverage_Now_Exists_on_Policy))
        //                        msg += "Collision Coverage Now Exists on Policy." + Environment.NewLine;
        //                    res.ClearEvent((int)ModGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
        //                }

        //                if (!this.ValidateVIN(this.InsuredVehicle))
        //                {
        //                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Manual_Claim_Lock))
        //                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
        //                }
        //            }

        //            //check for comprehensive coverage now on the policy
        //            if (newCoverage.isComprehensive && newCoverage.hasCoverage)
        //            {
        //                foreach (Reserve res in this.ReservesForReserveType((int)ModGeneratedEnums.ReserveType.Comprehensive))
        //                {
        //                    if (res.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage, (int)ModGeneratedEnums.EventType.Comp_Coverage_Now_Exists_on_Policy))
        //                        msg += "Comprehensive Coverage Now Exists on Policy." + Environment.NewLine;
        //                    res.ClearEvent((int)ModGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
        //                }

        //                if (!this.ValidateVIN(this.InsuredVehicle))
        //                {
        //                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Manual_Claim_Lock))
        //                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
        //                }
        //            }
        //        }
        //    }

        //    Hashtable parms = new Hashtable();
        //    int thisFraudId = CheckFraud();
        //    if (thisFraudId > 0)
        //    {
        //        parms.Add("Claim_Id", this.ClaimId);
        //        switch (m_FraudTypeId)
        //        {
        //            case 1:     //vendor
        //                parms.Add("Vendor_Id", thisFraudId);
        //                if (this.FireEvent((int)ModGeneratedEnums.EventType.Vendor_On_Fraud_List))
        //                    msg += "Vendor " + new Vendor(thisFraudId).Name + " On Fraud List" + Environment.NewLine;
        //                break;
        //            case 2:     //person
        //                parms.Add("Person_Id", thisFraudId);
        //                if (this.FireEvent((int)ModGeneratedEnums.EventType.Person_On_Fraud_List))
        //                    msg += new Person(thisFraudId).Name + " On Fraud List" + Environment.NewLine;
        //                break;
        //            case 3:     //vehicle
        //                parms.Add("Vehicle_Id", thisFraudId);
        //                if (this.FireEvent((int)ModGeneratedEnums.EventType.Vehicle_On_Fraud_List))
        //                    msg += "Vehicle " + new Vehicle(thisFraudId).ShortName + " On Fraud List" + Environment.NewLine;
        //                break;
        //        }
        //    }

        //    return msg;
        //    //    default:
        //    //        return "";
        //    //}
        //}

        public void UpdateNamedInsured()
        {
            //Updates the named insured on the claim.  This will be called if the Policy changed.  
            //Also updates any named insured claimants (comp or collision).
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Named_Insured", cn);
                cmd.Parameters.AddWithValue("@claim_id", ClaimID);
                
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            
            int newInsuredPersonId = Convert.ToInt32(dt.Rows[0][0]);
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Update_Claimant_Persons", cn);
                cmd.Parameters.AddWithValue("@claim_id", ClaimID);
                cmd.Parameters.AddWithValue("@old_person_id", insured_person_id);
                cmd.Parameters.AddWithValue("@new_person_id", newInsuredPersonId);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
                       
            this.insured_person_id = newInsuredPersonId.ToString();
            this.Update();
        }

        public string GetVersionNo(string policyNo, DateTime dol)
        {
            string vn = "";
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Personal_Lines_Version_No", cn);
                cmd.Parameters.AddWithValue("@policy_no", policyNo);
                cmd.Parameters.AddWithValue("@policy_coverage_type_id", dol);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            vn = dt.Rows[0][0].ToString();
            return vn;
        }

    //public string SyncPolicy(bool associatingWithNewPolicy = false, bool doneLoading = true, string msgTimer = "")
    //{
    //    // Synchronize the claim with the policy

        //    switch (ClaimTypeId)
        //    {
        //        case (int)ModGeneratedEnums.ClaimType.Auto:
        //            {
        //                // Auto claim

        //                // Sync Claim with Personal Lines.  Return any problems as an error message string.
        //                msgTimer += "Enter Claim.SyncPolicy " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

        //                // Me.ReadEventTypeIds()
        //                _coverageDescriptions = null;

        //                string msg = "";
        //                int insuredPersonId = 0;
        //                int driverPersonId = 0;
        //                Hashtable coverages = null/* TODO Change to default(_) if this is not a reference type */;
        //                Hashtable coverage = null/* TODO Change to default(_) if this is not a reference type */;
        //                string verNo = "";
        //                DateTime dtOfLoss;

        //                // Get Insured Vehicle, Person, and Driver Ids
        //                Vehicle insuredVehicle = this.insured_vehicle_id;
        //                insuredPersonId = this.insured_person_id;
        //                driverPersonId = this.DriverPersonId;
        //                Person driverPerson = this.DriverPerson;
        //                Person insuredPerson = this.InsuredPerson;
        //                msgTimer += "Initialization " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;


        //                if (this.PolicyNo.Trim == "")
        //                    goto done;// Claim was explicitly created without a policy - don't try to sync

        //                if (this.PolicyExists == false)
        //                {
        //                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Not_Found))
        //                        msg = msg + "Policy " + this.PolicyNo + " Not Found in Personal Lines" + Environment.NewLine;

        //                    // If Policy isn't found in Personal Lines, we don't need the other
        //                    // Personal Lines related locks to be set (NSF, Date of Loss not Covered, etc.). They can be unlocked.
        //                    this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);
        //                    this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Now_Found);
        //                    msg += this.ClearCancellationEvents;
        //                    goto done;
        //                }
        //                else
        //                {
        //                    if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Policy_Not_Found, (int)ModGeneratedEnums.EventType.Policy_Now_Found))
        //                        msg += "Policy " + this.PolicyNo + " is Now Found in Personal Lines" + Environment.NewLine;

        //                    this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Not_Found);
        //                    this.ClearEvent((int)ModGeneratedEnums.EventType.Claim_Created_Without_Policy);
        //                }
        //                msgTimer += "Policy Not Found " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

        //                // Check Version No
        //                dtOfLoss = this.DateOfLoss;
        //                string originalVersionNo = "";
        //                if (verNo != "")
        //                {
        //                    if (System.Convert.ToDouble(verNo) != 0)
        //                        originalVersionNo = this.VersionNo.ToString;
        //                }
        //                verNo = db.GetStringFromStoredProcedure("usp_Get_Personal_Lines_Version_No", this.PolicyNo, DateOfLoss);

        //                if (System.Convert.ToDouble(verNo) == -1)
        //                {
        //                    // We didn't get a good version number from Personal Lines and the Policy is not
        //                    // supposed to be expired.

        //                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
        //                        msg += "Policy " + this.PolicyNo + " Not in Force For Date of Loss " + DateOfLoss + Environment.NewLine;

        //                    this.IsExpiredPolicy = true;
        //                    this.UpdateNamedInsured();

        //                    // If Policy is Not in Force for the Date of Claim - NSF Cancellation, Pro-Rate, etc
        //                    // do not apply and can be unlocked
        //                    msg += this.ClearCancellationEvents;
        //                    this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss);
        //                    msgTimer += "Policy Not in Force " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;
        //                }
        //                else if (System.Convert.ToDouble(verNo) != -1)
        //                {
        //                    // We have a Good Personal Lines Version Number

        //                    if (this.IsExpiredPolicy)
        //                    {
        //                        // If the Claim used to belong to an expired policy, it is now in force
        //                        if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
        //                            msg += "Policy " + this.PolicyNo + " is Now in Force for the Date of Loss " + this.DateOfLoss + Environment.NewLine;

        //                        this.IsExpiredPolicy = false;
        //                        this.Update();
        //                        msgTimer += "Expired Policy " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;
        //                    }
        //                    else
        //                        // See if the Personal Lines Version Number Changed
        //                        if ((System.Convert.ToDouble(verNo) != this.VersionNo))
        //                    {
        //                        if (this.FireEvent((int)ModGeneratedEnums.EventType.Personal_Lines_Version_No_Changed))
        //                            msg += "Personal Lines Version Number Changed from " + this.VersionNo + " To " + VersionNo + Environment.NewLine;

        //                        this.VersionNo = System.Convert.ToDouble(verNo);
        //                        this.Update();
        //                    }

        //                    // Check Date of Loss and Transaction Code
        //                    PolGeneralInfo genInfo = new PolGeneralInfo(this.PolicyNo, verNo);

        //                    if (genInfo.DtExp <= DateOfLoss)
        //                    {
        //                        if (this.FireEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
        //                            msg += "Policy " + this.PolicyNo + " Not in Force For Date of Loss " + DateOfLoss + Environment.NewLine;
        //                        msg += this.ClearCancellationEvents;
        //                    }
        //                    else
        //                    {
        //                        // Policy is in Force for the Date of Claim
        //                        if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss, (int)ModGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
        //                            msg += "Policy " + this.PolicyNo + " Now in Force for Date of Loss " + DateOfLoss + Environment.NewLine;
        //                        this.ClearEvent((int)ModGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);

        //                        // Check Transaction Codes
        //                        switch (genInfo.CdTrx)
        //                        {
        //                            case 30:
        //                                {
        //                                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Short_Rate_Cancellation))
        //                                        msg += "Short Rate Cancellation on Policy " + this.PolicyNo + " Version " + VersionNo + Environment.NewLine;

        //                                    this.ClearEvent((int)ModGeneratedEnums.EventType.Short_Rate_Cancellation_No_Longer_Applies);
        //                                    break;
        //                                }

        //                            case 32:
        //                                {
        //                                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Pro_Rate_Cancellation))
        //                                        msg += "Pro-Rate Cancellation on Policy " + this.PolicyNo + " Version " + VersionNo + Environment.NewLine;

        //                                    this.ClearEvent((int)ModGeneratedEnums.EventType.Pro_Rate_Cancellation_No_Longer_Applies);
        //                                    break;
        //                                }

        //                            case 34:
        //                                {
        //                                    if (this.FireEvent((int)ModGeneratedEnums.EventType.Flat_Cancellation))
        //                                        msg += "Flat Cancellation on Policy " + this.PolicyNo + " Version " + VersionNo + Environment.NewLine;

        //                                    this.ClearEvent((int)ModGeneratedEnums.EventType.Flat_Cancellation_No_Longer_Applies);
        //                                    break;
        //                                }

        //                            case 35:
        //                                {
        //                                    if (this.FireEvent((int)ModGeneratedEnums.EventType.NSF_Cancellation))
        //                                        msg += "NSF Cancellation on Policy " + this.PolicyNo + " Version " + VersionNo + Environment.NewLine;

        //                                    this.ClearEvent((int)ModGeneratedEnums.EventType.NSF_Cancellation_No_Longer_Applies);
        //                                    break;
        //                                }

        //                            default:
        //                                {
        //                                    // Policy is Good - Remove Short-Rate, Flat, NSF Locks and Alerts
        //                                    msg += this.ClearCancellationEvents();
        //                                    break;
        //                                }
        //                        }
        //                        msgTimer += "Policy Cancelled " + DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;
        //                    }
        //                }

        //                break;
        //            }
        //    }
        //    return msgTimer;
        //}

        public string ClearCancellationEvents()
        {
            // Clear Claim Cancellation events (NSF, Pro-Rate, Short-Rate, etc.)
            // Returns message to display.

            string msg = "";

            if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Short_Rate_Cancellation, (int)ModGeneratedEnums.EventType.Short_Rate_Cancellation_No_Longer_Applies))
                msg = msg + "Short Rate Cancellation No Longer Applies to Policy" + Environment.NewLine;

            if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Pro_Rate_Cancellation, (int)ModGeneratedEnums.EventType.Pro_Rate_Cancellation_No_Longer_Applies))
                msg = msg + "Pro-Rate Cancellation No Longer Applies to Policy" + Environment.NewLine;

            if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.Flat_Cancellation, (int)ModGeneratedEnums.EventType.Flat_Cancellation_No_Longer_Applies))
                msg = msg + "Flat Cancellation No Longer Applies to Policy" + Environment.NewLine;

            if (this.FireEventIfSyncExists((int)ModGeneratedEnums.EventType.NSF_Cancellation, (int)ModGeneratedEnums.EventType.NSF_Cancellation_No_Longer_Applies))
                msg = msg + "NSF Cancellation No Longer Applies to Policy" + Environment.NewLine;

            this.ClearEvent((int)ModGeneratedEnums.EventType.Short_Rate_Cancellation);
            this.ClearEvent((int)ModGeneratedEnums.EventType.Pro_Rate_Cancellation);
            this.ClearEvent((int)ModGeneratedEnums.EventType.Flat_Cancellation);
            this.ClearEvent((int)ModGeneratedEnums.EventType.NSF_Cancellation);

            return msg;
        }

        public bool FireEventIfSyncExists(int checkEventTypeId, int eventTypeId)
        {

            // Fire Claim Event if Sync exists for other event.  Return True if event was fired, False otherwise.

            if (this.SyncEventExists(checkEventTypeId))
                return this.FireEvent(eventTypeId);

            return false;
        }

        public void ClearEvent(int eventTypeId)
        {
            //DelegateInt1 bgWorker = new DelegateInt1(ClearEvent_bgWorker);
            //bgWorker.Invoke(eventTypeId);
        }

        public bool FireEvent(int eventTypeId, Hashtable parms = null/* TODO Change to default(_) if this is not a reference type */)
        {
            DelegateBool2 bgWorker = new DelegateBool2(FireEvent_bgWorker);
            bgWorker.Invoke(eventTypeId, parms);
            return true;
        }

        private bool FireEvent_bgWorker(int eventTypeId, Hashtable parms)
        {

            // Fire Event Which is Relevant to a Claim (Close Claim, Create Claim, etc.)
            // eventTypeId is the event type (Close Claim, Create Claim, etc.)

            //MedJames.MedCsX.Logic.EventType et = new MedJames.MedCsX.Logic.EventType(eventTypeId);
            //ScriptParms sp = new ScriptParms(parms);

            //sp.GenerateParms(this);

            //if (et.FireEvent(sp.Parms))
            //    return true;

            return false;
        }

        public bool SyncEventExists(int eventTypeId)
        {
            // Return True if Claim_Sync_Event row exists for event type, False otherwise

            //return db.GetBoolFromStoredProcedure("usp_Claim_Sync_Event_Exists", this.Id, eventTypeId);
            return false;
        }

        public void GetClaimGeneralInfo(int _ClaimID)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_ID", _ClaimID);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ClaimFound = true;
                    display_claim_id = reader["display_claim_id"].ToString();
                    claim_status_id = reader["claim_status_id"].ToString();
                    PolicyNumber = reader["Policy_No"].ToString();
                    Version_No = reader["Version_No"].ToString();
                    DateOfLoss = reader["date_of_loss"].ToString();
                    date_reported = reader["date_reported"].ToString();
                    NameInsured = reader["insured_person_id"].ToString();
                    InsuredVehicle = reader["insured_vehicle"].ToString();
                    LossDescription = reader["loss_description"].ToString();
                    adjuster_id = reader["adjuster_id"].ToString();
                    is_expired_policy = Convert.ToBoolean(reader["is_expired_policy"]);
                    insured_person_id = reader["insured_person_id"].ToString();
                    insured_vehicle_id = reader["insured_vehicle_id"].ToString();
                    owner_person_id = reader["owner_person_id"].ToString();
                    driver_person_id = reader["driver_person_id"].ToString();

                    where_seen = reader["where_seen"].ToString();
                    loss_location = reader["loss_location"].ToString();
                    loss_city = reader["loss_city"].ToString();
                    loss_state_id = reader["loss_state_id"].ToString();
                    authority_contacted = reader["authority_contacted"].ToString();
                    police_report_number = reader["police_report_number"].ToString();
                    reported_by = reader["reported_by"].ToString();
                    reported_by_type_id = reader["reported_by_type_id"].ToString();
                    comments = reader["comments"].ToString();
                    date_reported = reader["date_reported"].ToString();
                    accident_type_id = reader["accident_type_id"].ToString();
                    at_fault_type_id = reader["at_fault_type_id"].ToString();
                    has_coverage_issue = reader["has_coverage_issue"].ToString();
                    has_disputed_liability = reader["has_disputed_liability"].ToString();
                    permission_to_use = reader["permission_to_use"].ToString();
                    is_comp = reader["is_comp"].ToString();
                    is_collision = reader["is_collision"].ToString();
                    large_loss = reader["large_loss"].ToString();
                    hasSR22 = reader["hasSR22"].ToString();
                    is_foxpro_conversion = reader["is_foxpro_conversion"].ToString();
                    foxpro_claim_no = reader["foxpro_claim_no"].ToString();
                    vehicle_damage = reader["vehicle_damage"].ToString();
                    company_location_id = reader["company_location_id"].ToString();
                    Modified_Date = reader["Modified_Date"].ToString();
                    Created_Date = reader["Created_date"].ToString();
                    Modified_By = reader["Modified_By"].ToString();
                    Created_By = reader["Created_By"].ToString();
                    ClaimTypeId = Convert.ToInt32(reader["Created_By"]);
                    PolicyExists = true;

                }
            }
        }

        public DataTable PopulateDDLClaimPersons(int _ClaimID)
        {

            DataTable dtUserList = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_InsuredPersons_By_Claim_id", cn);
                cmd.Parameters.AddWithValue("@claim_ID", _ClaimID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtUserList);
                }

            }

            return dtUserList;
        }

        public DataTable PopulateDDLAtFaultTypes()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_AtFaultTypes", cn);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                }

            }

            return dt;
        }

        public DataTable PopulateDDLClaimVehicles(int _ClaimID)
        {

            DataTable dtUserList = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Claim_Vehicles", cn);
                cmd.Parameters.AddWithValue("@claim_ID", _ClaimID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtUserList);
                }

            }

            return dtUserList;
        }
        public DataTable PopulateDDLStates()
        {

            DataTable dtUserList = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_States", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtUserList);
                }

            }

            return dtUserList;
        }

        public DataTable PopulateDDLAccidentTypes(int _Claim_Type_Id)
        {

            DataTable dtUserList = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_AccidentTypes", cn);
                cmd.Parameters.AddWithValue("@Claim_Type_Id", _Claim_Type_Id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtUserList);
                }

            }

            return dtUserList;
        }

        public DataTable PopulateDDLReportedByTypes(int _Claim_Type_Id)
        {

            DataTable dtUserList = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_ReportedByTypes", cn);
                cmd.Parameters.AddWithValue("@Claim_Type_Id", _Claim_Type_Id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtUserList);
                }

            }

            return dtUserList;
        }
        public void GetPolicyCoverageAmounts(int _ClaimID)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsx_Get_Policy_Coverage_Amounts", cn);
                cmd.Parameters.AddWithValue("@claim_ID", _ClaimID);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ClaimFound = true;

                    switch (Convert.ToInt32(reader["Policy_Coverage_Type_Id"]))
                    {
                        case 0:
                            NoCoverage = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 1:
                            PropertyDamage = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 2:
                            AutoBodilyInjury = reader["Per_Accident_Coverage"].ToString();
                            AutoBodilyInjuryPerPerson = reader["Per_Person_Coverage"].ToString();
                            break;
                        case 3:
                            MedicalPayments = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 4:
                            UninsuredMotorist = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 5:
                            UnderinsuredMotorist = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 6:
                            Collision = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 7:
                            Comprehensive = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 8:
                            PersonalInjuryProtection = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 9:
                            PIPMedical = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 10:
                            PIPRehab = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 11:
                            PIPWages = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 12:
                            PIPEssentialServices = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 13:
                            PIPFuneral = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 14:
                            UM_UIM = reader["Per_Accident_Coverage"].ToString();
                            UM_UIMPerPerson = reader["Per_Person_Coverage"].ToString();
                            break;
                        case 15:
                            Comp_Coll = reader["Per_Accident_Coverage"].ToString();
                            break;
                        case 16:
                            Glass = reader["Per_Accident_Coverage"].ToString();
                            break;


                        default:
                            break;
                    }
                }
            }
        }

        public DataTable GetPhoneNumbersForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Populate_Phone_Numbers", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetGetPossibleDuplicateClaims(string _policyNo)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Possible_Duplicate_Claims", cn);
                cmd.Parameters.AddWithValue("@policy_no", _policyNo);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetCompanies()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Companies", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetCompanyLocations(string companyID)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Company_Locations", cn);
                cmd.Parameters.AddWithValue("@company", companyID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public void GetCompanyLocationAbbrev(int company_location_id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Company_Location_Abbreviation", cn);
                cmd.Parameters.AddWithValue("@company_location_id", company_location_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    locationAbbreviation = reader["a"].ToString();

                }

            }
        }

        public void UpdateClaimDisplayName(int claim_id, string displayName)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Update_DisplayNameForClaim", cn);
                cmd.Parameters.AddWithValue("@claim_id", claim_id);
                cmd.Parameters.AddWithValue("@displayName", displayName);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }
        }

        public DataTable GetUsersWhoCanBeAssignedClaims()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Users_Which_Can_Be_Assigned_Claims", cn);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetGroupsWhoCanBeAssignedClaims()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_User_Groups_Which_Can_Be_Assigned_Claims", cn);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public int CreateClaim(string policy_No, decimal version_no, DateTime date_of_loss, int company_location_id, bool has_coverage_issue, int claim_status_id, DateTime date_reported, int sub_claim_type_id)
        {
            int insertedvalue = 0;
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Create_claim", cn);
                cmd.Parameters.AddWithValue("@policy_No", policy_No);
                cmd.Parameters.AddWithValue("@version_no", version_no);
                cmd.Parameters.AddWithValue("@date_of_loss", date_of_loss);
                cmd.Parameters.AddWithValue("@company_location_id", company_location_id);
                cmd.Parameters.AddWithValue("@has_coverage_issue", has_coverage_issue);
                cmd.Parameters.AddWithValue("@claim_status_id", claim_status_id);
                cmd.Parameters.AddWithValue("@date_reported", date_reported);
                cmd.Parameters.AddWithValue("@sub_claim_type_id", sub_claim_type_id);
             
                cmd.CommandType = CommandType.StoredProcedure;
                insertedvalue = Convert.ToInt32(cmd.ExecuteScalar());

               
            }

           

            return insertedvalue;
        }

        public void RefreshPolicyData(int claimID)
        {
            RefreshPolicyIndividuals(claimID);
            RefreshPolicyVehicles(claimID);
            RefreshPolicyLossPayees(claimID);
            RefreshPolicyCoverages(claimID);
        }

        public void RefreshPolicyIndividuals(int claimID, string policyNo, string versionNo)
        {
            // Refresh Policy Individuals from Personal Lines.  If there is no named 
            // insured in the Pol_Individuals table, determine named insured by the 
            // Name_Insd column in the Pol_GeneralInfo table.

            string[] names;
            string firstName = "";
            string lastName = "";
            DataTable persons = null/* TODO Change to default(_) if this is not a reference type */;
            DataRow personRow = null/* TODO Change to default(_) if this is not a reference type */;

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Refresh_Policy_Individuals", cn);
                cmd.Parameters.AddWithValue("@claim_id", claimID);
                cmd.Parameters.AddWithValue("@policy_no", policyNo);
                cmd.Parameters.AddWithValue("@version_no", versionNo);
                cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }


            //if (db.GetBoolFromStoredProcedure("usp_Is_Garaging_Address", this.Id))
            //    this.FireEvent(ModGeneratedEnums.EventType.Garaging_Address_Different_from_Mailing_Address);
            //else
            //    this.ClearEvent(ModGeneratedEnums.EventType.Garaging_Address_Different_from_Mailing_Address);

            if (NamedInsuredExists(claimID))
                return;

            //    // Get Named Insured from Pol_GeneralInfo table
            //    if (this.Policy.NameInsd == null)
            //        names = "".Split(" ");
            //    else
            //        names = this.Policy.NameInsd.Split(" ");
            //    if (names.Length - 1 < 1)
            //        goto pickFirst;// there is no first and last name, so just pick the first person

            //    firstName = names[0];
            //    lastName = names[names.Length - 1];

            //    // Get Personal Lines Persons on Claim
            //    persons = db.ExecuteStoredProcedure("usp_Get_Claim_Policy_Persons", this.Id);
            //    if (persons.Rows.Count() == 0)
            //        // nothing we can do - nobody is on the policy
            //        return;

            //    // Match on First and Last Name
            //    foreach (var personRow in persons.Rows)
            //    {
            //        if (personRow.Item("First_Name").ToString.Trim.ToUpper == firstName.Trim().ToUpper() && personRow.Item("Last_Name").ToString.Trim.ToUpper == lastName.Trim().ToUpper())
            //        {
            //            Person.SetToNamedInsured(System.Convert.ToInt32(personRow.Item("Person_Id")));
            //            return; // we have matched by first and last name and set named insured for claim
            //        }
            //    }

            //    // First and Last Name Match Didn't Work - Try Matching on First Name Only
            //    foreach (var personRow in persons.Rows)
            //    {
            //        if (personRow.Item("First_Name").ToString.Trim.ToUpper == firstName.Trim().ToUpper())
            //        {
            //            Person.SetToNamedInsured(System.Convert.ToInt32(personRow.Item("Person_Id")));
            //            return; // we have matched by first name and set named insured for claim
            //        }
            //    }

            //    // Try Matching on Last Name Only
            //    foreach (var personRow in persons.Rows)
            //    {
            //        if (personRow.Item("Last_Name").ToString.Trim.ToUpper == lastName.Trim().ToUpper())
            //        {
            //            Person.SetToNamedInsured(System.Convert.ToInt32(personRow.Item("Person_Id")));
            //            return; // we have matched by last name and set named insured for claim
            //        }
            //    }

            //pickFirst:
            //    ;

            //    // There is Nothing to Match By - Pick the First Person in the List
            //    if (persons != null)
            //    {
            //        foreach (var personRow in persons.Rows)
            //        {
            //            Person.SetToNamedInsured(System.Convert.ToInt32(personRow.Item("Person_Id")));
            //            return; // we set the first person in the list as the named insured
            //        }
            //    }
        }

        public void RefreshPolicyVehicles(int claimID, string policyNo, string versionNo)
        {
           

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Refresh_Policy_Vehicles", cn);
                cmd.Parameters.AddWithValue("@claim_id", claimID);
                cmd.Parameters.AddWithValue("@policy_no", policyNo);
                cmd.Parameters.AddWithValue("@version_no", versionNo);
                cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }


        }


        public void RefreshPolicyLossPayees(int claimID, string policyNo, string versionNo)
        {


            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Refresh_Policy_Loss_Payees", cn);
                cmd.Parameters.AddWithValue("@claim_id", claimID);
                cmd.Parameters.AddWithValue("@policy_no", policyNo);
                cmd.Parameters.AddWithValue("@version_no", versionNo);
                cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }


        }


        public DataTable GetCoverage(int claim_ID)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Coverage", cn);
                cmd.Parameters.AddWithValue("@claim_id", claim_ID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }
        public void RefreshPolicyCoverages(int claimID, string verNo)
        {
            // Insert Policy Coverages from Personal Lines into Policy_Coverage table
           // _policyCoverages = new Hashtable();

            DataTable c = null/* TODO Change to default(_) if this is not a reference type */;
            DataRow d = null/* TODO Change to default(_) if this is not a reference type */;
            bool hasPipCoverage = false;

            // Delete Existing Policy Coverage rows for Claim
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Delete_Policy_Coverage", cn);
                cmd.Parameters.AddWithValue("@claim_id", claimID);
            
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }


          
            if (is_expired_policy == false)
            {
                // Only get coverage amounts if the policy is not expired.

                DataTable dt = new DataTable();
                dt = GetCoverage(claimID);
                hasPipCoverage = false;
                string cd_state = "";
                foreach (DataRow row in dt.Rows)
                {
                    cd_state = row["cd_state"].ToString();
                    string mycd_cov = System.Convert.ToString(row["cd_cov"].ToString().ToUpper().Trim());
                    switch (System.Convert.ToString(row["cd_cov"].ToString().ToUpper().Trim()))
                    {
                        case "APD":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Per_Accident_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Property_Damage.ToString());
                                    cmd.Parameters.AddWithValue("@PerAccidentCoverage", System.Convert.ToDouble(row["cd_limit1"]));
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                               
                                break;
                            }

                        case "ABI":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Per_Person_Accident_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Auto_Bodily_Injury.ToString());
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit1"]));
                                    cmd.Parameters.AddWithValue("@perAccidentCoverage", System.Convert.ToDouble(row["cd_limit2"]));
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                              
                                break;
                            }

                        case "AMP":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Per_Person_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Medical_Payments.ToString());
                                    cmd.Parameters.AddWithValue("@perAccidentCoverage", System.Convert.ToDouble(row["cd_limit1"]));
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "AUM":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Per_Person_Accident_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Uninsured_Motorist.ToString());
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit1"]));
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit2"]));
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "AUN":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Per_Person_Accident_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Underinsured_Motorist.ToString());
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit1"]));
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit2"]));
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                               
                                break;
                            }

                        case "AUUM":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Per_Person_Accident_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.UM_UIM.ToString());
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit12"]));
                                    cmd.Parameters.AddWithValue("@perPersonCoverage", System.Convert.ToDouble(row["cd_limit2"]));
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "ACOL":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Deductable_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Collision.ToString());
                                    cmd.Parameters.AddWithValue("@DeductableAmount", System.Convert.ToDouble(row["cd_limit1"]));
                                  
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "ACMP":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Deductable_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Comprehensive.ToString());
                                    cmd.Parameters.AddWithValue("@DeductableAmount", System.Convert.ToDouble(row["cd_limit1"]));

                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "APHY":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Deductable_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Comp_Coll.ToString());
                                    cmd.Parameters.AddWithValue("@DeductableAmount", System.Convert.ToDouble(row["cd_limit1"]));

                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "APIP":
                            {
                                // For PIP, insert pip coverages for state from state pip table
                                hasPipCoverage = true;
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Policy_Coverage_From_State_Pip", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@state_id", Convert.ToInt32(row["state"]));
                                    cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));

                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        case "AGLS":
                            {
                                using (var cn = new ClaimsDB().GetOpenConnection())
                                {
                                    var cmd = new SqlCommand("usp_Insert_Deductable_Coverage", cn);
                                    cmd.Parameters.AddWithValue("@claim_id", claimID);
                                    cmd.Parameters.AddWithValue("@versionNo", verNo);
                                    cmd.Parameters.AddWithValue("@policyCoverageTypeId", ModGeneratedEnums.PolicyCoverageType.Glass.ToString());
                                    cmd.Parameters.AddWithValue("@DeductableAmount", 0);

                                    cmd.CommandType = CommandType.StoredProcedure;

                                    cmd.ExecuteNonQuery();

                                }
                                
                                break;
                            }

                        default:
                            {
                                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Error - Unknown Personal Lines Coverage Type - " + System.Convert.ToString(row["cd_cov"] + "');", true);
                                
                                break;
                            }
                    }
                }

                if (hasPipCoverage == false && dt.Rows.Count > 0)
                    // Inserts Pip coverage no periods and period type, but leaves per-person and per-accident
                    // coverage amounts at zero
                    using (var cn = new ClaimsDB().GetOpenConnection())
                    {
                        var cmd = new SqlCommand("usp_Insert_No_Policy_Coverage_From_State_Pip", cn);
                        cmd.Parameters.AddWithValue("@claim_id", claimID);
                        cmd.Parameters.AddWithValue("@state_id", cd_state);
                        cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();

                    }
                
            }
        }

        public bool NamedInsuredExists(int _claimid)
        {
            int result = 0;


            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Named_Insured_Exists", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claimid);
                cmd.CommandType = CommandType.StoredProcedure;

                result = (int)cmd.ExecuteScalar(); 
               


            }

            if (result == 0)
                return false;
            else
                return true;
        }


        //public int CreateClaimWithPolicy(string policy_No, decimal version_no, DateTime date_of_loss, int company_location_id, bool has_coverage_issue, int claim_status_id, DateTime date_reported, int sub_claim_type_id)
        //{
        //    int insertedvalue = 0;
        //    using (var cn = new ClaimsDB().GetOpenConnection())
        //    {
        //        var cmd = new SqlCommand("usp_MedCsX_Create_claim", cn);
        //        cmd.Parameters.AddWithValue("@policy_No", policy_No);
        //        cmd.Parameters.AddWithValue("@version_no", version_no);
        //        cmd.Parameters.AddWithValue("@date_of_loss", date_of_loss);
        //        cmd.Parameters.AddWithValue("@company_location_id", company_location_id);
        //        cmd.Parameters.AddWithValue("@has_coverage_issue", has_coverage_issue);
        //        cmd.Parameters.AddWithValue("@claim_status_id", claim_status_id);
        //        cmd.Parameters.AddWithValue("@date_reported", date_reported);
        //        cmd.Parameters.AddWithValue("@sub_claim_type_id", sub_claim_type_id);

        //        cmd.CommandType = CommandType.StoredProcedure;
        //        insertedvalue = Convert.ToInt32(cmd.ExecuteScalar());


        //    }

        //    return insertedvalue;
        //}

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Injured
    {

        public int AttorneyVendorID { get; set; }
        public string ExtentOfInjury { get; set; }
        public int InjuredId { get; set; }
        [System.ComponentModel.Description("Cause of the injury.")]
        public int InjuryCauseId { get; set; }
        [System.ComponentModel.Description("Type of injury.")]
        public int InjuryTypeId { get; set; }
        public double MedicalLeinAmount { get; set; }
        public int PersonId { get; set; }
        public int Injured_Id { get; set; }
        public string Home_Phone { get; set; }
        public string Work_Phone { get; set; }
        public string Cell_Phone { get; set; }
        public string Other_Contact_Phone { get; set; }
        public string Fax { get; set; }


        public void GetInjuredByID(string _injured_id)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_medCsx_GetInjured_By_ID", cn);
                cmd.Parameters.AddWithValue("@injured_id", _injured_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Injured_Id = Convert.ToInt32(reader["Injured_Id"]);
                    Home_Phone = reader["Home_Phone"].ToString();
                    Work_Phone = reader["Work_Phone"].ToString();
                    Cell_Phone = reader["Cell_Phone"].ToString();
                    Other_Contact_Phone = reader["Other_Contact_Phone"].ToString();
                    Fax = reader["Fax"].ToString();
                }
            }
        }

        public DataTable GetInjuredForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                // var cmd = new SqlCommand("usp_Get_Injured", cn);
                var cmd = new SqlCommand("Get_Claim_Claimant_Persons2", cn); 
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetInjuredReservesForClaim(int _claim_id, int _reserve_type_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Allowable_Injured_Coverages", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                //cmd.Parameters.AddWithValue("@reserve_type_id", _reserve_type_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetInjuredLiensForClaim(int _injured_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Injured_Liens", cn);
                cmd.Parameters.AddWithValue("@injured_id", _injured_id);
              
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            return dt;
        }


    }
}
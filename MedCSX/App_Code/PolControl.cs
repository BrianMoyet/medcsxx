﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCSX.App_Code
{
    public class PolControl : PersonalLinesTable
    {
        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public PolControl(string policyNumber) : base(policyNumber) { }
        public PolControl(string policyNumber, string versionNumber)
            : base(policyNumber, versionNumber)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Pol_Control", policyNumber);
            if (rows.Rows.Count > 0)
                row = db.DataTableRowToHashtable(rows, rows.Rows[0]);
            else
                row = new Hashtable();
        }

        public PolControl(Hashtable row) : base(row) { }
        #endregion

        #region Properties
        public override string TableName
        {
            get { return "PersonalLines.dbo.Pol_Control"; }
        }

        public string InsName
        {
            get { return (string)row["InsName"]; }
        }

        public string CdWorkInprog
        {
            get { return (string)row["Cd_WorkInprog"]; }
        }
        #endregion
    }
}
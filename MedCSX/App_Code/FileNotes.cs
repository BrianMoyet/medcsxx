﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class FileNotes
    {

        public string claim_id { get; set; }
        public string Claimant_Id { get; set; }
        public string Reserve_Id { get; set; }
        public string Transaction_Id { get; set; }
        public string User_Id { get; set; }
        public string File_Note_Text { get; set; }
        public string File_Note_Type_Id { get; set; }
        public string Event_Type_Id { get; set; }
        public string Diary_Type_Id { get; set; }
        public string Document_Id { get; set; }
        public string Authority_Needed_To_Unlock { get; set; }
        public string Is_Summary { get; set; }
        public string Created_By { get; set; }
        public string Modified_By { get; set; }
        public string Created_Date { get; set; }
        public string Modified_Date { get; set; }
        public string Created_By_Name { get; set; }
        public string Modified_By_Name { get; set; }
        public string Assigned_To { get; set; }
        public string ReserveDescription { get; set; }


        


        public DataTable GetFileNotes(int _claim_id, int _file_Note_type_id, int _claimant_id, int _reserve_id, int _user_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                // var cmd = new SqlCommand("usp_Get_Injured", cn);
                var cmd = new SqlCommand("usp_Get_File_Notes_With_Images", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.Parameters.AddWithValue("@file_Note_type_id", _file_Note_type_id);
                cmd.Parameters.AddWithValue("@claimant_id", _claimant_id);
                cmd.Parameters.AddWithValue("@reserve_id", _reserve_id);
                cmd.Parameters.AddWithValue("@user_id", _user_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public void GetFileNoteByID(int _id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_FileNote_By_ID", cn);
                cmd.Parameters.AddWithValue("@file_note_id", _id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    claim_id = reader["claim_id"].ToString();
                    Claimant_Id = reader["claimant_id"].ToString();
                    Reserve_Id = reader["Reserve_Id"].ToString();
                    Transaction_Id = reader["Transaction_Id"].ToString();
                    User_Id = reader["User_Id"].ToString();
                    File_Note_Text = reader["File_Note_Text"].ToString();
                    File_Note_Type_Id = reader["File_Note_Type_Id"].ToString();
                    Event_Type_Id = reader["Event_Type_Id"].ToString();
                    Diary_Type_Id = reader["Diary_Type_Id"].ToString();
                    Document_Id = reader["Document_Id"].ToString();
                    Authority_Needed_To_Unlock = reader["Authority_Needed_To_Unlock"].ToString();
                    Is_Summary = reader["Is_Summary"].ToString();
                    Created_By = reader["Created_By"].ToString();
                    Modified_By = reader["Modified_By"].ToString();
                    Created_Date = reader["Created_Date"].ToString();
                    Modified_Date = reader["Modified_Date"].ToString();
                    Created_By_Name = reader["CreatedByName"].ToString();
                    Modified_By_Name = reader["ModifiedByName"].ToString();
                    Assigned_To = reader["AssignedTo"].ToString();
                    ReserveDescription = reader["ReserveDescription"].ToString();
                }
            }
        }
        public DataTable GetFileNoteUserTypes()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                // var cmd = new SqlCommand("usp_Get_Injured", cn);
                var cmd = new SqlCommand("usp_Get_User_File_Note_Types", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public int CreateFileNote()
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Insert_FileNote", cn);
                cmd.Parameters.AddWithValue("@Claim_ID", claim_id);
                cmd.Parameters.AddWithValue("Claimant_Id", Claimant_Id);
                cmd.Parameters.AddWithValue("Reserve_Id", Reserve_Id);
                cmd.Parameters.AddWithValue("Transaction_Id", Transaction_Id);
                cmd.Parameters.AddWithValue("User_Id", User_Id);
                cmd.Parameters.AddWithValue("File_Note_Text", File_Note_Text);
                cmd.Parameters.AddWithValue("File_Note_Type_Id", File_Note_Type_Id);
                cmd.Parameters.AddWithValue("Event_Type_Id", Event_Type_Id);
                cmd.Parameters.AddWithValue("Diary_Type_Id", Diary_Type_Id);
                cmd.Parameters.AddWithValue("Document_Id", Document_Id);
                cmd.Parameters.AddWithValue("Authority_Needed_To_Unlock", Authority_Needed_To_Unlock);
                cmd.Parameters.AddWithValue("Is_Summary", Is_Summary);
                cmd.Parameters.AddWithValue("Created_By", Created_By);
                cmd.Parameters.AddWithValue("Modified_By", Modified_By);
                cmd.Parameters.AddWithValue("Created_Date", Created_Date);
                cmd.Parameters.AddWithValue("Modified_Date", Modified_Date);
                


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();


            }
            return 1;
        }
    }
}
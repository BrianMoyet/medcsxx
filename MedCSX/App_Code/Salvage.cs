﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Salvage
    {
        public DataTable GetSalvage(int _userID)
        {

            DataTable dtSalvageTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Salvage_For_User_Id", cn);
                cmd.Parameters.AddWithValue("@userid", _userID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtSalvageTable);

                }

            }

            return dtSalvageTable;
        }

        public DataTable GetSalvageForClaim(int _claim_id)
        {

            DataTable dtSalvageTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Salvage_For_Claim_Id", cn);
                cmd.Parameters.AddWithValue("@claimid", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtSalvageTable);

                }

            }

            return dtSalvageTable;
        }
    }
}
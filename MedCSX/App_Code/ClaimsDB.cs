﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MedCSX.App_Code
{
    public class ClaimsDB
    {
        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ClaimsConnectionString"].ConnectionString;
        }

        public SqlConnection GetOpenConnection()
        {
            SqlConnection conn = new SqlConnection(GetConnectionString());
            conn.Open();
            return conn;
        }

        public string SQLEncode(string sqlString)
        {
            return sqlString.Replace("'", "''");
        }
    }
}
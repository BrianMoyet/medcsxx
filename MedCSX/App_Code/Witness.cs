﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Witness
    {
        public string witness_passenger_id { get; set; }
        public string person_id { get; set; }
        public string witness_passenger_location { get; set; }
        public string witness_passenger_type_id { get; set; }

        public DataTable GetWitnessForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Witness_Passengers_With_Images", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetWitnessPassengerTypes()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_GetWitnessPassengerTypes", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public void GetWitnessInfoByID(int _id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Witness_Passenger", cn);
                cmd.Parameters.AddWithValue("@person_id", _id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    witness_passenger_id = reader["witness_passenger_id"].ToString();
                    witness_passenger_type_id = reader["witness_passenger_type_id"].ToString();
                    person_id = reader["person_id"].ToString();
                    witness_passenger_location = reader["witness_passenger_location"].ToString();
                   





                }
            }
        }

    }
}
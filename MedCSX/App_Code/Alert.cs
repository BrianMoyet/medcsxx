﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Alert
    {
        public DataTable GetAlerts(int _userID, int _alertDirection)
        {

            DataTable dtAlertTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand();
                if (_alertDirection == 1)  // to me
                { 
                    cmd = new SqlCommand("Get_My_Alerts_After2", cn);
                    cmd.Parameters.AddWithValue("@user_id", _userID);
                    cmd.Parameters.AddWithValue("@alert_start_date", "1/1/2019");
                }
                else  // from me
                {
                    cmd = new SqlCommand("Get_My_Alerts_Sent2", cn);
                    cmd.Parameters.AddWithValue("@user_id", _userID);
                }
                
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtAlertTable);

                }

            }

            return dtAlertTable;
        }

        public DataTable GetAlertsForClaim(int _claim_id)
        {

            DataTable dtAlertTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand();
                
                cmd = new SqlCommand("usp_Get_Alerts_For_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtAlertTable);

                }

            }

            return dtAlertTable;
        }

    }
}
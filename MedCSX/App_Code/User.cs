﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MedCSX.App_Code
{
    public enum Task_Allowed : int
    {
        Edit_Users,
        Close_Claim,
        Edit_Type_Tables,
        Edit_Vendors,
        Edit_User_Groups,
        Configure_Alerts_Diary_File_Notes,
        Edit_Global_Settings,
        Open_Error_Log,
        Edit_Data_Dictionary,
        Vendor_Type_Sub_Type_Associations,
        Logged_In_Users,
        Kill_Users,
        Event_Viewer,
        Add_Reserve,
        Change_Loss_Reserve,
        Change_Expense_Reserve,
        Reopen_Reserve,
        Close_Reserve,
        Close_Pending_Salvage,
        Close_Pending_Subro,
        Issue_Draft,
        Receive_Subro,
        Receive_Salvage,
        Void_Draft,
        Void_Subro,
        Void_Salvage,
        Modify_Claim_Data,
        Void_Reserve,
        Reissue_Draft,
        Reissue_Subro,
        Reissue_Salvage,
        Reopen_Claim,
        Change_Vendor_Tax_Id_Required,
        Send_Message,
        Staffing_Metrics,
        Recovery_Amounts_Report,
        Edit_Transaction_Type_Conversion,
        Print_Foxpro_Drafts,
        Accounting_Reports,
        Manually_Lock_Claim,
        Set_Average_Reserves,
        Set_Type_Table_Icons,
        SQL_Logging,
        Bulk_Assign_Claims,
        Unvoid_Draft,
        Audit_Claims,
        Log_In_As,
        Resend_Appraisal_Requests,
        Mark_Appraisals_as_Completed,
        Cancel_Appraisal,
        Change_Document_Status,
        Create_New_Document,
        Open_Document,
        Print_Draft_Queue,
        View_Appraiser_Data,
        Wire_Transfer,
        Pending_Drafts,
        Resend_Appraisal_Emails
    }

    internal class User
    {
        public string User_Id { get; set; }
        public string Logon_Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Password { get; set; }
        public string Phone_Ext { get; set; }
        public string Email { get; set; }
        public string Direct_Fax { get; set; }


        public void GetUserByID(int _id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_User_By_Id", cn);
                cmd.Parameters.AddWithValue("@user_id", _id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    User_Id = reader["User_Id"].ToString();
                    Logon_Id = reader["Logon_Id"].ToString();
                    First_Name = reader["First_Name"].ToString();
                    Last_Name = reader["Last_Name"].ToString();
                    Password = reader["Password"].ToString();
                    Phone_Ext = reader["Phone_Ext"].ToString();
                    Email = reader["Email"].ToString();
                    Direct_Fax = reader["Direct_Fax"].ToString();
                }
            }
        }



        public DataTable GetActiveUsers()
        {

            DataTable dtUserList = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Active_Users", cn);
                
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtUserList);
                }

            }

            return dtUserList;
        }



        public bool IsValid(string _username, string _pass)
        {
            // using (var cn = new SqlConnection(@"Data Source=DBX;Initial Catalog=BlockRESMain;Integrated Security=False;User ID=BRESMainWrite;Password=X33Nwqlo#"))
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_ValidateLogin", cn);
                cmd.Parameters.AddWithValue("@logon_id", _username);
                cmd.Parameters.AddWithValue("@password", _pass);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    System.Web.HttpContext.Current.Session["user_id"] = Convert.ToInt32(reader["user_id"]);
                    System.Web.HttpContext.Current.Session["FullName"] = reader["first_name"].ToString() + " " + reader["last_name"].ToString();

                    using (var cn2 = new ClaimsDB().GetOpenConnection())
                    {
                        var cmd2 = new SqlCommand("usp_Logon_MedCsX_User", cn2);
                        cmd2.Parameters.AddWithValue("@user_id", Convert.ToInt32(reader["user_id"]));
                        cmd2.Parameters.AddWithValue("@mac_address", "");
                        cmd2.Parameters.AddWithValue("@version", "20.0.0");
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.ExecuteNonQuery();
                    }

                    reader.Dispose();
                    cmd.Dispose();
                    return true;
                }
                else
                {
                    reader.Dispose();
                    cmd.Dispose();
                    return false;
                }


            }


        }
    }

    public class FlagsHelper
    {
        public static bool IsSet<T>(T flags, T flag)
        {
            long flagsValue = Convert.ToInt64(flags);
            long flagValue = Convert.ToInt64(Math.Pow(2, Convert.ToInt64(flag)));

            return (flagsValue & flagValue) != 0;
        }

        public static void SetTo<T>(ref T flags, T flag)
        {
            long flagsValue = Convert.ToInt64(flags);
            long flagValue = Convert.ToInt64(Math.Pow(2, Convert.ToInt64(flag)));

            flags = (T)(object)(flagsValue | flagValue);
        }

        //public static void UnSet<T>(ref T flags, T flag)
        //{
        //    long flagsValue = Convert.ToInt64(flags);
        //    long flagValue = Convert.ToInt64(Math.Pow(2, Convert.ToInt64(flag)));

        //    flags = (T)(object)(flagsValue & !flagValue);
        //}
    }

}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace MedCSX.App_Code
{
    static class HelperFunctions
    {
        public static string GetLanguageByID(int id)
        {
           
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Languages_For_User", cn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            string returnString = dt.Rows[0][0].ToString();
            return returnString;
        }

        public static string GetReserveCoverageDescriptionByID(int claim_id, int policy_coverage_type_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Coverage_Description", cn);
                cmd.Parameters.AddWithValue("@claim_id", claim_id);
                cmd.Parameters.AddWithValue("@policy_coverage_type_id", policy_coverage_type_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            string returnString = dt.Rows[0][0].ToString();
            return returnString;
        }

        public static string GetCurrentDate()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Current_Date", cn);
               
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            string returnString = dt.Rows[0][0].ToString();
            return returnString;
        }

        public static DataTable getTypeTableRows(string tableName, string orderByClaus, string whereClause = "1 = 1")
        {
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_TypeTable_Rows", cn);
                cmd.Parameters.AddWithValue("@tableName", tableName);
                cmd.Parameters.AddWithValue("@whereClause", whereClause);
                cmd.Parameters.AddWithValue("@orderByClaus", orderByClaus);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public static DataTable VendorsOfType(int VendorSubTypeId)
        {
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Vendors1", cn);
                cmd.Parameters.AddWithValue("@vendor_sub_type_id", VendorSubTypeId);
              
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public static DataTable getTableRow(string tableName, string whereClause)
        {
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Row", cn);
                cmd.Parameters.AddWithValue("@tableName", tableName);
                cmd.Parameters.AddWithValue("@whereClause", whereClause);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public static string getCompanyIDForPolicy(string policyNo)
        {
            DataTable dt = new DataTable();
            String state = "";

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Company_For_Policy", cn);
                cmd.Parameters.AddWithValue("@policy", policyNo);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            state = dt.Rows[0][1].ToString();
            return state;
        }

        public static bool PoliceReportOrdered(int claim_id)
        {
            DataTable dt = new DataTable();
            int result = 0;

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_No_ChoicePoint_Requests_For_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_id", claim_id);
                cmd.CommandType = CommandType.StoredProcedure;
                result = (int)cmd.ExecuteScalar();



            }

            if (result == 0)
                return false;
            else
                return true;
        }

        //public static bool GetBoolFromStoredProcedure(string spName, object parm1 = null,
        //          object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
        //          object parm6 = null)
        //{
        //    object Value = ExecuteStoredProcedureValuesOnly(spName, parm1, parm2, parm3, parm4, parm5, parm6);
        //    if (Value == null)
        //    {
        //        Value = 0;
        //    }

        //    int parse = 0;
        //    if (int.TryParse(Value.ToString().Replace("$", ""), out parse))
        //    {
        //        if ((int)Value == 0)
        //        {
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    else
        //    {
        //        switch (Value.ToString().ToUpper())
        //        {
        //            case "TRUE":
        //            case "YES":
        //            case "1":
        //                return true;
        //            default:
        //                return false;
        //        }
        //    }
        //}

        //static object ExecuteStoredProcedureValuesOnly(string spName, object parm1 = null,
        //         object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
        //         object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
        //         object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
        //         object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
        //         object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
        //         object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
        //         object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
        //         object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
        //         object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
        //         object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
        //{
        //    string parms = MakeParmList(parm1, parm2, parm3, parm4, parm5, parm6, parm7,
        //        parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15, parm16, parm17,
        //        parm18, parm19, parm20, parm21, parm22, parm23, parm24, parm25, parm26, parm27,
        //        parm28, parm29, parm30, parm31, parm32, parm33, parm34, parm35, parm36, parm37,
        //        parm38, parm39, parm40, parm41);
        //    string sql = "exec " + spName + parms;
        //    return ExecuteSelectReturnValue(sql);
        //}

        //static string MakeParmList(object parm1 = null,
        //   object parm2 = null, object parm3 = null, object parm4 = null, object parm5 = null,
        //   object parm6 = null, object parm7 = null, object parm8 = null, object parm9 = null,
        //   object parm10 = null, object parm11 = null, object parm12 = null, object parm13 = null,
        //   object parm14 = null, object parm15 = null, object parm16 = null, object parm17 = null,
        //   object parm18 = null, object parm19 = null, object parm20 = null, object parm21 = null,
        //   object parm22 = null, object parm23 = null, object parm24 = null, object parm25 = null,
        //   object parm26 = null, object parm27 = null, object parm28 = null, object parm29 = null,
        //   object parm30 = null, object parm31 = null, object parm32 = null, object parm33 = null,
        //   object parm34 = null, object parm35 = null, object parm36 = null, object parm37 = null,
        //   object parm38 = null, object parm39 = null, object parm40 = null, object parm41 = null)
        //{
        //    string parms = "";
        //    string parmName = "";
        //    object parmValue;

        //    CreateParameter(parm1, ref parms);
        //    CreateParameter(parm2, ref parms);
        //    CreateParameter(parm3, ref parms);
        //    CreateParameter(parm4, ref parms);
        //    CreateParameter(parm5, ref parms);
        //    CreateParameter(parm6, ref parms);
        //    CreateParameter(parm7, ref parms);
        //    CreateParameter(parm8, ref parms);
        //    CreateParameter(parm9, ref parms);
        //    CreateParameter(parm10, ref parms);
        //    CreateParameter(parm11, ref parms);
        //    CreateParameter(parm12, ref parms);
        //    this.CreateParameter(parm13, ref parms);
        //    this.CreateParameter(parm14, ref parms);
        //    this.CreateParameter(parm15, ref parms);
        //    this.CreateParameter(parm16, ref parms);
        //    this.CreateParameter(parm17, ref parms);
        //    this.CreateParameter(parm18, ref parms);
        //    this.CreateParameter(parm19, ref parms);
        //    this.CreateParameter(parm20, ref parms);
        //    this.CreateParameter(parm21, ref parms);
        //    this.CreateParameter(parm22, ref parms);
        //    this.CreateParameter(parm23, ref parms);
        //    this.CreateParameter(parm24, ref parms);
        //    this.CreateParameter(parm25, ref parms);
        //    this.CreateParameter(parm26, ref parms);
        //    this.CreateParameter(parm27, ref parms);
        //    this.CreateParameter(parm28, ref parms);
        //    this.CreateParameter(parm29, ref parms);
        //    this.CreateParameter(parm30, ref parms);
        //    this.CreateParameter(parm31, ref parms);
        //    this.CreateParameter(parm32, ref parms);
        //    this.CreateParameter(parm33, ref parms);
        //    this.CreateParameter(parm34, ref parms);
        //    this.CreateParameter(parm35, ref parms);
        //    this.CreateParameter(parm36, ref parms);
        //    this.CreateParameter(parm37, ref parms);
        //    this.CreateParameter(parm38, ref parms);
        //    this.CreateParameter(parm39, ref parms);
        //    this.CreateParameter(parm40, ref parms);
        //    this.CreateParameter(parm41, ref parms);

        //    return " " + parms.Trim(',');
        //}

        //static void CreateParameter(object Parameter, ref string Parameters)
        //{
        //    if (Parameter != null)
        //    {
        //        if ((Parameter.GetType() == typeof(DateTime)) || (Parameter.GetType() == typeof(string)))
        //        {
        //            if ((Parameter.GetType() == typeof(string)) && (Parameter.ToString().Length > 2) &&
        //                (((Parameter.ToString().Substring(0, 1) == "'") && (Parameter.ToString().Substring(Parameter.ToString().Length - 1, 1) == "'")) ||
        //                ((Parameter.ToString().Substring(0, 1) == "[") && (Parameter.ToString().Substring(Parameter.ToString().Length - 1, 1) == "]"))))
        //            {
        //                Parameters += ", " + Parameter.ToString();
        //            }
        //            else
        //            {
        //                Parameters += ", " + "'" + Parameter.ToString().Replace("'", "") + "'";
        //            }
        //        }
        //        else if (Parameter.GetType() == typeof(bool))
        //        {
        //            if ((bool)Parameter)
        //            {
        //                Parameters += ", " + "1";
        //            }
        //            else
        //            {
        //                Parameters += ", " + "0";
        //            }
        //        }
        //        else
        //        {
        //            Parameters += ", " + Parameter;
        //        }
        //    }
        //}

        //static object ExecuteSelectReturnValue(string sql)
        //{
        //    //Executes select statement and returns value
        //    DateTime startTime = DateTime.Now;
        //    object Value = db.ExecSQLReturnValue(sql);
        //    Log(sql, "", DateTime.Now.Subtract(startTime));
        //    return Value;
        //}

        //const string LOG_FILE_NAME = "sql.log";
        //static bool m_SqlLogging = false;

        //static bool SqlLogging
        //{
        //    get { return m_SqlLogging; }
        //    set { m_SqlLogging = value; }
        //}
        //static void Log(string sql, string parms, TimeSpan elapsedTime)
        //{
        //    //Writes to log
        //    //TODO hardcoded loggin variable
        //    SqlLogging = false;
        //    //Displays the query executed in the debug window
        //    System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("T") + " " + sql);

        //    if (!SqlLogging)
        //    {
        //        return;
        //    }

        //    StreamWriter sw = new StreamWriter(LOG_FILE_NAME, true);
        //    string s = DateTime.Now.ToLongTimeString() + "\t" + elapsedTime.TotalMilliseconds.ToString() + "\t" + sql;

        //    if ((parms != null) && (parms.Length > 1))
        //    {
        //        string[] parameters = parms.Split(',');
        //        s += " " + parameters[0];
        //        for (int i = 1; i < parameters.Length; i++)
        //        {
        //            s += ", " + parameters[i];
        //        }
        //    }

        //    sw.WriteLine(s);
        //    sw.Close();
        //}

        public static string getStateByID(string id)
        {
            DataTable dt = new DataTable();
            String state = "";

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("Get_State2", cn);
                cmd.Parameters.AddWithValue("@state_id", id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            state = dt.Rows[0][0].ToString();
            return state;
        }

        public static string FormatKNumber(Decimal num)
        {

            if (num > 0)
            {
                if ((int)num >= 100000)
                    return FormatKNumber(num / 1000) + "K";
                if ((int)num >= 10000)
                {
                    return ((int)num / 1000D).ToString("0.#") + "K";
                }


                return num.ToString("#,0");
            }
            else
            {
                return "None";
            }
        }
        
        public static string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length) + "...";
            }
            return source;
        }

        public static string GetDescription(string table, string id)
        {
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Description", cn);
                cmd.Parameters.AddWithValue("@tableName", table);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            string description = dt.Rows[0][0].ToString();
            return description;
        }

        public static string GetPersonName(string id)
        {
            DataTable dt = new DataTable();
            String personname = "";

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("Get_Person_Name_For_Id", cn);
                cmd.Parameters.AddWithValue("@person_id", id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            personname = dt.Rows[0][0].ToString();
            return personname;
        }

        public static string GetAgentForClaim(string id)
        {
            DataTable dt = new DataTable();
            String personname = "";

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("[usp_Get_Agent_For_Claim]", cn);
                cmd.Parameters.AddWithValue("@claim_id", id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            personname = dt.Rows[0][1].ToString() + System.Environment.NewLine;
            personname += dt.Rows[0][3].ToString() + System.Environment.NewLine;
            personname += dt.Rows[0][5].ToString() + ", " + dt.Rows[0][6].ToString() + ", " + dt.Rows[0][7].ToString();
            personname += "Phone: " + dt.Rows[0][8].ToString() + System.Environment.NewLine;
            
           // personname += dt.Rows[0][12].ToString() + System.Environment.NewLine;
            return personname;
        }
      

        public static string GetAdjusterInfo(string user_id)
        {
            string returnString = "";
            User myUser = new User();
            myUser.GetUserByID(Convert.ToInt32(user_id));

            returnString += myUser.First_Name + " " + myUser.Last_Name + System.Environment.NewLine;
            returnString += "Ph.  " + myUser.Phone_Ext + System.Environment.NewLine;
            returnString += "Fax.  " + myUser.Direct_Fax + System.Environment.NewLine;
            returnString += "Email  " + myUser.Email + System.Environment.NewLine;

            return returnString;
        }
        public static string ExtendedDetails(string id)
        {

            string display = "";

            Claimants myData = new Claimants();
            myData.GetPersonByID(Convert.ToInt32(id));
            display += myData.salutation + " " + myData.first_name + " " + myData.middle_initial + " " + myData.last_name + " " + myData.suffix;
            if ((myData.address1 != "") || (myData.city != "") || (myData.state != "") || (myData.zipcode != ""))
            {
                display += "<br />" + myData.address1 + " " + myData.address2;
                display += "<br />" + myData.city + ", " + myData.state + " " + myData.zipcode;
            }
            if ((myData.home_phone != "") && (CountDigits(myData.home_phone) > 0))
                display += "<br />" + "Home Phone: " + myData.home_phone;
            if ((myData.work_phone != "") && (CountDigits(myData.work_phone) > 0))
                display += "<br />" + "Work Phone: " + myData.work_phone;
            if ((myData.cell_phone != "") && (CountDigits(myData.cell_phone) > 0))
                display += "<br />" + "Cell Phone: " + myData.cell_phone;
            if ((myData.other_contact_phone != "") && (CountDigits(myData.other_contact_phone) > 0))
                display += "<br />" + "Other Contact Phone: " + myData.other_contact_phone;
            if ((myData.fax != "") && (CountDigits(myData.fax) > 0))
                display += "<br />" + "Fax: " + myData.fax;
            if ((myData.email != "") && (myData.email.Length > 0))
                display += "<br />" + "Email: " + myData.email;
            if ((myData.date_of_birth != "1/1/1900 12:00:00 AM") && (myData.date_of_birth.Length > 0))
                display += "<br />" + "Date of Birth: " + myData.date_of_birth;
            if (myData.ssn != "")
                display += "<br />" + "SSN: " + myData.ssn;
            if (myData.drivers_license_no != "")
                display += "<br />" + "Drivers License No: " + myData.drivers_license_no;
            if (myData.sex != "")
                display += "<br />" + "Sex: " + myData.sex;
            if (myData.employer != "")
                display += "<br />" + "Employer: " + myData.employer;
            if (myData.language_id != "")
                display += "<br />" + "Employer: " + HelperFunctions.GetLanguageByID(Convert.ToInt32(myData.language_id));

            return display;
        }

        public static int CountDigits(string s)
        {
            int i;
            int count;

            count = 0;
            for (i = 0; i <= s.Length - 1; i++)
            {
                if (IsNumeric(s.Substring(i, 1)))
                    count = count + 1;
            }
            return count;
        }

        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

    }
}
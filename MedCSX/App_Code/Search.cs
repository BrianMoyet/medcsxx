﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Search
    {
        public DataTable SearchClaimByClaimNo(string _searchString, int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Claim_No", cn);
                cmd.Parameters.AddWithValue("@claim_no", _searchString);
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByPhoneNo(string _searchString, int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_PhoneNumber", cn);
                cmd.Parameters.AddWithValue("@phoneNumber", _searchString);
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByDateOfLoss(string _searchString, int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Date_Of_Loss", cn);
                cmd.Parameters.AddWithValue("@date_of_loss", Convert.ToDateTime(_searchString));
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByPolicyNo(string _searchString, int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Policy_No", cn);
                cmd.Parameters.AddWithValue("@policy_no", _searchString);
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByAllPending(int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_All_Pending", cn);
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByDraftNo(string _searchString, int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Draft_No", cn);
                cmd.Parameters.AddWithValue("@draft_no", _searchString);
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByAllLockedPending(int _subClaimTypeId = 0)
        {
            int? subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_All_Locked_Pending", cn);
                cmd.Parameters.AddWithValue("@subClaimTypeId", subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByRecentPending(int _days, int _subClaimTypeId = 0)
        {
            int? _subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Date", cn);
                cmd.Parameters.AddWithValue("@no_days", _days);
                cmd.Parameters.AddWithValue("@subClaimTypeId", _subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByName(string _firstName, string _lastName, int _subClaimTypeId = 0)
        {
            int? _subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Name", cn);
                cmd.Parameters.AddWithValue("@first_name", _firstName);
                cmd.Parameters.AddWithValue("@last_name", _lastName);
                cmd.Parameters.AddWithValue("@subClaimTypeId", _subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }

        public DataTable SearchClaimByVehicle(string _year, string _make, string _model, string _vin, int _subClaimTypeId = 0 )
        {
            int? _subClaimType = null;
            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Search_Claims_By_Vehicle", cn);
                cmd.Parameters.AddWithValue("@year", _year);
                cmd.Parameters.AddWithValue("@make", _make);
                cmd.Parameters.AddWithValue("@model", _model);
                cmd.Parameters.AddWithValue("@vin", _vin);
                cmd.Parameters.AddWithValue("@subClaimTypeId", _subClaimType);

                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }
            }

            return dt;
        }
    }
  
}
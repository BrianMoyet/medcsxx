﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Net;
using MedJames.ServiceWrappers.ReportWrapper.Internal;
using MedJames.ServiceWrappers.ReportWrapper.Model;

namespace MedCSX.App_Code
{
    public class ReportService
    {
        private MedJames.ServiceWrappers.ReportWrapper.Services.IReportService _wrappedreportingService;

        public ReportService(NetworkCredential iCredentials, string endPointAddress, string executionEndPointAddress)
        {
            ReportDataSource rds = new ReportDataSource(iCredentials, endPointAddress);
            ReportExecutionDataSource rdes = new ReportExecutionDataSource(iCredentials, executionEndPointAddress);
            _wrappedreportingService = new MedJames.ServiceWrappers.ReportWrapper.Services.Impl.ReportService(rds, rdes);
        }

        public byte[] GetByFormat(ReportDocument report, IDictionary<string, string> parms)
        {
            Report wrapperReport = new Report();
            wrapperReport.Name = report.Name;
            wrapperReport.Path = report.Path;
            wrapperReport.ContentType = ContentType.Pdf;
            wrapperReport.Parameters = parms.Select(x => new Parameter() { Name = x.Key, Value = x.Value }).ToList();
           

            try
            {
                return _wrappedreportingService.Render(wrapperReport);
            }
            catch (Exception ex)
            {
                return _wrappedreportingService.Render(wrapperReport);
            }
        }

       

        private Dictionary<string, string> GetReportParmsDepartment()
        {
            Dictionary<string, string> newparms = new Dictionary<string, string>();


            newparms.Add("user_id", "6");


            return newparms;
        }



    }
}
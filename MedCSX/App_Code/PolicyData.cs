﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class PolicyData
    {

        public DateTime dt_Exp {get; set;}


        public DataTable GetPolicyDataForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Policy_Data", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

       

        public DataTable FindPolicies(bool findActivePolicies, string dateOfLoss, string policyNumber
            , string firstName, string lastName, string SSN, string autoYear, string make, string model
            , string VIN, string address, string city, string state, string zip, ArrayList excludedPolicies = null)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_FindPolicies", cn);
                cmd.Parameters.AddWithValue("@findActivePolicies", findActivePolicies);
                cmd.Parameters.AddWithValue("@dateOfLoss", dateOfLoss);
                cmd.Parameters.AddWithValue("@policyNumber", policyNumber);
                //cmd.Parameters.AddWithValue("@firstName", firstName);
                //cmd.Parameters.AddWithValue("@lastName", lastName);
                //cmd.Parameters.AddWithValue("@SSN", SSN);
                //cmd.Parameters.AddWithValue("@autoYear", autoYear);
                //cmd.Parameters.AddWithValue("@make", make);
                //cmd.Parameters.AddWithValue("@model", model);
                //cmd.Parameters.AddWithValue("@VIN", VIN);
                //cmd.Parameters.AddWithValue("@address",address);
                //cmd.Parameters.AddWithValue("@city", city);
                //cmd.Parameters.AddWithValue("@state", state);
                //cmd.Parameters.AddWithValue("@zip", zip);



                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            //if (excludedPolicies != null)
            //{
            //    if ((excludedPolicies.Count == 0) && (Policies.Count == 0))
            //    {
            //        DateTime dtOfLoss = DateTime.Parse(dateOfLoss);
            //        DateTime newDOL = dtOfLoss.AddDays(7);
            //        dt = dbConnection.ExecuteStoredProcedureReturnHashList("usp_FindPolicies", true,
            //                 dateOfLoss, policyNumber, firstName, lastName, SSN, autoYear, make, model, VIN, address, city, state, zip);
            //    }
            //}

            return dt;

          
        }

        public DataTable GetPolicyIndividuals(string policyNo, string versionNo, int mode = 0)
        {
            if (mode == 0)
            {
                DataTable dt = new DataTable();

                using (var cn = new ClaimsDB().GetOpenConnection())
                {
                    var cmd = new SqlCommand("usp_Get_Policy_Individuals", cn);
                    cmd.Parameters.AddWithValue("@policy", policyNo);
                    cmd.Parameters.AddWithValue("@version", versionNo);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);

                    }

                }

                return dt;
            }
            else
            {
                DataTable dt = new DataTable();

                using (var cn = new ClaimsDB().GetOpenConnection())
                {
                    var cmd = new SqlCommand("usp_Get_Policy_Version_Individuals", cn);
                    cmd.Parameters.AddWithValue("@policy", policyNo);
                    cmd.Parameters.AddWithValue("@version", versionNo);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);

                    }

                }

                return dt;
            }
        }


        public DataTable GetPolicyVehicles(string policyNo, string versionNo, int mode = 0)
        {
            if (mode == 0)
            {
                DataTable dt = new DataTable();

                using (var cn = new ClaimsDB().GetOpenConnection())
                {
                    var cmd = new SqlCommand("usp_Get_Policy_Vehicles", cn);
                    cmd.Parameters.AddWithValue("@policy", policyNo);
                    cmd.Parameters.AddWithValue("@version", versionNo);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);

                    }

                }

                return dt;
            }
            else
            {
                DataTable dt = new DataTable();

                using (var cn = new ClaimsDB().GetOpenConnection())
                {
                    var cmd = new SqlCommand("usp_Get_Policy_Version_Vehicles", cn);
                    cmd.Parameters.AddWithValue("@policy", policyNo);
                    cmd.Parameters.AddWithValue("@version", versionNo);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);

                    }

                }

                return dt;
            }
        }

        public int InsertDuplicate(string policyNo, string versionNo, int claimId, string dateOfLoss, int possibleDuplicationClaimMode)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Insert_Possible_Duplicates", cn);
                cmd.Parameters.AddWithValue("@policy", policyNo);
                cmd.Parameters.AddWithValue("@version", versionNo);
                cmd.Parameters.AddWithValue("@claim_id", claimId);
                cmd.Parameters.AddWithValue("@dol", dateOfLoss);
                cmd.Parameters.AddWithValue("@mode", possibleDuplicationClaimMode);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            int returnString = Convert.ToInt32(dt.Rows[0][0]);
            return returnString;
        }

        public void PolGeneralInfo(string pn, string vn)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Pol_GeneralInfo", cn);
                cmd.Parameters.AddWithValue("@policyNo", pn);
                cmd.Parameters.AddWithValue("@versionNo", vn);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                   
                    dt_Exp = Convert.ToDateTime(reader["dt_exp"]);
                   
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace MedCSX.App_Code
{
    public class Vehicle
    {
        public string claim_id { get; set; }
        public string policy_vehicle_id { get; set; }
        public string year_made { get; set; }
        public string manufacturer { get; set; }
        public string model { get; set; }
        public string license_plate_no { get; set; }
        public string license_plate_state_id { get; set; }
        public string vin { get; set; }
        public string color { get; set; }
        public string is_drivable { get; set; }
        public string loss_payee_name1 { get; set; }
        public string loss_payee_name2 { get; set; }
        public string loss_payee_address1 { get; set; }
        public string loss_payee_address2 { get; set; }
        public string loss_payee_city { get; set; }
        public string loss_payee_state_id { get; set; }
        public string loss_payee_zipcode { get; set; }
        public string loss_payee_loan_no { get; set; }
        public string loss_payee_phone { get; set; }
        public string loss_payee_fax { get; set; }
        public string loss_payee_corrected_phone { get; set; }
        public string location_contact_first_name { get; set; }
        public string location_contact_email { get; set; }
        public string location_contact_last_name { get; set; }
        public string location_contact_address1 { get; set; }
        public string location_contact_address2 { get; set; }
        public string location_city { get; set; }
        public string location_state_id { get; set; }
        public string location_zipcode { get; set; }
        public string location_phone1 { get; set; }
        public string location_phone2 { get; set; }
        public string flag_fraud { get; set; }
        public string DriverPerson { get; set; }
        public string ManufactureCode { get; set; }
        public string ModelCode { get; set; }
        public string Created_By { get; set; }
        public string Modified_By { get; set; }
        public string Created_Date { get; set; }
        public string Modified_Date { get; set; }

        public string Name
        {
            get
            {
                if (this.manufacturer.Length >= 7)
                {
                    if (this.manufacturer.Substring(0, 7) == "Unknown")
                        // The manufacturer is unknown therfore the car is unknown
                        return this.manufacturer;
                }

                if (this.vin == "")
                    // The vehicle has no vin set, just return the year, make, and model
                    return (this.year_made + " " + this.manufacturer + " " + this.model).Trim();
                else
                    // Return the vehicle year, make, model, and vin
                    return (this.year_made + " " + this.manufacturer + " " + this.model + " VIN: " + this.vin).Trim();
            }
        }

        public int Add()
        {
            int insertedvalue = 0;
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Insert_Vehicle", cn);
                cmd.Parameters.AddWithValue("@Claim_Id", Convert.ToInt32(claim_id));
                cmd.Parameters.AddWithValue("@Year_Made", Convert.ToInt32(year_made));
                cmd.Parameters.AddWithValue("@Manufacturer", manufacturer);
                cmd.Parameters.AddWithValue("@Model", model);
                cmd.Parameters.AddWithValue("@License_Plate_No", model);
                cmd.Parameters.AddWithValue("@License_Plate_State_Id",Convert.ToInt32(license_plate_state_id));
                cmd.Parameters.AddWithValue("@VIN", vin);
                cmd.Parameters.AddWithValue("@Color", color);
                cmd.Parameters.AddWithValue("@Is_Drivable", Convert.ToInt32(is_drivable));
                cmd.Parameters.AddWithValue("@Location_Contact_First_Name", location_contact_first_name);
                cmd.Parameters.AddWithValue("@Location_Contact_Last_Name", location_contact_last_name);
                cmd.Parameters.AddWithValue("@Location_Contact_Email_Address", location_contact_email);
                cmd.Parameters.AddWithValue("@Location_Address1", location_contact_address1);
                cmd.Parameters.AddWithValue("@Location_Address2", location_contact_address2);
                cmd.Parameters.AddWithValue("@Location_City", location_city);
                cmd.Parameters.AddWithValue("@Location_State_Id", Convert.ToInt32(location_state_id));
                cmd.Parameters.AddWithValue("@Location_Zipcode", location_zipcode);
                cmd.Parameters.AddWithValue("@Location_Phone1", location_phone1);
                cmd.Parameters.AddWithValue("@Location_Phone2", location_phone2);
                cmd.Parameters.AddWithValue("@Created_By", Convert.ToInt32(Created_By));
                cmd.Parameters.AddWithValue("@Modified_By", Convert.ToInt32(Modified_By));
                cmd.Parameters.AddWithValue("@Created_Date", Convert.ToDateTime(Created_Date));
                cmd.Parameters.AddWithValue("@Modified_Date", Convert.ToDateTime(Modified_Date));
                cmd.Parameters.AddWithValue("@Flag_Fraud", Convert.ToInt32(flag_fraud));
                cmd.Parameters.AddWithValue("@Loss_Payee_Name1", loss_payee_name1);
                cmd.Parameters.AddWithValue("@Loss_Payee_Name2", loss_payee_name2);
                cmd.Parameters.AddWithValue("@Loss_Payee_Address1", loss_payee_address1);
                cmd.Parameters.AddWithValue("@Loss_Payee_Address2", loss_payee_address2);
                cmd.Parameters.AddWithValue("@Loss_Payee_City", loss_payee_city);
                cmd.Parameters.AddWithValue("@Loss_Payee_State_Id", Convert.ToInt32(loss_payee_state_id));
                cmd.Parameters.AddWithValue("@Loss_Payee_Zipcode",loss_payee_zipcode);
                cmd.Parameters.AddWithValue("@Loss_Payee_Loan_No", loss_payee_loan_no);
                cmd.Parameters.AddWithValue("@Loss_Payee_Phone", loss_payee_phone);
                cmd.Parameters.AddWithValue("@Loss_Payee_Fax", loss_payee_fax);
                cmd.Parameters.AddWithValue("@Loss_Payee_Corrected_Phone", loss_payee_corrected_phone);
   






                cmd.CommandType = CommandType.StoredProcedure;
                insertedvalue = Convert.ToInt32(cmd.ExecuteScalar());
            }



            return insertedvalue;
        }

        public void Update()
        {
           
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Update_Vehicle", cn);
                cmd.Parameters.AddWithValue("@Vehicle_Id", Convert.ToInt32(this.policy_vehicle_id));
                cmd.Parameters.AddWithValue("@Year_Made", Convert.ToInt32(year_made));
                cmd.Parameters.AddWithValue("@Manufacturer", manufacturer);
                cmd.Parameters.AddWithValue("@Model", model);
                cmd.Parameters.AddWithValue("@License_Plate_No", license_plate_no);
                cmd.Parameters.AddWithValue("@License_Plate_State_Id", Convert.ToInt32(license_plate_state_id));
                cmd.Parameters.AddWithValue("@VIN", vin);
                cmd.Parameters.AddWithValue("@Color", color);
                cmd.Parameters.AddWithValue("@Is_Drivable", Convert.ToInt32(is_drivable));
                cmd.Parameters.AddWithValue("@Location_Contact_First_Name", location_contact_first_name);
                cmd.Parameters.AddWithValue("@Location_Contact_Last_Name", location_contact_last_name);
                cmd.Parameters.AddWithValue("@Location_Contact_Email_Address", location_contact_email);
                cmd.Parameters.AddWithValue("@Location_Address1", location_contact_address1);
                cmd.Parameters.AddWithValue("@Location_Address2", location_contact_address2);
                cmd.Parameters.AddWithValue("@Location_City", location_city);
                cmd.Parameters.AddWithValue("@Location_State_Id", Convert.ToInt32(location_state_id));
                cmd.Parameters.AddWithValue("@Location_Zipcode", location_zipcode);
                cmd.Parameters.AddWithValue("@Location_Phone1", location_phone1);
                cmd.Parameters.AddWithValue("@Location_Phone2", location_phone2);
                cmd.Parameters.AddWithValue("@Modified_By", Convert.ToInt32(Modified_By));
                cmd.Parameters.AddWithValue("@Modified_Date", Convert.ToDateTime(Modified_Date));
                cmd.Parameters.AddWithValue("@Flag_Fraud", Convert.ToInt32(flag_fraud));
                cmd.Parameters.AddWithValue("@Loss_Payee_Name1", loss_payee_name1);
                cmd.Parameters.AddWithValue("@Loss_Payee_Name2", loss_payee_name2);
                cmd.Parameters.AddWithValue("@Loss_Payee_Address1", loss_payee_address1);
                cmd.Parameters.AddWithValue("@Loss_Payee_Address2", loss_payee_address2);
                cmd.Parameters.AddWithValue("@Loss_Payee_City", loss_payee_city);
                cmd.Parameters.AddWithValue("@Loss_Payee_State_Id", Convert.ToInt32(loss_payee_state_id));
                cmd.Parameters.AddWithValue("@Loss_Payee_Zipcode", loss_payee_zipcode);
                cmd.Parameters.AddWithValue("@Loss_Payee_Loan_No", loss_payee_loan_no);
                cmd.Parameters.AddWithValue("@Loss_Payee_Phone", loss_payee_phone);
                cmd.Parameters.AddWithValue("@Loss_Payee_Fax", loss_payee_fax);
                cmd.Parameters.AddWithValue("@Loss_Payee_Corrected_Phone", loss_payee_corrected_phone);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }



          
        }


        public Vehicle(int id)
        {
            GetVehicleByID(id);
        }

        public Vehicle()
        {
            
        }

        public string GetVehicleName(int vehicleId)
        {
            Vehicle v = new Vehicle(vehicleId);
            return v.Name;
        }



        public DataTable GetVehicles_By_ClaimId(int _claim_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_GetVehicles_By_ClaimId", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public void GetVehicleByID(int _id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Row", cn);
                cmd.Parameters.AddWithValue("@tableName", "Vehicle");
                cmd.Parameters.AddWithValue("@id", _id.ToString());
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    claim_id = reader["claim_id"].ToString();
                    policy_vehicle_id = reader["policy_vehicle_id"].ToString();
                    year_made = reader["year_made"].ToString();
                    manufacturer = reader["manufacturer"].ToString();
                    model = reader["model"].ToString();
                    license_plate_no = reader["license_plate_no"].ToString();
                    license_plate_state_id = reader["license_plate_state_id"].ToString();
                    vin = reader["vin"].ToString();
                    color = reader["color"].ToString();
                    is_drivable = reader["is_drivable"].ToString();
                    loss_payee_name1 = reader["loss_payee_name1"].ToString();
                    loss_payee_name2 = reader["loss_payee_name2"].ToString();
                    loss_payee_address1 = reader["loss_payee_address1"].ToString();
                    loss_payee_address2 = reader["loss_payee_address2"].ToString();
                    loss_payee_city = reader["loss_payee_city"].ToString();
                    loss_payee_state_id = reader["loss_payee_state_id"].ToString();
                    loss_payee_zipcode = reader["loss_payee_zipcode"].ToString();
                    loss_payee_loan_no = reader["loss_payee_loan_no"].ToString();
                    loss_payee_phone = reader["loss_payee_phone"].ToString();
                    loss_payee_fax = reader["loss_payee_fax"].ToString();
                    loss_payee_corrected_phone = reader["loss_payee_corrected_phone"].ToString();
                    location_contact_first_name = reader["location_contact_first_name"].ToString();
                    location_contact_last_name = reader["location_contact_last_name"].ToString();
                    location_contact_address1 = reader["location_address1"].ToString();
                    location_contact_address2 = reader["location_address2"].ToString();
                    location_city = reader["location_city"].ToString();
                    location_state_id = reader["location_state_id"].ToString();
                    location_zipcode = reader["location_zipcode"].ToString();
                    location_phone1 = reader["location_phone1"].ToString();
                    location_phone2 = reader["location_phone2"].ToString();
                    flag_fraud = reader["flag_fraud"].ToString();
                    location_contact_email = reader["location_contact_email_address"].ToString();

                }
            }
        }

        public bool Set_ISO_Code(string manufacturer, string model, ref string closestMatch)
        {
            string uri = System.Configuration.ConfigurationManager.AppSettings["VehicleCodeXML"];
            bool foundIt = false;

            foreach (var veh in XmlHelper.StreamVehicles(uri))
            {
                if ((manufacturer.ToUpper() == veh.Make) ||
                    ((manufacturer.Length > 4) && (manufacturer.ToUpper().Substring(0, 4) == veh.Make.Substring(0, 4))))
                {
                    closestMatch = veh.Make;
                    if (model.ToUpper() == veh.Model)
                    {
                        this.ManufactureCode = veh.Make_Code;
                        this.ModelCode = veh.Model_Code;
                        foundIt = true;
                        break;
                    }
                    else if (model.ToUpper().Substring(0, 2) == veh.Model.Substring(0, 2))
                        closestMatch += " " + veh.Model;
                }
            }
            return foundIt;
        }

       




        public static class XmlHelper
        {
            public static IEnumerable<xVehicle> StreamVehicles(string uri)
            {
                using (XmlReader reader = XmlReader.Create(uri))
                {
                    string model = null;
                    string makeCode = null;
                    string modelCode = null;
                    string description = null;

                    reader.MoveToContent();
                    while (reader.Read())
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ROW"))
                        {
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "VEHICLE_MAKE_CODE"))
                                {
                                    makeCode = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "VEHICLE_MODEL_CODE"))
                                {
                                    modelCode = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "MAKE_MODEL_DESCRIPTION"))
                                {
                                    description = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "MODEL_DESCRIPTION"))
                                {
                                    model = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            string[] makeModDesc = description.Split(' ');
                            yield return new xVehicle()
                            {
                                Make = makeModDesc[0],
                                Model = model,
                                Make_Code = makeCode,
                                Model_Code = modelCode
                            };
                        }
                    }
                }
            }
        }

        public class xVehicle
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Make_Code { get; set; }
            public string Model_Code { get; set; }
        }

    }

}
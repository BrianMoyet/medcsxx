﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Claimants
    {

        public string salutation { get; set; }
        public string last_name { get; set; }
        public string middle_initial { get; set; }
        public string first_name { get; set; }
        public string suffix { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state_id { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string home_phone { get; set; }
        public string work_phone { get; set; }
        public string cell_phone { get; set; }
        public string other_contact_phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string date_of_birth { get; set; }
        public string ssn { get; set; }
        public string drivers_license_no { get; set; }
        public string sex { get; set; }
        public string employer { get; set; }
        public string language_id { get; set; }
        public string comments { get; set; }
        public string isCompany { get; set; }
        public string person_id { get; set; }
        public string claim_id { get; set; }
        public string Created_By { get; set; }
        public string Modified_By { get; set; }
        public string Created_Date { get; set; }
        public string Modified_Date { get; set; }
        public string Created_By_Name { get; set; }


        public int AddClaimant()
        {
            int insertedvalue = 0;
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Insert_Claimant", cn);
                cmd.Parameters.AddWithValue("@Person_Id", Convert.ToInt32(person_id));
                cmd.Parameters.AddWithValue("@claim_id", Convert.ToInt32(claim_id));
                cmd.Parameters.AddWithValue("@Created_By", Convert.ToInt32(Created_By));
                cmd.Parameters.AddWithValue("@Modified_By", Convert.ToInt32(Modified_By));
                cmd.Parameters.AddWithValue("@Created_Date", Convert.ToDateTime(Created_Date));
                cmd.Parameters.AddWithValue("@Modified_Date", Convert.ToDateTime(Modified_Date));

                cmd.CommandType = CommandType.StoredProcedure;
                insertedvalue = Convert.ToInt32(cmd.ExecuteScalar());
            }



            return insertedvalue;
        }
        public DataTable GetClaimantsForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Claim_Persons", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public DataTable GetClaimantsAndIDsForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Claimants", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }


        public void GetPersonByID(int _person_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Person", cn);
                cmd.Parameters.AddWithValue("@person_id", _person_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    salutation = reader["salutation"].ToString();
                    last_name = reader["last_name"].ToString();
                    middle_initial = reader["middle_initial"].ToString();
                    first_name = reader["first_name"].ToString();
                    suffix = reader["suffix"].ToString();
                    address1 = reader["address1"].ToString();
                    address2 = reader["address2"].ToString();
                    city = reader["city"].ToString();
                    state_id = reader["state_id"].ToString();
                    zipcode = reader["zipcode"].ToString();
                    home_phone = reader["home_phone"].ToString();
                    work_phone = reader["work_phone"].ToString();
                    cell_phone = reader["cell_phone"].ToString();
                    other_contact_phone = reader["other_contact_phone"].ToString();
                    fax = reader["fax"].ToString();
                    email = reader["email"].ToString();
                    date_of_birth = reader["date_of_birth"].ToString();
                    ssn = reader["ssn"].ToString();
                    drivers_license_no = reader["drivers_license_no"].ToString();
                    sex = reader["sex"].ToString();
                    employer = reader["employer"].ToString();
                    language_id = reader["language_id"].ToString();
                    comments = reader["comments"].ToString();
                    isCompany = reader["is_Company"].ToString();
                }

            }

           
        }
    }
}
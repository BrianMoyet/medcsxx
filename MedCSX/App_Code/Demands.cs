﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Demands
    {
        public static int AttorneyVendorType = System.Convert.ToInt32(ModGeneratedEnums.VendorSubType.Claimant_Attorney);

        public DataTable GetDemands(int _userID)
        {

            DataTable dtDemands = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_MY_Demands", cn);
                cmd.Parameters.AddWithValue("@adjuster_id", _userID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtDemands);

                }

            }

            return dtDemands;
        }

        public DataTable GetDemandsByClaimID(int _claim_id)
        {

            DataTable dtDemands = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Demands_by_Claim_ID", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtDemands);

                }

            }

            return dtDemands;
        }

        public DataTable GetDemandHistoryByClaimID(int vendorID, int reserveID)
        {

            DataTable dtDemands = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Demand_History", cn);
                cmd.Parameters.AddWithValue("@vendor_id", vendorID);
                cmd.Parameters.AddWithValue("@reserve_id", reserveID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtDemands);

                }

            }

            return dtDemands;
        }
    }
}
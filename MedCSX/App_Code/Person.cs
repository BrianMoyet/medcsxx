﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Person
    {
       
        public string claim_id { get; set; }
        public string person_id { get; set; }
        public string person_type_id { get; set; }
        public string policy_individual_id { get; set; }
        public string salutation { get; set; }
        public string last_name { get; set; }
        public string middle_initial { get; set; }
        public string first_name { get; set; }
        public string suffix { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state_id { get; set; }
        public string zipcode { get; set; }
        public string home_phone { get; set; }
        public string work_phone { get; set; }
        public string cell_phone { get; set; }
        public string other_contact_phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string date_of_birth { get; set; }
        public string ssn { get; set; }
        public string drivers_license_no { get; set; }
        public string drivers_license_state_id { get; set; }
        public string sex { get; set; }
        public string employer { get; set; }
        public string language_id { get; set; }
        public string comments { get; set; }
        public bool isCompany { get; set; }
        public bool HasOtherInsurance { get; set; }
        public bool fraudInd { get; set; }
        public string OtherInsCompany { get; set; }
        public string OtherInsPolicyNo { get; set; }
        public string OtherInsClaimNo { get; set; }
        public string OtherInsAgentName { get; set; }
        public string OtherInsAgentAddress { get; set; }
        public string OtherInsAgentCity { get; set; }
        public string OtherInsAgentZipCode { get; set; }
        public string OtherInsAgentStateId { get; set; }
        public string OtherInsAgentPhone { get; set; }
        public string OtherInsAgentFax { get; set; }
        public string OtherInsAgentEmail { get; set; }

        public string Created_By { get; set; }
        public string Modified_By { get; set; }
        public string Created_Date { get; set; }
        public string Modified_Date { get; set; }

        public int AddPerson()
        {
            int insertedvalue = 0;
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Insert_Person", cn);
                cmd.Parameters.AddWithValue("@claim_id", Convert.ToInt32(claim_id));
                cmd.Parameters.AddWithValue("@policy_individual_id", Convert.ToInt32(policy_individual_id));
                cmd.Parameters.AddWithValue("@person_type_id", Convert.ToInt32(person_type_id));
                cmd.Parameters.AddWithValue("@salutation", salutation);
                cmd.Parameters.AddWithValue("@last_name", last_name);
                cmd.Parameters.AddWithValue("@middle_initial", middle_initial);
                cmd.Parameters.AddWithValue("@first_name", first_name);
                cmd.Parameters.AddWithValue("@suffix", suffix);
                cmd.Parameters.AddWithValue("@address1", address1);
                cmd.Parameters.AddWithValue("@address2", address2);
                cmd.Parameters.AddWithValue("@city", city);
                cmd.Parameters.AddWithValue("@state_id", Convert.ToInt32(state_id));
                cmd.Parameters.AddWithValue("@zipcode", zipcode);
                cmd.Parameters.AddWithValue("@home_phone", home_phone);
                cmd.Parameters.AddWithValue("@work_phone", work_phone);
                cmd.Parameters.AddWithValue("@cell_phone", cell_phone);
                cmd.Parameters.AddWithValue("@other_contact_phone", other_contact_phone);
                cmd.Parameters.AddWithValue("@fax", fax);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@date_of_birth", date_of_birth);
                cmd.Parameters.AddWithValue("@ssn", ssn);
                cmd.Parameters.AddWithValue("@drivers_license_no", drivers_license_no);
                cmd.Parameters.AddWithValue("@sex", sex);
                cmd.Parameters.AddWithValue("@employer", employer);
                cmd.Parameters.AddWithValue("@language_id", Convert.ToInt32(language_id));
                cmd.Parameters.AddWithValue("@is_company", Convert.ToInt32(isCompany));
                cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));
                cmd.Parameters.AddWithValue("@comments", comments);
                cmd.Parameters.AddWithValue("@fraudInd", fraudInd);
                cmd.Parameters.AddWithValue("@hasOtherIns", HasOtherInsurance);

                if (HasOtherInsurance)
                {
                    cmd.Parameters.AddWithValue("@OtherInsCompany", OtherInsCompany);
                    cmd.Parameters.AddWithValue("@OtherInsPolicyNo", OtherInsPolicyNo);
                    cmd.Parameters.AddWithValue("@OtherInsClaimNo", OtherInsClaimNo);
                    cmd.Parameters.AddWithValue("@OtherInsAgent", OtherInsAgentName);
                    cmd.Parameters.AddWithValue("@OtherInsAgentAddress", OtherInsAgentAddress);
                    cmd.Parameters.AddWithValue("@OtherInsAgentCity", OtherInsAgentCity);
                    cmd.Parameters.AddWithValue("@OtherInsAgentStateId", OtherInsAgentStateId);
                    cmd.Parameters.AddWithValue("@OtherInsAgentZip", OtherInsAgentZipCode);
                    cmd.Parameters.AddWithValue("@OtherInsAgentPhone", OtherInsAgentPhone);
                    cmd.Parameters.AddWithValue("@OtherInsAgentFax", OtherInsAgentFax);
                    cmd.Parameters.AddWithValue("@OtherInsAgentEmail", OtherInsAgentEmail);

                }





                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();


            }



            return insertedvalue;
        }

        public void UpdatePerson()
        {
          
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Update_Person", cn);
                cmd.Parameters.AddWithValue("@person_id", Convert.ToInt32(person_id));
                cmd.Parameters.AddWithValue("@claim_id", Convert.ToInt32(claim_id));
                cmd.Parameters.AddWithValue("@person_type_id", Convert.ToInt32(person_type_id));
                cmd.Parameters.AddWithValue("@policy_individual_id", Convert.ToInt32(policy_individual_id));
                cmd.Parameters.AddWithValue("@salutation", salutation);
                cmd.Parameters.AddWithValue("@last_name", last_name);
                cmd.Parameters.AddWithValue("@middle_initial", middle_initial);
                cmd.Parameters.AddWithValue("@first_name", first_name);
                cmd.Parameters.AddWithValue("@suffix", suffix);
                cmd.Parameters.AddWithValue("@address1", address1);
                cmd.Parameters.AddWithValue("@address2", address2);
                cmd.Parameters.AddWithValue("@city", city);
                cmd.Parameters.AddWithValue("@state_id", Convert.ToInt32(state_id));
                cmd.Parameters.AddWithValue("@zipcode", zipcode);
                cmd.Parameters.AddWithValue("@home_phone", home_phone);
                cmd.Parameters.AddWithValue("@work_phone", work_phone);
                cmd.Parameters.AddWithValue("@cell_phone", cell_phone);
                cmd.Parameters.AddWithValue("@other_contact_phone", other_contact_phone);
                cmd.Parameters.AddWithValue("@fax", fax);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@date_of_birth", date_of_birth);
                cmd.Parameters.AddWithValue("@ssn", ssn);
                cmd.Parameters.AddWithValue("@drivers_license_no", drivers_license_no);
                cmd.Parameters.AddWithValue("@sex", sex);
                cmd.Parameters.AddWithValue("@employer", employer);
                cmd.Parameters.AddWithValue("@language_id", Convert.ToInt32(language_id));
                cmd.Parameters.AddWithValue("@is_company", Convert.ToInt32(isCompany));
                cmd.Parameters.AddWithValue("@user_id", Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"]));
                cmd.Parameters.AddWithValue("@addcomment", comments);
                cmd.Parameters.AddWithValue("@fraudInd", fraudInd);
                cmd.Parameters.AddWithValue("@hasOtherIns", HasOtherInsurance);

                if (HasOtherInsurance)
                {
                    cmd.Parameters.AddWithValue("@OtherInsCompany", OtherInsCompany);
                    cmd.Parameters.AddWithValue("@OtherInsPolicyNo", OtherInsPolicyNo);
                    cmd.Parameters.AddWithValue("@OtherInsClaimNo", OtherInsClaimNo);
                    cmd.Parameters.AddWithValue("@OtherInsAgent", OtherInsAgentName);
                    cmd.Parameters.AddWithValue("@OtherInsAgentAddress", OtherInsAgentAddress);
                    cmd.Parameters.AddWithValue("@OtherInsAgentCity", OtherInsAgentCity);
                    cmd.Parameters.AddWithValue("@OtherInsAgentStateId", OtherInsAgentStateId);
                    cmd.Parameters.AddWithValue("@OtherInsAgentZip", OtherInsAgentZipCode);
                    cmd.Parameters.AddWithValue("@OtherInsAgentPhone", OtherInsAgentPhone);
                    cmd.Parameters.AddWithValue("@OtherInsAgentFax", OtherInsAgentFax);
                    cmd.Parameters.AddWithValue("@OtherInsAgentEmail", OtherInsAgentEmail);

                }





                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();


            }



          
        }

        public void GetPersonByPersonID(int _person_id)
        {
            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Person", cn);
                cmd.Parameters.AddWithValue("@person_id", _person_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    claim_id = reader["claim_id"].ToString();
                    person_type_id = reader["person_type_id"].ToString();
                    policy_individual_id = reader["policy_individual_id"].ToString();
                    salutation = reader["salutation"].ToString();
                    last_name = reader["last_name"].ToString();
                    middle_initial = reader["middle_initial"].ToString();
                    first_name = reader["first_name"].ToString();
                    suffix = reader["suffix"].ToString();
                    address1 = reader["address1"].ToString();
                    address2 = reader["address2"].ToString();
                    city = reader["city"].ToString();
                    state_id = reader["state_id"].ToString();
                    zipcode = reader["zipcode"].ToString();
                    home_phone = reader["home_phone"].ToString();
                    work_phone = reader["work_phone"].ToString();
                    cell_phone = reader["cell_phone"].ToString();
                    other_contact_phone = reader["other_contact_phone"].ToString();
                    fax = reader["fax"].ToString();
                    email = reader["email"].ToString();
                    date_of_birth = reader["date_of_birth"].ToString();
                    ssn = reader["ssn"].ToString();
                    drivers_license_no = reader["drivers_license_no"].ToString();
                    sex = reader["sex"].ToString();
                    employer = reader["employer"].ToString();
                    language_id = reader["language_id"].ToString();
                    comments = reader["comments"].ToString();
                    isCompany = Convert.ToBoolean(reader["is_Company"]);

                    fraudInd = Convert.ToBoolean(reader["flag_fraud"]);
                    HasOtherInsurance = Convert.ToBoolean(reader["has_other_insurance"]);
                    OtherInsCompany = reader["Other_Insurance_Company"].ToString();
                    OtherInsPolicyNo = reader["Other_Insurance_Policy_No"].ToString();
                    OtherInsClaimNo = reader["Other_Insurance_Claim_No"].ToString();
                    OtherInsAgentName = reader["Other_Insurance_Agent_name"].ToString();
                    OtherInsAgentAddress =  reader["Other_Insurance_Agent_Address"].ToString();
                    OtherInsAgentCity =  reader["Other_Insurance_Agent_City"].ToString();
                    OtherInsAgentStateId =  reader["Other_Insurance_Agent_State_Id"].ToString();
                    OtherInsAgentZipCode =  reader["Other_Insurance_Agent_Zipcode"].ToString();
                    OtherInsAgentPhone = reader["Other_Insurance_Agent_Phone"].ToString();
                    OtherInsAgentFax = reader["Other_Insurance_Agent_Fax"].ToString();
                    OtherInsAgentEmail = reader["Other_Insurance_Agent_Email"].ToString();


                }
            }
        }

        public DataTable GetLanguages()
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Languages", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }
    }


}
﻿using MedCsxLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MedCsxLogic;
using MedCsxDatabase;

namespace MedCSX.App_Code
{
    public class ModForms
    {
        private static modLogic ml = new modLogic();
        private static clsDatabase db = new clsDatabase();
        private static ModMain mm = new ModMain();
       
        private string msg = string.Empty;
        private string reasonPart = string.Empty;
        private string noLockPart = ".  Proceeding Will Place the Reserve in a Locked State Until Approved by your Supervisor.  Do You Wish to Proceed?";
        private string lockedPart = ".  This would normally add a lock to the reserve, but since it's alread locked, the system will just create a note instead.  Do You Wish to Proceed?";
        private int m_Claim_Id;
        //private ImageList ilTypeTables = null;
        //private ImageList ilStates = null;
        private List<int> AllItemImages = new List<int>();
        private List<int> AllItemIDs = new List<int>();
        private List<Hashtable> AllItemsWithIds = new List<Hashtable>();

        public void ProcessError(Exception e)
        {
            //Update the last activity time
            mm.UpdateLastActivityTime();
            //MouseNormal(); //Set the cursor back to normal

            //Display error
            //TODO*
            //System.Windows.MessageBox.Show(e.ToString(), "MecCsX Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error, System.Windows.MessageBoxResult.OK);
        }
    }
}
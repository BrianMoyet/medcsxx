﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace MedCSX.App_Code
{
    public class Litigation
    {
        public string litigation_id { get; set; }
        public string litigation_status_id { get; set; }
        public string defense_attorney_id { get; set; }
        public string plaintiff_attorney_id { get; set; }
        public string court_location_id { get; set; }
        public string date_filed { get; set; }
        public string date_served { get; set; }
        public string reslolution_amount { get; set; }
        public string comments { get; set; }


        public static int DefenseAttorneyType = System.Convert.ToInt32(ModGeneratedEnums.VendorSubType.Defense_Attorney);
        public static int PlaintiffAttorneyType = System.Convert.ToInt32(ModGeneratedEnums.VendorSubType.Claimant_Attorney);
        public static int CourtLocationType = System.Convert.ToInt32(ModGeneratedEnums.VendorSubType.Courts_Municipalities);


        public void GetLitigationByID(int _litigation_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Litigation_By_Id", cn);
                cmd.Parameters.AddWithValue("@litigation_id", _litigation_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    litigation_id = reader["litigation_id"].ToString();
                    litigation_status_id = reader["litigation_status_id"].ToString();
                    defense_attorney_id = reader["defense_attorney_id"].ToString();
                    plaintiff_attorney_id = reader["plaintiff_attorney_id"].ToString();
                    court_location_id = reader["court_location_id"].ToString();
                    date_filed = reader["date_filed"].ToString();
                    date_served = reader["date_served"].ToString();
                    comments = reader["comments"].ToString();
                    

                }

            }


        }
        public DataTable GetLitigationForClaim(int _claim_id)
        {

            DataTable dtSalvageTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Litigation", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtSalvageTable);

                }

            }

            return dtSalvageTable;
        }

        public DataTable usp_Get_Litigation_Defendents(int _litigation_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Litigation_Defendents", cn);
                cmd.Parameters.AddWithValue("@litigation_id", _litigation_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Documents
    {
        public DataTable GetDocuments_By_ClaimId(int _claim_id)
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Documents", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Task
    {


        public string user_task_type_id { get; set; }
        public string task_description { get; set; }
        public string claim_id { get; set; }
        public string display_claim_id { get; set; }
        public string claimant_id { get; set; }
        public string reserve_type_id { get; set; }


        public DataTable GetTask(int _userID)
        {

            DataTable dtTaskTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("Get_My_Tasks2", cn);
                cmd.Parameters.AddWithValue("@user_id", _userID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtTaskTable);

                }

            }

            return dtTaskTable;
        }

        public DataTable GetTasksForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Tasks_For_Claim_And_User", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.Parameters.AddWithValue("@user_id", 0);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public void GetTasksByID(int _user_task_id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_MedCsX_Get_Tasks_By_Id", cn);
                cmd.Parameters.AddWithValue("@user_task_id", _user_task_id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user_task_type_id = reader["user_task_type_id"].ToString();
                    task_description = reader["task_description"].ToString();
                    claim_id = reader["claim_id"].ToString();
                    display_claim_id = reader["display_claim_id"].ToString();
                    claimant_id = reader["claimant_id"].ToString();
                    reserve_type_id = reader["reserve_type_id"].ToString();
                }
            }
        }


    }
}
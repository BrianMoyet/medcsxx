﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class PropertyDamage
    {
        public string reserve_id { get; set; }
        public string property_type_id { get; set; }
        public string property_description { get; set; }
        public string vehicle_id { get; set; }
        public string other_insurance_company { get; set; }
        public string other_insurance_policy_no { get; set; }
        public string other_insurance_claim_no { get; set; }
        public string other_insurance_adjuster { get; set; }
        public string other_insurance_phone { get; set; }
        public string other_insurance_fax { get; set; }
        public string other_insurance_address { get; set; }
        public string other_insurance_city { get; set; }
        public string other_insurance_state_id { get; set; }
        public string other_insurance_zipcode { get; set; }
        public string damage_description { get; set; }
        public string where_seen { get; set; }
        public string estimated_damage_amount { get; set; }
        public string owner_person_id { get; set; }
        public string driver_person_id { get; set; }
        public string rental_days { get; set; }
        public string towing { get; set; }
        public string storage_days { get; set; }
        public string gross_storage { get; set; }
        public string date_title_received { get; set; }
        public string reserve_type_id { get; set; }


        public DataTable GetPropertyDamagesForClaim(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("Get_Property_Damages_For_Claim", cn);
                cmd.Parameters.AddWithValue("@claim_id", _claim_id);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }

        public void GetPropertyDamagesByID(int _id)
        {

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Property_Damage", cn);
                cmd.Parameters.AddWithValue("@property_damage_id", _id);
                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    reserve_id = reader["reserve_id"].ToString();
                    property_type_id = reader["property_type_id"].ToString();
                    property_description = reader["property_description"].ToString();
                    vehicle_id = reader["vehicle_id"].ToString();
                    other_insurance_company = reader["other_insurance_company"].ToString();
                    other_insurance_policy_no = reader["other_insurance_policy_no"].ToString();
                    other_insurance_claim_no = reader["other_insurance_claim_no"].ToString();
                    other_insurance_adjuster = reader["other_insurance_adjuster"].ToString();
                    other_insurance_phone = reader["other_insurance_phone"].ToString();
                    other_insurance_fax = reader["other_insurance_fax"].ToString();
                    other_insurance_address = reader["other_insurance_address"].ToString();
                    other_insurance_city = reader["other_insurance_city"].ToString();
                    other_insurance_state_id = reader["other_insurance_state_id"].ToString();
                    other_insurance_zipcode = reader["other_insurance_zipcode"].ToString();
                    damage_description = reader["damage_description"].ToString();
                    where_seen = reader["where_seen"].ToString();
                    estimated_damage_amount = reader["estimated_damage_amount"].ToString();
                    owner_person_id = reader["owner_person_id"].ToString();
                    driver_person_id = reader["driver_person_id"].ToString();
                    rental_days = reader["rental_days"].ToString();
                    towing = reader["towing"].ToString();
                    storage_days = reader["storage_days"].ToString();
                    gross_storage = reader["gross_storage"].ToString();
                    date_title_received = reader["date_title_received"].ToString();
                    reserve_type_id = reader["reserve_type_id"].ToString();





                }
            }
        }

        public DataTable GetPropertyTypes()
        {

            DataTable dtReserveTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Property_Types", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtReserveTable);

                }

            }

            return dtReserveTable;
        }

        public DataTable GePersonNamesAndTypes(int _claim_id)
        {

            DataTable dt = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_Claim_Persons", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }

            return dt;
        }
    }
}
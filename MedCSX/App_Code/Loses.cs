﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MedCSX.App_Code
{
    public class Loses
    {
        public DataTable GetLoses(int _userID)
        {

            DataTable dtLosesTable = new DataTable();

            using (var cn = new ClaimsDB().GetOpenConnection())
            {
                var cmd = new SqlCommand("usp_Get_My_TotalLosses", cn);
                cmd.Parameters.AddWithValue("@user_id", _userID);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dtLosesTable);

                }

            }

            return dtLosesTable;
        }
    }
}
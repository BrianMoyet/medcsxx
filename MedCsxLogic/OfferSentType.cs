﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class OfferSentType:TypeTable
    {

        #region Contructors
        public OfferSentType() : base() { }
        public OfferSentType(int id) : base(id) { }
        public OfferSentType(string description) : base(description) { }
        public OfferSentType(Hashtable row) : base(row) { }
        #endregion

        #region Table Properties
        public override string TableName
        {
            get
            {
                return "Offer_Sent_Type";
            }
        }
        #endregion
    }
}

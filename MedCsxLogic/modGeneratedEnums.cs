﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace MedCsxLogic
{
    public static class modGeneratedEnums
    {
        public enum AccidentType : int
        {
            IV_Rear_Ended_CV = 2,
            CV_Rear_Ended_IV = 3,
            IV_Failed_to_Yield_Right_of_Way = 4, 
            IV_Ran_Stop_Sign = 5,
            IV_Ran_Red_Light = 6,
            IV_Struck_Parked_and_Unoccupied_CV = 7,
            IV_Parked_and_Unoccupied_when_Struck_by_Hit_and_Run = 8,
            IV_Backed_out_of_Parking_Space_and_Struck_CV = 9,
            IV_Made_Left_Turn_in_Front_of_CV = 10,
            Insured_Person_in_Other_Vehicle = 11,
            Insured_Hit_Pedestrian = 12,
            Insured_Vehicle_Stolen = 13,
            Insured_Vehicle_Vandalized = 14,
            Slip_And_Fall = 15,
            Trip_And_Fall_Same_Level = 16,
            Food_Poisoning = 17,
            Cart_Damage = 18,
            Struck_By_Object_Handled_by_Other = 19,
            Struck_by_Object_Falling = 20,
            Struck_by_Object_Stationary = 21,
            Miscellaneous = 22,
            Trip_and_Fall_Different_Level = 23,
            No_Known_Cause = 24,
            Ingest_Inhale = 25,
            /* these need to be checked after the live insert for the correct values */
            Explosion = 50,
            Lightning = 51,
            Smoke = 52,
            Sprinkler_Leakage = 53,
            Theft = 54,
            Vandalism = 55,
            Vehicle_ = 56,
            Weight_of_Snow_and_Ice = 57,
            Windstorm_or_Hail = 58,
            CV_Merged_into_IV_lane = 369,
            IV_merged_into_CV_lane = 370
        };

        public enum AlertPreference : int
        {
            MedCsX_Alert = 1,           //01
            Email = 2,                  //02
            None = 3,                   //03
            Electric_Shock_Collar = 4,  //04
            Non_intrusive = 5,          //05
            Non_intrusive_Happy = 6,    //06
            Non_intrusive_Dude = 7,     //07
            Non_intrusive_Cartman = 9,  //08
            Non_intrusive_Bill = 9     //09
        };

        public enum AppraisalInvestigationType : int
        {
            Appraisal,          //01
            Photo_Only_Vehicle, //02
            Scene_Investigation,//03
            Statement           //04
        };

        public enum AppraisalRequestStatus : int
        {
            XML_Written = 2,
            Request_Created = 3,
            Documents_Received = 4,
            Request_Manually_Created = 5
        };

        public enum AppraisalTaskStatus : int
        {
            Appraisal_Requested = 2,
            Appraisal_Completed = 3,
            Awaiting_Response = 4,
            Appraisal_Cancelled = 5,
            Appraisal_Resent = 6
        };

        public enum AtFaultType : int
        {
            Undetermined = 0,
            Yes = 3,
            No = 4
        };

        public enum BIDiagnosisType : int
        {
            Degenerative_Disc_Disease = 5,
            Soft_Tissue_Back_Only = 6,
            Soft_Tissue_Neck_Upper_Back_Only = 7,
            Soft_Tissue_Neck_and_Back = 8
        };

        public enum BIDiagnosticType : int
        {
            MRI = 2,
            CT_Scan = 3,
            X_Rays = 4,
            Nerve_Conduction_Study_EMG = 6,
            Drugs = 7
        };

        public enum BIInjuryType : int
        {
            Soft_Tissue_Neck_and_Back = 2, 
            Back_and_Neck_Traumatic_Disk = 3,
            Back_Neck_Chronic_Disk_or_Degenerative_Changes = 4,
            Minor_Fractures = 5,
            Significant_Fractures = 6,
            Other_Injury = 7
        };

        public enum BodyPart : int 
        {
            Undefined = 0,
            Head = 1,
            Eyes = 2,
            Nose = 3,
            Mouth = 4,
            Teeth = 5,
            Neck = 6,
            Shoulder = 7,
            Upper_Back = 8,
            Lower_Back = 9,
            Arm = 10,
            Elbow = 11,
            Wrist = 12,
            Hand = 13,
            Fingers = 14,
            Chest = 15,
            Stomach = 16,
            Hip = 17,
            Pelvis = 18,
            Upper_Leg = 19,
            Knee = 20,
            Lower_Leg = 21,
            Foot = 22,
            Ankle = 23,
            Toes = 24,
            Thumb = 25,
            Ear = 26,
            Buttocks = 27,
            Tailbone = 28
        };

        public enum BugSeverityType : int
        {
            Low,
            Medium,
            High
        };

        public enum BugStatusType : int
        {
            Reported,
            Working_On,
            Fixed,
            Couldnt_Reproduce,
            Not_an_Error,
            Cant_Fix
        };

        public enum BusinessDescription : int 
        {
            PROBABILITY_OF_OUTCOME_DAMAGE_EVALUATION,       //01
            PROBABILITY_OF_OUTCOME_LIABILITY_EVALUATION,    //02
            BEST_CASE_COMPARATIVE_NEGLIGENCE_OUTCOME,       //03
            WORST_CASE_COMPARATIVE_NEGLIGENCE_OUTCOME,      //04
            GENERAL_DAMAGES,                                //05
            WAGE_LOSS,                                      //06
            ESTIMATED_FUTURE_MEDICAL_AMOUNT,                //07
            WORST_CASE_COST,                                //08
            BEST_CASE_COST,                                 //09
            CHRONIC,                                        //10
            REPORT_FEES,                                    //11
            AMBULANCE                                       //12
        };

        public enum ChoicePointReportType : int
        {
            Auto_Accident,                 //01
            Auto_Theft,                    //02
            Auto_Theft_Recovery,           //03
            Theft_Burglary,                //04
            Fire_Building,                 //05
            Fire_Auto,                     //06
            Vandalism_Civil_Offense,       //07
            DUI_Report,                    //08
            Drivers_Record,                //09
            Registered_Vehicle_Owner,      //10
            Coroners_Report,               //11
            Homicide_Report,               //12
            Accident_Photos,               //13
            Issue_Letter_of_Interest,      //14
            Other_See_Other_Description,   //15
            Insurance_Verification,        //16
            Title_History,                 //17
            All_Reg_Vehicles_in_Household, //18
            Death_Certificate,             //19
            Birth_Certificate,             //20
            Arrest_Report,                 //21
            Citation_Conviction_Report,    //22
            EMS_Report,                    //23
            Reconstruction_Report,         //24
            Supplemental_Report,           //25
            Toxicology_Report              //26
        };

        public enum ChoicePointRequestStatus : int 
        {
            Police_Report_Requested,                //1
            Waiting_for_Report_from_ChoicePoint,    //2
            Police_Report_Delivered                 //3
        };

        public enum ClaimAuditCategoryType : int
        {
            Contacts = 67,
            Coverage = 68,
            Investigation = 69,
            Liability_Evaluation = 70,
            Bodily_Injury = 71,
            Documentation = 72,
            Resolution = 73,
            Salvage = 74,
            Physical_Damage = 75,
            Rental = 76,
            Subrogation = 77
        };

        public enum ClaimAuditItemType : int 
        {
            Proactive_Insured_Contact = 33,
            Proactive_Claimant_Contact = 34,
            Regulatory_Compliance = 35,
            Appropriate_Identification = 36,
            Timeliness = 37,
            Investigation_Quality = 39,
            Investigation_Timeliness = 40,
            Evaluation_Quality = 42,
            Evaluation_Timeliness = 50,
            Timeliness_of_Response = 53,
            Documentation_Quality = 55,
            Quality = 57,
            Appraisal_Quality = 62,
            Appraisal_Timeliness = 63,
            Appropriate_Replacement = 65,
            Duration_Control = 66,
            Aggresive_Pursuit = 69
        };

        public enum ClaimAuditResult : int 
        {
            Yes,
            No,
            N_A
        };

        public enum ClaimStatus : int 
        {
            Undefined = 0,
            Claim_Open = 4,
            Claim_Open_Without_Policy = 5,
            Claim_Closed = 6,
            Claim_Reopened = 7
        };

        public enum ClaimType : int 
        {
            All = 0,
            Auto = 1,
            Commercial = 2,
            London = 3,  //do I have to put a note here saying this is for London?  HT.
            Obsolete = 4,
            MJi_Habitational = 5
        };

        public enum Company : int 
        {
            Undefined = 0,
            Guarantee_National = 1,
            Benchmark = 2,
            American_Sterling = 9,
            Key_Insurance = 10,
            Austin_Mutual = 999,
            Companion = 1001,
            Key_Extracontractual = 1002,
            MJi_Specialty_Programs_Property_2017 = 1003,
            MJi_Specialty_Programs_Property_20xx = 1004,
            MJi_Specialty_Programs_Property_2018 = 1005,
            MJi_Specialty_Programs_Property_2019 = 1006,
            Trisura_Commercial_Property = 1007,
            Trisura_Commercial_Liability = 1008
        };

        public enum CompanyLocation : int
        {
            Undefined = 0,
            Benchmark_Kansas_City = 2,
            Guarantee_National_Kansas_City = 3,
            American_Sterling_Kansas_City = 5,
            //Key_Insurance_Kansas_City = 7
            Key_Insurance_Company = 7, //this was changed to match the database entry in Claims.Company_Location.Enum_Name - H.T. 11/13/08
            Austin_Mutual_Kansas_City = 6,
            Lloyds_London_Kansas_City = 8 , //Added for London.  HT.
            Companion_Kansas_City = 10
        };

        public enum CoverageType : int 
        {
            Policy_in_Force,                //1
            Policy_not_Found,               //2
            Date_not_Covered,               //3
            NSF_Cancellation,               //4
            Short_Rate_Cancellation,        //5
            Pro_Rate_Cancellation,          //6
            Flat_Cancellation               //7
        };

        public enum DemandOfferType : int 
        {
            Demand,
            Offer
        };

        public enum DenialReasonType : int 
        {
            Unknown = 0,
            No_Coverage_Policy_Not_In_Force = 3,
            No_Coverage_Non_permissive_use = 4,
            No_Coverage_Non_cooperation_Of_Insured = 5,
            No_Coverage_Policy_Voided_Ab_Initio = 6,
            No_Coverage_Policy_Exclusion = 7,
            Third_Party_Claim_Insured_Not_Liable = 8,
            Third_Party_Claim_Claim_Withdrawn_or_Not_Pursued = 9,
            First_Party_Insured_Withdrew_Claim = 10,
            Insured_Loss_Less_Than_Deductible = 11,
            FoxPro_Conversion_Loss_paid_in_FoxPro = 12,
            Claim_Void_Set_Up_In_Error = 13,
            Insured_Reported_As_Record_Only = 14
        };

        public enum DiaryType : int 
        {
            General_Diary = 2,
            Respond_to_Supervisory_Instructions = 3,
            Pending_Attorney_Response = 4,
            Pending_Insured_Response = 5,
            Pending_Claimant_Response = 6,
            Pending_Witness_Response = 7,
            Pending_Adverse_Carrier_Response = 8,
            Pending_Underwriting_Response = 9,
            LW_Form = 10,
            PIP_App = 11,
            Waiting_For_Med_Bills_Records = 12,
            Waiting_For_Med_Auth = 13,
            Waiting_For_Settlement_Demand = 14,
            Waiting_For_Estimates = 15,
            Waiting_For_Release = 16,
            Waiting_For_Rental_Bill = 17,
            Waiting_For_Adjuster_Report = 18,
            Waiting_For_Police_Report = 19,
            Waiting_For_Title = 20,
            Waiting_For_Lien_Release = 21,
            Waiting_For_Salvage_Proceeds = 22,
            Waiting_For_Subrogation = 23,
            Waiting_for_Power_Of_Attorney = 24,
            Pending_For_Agency_Response = 25,
            Supervisor_Review_Outstanding_BI_Demand = 26,
            Supervisor_Follow_up_with_Adjuster_on_ID_Complaint = 27,
            Waiting_for_Subrogation_Proceeds = 28,
            Pending_Offer = 29,
            Pending_Offer_Acceptance = 30
        };

        public enum DocumentStatus : int 
        {
            Undefined,
            Pending,
            Printed,
            Cancelled 
        };

        public enum DocumentType : int 
        {
            Acknowledge_Attorney_Rep_Letter = 1,
            Acknowledge_Demand_And_Make_Counteroffer = 2,
            Acknowledge_Demand = 3,
            Acknowledge_Subro_To_Another_Insurer = 4,
            Affidavit_Certified_Policy_Notarized_Melanie_French = 5,
            Affidavit_Of_Correction = 6,
            Affidavit_Of_Records_Custodian = 7,
            Assignment_To_IA_Where_Insured_Is_Not_Cooperating = 8,
            Claims_Professionals_Blank_Letterhead_Letterhead_Only = 9,
            Claims_Professionals_Blank_Letterhead_Spanish = 10,
            Claims_Professionals_Blank_Letterhead_To_Another_Insurer = 11,
            Claims_Professionals_Blank_Letterhead_To_Insurance_Dept = 12,
            Claims_Professionals_Blank_Letterhead_To_Medical_Provider = 13,
            Claims_Professionals_Blank_Letterhead = 14,
            Contact_Letter_Insured_Third_Party_Claim_Pending_Spanish = 15,
            Contact_Letter_Insured_Third_Party_Claim_Pending = 16,
            Copart_Generic_Sell_Letter = 17,
            Copart_KCKS_Sell_Letter = 18,
            Copart_Phoenix_Sell_Letter = 19,
            Copart_Las_Vegas_Sell_Letter = 20,
            Copart_Tucson_Sell_Letter = 21,
            Copart_Wichita_Sell_Letter = 22,
            Corvel_Referal_Form = 23,
            Field_Assignment = 24,
            Fifteen_Day_Notice_To_Close_File_Spanish = 25,
            Fifteen_Day_Notice_To_Close_File = 26,
            Final_Recission_Letter_Return_Premium_Enclosed_Spanish = 27,
            Final_Recission_Letter_Return_Premium_Enclosed = 28,
            First_Recission_Letter_Kansas_Spanish = 29,
            First_Recission_Letter_Kansas = 30,
            General_Denial_Spanish = 31,
            General_Denial = 32,
            Kansas_Notification_To_Party_Keeping_Vehicle = 33,
            Letter_Of_Guarantee = 34,
            Lost_Wage_Form_Return_Cover_Letter = 35,
            Lost_Wage_Form = 36,
            Medical_Authorization_Form_Cover_Letter_Spanish = 37,
            Medical_Authorization_Form_Cover_Letter_Request = 38,
            Medical_Authorization_Form_Cover_Letter = 39,
            Medical_Authorization_Form = 40,
            Nevada_Acknowledge_Rep_Letter_Decline_to_Provide_Limits_Unti = 41,
            Non_Cooperation_Reservation_of_Rights_Spanish = 42,
            Non_Cooperation_Reservation_of_Rights = 43,
            Notice_of_Demand_Spanish = 44,
            Notice_of_Demand = 45,
            Ongoing_Investigation_No_Payments_At_This_Time = 46,
            PIP_I_Am_Or_Am_Not_A_Kansas_Titled_Vehicle_Owner = 47,
            PIP_Acknowledge_Lien_From_Another_Company = 48,
            PIP_Auto_Ownership_Letter_Spanish = 49,
            PIP_Cover_Letter_Spanish = 50,
            PIP_Declination_To_Provider_No_PIP_App_Form_Received = 51,
            PIP_Disability_Follow_Up_Request_Form = 52,
            PIP_Exhaustion_Letter_To_Provider = 53,
            PIP_Kansas_Application_For_Benefits = 54,
            PIP_Request_For_Medical_Records = 55,
            Policy_Certification_Form = 56,
            Policy_Not_In_Force_Spanish = 57,
            Policy_Not_In_Force = 58,
            Power_Of_Attourney = 59,
            Redline_Report_Request_Form = 60,
            Referal_to_Dr_Manfredi = 61,
            Release_of_all_Claims = 62,
            Request_to_Fort_Riley_Military_Police = 63,
            StopPayment_Request_Form = 64,
            Supplement_Letter = 65,
            Third_Party_Claim_Pending = 66,
            To_Insured_Med_Bill_Received_No_Accident_Reported = 67,
            Topeka_Salvage_Pool_Sell_Letter = 68,
            Total_Loss_Notice_To_Dealership = 69,
            Total_Loss_Offer_To_Insured_with_75_Payment_Included = 70,
            Total_Loss_Offer_To_Insured = 71,
            Vehicle_Abandonment_Letter_to_CoPart_Kansas_City = 72,
            Wrongful_Death_and_Survival_Action_Release = 73,
            Copart_Vehicle_Abandonment_Phoenix_AZ = 78,
            Copart_Vehicle_Abandonment_Wichita_KS = 79,
            Copart_Generic_Sell_Letter_2 = 80,
            Copart_Vehicle_Abandonment_Las_Vegas_NV = 81,
            Non_Cooperation_Declination_Of_Coverage = 82,
            PIP_Cover_Letter = 83,
            Material_Misrepresentation_Declination_Kansas_Spanish = 84,
            Material_Misrepresentation_Declination_Kansas_English = 85,
            Police_Notification_Of_Ownership = 87,
            Notice_to_Attorney_NO_Med_Pay_UIM_Documentation_TO_FOLLOW = 89,
            Notice_to_Attorney_NO_Med_Pay_UIM_Documentation_INCLUDED = 90,
            Release_Property_Damage_Claim = 91,
            Non_Cooperation_Reservation_of_Rights_American_Sterling = 92,
            Total_Loss_Offer = 93,
            Excess_Letter_to_Insured = 94,
            Assignment_To_Defense_Counsel = 95
        };

        public enum DraftPrintingPermissionType : int 
        {
            On = 2,
            Off = 3,
            Use_Set_Times = 4
        };

        public enum DraftVoidReason : int 
        {
            Unknown = 0,
            Applied_to_the_Wrong_File = 3,
            Printing_Problem = 4,
            Wrong_Information_On_Draft = 6,
            Paid_Off_Wrong_Reserve_Line = 7,
            Lost_In_Mail = 8,
            Duplicate_Payment = 9,
            Draft_Returned_By_Payee = 10
        };

        public enum EnhancementImportanceType : int 
        {
            Undefined = 0,
            Low = 2,
            Medium = 3,
            High = 4
        };

        public enum EnhancementStatusType : int 
        {
            Undefined = 0,
            Requested = 2,
            Under_Consideration = 3,
            Planned = 4,
            Rejected = 5,
            Completed = 6
        };

        public enum ErrorType : int 
        {
            Database_Error
        };

        public enum EventType : int 
        {
            Claim_Created_Using_Policy = 3,
            Claim_Created_Without_Policy = 4,
            Property_Damage_Added = 105,
            Witness_Passenger_Added = 106,
            Claimant_Added = 142,
            Reserve_Added = 146,
            Injured_Added = 234,
            Subro_Added = 296,
            Salvage_Added = 302,
            Subro_Amount_Greater_Than_Reserve_Loss = 314,
            Salvage_Amount_Greater_Than_Reserve_Loss = 315,
            Draft_Amount_Exceeds_User_Authority = 316,
            Draft_Amount_Exceeds_Per_Person_Coverage = 317,
            Draft_Amount_Exceeds_Per_Accident_Coverage = 318,
            Loss_Reserve_Changed_Above_Adjuster_Authority = 319,
            Expense_Reserve_Changed_Above_Adjuster_Authority = 320,
            Loss_Reserve_Exceeds_Per_Person_Coverage = 321,
            Loss_Reserve_Exceeds_Per_Accident_Coverage = 322,
            Draft_Voided = 323,
            Subro_Voided = 324,
            Salvage_Voided = 325,
            Reserve_Closed = 326,
            Reserve_Reopened = 327,
            Reserve_Closed_Pending_Salvage = 328,
            Reserve_Closed_Pending_Subro = 329,
            Reserve_Voided = 330,
            Claim_Closed = 331,
            Insured_Vehicle_Not_On_Policy = 333,
            Insured_Not_On_Policy = 334,
            NSF_Cancellation = 336,
            Pro_Rate_Cancellation = 337,
            Flat_Cancellation = 338,
            Associate_Claim_With_New_Policy = 340,
            Driver_Is_An_Excluded_Driver = 341,
            Policy_Not_Found = 342,
            Short_Rate_Cancellation = 344,
            Claim_Adjuster_Id_Changed = 374,
            Policy_Now_In_Force_For_Date_Of_Loss = 380,
            Policy_Not_In_Force_For_Date_Of_Loss = 382,
            Claim_Reopened = 383,
            Loss_Reserve_Set_With_No_Coverage = 384,
            Draft_Issued_With_No_Coverage = 385,
            Claim_Updated = 396,
            Claimant_Updated = 430,
            Draft_Is_Printed_Changed = 458,
            Injured_Updated = 463,
            Person_Updated = 468,
            Property_Damage_Updated = 493,
            Vehicle_Updated = 558,
            Witness_Passenger_Updated = 637,
            Insured_Vehicle_Now_on_Policy = 638,
            Insured_Now_On_Policy = 639,
            Driver_Is_Not_An_Excluded_Driver_Anymore = 640,
            Policy_Now_Found = 641,
            Loss_Reserve_Doesnt_Exceed_Per_Person_Coverage_Anymore = 642,
            Loss_Reserve_Doesnt_Exceed_Per_Accident_Coverage_Anymore = 643,
            Draft_Amount_Doesnt_Exceed_Per_Accident_Coverage_Anymore = 645,
            Draft_Amount_Doesnt_Exceed_Per_Person_Coverage_Anymore = 646,
            Personal_Lines_Version_No_Changed = 647,
            NSF_Cancellation_No_Longer_Applies = 664,
            Flat_Cancellation_No_Longer_Applies = 665,
            Short_Rate_Cancellation_No_Longer_Applies = 666,
            Pro_Rate_Cancellation_No_Longer_Applies = 667,
            Loss_Reserve_Changed = 668,
            Expense_Reserve_Changed = 669,
            User_Logon = 674,
            User_Logoff = 675,
            Unlock_Claim = 676,
            Unlock_Reserve = 677,
            Unlock_Transaction = 678,
            Pip_Policy_Periods_Exceeded = 681,
            Transactions_Dont_Sum_to_Zero_on_Reserve = 682,
            Change_To_Policy_in_Progress = 683,
            Holding_Bad_Data_See_Mel = 684,
            Payment_Posting_Error = 685,
            Policy_Has_Expired = 686,
            Policy_Is_Cancelled = 687,
            Policy_Is_Inactive = 688,
            Waiting_Final_Rating = 689,
            Waiting_For_New_Business = 690,
            Change_To_Policy_In_Progress_No_Longer_Applies = 691,
            Holding_Bad_Data_See_Mel_No_Longer_Applies = 692,
            Payment_Posting_Error_No_Longer_Applies = 693,
            Policy_Has_Expired_No_Longer_Applies = 694,
            Policy_Is_Cancelled_No_Longer_Applies = 695,
            Waiting_Final_Rating_No_Longer_Applies = 696,
            Waiting_For_New_Business_No_Longer_Applies = 697,
            Policy_Is_Inactive_No_Longer_Applies = 698,
            Comp_Reserve_Created_Without_Coverage = 699,
            Comp_Coverage_Now_Exists_on_Policy = 701,
            Collision_Coverage_Now_Exists_on_Policy = 702,
            Collision_Reserve_Created_Without_Coverage = 703,
            Driver_Not_On_Policy = 704,
            Driver_Now_On_Policy = 705,
            Manual_Claim_Lock = 706,
            Loss_Draft_Missing_Insured_Or_Loss_Payee = 707,
            NSF_Cancellation_NSF_Check_Register_ = 708,
            NSF_Cancellation_NSF_Check_Register_No_Longer_Applies = 709,
            Phone_Call_Recorded = 710,
            Phone_Call_Updated = 711,
            Physical_Recording_Requested = 712,
            Physical_Recording_Cancelled = 713,
            Physical_Recording_Completed = 714,
            Phone_Call_Deleted = 715,
            Phone_Call_Assigned_To_Claim = 716,
            Phone_Call_Claimant = 717,
            Phone_Call_Insured = 718,
            Phone_Call_Other = 719,
            Appraisal_Task_Inserted = 720,
            Unvoid_Draft = 721,
            Police_Report_Request_Added = 722,
            Choicepoint_Response_Received = 723,
            Appraisal_Response_Inserted = 724,
            Appraisal_Completed = 725,
            Initial_Reserve_Assignment = 727,
            Claim_Closed_With_Outstanding_Tasks = 728,
            Property_Damage_Reserve_Created = 729,
            ABI_Major_Reserve_Created = 730,
            ABI_Minor_Reserve_Created = 731,
            Med_Pay_Reserve_Created = 732,
            UM_Reserve_Created = 733,
            UIM_Reserve_Created = 734,
            PIP_Medical_Reserve_Created = 735,
            PIP_Rehab_Reserve_Created = 736,
            PIP_Wages_Reserve_Created = 737,
            PIP_Essential_Services_Reserve_Created = 738,
            Collision_Reserve_Created = 739,
            Comprehensive_Reserve_Created = 740,
            PIP_Funeral_Reserve_Created = 741,
            First_Contact_Named_Insured_File_Note_Entered = 742,
            First_Contact_Claimant_File_Note_Entered = 743,
            First_Contact_Insured_Driver_File_Note_Entered = 744,
            Liability_Evaluation_File_Note_Entered = 745,
            Damage_Evaluation_File_Note_Entered = 746,
            Property_Damage_Reserve_Created_Without_Coverage = 747,
            Property_Damage_Coverage_Now_Exists_on_Policy = 748,
            Reserve_Assignment = 749,
            Return_Phone_Call_File_Note_Entered = 750,
            Phone_Message_File_Note_Entered = 751,
            Enter_Demand_Received_File_Note = 752,
            Garaging_Address_Different_from_Mailing_Address = 754,
            Loss_Paid_With_At_Fault_Undetermined = 757,
            Document_Needed_Continuing_Investigation = 758,
            Document_Needed_Demand_Acknowledgment = 759,
            Document_Needed_Contact_Insured = 760,
            Diary_Clear_Attempt = 761,
            Appraisal_Cancelled = 762,
            Possible_Duplicate_Claims_Found = 763,
            Property_Added = 764,
            Property_Updated = 765,
            Commercial_General_Liability_BI_Reserve_Created = 771,
            Commercial_General_Liability_PD_Reserve_Created = 772,
            Commercial_Property_Reserve_Created = 773,
            Crime_Reserve_Created = 774,
            Crime_Added = 775,
            Crime_Updated = 776,
            Deductibles_Added = 777,
            Deductibles_Amount_Greater_Than_Reserve_Loss = 778,
            Deductibles_Voided = 779,
            Reserve_Closed_Pending_Deductibles = 780,
            Reserve_Changed_Above_Limit = 781,
            Payment_Reserve_Created_Above_Limit = 782,
            Aggregate_Used_Near_Policy_Limit = 783,
            Aggregate_Used_Over_Policy_Limit = 784,
            SIR_Recovery_Added = 785,
            SIR_Credit_Added = 786,
            SIR_Recovery_Voided = 787,
            SIR_Credit_Voided = 788,
            Reserve_Closed_Pending_SIR_Recovery = 791,
            SIR_Used_Near_Policy_Limit = 792,
            SIR_Used_Over_Policy_Limit = 793,
            SIR_Recovery_Amount_Greater_Than_Reserve_Payments = 794,
            Reserve_Created_Without_Coverage = 795,
            Umbrella_Reserve_Created = 796,
            Received_Demand = 797,
            Offer_Made = 798,
            Litigation_Created = 799,
            Litigation_Updated = 800,
            Salvage_Wait_30 = 805,
            Salvage_Wait_60 = 806,
            Salvage_Wait_90 = 807,
            Vendor_On_Fraud_List = 811,
            Person_On_Fraud_List = 812,
            Vehicle_On_Fraud_List = 813
        };

        public enum FileNoteLevel : int 
        {
            Claim = 2,
            Claimant = 3,
            Reserve = 4,
            Transaction = 5,
            Vehicle = 6
        };

        public enum FileNoteType : int 
        {
            Undefined = 0,
            Memo_to_File = 1,
            First_Contact_Named_Insured = 2,
            First_Contact_Claimant = 3,
            Lock_Reserve = 4,
            Unlock_Reserve = 5,
            Unlock_Claim = 6,
            Unlock_Transaction = 7,
            Claim_Created = 8,
            Adjuster_Assigned = 9,
            First_Identification_of_Possible_Total_Loss = 10,
            Claimant_Medical_Review = 11,
            Diary = 12,
            Property_Damage_Added = 13,
            Witness_Passenger_Added = 14,
            Claimant_Added = 15,
            Reserve_Added = 16,
            Injured_Added = 17,
            Subro_Added = 19,
            Salvage_Added = 20,
            Subro_Greater_than_Loss = 21,
            Salvage_Greater_than_Loss = 22,
            Draft_Exceeds_Authority = 23,
            Draft_Exceeds_Coverage = 24,
            Draft_Voided = 27,
            Subro_Voided = 28,
            Salvage_Voided = 29,
            Reserve_Closed = 30,
            Reserve_Reopened = 31,
            Reserve_Pending_Salvage = 32,
            Reserve_Pending_Subro = 33,
            Reserve_Voided = 34,
            Claim_Closed = 35,
            Insured_Vehicle_not_on_Policy = 36,
            Insured_not_on_Policy = 37,
            NSF_Cancellation = 38,
            Pro_Rate_Cancellation = 39,
            Flat_Cancellation = 40,
            New_Policy = 41,
            Excluded_Driver = 42,
            Policy_Not_Found = 43,
            Short_Rate_Cancellation = 44,
            Policy_Now_in_Force = 45,
            Policy_Not_in_Force = 46,
            Claim_Reopened = 47,
            Draft_With_No_Coverage = 49,
            Claim_Updated = 50,
            Claimant_Updated = 51,
            Draft_Printed = 52,
            Injured_Updated = 53,
            Person_Updated = 54,
            Property_Damage_Updated = 55,
            Vehicle_Updated = 56,
            Witness_Passenger_Updated = 57,
            Insured_Vehicle_Now_on_Policy = 58,
            Insured_Now_on_Policy = 59,
            Not_Excluded_Driver = 60,
            Policy_Now_Found = 61,
            Draft_Doesnt_Exceed_Coverage = 63,
            Version_No_Changed = 64,
            No_NSF_Cancellation = 65,
            No_Flat_Cancellation = 66,
            No_Short_Rate_Cancellation = 67,
            No_Pro_Rate_Cancellation = 68,
            Loss_Reserve_Changed = 69,
            Expense_Reserve_Changed = 70,
            Expense_Reserve_Above_Authority = 71,
            Loss_Reserve_Above_Authority = 72,
            Loss_Reserve_Exceeds_Coverage = 73,
            Expense_Reserve_Exceeds_Coverage = 74,
            Loss_Reserve_Doesnt_Exceed_Coverage = 75,
            Expense_Reserve_Doesnt_Exceed_Coverage = 76,
            Loss_Reserve_With_No_Coverage = 77,
            PIP_Policy_Periods_Exceeded = 78,
            Supervisory_Instructions = 79,
            Transactions_Dont_Sum_to_Zero = 80,
            Change_To_Policy_In_Progress = 81,
            Change_To_Policy_In_Progress_No_Longer_Applies = 82,
            Holding_Bad_Data = 83,
            Holding_Bad_Data_No_Longer_Applies = 84,
            Payment_Posting_Error = 85,
            Payment_Posting_Error_No_Longer_Applies = 86,
            Policy_Expired = 87,
            Policy_Expired_No_Longer_Applies = 88,
            Policy_Cancelled = 89,
            Policy_Cancelled_No_Longer_Applies = 90,
            Policy_Inactive = 92,
            Policy_Inactive_No_Longer_Applies = 93,
            Waiting_Final_Rating = 94,
            Waiting_Final_Rating_No_Longer_Applies = 95,
            Waiting_New_Business = 96,
            Waiting_New_Business_No_Longer_Applies = 97,
            Collision_Coverage_Now_Exists = 98,
            Collision_Reserve_Created_Without_Coverage = 99,
            Comp_Coverage_Now_Exists = 100,
            Comp_Reserve_Created_Without_Coverage = 101,
            Driver_Not_On_Policy = 102,
            Driver_Now_On_Policy = 103,
            Phone_Contact_to_Supervisor_Or_Manager = 104,
            Attempt_Contact_Claimant = 105,
            Attempt_Contact_Insured = 106,
            Manual_Claim_Lock = 107,
            Loss_Draft_Missing_Insured_or_Loss_Payee = 108,
            First_Contact_Insured_Driver_Other_Than_Named_Insured_ = 109,
            Phone_Call_Insured = 110,
            Phone_Call_Claimant = 111,
            Phone_Call_Other = 112,
            Authorization_To_Settle_Claimant_PD = 113,
            Authorization_To_Settle_Insured_PD = 114,
            Authorization_To_Settle_BI_PIP_Med_Pay = 115,
            Phone_Call_Recorded = 116,
            Phone_Call_Updated = 117,
            Phone_Call_Deleted = 118,
            Phone_Call_Assigned_To_Claim = 119,
            Physical_Recording_Requested = 120,
            Physical_Recording_Completed = 121,
            Physical_Recording_Cancelled = 122,
            Follow_up_for_Adjuster_to_Return_Phone_Call = 123,
            Demand_Received = 124,
            Appraisal_Requested = 125,
            Unvoid_Draft = 126,
            Police_Report_Requested = 127,
            NSF_Warning = 128,
            Choicepoint_Response_Received = 129,
            Appraisal_Received = 130,
            Liability_Evaluation = 131,
            Damage_Evaluation = 132,
            Salvage_Status = 133,
            Appraisal_Completed = 134,
            Subrogation = 135,
            Alert_Cleared_From_My_Alerts_Screen = 136,
            Initial_Reserve_Assignment = 138,
            Claim_Closed_with_Outstanding_Tasks = 139,
            Property_Damage_Reserve_Created = 140,
            ABI_Major_Reserve_Created = 141,
            ABI_Minor_Reserve_Created = 142,
            UM_Reserve_Created = 143,
            UIM_Reserve_Created = 144,
            Med_Pay_Reserve_Created = 145,
            PIP_Medical_Reserve_Created = 146,
            PIP_Rehab_Reserve_Created = 147,
            PIP_Wages_Reserve_Created = 148,
            PIP_Essential_Services_Reserve_Created = 149,
            Collision_Reserve_Created = 150,
            Comprehensive_Reserve_Created = 151,
            PIP_Funeral_Reserve_Created = 152,
            Contact_Insured_Task_Cleared = 153,
            Contact_Claimant_Task_Cleared = 154,
            Order_Police_Report_Task_Cleared = 155,
            Evaluate_Liability_Task_Cleared = 156,
            Evaluate_Damages_Task_Cleared = 157,
            Contact_Insured_Driver_Task_Cleared = 158,
            Manually_Assigned_Task_Cleared = 159,
            Rental_Car = 160,
            Property_Damage_Reserve_Created_Without_Coverage = 161,
            Reserve_Assignment = 162,
            Lien = 163,
            Litigation_Opened = 164,
            Salvage_Location = 165,
            Phone_Message = 166,
            Phone_Call_Returned = 167,
            Diary_Date_Insertion = 168,
            Return_Phone_Call_Task_Cleared = 169,
            Garaging_Address_Different_from_Mailing_Address = 170,
            Total_Loss_Evaluation = 171,
            Underwriting_Alert = 172,
            Loss_Paid_With_At_Fault_Undetermined = 173,
            Notify_Insured_of_Demands_Task_Cleared = 174,
            Appraisal_Cancelled = 175,
            Possible_Duplicate_Claims = 176,
            Coverage_Investigation = 178,
            Coverage_Evaluation = 179,
            Cant_Contact_Named_Insured_Notify_Agent = 181,
            Registration_Requested = 182,
            Registration_Received_And_Verified = 183,
            Contact_Adverse_Carrier = 184,
            Discussion_With_Claimant = 185,
            Discussion_With_Insured = 186,
            Commercial_General_Liability_BI_Reserve_Created = 189,
            Commercial_General_Liability_PD_Reserve_Created = 190,
            Commercial_Property_Reserve_Created = 191,
            Crime_Reserve_Created = 192,
            Crime_Added = 193,
            Crime_Updated = 194,
            Deductibles_Added = 195,
            Deductibles_Amount_Greater_Than_Reserve_Loss = 196,
            Deductibles_Voided = 197,
            Reserve_Closed_Pending_Deductibles = 198,
            Reserve_Changed_Above_Limit = 199,
            Payment_Reserve_Created_Above_Limit = 200,
            Aggregate_Used_Near_Policy_Limit = 201,
            Aggregate_Used_Over_Policy_Limit = 202,
            SIR_Recovery_Added = 203,
            SIR_Credit_Added = 204,
            SIR_Recovery_Voided = 205,
            SIR_Credit_Voided = 206,
            Reserve_Closed_Pending_SIR_Recovery = 209,
            SIR_Used_Near_Policy_Limit = 210,
            SIR_Used_Over_Policy_Limit = 211,
            SIR_Recovery_Amount_Greater_Than_Reserve_Payments = 212,
            Reserve_Created_Without_Coverage = 213,
            Property_Added = 214,
            Property_Updated = 215,
            Umbrella_Reserve_Created = 216,
            Adjuster_Review_With_Supervisor = 217,
            Received_Demand = 218,
            Offer_Made = 219,
            Litigation_Created = 220,
            Litigation_Updated = 221,
            Salvage_Wait_30 = 346,
            Salvage_Wait_60 = 347,
            Salvage_Wait_90 = 348,
            Vendor_Changed = 349
        };

        public enum FinalSettlementType : int 
        {
            Settlement,
            Judgement,
            Friendly_Suit,
            Declatory_Judgement
        };

        public enum FoxproTransactionType : int 
        {
            Payment_code_Reimburse_Insured_Their_Deductible = 10,
            Partial_Payment = 11,
            Final_Payment = 12,
            Close_reserves_Without_Payment = 13,
            Supplemental_Payment = 14,
            Subro_Salvage_Refund = 15,
            Open_New_Reserve_for_Coverage_Individual = 16,
            Increase_Reserves = 17,
            Reduce_Reserves_or_Close_With_Payment = 18,
            Ind_Adj_Exp_Invest_Appr_Police_Rept_ = 19,
            Payment_Adjustment_Journal_Entry = 24,
            Salvage = 25,
            Reopen_Reserve_for_Coverage_Individual = 26,
            Med_Exp_No_Suit_Peer_Review_Med_Records_ = 29,
            Payment_Adjustment_Overpayment = 34,
            PIP_Recovery = 35,
            Med_Exp_Suit_Filed_IME_depo_records_ = 39,
            Contriubtion_Indemnification = 45,
            Legal_Exp_File_in_Subro = 49,
            Legal_Exp_Suit_Filed_Atty_fees_court_reporters = 59,
            Legal_Exp_No_Suit_Opinion_Research_Stmnt_ = 69,
            Recovery_Expense_contribution_to_Defa_Costs_ = 79
        };

        public enum InjuryCause : int 
        {
            Undefined = 0,
            Ice = 1,
            Water_Liquid = 2,
            Foreign_Object = 3,
            Cart = 4,
            Cart_Corral = 5,
            Food_Product = 6,
            Mat_Rug = 7,
            Checkstand = 8,
            Oil_Fluid = 9,
            Snow = 10,
            Stockcart = 11,
            Unknown = 12,
            Dog_Food = 13,
            No_Known_Cause = 14,
            Display = 15,
            Fixture = 16,
            Case = 17,
            Shelf = 18,
            Spillage = 19,
            Pallet = 20,
            Pallet_Jack = 21,
            Sidewalk = 22,
            Parking_Lot = 23
        };

        public enum InjuryType : int 
        {
            Undefined = 0,
            Fracture = 1,
            Sprain_Strain = 2,
            Laceration = 3,
            Contusion = 4,
            Ingestion = 5,
            Inhale = 6,
            Bite = 7,
            Twist = 8
        };

        public enum InvestigatingAgencyType : int 
        {
            City_Police,                        //1
            County_Police,                      //2
            Fire_Department,                    //3
            County_Fire_Department,             //4
            County_Sheriff_Department,          //5
            Sheriff_Department,                 //6
            Highway_Patrol_or_State_Police,     //7
            Department_of_Public_Safety,        //8
            Division_of_Motor_Vehicles,         //9
            Township_Fire_Department,           //10
            Township_Police_Department,         //11
            Private_Security,                   //12
            Military_Police,                    //13
            Coroner,                            //14
            Emergency_Medical_Service,          //15
            Bureau_of_Vital_Statistics,         //16
            Environmental_Protection_Agency     //17
        };

        public enum Language : int 
        {
            Undefined = 0,
            English = 2,
            Spanish = 3,
            French = 4,
            German = 5,
            Japanese = 9,
            Swiss = 10,
            Italian = 11
        };

        public enum LitigationStatus : int 
        {
            Open,                   //1
            Closed_With_Settlement, //2
            Closed_With_Judgement,  //3
            Appeal_From_Judgement   //4
        };

        public enum LogMessageType : int 
        {
            Cannot_Connect_to_ChoicePoint_FTP_Site_to_Deliver_Requests = 2,
            Cannot_Connect_to_ChoicePoint_FTP_Site_to_Receive_Files = 3,
            Cannot_Change_Directory_on_ChoicePoint_FTP_Site = 4,
            Received_File_From_ChoicePoint = 5,
            Cannot_Receive_File_From_ChoicePoint = 6,
            Deleted_ChoicePoint_File = 7,
            Cannot_Delete_ChoicePoint_File = 8,
            Importing_ChoicePoint_File_Into_ImageRight = 9,
            Index_File_Imported_Into_ImageRight = 10,
            Created_ChoicePoint_Request_File = 11,
            Cannot_Insert_Appraisal_Response = 12,
            Cannot_Update_Appraisal_Request = 13,
            Importing_SceneAccess_File_Into_ImageRight = 14,
            Cannot_Import_SceneAccess_File_Into_ImageRight = 15,
            SceneAccess_Index_File_Imported_Into_ImageRight = 16,
            Cannot_Import_SceneAccess_Index_File_Into_ImageRight = 17,
            General_Scene_Access_Error = 18,
            Service_Started = 19,
            Service_Stopped = 20,
            Polling_Interval = 21,
            ChoicePoint_Index_File_Imported_Into_ImageRight = 22,
            Delivered_ChoicePoint_Request_File = 23,
            Cannot_Deliver_ChoicePoint_Request_File = 24,
            ChoicePoint_File_Already_Processed = 25,
            Cannot_Import_ChoicePoint_File_Into_ImageRight = 26,
            Cannot_Import_ChoicePoint_Index_File_Into_ImageRight = 28,
            ChoicePoint_Response_Inserted = 29,
            Cannot_Insert_ChoicePoint_Response = 30
        };

        public enum LogonType : int 
        {
            Logon,
            Logoff
        };

        public enum LossLocation : int 
        {
            Undefined = 0,
            Front_End = 1,
            Entrance_Exit = 2,
            Checkout_Lanes = 3,
            Grocery = 4,
            Deli = 5,
            Bakery = 6,
            Floral = 7,
            Parking_Lot = 8,
            Sidewalk = 9,
            Dairy = 10,
            Meat = 11,
            Frozen = 12,
            Produce = 13,
            Bathroom = 14,
            Stockroom = 15,
            Service_Desk = 16,
            At_Home = 17,
            Vehicle = 18,
            // these need to be checked after the live insert for the correct values
            Basement = 0,
            Claimant_Premises = 0,
            Garage = 0,
            Insured_Premises = 0,
            Kitchen = 0,
            Misc = 0,
            Office = 0,
            Outside = 0,
            Shop = 0,
            Storage = 0,
            Vehicle_ = 0
        };

        public enum LossType : int
        {
            Undefined = 0,
            Flood = 1,
            Fire = 2,
            Tornado = 3
        };

        public enum OfferSentType : int 
        {
            Undefined = 0,
            Mail = 1,
            Fax = 2,
            Email = 3,
            Phone = 4
        };

        public enum PendingDraftStatus : int 
        {
            Pending,
            Complete,
            Cancelled
        };

        public enum PeriodType : int 
        {
            Undefined = 0,
            Flat = 1,
            Per_Day = 2,
            Per_Month = 3,
            Per_Year = 4
        };

        public enum PersonType : int 
        {
            Undefined = 0,
            Named_Insured = 1,
            Additional_Driver = 2,
            Excluded_Driver = 3,
            Home_Owner = 4,
            In_Household_Not_Rated = 5,
            Renter = 6,
            Owner = 7,
            Driver = 8,
            Claimant = 9,
            Injured = 10,
            Witness = 11,
            Passenger_IV = 12,
            Passenger_CV = 13
        };

        public enum PhysicalRecordingType : int 
        {
            CD = 2,
            Cassette = 3
        };

        public enum PolicyCoverageType : int 
        {
            Property_Damage = 1,            //01
            Auto_Bodily_Injury = 2,         //02
            Medical_Payments = 3,           //03
            Uninsured_Motorist = 4,         //04
            Underinsured_Motorist = 5,      //05
            Collision = 6,                  //06
            Comprehensive = 7,              //07
            Personal_Injury_Protection = 8, //08
            PIP_Medical = 9,                //09
            PIP_Rehab = 10,                  //10
            PIP_Wages = 11,                  //11
            PIP_Essential_Services = 12,     //12
            PIP_Funeral = 13,                //13
            UM_UIM = 14,                     //14
            Comp_Coll = 15,                  //15
            Glass =16                      //16
        };

        public enum PossibleDuplicateClaimType : int 
        {
            Prior_Claim_on_Policy,
            Match_on_Named_Insured,
            Match_on_Last_Name_and_DOL,
            Match_on_Vehicle_and_DOL
        };

        public enum PropertyType : int 
        {
            Undefined = 0,
            Auto_Property = 2,
            Personal_Property_Other_than_Auto = 3,
            Real_Property = 4
        };

        public enum ReportedByType : int 
        {
            Undefined = 0,
            Claimant = 2,
            Insured = 3,
            Other = 4,
            Claimant_Mail = 5,
            Insured_Mail = 6,
            Other_Mail = 7,
            Atty_Insd_Phone = 8,
            Attorney_For_Insured_By_Mail = 9,
            Attorney_For_Claimant_By_Telephone = 10,
            Attorney_For_Claimant_By_Mail = 11,
            Insurance_Company_For_Claimant_By_Telephone = 12,
            Insurance_Company_For_Claimant_By_Mail = 13,
            Legal_Papers_By_Fax_Or_Mail = 14,
            Claimants_Agent_By_Telephone = 16,
            Claimants_Agent_By_Mail = 17,
            Insureds_Agent_By_Telephone = 18,
            Insureds_Agent_By_Mail = 19,
            Medical_Provider = 20,
            Store_Director = 21,
            Store_Manager = 22,
            Manager = 23,
            Asst_Manager = 24,
            Asst_Director = 25,
            Front_End_Manager = 26,
            Meat_Manager = 27,
            Produce_Manager = 28,
            Bakery_Manager = 29,
            Stocker = 30,
            Cashier = 31,
            Customer_Service = 32,
            Maintenance = 33,
            Sacker = 34,
            Accounting = 35,
            Home_Office = 36,
            Controller = 37,
            Owner = 38,
            Human_Resources = 39,
            // these need to be checked after the live insert for the correct values
            Insured_ = 54,
            Agent = 44,
            Other_ = 43,
            Underwriter = 48
        };

        public enum ReserveStatus : int 
        {
            Open_Reserve = 2,
            Closed_Pending_Subro = 3,
            Closed_Pending_Salvage = 4,
            Closed = 5,
            Void = 7,
            Closed_Pending_Deductibles = 8,
            Closed_Pending_SIR_Recovery = 9
        };

        public enum ReserveType : int 
        {
            Undefined = 0,
            ABI_Major = 2,
            ABI_Minor = 3,
            Medical_Payments = 4,
            Uninsured_Motorist = 5,
            Underinsured_Motorist = 6,
            PIP_Medical = 7,
            PIP_Rehab = 8,
            PIP_Wages = 9,
            PIP_Essential_Services = 10,
            Property_Damage = 11,
            Collision = 12,
            Comprehensive = 13,
            PIP_Funeral = 14,
            UM_UIM = 15,
            Comp_Collision = 16,
            Glass = 17,
            Commercial_General_Liability_BI = 18,
            Commercial_General_Liability_PD = 19,
            Signs = 20,
            Food_Spoilage = 21,
            Equipment_Breakdown_BI = 22,
            Equipment_Breakdown_DirectDamage = 23,
            Employee_Dishonesty = 24,
            Other_Crime = 25,
            OPPF_BI = 26,
            OPPF_DirectDamage = 27,
            Business_Income = 28,
            Commercial_Property_Bldg = 29,
            Commercial_Property_Bus_Pers_Prop = 30,
            Fire_Damage = 31,
            Pers_Adv_Injury = 32,
            Med_Pay = 33,
            Umbrella = 34,
            Weekend_Crime = 35,
            Rents = 36,
            Earthquake = 37,
            Commercial_Glass = 38,
            Property_Plus = 39,
            Hired_Non_Owned_Auto = 93,
            Medical_Expense = 94
        };

        public enum RestrictionType : int 
        {
            Undefined = 0,
            No_Restrictions = 1,
            Claim = 2,
            Reserve = 3,
            Transaction = 4
        };

        public enum State : int 
        {
            Undefined = 0,
            Alabama = 1,
            Arizona = 2,
            Arkansas = 3,
            California = 4,
            Colorado = 5,
            Connecticut = 6,
            Delaware = 7,
            Dist_of_Col = 8,
            Florida = 9,
            Georgia = 10,
            Idaho = 11,
            Illinois = 12,
            Indiana = 13,
            Iowa = 14,
            Kansas = 15,
            Kentucky = 16,
            LOUISIANA = 17,
            MAINE = 18,
            MARYLAND = 19,
            MASSACHUSETTS = 20,
            MICHIGAN = 21,
            MINNESOTA = 22,
            MISSISSIPPI = 23,
            Missouri = 24,
            MONTANA = 25,
            Nebraska = 26,
            Nevada = 27,
            N_HAMPSHIRE = 28,
            NEW_JERSEY = 29,
            NEW_MEXICO = 30,
            NEW_YORK = 31,
            N_CAROLINA = 32,
            N_DAKOTA = 33,
            OHIO = 34,
            Oklahoma = 35,
            OREGON = 36,
            PENNSYLVANIA = 37,
            RHODE_ISLAND = 38,
            S_CAROLINA = 39,
            S_DAKOTA = 40,
            Tennesse = 41,
            TEXAS = 42,
            UTAH = 43,
            VERMONT = 44,
            VIRGINIA = 45,
            WASHINGTON = 46,
            W_VIRGINIA = 47,
            WISCONSIN = 48,
            WYOMING = 49,
            HAWAII = 52,
            ALASKA = 54
        };

        public enum StateDefaultText : int 
        {
            Alabama,
            Arizona,
            Arkansas,
            California,
            Colorado,
            Connecticut,
            Delaware,
            Dist_of_Col,
            Florida,
            Georgia,
            Idaho,
            Illinois,
            Indiana,
            Iowa,
            Kansas,
            Kentucky,
            Louisiana,
            Maine,
            Maryland,
            Massachusetts,
            Michigan,
            Minnesota,
            Mississippi,
            Missouri,
            Montana,
            Nebraska,
            Nevada,
            N_Hampshire,
            New_Jersey,
            New_Mexico,
            New_York,
            N_Carolina,
            N_Dakota,
            Ohio,
            Oklahoma,
            Oregon,
            Pennsylvania,
            Rhode_Island,
            S_Carolina,
            S_Dakota,
            Tennessee,
            Texas,
            Utah,
            Vermont,
            Virginia,
            Washington,
            W_Virginia,
            Wisconsin,
            Wyoming,
            Hawaii,
            Alaska
        };

        public enum SubClaimType : int 
        {
            Auto = 1,
            General_Liability = 2,
            Property_Damage = 3,
            Crime = 4,
            Umbrella = 5,
            LIAB = 6,
            PROP = 7,
            AUTO_ = 8,
            INMR = 10,
            UMB = 12
        };

        public enum SubroSalvageVoidReason : int 
        {
            Unknown = 0,
            Applied_to_Wrong_File = 2,
            NSF_Stop_Payment = 3
        };

        public enum Task : int 
        {
            Undefined = 0,
            Edit_Users = 3,
            Close_Claim = 5,
            Edit_Type_Tables = 7,
            Edit_Vendors = 8,
            Edit_User_Groups = 9,
            Configure_Alerts_Diary_File_Notes = 10,
            Edit_Global_Settings = 12,
            Open_Error_Log = 13,
            Edit_Data_Dictionary = 14,
            Vendor_Type_Sub_Type_Associations = 15,
            Logged_In_Users = 16,
            Kill_Users = 17,
            Event_Viewer = 18,
            Add_Reserve = 19,
            Change_Loss_Reserve = 20,
            Change_Expense_Reserve = 21,
            Reopen_Reserve = 22,
            Close_Reserve = 23,
            Close_Pending_Salvage = 24,
            Close_Pending_Subro = 25,
            Issue_Draft = 26,
            Receive_Subro = 27,
            Receive_Salvage = 28,
            Void_Draft = 29,
            Void_Subro = 30,
            Void_Salvage = 31,
            Modify_Claim_Data = 32,
            Void_Reserve = 33,
            Reissue_Draft = 34,
            Reissue_Subro = 35,
            Reissue_Salvage = 36,
            Reopen_Claim = 37,
            Change_Vendor_Tax_Id_Required = 38,
            Send_Message = 39,
            Staffing_Metrics = 40,
            Recovery_Amounts_Report = 41,
            Edit_Transaction_Type_Conversion = 42,
            Print_Foxpro_Drafts = 43,
            Accounting_Reports = 44,
            Manually_Lock_Claim = 45,
            Set_Average_Reserves = 46,
            Set_Type_Table_Icons = 47,
            SQL_Logging = 48,
            Bulk_Assign_Claims = 49,
            Unvoid_Draft = 50,
            Audit_Claims = 51,
            Log_In_As = 52,
            Resend_Appraisal_Requests = 53,
            Mark_Appraisals_as_Completed = 54,
            Cancel_Appraisal = 55,
            Change_Document_Status = 56,
            Create_New_Document = 57,
            Open_Document = 58,
            Print_Draft_Queue = 59,
            View_Appraiser_Data = 60,
            Wire_Transfer = 61,
            Pending_Drafts = 62,
            Resend_Appraisal_Emails = 63,
            Recover_Deductibles = 65,
            Void_Deductibles = 66,
            Close_Pending_Deductibles = 67,
            Recover_SIR = 68,
            Credit_SIR = 69,
            Void_SIR_Recovery = 70,
            Void_SIR_Credit = 71,
            Close_Pending_SIR_Recovery = 72,
            Edit_Commercial_Limits_and_Deductibles = 73
        };

        public enum TransactionType : int 
        {
            Undefined = 0,
            Open_Reserve = 2,
            Close_Reserve = 3,
            Reserve = 4,
            Reopen_Reserve = 5,
            Partial_Loss_Payment = 6,
            Final_Loss_Payment = 7,
            Independent_Appraiser = 11,
            Independent_Adjuster = 12,
            Legal_Fees = 13,
            Legal_Expenses = 14,
            Experts_Non_Litigation = 15,
            Experts_Litigation = 16,
            Reports = 17,
            Special_Investigations = 18,
            Medical_Records_and_IMEs = 19,
            Salvage_Yards = 20,
            Transcription_Fees = 21,
            Translation_Services = 22,
            Rental_Cars = 23,
            Subro = 24,
            Salvage = 25,
            Void_Draft = 26,
            Void_Subro = 27,
            Void_Salvage = 28,
            Close_Salvage = 29,
            Close_Subro = 30,
            Void_Reserve = 31,
            Supplemental_Reserve = 33,
            Supplemental_Loss_Payment = 34,
            Deductible_Reimbursement = 35,
            Unvoid_Loss_Draft = 36,
            Unvoid_Expense_Draft = 37,
            Wire_Transfer = 38,
            Wire_Transfer_Overpayment = 39,
            Void_Wire_Transfer = 40,
            Void_Wire_Transfer_Overpayment = 41,
            Subro_Expense = 42,
            Deductibles = 43,
            Void_Deductibles = 44,
            Close_Deductibles = 45,
            SIR_Recovery = 46,
            SIR_Credit = 47,
            Void_SIR_Recovery = 48,
            Void_SIR_Credit = 49,
            Close_SIR_Recovery = 50
        };

        public enum UserGroup : int 
        {
            Management = 1,
            IT_Staff = 3,
            Adjusters = 4,
            Clerical = 5,
            Personal_Lines = 6,
            Accounting = 7,
            Bodily_Injury = 8,
            Property_Damage = 9,
            Litigation = 10
        };

        public enum UserStatus : int 
        {
            Undefined = 0,
            Active = 1,
            Inactive = 2
        };

        public enum UserSubType : int 
        {
            Undefined = 0,
            Trainee = 1,
            Apprentice = 2,
            Expert = 3,
            Master = 4
        };

        public enum UserTaskActionType : int 
        {
            Undefined,
            Task_Locked,
            Task_Cleared,
            Task_Created,
            Task_Reassigned
        };

        public enum UserTaskLevel : int 
        {
            Claim = 2,
            Claimant = 3,
            Reserve = 4,
            Transaction = 5,
            None = 6
        };

        public enum UserTaskStatus : int 
        {
            Undefined,
            Task_Created,
            Task_Locked,
            Task_Cleared
        };

        public enum UserTaskType : int 
        {
            Call_Named_Insured = 1,
            Call_Claimant = 2,
            Order_Police_Report = 3,
            Notify_Insured_of_Demands_Offers_and_Any_Potential_Excess = 13,
            Request_Registration = 14,
            Registration_Received_And_Verified = 16
        };

        public enum UserType : int 
        {
            Undefined = 0,
            Manager = 1,
            Administrator = 2,
            Supervisor = 3,
            Adjuster = 4,
            Clerical = 5,
            Commercial_Adjuster = 6,
            Attorney = 7,
            Attorney_Supervisor = 8
        };

        public enum ValidationType : int 
        {
            Undefined = 0,
            SSN = 2,
            Tax_Id = 3,
            Phone_Number = 4,
            Email_Address = 5,
            Zipcode = 6,
            State = 7
        };

        public enum VendorSubType : int 
        {
            Undefined = 0,
            Adjusters_Appraisers_Information = 2,
            Auto_Repair_Services = 3,
            Salvage = 4,
            Tow_Services = 5,
            Collection_Agency_Subro_ = 6,
            Contractors = 7,
            Mortgagees_Leinholders = 8,
            Fire_Departments = 9,
            Insurance_Services = 10,
            Investigation_Detective_Services = 11,
            Attorney_Firm_Information = 12,
            Courts_Municipalities = 13,
            Court_Reporters = 14,
            Ambulance_Service_Information = 15,
            Clinics_Medical_Groups = 16,
            Doctors_Information = 17,
            Hospital_Information = 18,
            Medical_Review = 19,
            Police_Departments = 20,
            Miscellaneous = 21,
            Drive_in_Locations = 22,
            Rental_Agencies_Non_Auto_ = 23,
            Automobile_Rental_Agencies_Information = 24,
            Claimant_Attorney = 25,
            Triers_of_Fact = 26,
            Defense_Attorney = 27,
            Insurance_Company = 28
        };

        public enum VendorType : int 
        {
            Undefined = 0,
            Adjuster_Services = 15,
            Automotive_Services = 16,
            Collection_Services = 17,
            Construction_Services = 18,
            Financial_Services = 19,
            Fire_Departments = 20,
            Insurance_Services = 21,
            Investigative_Services = 22,
            Legal_Services = 23,
            Medical_Services = 24,
            Police_Departments = 25,
            Miscellaneous = 26,
            PCI_Services = 27,
            Rental_Services = 28,
            Claimant_Attorney = 29
        };

        public enum WitnessPassengerLocation : int
        {
            PassengerSeat,
            BackSeatDrivers,
            BackSeatPassenger,
            BackSeatMiddle,
            ThirdRowDrivers,
            ThirdRowPassenger,
            ThirdRowMiddle,
            FrontSeatMiddle,
            OutsideFront,
            OutsideFrontPassenger,
            OutsideFrontDriver,
            OutsideSidePassenger,
            OutsideSideDriver,
            OutsideBackPassenger,
            OutsideBackDriver,
            OutsideBack,
            TruckBedDriverFront,
            TruckBedPassengerFront,
            TruckBedCenterFront,
            TruckBedDriverMiddle,
            TruckBedPassengerMiddle,
            TruckBedCenterMiddle,
            TruckBedDriverBack,
            TruckBedPassengerBack,
            TruckBedCenterBack
        };

        public enum WitnessPassengerType : int 
        {
            Undefined = 0,
            Witness = 1,
            Passenger_IV = 3,
            Passenger_CV = 4,
            Bystander = 5,
            Employee = 6,
            Manager = 7
        };

        public static void MoveTo(this Image target, double newX, double newY)
        {
            var top = Canvas.GetTop(target);
            var left = Canvas.GetLeft(target);
            TranslateTransform trans = new TranslateTransform();
            target.RenderTransform = trans;
            DoubleAnimation anim1 = new DoubleAnimation(top, newY - top, TimeSpan.FromSeconds(10));
            DoubleAnimation anim2 = new DoubleAnimation(left, newX - left, TimeSpan.FromSeconds(10));
            trans.BeginAnimation(TranslateTransform.XProperty, anim1);
            trans.BeginAnimation(TranslateTransform.YProperty, anim2);
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Web;
using MedCsxDatabase;


namespace MedCsxLogic
{
    public class ISOdoc
    {

        private Claim m_Claim = null;
        private User m_User = null;
        private string m_RqUID = "";
        private int idCounter = 0;
        private XElement Addresses;
        private int thisPersonId;
        private List<Hashtable> RefTable = new List<Hashtable>();

        private clsDatabase db = new clsDatabase();

        public ISOdoc() { }
        public ISOdoc(Claim claim, User user)
        {
            m_Claim = claim;
            m_User = user;
            m_RqUID = m_Claim.DisplayClaimId + DateTime.Now.ToString("MMddyyyyhhmmss");
        }

        public XDocument CreateDoc()
        {
            XDocument doc = new XDocument(
                new XElement("ACORD",
                    new XElement("SignonRq",
                        new XElement("SignonPswd",
                            new XElement("SignonRoleCd", "CI"),
                            new XElement("CustId",
                                new XElement("SPName", "iso.com"),
                                new XElement("CustPermId",
                                    new XAttribute("id", "ID1" /*"KEYINSID"*/), "KEYINS")),
                //new XElement("CustLoginId", m_User.ISOuserId)),
                                new XElement("CustPswd",
                                    new XElement("EncryptionTypeCd", "NONE"))),
                //new XElement("Pswd", m_User.ISOuserPswd)),
                //new XElement("GenSessKey", getSessionKey())),
                        new XElement("ClientDt", string.Format("{0:u}", DateTime.Now)),
                        new XElement("CustLangPref", "en-US"),
                        new XElement("ClientApp",
                            new XElement("Org", "ISO"),
                            new XElement("Name", "XML_TEST"),
                            new XElement("Version", "2.0")),
                    new XElement("SuppressNotificationInd", "0"),
                    new XElement("ClaimsSvcRq",
                        new XElement("RqUID",
                            new XAttribute("id", "ID5" /* "KEYINS" + DateTime.Now.ToString("MMddyyy") */), m_RqUID),
                        new XElement("SPName", "iso.com"),
                        new XElement("ClaimInvestigationAddRq",
                            new XElement("RqUID",
                                new XAttribute("id", "ID6"), m_RqUID),
                            new XElement("TransactionRequestDt", string.Format("{0:u}", DateTime.Now)),
                            new XElement("CurCd", "USD"),

                            //getCodeList(), 
                            Producer(),
                            Insured(),
                            PolicyCd(),
                            ClaimOcc(),
                            ClaimsParty(),
                            AdjusterParty(),
                            AutoLoss()
                            )
                    )
                )
            ));

            return doc;
        }

        private XElement AutoLoss()
        {
            int vehicle_id;
            bool qInsuredVehicle = false;
            string owner_id = null;
            string autoLoss_id;
            XElement newDoc = null;
            foreach (Hashtable veh in m_Claim.Vehicles)
            {
                vehicle_id = (int)veh["Vehicle_Id"];
                Vehicle v = new Vehicle(vehicle_id);
                PropertyDamage pd = null;
                foreach (PropertyDamage d in m_Claim.PropertyDamages())
                {
                    if (d.VehicleId == vehicle_id)
                        pd = d;
                }
                if (pd == null)
                    continue;

                autoLoss_id = "ALI" + vehicle_id.ToString();
                foreach (Claimant cl in m_Claim.Claimants)
                {
                    if (cl.PersonId == v.OwnerPerson.Id)
                    {
                        foreach (Hashtable tab in RefTable)
                        {
                            string maskId = cl.Id.ToString();
                            string testId = (string)tab["Id"];
                            if ((testId.Length > maskId.Length) && (testId.Substring(2, maskId.Length) == maskId))
                            {
                                owner_id = testId;
                                break;
                            }
                        }
                    }
                    else if (m_Claim.OwnerPersonId == v.OwnerPerson.Id)
                    {
                        foreach (Hashtable tab in RefTable)
                        {
                            string maskId = m_Claim.OwnerPersonId.ToString();
                            string testId = (string)tab["Id"];
                            if ((testId.Length > maskId.Length) && (testId.Substring(2, maskId.Length) == maskId))
                            {
                                owner_id = testId;
                                qInsuredVehicle = true;
                                break;
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(owner_id))
                        break;
                }

                XElement autoDoc = new XElement(
                    new XElement("AutoLossInfo",
                        new XAttribute("id", autoLoss_id),
                        new XAttribute("ClaimsPartyRefs", owner_id),
                        VehInfo(vehicle_id),
                        new XElement("ManufacturerCd",
                            new XAttribute("codelistref", "NCIC"), v.ManufactureCode),
                        new XElement("ModelCd",
                            new XAttribute("codelistref", "NCIC"), v.ModelCode),
                        new XElement("DamageDesc", pd.DamageDescription)));

                if (newDoc == null)
                    newDoc = autoDoc;
                else
                    newDoc.Add(autoDoc);

            }
            return newDoc;
        }

        private IEnumerable< XElement> VehInfo(int vehicle_id)
        {
            Vehicle v = new Vehicle(vehicle_id);
            XElement newDoc = new XElement("root");
            newDoc.Add(new XElement("Manufacturer", v.Manufacturer));
            newDoc.Add(new XElement("Model", v.Model));
            newDoc.Add(new XElement("ModelYear", v.YearMade));
            newDoc.Add(new XElement("VehIdentificationNumber", v.VIN));

            return newDoc.Elements();
        }

        private XElement AdjusterParty()
        {
            ArrayList adjusters = new ArrayList();

            foreach (Reserve res in m_Claim.Reserves())
            {
                if (!adjusters.Contains(res.AdjusterId.ToString().Trim()))
                    adjusters.Add(res.AdjusterId.ToString().Trim());
            }

            XElement newDoc = null;

            foreach (string adjId in adjusters)
            {
                XElement apDoc = new XElement(
                new XElement("AdjusterParty",
                    new XAttribute("id", "KI" + adjId),
                    AdjPartyInfo(adjId)));
                if (newDoc == null)
                    newDoc = apDoc;
                else
                    newDoc.AddAfterSelf(apDoc);
            }
            return newDoc;
        }

        private XElement AdjPartyInfo(string adjuster_id)
        {
            XElement newDoc = null;

            foreach (Hashtable hRef in RefTable)
            {
                if (newDoc == null)
                    newDoc = getAdjInfo(hRef, adjuster_id);
                else
                    newDoc.Add(getAdjInfo(hRef, adjuster_id));
            }
            return newDoc;
        }

        private XElement getAdjInfo(Hashtable hRef, string adjuster_id)
        {
            XElement newDoc = null;
            if (hRef["Type"] == "Injured")
            {
                foreach (Reserve res in m_Claim.Reserves())
                {
                    if ((res.ReserveTypeId != (int)modGeneratedEnums.ReserveType.Property_Damage) &&
                        (res.ReserveTypeId != (int)modGeneratedEnums.ReserveType.Collision) &&
                        (res.ReserveTypeId != (int)modGeneratedEnums.ReserveType.Comprehensive) &&
                        (res.ReserveTypeId != (int)modGeneratedEnums.ReserveType.Comp_Collision) &&
                        (res.AdjusterId.ToString().Trim() == adjuster_id))
                    {
                        XElement injDoc = new XElement(
                        new XElement("AdjusterPartyInfo",
                            new XAttribute("AssignmentRef", (string)hRef["Id"]),
                            new XElement("AssignedDt", string.Format("{0:u}", m_Claim.AdjusterAssignmentDate)),
                            getCoverageLoss(res)));

                        if (newDoc == null)
                            newDoc = injDoc;
                        else
                            newDoc.AddAfterSelf(injDoc);
                    }
                }

            }
            return newDoc;
        }

        private XElement getCoverageLoss(Reserve res, PropertyDamage pd = null)
        {
            XElement newDoc;
            switch (res.ReserveTypeId)
            {
                case (int)modGeneratedEnums.ReserveType.ABI_Major:
                case (int)modGeneratedEnums.ReserveType.ABI_Minor:
                    newDoc = new XElement("CoverageCd", "BODI");
                    newDoc.AddAfterSelf(new XElement("LossCauseCd", "BODI"));
                    break;
                case (int)modGeneratedEnums.ReserveType.Medical_Payments:
                    newDoc = new XElement("CoverageCd", "MPAY");
                    newDoc.AddAfterSelf(new XElement("LossCauseCd", "MPAY"));
                    break;
                case (int)modGeneratedEnums.ReserveType.Uninsured_Motorist:
                    newDoc = new XElement("CoverageCd", "UNIN");
                    newDoc.AddAfterSelf(new XElement("LossCauseCd", "BODI"));
                    break;
                case (int)modGeneratedEnums.ReserveType.Underinsured_Motorist:
                    newDoc = new XElement("CoverageCd", "UNDR");
                    newDoc.AddAfterSelf(new XElement("LossCauseCd", "BODI"));
                    break;
                case (int)modGeneratedEnums.ReserveType.PIP_Essential_Services:
                case (int)modGeneratedEnums.ReserveType.PIP_Funeral:
                case (int)modGeneratedEnums.ReserveType.PIP_Medical:
                case (int)modGeneratedEnums.ReserveType.PIP_Rehab:
                case (int)modGeneratedEnums.ReserveType.PIP_Wages:
                    newDoc = new XElement("CoverageCd", "NFLT");
                    newDoc.AddAfterSelf(new XElement("LossCauseCd", "NFLT"));
                    break;
                case (int)modGeneratedEnums.ReserveType.Collision:
                case (int)modGeneratedEnums.ReserveType.Comprehensive:
                case (int)modGeneratedEnums.ReserveType.Property_Damage:
                    if (pd.PropertyTypeId == (int)modGeneratedEnums.PropertyType.Auto_Property)
                    {
                        newDoc = new XElement("CoverageCd", "PRPD");
                        newDoc.AddAfterSelf(new XElement("LossCauseCd", "OTAU"));
                    }
                    else
                    {
                        newDoc = new XElement("CoverageCd", "PRPD");
                        newDoc.AddAfterSelf(new XElement("LossCauseCd", "PRPD"));
                    }
                    break;
                default:
                    newDoc = null;
                    break;
            }
            return newDoc;
        }

        private IEnumerable<XElement> ClaimsParty()
        {
            XElement newDoc = new XElement("root");

            foreach (Claimant cl in m_Claim.Claimants)
            {
                    newDoc.Add(ClaimantParty(cl));
            }

            foreach (WitnessPassenger wp in m_Claim.WitnessPassengers)
            {
                    newDoc.Add(ClaimantParty(wp));
            }

                newDoc.Add(ClaimantParty(m_Claim.InsuredPerson));
            return newDoc.Elements();
        }

        private XElement ClaimantParty(object cp)
        {
            string party = "";
            string myId = "";
            Hashtable myRefs = new Hashtable();
            myRefs.Add("Element", "ClaimantParty");

            if (cp.GetType() == typeof(Claimant))
            {
                party = "C";
                myId = "CL" + ((Claimant)cp).Id + idCounter++;
                this.thisPersonId = ((Claimant)cp).PersonId;
                myRefs.Add("Type", "Claimant");
                myRefs.Add("Id", myId);
            }
            else if (cp.GetType() == typeof(WitnessPassenger))
            {
                party = "W";
                myId = "WP" + ((WitnessPassenger)cp).Id + idCounter++;
                this.thisPersonId = ((WitnessPassenger)cp).PersonId;
                myRefs.Add("Type", "Witness");
                myRefs.Add("Id", myId);
            }
            else
            {
                party = "I";
                myId = "IN" + ((Person)cp).Id + idCounter++;
                this.thisPersonId = ((Person)cp).Id;
                myRefs.Add("Type", "Insured");
                myRefs.Add("Id", myId);
            }
            RefTable.Add(myRefs);

            XElement newDoc = new XElement("ClaimsParty",
                   new XAttribute("id", myId),
                   new XElement("ClaimsPartyInfo",
                       new XElement("ClaimsPartyRoleCd",
                           new XAttribute("codelistref", "ClaimsPartyRoleCdList"), getRole(cp)),
                       CloseDate()),
                    new XElement("ClaimsDriverInfo",
                        DLInfo(),
                        DrAtFault())

            );

            foreach (Injured inj in m_Claim.Injured())
                newDoc.Add(ClInjured(inj));

            return newDoc;
        }

        private XElement ClInjured(Injured inj)
        {
            XElement injDoc = null;
            if (inj.PersonId == this.thisPersonId)
            {
                string injId = "INJ" + inj.InjuredId + idCounter++;
                Hashtable myRefs = new Hashtable();
                myRefs.Add("Element", "Injured");
                myRefs.Add("Type", "Injured");
                myRefs.Add("Id", injId);
                injDoc = new XElement("ClaimsInjuredInfo",
                        new XAttribute("id", injId),
                        new XElement("EventsDesc", XMLHelper(inj.ExtentOfInjury))
                        );

                foreach (ICDCode code in inj.ICDCodes())
                    injDoc.Add(ICDDiagnostic(code));


                XElement clmInjDoc = new XElement("ClaimsInjury");
                foreach (ICDCode vcode in inj.VCodes())
                    clmInjDoc.Add(ICDDiagnostic(vcode, 1));
                clmInjDoc.Add(new XElement("InjuryNatureDesc", inj.InjuryDescription));

                if (inj.ISO_InjuredBodyParts().Rows.Count > 0)
                {
                    string bodyPartDescription = "";
                    clmInjDoc.Add(BodyParts(inj, ref bodyPartDescription));
                    XElement bpdDoc = new XElement("BodyPartDesc", bodyPartDescription);
                    clmInjDoc.Add(bpdDoc);
                }

                //ClaimsInjury(inj)
                injDoc.Add(clmInjDoc);
            }
            return injDoc;
        }

        //private XElement ClaimsInjury(Injured inj)
        //{
            //XElement newDoc = ICDDiagnostic(inj, 1);
            //if (!string.IsNullOrWhiteSpace(inj.InjuryDescription))
            //{
            //    if (newDoc != null)
            //        newDoc.AddAfterSelf(new XElement("InjuryNatureDesc", inj.InjuryDescription));
            //    else
            //        newDoc = 
            //}

            //if (newDoc != null)
            //    newDoc.AddAfterSelf(BodyParts(inj));
            //else
            //    newDoc = BodyParts(inj);
            //return newDoc;
        //}

        private XElement BodyParts(Injured inj, ref string bodyPartDescription)
        {
            string lastBodyPart = "";
            if (inj.ISO_InjuredBodyParts().Rows.Count <= 0)
                return null;


            XElement newDoc = null;
            foreach (DataRow dr in inj.ISO_InjuredBodyParts().Rows)
            {
                if (lastBodyPart == (string)dr.ItemArray[0])
                    continue;

                XElement bpDoc = new XElement("BodyPartCd", (string)dr.ItemArray[0]);
                if (string.IsNullOrWhiteSpace(bodyPartDescription))
                    bodyPartDescription = (string)dr.ItemArray[1];

                if (newDoc == null)
                    newDoc = bpDoc;
                else
                    newDoc.AddAfterSelf(bpDoc);

                lastBodyPart = (string)dr.ItemArray[0];
            }


            return newDoc;
        }

        //private XElement ICDDiagnostic(Injured inj, int mode = 0)
        private XElement ICDDiagnostic(ICDCode icd, int mode = 0)
        {
            XElement icdDoc;

            switch (mode)
            {
                case 1:
                    //foreach (ICDCode icd in inj.VCodes())
                    //{
                    icdDoc = new XElement("InjuryNatureCd",
                            new XAttribute("codelistref", "ICD-10"),
                            icd.Code);
                    //    if (newDoc == null)
                    //        newDoc = icdDoc;
                    //    else
                    //        newDoc.AddAfterSelf(icdDoc);
                    //}
                    break;
                default:
                    //foreach (ICDCode icd in inj.ICDCodes())
                    //{
                    icdDoc = new XElement("ICDDiagnosticCd",
                            new XAttribute("codelistref", "ICD-10"),
                            icd.Code);
                    //    if (newDoc == null)
                    //        newDoc = icdDoc;
                    //    else
                    //        newDoc.AddAfterSelf(icdDoc);
                    //}
                    break;
            }
            return icdDoc;
        }

        private XElement DrAtFault()
        {
            if ((thisPersonId == m_Claim.DriverPersonId) && (m_Claim.AtFaultTypeId == (int)modGeneratedEnums.AtFaultType.Yes))
            {
                XElement newDoc = new XElement("OperatorAtFaultInd", "1");
                return newDoc;
            }
            return null;
        }

        private XElement DLInfo()
        {
            Person p = new Person(this.thisPersonId);
            Hashtable myRefs = new Hashtable();

            if (!string.IsNullOrWhiteSpace(p.DriversLicenseNo))
            {
                string dlId = "DL" + p.Id + idCounter++;
                myRefs.Add("Element", "DriversLicense");
                myRefs.Add("Type", this.thisPersonId.ToString());
                myRefs.Add("Id", dlId);

                XElement dlInfo = new XElement("DriversLicense",
                        new XAttribute("id", dlId),
                        new XElement("DriversLicenseNumber", p.DriversLicenseNo),
                        new XElement("LicenseClassCd", p.DriversLicenseClass),
                        new XElement("StateProvCd", p.DriversLicenseState));
                return dlInfo;
            }
            return null;
        }

        private XElement CloseDate()
        {
            if (m_Claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
            {
                XElement newDoc = new XElement("ClosedDt", string.Format("{0:u}", m_Claim.CloseDate));
                return newDoc;
            }
            return null;
        }

        private string getRole(object cp)
        {
            int cpId = 0;
            if (cp.GetType() == typeof(Claimant))
            {
                cpId = ((Claimant)cp).PersonId;
            }
            else if (cp.GetType() == typeof(WitnessPassenger))
            {
                cpId = ((WitnessPassenger)cp).PersonId;
            }
            else if (cp.GetType() == typeof(Vendor))
            {
                cpId = ((Vendor)cp).Id;
            }
            else
            {
                cpId = ((Person)cp).Id;
            }

            if (cpId == m_Claim.InsuredPersonId)
            {
                if (cpId == m_Claim.DriverPersonId)
                    return "ID";    //Insured Driver
                else if (cpId != m_Claim.OwnerPersonId)
                    return "IP";    //Insured Passenger
                else
                {
                    foreach (Claimant c in m_Claim.Claimants)
                    {
                        if (cpId == c.Id)
                            return "CI";    //Both Claimant and Insured
                    }
                    return "IN";        //Insured
                }
            }

            foreach (Claimant c in m_Claim.Claimants)
            {
                if (cpId == c.PersonId)
                {
                    foreach (WitnessPassenger wp in m_Claim.WitnessPassengers)
                    {
                        if (cpId == wp.PersonId)
                            return "CP";        //Claimant Passenger
                    }

                    foreach (PropertyDamage pd in m_Claim.PropertyDamages())
                    {
                        if (cpId == pd.DriverPersonId)
                            return "CD";        //Claimant Driver
                    }

                    if (m_Claim.AccidentTypeId == (int)modGeneratedEnums.AccidentType.Insured_Hit_Pedestrian)
                        return "CE";        //Claimant Pedestrian
                    return "CL";        //Claimant
                }
            }

            foreach (WitnessPassenger wp in m_Claim.WitnessPassengers)
            {
                if (cpId == wp.PersonId)
                    return "WT";        //Witness
            }

            if (m_Claim.Injured().Count > 0)
            {
                foreach (Injured inj in m_Claim.Injured())
                {
                    foreach (BITreatment bit in inj.BIEvaluation().BITreatments())
                    {
                        if (cpId == bit.TreatingDoctorId)
                        {
                            Vendor v = new Vendor(cpId);
                            if (v.VendorSubType().VendorTypeId == (int)modGeneratedEnums.VendorType.Medical_Services)
                            {
                                if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Clinics_Medical_Groups)
                                    return "MK";        //Medical Clinic
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Doctors_Information)
                                    return "MD";        //Medical Doctor
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Hospital_Information)
                                    return "MF";        //Hospital
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Medical_Review)
                                    return "MR";        //Laboratory
                                /**** need to add
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Chiropractor_Information)
                                    return "MC";        //Chiropractor
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Radiologist_Information)
                                    return "MG";        //Radiologist
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Laboratory_Information)
                                    return "MR";        //Laboratory
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Dentist_Information)
                                    return "MS";        //Dentist
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Physical_Therapy_Information)
                                    return "MT";        //Physical Therapy
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.XRay_Information)
                                    return "MX";        //XRay
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Other_Medical_Provider_Information)
                                    return "MO";        //Other Medical Provider
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Osteopath_Information)
                                    return "OS";        //Osteopath
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Pharmacy_Information)
                                    return "PH";        //Pharmacy
                                else if (v.VendorSubTypeId == (int)modGeneratedEnums.VendorSubType.Psychologist_Information)
                                    return "PS";        //Psychologist
                                ****/
                            }
                        }
                    }
                }
            }
            return "OW";
        }

        private XElement ClaimOcc()
        {
            string truncatedDesc = m_Claim.LossDescription;
            string truncatedRemain = "";
            if (System.Text.ASCIIEncoding.UTF8.GetByteCount(XMLHelper(m_Claim.LossDescription)) <= 50)
                truncatedDesc = Truncate2Bytes(XMLHelper(m_Claim.LossDescription), 50, out truncatedRemain);
            XElement newDoc = new XElement("ClaimsOccurrence",
                    new XAttribute("id", m_Claim.DisplayClaimId),
                    new XElement("PolicyRenewalInd", "Y"),
                    new XElement("ClaimStatusCd", retClaimStatus(m_Claim.ClaimStatusId)),
                    new XElement("com.iso_PoliceReportFiledInd", m_Claim.PoliceReportNumber != "" ? "1" : "0"),
                    new XElement("LossDt", m_Claim.DateOfLoss.ToString("yyyy-MM-dd")),
                    new XElement("LossTime", m_Claim.DateOfLoss.ToString("hh:mm")),
                    new XElement("IncidentDesc", truncatedDesc),
                    new XElement("WhereOccurredDesc",
                        new XAttribute("id", getAddress("LOC", 2, XMLHelper(m_Claim.LossLocation), XMLHelper(m_Claim.LossCity), m_Claim.LossState.Abbreviation, "")),
                        XMLHelper(m_Claim.WhereSeen)),
                //ProbableAmount(),
                    new XElement("com.iso_SingleVehInvolvedInd", m_Claim.Vehicles.Count > 1 ? "Y" : "N"),
                    new XElement("com.iso_PhantomVehInd", Unknowns(m_Claim.Vehicles)),
                    new XElement("com.iso_AccidentWitnessedInd", m_Claim.WitnessPassengers.Count > 0 ? "Y" : "N"),
                    new XElement("com.iso_NewExtLossDesc", truncatedRemain));

            return newDoc;
        }

        private string Truncate2Bytes(string p1, int p2, out string p3, int p4 = 200)
        {
            for (Int32 i = p1.Length - 1; i >= 0; i--)
            {
                if (System.Text.Encoding.UTF8.GetByteCount(p1.Substring(0, i + 1)) <= p2)
                {
                    p3 = p1.Substring(i, p1.Length - i);
                    return p1.Substring(0, i);
                }
            }
            string p5 = "";
            p3 = Truncate2Bytes(p1, p4, out p5);
            return string.Empty;
        }

        private string Unknowns(List<Hashtable> list, int mode = 0)
        {
            switch (mode)
            {
                case 0: //Vehicle
                    foreach (Hashtable v in list)
                    {
                        string vName = (string)v["Manufacturer"];
                        if ((vName.Length >= 7) && (vName.Substring(0, 7).ToUpper() == "UNKNOWN"))
                            return "Y";
                    }
                    break;
                default:
                    break;
            }
            return "N";
        }

        private string XMLHelper(string p)
        {
            string ret = p.Replace("&", "&amp;");
            ret = ret.Replace("<", "&lt;");
            ret = ret.Replace(">", "&gt;");
            ret = ret.Replace("\"", "&quot;");
            ret = ret.Replace("'", "&#39;");

            return ret;
        }

        private string retClaimStatus(int p)
        {
            switch (p)
            {
                case (int)modGeneratedEnums.ClaimStatus.Claim_Closed:
                    return "C";
                case (int)modGeneratedEnums.ClaimStatus.Claim_Open:
                case (int)modGeneratedEnums.ClaimStatus.Claim_Open_Without_Policy:
                case (int)modGeneratedEnums.ClaimStatus.Claim_Reopened:
                    return retReserveStatus();
                default:
                    return "CWP";
            }
        }

        private string retReserveStatus()
        {
            foreach (Reserve res in m_Claim.Reserves())
            {
                switch (res.ReserveStatusId)
                {
                    case (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage:
                        return "SAL";
                    case (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro:
                        return "SUB";
                }
            }
            return "O";
        }

        private XElement PolicyCd()
        {
            PolGeneralInfo policyInfo = m_Claim.Policy();

            if (policyInfo == null)
                return null;

            XElement newDoc = new XElement("Policy",
                    new XAttribute("id", "ID508"),
                    new XElement("PolicyNumber",
                        new XAttribute("id", "ID509"), policyInfo.PolicyNo),
                    new XElement("PolicyVersion",
                        new XAttribute("id", "ID510"), policyInfo.VersionNo),
                    new XElement("LOBCd", "PAPP"),
                    new XElement("ContractTerm",
                        new XElement("EffectiveDt", string.Format("{0:u}", policyInfo.dtEff)),
                        new XElement("ExpirationDt", string.Format("{0:u}", policyInfo.dtExp))),
                    new XElement("Binder",
                            new XElement("AgencyBinderNumber", "Agency Binder Number 1"),
                            new XElement("ContractTerm",
                        new XElement("EffectiveDt", policyInfo.dtEff.ToString("yyyy-MM-dd")),
                        new XElement("ExpirationDt", policyInfo.dtExp.ToString("yyyy-MM-dd"))),
                        new XElement("EffectiveDt", policyInfo.dtEff.ToString("yyyy-MM-dd")),
                        new XElement("ExpirationDt", policyInfo.dtExp.ToString("yyyy-MM-dd"))),
                    TotalPaidLoss(),
                    new XElement("NumLosses", ""),
                    new XElement("NumLossesYrs", ""),
                    new XElement("AccountNumberId", ""),
                    new XElement("AssignedRiskInd", "1"));

            return newDoc;
        }

        private XElement TotalPaidLoss()
        {
            double ttlPaidLoss = 0;
            string sql = "SELECT SUM(Paid_Loss) FROM Reserve WHERE Claim_Id in ";
            sql += "(SELECT Claim_Id FROM Claim WHERE Policy_No='" + m_Claim.PolicyNo + "')";
            decimal ttlPaid = (decimal)db.ExecuteSelectReturnValue(sql);
            ttlPaidLoss = (double)ttlPaid;

            if (ttlPaidLoss > 0)
            {
                XElement newDoc = new XElement("TotalPaidLossAmt",
                        new XElement("Amt", ttlPaidLoss.ToString()),
                        new XElement("CurCd", "USD"));

                return newDoc;
            }
            return null;
        }

        private XElement Insured()
        {
            Person m_Insured = m_Claim.InsuredPerson;
            if (m_Insured == null)
                return null;

            List<Hashtable> polInsured = db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Individuals", m_Claim.PolicyNo, m_Claim.VersionNo);
            if (polInsured == null)
                return null;
            if (polInsured.Count > 0)
            {
                Hashtable polIns = polInsured[0];

                XElement newDoc = new XElement("InsuredOrPrincipal",
                        new XElement("InsuredOrPrincipalInfo",
                            new XElement("PersonInfo",
                                new XElement("GenderCd", m_Insured.Sex),
                                new XElement("BirthDt", string.Format("{0:u}", m_Insured.DateOfBirth)),
                                new XElement("MaritalStatusCd", (string)polIns["Stat_Martl"] == null ? "" : (string)polIns["Stat_Martl"]),
                                new XElement("MiscParty",
                                    new XElement("ItemIdInfo",
                                        new XElement("AgencyId", "Something from ISO")),
                                    new XElement("MiscPartyInfo",
                                        new XElement("MiscPartyRoleCd", "CarrierInsurer"))))));

                return newDoc;
            }
            else
                return new XElement("InsuredOrPrincipal");
        }

        private XElement Producer()
        {
            XElement newDoc = new XElement(
               new XElement("Producer",
                   new XAttribute("id", "ID73"),
                   new XElement("GeneralPartyInfo",
                       new XAttribute("id", "ID74"),
                       new XElement("NameInfo",
                           new XAttribute("id", "ID75"),
                           new XElement("CommlName",
                                new XAttribute("id", "ID76"),
                                new XElement("CommercialName",
                                    new XAttribute("id", "ID77"), "Key Insurance Co"),
                                new XElement("SupplementaryNameInfo",
                                    new XAttribute("id", "ID79"),
                                    new XElement("SupplementaryNameCd",
                                        new XAttribute("codelistref", "ID17"),
                                        new XAttribute("id", "ID80"), "MJ"),
                                    new XElement("SupplementaryName",
                                        new XAttribute("id", "ID81"), "Med James, Inc"))),
                           new XElement("LegalEntityCd",
                                new XAttribute("codlistref", "ID20"),
                                new XAttribute("id", "ID88"), "MJI"),
                             new XElement("TaxIdentity",
                                new XAttribute("id", "ID89"),
                                new XElement("TaxIdTypeCd",
                                   new XAttribute("codlistref", "TaxIdTypeCd"),
                                   new XAttribute("id", "ID90"), "TIN"),
                                new XElement("TaxId",
                                   new XAttribute("id", "ID91"), "208638622"),
                                new XElement("StateProvCd",
                                   new XAttribute("id", "ID93"), "KS"),
                                new XElement("StateProv",
                                   new XAttribute("id", "ID94"), "KANSAS"),
                                new XElement("County",
                                   new XAttribute("id", "ID97"), "JOHNSON"))),
                        AddressCollection(),
                   new XElement("ProducerInfo",
                       new XAttribute("id", "ID451"),
                       new XElement("ContractNumber",
                            new XAttribute("id", "ID452"), "ContractNumber1"),
                       new XElement("ProducerSubCode",
                            new XAttribute("id", "ID453"), "ProducerSubCode1"),
                       new XElement("PlacingOffice",
                            new XAttribute("id", "ID454"), "PlacingOffice1")))));
            return newDoc;
        }

        private XElement AddressCollection()
        {
            string myId = getAddress("P", 0, "", "", "", "", "", 1);
            return Addresses;
        }

        private XElement getCodeList()
        {
            XElement codeList = null;
            for (int i = 0; i < 3; i++)
            {
                if (codeList == null)
                    codeList = generateCodeList(i);
                else
                    codeList.AddAfterSelf(generateCodeList(i));
            }
            return codeList;
        }

        private XElement generateCodeList(int i)
        {
            XElement newDoc = new XElement("CodeList",
                    new XAttribute("id", i),
                    new XElement("CodeListName",
                        new XAttribute("id", "ID10"), "CodeListName"),
                    new XElement("CodeListVersion",
                        new XAttribute("id", "ID11"), "CodeListVersion"),
                    new XElement("CodeListOwnerCd",
                        new XAttribute("codelistref", "ID1"),
                        new XAttribute("id", "ID12"), "ISOUS"),
                    new XElement("Description",
                        new XAttribute("id", "ID13"), "Description"),
                    new XElement("WebsiteURL",
                        new XAttribute("id", "ID14"), "WebsiteURL"));
            return newDoc;
        }

        private string getAddress(string ind, int addressType, string address1, string city, string state, string zipCode, string address2 = "", int mode = 0)
        {
            int counter = idCounter++;
            string id = ind + counter.ToString();
            XElement newDoc;
            if (mode == 1)
            {
                newDoc = new XElement("Addr",
                        new XAttribute("id", id),
                        new XElement("AddrTypeCd", "MailingAddress"),
                        new XElement("Addr1", "P.O. Box 2014"),
                        new XElement("City", "Shawnee Mission"),
                        new XElement("StateProvCd", "KS"),
                        new XElement("PostalCode", "66201"));
            }
            else
            {
                newDoc = new XElement("Addr",
                        new XAttribute("id", id),
                        new XElement("AddrTypeCd", addressType == 1 ? "MailingAddress" : "PhysicalRisk"),
                        new XElement("Addr1", address1),
                        new XElement("City", city),
                        new XElement("StateProvCd", state),
                        new XElement("PostalCode", zipCode));
            }
            if (Addresses == null)
                Addresses = newDoc;
            else
                Addresses.AddAfterSelf(newDoc);
            return id;
        }

        private string getSessionKey()
        {
            return SessionKey;
        }

        private string SessionKey
        {
            get
            {
                return SessionVar.GetString("Session_Key");
            }
            set
            {
                SessionVar.SetString("Session_Key", value);
            }
        }
    }

    public class SessionVar
    {
        static System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ApplicationException("No Http Context, No Session to Get!");

                return HttpContext.Current.Session;
            }
        }

        public static T Get<T>(string key)
        {
            if (Session[key] == null)
                return default(T);
            else
                return (T)Session[key];
        }

        public static void Set<T>(string key, T value)
        {
            Session[key] = value;
        }

        public static string GetString(string key)
        {
            string s = Get<string>(key);
            return s == null ? string.Empty : s;
        }

        public static void SetString(string key, string value)
        {
            Set<string>(key, value);
        }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UnsavedFileNoteDiary : MedCsXTable
    {

        #region Constructors
        public UnsavedFileNoteDiary() : base() { }
        public UnsavedFileNoteDiary(int id) : base(id) { }
        public UnsavedFileNoteDiary(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Unsaved_File_Note_Diary"; }
        }

        public int UnsavedFileNoteDiaryId
        {
            get { return (int)row["Unsaved_File_Note_Diary_Id"]; }
            set { this.setRowValue("Unsaved_File_Note_Diary_Id", value); }
        }

        public int UnsavedFileNoteId
        {
            get { return (int)row["Unsaved_File_Note_Id"]; }
            set { this.setRowValue("Unsaved_File_Note_Id", value); }
        }

        public string UserName
        {
            get { return (string)row["User_Name"]; }
            set { this.setRowValue("User_Name", value); }
        }
        #endregion
}
}

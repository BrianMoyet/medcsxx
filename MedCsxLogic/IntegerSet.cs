﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class IntegerSet
    {

        #region Class Variables
        private Dictionary<int, int> myCollection = new Dictionary<int, int>();
        #endregion

        #region Properties
        private Dictionary<int, int> Collection
        {
            get { return this.myCollection; }
            set { this.myCollection = value; }
        }
        #endregion

        #region Constructors
        public IntegerSet() { }
        #endregion

        #region Methods
        public void Add(int i)
        {
            if (!this.Collection.ContainsKey(i))
                this.Collection.Add(i, 1);
        }

        public void Add(ArrayList a)
        {
            foreach (int i in a)
            {
                this.Add(i);
            }
        }

        public void Add(IntegerSet s)
        {
            foreach (int i in s.ToList())
            {
                this.Add(i);
            }
        }

        public void Add(List<int> list)
        {
            foreach (int i in list)
            {
                this.Add(i);
            }
        }

        public bool Contains(int i)
        {
            return this.Collection.ContainsKey(i);
        }

        public List<int> ToList()
        {
            List<int> a = new List<int>();

            foreach (int i in this.Collection.Keys)
            {
                a.Add(i);
            }
            return a;
        }
        #endregion
    }
}

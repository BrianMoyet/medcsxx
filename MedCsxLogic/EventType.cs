﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom.Compiler;
using System.Reflection;
using MedCsxDatabase;
using System.Web;

namespace MedCsxLogic
{
    public class EventType:TypeTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public EventType() : base() { }
        public EventType(int id) : base(id) { }
        public EventType(string description) : base(description) { }
        public EventType(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Event_Type";
            }
        }

        #region Column Names
        public bool areAlertsDeferred
        {
            get { return (bool)row["Are_Alerts_Deferred"]; }
            set { this.setRowValue("Are_Alerts_Deferred", value); }
        }

        public bool CheckForExistingLock
        {
            get { return row["Check_For_Existing_Lock"] == null ? false : (bool)row["Check_For_Existing_Lock"]; }
            set { this.setRowValue("Check_For_Existing_Lock", value); }
        }

        public int ClearsDiaryTypeId
        {
            get { return row["Clears_Diary_Type_Id"] == null ? 0 : (int)row["Clears_Diary_Type_Id"]; }
            set { this.setRowValue("Clears_Diary_Type_Id", value); }
        }

        public int ClearsFileNoteTypeId
        {
            get { return row["Clears_File_Note_Type_Id"] == null ? 0 : (int)row["Clears_File_Note_Type_Id"]; }
            set { this.setRowValue("Clears_File_Note_Type_Id", value); }
        }

        public int DiaryDueDateOffset
        {
            get { return row["Diary_Due_Date_Offset"] == null ? 0 : (int)row["Diary_Due_Date_Offset"]; }
            set { this.setRowValue("Diary_Due_Date_Offset", value); }
        }

        public int DiaryTypeId
        {
            get { return row["Diary_Type_Id"] == null ? 0 : (int)row["Diary_Type_Id"]; }
            set { this.setRowValue("Diary_Type_Id", value); }
        }

        public int EventTypeId
        {
            get { return row["Event_Type_Id"] == null ? 0 : (int)row["Event_Type_Id"]; }
            set { this.setRowValue("Event_Type_Id", value); }
        }

        public string FileNoteScript
        {
            get { return (string)row["File_Note_Script"]; }
            set { this.setRowValue("File_Note_Script", value); }
        }

        public string FileNoteScriptLanguage
        {
            get { return (string)row["File_Note_Script_Language"]; }
            set { this.setRowValue("File_Note_Script_Language", value); }
        }

        public int FileNoteTypeId
        {
            get { return row["File_Note_Type_Id"] == null ? 0 : (int)row["File_Note_Type_Id"]; }
            set { this.setRowValue("File_Note_Type_Id", value); }
        }

        public string GeneralScript
        {
            get { return (string)row["General_Script"]; }
            set { this.setRowValue("General_Script", value); }
        }

        public int GeneralScriptLanguage
        {
            get { return row["General_Script_Language"] == null ? 0 : (int)row["General_Script_Language"]; }
            set { this.setRowValue("General_Script_Language", value); }
        }

        public bool hasFileNoteScript
        {
            get { return row["Has_File_Note_Script"] == null ? false : (bool)row["Has_File_Note_Script"]; }
            set { this.setRowValue("Has_File_Note_Script", value); }
        }

        public bool hasGeneralScript
        {
            get { return row["Has_General_Script"] == null ? false : (bool)row["Has_General_Script"]; }
            set { this.setRowValue("Has_General_Script", value); }
        }

        public bool isSyncEvent
        {
            get { return row["Is_Sync_Event"] == null ? false : (bool)row["Is_Sync_Event"]; }
            set { this.setRowValue("Is_Sync_Event", value); }
        }
      
        public int RestrictionTypeId
        {
            get { return row["Restriction_Type_Id"] == null ? 0 : (int)row["Restriction_Type_Id"]; }
            set { this.setRowValue("Restriction_Type_Id", value); }
        }

        public bool SendAlert
        {
            get { return row["Send_Alert"] == null ? false : (bool)row["Send_Alert"]; }
            set { this.setRowValue("Send_Alert", value); }
        }

        public bool SendAlertToAdjuster
        {
            get { return row["Send_Alert_To_Adjuster"] == null ? false : (bool)row["Send_Alert_To_Adjuster"]; }
            set { this.setRowValue("Send_Alert_To_Adjuster", value); }
        }

        public bool SendAlertToSupervisor
        {
            get { return row["Send_Alert_To_Supervisor"] == null ? false : (bool)row["Send_Alert_To_Supervisor"]; }
            set { this.setRowValue("Send_Alert_To_Supervisor", value); }
        }

        public bool SendAlertToTechnicalSupervisor
        {
            get { return row["Send_Alert_To_Technical_Supervisor"] == null ? false : (bool)row["Send_Alert_To_Technical_Supervisor"]; }
            set { this.setRowValue("Send_Alert_To_Technical_Supervisor", value); }
        }

        public bool SendUnlockAlertToTechnicalSupervisor
        {
            get { return row["Send_Unlock_Alert_To_Technical_Supervisor"] == null ? false : (bool)row["Send_Unlock_Alert_To_Technical_Supervisor"]; }
            set { this.setRowValue("Send_Unlock_Alert_To_Technical_Supervisor", value); }
        }

        public bool SendUnlockAlertToSupervisor
        {
            get { return row["Send_Unlock_Alert_To_Supervisor"] == null ? false : (bool)row["Send_Unlock_Alert_To_Supervisor"]; }
            set { this.setRowValue("Send_Unlock_Alert_To_Supervisor", value); }
        }

        public bool SendUnlockAlertToAdjuster
        {
            get { return row["Send_Unlock_Alert_To_Adjuster"] == null ? false : (bool)row["Send_Unlock_Alert_To_Adjuster"]; }
            set { this.setRowValue("Send_Unlock_Alert_To_Adjuster", value); }
        }

        public bool SetDiary
        {
            get { return row["Set_Diary"] == null ? false : (bool)row["Set_Diary"]; }
            set { this.setRowValue("Set_Diary", value); }
        }

        public bool SetDiaryForAdjuster
        {
            get { return row["Set_Diary_For_Adjuster"] == null ? false : (bool)row["Set_Diary_For_Adjuster"]; }
            set { this.setRowValue("Set_Diary_For_Adjuster", value); }
        }

        public bool SetDiaryForSupervisor
        {
            get { return row["Set_Diary_For_Supervisor"] == null ? false : (bool)row["Set_Diary_For_Supervisor"]; }
            set { this.setRowValue("Set_Diary_For_Supervisor", value); }
        }

        public bool SetDiaryForTechnicalSupervisor
        {
            get { return row["Set_Diary_For_Technical_Supervisor"] == null ? false : (bool)row["Set_Diary_For_Technical_Supervisor"]; }
            set { this.setRowValue("Set_Diary_For_Technical_Supervisor", value); }
        }
        #endregion

        #region Methods
        public bool isLockDefined
        {
            get { return this.RestrictionTypeId != (int)modGeneratedEnums.RestrictionType.No_Restrictions; }
        }

        public void DeleteFileNoteDefinition()
        {
            //Deletes file note definition for a given event type
            db.ExecuteStoredProcedure("usp_Delete_File_Note_Definition", this.Id, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public void DeleteGeneralDefinition()
        {
            //Deletes general definition for a given event type
            db.ExecuteStoredProcedure("usp_Delete_General_Definition", this.Id, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public List<Hashtable> CreatedTaskTypeRows()
        {
            /*Returns list of created task types for this event.
             * Returns arraylist format to display in a list view.
            */
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Created_Task_Types", this.Id);
        }

        public List<Hashtable> ClearedTaskTypeRows()
        {
            /*Returns list of Cleared task types for this event.
             * Returns arraylist format to display in a list view.
            */
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Cleared_Task_Types", this.Id);
        }

        public FileNoteType FileNoteType()
        {
            return new FileNoteType(this.FileNoteTypeId);
        }

        public void ClearTasks(ScriptParms sp)
        {
            //Clears related tasks - as defined in EventTypeClearsTask

            DataTable rows = db.ExecuteStoredProcedure("usp_Get_User_Tasks_To_Be_Cleared", this.Id, sp.ClaimId(), sp.ClaimantId(), sp.ReserveId());

            foreach (DataRow row in rows.Rows)
            {
                UserTask ut = new UserTask(rows, row);
                string desc = ut.DefaultDescription();
                desc += Environment.NewLine + "Task cleared automatically by event " + this.Description + ".";

                if (sp.ClaimId() != 0)
                    desc += Environment.NewLine + "Claim = " + sp.Claim().DisplayClaimId;

                if (sp.ReserveId() != 0)
                    desc += Environment.NewLine + "Reserve = " + sp.Reserve().Description();
                else if (sp.ClaimantId() != 0)
                    desc += Environment.NewLine + "Claimant = " + sp.Claimant().Name;

                ut.ClearTask(desc);
            }
        }

        public void CreateTasks(ScriptParms sp)
        {
            /*Creates related tasks - as defined in EventTypeCreatesTask
             * If a task type has alread been created and cleared, don't create it again.
             * If a task type has been created but not cleared, create the tasks 
             * only for users who do not yet have them.
            */

            DataTable rows = db.ExecuteStoredProcedure("usp_Get_User_Task_Types_To_Be_Created", this.Id, sp.ClaimId(), sp.ClaimantId(), sp.ReserveId());

            foreach (DataRow row in rows.Rows)
            {
                UserTaskType utt = new UserTaskType(rows, row);
                string desc = "This task has been automatically created by the system.";
                desc += Environment.NewLine + "Task: " + utt.Description;

                if (sp.ClaimId() != 0)
                    desc += Environment.NewLine + "Claim = " + sp.Claim().DisplayClaimId;

                if (sp.ReserveId() != 0)
                    desc += Environment.NewLine + "Reserve = " + sp.Reserve().Description();
                else if (sp.ClaimantId() != 0)
                    desc += Environment.NewLine + "Claimant = " + sp.Claimant().Name;

                UserTask ut = new UserTask();
                ut.UserTaskTypeId = utt.Id;
                ut.UserTaskStatusId = (int)modGeneratedEnums.UserTaskStatus.Task_Created;
                ut.ClaimId = sp.ClaimId();
                ut.ClaimantId = sp.ClaimantId();
                ut.ReserveId = sp.ReserveId();
                ut.Description = desc;
                ut.Update();
            }
        }

        public bool LockClaim()
        {
            return this.RestrictionTypeId == (int)modGeneratedEnums.RestrictionType.Claim ? true : false;
        }

        public bool LockReserve()
        {
            return this.RestrictionTypeId == (int)modGeneratedEnums.RestrictionType.Reserve ? true : false;
        }

        public bool LockTransaction()
        {
            return this.RestrictionTypeId == (int)modGeneratedEnums.RestrictionType.Transaction ? true : false;
        }

        public bool isManualClaimLock()
        {
            return this.Id == (int)modGeneratedEnums.EventType.Manual_Claim_Lock ? true : false;
        }

        public bool isDraftAmountExceedsUserAuthority()
        {
            return this.Id == (int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_User_Authority ? true : false;
        }

        public bool isExpenseReserveChanged()
        {
            return this.Id == (int)modGeneratedEnums.EventType.Expense_Reserve_Changed ? true : false;
        }

        public bool isLossReserveChangedAboveAdjusterAuthority()
        {
            return this.Id == (int)modGeneratedEnums.EventType.Loss_Reserve_Changed_Above_Adjuster_Authority ? true : false;
        }

        public int InsertFileNote(string fileNoteText, ScriptParms sp)
        {
            //Inserts file note and returns new FileNoteId.

            Hashtable row = new Hashtable();
            double authorityNeeded = 0;  //if authority related event, determine financial authority needed to unlock

            if (this.isDraftAmountExceedsUserAuthority())
                authorityNeeded = sp.Transaction().TransactionAmount * -1;

            if (this.isExpenseReserveChanged())
                authorityNeeded = sp.Reserve().NetExpenseReserve;

            if (this.isLossReserveChangedAboveAdjusterAuthority())
                authorityNeeded = sp.Reserve().NetLossReserve;

            //Insert File note row
            FileNote f = new FileNote();
            f.ClaimId = sp.ClaimId();
            f.ClaimantId = sp.ClaimantId();
            f.ReserveId = sp.ReserveId();
            f.TransactionId = sp.TransactionId();
            f.DocumentId = sp.DocumentId();
            f.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            f.AuthorityNeededToUnlock = authorityNeeded;
            f.FileNoteText = fileNoteText;
            f.FileNoteTypeId = this.FileNoteTypeId;
            f.EventTypeId = this.Id;
            f.DiaryTypeId = this.DiaryTypeId;

            return f.Update();
        }

        public List<int> getUserIds(int claimId, int reserveId, ArrayList users, bool forUnlock = false)
        {
            /*Adds adjuster or superisor ids to the collection of user ids passed.
             * Returns a complete collection of user ids to send an alert or set a diary entry.
            */

            IntegerSet userIds = new IntegerSet();
            IntegerSet adjusterIds = new IntegerSet();
            IntegerSet supervisorIds = new IntegerSet();
            IntegerSet techSupervisorIds = new IntegerSet();

            //get adjusters assigned to claim or reserve
            if (reserveId == 0)
            {
                if (claimId != 0)
                {
                    Claim claim = new Claim(claimId);

                    foreach(Reserve res in claim.Reserves())
                    {
                        if (res.AdjusterId == 0)
                        {
                            UserGroup ug = res.AssignedToUserGroup();

                            foreach(User v in ug.Users())
                            {
                                adjusterIds.Add(v.Id);
                                supervisorIds.Add(v.SupervisorId);
                                techSupervisorIds.Add(v.UserReserveType(res.ReserveTypeId).TechnicalSupervisorId);
                            }
                        }
                        else
                        {
                            adjusterIds.Add(res.AdjusterId);
                            supervisorIds.Add(res.AssignedToUser().SupervisorId);
                            techSupervisorIds.Add(res.AssignedToUser().UserReserveType(res.ReserveTypeId).TechnicalSupervisorId);
                        }
                    }
                }
            }
            else //reserve id is specified
            {
                Reserve res = new Reserve(reserveId);

                //Get assigned adjuster(s)
                if (res.AdjusterId != 0)
                {
                    adjusterIds.Add(res.AdjusterId);
                    supervisorIds.Add(res.AssignedToUser().SupervisorId);
                    techSupervisorIds.Add(res.AssignedToUser().UserReserveType(res.ReserveTypeId).TechnicalSupervisorId);
                }
                else
                {
                    UserGroup ug = res.AssignedToUserGroup();

                    foreach (User v in ug.Users())
                    {
                        adjusterIds.Add(v.Id);
                        supervisorIds.Add(v.SupervisorId);
                        techSupervisorIds.Add(v.UserReserveType(res.ReserveTypeId).TechnicalSupervisorId);
                    }
                }
            }  //adjustIds, supervisorIds and techSupervisorIds are populated

            if ((this.SendAlertToAdjuster && !forUnlock) || (this.SendUnlockAlertToAdjuster && forUnlock))
                userIds.Add(adjusterIds.ToList());  //add adjusters to user list to send alerts to

            if ((this.SendAlertToSupervisor && !forUnlock) || (this.SendUnlockAlertToSupervisor && forUnlock))
                userIds.Add(supervisorIds.ToList());  // add supervisors to user list to send alerts to

            if ((this.SendAlertToTechnicalSupervisor && !forUnlock) || (this.SendUnlockAlertToTechnicalSupervisor && forUnlock))
                userIds.Add(techSupervisorIds.ToList());  // add technical supervisors to user list to send alerts to

            //Add explicitly specified users passed to list
            foreach(int i in users)
            {
                userIds.Add(i);
            }

            return userIds.ToList();
        }

        public void InsertAlerts(int claimId, int reserveId, int fileNoteId, ArrayList userIds, int fromUserId,
            DateTime deferredDate = default(DateTime), bool isUrgent = false, bool forUnlock = false)
        {
            //Inserts alerts from event handler or file not entry screen

            string userNames = ""; //list of user names to send alert - used to update file note

            //get list of users to send alert

            //set alert for each user
            foreach(int uId in this.getUserIds(claimId, reserveId, userIds, forUnlock))
            {
                User v = new User(uId);

                if(v.AlertPreferenceId!=(int)modGeneratedEnums.AlertPreference.None) //user has alert preference
                {
                    Alert a = new Alert();
                    a.FileNoteId = fileNoteId;
                    a.SentFromUserId = fromUserId;
                    a.SentToUserId = uId;
                    a.DateSent = ml.DBNow();
                    a.ShowAlert = true;
                    a.isDeferred = this.areAlertsDeferred;
                    a.isUrgent = isUrgent;

                    User fromUser = new User(fromUserId);

                    if (fromUser.isAboveUser(v))
                        a.isUrgent = (bool)isUrgent;

                    if (fromUser.isManagement() && !v.isManagement())
                        a.isUrgent = (bool)isUrgent;

                    if (this.areAlertsDeferred)
                        a.DeferredDate = deferredDate;

                    a.Update();

                    if (v.AlertPreferenceId == (int)modGeneratedEnums.AlertPreference.Email)
                    {
                        //email alert
                        a.SendAlertByEmail();
                        userNames += v.Name + " (email alert), ";
                    }
                    else
                    {
                        //medcsx alert
                        userNames += v.Name + ", ";
                    }
                }

            }

            //update file not with names of users alert is sent to
            if (userNames == "")
                return;

            string deferredString = "";
            string deferredSuffix = "";

            if (this.areAlertsDeferred)
            {
                deferredString = "Deferred ";
                deferredSuffix = ". " + Environment.NewLine + "Alerts Deferred Until " + 
                    deferredDate.ToShortDateString() + " at " + deferredDate.ToLongTimeString() + ".";
            }

            userNames = userNames.Substring(0, userNames.Length - 2).Trim(); //removes last comma and space

            FileNote fileNote = new FileNote(fileNoteId);

            fileNote.FileNoteText += Environment.NewLine + Environment.NewLine + deferredString + 
                "Alerts Sent To: " + userNames + deferredSuffix;
            fileNote.Update();
        }

        public void InsertAlerts(int fileNoteId, ScriptParms sp)
        {
            //Inserts alerts for event - to assigned adjuster, supervisor, & specified users

            //add users to the dictionary
            ArrayList userIds = new ArrayList();
            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Alert_Definition_Users", this.Id).Rows)
            {
                userIds.Add(row["User_Id"]);
            }

            /*If authority related event - mak sure alert goes to the manager with the financial authority
             * to unlock it.  Recursively progress up the chain of command starting with the 
             * technical superisor of the assigned adjuster.
            */

            FileNote f = new FileNote(fileNoteId);
            double authorityNeeded = f.AuthorityNeededToUnlock;

            if (authorityNeeded > 0)
            {
                int adjusterId = Convert.ToInt32(HttpContext.Current.Session["userID"]);  //claim["Adjuster_Id"]
                Reserve r = new Reserve(sp.ReserveId());
                UserReserveType urt = new User(adjusterId).UserReserveType(r.ReserveTypeId);

                double authorityAmount = db.GetDblFromStoredProcedure("usp_Get_User_Authority", Convert.ToInt32(HttpContext.Current.Session["userID"]), r.ReserveTypeId);
               
                int supervisorId = adjusterId;
                int count = 0;

                while ((authorityAmount < authorityNeeded) && (count < 10))  //only allow 10 iterations
                {
                    urt = new User(urt.TechnicalSupervisorId).UserReserveType(r.ReserveTypeId);
                    supervisorId = urt.UserId;
                    count++;
                }
                userIds.Add(supervisorId);
            }

            //Insert alerts
            this.InsertAlerts(sp.ClaimId(), sp.ReserveId(), fileNoteId, userIds, 0);
        }

        public void InsertDiaries(int claimId, int reserveId, int fileNoteId, ArrayList userIds, 
            short nbrDays, int fromUserId)
        {
            //Inserts diary entries from file note entry screen

            //set diary entry for each user
            foreach (int v in this.getUserIds(claimId, reserveId, userIds))
            {
                Diary d = new Diary();
                d.FileNoteId = fileNoteId;
                d.SentFromUserId = fromUserId;
                d.SentToUserId = v;
                d.DiaryEntryDate = ml.DBNow();
                d.ShowDiary = true;
                d.NbrDays = nbrDays;
                d.DiaryDueDate = ml.DBNow().AddDays(nbrDays);
                //d.ReserveId = reserveId;
                d.Update();
            }
        }

        public int RunFileNoteEventHandler(Hashtable parms)
        {
            //Run file note .Net script (dynamically compile code)

            //try
            //{
                //compile script
                CompilerResults cr = DynamicCompiler(this.FileNoteScript);

                if (cr.Errors.Count > 0)
                {
                    //error when compiling script
                    return 0;
                }
                else
                {

                
                    //compiled successfully

                    //get the compiled assembly
                    Assembly assembly = cr.CompiledAssembly;
                    Type t = assembly.GetType("FileScript");

                    //set the parameters
                    object[] arg = {ml.myScriptFunctions};
                    t.InvokeMember("SetMedCsXDLL", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, arg);
                    arg = new object[] {parms};
                    t.InvokeMember("SetParms", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, arg);
                    arg = new object[] {this.row};
                    t.InvokeMember("SetEventType", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, arg);

                    //execute file note script function
                    object result = t.InvokeMember("FileNoteEventHandler", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, null);

                    string fileNoteText = "";
                    if (result != null)
                        fileNoteText = result.ToString();

                    //if no file note text was returned, exit - scripts that return "" do nothing
                    if (fileNoteText.Trim() == "")
                        return 0;

                    //create the script parameters object
                    ScriptParms sp = new ScriptParms(parms);

                    if (this.LockReserve())
                    {
                        if (sp.Reserve().isLocked)
                            fileNoteText += " Attempt to lock a locked reserve, locking ignored. ";
                    }

                    //insert file note row
                    int fileNoteId = this.InsertFileNote(fileNoteText, sp);

                    //create alerts and diaries
                    if (!((this.Id == (int)modGeneratedEnums.EventType.Reserve_Added) && (!sp.Claim().SetupComplete)))
                    {
                        if (this.SendAlert)
                            this.InsertAlerts(fileNoteId, sp);
                    }

                    //set claim lock
                    if (this.LockClaim())
                    {
                        sp.Claim().Lock(fileNoteId);
                        return fileNoteId;
                    }

                    //set reserve lock
                    if (this.LockReserve())
                    {
                        if (!sp.Reserve().isLocked)
                        {
                            sp.Reserve().Lock(fileNoteId);
                            return fileNoteId;
                        }
                    }

                    //set transaction lock
                    if (this.LockTransaction())
                    {
                        sp.Transaction().Lock(fileNoteId);
                        return fileNoteId;
                    }
                    return fileNoteId;
                }
            //}
            //catch(Exception ex){
            //    return 0;
            //}
        }

        public void RunGeneralEventHandler(Hashtable parms)
        {
            //run general event handler event
            try
            {
                //compile the script
                CompilerResults cr = DynamicCompiler(this.FileNoteScript);

                if (cr.Errors.Count > 0)
                {
                    //error when compiling script
                }
                else
                {
                    //compiled successfully

                    //get the compiled assembly
                    Assembly assembly = cr.CompiledAssembly;
                    Type t = assembly.GetType("FileScript");

                    //set the parameters
                    object[] arg = { ml.myScriptFunctions };
                    t.InvokeMember("setMedCsXDll", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, arg);
                    arg = new object[] { parms };
                    t.InvokeMember("setParms", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, arg);
                    arg = new object[] { this.row };
                    t.InvokeMember("setEventType", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, arg);

                    //execute file note script function
                    t.InvokeMember("GeneralEventHandler", (BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod), null, null, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool FireEvent(Hashtable parms)
        {
            /*Fires an application event.
             * Event types are stored in the Event_Type table.
             * parms is a dictionary of variables to pass to the scripts registered to 
             * execute when the event is fired.
             * Scripts may be registered for an event to enter a file note or send an alert.
             * A general script (which does not set alerts, file notes, etc.) may also be registered
             * in response to an event.
            */

            ScriptParms sp = new ScriptParms(parms);

            //if it is a sync event and the event has already happened before, don't run script, send alerts, or set locks
            if (this.isSyncEvent)
            {
                if (sp.SyncEventExists(this.Id))
                    return false;
                else
                    sp.InsertSyncEvent(this.Id);
            }

            int fileNoteId = 0;
            if ((this.Id == (int)modGeneratedEnums.EventType.Claim_Created_Without_Policy) || (this.Id == (int)modGeneratedEnums.EventType.Claim_Created_Using_Policy))
            {
                //run scripts

                if (this.hasFileNoteScript)
                    fileNoteId = this.RunFileNoteEventHandler(parms);

                if (this.hasGeneralScript)
                    this.RunGeneralEventHandler(parms);

                //Clear and create tasks
                this.ClearTasks(sp);
                this.CreateTasks(sp);

                //clear alerts and diaries
                this.ClearAlerts(sp);

                //clear and create diaries
                this.ClearDiaries(sp);
                this.CreateDiaries(fileNoteId, sp);

                //if claim is locked for same reason, or unlocked but "Condition_Still_Exists" - ignore event
                if (this.CheckForExistingLock)
                {
                    if (this.LockClaim() && !this.isManualClaimLock())
                    {
                        ClaimLock claimLock = sp.Claim().LatestLockForEventType(this.Id);
                        if (claimLock != null)
                        {
                            if (claimLock.isLocked)
                                return false;
                        }
                    }
                }

                //if claim is locked for same reason, or unlocked but "Condition_Still_Exists" - ignore event
                if (this.LockReserve())
                {
                    TransactionLock tl = sp.Transaction().LatestLockForEventType(this.Id);

                    if (tl != null)
                    {
                        if (tl.isLocked)
                            return false;
                    }
                }
            }
            else
            {
                //if claim is locked for same reason, or unlocked but "Condition_Still_Exists" - ignore event
                if (this.CheckForExistingLock)
                {
                    if (this.LockClaim() && !this.isManualClaimLock())
                    {
                        ClaimLock claimLock = sp.Claim().LatestLockForEventType(this.Id);
                        if (claimLock != null)
                        {
                            if (claimLock.isLocked)
                                return false;
                        }
                    }
                }

                //TODO
                ////if claim is locked for same reason, or unlocked but "Condition_Still_Exists" - ignore event
                if (this.LockReserve())
                {
                    ReserveLock tl = sp.Reserve().LatestLockForEventType(this.Id);

                    if (tl != null)
                    {
                        if (tl.isLocked)
                            return false;
                    }
                }

                //run scripts

                if (this.hasFileNoteScript)
                    fileNoteId = this.RunFileNoteEventHandler(parms);

                if (this.hasGeneralScript)
                    this.RunGeneralEventHandler(parms);

                //Clear and create tasks
                 this.ClearTasks(sp);
                this.CreateTasks(sp);

                //clear alerts and diaries
                this.ClearAlerts(sp);

                //clear and create diaries
                this.ClearDiaries(sp);
                this.CreateDiaries(fileNoteId, sp);
            }
            return true;
        }

        public void ClearAlerts(ScriptParms sp)
        {
            //Clear alerts related to the ClearsFileNoteTypeId field
            if (sp.Claim() == null)
                return;

            foreach (FileNote f in sp.Claim().ChildObjects(typeof(FileNote)))
            {
                if ((f.ClaimId == sp.ClaimId()) && (f.ClaimantId==sp.ClaimantId()) &&
                    (f.ReserveId == sp.ReserveId()) && (f.TransactionId == sp.TransactionId()) &&
                    (this.ClearsFileNoteTypeId == f.FileNoteTypeId))
                    f.ClearAlerts();
            }
        }
     
        public void ClearDiaries(ScriptParms sp)
        {
            //Clear diaries related to the ClearsFileNoteTypeId field
            if (sp.Claim() == null)
                return;

            foreach (FileNote f in sp.Claim().ChildObjects(typeof(FileNote)))
            {
                if ((f.ClaimId == sp.ClaimId()) && (f.ClaimantId==sp.ClaimantId()) &&
                    (f.ReserveId == sp.ReserveId()) && (f.TransactionId == sp.TransactionId()) &&
                    (this.ClearsFileNoteTypeId == f.FileNoteTypeId))
                    f.ClearDiaries();
            }
        }

       public void CreateDiaries(int fileNoteId, ScriptParms sp)
       {
           //Automatically creates diaries in response to an event firing
           if (this.DiaryTypeId == 0)
               return;

           //create list of users to set diary
           IntegerSet users = new IntegerSet();
           if (this.SetDiaryForAdjuster)
           {
               if (sp.ReserveId() != 0)
                   users.Add(sp.Reserve().AssignedToUserIds());
               else
               {
                   if (sp.ClaimantId() != 0)
                       users.Add(sp.Claimant().AssignedToUserIds());
                   else
                       users.Add(sp.Claim().AssignedToUserIds());
               }
           }

           //get reserves
           ArrayList res = new ArrayList();
           if (sp.ReserveId() != 0)
               res.Add(sp.Reserve());
           else
           {
               if (sp.ClaimantId() != 0)
               {
                   foreach (Reserve r in sp.Claimant().Reserves())
                   {
                       res.Add(r);
                   }
               }
           }

           //add supervisors to list
           if (this.SetDiaryForSupervisor)
           {
               IntegerSet supers = new IntegerSet();
               foreach (int uId in users.ToList())
               {
                   User u = new User(uId);
                   supers.Add(u.SupervisorId);
               }
           }

           //add tech supervisors to list
           if (this.SetDiaryForTechnicalSupervisor)
           {
               foreach (Reserve r in res)
               {
                   foreach (User u in r.AssignedToUsers())
                   {
                       users.Add(u.UserReserveType(r.ReserveTypeId).TechnicalSupervisorId);
                   }
               }
           }

           //add any other users specified
           foreach (DiaryDefinitionUser ddu in this.ChildObjects(typeof(DiaryDefinitionUser)))
           {
               users.Add(ddu.UserId);
           }

           //create diary to be sent
           foreach (int v in users.ToList())
           {
               Diary d = new Diary();
               d.FileNoteId = fileNoteId;
               d.DiaryDueDate = ml.DBNow().AddDays(this.DiaryDueDateOffset);
               d.SentFromUserId = 0;
               d.SentToUserId = v;
               d.NbrDays = this.DiaryDueDateOffset;
               d.DiaryEntryDate = ml.DBNow();
               d.ShowDiary = true;
               d.Update();
           }
       }

        public bool FireEventIfNecessary(object oldValue, object newValue, Hashtable parms)
        {
            /*Fires event if old value is different than new value.
             * parms is a dictionary of parameters to be passed to the event handler.
             * It is defined as a Variant but is really a Dictionary.
             * It is defined as a Variant because a calling routine needed it to be optional.
            */

            if (oldValue != newValue)
            {
                if (parms.Contains("Old_Value"))
                    parms["Old_Value"] = oldValue;
                else
                    parms.Add("Old_Value", oldValue);

                if (parms.Contains("New_Value"))
                    parms["New_Value"] = newValue;
                else
                    parms.Add("New_Value", newValue);

                if (this.FireEvent(parms))
                    return true;
            }
            return false;
        }

        public RestrictionType RestrictionType()
        {
            return new RestrictionType(this.RestrictionTypeId);
        }

        public DiaryType DiaryType()
        {
            return new DiaryType(this.DiaryTypeId);
        }

        public DiaryType ClearsDiaryType()
        {
            return new DiaryType(this.ClearsDiaryTypeId);
        }

        public FileNoteType ClearsFileNoteType()
        {
            return new FileNoteType(this.ClearsFileNoteTypeId);
        }
        
        public static CompilerResults DynamicCompiler(string sourceCode)
        {
            //Compiles the source code and returns the results

            //remove the set statements
            //sourceCode = System.Text.RegularExpressions.Regex.Replace(sourceCode, "/r/n *?set ", Environment.NewLine);
            //sourceCode = System.Text.RegularExpressions.Regex.Replace(sourceCode, "/r/n *?let ", Environment.NewLine);
            //string SourceCode = sourceCode.Replace("/r/n *?set ", Environment.NewLine);
            //SourceCode = SourceCode.Replace("/r/n *?let ", Environment.NewLine);
            string SourceCode = sourceCode.Replace("let ", Environment.NewLine).Replace("set ", Environment.NewLine);

            //add the necessary code to make the script valid
            //sourceCode = "using System.Collections;" + Environment.NewLine + Environment.NewLine +
            //    "public class FileScript" + Environment.NewLine + "{" + Environment.NewLine + "public object MedCsXDll;" + Environment.NewLine +
            //    "public Hashtable parms;" + Environment.NewLine + "public Hashtable eventType;" + Environment.NewLine + Environment.NewLine +
            //    "public void setMedCsXDll(object parm) { " + Environment.NewLine + "MedCsXDll = parm; }" + Environment.NewLine +
            //    "public void setParms(object parm) { " + Environment.NewLine + "parms = parm; }" + Environment.NewLine +
            //    "public void setEventType(object parm) { " + Environment.NewLine + "eventType = parm; }" + Environment.NewLine + "}";


            string  sourceCodeFinal = "Imports System.Collections" + Environment.NewLine + "Imports Microsoft.VisualBasic" + Environment.NewLine + Environment.NewLine + "Imports System.Web" + Environment.NewLine + Environment.NewLine +
        "Module FileScript" + Environment.NewLine + Environment.NewLine + "Public MedCsXDLL As Object" + Environment.NewLine + "Public parms as Hashtable" +
        Environment.NewLine + "Public eventType as Hashtable" + Environment.NewLine + Environment.NewLine +
        "Public Sub SetMedCsXDLL(ByVal parm As Object)" + Environment.NewLine + "MedCsXDLL = parm" + Environment.NewLine + "End Sub" + Environment.NewLine + Environment.NewLine +
        "Public Sub SetParms(ByVal parm As Object)" + Environment.NewLine + "parms = parm" + Environment.NewLine + "End Sub" + Environment.NewLine + Environment.NewLine +
        "Public Sub SetEventType(ByVal parm As Object)" + Environment.NewLine + "eventType = parm" + Environment.NewLine + "End Sub" + Environment.NewLine + Environment.NewLine +
         SourceCode + Environment.NewLine + Environment.NewLine + "End Module";

            //sourceCode = "using System.Collections; \r\n" + 
            //   "public class FileScript \r\n { \r\n public object MedCsXDll;"  +
            //   "public Hashtable parms; \r\n public Hashtable eventType; \r\n" + 
            //   "public void setMedCsXDll(object parm) { \r\n MedCsXDll = parm; }" + 
            //   "public void setParms(object parm) { \r\n  parms = parm; }" + 
            //   "public void setEventType(object parm) { \r \n eventType = parm; } \r\n}";



            //create the code provider
            //CodeDomProvider provider =CodeDomProvider.CreateProvider("CSharp");
            CodeDomProvider provider = CodeDomProvider.CreateProvider("VisualBasic");

            //load the compiler parameters
            //string[] referenceAssemblies={"System.dll", "System.Windows.Forms.dll", "Microsoft.CSharp.dll"};
            string[] referenceAssemblies = { "System.dll", "System.Windows.Forms.dll", "Microsoft.VisualBasic.dll", "System.Web.dll" };
            //CompilerParameters cp = new CompilerParameters();
            //cp.GenerateExecutable = false;
            //cp.GenerateInMemory = true;
            //cp.ReferencedAssemblies.Add("System.dll");
            //cp.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            //cp.ReferencedAssemblies.Add("Microsoft.CSharp.dll");

            CompilerParameters cp = new CompilerParameters(referenceAssemblies);
            cp.GenerateExecutable = false;
            cp.GenerateInMemory = true;
            

            //compile the script
            CompilerResults cr = provider.CompileAssemblyFromSource(cp, sourceCodeFinal);

            //return the compilation results
            return cr;
        }

        public static bool DynamicCompilerSyntaxCheck(string sourceCode, ref string errorMessage)
        {
            //Syntax check the script by dynamcially compiling it

            //Compile the script
            CompilerResults cr = DynamicCompiler(sourceCode);

            errorMessage = "";
            if (cr.Errors.Count > 0)
            {
                //Error generated when compiling the script

                //create the compilation errors message
                foreach (CompilerError ce in cr.Errors)
                {
                    errorMessage += ce.ToString() + Environment.NewLine;
                }
                return false;
            }
            else
            {
                //Compiled sucessfully
                errorMessage = "Syntax Check Successful!";
                return true;
            }
        }
        #endregion


    }
}

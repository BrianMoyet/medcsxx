﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class LitigationDefendant:Litigation
   {

       #region Class Variables
       public const int InsuranceCompanyType = (int)modGeneratedEnums.VendorSubType.Insurance_Company;

       private Litigation m_Litigation = null;
       private Person m_Person = null;
       private Vendor m_Vendor = null;
       #endregion

       #region Constructors
       public LitigationDefendant() : base() { }
       public LitigationDefendant(int id) : base(id) { }
       public LitigationDefendant(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get
           {
               return "Litigation_Defendent";
           }
       }

       public int LitigationId
       {
           get { return (int)row["Litigation_Id"]; }
           set { this.setRowValue("Litigation_Id", value); }
       }

       public int PersonId
       {
           get { return (int)row["Person_Id"]; }
           set { this.setRowValue("Person_Id", value); }
       }

       public int Vendor_Id
       {
           get { return (int)row["Vendor_Id"]; }
           set { this.setRowValue("Vendor_Id", value); }
       }
       #endregion

       #region Methods
       public Litigation Litigation
       {
           get { return this.m_Litigation == null ? this.m_Litigation = new Litigation(this.LitigationId) : this.m_Litigation; }
       }

       public Person Person
       {
           get { return this.m_Person == null ? this.m_Person = new Person(this.PersonId) : this.m_Person; }
       }

       public Vendor Vendor
       {
           get { return this.m_Vendor == null ? this.m_Vendor = new Vendor(this.Vendor_Id) : this.m_Vendor; }
       }
       #endregion
   }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Deductibles:TransactionReferenceTable
    {
        private static clsDatabase db=new clsDatabase();
      
        #region Constructors
        public Deductibles() : base() { }
        public Deductibles(int id) : base(id) { }
        public Deductibles(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Deductibles"; }
        }

        public Int64 CheckNo
        {
            get { return (Int64)row["Check_No"]; }
            set { this.setRowValue("Check_No", value); }
        }

        public bool isVoid
        {
            get { return (bool)row["Is_Void"]; }
            set { this.setRowValue("Is_Void", value); }
        }

        public string RecoverySource
        {
            get { return (string)row["Recovery_Source"]; }
            set { this.setRowValue("Recovery_Source", value); }
        }

        public int SubroSalvageVoidReasonId
        {
            get { return (int)row["Subro_Salvage_Void_Reason_Id"]; }
            set { this.setRowValue("Subro_Salvage_Void_Reason_Id", value); }
        }
        #endregion

        #region Misc Methods
        public int TransactionId
        {
            //get transaction ID
            get { return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Deductibles_Id", this.Id); }
        }

        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class VendorSubType:TypeTable
    {
        #region Constructors
        public VendorSubType() : base() { }
        public VendorSubType(int id) : base(id) { }
        public VendorSubType(string description) : base(description) { }
        public VendorSubType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Vendor_Sub_Type";
            }
        }

        public bool RequireTaxId
        {
            get { return (bool)row["Require_Tax_Id"]; }
            set { this.setRowValue("Require_Tax_Id", value); }
        }
      
        public int VendorSubTypeId
        {
            get { return (int)row["Vendor_Sub_Type_Id"]; }
            set { this.setRowValue("Vendor_Sub_Type_Id", value); }
        }
        public int VendorTypeId
        {
            get { return (int)row["Vendor_Type_Id"]; }
            set { this.setRowValue("Vendor_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public VendorType VendorType()
        {
            return new VendorType(this.VendorTypeId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ReserveLock:LockTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public ReserveLock() : base() { }
        public ReserveLock(int id) : base(id) { }
        public ReserveLock(Hashtable row) : base(row) { }
        public ReserveLock(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Reserve_Lock"; }
        }

        public int ReserveId
        {
            get { return (int)row["Reserve_Id"]; }
            set { this.setRowValue("Reserve_Id", value); }
        }

        public int ReserveLockId
        {
            get { return (int)row["Reserve_Lock_Id"]; }
            set { this.setRowValue("Reserve_Lock_Id", value); }
        }
        #endregion

        #region Misc Methods
        public Reserve Reserve()
        {
            return new Reserve(this.ReserveId);
        }

        public int ClaimantId
        {
            get { return this.Reserve().ClaimantId; }
        }

        public int ClaimId
        {
            get { return this.Reserve().ClaimId; }
        }

        public Claimant Claimant()
        {
            return this.Reserve().Claimant;
        }

        public Claim Claim()
        {
            return this.Reserve().Claim;
        }

        public void SendUnlockAlerts()
        {
            //sends unlock alerts for a reserve
            ArrayList userIds = new ArrayList();  //users dictionary
            FileNote f = new FileNote(this.LockFileNoteId);
            EventType et = new EventType(f.EventTypeId);

            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Alert_Definition_Unlock_Users", et.Id).Rows)
                userIds.Add(row["User_Id"]);

            //send alerts
            et.InsertAlerts(this.ClaimId, this.ReserveId, this.UnlockFileNoteId, userIds, 0, ml.DEFAULT_DATE, false, true);
        }

        public void Unlock(string unlockFileNoteText)
        {
            //unlocks reserve - update reserve_Lock row to turn lock off
            int unlockFileNoteId = this.Reserve().InsertFileNote((int)modGeneratedEnums.FileNoteType.Unlock_Reserve, unlockFileNoteText);
            //inserts unlock file note and returns file note ID

            //unlock reserve
            this.isLocked = false;
            this.UnlockDate = ml.DBNow();
            this.UnlockedBy = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            this.UnlockFileNoteId = unlockFileNoteId;
            this.Update();

            //fire unlock reserve event
            this.Reserve().FireEvent((int)modGeneratedEnums.EventType.Unlock_Reserve);

            //send unlock alerts
            this.SendUnlockAlerts();
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class DiaryDefinitionUser:MedCsXTable
    {

        #region Constructors
        public DiaryDefinitionUser() : base() { }
        public DiaryDefinitionUser(int id) : base(id) { }
        public DiaryDefinitionUser(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int DiaryDefinitionUserId
        {
            get { return (int)row["Diary_Definition_User_Id"]; }
            set { this.setRowValue("Diary_Definition_User_Id", value); }
        }

        public int EventTypeId
        {
            get { return (int)row["Event_Type_Id"]; }
            set { this.setRowValue("Event_Type_Id", value); }
        }
        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }
        #endregion

        #region Methods
        public override string TableName
        {
            get { return "Diary_Definition_User"; }
        }

        public EventType EventType()
        {
            return new EventType(this.EventTypeId);
        }

        public User User()
        {
            return new User(this.UserId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UserReserveType:MedCsXTable
    {

        #region Constructors
        public UserReserveType() : base() { }
        public UserReserveType(int id) : base(id) { }
        public UserReserveType(Hashtable row) : base(row) { }
        public UserReserveType(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "User_Reserve_Type"; }
        }

        public double AuthorityAmount
        {
            get { return (double)row["Authority_Amount"]; }
            set { this.setRowValue("Authority_Amount", value); }
        }

        public int ReserveTypeId
        {
            get { return (int)row["Reserve_Type_Id"]; }
            set { this.setRowValue("Reserve_Type_Id", value); }
        }
  
        public int TechnicalSupervisorId
        {
            get { return (int)row["Technical_Supervisor_Id"]; }
            set { this.setRowValue("Technical_Supervisor_Id", value); }
        }

        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }
        #endregion

        #region Misc Methods
        public User User()
        {
            return new User(this.UserId);
        }

        public User TechnicalSupervisor()
        {
            return new User(this.TechnicalSupervisorId);
        }

        public ReserveType ReserveType()
        {
            return new ReserveType(this.ReserveTypeId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public class BusinessDescription:TypeTable
   {

       #region Constructors
       public BusinessDescription() : base() { }
       public BusinessDescription(int id) : base(id) { }
       public BusinessDescription(string description) : base(description) { }
       public BusinessDescription(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get
           {
               return "Business_Description";
           }
       }

       public int BusinessDescriptionId
       {
           get { return (int)row["Business_Description_Id"]; }
           set { this.setRowValue("Business_Description_Id", value); }
       }

       public string BusinessTerm
       {
           get { return (string)row["Business_Term"]; }
           set { this.setRowValue("Business_Term", value); }
       }
       #endregion
       #region
       #endregion
       #region
       #endregion
   }
}

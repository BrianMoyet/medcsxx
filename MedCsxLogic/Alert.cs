﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Alert : MedCsXTable
    {
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();

        #region Constructors
        public Alert() : base() { }
        public Alert(int id) : base(id) { }
        public Alert(Hashtable row) : base(row) { }
        public Alert(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Alert"; }
        }

        public int AlertId
        {
            get { return (int)row["Alert_Id"]; }
            set { this.setRowValue("Alert_Id", value); }
        }

        public bool ClearedFromMainScreen
        {
            get { return (bool)row["Cleared_From_Main_Screen"]; }
            set { this.setRowValue("Cleared_From_Main_Screen", value); }
        }

        public DateTime DateRespondedTo
        {
            get { return (DateTime)row["Date_Responded_To"]; }
            set { this.setRowValue("Date_Responded_To", value); }
        }

        public DateTime DateSent
        {
            get { return (DateTime)row["Date_Sent"]; }
            set { this.setRowValue("Date_Sent", value); }
        }

        public DateTime DeferredDate
        {
            get { return (DateTime)row["Deferred_Date"]; }
            set { this.setRowValue("Deferred_Date", value); }
        }

        public int FileNoteId
        {
            get { return (int)row["File_Note_Id"]; }
            set { this.setRowValue("File_Note_Id", value); }
        }

        public bool isDeferred
        {
            get { return (Int16)row["Is_Deferred"] == 1; }
            set 
            {
                if (value)
                    this.setRowValue("Is_Deferred", 1);
                else
                    this.setRowValue("Is_Deferred", 0);
            }
        }

        public bool isUrgent
        {
            get { return (Int16)row["Is_Urgent"] == 1; }
            set 
            {
                if (value)
                    this.setRowValue("Is_Urgent", 1);
                else
                    this.setRowValue("Is_Urgent", 0);
            }
        }

        public int SentFromUserId
        {
            get { return (int)row["Sent_From_User_Id"]; }
            set { this.setRowValue("Sent_From_User_Id", value); }
        }

        public int SentToUserId
        {
            get { return (int)row["Sent_To_User_Id"]; }
            set { this.setRowValue("Sent_To_User_Id", value); }
        }

        public bool ShowAlert
        {
            get { return (Int16)row["Show_Alert"] == 1; }
            set 
            {
                if (value)
                    this.setRowValue("Show_Alert", 1);
                else
                    this.setRowValue("Show_Alert", 0);
            }
        }
        #endregion

        #region Misc Methods
        public Claim Claim
        {
            get { return new Claim(this.ClaimId); }
        }

        public int ClaimId
        {
            get { return db.GetIntFromStoredProcedure("usp_Get_Claim_Id_For_Alert_Id", this.Id); }
        }

        public bool AlertsClearedForClaim
        {
            get
            {
                return db.GetIntFromStoredProcedure("usp_Alerts_Cleared_For_Claim", this.ClaimId, this.SentToUserId) != 0;
            }
        }
        public bool hasRepliedToAlert
        {
            get { return db.GetIntFromStoredProcedure("usp_Has_Replied_To_Alert", this.Id) != 0; }
        }

        public FileNote FileNote()
        {
            return new FileNote(this.FileNoteId);
        }

        public void Clear()
        {
            this.ShowAlert = false;
            this.Update();
        }

        public void ClearFromMainScreen()
        {
            this.ShowAlert = false;
            this.ClearedFromMainScreen = true;
            this.Update();
        }

        public override void AssignTo(User user)
        {
            if (user == null)
                return;
            this.SentToUserId = user.Id;
            this.Update();
        }

        public override void AssignTo(UserGroup userGroup)
        {
            return; //assign to user group not supported for alerts
        }

        public void SendAlertByEmail()
        {
            //send an alert to the user's email
            try
            {
                User v = new User(this.SentToUserId);
                FileNote f = new FileNote(this.FileNoteId);
                Claim c = f.Claim;

                //create the msg fields
                string Subject = "MedCsX Alert from " + ml.CurrentUser.Name + " for " + c.DisplayClaimId;
                string Body = "You Have Received a MedCsx Alert from " + ml.CurrentUser.Name + Environment.NewLine + Environment.NewLine +
                    "Claim: " + c.DisplayClaimId + Environment.NewLine +
                    "Policy: " + c.PolicyNo + Environment.NewLine +
                    "Date of Loss: " + c.DateOfLoss.ToShortDateString() + Environment.NewLine + Environment.NewLine +
                    "Alert Description: " + f.FileNoteText;

                //send email
                SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, ml.CurrentUser.EmailAddress, v.Email, Subject, Body, false);
            }
            catch (Exception ex)
            {
                throw ex;
                //System.Windows.MessageBox.Show(ex.ToString());
            }
        }

        public User SentFrom()
        {
            return new User(this.SentFromUserId);
        }

        public User SentTo()
        {
            return new User(this.SentToUserId);
        }
        #endregion
    }
}

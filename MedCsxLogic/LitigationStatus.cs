﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class LitigationStatus:TypeTable 
    {

       #region Constructors
       public LitigationStatus() : base() { }
       public LitigationStatus(int id) : base(id) { }
       public LitigationStatus(string description) : base(description) { }
       public LitigationStatus(Hashtable row) : base(row) { }
       #endregion

       #region Table Properties
       public override string TableName
       {
           get
           {
               return "Litigation_Status";
           }
       }
       #endregion

    }
}

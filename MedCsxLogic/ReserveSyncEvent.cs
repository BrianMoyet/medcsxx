﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ReserveSyncEvent:SyncEventTable
    {

        #region Constructors
        public ReserveSyncEvent() : base() { }
        public ReserveSyncEvent(int id) : base(id) { }
        public ReserveSyncEvent(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Reserve_Sync_Event";
            }
        }

        public int ReserveId
        {
            get { return (int)row["Reserve_Id"]; }
            set { this.setRowValue("Reserve_Id", value); }
        }

        public int ReserveSyncEventId
        {
            get { return (int)row["Reserve_Sync_Event_Id"]; }
            set { this.setRowValue("Reserve_Sync_Event_Id", value); }
        }

        public Reserve Reserve()
        {
            return new Reserve(this.ReserveId);
        }
        #endregion
    }
}

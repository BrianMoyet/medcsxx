﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MedCsxLogic
{
    public class VehicleGrid
    {
        public int vehicleId { get; set; }
        public int claimId { get; set; }
        public int policyId { get; set; }
        public int vYear { get; set; }
        public string vMake { get; set; }
        public string vModel { get; set; }
        public string vLicenseNo { get; set; }
        public string vVIN { get; set; }
        public string vColor { get; set; }
        public string vName { get; set; }
        public string vType { get; set; }
        public string vShortName { get; set; }
        public string vState { get; set; }
        public string level { get; set; }
        public int vAge { get; set; }
        public bool fraud_select { get; set; }
        public string NameType
        {
            get
            {
                if (!string.IsNullOrEmpty(vName) && !string.IsNullOrEmpty(vType))
                {
                    string s = vName + " " + vType;
                    return s.Trim();
                }
                else
                    return "";
            }
        }
        public string YearMakeModel
        {
            get
            {
                string ymm="";
                if (vYear > 1900)
                    ymm += vYear.ToString() + " ";
                if (!string.IsNullOrEmpty(vMake))
                    ymm += vMake + " ";
                if (!string.IsNullOrEmpty(vModel))
                    ymm += vModel + " ";
                if (!string.IsNullOrEmpty(vVIN))
                    ymm += vVIN;
                return ymm.Trim();
            }
        }
        public string vYearOut
        {
            get
            {
                if (vYear > 1900)
                    return vYear.ToString();
                return "";
            }
        }
    }
}

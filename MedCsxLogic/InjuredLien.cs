﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class InjuredLien:ClaimTable
    {

        #region Constructors
        public InjuredLien() : base() { }
        public InjuredLien(int id) : base(id) { }
        public InjuredLien(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Injured_Lien"; }
        }

        public int InjuredId
        {
            get { return (int)row["Injured_Id"]; }
            set { this.setRowValue("Injured_Id", value); }
        }

        public int VendorId
        {
            get { return (int)row["Vendor_Id"]; }
            set { this.setRowValue("Vendor_Id", value); }
        }

        public double Amount
        {
            get { return (double)((decimal)row["Amount"]); }
            set { this.setRowValue("Amount", value); }
        }
        #endregion
    }
}

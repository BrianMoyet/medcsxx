﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class InvestigatingAgencyType:TypeTable
   {

       #region Constructors
       public InvestigatingAgencyType() : base() { }
       public InvestigatingAgencyType(int id) : base(id) { }
       public InvestigatingAgencyType(string description) : base(description) { }
       public InvestigatingAgencyType(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get
           {
               return "Investigating_Agency_Type";
           }
       }

       public string InvestigatingAgencyTypeCode
       {
           get { return (string)row["Investigating_Agency_Type_Code"]; }
           set { this.setRowValue("Investigating_Agency_Type_Code", value); }
       }

       public int InvestigatingAgencyTypeId
       {
           get { return (int)row["Investigating_Agency_Type_Id"]; }
           set { this.setRowValue("Investigating_Agency_Type_Id", value); }
       }
       #endregion
   }
}

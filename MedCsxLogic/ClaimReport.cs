﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MedCsxLogic
{
    public class ClaimReport:MedCsXTable
    {

        #region Constructors
        public ClaimReport() : base() { }
        public ClaimReport(int id) : base(id) { }
        public ClaimReport(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public string Agent
        {
            get { return (string)row["Agent"]; }
            set { this.setRowValue("Agent", value); }
        }
        
        public string BICoverage
        {
            get { return (string)row["BI_Coverage"]; }
            set { this.setRowValue("BI_Coverage", value); }
        }

        public int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public int ClaimReportId
        {
            get { return (int)row["Claim_Report_Id"]; }
            set { this.setRowValue("Claim_Report_Id", value); }
        }

        public string ClaimTitle
        {
            get { return (string)row["Claim_Title"]; }
            set { this.setRowValue("Claim_Title", value); }
        }

        public string CollisionDed
        {
            get { return (string)row["Collision_Ded"]; }
            set { this.setRowValue("Collision_Ded", value); }
        }

        public string CompDed
        {
            get { return (string)row["Comp_Ded"]; }
            set { this.setRowValue("Comp_Ded", value); }
        }

        public string Driver
        {
            get { return (string)row["Driver"]; }
            set { this.setRowValue("Driver", value); }
        }

        public string DriverLine2
        {
            get { return (string)row["Driver_Line2"]; }
            set { this.setRowValue("Driver_Line2", value); }
        }

        public string DriverLine3
        {
            get { return (string)row["Driver_Line3"]; }
            set { this.setRowValue("Driver_Line3", value); }
        }

        public string DriverLine4
        {
            get { return (string)row["Driver_Line4"]; }
            set { this.setRowValue("Driver_Line4", value); }
        }

        public string DriverLine5
        {
            get { return (string)row["Driver_Line5"]; }
            set { this.setRowValue("Driver_Line5", value); }
        }

        public string DriverName
        {
            get { return (string)row["Driver_Name"]; }
            set { this.setRowValue("Driver_Name", value); }
        }

        public DateTime EffectiveDate
        {
            get { return (DateTime)row["Effective_Date"]; }
            set { this.setRowValue("Effective_Date", value); }
        }

        public DateTime ExpirationDate
        {
            get { return (DateTime)row["Expiration_Date"]; }
            set { this.setRowValue("Expiration_Date", value); }
        }

        public string Insured
        {
            get { return (string)row["Insured"]; }
            set { this.setRowValue("Insured", value); }
        }

        public string InsuredLine2
        {
            get { return (string)row["Insured_Line2"]; }
            set { this.setRowValue("Insured_Line2", value); }
        }

        public string InsuredLine3
        {
            get { return (string)row["Insured_Line3"]; }
            set { this.setRowValue("Insured_Line3", value); }
        }

        public string InsuredLine4
        {
            get { return (string)row["Insured_Line4"]; }
            set { this.setRowValue("Insured_Line4", value); }
        }

        public string InsuredLine5
        {
            get { return (string)row["Insured_Line5"]; }
            set { this.setRowValue("Insured_Line5", value); }
        }

        public string InsuredName
        {
            get { return (string)row["Insured_Name"]; }
            set { this.setRowValue("Insured_Name", value); }
        }

        public string InsuredVehicle
        {
            get { return (string)row["Insured_Vehicle"]; }
            set { this.setRowValue("Insured_Vehicle", value); }
        }

        public string InsuredVehicleLine2
        {
            get { return (string)row["Insured_Vehicle_Line2"]; }
            set { this.setRowValue("Insured_Vehicle_Line2", value); }
        }

        public string InsuredVehicleLine3
        {
            get { return (string)row["Insured_Vehicle_Line3"]; }
            set { this.setRowValue("Insured_Vehicle_Line3", value); }
        }

        public string InsuredVehicleName
        {
            get { return (string)row["Insured_Vehicle_Name"]; }
            set { this.setRowValue("Insured_Vehicle_Name", value); }
        }

        public string LossPayee
        {
            get { return (string)row["Loss_Payee"]; }
            set { this.setRowValue("Loss_Payee", value); }
        }

        public string MedPayCoverage
        {
            get { return (string)row["Med_Pay_Coverage"]; }
            set { this.setRowValue("Med_Pay_Coverage", value); }
        }

        public string Owner
        {
            get { return (string)row["Owner"]; }
            set { this.setRowValue("Owner", value); }
        }

        public string OwnerLine2
        {
            get { return (string)row["Owner_Line2"]; }
            set { this.setRowValue("Owner_Line2", value); }
        }

        public string OwnerLine3
        {
            get { return (string)row["Owner_Line3"]; }
            set { this.setRowValue("Owner_Line3", value); }
        }

        public string OwnerLine4
        {
            get { return (string)row["Owner_Line4"]; }
            set { this.setRowValue("Owner_Line4", value); }
        }

        public string OwnerLine5
        {
            get { return (string)row["Owner_Line5"]; }
            set { this.setRowValue("Owner_Line5", value); }
        }

        public string OwnerName
        {
            get { return (string)row["Owner_Name"]; }
            set { this.setRowValue("Owner_Name", value); }
        }

        public string PIPEssServicesCoverage
        {
            get { return (string)row["PIP_Ess_Services_Coverage"]; }
            set { this.setRowValue("PIP_Ess_Services_Coverage", value); }
        }

        public string PIPMedCoverage
        {
            get { return (string)row["PIP_Med_Coverage"]; }
            set { this.setRowValue("PIP_Med_Coverage", value); }
        }

        public string PIPRehabCoverage
        {
            get { return (string)row["PIP_Rehab_Coverage"]; }
            set { this.setRowValue("PIP_Rehab_Coverage", value); }
        }
        
        public string PIPWagesCoverage
        {
            get { return (string)row["PIP_Wages_Coverage"]; }
            set { this.setRowValue("PIP_Wages_Coverage", value); }
        }

        public string PropertyDamageCoverage
        {
            get { return (string)row["Property_Damage_Coverage"]; }
            set { this.setRowValue("Property_Damage_Coverage", value); }
        }

        public string UIMCoverage
        {
            get { return (string)row["UIM_Coverage"]; }
            set { this.setRowValue("UIM_Coverage", value); }
        }

        public string UMCoverage
        {
            get { return (string)row["UM_Coverage"]; }
            set { this.setRowValue("UM_Coverage", value); }
        }

        public double VersionNo
        {
            get { return (double)row["Version_No"]; }
            set { this.setRowValue("Version_No", value); }
        }
        #endregion

        #region Misc Methods
        public void PopulateFromClaim(Claim claim)
        {
            //clear all fields
            this.ClearAllFields();

            //get coverage descriptions
            this.BICoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Auto_Bodily_Injury); //2
            this.UIMCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Underinsured_Motorist); //5
            this.PIPMedCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Medical); //9
            this.PropertyDamageCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Property_Damage); //1
            this.CompDed = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Comprehensive); //7
            this.PIPRehabCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Rehab); //10
            this.MedPayCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Medical_Payments); //3
            this.CollisionDed = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Collision); //6
            this.PIPWagesCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Wages); //11
            this.UMCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.Uninsured_Motorist); //4
            this.PIPEssServicesCoverage = claim.CoverageDescriptions((int)modGeneratedEnums.PolicyCoverageType.PIP_Essential_Services); //12

            //populate persons
            this.Insured = claim.InsuredPerson.ExtendedDescription();
            this.Owner = claim.OwnerPerson.ExtendedDescription();
            this.Driver = claim.DriverPerson.ExtendedDescription();

            //populate insured vehicle
            this.InsuredVehicle = claim.InsuredVehicle.LongName;

            //populate loss payee and agent
            this.LossPayee = claim.LossPayeeDescription;
            this.Agent = claim.AgentDescription;

            //update claim report row
            this.Update();
        }

        public void ClearAllFields()
        {
            //clear all fields
     
            //get coverage descriptions
            this.BICoverage = "";
            this.UIMCoverage = "";
            this.PIPMedCoverage = "";
            this.PropertyDamageCoverage = "";
            this.CompDed = "";
            this.PIPRehabCoverage = "";
            this.MedPayCoverage = "";
            this.CollisionDed = "";
            this.PIPWagesCoverage = "";
            this.UMCoverage = "";
            this.PIPEssServicesCoverage = "";

            //populate persons
            this.Insured = "";
            this.Owner = "";
            this.Driver = "";

            //populate insured vehicle
            this.InsuredVehicle = "";

            //populate loss payee and agent
            this.LossPayee = "";
            this.Agent = "";
        }
        #endregion

        #region Nonimplemented
        public override string TableName
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}

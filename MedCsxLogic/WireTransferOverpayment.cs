﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class WireTransferOverpayment : TransactionReferenceTable
    {

        #region Constructors
        public WireTransferOverpayment() : base() { }
        public WireTransferOverpayment(int id) : base(id) { }
        public WireTransferOverpayment(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Wire_Transfer_Overpayment"; }
        }

        public DateTime DateReceived
        {
            get { return (DateTime)row["Date_Received"]; }
            set { this.setRowValue("Date_Received", value); }
        }

        public int DraftNo
        {
            get { return (int)row["Draft_No"]; }
            set { this.setRowValue("Draft_No", value); }
        }

        public string Notes
        {
            get { return (string)row["Notes"]; }
            set { this.setRowValue("Notes", value); }
        }

        public string ReceivedFrom
        {
            get { return (string)row["Received_From"]; }
            set { this.setRowValue("Received_From", value); }
        }

        public int TransactionId
        {
            get { return (int)row["Transaction_Id"]; }
            set { this.setRowValue("Transaction_Id", value); }
        }

        public int VoidTransactionId
        {
            get { return (int)row["Void_Transaction_Id"]; }
            set { this.setRowValue("Void_Transaction_Id", value); }
        }

        public int WireTransferId
        {
            get { return (int)row["Wire_Transfer_Id"]; }
            set { this.setRowValue("Wire_Transfer_Id", value); }
        }

        public int WireTransferOverpaymentId
        {
            get { return (int)row["Wire_Transfer_Overpayment_Id"]; }
            set { this.setRowValue("Wire_Transfer_Overpayment_Id", value); }
        }
        #endregion

        #region Methods
        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }
        #endregion
    }
}

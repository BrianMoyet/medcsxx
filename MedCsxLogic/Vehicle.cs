﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Vehicle:ClaimTable
    {

        private static clsDatabase db = new clsDatabase();

        #region Class Variables
        private State m_LossPayeeState;
        private State m_LicensePlateState;
        private State m_LocationState;
        #endregion

        #region Constructors
        public Vehicle() : base() { }
        public Vehicle(int id) : base(id) { }
        public Vehicle(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Vehicle"; }
        }

        public string Color
        {
            get { return (string)row["Color"]; }
            set { this.setRowValue("Color", value); }
        }

        public bool isDrivable
        {
            get { return (bool)row["Is_Drivable"]; }
            set { this.setRowValue("Is_Drivable", value); }
        }

        public bool IsFraudulent
        {
            get { return (bool)row["Flag_Fraud"]; }
            set { this.setRowValue("Flag_Fraud", value); }
        }
        
        public int PolicyVehicleId
        {
            get { return (int)row["Policy_Vehicle_Id"]; }
            set { this.setRowValue("Policy_Vehicle_Id", value); }
        }

        public string YearMade
        {
            get { return (string)row["Year_Made"]; }
            set { this.setRowValue("Year_Made", value); }
        }

        public string Manufacturer
        {
            get { return (string)row["Manufacturer"]; }
            set { this.setRowValue("Manufacturer", value); }
        }

        public string Model
        {
            get { return (string)row["Model"]; }
            set { this.setRowValue("Model", value); }
        }

        public string LicensePlateNo
        {
            get { return (string)row["License_Plate_No"]; }
            set { this.setRowValue("License_Plate_No", value); }
        }

        public int LicensePlateStateId
        {
            get { return (int)row["License_Plate_State_Id"]; }
            set 
            { 
                this.setRowValue("License_Plate_State_Id", value);
                m_LicensePlateState = null;
            }
        }

        public State LicensePlateState
        {
            get { return m_LicensePlateState == null ? m_LicensePlateState = new State(LicensePlateStateId) : m_LicensePlateState; }
        }

        public string VIN
        {
            get { return (string)row["VIN"]; }
            set { this.setRowValue("VIN", value); }
        }

        public string LocationContactFirstName
        {
            get { return (string)row["Location_Contact_First_Name"]; }
            set { this.setRowValue("Location_Contact_First_Name", value); }
        }

        public string LocationContactLastName
        {
            get { return (string)row["Location_Contact_Last_Name"]; }
            set { this.setRowValue("Location_Contact_Last_Name", value); }
        }

        public string LocationContactEmailAddress
        {
            get { return (string)row["Location_Contact_Email_Address"]; }
            set { this.setRowValue("Location_Contact_Email_Address", value); }
        }

        public string LocationAddress1
        {
            get { return (string)row["Location_Address1"]; }
            set { this.setRowValue("Location_Address1", value); }
        }

        public string LocationAddress2
        {
            get { return (string)row["Location_Address2"]; }
            set { this.setRowValue("Location_Address2", value); }
        }

        public string LocationCity
        {
            get { return (string)row["Location_City"]; }
            set { this.setRowValue("Location_City", value); }
        }

        public int LocationStateId
        {
            get { return (int)row["Location_State_Id"]; }
            set 
            { 
                this.setRowValue("Location_State_Id", value);
                m_LocationState = null;
            }
        }

        public State LocationState
        {
            get { return m_LocationState == null ? m_LocationState = new State(LocationStateId) : m_LocationState; }
        }

        public string LocationZipcode
        {
            get { return (string)row["Location_Zipcode"]; }
            set { this.setRowValue("Location_Zipcode", value); }
        }

        public string LocationPhone1
        {
            get { return (string)row["Location_Phone1"]; }
            set { this.setRowValue("Location_Phone1", value); }
        }

        public string LocationPhone2
        {
            get { return (string)row["Location_Phone2"]; }
            set { this.setRowValue("Location_Phone2", value); }
        }

        public string LossPayeeName1
        {
            get { return (string)row["Loss_Payee_Name1"]; }
            set { this.setRowValue("Loss_Payee_Name1", value); }
        }

        public string LossPayeeName2
        {
            get { return (string)row["Loss_Payee_Name2"]; }
            set { this.setRowValue("Loss_Payee_Name2", value); }
        }

        public string LossPayeeAddress1
        {
            get { return (string)row["Loss_Payee_Address1"]; }
            set { this.setRowValue("Loss_Payee_Address1", value); }
        }

        public string LossPayeeAddress2
        {
            get { return (string)row["Loss_Payee_Address2"]; }
            set { this.setRowValue("Loss_Payee_Address2", value); }
        }

        public string LossPayeeCity
        {
            get { return (string)row["Loss_Payee_City"]; }
            set { this.setRowValue("Loss_Payee_City", value); }
        }

        public int LossPayeeStateId
        {
            get { return (int)row["Loss_Payee_State_Id"]; }
            set
            {
                this.setRowValue("Loss_Payee_State_Id", value);
                m_LossPayeeState = null;
            }
        }

        public State LossPayeeState
        {
            get { return m_LossPayeeState == null ? m_LossPayeeState = new State(LossPayeeStateId) : m_LossPayeeState; }
        }

        public string LossPayeeZipcode
        {
            get { return (string)row["Loss_Payee_Zipcode"]; }
            set { this.setRowValue("Loss_Payee_Zipcode", value); }
        }
        
        public string LossPayeeLoanNo
        {
            get { return (string)row["Loss_Payee_Loan_No"]; }
            set { this.setRowValue("Loss_Payee_Loan_No", value); }
        }

        public string LossPayeePhone
        {
            get { return (string)row["Loss_Payee_Phone"]; }
            set { this.setRowValue("Loss_Payee_Phone", value); }
        }

        public string LossPayeeFax
        {
            get { return (string)row["Loss_Payee_Fax"]; }
            set { this.setRowValue("Loss_Payee_Fax", value); }
        }

        public string LossPayeeCorrectedPhone
        {
            get { return (string)row["Loss_Payee_Corrected_Phone"]; }
            set { this.setRowValue("Loss_Payee_Corrected_Phone", value); }
        }

        public string ManufactureCode
        {
            get { return (string)row["Manufacture_Code"]; }
            set { this.setRowValue("Manufacture_Code", value); }
        }

        public string ModelCode
        {
            get { return (string)row["Model_Code"]; }
            set { this.setRowValue("Model_Code", value); }
        }
        #endregion

        #region Methods
        public static string VehicleName(int vehicleId)
        {
            return db.GetStringFromStoredProcedure("usp_Get_Vehicle_Name_For_Id", vehicleId);
        }

        public static string VehicleShortName(int vehicleId)
        {
            return db.GetStringFromStoredProcedure("usp_Get_Vehicle_Short_Name_For_Id", vehicleId);
        }

        public static Vehicle Named(string name, int claimId)
        {
            return new Vehicle(db.GetIntFromStoredProcedure("usp_Get_Vehicle_Id_For_Name", name, claimId));
        }

        public string LossPayeeDescription()
        {
            /*Returns a loss payee description (name, address, phone, etc.)
             * as one string with embedded crlf's
            */

            string s, phone;

            s = this.LossPayeeName1;
            if (this.LossPayeeName2 != "")
                s += Environment.NewLine + this.LossPayeeName2;

            if (this.LossPayeeAddress1 != "")
                s += Environment.NewLine + this.LossPayeeAddress1;
            
            if (this.LossPayeeAddress2 != "")
                s += Environment.NewLine + this.LossPayeeAddress2;
            
            if (this.LossPayeeCity != "")
                s += Environment.NewLine + this.LossPayeeCity + ", " + 
                    this.LossPayeeState.Abbreviation + " " + this.LossPayeeZipcode;
            
            if (this.LossPayeeLoanNo != "")
                s += Environment.NewLine + "Loan No: "+this.LossPayeeLoanNo;
            
            phone = this.LossPayeePhone;
            if (this.LossPayeeCorrectedPhone != "")
                phone = this.LossPayeeCorrectedPhone;

            if (phone != "")
                s += Environment.NewLine + "Phone: " + phone;

            return s;
        }

        public string Name
        {
            get { 
                if (this.Manufacturer.Length>=7)
                {
                    if (this.Manufacturer.Substring(0, 7).ToUpper() == "UNKNOWN")
                        return this.Manufacturer;
                    //The assumption is that if the manufacturer is unknown, the car is unknown
                }

                if (this.VIN == "")
                {
                    //The vehicle has no VIN set - return year, make, model
                    return (this.YearMade + " " + this.Manufacturer + " " + this.Model).Trim();
                }
                else
                {
                    //Return vehicle year, make, model, and VIN
                    return (this.YearMade + " " + this.Manufacturer + " " + this.Model + " VIN: " + this.VIN).Trim();
                }
            }
        }

        public string ShortName
        {
            get
            {
                if (this.Manufacturer.Length >= 7)
                {
                    if (this.Manufacturer.Substring(0, 7).ToUpper() == "UNKNOWN")
                        return this.Manufacturer;
                    //The assumption is that if the manufacturer is unknown, the car is unknown
                }

                    //Return vehicle year, make, model
                return (this.YearMade + " " + this.Manufacturer + " " + this.Model).Trim();
            }
        }

        public string LongName
        {
            get
            {
                string name = this.ShortName;
                if (this.VIN != "")
                    name += Environment.NewLine + "VIN: " + this.VIN;

                if (this.Color != "")
                    name += Environment.NewLine + "Color: " + this.Color;

                name += Environment.NewLine + "Drivable: " + this.isDrivable.ToString();
                return name;
            }
        }

        public Claimant Claimant
        {
            get
            {
                //Returns claimant corresponding to this vehicle
                PropertyDamage p = this.PropertyDamage;

                if (p == null)
                    return null;

                Reserve r = new Reserve(p.ReserveId);
                return r.Claimant;
            }
        }

        public PropertyDamage PropertyDamage
        {
            get
            {
                //Returns property damage object corresponding to this vehicle
                DataTable rows = new DataTable();

                if (this.Id != 0)
                    rows = db.ExecuteStoredProcedure("usp_Get_Property_Damage_For_Vehicle_Id", this.Id);

                return rows.Rows.Count == 0 ? null : new PropertyDamage(rows, rows.Rows[0]);
            }
        }

        public Person OwnerPerson
        {
            get
            {
                PropertyDamage p = this.PropertyDamage;

                if (p == null)
                {
                    if (this.PolicyVehicleId == 0)
                        return null;
                    else
                        return this.Claim.OwnerPerson;
                }
                return p.OwnerPerson();
            }
        }

        public Person DriverPerson
        {
            get
            {
                PropertyDamage p = this.PropertyDamage;

                if (p == null)
                {
                    if (this.PolicyVehicleId == 0)
                        return null;
                    else
                        return this.Claim.OwnerPerson;
                }
                return p.DriverPerson();

            }
        }

        public void UpdateUnknownVehicles()
        {
            //Adds additional unknown vehicle if necessary
            string[] a;
            int result;

            a = this.Manufacturer.Split(' ');
            if (a[0] == "Unknown")
            {
                if (a.Length - 1 > 0)
                {
                    if (int.TryParse(a[1], out result))
                        this.Claim.InsertUnknownVehicleIfNecessary(int.Parse(a[1]) + 1);

                }
                else
                    this.Claim.InsertUnknownVehicleIfNecessary(2);
            }
        }

        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_Vehicle_Row";
        }

        public bool Set_ISO_Code(string manufacturer, string model, ref string closestMatch)
        {
            string uri = System.Configuration.ConfigurationManager.AppSettings["VehicleCodeXML"];
            bool foundIt = false;

            foreach (var veh in XmlHelper.StreamVehicles(uri))
            {
                if ((manufacturer.ToUpper() == veh.Make) || 
                    ((manufacturer.Length > 4) && (manufacturer.ToUpper().Substring(0, 4) == veh.Make.Substring(0, 4))))
                {
                    closestMatch = veh.Make;
                    if (model.ToUpper() == veh.Model)
                    {
                        this.ManufactureCode = veh.Make_Code;
                        this.ModelCode = veh.Model_Code;
                        foundIt = true;
                        break;
                    }
                    else if (model.ToUpper().Substring(0, 2) == veh.Model.Substring(0, 2))
                        closestMatch += " " + veh.Model;
                }
            }
            return foundIt;
        }

        public class xVehicle
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Make_Code { get; set; }
            public string Model_Code { get; set; }
        }

        public static class XmlHelper
        {
            public static IEnumerable<xVehicle > StreamVehicles(string uri)
            {
                using (XmlReader reader = XmlReader.Create(uri))
                {
                    string model = null;
                    string makeCode = null;
                    string modelCode = null;
                    string description = null;

                    reader.MoveToContent();
                    while (reader.Read())
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ROW"))
                        {
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "VEHICLE_MAKE_CODE"))
                                {
                                    makeCode = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "VEHICLE_MODEL_CODE"))
                                {
                                    modelCode = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "MAKE_MODEL_DESCRIPTION"))
                                {
                                    description = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            while (reader.Read())
                            {
                                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "MODEL_DESCRIPTION"))
                                {
                                    model = reader.ReadElementContentAsString();
                                    break;
                                }
                            }
                            string[] makeModDesc=description.Split(' ');
                            yield return new xVehicle()
                            {
                                Make = makeModDesc[0],
                                Model = model,
                                Make_Code = makeCode,
                                Model_Code = modelCode
                            };
                        }
                    }
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Vendor:MedCsXTable
    {

        #region Class Variables
        private State m_State;
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #endregion

        #region Constructors
        public Vendor() : base() { }
        public Vendor(int id) : base(id) { }
        public Vendor(Hashtable row) : base(row) { }
        public Vendor(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Vendor"; }
        }

        public State State
        {
            get { return m_State == null ? m_State = new State(this.StateId) : m_State; }
        }

        public string Name
        {
            get { return (string)row["Name"]; }
            set { this.setRowValue("Name", value); }
        }

        public string Address1
        {
            get { return (string)row["Address1"]; }
            set { this.setRowValue("Address1", value); }
        }

        public string Address2
        {
            get { return (string)row["Address2"]; }
            set { this.setRowValue("Address2", value); }
        }

        public string City
        {
            get { return (string)row["City"]; }
            set { this.setRowValue("City", value); }
        }

        public int StateId
        {
            get { return (int)row["State_Id"]; }
            set { this.setRowValue("State_Id", value); }
        }

        public string Zipcode
        {
            get { return (string)row["Zipcode"]; }
            set { this.setRowValue("Zipcode", value); }
        }

        public string Phone
        {
            get { return (string)row["Phone"]; }
            set { this.setRowValue("Phone", value); }
        }

        public string Fax
        {
            get { return (string)row["Fax"]; }
            set { this.setRowValue("Fax", value); }
        }

        public string Email
        {
            get { return (string)row["Email"]; }
            set { this.setRowValue("Email", value); }
        }

        public bool IsFraudulent
        {
            get { return (bool)row["Flag_Fraud"]; }
            set { this.setRowValue("Flag_Fraud", value); }
        }
        
        public string TaxId
        {
            get { return (string)row["Tax_Id"]; }
            set { this.setRowValue("Tax_Id", value); }
        }

        public int VendorSubTypeId
        {
            get { return (int)row["Vendor_Sub_Type_Id"]; }
            set { this.setRowValue("Vendor_Sub_Type_Id", value); }
        }

        public bool TaxIdRequired
        {
            get { return (bool)row["Tax_Id_Required"]; }
            set { this.setRowValue("Tax_Id_Required", value); }
        }

        public bool Requires1099
        {
            get { return (bool)row["Requires_1099"]; }
            set { this.setRowValue("Requires_1099", value); }
        }
        #endregion

        #region Misc Methods
        public static List<Hashtable> VendorsOfType(int vendorSubTypeId)
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Vendors1", vendorSubTypeId);
        }

        public static int getVendorTypeIdForVendorId(int vendorId)
        {
            //returns vendor type ID for vendor ID
            return db.GetIntFromStoredProcedure("usp_Get_Vendor_Type_Id_For_Vendor_Id", vendorId);
        }

        public static void DeleteVendor(int vendorId)
        {
            db.ExecuteStoredProcedure("usp_Delete_Vendor", vendorId);
        }

        public static int getVendorIdForTaxId(string taxId)
        {
            //returns vendor ID for tax ID.  returns zero if no vendor found.
            return db.GetIntFromStoredProcedure("usp_Get_Vendor_Id_For_Tax_Id", taxId);
        }

        public string Description
        {
            get
            {
                string s = this.Name;

                if (this.Address1 != "")
                    s += Environment.NewLine + this.Address1;

                if (this.Address2 != "")
                    s += Environment.NewLine + this.Address2;

                if ((this.City != "") || (this.StateId != 0) || (this.Zipcode != ""))
                    s += Environment.NewLine + this.City + ", " + this.State.Abbreviation + " " + this.Zipcode;

                if (this.Phone != "")
                    s += Environment.NewLine + "Phone: " + this.Phone;

                if (this.Fax != "")
                    s += Environment.NewLine + "Fax: " + this.Fax;

                if (this.Email != "")
                    s += Environment.NewLine + "Email: " + this.Email;

                return s;
            }
        }

        public VendorSubType VendorSubType()
        {
            return new VendorSubType(this.VendorSubTypeId);
        }

        public static Vendor Named(string vendorName)
        {
            if (vendorName == "")
                return new Vendor(0);

            DataTable rows = db.ExecuteStoredProcedure("usp_Vendor_Named", vendorName);
            if (rows.Rows.Count == 0)
                return null;
            return new Vendor(rows, rows.Rows[0]);
        }

        public bool isClaimantAttorney
        {
            get { return this.VendorSubType().VendorTypeId == (int)modGeneratedEnums.VendorType.Claimant_Attorney; }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class AppraisalInvestigationType:TypeTable
    {
        #region Constructors
        public AppraisalInvestigationType() : base() { }
        public AppraisalInvestigationType(int id) : base(id) { }
        public AppraisalInvestigationType(string description) : base(description) { }
        public AppraisalInvestigationType(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Appraisal_Investigation_Type";
            }
        }
     }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class DraftPrint:MedCsXTable
    {

        private static modLogic ml=new modLogic();
        #region Constructors
        public DraftPrint() : base() { }
        public DraftPrint(int id) : base(id) { }
        public DraftPrint(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Draft_Print"; }
        }

        public string ClaimNo
        {
            get { return (string)row["Claim_No"]; }
            set { this.setRowValue("Claim_No", value); }
        }

        public string Coverage
        {
            get { return (string)row["Coverage"]; }
            set { this.setRowValue("Coverage", value); }
        }

        public string DraftAmount
        {
            get { return (string)row["Draft_Amount"]; }
            set { this.setRowValue("Draft_Amount", value); }
        }

        public string DraftAmountDescription
        {
            get { return (string)row["Draft_Amount_Description"]; }
            set { this.setRowValue("Draft_Amount_Description", value); }
        }

        public string DraftAmountWithStars
        {
            get { return (string)row["Draft_Amount_With_Stars"]; }
            set { this.setRowValue("Draft_Amount_With_Stars", value); }
        }

        public string DraftDate
        {
            get { return (string)row["Draft_Date"]; }
            set { this.setRowValue("Draft_Date", value); }
        }

        public int DraftId
        {
            get { return (int)row["Draft_Id"]; }
            set { this.setRowValue("Draft_Id", value); }
        }

        public string DraftLongDate
        {
            get { return (string)row["Draft_Long_Date"]; }
            set { this.setRowValue("Draft_Long_Date", value); }
        }
        
        public string DraftNo
        {
            get { return (string)row["Draft_No"]; }
            set { this.setRowValue("Draft_No", value); }
        }

        public int DraftPrintId
        {
            get { return (int)row["Draft_Print_Id"]; }
            set { this.setRowValue("Draft_Print_Id", value); }
        }

        public string Employee
        {
            get { return (string)row["Employee"]; }
            set { this.setRowValue("Employee", value); }
        }

        public string ExplanationOfProceeds
        {
            get { return (string)row["Explanation_Of_Proceeds"]; }
            set { this.setRowValue("Explanation_Of_Proceeds", value); }
        }

        public bool FromFoxpro
        {
            get { return (bool)row["From_Foxpro"]; }
            set { this.setRowValue("From_Foxpro", value); }
        }

        public string Insured
        {
            get { return (string)row["Insured"]; }
            set { this.setRowValue("Insured", value); }
        }

        public string LossDate
        {
            get { return (string)row["Loss_Date"]; }
            set { this.setRowValue("Loss_Date", value); }
        }

        public string MailToLine1
        {
            get { return (string)row["Mail_To_Line_1"]; }
            set { this.setRowValue("Mail_To_Line_1", value); }
        }

        public string MailToLine2
        {
            get { return (string)row["Mail_To_Line_2"]; }
            set { this.setRowValue("Mail_To_Line_2", value); }
        }

        public string MailToLine3
        {
            get { return (string)row["Mail_To_Line_3"]; }
            set { this.setRowValue("Mail_To_Line_3", value); }
        }

        public string MailToLine4
        {
            get { return (string)row["Mail_To_Line4"]; }
            set { this.setRowValue("Mail_To_Line4", value); }
        }

        public string NonNegotiable
        {
            get { return (string)row["Non_Negotiable"]; }
            set { this.setRowValue("Non_Negotiable", value); }
        }

        public string Payee
        {
            get { return (string)row["Payee"]; }
            set { this.setRowValue("Payee", value); }
        }

        public string PolicyNo
        {
            get { return (string)row["Policy_No"]; }
            set { this.setRowValue("Policy_No", value); }
        }

        public string RoutingSymbols
        {
            get { return (string)row["Routing_Symbols"]; }
            set { this.setRowValue("Routing_Symbols", value); }
        }

        public string TaxId
        {
            get { return (string)row["Tax_Id"]; }
            set { this.setRowValue("Tax_Id", value); }
        }
        #endregion

        #region Misc Methods
        public Claim Claim()
        {
            return new Claim(modLogic.ExtractClaimId(this.ClaimNo));
        }
        #endregion
    }
}

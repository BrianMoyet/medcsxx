﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{

    public class ClaimAuditItemTypeState:TypeTable
    {
        #region Class Variables
        private State m_State = null;
        #endregion

        #region Constructors
        public ClaimAuditItemTypeState() : base() { }
        public ClaimAuditItemTypeState(int id) : base(id) { }
        public ClaimAuditItemTypeState(Hashtable row) : base(row) { }
        public ClaimAuditItemTypeState(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Claim_Audit_Item_Type_State";
            }
        }

        public State State
        {
            get { return m_State == null ? m_State = new State(this.StateId) : m_State; }
        }

        public int ClaimAuditItemTypeId
        {
            get { return (int)row["Claim_Audit_Item_Type_Id"]; }
            set { this.setRowValue("Claim_Audit_Item_Type_Id", value); }
        }

        public int StateId
        {
            get { return (int)row["State_Id"]; }
            set { this.setRowValue("State_Id", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    class Language:TypeTable
    {

        #region Constructors
        public Language() : base() { }
        public Language(int id) : base(id) { }
        public Language(string description) : base(description) { }
        public Language(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Language";
            }
        }

        #region Column Names
        public int LanguageId
        {
            get { return (int)row["Language_Id"]; }
            set { this.setRowValue("Language_Id", value); }
        }
        #endregion

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public abstract class TransactionReferenceTable:MedCsXTable
    {
        #region Constructors
        public TransactionReferenceTable() : base() { }
        public TransactionReferenceTable(int id) : base(id) { }
        public TransactionReferenceTable(Hashtable row) : base(row) { }
        public TransactionReferenceTable(DataTable Table, DataRow Row) : base(Table, Row) { }
        #endregion


        public virtual Transaction Transaction()
        {
            return new Transaction();
        }

        #region Methods
        public int ReserveId
        {
            get { return this.Transaction().ReserveId; }
        }

        public Reserve Reserve()
        {
            return this.Transaction().Reserve;
        }

        public int ClaimantId
        {
            get { return this.Transaction().ClaimantId; }
        }

        public Claimant Claimant()
        {
            return this.Transaction().Claimant;
        }

        public int ClaimId
        {
            get { return this.Transaction().ClaimId; }
        }

        public Claim Claim()
        {
            return this.Transaction().Claim;
        }
        #endregion
    }
}

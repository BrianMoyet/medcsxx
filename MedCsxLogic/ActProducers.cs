﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ActProducers:PersonalLinesTable
    {
        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public ActProducers() : base() { }
        public ActProducers(string noProducer)
            : base(noProducer)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Act_Producer", noProducer);
            if (rows.Rows.Count > 0)
                row = db.DataTableRowToHashtable(rows, rows.Rows[0]);
            else
                new Hashtable();
        }
        #endregion


        public override string TableName
        {
            get { return "PersonalLines.dbo.Act_Producers"; }
        }

    }
}

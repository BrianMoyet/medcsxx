﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Litigation : ClaimantTable
    {

        #region Class Variables
        public const int defenseAttorneyType = (int)modGeneratedEnums.VendorSubType.Defense_Attorney;
        public const int plaintiffAttorneyType = (int)modGeneratedEnums.VendorSubType.Claimant_Attorney;
        public const int courtLocationType = (int)modGeneratedEnums.VendorSubType.Courts_Municipalities;

        private LitigationStatus m_LitigationStatus = null;
        private Vendor m_DefenseAttorney = null;
        private Vendor m_PlaintiffAttorney = null;
        private Vendor m_CourtLocation = null;

        private static clsDatabase db = new clsDatabase();
        #endregion

        #region Constructors
        public Litigation() : base() { }
        public Litigation(int id) : base(id) { }
        public Litigation(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Litigation"; }
        }

        public int LitigationStatusId
        {
            get { return (int)row["Litigation_Status_Id"]; }
            set { this.setRowValue("Litigation_Status_Id", value); }
        }

        public override int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public int DefenseAttorneyId
        {
            get { return (int)row["Defense_Attorney_Id"]; }
            set { this.setRowValue("Defense_Attorney_Id", value); }
        }

        public int PlaintiffAttorneyId
        {
            get { return (int)row["Plaintiff_Attorney_Id"]; }
            set { this.setRowValue("Plaintiff_Attorney_Id", value); }
        }

        public int CourtLocationId
        {
            get { return (int)row["Court_Location_Id"]; }
            set { this.setRowValue("Court_Location_Id", value); }
        }

        public DateTime DateFiled
        {
            get { return (DateTime)row["Date_Filed"]; }
            set { this.setRowValue("Date_Filed", value); }
        }
        
        public DateTime DateServed
        {
            get { return (DateTime)row["Date_Served"]; }
            set { this.setRowValue("Date_Served", value); }
        }
        
        public double Resolution_Amount
        {
            get { return Convert.ToDouble(row["Resolution_Amount"]); }
            set { this.setRowValue("Resolution_Amount", value); }
        }

        public string Comments
        {
            get { return (string)row["Comments"]; }
            set { this.setRowValue("Comments", value); }
        }
        #endregion
        
        #region Methods
        public LitigationStatus LitigationStatus
        {
            get { return this.m_LitigationStatus == null ? this.m_LitigationStatus = new LitigationStatus(this.LitigationStatusId) : this.m_LitigationStatus; }
        }

        public Vendor DefenseAttorney
        {
            get { return this.m_DefenseAttorney == null ? this.m_DefenseAttorney = new Vendor(this.DefenseAttorneyId) : this.m_DefenseAttorney; }
        }

        public Vendor PlaintiffAttorney
        {
            get { return this.m_PlaintiffAttorney == null ? this.m_PlaintiffAttorney = new Vendor(this.PlaintiffAttorneyId) : this.m_PlaintiffAttorney; }
        }

        public Vendor CourtLocation
        {
            get { return this.m_CourtLocation == null ? this.m_CourtLocation = new Vendor(this.CourtLocationId) : this.m_CourtLocation; }
        }

        public new List<Hashtable> Reserves()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Litigation_Reserves", this.Id);
        }

        public List<Hashtable> Defendents()
        {
            List<Hashtable> allDefendents = db.ExecuteStoredProcedureReturnHashList("usp_Get_Litigation_Defendents", this.Id);

            foreach (Hashtable defendent in allDefendents)
            {
                if ((int)defendent["Person_Id"] == 0)
                    defendent.Add("Name", (string)defendent["Vendor_Name"]);    //use vendor name
                else if ((int)defendent["Vendor_Id"] == 0)
                    defendent.Add("Name", (string)defendent["Person_Name"]);    //use person name
            }
            return allDefendents;
        }

        public List<Hashtable> Plaintiffs()
        {
            List<Hashtable> allPlaintiffs = db.ExecuteStoredProcedureReturnHashList("usp_Get_Litigation_Plaintiffs", this.Id);

            foreach (Hashtable plaintiff in allPlaintiffs)
            {
                if ((int)plaintiff["Person_Id"] == 0)
                    plaintiff.Add("Name", (string)plaintiff["Vendor_Name"]);    //use vendor name
                else if ((int)plaintiff["Vendor_Id"] == 0)
                    plaintiff.Add("Name", (string)plaintiff["Person_Name"]);    //use person name
            }
            return allPlaintiffs;
        }

        public static List<Hashtable> LitigationForClaim(int claim_id)
        {   //returns the litigation items for the designated claim
            List<Hashtable> allLitigation = db.ExecuteStoredProcedureReturnHashList("usp_Get_Litigation", claim_id);

            foreach (Hashtable lit in allLitigation)        //change resolution amount to currency
                lit["Resolution_Amount"] = ((double)((decimal)lit["Resolution_Amount"])).ToString("c");
            return allLitigation;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ExternalSite : MedCsXTable
    {
        #region Form Initialization
        public ExternalSite() : base() { }
        public ExternalSite(int id) : base(id) { }
        #endregion


        public override string TableName
        {
            get { return "User_3rd_Party_Sites"; }
        }

        #region Column Names
        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }

        public int SiteId
        {
            get { return (int)row["Site_Id"]; }
            set { this.setRowValue("Site_Id", value); }
        }

        public string SiteName
        {
            get { return (string)row["Site_Name"]; }
            set { this.setRowValue("Site_Name", value); }
        }

        public string SiteAddress
        {
            get { return (string)row["Site_Address"]; }
            set { this.setRowValue("Site_Address", value); }
        }

        public string LogonId
        {
            get { return (string)row["Logon_Id"]; }
            set { this.setRowValue("Logon_Id", value); }
        }

        public string LogonPassword
        {
            get { return (string)row["Logon_Password"]; }
            set { this.setRowValue("Logon_Password", value); }
        }

        public bool IsApproved
        {
            get { return (bool)row["Is_Approved"]; }
            set { this.setRowValue("Is_Approved", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Demand : ReserveTable
    {

        #region Contructors
        public Demand() : base() { }
        public Demand(int id) : base(id) { }
        public Demand(Hashtable row) : base(row) { }
        #endregion

        #region Class Variables
        public const  int AttorneyVendorType = (int)modGeneratedEnums.VendorSubType.Claimant_Attorney;
        private Vendor myVendor = null;     //vendor object
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Demand"; }
        }

        public int DemandId
        {
            get { return (int)row["Demand_Id"]; }
            set { this.setRowValue("Demand_Id", value); }
        }

        public int VendorId
        {
            get { return (int)row["Vendor_Id"]; }
            set { this.setRowValue("Vendor_Id", value); }
        }

        public double DemandAmount
        {
            get { return (double)((decimal)row["Demand_Amount"]); }
            set { this.setRowValue("Demand_Amount", value); }
        }

        public DateTime DemandDate
        {
            get { return (DateTime)row["Demand_Date"]; }
            set { this.setRowValue("Demand_Date", value); }
        }

        public DateTime ReceivedDate
        {
            get { return (DateTime)row["Received_Date"]; }
            set { this.setRowValue("Received_Date", value); }
        }

        public DateTime DueDate
        {
            get { return (DateTime)row["Due_Date"]; }
            set { this.setRowValue("Due_Date", value); }
        }
        #endregion

        #region Methods
        public Vendor Vendor
        {
            get { return this.myVendor == null ? this.myVendor = new Vendor(this.VendorId) : this.myVendor; }
        }

        public static List<Hashtable> DemandHistory(int vendor_id, int reserve_id)
        {   //return the demand history (including offered for the specified vendor ID and reserve ID
            List<Hashtable> history = db.ExecuteStoredProcedureReturnHashList("usp_Get_Demand_History", vendor_id, reserve_id);

            foreach (Hashtable hItem in history)
            {
                if ((DateTime)hItem["Received_Date"] == ml.DEFAULT_DATE)
                    hItem["Received_Date"] = "";
                if ((DateTime)hItem["Due_Date"] == ml.DEFAULT_DATE)
                    hItem["Due_Date"] = "";
                hItem["Amount"] = ((double)((decimal)hItem["Amount"])).ToString("c");
            }
            return history;
        }

        public static bool DemandExists(int vendor_id, int reserve_id)
        {   //Does a demand exist for the specified vendor ID and reserve ID?
            return DemandHistory(vendor_id, reserve_id).Count > 0;
        }
        #endregion
    }
}

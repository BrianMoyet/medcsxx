﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ClaimAuditItemType:TypeTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public ClaimAuditItemType() : base() { }
        public ClaimAuditItemType(int id) : base(id) { }
        public ClaimAuditItemType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Claim_Audit_Item_Type";
            }
        }

        public bool NaAllowed
        {
            get { return (bool)row["NA_Allowed"]; }
            set { this.setRowValue("NA_Allowed", value); }
        }

        public int ClaimAuditCategoryTypeId
        {
            get { return (int)row["Claim_Audit_Category_Type_Id"]; }
            set { this.setRowValue("Claim_Audit_Category_Type_Id", value); }
        }
        #endregion
        #region Misc Methods
        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            //if an insert, set sort order to be the current highest value in the table +1
            if (isAdd)
            {
                if (!row.Contains("Sort_Order"))
                    row.Add("Sort_Order", this.maxSortOrder+1);
            }
            return base.Update(parms, fireEvents);
        }

        public Hashtable States
        {
            //returns hashtable of ClaimAuditItemTypeState objects for this item type, indexed by state abbreviation
            get
            {
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Claim_Audit_Item_Type_States", this.Id);
                Hashtable a = new Hashtable();
                ClaimAuditItemTypeState s;

                foreach (DataRow row in rows.Rows)
                {
                    s = new ClaimAuditItemTypeState(rows, row);
                    a.Add(s.State.Abbreviation.ToUpper(), s);
                }
                return a;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MedCsxLogic
{

    public class ImageRightAFupFile:ImageRightFile
    {
        #region Class Variables
        private ImageRightAFupFields m_Fields;
        #endregion

        #region Constructors
        public ImageRightAFupFile() : base() { }
        public ImageRightAFupFile(int claimId)
            : base(claimId)
        {
            m_Fields = new ImageRightAFupFields();
            m_Fields.Drawer = DetermineClaimsImageRightDrawer(this.Claim.CompanyLocation().CompanyId);
            m_Fields.Drawer2 = DeterminePolicyImageRightDrawer(this.Claim.CompanyLocation().CompanyId, this.Claim.PolicyNo);
            m_Fields.FileNumber = this.Claim.DisplayClaimId;
            m_Fields.FileNumber2 = this.Claim.PolicyNo;
        }
        #endregion

        #region Misc Methods
        public string EntireRecord
        {
            get { return m_Fields.EntireRecord; }
        }

        public string DeleteRecord
        {
            get { return m_Fields.DeleteRecord; }
        }

        public void WriteFile()
        {
            if (this.Claim.CompanyLocation().CompanyId == (int)modGeneratedEnums.Company.Companion)
                return;
            string path = HttpContext.Current.Server.MapPath(ModConfig.AFUP_PATH + Claim.DisplayClaimId + ".AFUP");
            StreamWriter sw = new StreamWriter(path);
            sw.WriteLine(m_Fields.DeleteRecord);
            sw.WriteLine(m_Fields.EntireRecord);
            sw.Close();

            Debug.WriteLine("ImageRight AFUP File Written.  Claim: " + Claim.DisplayClaimId + ", Policy: " + Claim.PolicyNo);
        }
        #endregion
    }
}

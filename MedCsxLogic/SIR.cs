﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class SIR:TransactionReferenceTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public SIR() : base() { }
        public SIR(int id) : base(id) { }
        public SIR(Hashtable row) : base(row) { }
        #endregion

        #region Table Properties
        public override string TableName
        {
            get { return "SIR"; }
        }
        #endregion

        #region Column Names
        public Int64 CheckNo
        {
            get { return (Int64)row["Check_No"]; }
            set { this.setRowValue("Check_No", value); }
        }

        public bool isRecovery
        {
            get { return (bool)row["Is_Recovery"]; }
            set { this.setRowValue("Is_Recovery", value); }
        }

        public bool isVoid
        {
            get { return (bool)row["Is_Void"]; }
            set { this.setRowValue("Is_Void", value); }
        }
        public int SubroSalvageVoidReasonId
        {
            get { return (int)row["Subro_Salvage_Void_Reason_Id"]; }
            set { this.setRowValue("Subro_Salvage_Void_Reason_Id", value); }
        }
        #endregion

        #region Methods
        public int TransactionId()
        {
            //Returns transaction Id
            return this.isRecovery ?
                db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_SIR_Recovery_Id", this.Id) :
                db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_SIR_Credit_Id", this.Id);
        }

        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId());
        }
        #endregion
    }
}

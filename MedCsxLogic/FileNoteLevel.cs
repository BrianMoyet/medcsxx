﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class FileNoteLevel:TypeTable
    {
        #region Constructors
        public FileNoteLevel() : base() { }
        public FileNoteLevel(int id) : base(id) { }
        public FileNoteLevel(string description) : base(description) { }
        public FileNoteLevel(Hashtable row) : base(row) { }
        #endregion
        
        #region Column Names
        public int FileNoteLevelId
        {
            get { return (int)row["File_Note_Level_Id"]; }
            set { this.setRowValue("File_Note_Level_Id", value); }
        }
        #endregion

        #region Methods
        public override string TableName
        {
            get
            {
                return "File_Note_Level";
            }
        }

        public bool isClaimLevel()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteLevel.Claim;
        }

        public bool isClaimantLevel()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteLevel.Claimant;
        }

        public bool isReserveLevel()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteLevel.Reserve;
        }

        public bool isTransactionLevel()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteLevel.Transaction;
        }

        public bool isVehicleLevel()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteLevel.Vehicle;
        }
        #endregion
    }
}

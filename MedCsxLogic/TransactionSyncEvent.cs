﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class TransactionSyncEvent:SyncEventTable
    {

        #region Constructors
        public TransactionSyncEvent() : base() { }
        public TransactionSyncEvent(int id) : base(id) { }
        public TransactionSyncEvent(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Transaction_Sync_Event";
            }
        }

        public int TransactionId
        {
            get { return (int)row["Transaction_Id"]; }
            set { this.setRowValue("Transaction_Id", value); }
        }

        public int TransactionSyncEventId
        {
            get { return (int)row["Transaction_Sync_Event_Id"]; }
            set { this.setRowValue("Transaction_Sync_Event_Id", value); }
        }

        public Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }
        #endregion
        #region
        #endregion
        #region
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class AppraisalSupplemental:MedCsXTable
    {

        #region Constructors
        public AppraisalSupplemental() : base() { }
        public AppraisalSupplemental(int id) : base(id) { }
        public AppraisalSupplemental(string description) : base(description) { }
        public AppraisalSupplemental(Hashtable row) : base(row) { }
        public AppraisalSupplemental(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Appraisal_Supplemental"; }
        }

        public int AppraisalSupplementalId
        {
            get { return (int)row["Appraisal_Supplemental_Id"]; }
            set { this.setRowValue("Appraisal_Supplemental_Id", value); }
        }
        
        public int AppraisalResponseId
        {
            get { return (int)row["Appraisal_Response_Id"]; }
            set { this.setRowValue("Appraisal_Response_Id", value); }
        }
        
        public double SupplementalAmount
        {
            get { return (double)((decimal)row["Supplemental_Amount"]); }
            set { this.setRowValue("Supplemental_Amount", value); }
        }

        public string SupplementalDescription
        {
            get { return (string)row["Supplemental_Description"]; }
            set { this.setRowValue("Supplemental_Description", value); }
        }

        public int AppraiserId
        {
            get { return (int)row["Appraiser_Id"]; }
            set { this.setRowValue("Appraiser_Id", value); }
        }
        #endregion
    }
}

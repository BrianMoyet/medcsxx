﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public static class ModConfig
    {

        public static readonly string FUP_PATH = ConfigurationManager.AppSettings["ImageRightFupPath"];
        public static readonly string ClaimsDrawer = ConfigurationManager.AppSettings["ClaimsDrawer"];
        public static readonly string ClaimsDrawer_AustingMutual = ConfigurationManager.AppSettings["AustinMutualDrawer"];
        public static readonly string ClaimsDrawer_Companion = ConfigurationManager.AppSettings["CompanionDrawer"];
        public static readonly string PolicyDrawer = ConfigurationManager.AppSettings["PersonalAutoPolicyDrawer"];
        public static readonly string PolicyDrawer_AustinMutual = ConfigurationManager.AppSettings["AustinMutualPolicyDrawer"];
        public static readonly string PolicyDrawer_MJIBrokerageCompanion = ConfigurationManager.AppSettings["CompanionPolicyDrawerMJIBrokerage"];
        public static readonly string PolicyDrawer_AllOtherCompanion = ConfigurationManager.AppSettings["CompanionPolicyDrawerAllOther"];
        public static readonly string AFUP_PATH = ConfigurationManager.AppSettings["ImageRightAFupPath"];
    }
}

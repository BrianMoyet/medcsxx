﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class AccidentType:TypeTable
    {

        #region Constructors
        public AccidentType() : base() { }
        public AccidentType(int id) : base(id) { }
        public AccidentType(string desc) : base(desc) { }
        public AccidentType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Accident_Type";
            }
        }

        public int AccidentTypeId
        {
            get { return (int)row["Accident_Type_Id"]; }
            set { this.setRowValue("Accident_Type_Id", value); }
        }

        [System.ComponentModel.Description("Type of claim.")]
        public int ClaimTypeId
        {
            get { return (int)row["Claim_Type_Id"]; }
            set { this.setRowValue("Claim_Type_Id", value); }
        }

        public string LossDescriptionText
        {
            get { return (string)row["Loss_Description_Text"]; }
            set { this.setRowValue("Loss_Description_Text", value); }
        }
        #endregion
    }
}

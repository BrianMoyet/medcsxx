﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public abstract class PersonalLinesTable:MedCsXTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Class Variables
        private string m_policyNo;
        private string m_versionNo;
        #endregion

        #region Constructors
        public PersonalLinesTable() : base() { }
        public PersonalLinesTable(string policyNumber)
            : base()
        {
            PolicyNo = policyNumber;
        }
        public PersonalLinesTable(string policyNumber, string versionNumber)
            : base()
        {
            PolicyNo = policyNumber;
            VersionNo = versionNumber;
        }
        public PersonalLinesTable(Hashtable row) : base(row) { }
        #endregion

        #region Methods
        public string VersionNo
        {
            get { return m_versionNo; }
            set { m_versionNo = value; }
        }

        public string PolicyNo
        {
            get { return m_policyNo; }
            set { m_policyNo = value; }
        }

        public State State
        {
            get { return new State((int)row["Cd_State"]); }
        }

        public string CdState
        {
            get { return (string)row["Cd_State"]; }
        }

        public int cdTrx
        {
            get { return (int)row["Cd_Trx"]; }
        }

        public int NoOpenClaims()
        {
            //Returns the number of open claims for a policy (claim status = 6)
            return db.GetIntFromStoredProcedure("usp_No_Open_Claims_For_Policy", this.PolicyNo);
        }

        public DataTable OpenClaimsForListView()
        {
            //Returns open claims for policy for load into listview
            return db.ExecuteStoredProcedure("usp_Get_Open_Claims_For_Policy", this.PolicyNo);
        }
        #endregion
    }
}

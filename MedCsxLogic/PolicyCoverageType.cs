﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PolicyCoverageType:TypeTable
    {

        #region Constructors
        public PolicyCoverageType() : base() { }
        public PolicyCoverageType(int id) : base(id) { }
        public PolicyCoverageType(string description) : base(description) { }
        public PolicyCoverageType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Policy_Coverage_Type";
            }
        }

        public bool hasPerAccidentCoverage
        {
            get { return (bool)row["Has_Per_Accident_Coverage"]; }
            set { this.setRowValue("Has_Per_Accident_Coverage", value); }
        }

        public bool hasPerPersonCoverage
        {
            get { return (bool)row["Has_Per_Person_Coverage"]; }
            set { this.setRowValue("Has_Per_Person_Coverage", value); }
        }

        public string PersonalLinesCoverageCode
        {
            get { return (string)row["Personal_Lines_Coverage_Code"]; }
            set { this.setRowValue("Personal_Lines_Coverage_Code", value); }
        }

        public int PolicyCoverageTypeId
        {
            get { return (int)row["Policy_Coverage_Type_Id"]; }
            set { this.setRowValue("Policy_Coverage_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public bool isAutoBodilyInjury
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.Auto_Bodily_Injury; }
        }

        public bool isCollision
        {
            get { return ((this.Id == (int)modGeneratedEnums.PolicyCoverageType.Collision) ||
                (this.Id == (int)modGeneratedEnums.PolicyCoverageType.Comp_Coll)); }
        }

        public bool isComprehensive
        {
            get
            {
                return ((this.Id == (int)modGeneratedEnums.PolicyCoverageType.Comprehensive) ||
                    (this.Id == (int)modGeneratedEnums.PolicyCoverageType.Comp_Coll));
            }
        }

        public bool isMedicalPayments
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.Medical_Payments; }
        }

        public bool isPipEssentialServices
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.PIP_Essential_Services; }
        }

        public bool isPipFuneral
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.PIP_Funeral; }
        }

        public bool isPipMedical
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.PIP_Medical; }
        }

        public bool isPipRehab
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.PIP_Rehab; }
        }

        public bool isPipWages
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.PIP_Wages; }
        }

        public bool isPropertyDamage
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.Property_Damage; }
        }

        public bool isUnderinsuredMotorist
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.Underinsured_Motorist; }
        }

        public bool isUninsuredMotorist
        {
            get { return this.Id == (int)modGeneratedEnums.PolicyCoverageType.Uninsured_Motorist; }
        }
        
        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_Policy_Coverage_Type_Row";
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using MedCsxDatabase;

namespace MedCsxLogic
{


    public enum Task_Allowed : int
    {
        Edit_Users = 0,
        Close_Claim = 1,
        Edit_Type_Tables = 2,
        Edit_Vendors = 3,
        Edit_User_Groups = 4,
        Configure_Alerts_Diary_File_Notes = 5,
        Edit_Global_Settings = 6,
        Open_Error_Log = 7,
        Edit_Data_Dictionary = 8,
        Vendor_Type_Sub_Type_Associations = 9,
        Logged_In_Users = 10,
        Kill_Users = 11,
        Event_Viewer = 12,
        Add_Reserve = 13,
        Change_Loss_Reserve = 14,
        Change_Expense_Reserve = 15,
        Reopen_Reserve = 16,
        Close_Reserve = 17,
        Close_Pending_Salvage = 18,
        Close_Pending_Subro = 19,
        Issue_Draft = 20,
        Receive_Subro = 21,
        Receive_Salvage = 22,
        Void_Draft = 23,
        Void_Subro = 24,
        Void_Salvage = 25,
        Modify_Claim_Data = 26,
        Void_Reserve = 27,
        Reissue_Draft = 28,
        Reissue_Subro = 29,
        Reissue_Salvage = 30,
        Reopen_Claim = 31,
        Change_Vendor_Tax_Id_Required = 32,
        Send_Message = 33,
        Staffing_Metrics = 34,
        Recovery_Amounts_Report = 35,
        Edit_Transaction_Type_Conversion = 36,
        Print_Foxpro_Drafts = 37,
        Accounting_Reports = 38,
        Manually_Lock_Claim = 39,
        Set_Average_Reserves = 40,
        Set_Type_Table_Icons = 41,
        SQL_Logging = 42,
        Bulk_Assign_Claims = 43,
        Unvoid_Draft = 44,
        Audit_Claims = 45,
        Log_In_As = 46,
        Resend_Appraisal_Requests = 47,
        Mark_Appraisals_as_Completed = 48,
        Cancel_Appraisal = 49,
        Change_Document_Status = 50,
        Create_New_Document = 51,
        Open_Document = 52,
        Print_Draft_Queue = 53,
        View_Appraiser_Data = 54,
        Wire_Transfer = 55,
        Pending_Drafts = 56,
        Resend_Appraisal_Emails = 57
    };

    public class User:MedCsXTable
    {
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public User(int id) : base(id) { }
        public User(Hashtable row) : base(row) { }
        public User(DataTable Table, DataRow Row) : base(Table, Row) { }
        #endregion

        public override string TableName
        {
            get { return "User"; }
        }

        #region Column Names
        public int AlertPreferenceId
        {
            get { return (int)row["Alert_Preference_Id"]; }
            set { this.setRowValue("Alert_Preference_Id", value); }
        }

        public string AppointmentImportance
        {
            get { return (string)row["Appointment_Importance"]; }
            set { this.setRowValue("Appointment_Importance", value); }
        }

        public int AttorneyVendorId
        {
            get { return (int)row["Attorney_Vendor_Id"]; }
            set { this.setRowValue("Attorney_Vendor_Id", value); }
        }

        public bool AutoClearOlderDiaries
        {
            get { return (bool)row["Auto_Clear_Older_Diaries"]; }
            set { this.setRowValue("Auto_Clear_Older_Diaries", value); }
        }

        public bool AutoOpenImageRightClaims
        {
            get { return (bool)row["Auto_Open_ImageRight_Claims"]; }
            set { this.setRowValue("Auto_Open_ImageRight_Claims", value); }
        }

        public string BusyStatus
        {
            get { return (string)row["Busy_Status"]; }
            set { this.setRowValue("Busy_Status", value); }
        }

        public bool CanBeAssignedClaims
        {
            get { return (bool)row["Can_Be_Assigned_Claims"]; }
            set { this.setRowValue("Can_Be_Assigned_Claims", value); }
        }

        public DateTime DateClaimExperienceStarted
        {
            get { return (DateTime)row["Date_Claim_Experience_Started"]; }
            set { this.setRowValue("Date_Claim_Experience_Started", value); }
        }

        public DateTime DatePasswordSet
        {
            get { return (DateTime)row["Date_Password_Set"]; }
            set { this.setRowValue("Date_Password_Set", value); }
        }

        public int DiaryGracePeriod
        {
            get { return (int)row["Diary_Grace_Period"]; }
            set { this.setRowValue("Diary_Grace_Period", value); }
        }

        public int DiaryThemeId
        {
            get { return (int)row["Diary_Theme_Id"]; }
            set { this.setRowValue("Diary_Theme_Id", value); }
        }

        public string DirectFax
        {
            get { return (string)row["Direct_Fax"]; }
            set { this.setRowValue("Direct_Fax", value); }
        }

        public string Email
        {
            get { return (string)row["Email"]; }
            set { this.setRowValue("Email", value); }
        }

        public string FirstName
        {
            get { return (string)row["First_Name"]; }
            set { this.setRowValue("First_Name", value); }
        }

        public bool ForceDiary
        {
            get { return (bool)row["Force_Diary"]; }
            set { this.setRowValue("Force_Diary", value); }
        }

        public bool KillSession
        {
            get { return (bool)row["Kill_Session"]; }
            set { this.setRowValue("Kill_Session", value); }
        }

        public string LastName
        {
            get { return (string)row["Last_Name"]; }
            set { this.setRowValue("Last_Name", value); }
        }

        public DateTime LastPolledActiveDate
        {
            get { return (DateTime)row["Last_Polled_Active_Date"]; }
            set { this.setRowValue("Last_Polled_Active_Date", value); }
        }

        public DateTime LoginDate
        {
            get { return (DateTime)row["Login_Date"]; }
            set { this.setRowValue("Login_Date", value); }
        }

        public string LoginId
        {
            get { return (string)row["Logon_Id"]; }
            set { this.setRowValue("Logon_Id", value); }
        }

        public DateTime LogoutDate
        {
            get { return (DateTime)row["Logout_Date"]; }
            set { this.setRowValue("Logout_Date", value); }
        }

        public string MACAddress
        {
            get { return (string)row["MAC_Address"]; }
            set { this.setRowValue("MAC_Address", value); }
        }

        public string MedCsXVersion
        {
            get { return (string)row["MedCsX_Version"]; }
            set { this.setRowValue("MedCsX_Version", value); }
        }

        public int MemoryUsage
        {
            get { return (int)row["Memory_Usage"]; }
            set { this.setRowValue("Memory_Usage", value); }
        }

        public DateTime MyAlertsChanged
        {
            get { return (DateTime)row["My_Alerts_Changed"]; }
            set { this.setRowValue("My_Alerts_Changed", value); }
        }

        public DateTime MyDiaryChanged
        {
            get { return (DateTime)row["My_Diary_Changed"]; }
            set { this.setRowValue("My_Diary_Changed", value); }
        }

        public DateTime MyReservesChanged
        {
            get { return (DateTime)row["My_Reserves_Changed"]; }
            set { this.setRowValue("My_Reserves_Changed", value); }
        }

        public DateTime MyTotalLossesChanged
        {
            get { return (DateTime)row["My_TotalLosses_Changed"]; }
            set { this.setRowValue("My_TotalLosses_Changed", value); }
        }

        public DateTime MyReconNonPDChanged
        {
            get { return (DateTime)row["My_ReconNonPD_Changed"]; }
            set { this.setRowValue("My_ReconNonPD_Changed", value); }
        }

        public DateTime MyReconPDChanged
        {
            get { return (DateTime)row["My_ReconPD_Changed"]; }
            set { this.setRowValue("My_ReconPD_Changed", value); }
        }

        public DateTime MyTasksChanged
        {
            get { return (DateTime)row["My_Tasks_Changed"]; }
            set { this.setRowValue("My_Tasks_Changed", value); }
        }

        public DateTime MySalvageChanged
        {
            get
            {
                if ((DateTime?)row["My_Salvage_Changed"] == null)
                    return ml.DEFAULT_DATE;
                return (DateTime)row["My_Salvage_Changed"]; 
            }
            set { this.setRowValue("My_Salvage_Changed", value); } 
        }

        public int NbrDaysInAdvance
        {
            get { return (int)row["Nbr_Days_In_Advance"]; }
            set { this.setRowValue("Nbr_Days_In_Advance", value); }
        }

        public int OpenClaims
        {
            get { return (int)row["Open_Claims"]; }
            set { this.setRowValue("Open_Claims", value); }
        }

        public int OutgoingAlertPreferenceId
        {
            get { return (int)row["Outgoing_Alert_Preference_Id"]; }
            set { this.setRowValue("Outgoing_Alert_Preference_Id", value); }
        }

        public int UserDefaultDraftPrinterId
        {
            get { return (int)row["User_Default_Draft_Printer_Id"]; }
            set { this.setRowValue("User_Default_Draft_Printer_Id", value); }
        }

        public bool OutlookIntegration
        {
            get { return (bool)row["Outlook_Integration"]; }
            set { this.setRowValue("Outlook_Integration", value); }
        }

        public string Password
        {
            get { return (string)row["Password"]; }
            set { this.setRowValue("Password", value); }
        }

       
        public string PhoneExt
        {
            get { return (string)row["Phone_Ext"]; }
            set { this.setRowValue("Phone_Ext", value); }
        }

        public DateTime PreviousLoginDate
        {
            get { return (DateTime)row["Previous_Login_Date"]; }
            set { this.setRowValue("Previous_Login_Date", value); }
        }

        public string SceneAccessAdjCode
        {
            get { return (string)row["Scene_Access_Adj_Code"]; }
            set { this.setRowValue("Scene_Access_Adj_Code", value); }
        }

        public bool SearchAsIType
        {
            get { return (bool)row["Search_As_I_Type"]; }
            set { this.setRowValue("Search_As_I_Type", value); }
        }

        public bool SetReminderAppointments
        {
            get { return (bool)row["Set_Reminder_Appointments"]; }
            set { this.setRowValue("Set_Reminder_Appointments", value); }
        }

        public string SignatureFileName
        {
            get { return (string)row["Signature_File_Name"]; }
            set { this.setRowValue("Signature_File_Name", value); }
        }

        public int SupervisorId
        {
            get { return (int)row["Supervisor_Id"]; }
            set { this.setRowValue("Supervisor_Id", value); }
        }

        public int TestAssignmentAdjusterId
        {
            get { return (int)row["Test_Assignment_Adjuster_Id"]; }
            set { this.setRowValue("Test_Assignment_Adjuster_Id", value); }
        }

        public bool UseDiaryGracePeriodFromUserType
        {
            get { return (bool)row["Use_Diary_Grace_Period_From_User_Type"]; }
            set { this.setRowValue("Use_Diary_Grace_Period_From_User_Type", value); }
        }

        public bool UseForceDiaryFromUserType
        {
            get { return (bool)row["Use_Force_Diary_From_User_Type"]; }
            set { this.setRowValue("Use_Force_Diary_From_User_Type", value); }
        }

        public Int64 UserAllowance
        {
            get { return row["TaskAbility"] == null ? 0 : (Int64)row["TaskAbility"]; }
            set { this.setRowValue("TaskAbility", value); }
        }

        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }

        public int UserStatusId
        {
            get { return (int)row["User_Status_Id"]; }
            set { this.setRowValue("User_Status_Id", value); }
        }

        public int UserSubTypeId
        {
            get { return (int)row["User_Sub_Type_Id"]; }
            set { this.setRowValue("User_Sub_Type_Id", value); }
        }

        public int UserTypeId
        {
            get { return (int)row["User_Type_Id"]; }
            set { this.setRowValue("User_Type_Id", value); }
        }

        public bool WordWrapAlerts
        {
            get { return (bool)row["Word_Wrap_Alerts"]; }
            set { this.setRowValue("Word_Wrap_Alerts", value); }
        }

        #endregion

        
        #region Methods
        public static void LogonUser()
        {
            //Logs user onto system
            db.ExecuteStoredProcedureNoReturnValue("usp_Logon_MedCsX_User", Convert.ToInt32(HttpContext.Current.Session["userID"]), "", Application.ProductVersion);
        }

        public static void LogoffUser()
        {
            //Logs user off of system
            db.ExecuteStoredProcedureNoReturnValue("usp_Logoff_User", Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public static DataTable CompanyAuthority(int UserID)
        {
            //Returns the user's authority for all companies

            DataTable Companies = Company.getCompanies(); //Get companies
            //DataTable Authorities = db.ExecuteStoredProcedure("usp_Get_User_Company_Authority", UserID); //Get user authority

            //foreach (DataRow Comp in Companies)
            //{
            //    Comp.Add("User_Company_Authority_Id", 0);
            //    Comp.Add("Open_Allowed", false);

            //    foreach (DataRow Auth in Authorities.Rows)
            //    {
            //        string tmp1 = Auth["Company_Id"].ToString();
            //        string tmp2 = Comp["Company_Id"].ToString();
            //        if (tmp1 == tmp2)
            //        {
            //            //Found company
            //            Comp["Open_Allowed"] = Auth["Open_Allowed"];  //assign authority
            //            Comp["User_Company_Authority_Id"] = Auth["User_Company_Authority_Id"]; //assign id
            //        }
            //    }
            //}
            return Companies;
        }

        public static ArrayList UserIdsForNames(ref ArrayList userNames)
        {
            //Returns a collection of UserIds give a collection of UserNames

            string[] name;
            ArrayList userIds = new ArrayList();
            int id;

            foreach (string v in userNames)
            {
                name = v.Split(' ');
                id = User.UserIdForName(ref name[0], ref name[1]);
                if (id != 0)
                    userIds.Add(id);
            }
            return userIds;
        }

        public static int UserIdForName(ref string firstName, ref string lastName)
        {
            //Returns the User_Id for the user's first and last names
            return db.GetIntFromStoredProcedure("usp_Get_User_Id_For_Name", firstName, lastName);
        }

        public static int DefaultPrinterForUser(int usrid)
        {
            //Returns the User_Id for the user's first and last names
            return db.GetIntFromStoredProcedure("usp_Get_User_Default_Printer", usrid);
        }

        public static int UserIdForName(string name)
        {
            string[] a = name.Split(' ');

            if (a.Length - 1 != 1)
                return 0;

            return db.GetIntFromStoredProcedure("usp_Get_User_Id_For_Name", a[0], a[1]);
        }

        public static string UserNameForId(int UserID)
        {
            return db.GetStringFromStoredProcedure("usp_Get_User_Name", UserID);
        }

        public static User Named(string name)
        {
            return new User(UserIdForName(name));
        }

        public string EmailAddress
        {
            get { return this.Email; }
        }

        public string Name
        {
            //Returns first and last name
            get { return (this.FirstName + " " + this.LastName).Trim(); }
        }

        public void Kill()
        {
            //Kills user.  Session will abort at the next polling time (approx. 1 min)
            this.KillSession = true;
            this.Update();
        }

        public DataTable GetMessages
        {
            //Retrieves messages for user as collection of User_Message rows
            get { return db.ExecuteStoredProcedure("usp_Get_Messages_For_User", this.Id); }
        }

        public void DeleteMessages()
        {
            //Deletes User_Message rows for user
            db.ExecuteStoredProcedure("usp_Delete_Messages_For_User", this.Id);
        }

        public UserReserveType UserReserveType(int reserveTypeId)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_User_Reserve_Type", this.Id, reserveTypeId);
            DataRow row = rows.Rows[0];

            return new UserReserveType(rows, row);
        }

        public bool isAboveUser(User v)
        {
            //Is this user above user?

            //if user1 is admin - automatically above user2
            if (this.UserTypeId == (int)modGeneratedEnums.UserType.Administrator)
                return true;

            int s;
            int u2 = v.Id;

            while (u2 != 0)
            {
                User user = new User(u2);
                s = user.SupervisorId;

                if (s == this.Id)
                    return true;

                if (s == u2)  //prevents infinite loop
                    return false;

                u2 = s;
            }

            return false;
        }

        public bool isManagement()
        {
            if ((this.UserTypeId == (int)modGeneratedEnums.UserType.Manager) || (this.UserTypeId == (int)modGeneratedEnums.UserType.Supervisor))
                return true;
            return false;
        }

        public override ArrayList Reserves()
        {
            //Returns pending reserves for user
            ArrayList a = new ArrayList();
            DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_User", this.Id);
            foreach (DataRow row in myReserves.Rows)
            {
                a.Add(new Reserve(myReserves, row));
            }

            return a;
        }

        public override ArrayList Alerts()
        {
            //Returns active alerts for user
            ArrayList a = new ArrayList();
            DataTable myAlerts = db.ExecuteStoredProcedure("usp_Get_Alerts_For_User", this.Id);
            foreach (DataRow row in myAlerts.Rows)
            {
                a.Add(new Alert(myAlerts, row));
            }

            return a;
        }

        public override ArrayList Diaries()
        {
            //Returns active Diaries for user
            ArrayList a = new ArrayList();
            DataTable myDiaries = db.ExecuteStoredProcedure("usp_Get_Diaries_For_User", this.Id);
            foreach (DataRow row in myDiaries.Rows)
            {
                a.Add(new Diary(myDiaries, row));
            }

            return a;
        }

        public override ArrayList UserTasks()
        {
            //Returns non-cleared UserTasks for user
            ArrayList a = new ArrayList();
            DataTable myUserTasks = db.ExecuteStoredProcedure("usp_Get_User_Tasks_For_User", this.Id);
            foreach (DataRow row in myUserTasks.Rows)
            {
                a.Add(new UserTask(myUserTasks, row));
            }

            return a;
        }

        public UserType UserType()
        {
            return new UserType(this.UserTypeId);
        }

        public bool shouldForceDiary
        {
            get { return this.UseForceDiaryFromUserType ? this.UserType().ForceDiary : this.ForceDiary; }
        }

        public bool isAssignedToClaim(Claim claim)
        {
            //Is the current user assigned to a reserve line in the claim?
            return db.GetBoolFromStoredProcedure("usp_Is_User_Assigned_To_Claim", this.Id, claim.Id);
        }

        public void ClearAlertsBefore(DateTime beforeDate)
        {
            db.ExecuteStoredProcedure("usp_Clear_Alerts_For_User", this.Id, beforeDate);
        }

        public void ClearDiariesBefore(DateTime beforeDate)
        {
            db.ExecuteStoredProcedure("usp_Clear_Diary_For_User", this.Id, beforeDate);
        }

        public int NbrAlertsBefore(DateTime beforeDate)
        {
            return db.GetIntFromStoredProcedure("usp_Nbr_Alerts_For_User", this.Id, beforeDate);
        }

        public int NbrDiariesBefore(DateTime beforeDate)
        {
            return db.GetIntFromStoredProcedure("usp_Nbr_Diary_For_User", this.Id, beforeDate);
        }

        public int getDiaryGracePeriod()
        {
            /*Calculate diary grace period
             * If Use_Diary_Grace_Period_From_User_Type = true:
             *    returns Diary_Grace_Period from User_Type table
             *    else return Diary_Grace_Period from User Table
            */

            if (this.UseDiaryGracePeriodFromUserType)
                return this.UserType().DiaryGracePeriod;
            return this.DiaryGracePeriod;
        }

        public void SetDiarTheme(string themeName)
        {
            switch (themeName)
            {
                case "Christmas":
                    this.DiaryThemeId = 1;
                    this.Update();
                    break;
                case "Summer":
                    this.DiaryThemeId = 2;
                    this.Update();
                    break;
                default: //case "Normal"
                    this.DiaryThemeId = 0;
                    this.Update();
                    break;
            }
        }

        public string GetDiaryTheme()
        {
            switch (this.DiaryThemeId)
            {
                case 1:
                    return "Christmas";
                case 2:
                    return "Summer";
                default:
                    return "Normal";
            }
        }

        public bool isAlertPreferenceMedCsX()
        {
            return ((this.AlertPreferenceId == (int)modGeneratedEnums.AlertPreference.MedCsX_Alert) ||
                (this.AlertPreferenceId == (int)modGeneratedEnums.AlertPreference.Electric_Shock_Collar)) ?
                true : false;
        }

        public bool isAlertPreferenceEmail()
        {
            return (this.AlertPreferenceId == (int)modGeneratedEnums.AlertPreference.Email) ?
                true : false;
        }

        public bool isAlertPreferenceNone()
        {
            return (this.AlertPreferenceId == (int)modGeneratedEnums.AlertPreference.None) ?
                true : false;
        }

        public bool isAlertPreferenceNonIntrusive()
        {
            return this.AlertPreference().isNonIntrusive;
        }

        public Vendor AttorneyVendor()
        {
            return (this.AttorneyVendorId == 0) ? null : new Vendor(this.AttorneyVendorId);
        }

        public bool isAttorney()
        {
            return ((this.UserTypeId == (int)modGeneratedEnums.UserType.Attorney) ||
                (this.UserTypeId == (int)modGeneratedEnums.UserType.Attorney_Supervisor)) ?
                true : false;
        }

        public ArrayList AttorneysAuthorizedToView()
        {
            return this.isAttorney() ? this.UserAndAllSubordinates() : MedCsXSystem.AllAttorneys();
        }

        public ArrayList UserAndAllSubordinates()
        {
            ArrayList a = new ArrayList();
            a.Add(this);

            foreach (User v in MedCsXSystem.Users())
            {
                if ((v.SupervisorId == this.Id) && (v.Id != this.Id))
                    a.AddRange(v.UserAndAllSubordinates());
            }
            return a;
        }

        public AlertPreference AlertPreference()
        {
            return new AlertPreference(this.AlertPreferenceId);
        }

        public AlertPreference OutgoingAlertPreference()
        {
            return new AlertPreference(this.OutgoingAlertPreferenceId);
        }

        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_User_Row";
        }
        #endregion
    }

    public static class FlagsHelper
    {
        public static bool IsSet<T>(T flags, T flag) where T : struct
        {
            long flagsValue = (long)(object)flags;
            long flagValue = (long)System.Math.Pow(2, (long)(object)flag);

            return (flagsValue & flagValue) != 0;
        }

        public static void Set<T>(ref T flags, T flag) where T : struct
        {
            long flagsValue = (long)(object)flags;
            long flagValue = (long)System.Math.Pow(2, (long)(object)flag);

            flags = (T)(object)(flagsValue | flagValue);
        }

        public static void UnSet<T>(ref T flags, T flag) where T : struct
        {
            long flagsValue = (long)(object)flags;
            long flagValue = (long)System.Math.Pow(2, (long)(object)flag);

            flags = (T)(object)(flagsValue & (~flagValue));
        }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class InjuredCoverage:ReserveTable
    {

        #region Constructors
        public InjuredCoverage() : base() { }
        public InjuredCoverage(int id) : base(id) { }
        public InjuredCoverage(Hashtable row) : base(row) { }
        public InjuredCoverage(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Injured_Coverage"; }
        }

        public bool CoverageSpecified
        {
            get { return (bool)row["Coverage_Specified"]; }
            set { this.setRowValue("Coverage_Specified", value); }
        }

        public int InjuredCoverageId
        {
            get { return (int)row["Injured_Coverage_Id"]; }
            set { this.setRowValue("Injured_Coverage_Id", value); }
        }

        public int InjuredId
        {
            get { return (int)row["Injured_Id"]; }
            set { this.setRowValue("Injured_Id", value); }
        }
        
        public int ReserveTypeId
        {
            get { return (int)row["Reserve_Type_Id"]; }
            set { this.setRowValue("Reserve_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            if (this.CoverageSpecified && (this.ReserveId == 0))
                this.ReserveId = this.CreateReserve();
            return base.Update(parms, fireEvents);
        }

        public int CreateReserve()
        {
            //create reserve and return reserve ID

            Hashtable parms = new Hashtable();
            parms.Add("Injured_Id", this.InjuredId);

            switch (this.ReserveTypeId)
            {
                case (int)modGeneratedEnums.ReserveType.ABI_Major:
                    return this.Claimant.CreateABIMajorReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.ABI_Minor:
                    return this.Claimant.CreateABIMinorReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.Medical_Payments:
                    return this.Claimant.CreateMedicalPaymentsReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.PIP_Essential_Services:
                    return this.Claimant.CreatePIPEssentialServicesReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.PIP_Funeral:
                    return this.Claimant.CreatePIPFuneralReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.PIP_Medical:
                    return this.Claimant.CreatePIPMedicalReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.PIP_Rehab:
                    return this.Claimant.CreatePIPRehabReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.PIP_Wages:
                    return this.Claimant.CreatePIPWagesReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.Underinsured_Motorist:
                    return this.Claimant.CreateUnderinsuredMotoristReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.Uninsured_Motorist:
                    return this.Claimant.CreateUninsuredMotoristReserve(parms).Id;
                case (int)modGeneratedEnums.ReserveType.Commercial_General_Liability_BI:
                    return this.Claimant.CreateCGLInjuryReserve(parms).Id;
                default:
                    return 0;
            }

        }

        public Injured Injured()
        {
            return new Injured(this.InjuredId);
        }

        public ReserveType ReserveType()
        {
            return new ReserveType(this.ReserveTypeId);
        }

        public override int ClaimantId
        {
            get
            {
                return this.Injured().ClaimantId;
            }
            set
            {
                //not appropriate to assign claimant ID here
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}

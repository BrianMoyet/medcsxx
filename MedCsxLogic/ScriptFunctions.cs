﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ScriptFunctions
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Class Variables
        public Hashtable personalDescriptions = new Hashtable();
        private Hashtable userNames = new Hashtable();
        #endregion

        #region
        #endregion

        public string getVendorDescription(ref int vendorId)
        {
            //Returns vendor description given vendor ID

            string desc;
            Hashtable vendor = new Hashtable();

            vendor = getRow("Vendor", vendorId);
            desc = vendor["Name"] + Environment.NewLine;
            if ((string)vendor["Address1"] != "")
                desc += vendor["Address1"] + Environment.NewLine;

            if ((string)vendor["Address2"] != "")
                desc += vendor["Address2"] + Environment.NewLine;

            if (((string)vendor["City"] != "") || ((int)vendor["State_Id"] != 0) || ((string)vendor["Zipcode"] != ""))
                desc += vendor["City"] + ", " + vendor["State_Id"] + " " + vendor["Zipcode"] + Environment.NewLine;

            if ((string)vendor["Phone"] != "")
                desc += "Phone: " + vendor["Phone"] + Environment.NewLine;

            if ((string)vendor["Fax"] != "")
                desc += "Fax: " + vendor["Fax"] + Environment.NewLine;

            if ((string)vendor["Email"] != "")
                desc += "Email: " + vendor["Email"] + Environment.NewLine;

            if ((string)vendor["Tax_Id"] != "")
                desc += "Tax Id: " + vendor["Tax_Id"] + Environment.NewLine;

            return desc;
        }

        //public string getPoliceReportDescription(int policeReportRequestId)
        //{
        //    ChoicePointRequest p = new ChoicePointRequest(policeReportRequestId);
        //    return p.Description;
        //}

        public string getDisplayClaimId(int claimId)
        {
            //Gets Display_Claim_Id for given Claim_Id.

            Claim c = new Claim(claimId);
            return c.DisplayClaimId;
        }

        public string ReserveDescription(ref int reserveId)
        {
            /* Returns a reserve description.
             * This is used by the File Note scripts.
            */

            return new Reserve(reserveId).Description();
        }

        public string ReserveAssignedToDescription(ref int reserveId)
        {
            /* Returns a description of to whom the reserve is assigned
             * This is used by the File Note scripts.
            */

            return new Reserve(reserveId).AssignedToDescription();
        }

        public string getPersonName(int personID)
        {
            /* Returns person name for Id.
             * This is used by the File Note scripts.
            */

            return Person.PersonName(personID);
        }

        //public string getPhoneCallShortDescription(int phoneCallId)
        //{
        //    //returns phone call short description for phone call id
        //    PhoneCall p = new PhoneCall(phoneCallId);
        //    return p.ShortDescription;
        //}

        public string getColumnDisplayName(string tableName, string columnName)
        {
            //gets column description from data dictionary
            return db.GetStringFromStoredProcedure("usp_Get_Column_Display_Name", tableName, columnName);
        }

        public string AppraisalRequestedFileNoteText(int appraisalTaskId)
        {
            AppraisalTask a = new AppraisalTask(appraisalTaskId);
            return a.AppraisalRequestedFileNoteText;
        }

        public string getVendorName(int vendorId)
        {
            //returns vendor name for given vendor ID
            Hashtable vendor = getRow("Vendor", vendorId);
            return (string)vendor["Name"];
        }

        public int getTransactionIdForDeductiblesId(int deductiblesId)
        {
            //returns transaction ID for given deductibles ID - if multiple transactions per deductibles, return first
            return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Deductibles_Id", deductiblesId);
        }

        public string getPersonDescription(int personId, bool isLondon = false)
        {
            //returns person description for given person ID
            if (personalDescriptions.Contains(personId))
                return (string)personalDescriptions[personId];

            List<Hashtable> c = db.ExecuteStoredProcedureReturnHashList("usp_Get_Person_Description_For_Id", personId, 0);
            Hashtable d;
            string desc = "";

            if (c.Count > 0)
            {
                d = (Hashtable)ml.firstItem(c);
                desc = (string)d["Description"];
            }
            personalDescriptions.Add(personId, desc);

            return desc;
        }

        public int getTransactionIdForSalvageId(int salvageId)
        {
            //returns transaction ID for salvage ID - if multiple transactions per salvage, return first
            return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Salvage_Id", salvageId);
        }

        public int getTransactionIdForSubroId(int subroId)
        {
            //returns transaction ID for subro ID - if multiple transactions per subro, return first
            return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Subro_Id", subroId);
        }

        public int getTransactionIdForSIRId(int SIRId)
        {
            //returns transaction ID for SIR ID - if multiple transactions per SIR, return first
            SIR mySIR = new SIR(SIRId);
            if(mySIR.isRecovery)
                return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_SIR_Recovery_Id", SIRId);
            return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_SIR_Credit_Id", SIRId);
        }

        public string getVehicleName(int vehicleId)
        {
            //returns vehicle name for given vehicle ID
            Vehicle car = new Vehicle(vehicleId);
            return car.Name;
        }

        public string getState(int stateId)
        {
            //returns state abbreviation for given state ID
            return db.GetStringFromStoredProcedure("usp_Get_State", stateId);
        }

        public string getDraftDescriptionFromDraftId(int draftId)
        {
            //returns draft description from given draft ID
            Draft d = new Draft(draftId);
            return d.Description;
        }

        public double getUserAuthority(int reserveTypeId)
        {
            //returns user authority for given reserve type
            ReserveType rt = new ReserveType(reserveTypeId);
            return rt.UserAuthority();
        }

        public double getReserveLossAmount(int reserveId)
        {
            //returns total loss amount for the given reserve ID
            return new Reserve(reserveId).LossAmount;
        }

        public string getUserName(int userId) 
        {
            //returns user name for give user ID

            if(!userNames.Contains(userId)) //cache user name
            {
                List<Hashtable> c = db.ExecuteStoredProcedureReturnHashList("usp_Get_User_Name", userId);
                Hashtable d = (Hashtable)ml.firstItem(c);
                userNames.Add(userId, d["Name"]);
            }
            return (string)userNames[userId];
        }

        public string getCurrentUserName()
        {
            return getUserName(Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public string ExpenseReserveDescription(int reserveId)
        {
            //returns expense reserve description for given reserve ID
            Hashtable res = getRow("Reserve", reserveId);
            int reserveTypeId = (int)res["Reserve_Type_Id"];
            int claimantId = (int)res["Claimant_Id"];
            Hashtable claimant = getRow("Claimant", claimantId);
            int personId = (int)claimant["Person_Id"];
            string msg = getDescription("Reserve_Type", reserveTypeId) + " Expense Reserve For " + Person.PersonName(personId);

            return msg;
        }

        public string LossReserveDescription(int reserveId)
        {
            //returns loss reserve description for given reserve ID
            Hashtable res = getRow("Reserve", reserveId);
            int reserveTypeId = (int)res["Reserve_Type_Id"];
            int claimantId = (int)res["Claimant_Id"];
            Hashtable claimant = getRow("Claimant", claimantId);
            int personId = (int)claimant["Person_Id"];
            string msg = getDescription("Reserve_Type", reserveTypeId) + " Loss Reserve For " + Person.PersonName(personId);

            return msg;
        }

        //public string ChoicePointResponseFileNoteText(int choicePointResponseId)
        //{
        //    ChoicePointReponse c = new ChoicePointReponse(choicePointResponseId);
        //    return c.FileNoteText;
        //}

        public string InitialReserveAssignmentFileNoteText(int claimId, int userGroupId, int userId)
        {
            //generate file note text for initial reserve assignment event
            Claim claim = new Claim(claimId);
            string fnText = "The Following Reserves Have Been Created and Assigned to ";

            if (userGroupId != 0)
            {
                UserGroup ug = new UserGroup(userGroupId);
                fnText += ug.Description + ":" + Environment.NewLine + Environment.NewLine;
                foreach (Reserve res in claim.ReservesAssignedToUserGroupId(userGroupId))
                    fnText += res.Description() + Environment.NewLine;
                return fnText;
            }

            if (userId != 0)
            {
                User u = new User(userId);
                fnText += u.Name + ":" + Environment.NewLine + Environment.NewLine;
                foreach (Reserve res in claim.ReservesAssignedToUserId(userId))
                    fnText += res.Description() + Environment.NewLine;
                return fnText;
            }

            //userGroupId=0 and userId=0
            return "";
        }

        public string getClaimantNameForReserveId(int reserveId)
        {
            try
            {
                Reserve res = new Reserve(reserveId);
                return res.ClaimantName();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getPropertyDamageDescription(int propertyDamageId)
        {
            try
            {
                return new PropertyDamage(propertyDamageId).Description;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getInjuredDescriptionForReserve(int injuredId)
        {
            try
            {
                return new Injured(injuredId).Description;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getSubroCheckNoString(int subroId)
        {
            try
            {
                return new Subro(subroId).CheckNo.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getDeductiblesCheckNoString(int deductiblesId)
        {
            try
            {
                return new Deductibles(deductiblesId).CheckNo.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string getSIRCheckNoString(int SIRId)
        {
            try
            {
                return new SIR(SIRId).CheckNo.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public Hashtable getRow(string tableName, int id)
        {
            return ml.getRow(tableName, id);
        }

        public string getDescription(string tableName, int id)
        {
            return ml.getDescriptions(tableName, id);
        }
    }
}

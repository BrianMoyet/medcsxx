﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class SceneAccessAppraiserState:MedCsXTable
    {

        #region Constructors
        public SceneAccessAppraiserState() : base() { }
        public SceneAccessAppraiserState(int id) : base(id) { }
        public SceneAccessAppraiserState(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Scene_Access_Appraiser_State"; }
        }

        public int SceneAccessAppraiserId
        {
            get { return (int)row["Scene_Access_Appraiser_Id"]; }
            set { this.setRowValue("Scene_Access_Appraiser_Id", value); }
        }

        public int StateId
        {
            get { return (int)row["State_Id"]; }
            set { this.setRowValue("State_Id", value); }
        }
        #endregion
    }
}

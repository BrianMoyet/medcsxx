﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class WitnessPassenger:ClaimTable
    {
        #region Constructors
        public WitnessPassenger() : base() { }
        public WitnessPassenger(int id) : base(id) { }
        public WitnessPassenger(Hashtable row) : base(row) { }
        public WitnessPassenger(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Witness_Passenger"; }
        }

        public int PersonId
        {
            get { return (int)row["Person_Id"]; }
            set { this.setRowValue("Person_Id", value); }
        }

        public int WitnessPassengerId
        {
            get { return (int)row["Witness_Passenger_Id"]; }
            set { this.setRowValue("Witness_Passenger_Id", value); }
        }

        public int WitnessPassengerTypeId
        {
            get { return (int)row["Witness_Passenger_Type_Id"]; }
            set { this.setRowValue("Witness_Passenger_Type_Id", value); }
        }

        public int WitnessPassengerLocation
        {
            get { return (int)row["Witness_Passenger_Location"]; }
            set { this.setRowValue("Witness_Passenger_Location", value); }
        }
        #endregion

        #region Misc Methods
        public Person Person()
        {
            return new Person(this.PersonId);
        }

        public WitnessPassengerType WitnessPassengerType()
        {
            return new WitnessPassengerType(this.WitnessPassengerTypeId);
        }
        #endregion
    }
}

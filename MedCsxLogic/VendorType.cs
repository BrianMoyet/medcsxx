﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class VendorType:TypeTable
    {
        #region Constructors
        public VendorType() : base() { }
        public VendorType(int id) : base(id) { }
        public VendorType(string description) : base(description) { }
        public VendorType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Vendor_Type";
            }
        }

        public int VendorTypeId
        {
            get { return (int)row["Vendor_Type_Id"]; }
            set { this.setRowValue("Vendor_Type_Id", value); }
        }
        #endregion
    }
}

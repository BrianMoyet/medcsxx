﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MedCsxLogic
{
    public class UserTask:ClaimTable
    {

        private static modLogic ml=new modLogic();
        #region Constructors
        public UserTask() : base() { }
        public UserTask(int id) : base(id) { }
        public UserTask(Hashtable row) : base(row) { }
        public UserTask(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "User_Task"; }
        }

        public int ClaimantId
        {
            get { return (int)row["Claimant_Id"]; }
            set { this.setRowValue("Claimant_Id", value); }
        }

        public int ClearedByUserId
        {
            get { return (int)row["Cleared_By_User_Id"]; }
            set { this.setRowValue("Cleared_By_User_Id", value); }
        }

        public string ClearedComments
        {
            get { return (string)row["Cleared_Comments"]; }
            set { this.setRowValue("Cleared_Comments", value); }
        }

        public string Description
        {
            get { return (string)row["Description"]; }
            set { this.setRowValue("Description", value); }
        }

        public int LockedByUserId
        {
            get { return (int)row["Locked_By_User_Id"]; }
            set { this.setRowValue("Locked_By_User_Id", value); }
        }

        public int ReserveId
        {
            get { return (int)row["Reserve_Id"]; }
            set { this.setRowValue("Reserve_Id", value); }
        }

        public int UserTaskId
        {
            get { return (int)row["User_Task_Id"]; }
            set { this.setRowValue("User_Task_Id", value); }
        }

        public int UserTaskStatusId
        {
            get { return (int)row["User_Task_Status_Id"]; }
            set { this.setRowValue("User_Task_Status_Id", value); }
        }

        public int UserTaskTypeId
        {
            get { return (int)row["User_Task_Type_Id"]; }
            set { this.setRowValue("User_Task_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public UserTaskType UserTaskType()
        {
            return new UserTaskType(this.UserTaskTypeId);
        }

        public void ClearTask(string clearedComments)
        {
            ArrayList tasksCleared = new ArrayList();

            if (this.UserTaskStatusId != (int)modGeneratedEnums.UserTaskStatus.Task_Cleared)
            {
                this.ClearedComments = clearedComments;
                this.UserTaskStatusId = (int)modGeneratedEnums.UserTaskStatus.Task_Cleared;
                this.Update();
                tasksCleared.Add(this);

                //insert task history
                UserTaskHistory.ClearTask(this);
            }

            //insert filenote
            if (tasksCleared.Count > 0)
            {
                FileNote f = new FileNote();
                f.FileNoteTypeId=this.UserTaskType().ClearedFileNoteTypeId;
                f.FileNoteText = clearedComments + Environment.NewLine;
                f.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
                f.ClaimId = this.ClaimId;
                f.ClaimantId = this.ClaimantId;
                f.ReserveId = this.ReserveId;
                f.Update();
            }
        }

        public User LockedbyUser()
        {
            return new User(this.LockedByUserId);
        }

        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            if (this.isAdd)
            {
                try
                {
                    base.Update();
                }
                catch (SqlException ex)
                {
                    if (ex.ToString().ToLower().Contains("deadlock"))
                    {
                        //if task failed due to deadlock - try again
                        base.Update();
                    }
                    else
                        throw ex;
                }
                UserTaskHistory.CreateTask(this);
            }
            else
                base.Update();
            return 0;
        }

        public string DefaultDescription()
        {
            
            DateTime nowTime = ml.DBNow();
            string desc = this.UserTaskType().Description + " task was cleared on " + 
                nowTime.ToShortDateString() + " " + nowTime.ToLongTimeString() + "." + 
                Environment.NewLine + Environment.NewLine + 
                "Task Description: " + this.Description + Environment.NewLine;
            return desc;
        }

        public UserTaskLevel ClearUserTaskLevel()
        {
            return this.UserTaskType().ClearUserTaskLevel();
        }

        public UserTaskLevel CreateUserTaskLevel()
        {
            return this.UserTaskType().CreateUserTaskLevel();
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class RestrictionType : TypeTable
    {

        #region Constructors
        public RestrictionType() : base() { }
        public RestrictionType(int id) : base(id) { }
        public RestrictionType(string description) : base(description) { }
        public RestrictionType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int RestrictionTypeId
        {
            get { return (int)row["Restriction_Type_Id"]; }
            set { this.setRowValue("Restriction_Type_Id", value); }
        }
        #endregion

        #region Methods
        public override string TableName
        {
            get
            {
                return "Restriction_Type";
            }
        }

        public bool isNoRestrictions()
        {
            return this.Id == (int)modGeneratedEnums.RestrictionType.No_Restrictions;
        }

        public bool isReserve()
        {
            return this.Id == (int)modGeneratedEnums.RestrictionType.Reserve;
        }

        public bool isTransaction()
        {
            return this.Id == (int)modGeneratedEnums.RestrictionType.Transaction;
        }
        #endregion
    }
}

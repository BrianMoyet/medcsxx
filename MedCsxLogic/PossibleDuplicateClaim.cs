﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{

    public class PossibleDuplicateClaim:ClaimTable
    {
        #region Constructors
        public PossibleDuplicateClaim() : base() { }
        public PossibleDuplicateClaim(int id) : base(id) { }
        public PossibleDuplicateClaim(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Possible_Duplicate_Claim"; }
        }

        public string Description
        {
            get { return (string)row["Description"]; }
            set { this.setRowValue("Description", value); }
        }

        public int DuplicateClaimId
        {
            get { return (int)row["Duplicate_Claim_Id"]; }
            set { this.setRowValue("Duplicate_Claim_Id", value); }
        }

        public int PossibleDuplicateClaimId
        {
            get { return (int)row["Possible_Duplicate_Claim_Id"]; }
            set { this.setRowValue("Possible_Duplicate_Claim_Id", value); }
        }

        public int PossibleDuplicateClaimTypeId
        {
            get { return (int)row["Possible_Duplicate_Claim_Type_Id"]; }
            set { this.setRowValue("Possible_Duplicate_Claim_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public Claim DuplicateClaim()
        {
            return new Claim(this.DuplicateClaimId);
        }

        public PossibleDuplicateClaimType PossibleDuplicateClaimType()
        {
            return new PossibleDuplicateClaimType(this.PossibleDuplicateClaimTypeId);
        }

        #endregion
    
    }
}

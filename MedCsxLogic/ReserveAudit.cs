﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ReserveAudit:ReserveTable
    { 

        #region Class Variables
        private int m_ClaimantId = 0;
        private Draft m_Draft = null;
        #endregion

        #region Constructors
        public ReserveAudit() : base() { }
        public ReserveAudit(int id) : base(id) { }
        public ReserveAudit(Hashtable row) : base(row) { }
        public ReserveAudit(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Reserve_Audit"; }
        }

        public double LossLeakage
        {
            get { return (double)row["Loss_Leakage"]; }
            set { this.setRowValue("Loss_Leakage", value); }
        }

        public double ExpenseLeakage
        {
            get { return (double)row["Expense_Leakage"]; }
            set { this.setRowValue("Expense_Leakage", value); }
        }

        public string Comments
        {
            get { return (string)row["Comments"]; }
            set { this.setRowValue("Comments", value); }
        }
        #endregion

        #region Nonimplemented
        public override int ClaimantId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}

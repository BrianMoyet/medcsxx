﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BIDiagnostic:MedCsXTable
    {

        #region Constructors
        public BIDiagnostic() : base() { }
        public BIDiagnostic(int id) : base(id) { }
        public BIDiagnostic(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "BI_Diagnostic"; }
        }

        public double BestCaseCost
        {
            get { return Convert.ToDouble(row["Best_Case_Cost"]); }
            set { this.setRowValue("Best_Case_Cost", value); }
        }
        
        public int BIDiagnosticId
        {
            get { return (int)row["BI_Diagnostic_Id"]; }
            set { this.setRowValue("BI_Diagnostic_Id", value); }
        }

        public int BIDiagnosticTypeId
        {
            get { return (int)row["BI_Diagnostic_Type_Id"]; }
            set { this.setRowValue("BI_Diagnostic_Type_Id", value); }
        }

        public int BIEvaluationId
        {
            get { return (int)row["BI_Evaluation_Id"]; }
            set { this.setRowValue("BI_Evaluation_Id", value); }
        }

        public DateTime DiagnosticDate
        {
            get { return (DateTime)row["Diagnostic_Date"]; }
            set { this.setRowValue("Diagnostic_Date", value); }
        }

        public int OrderedById
        {
            get { return (int)row["Ordered_By_Id"]; }
            set { this.setRowValue("Ordered_By_Id", value); }
        }

        public double WorstCaseCost
        {
            get { return Convert.ToDouble(row["Worst_Case_Cost"]); }
            set { this.setRowValue("Worst_Case_Cost", value); }
        }
        #endregion

        #region Misc Methods
        public BIDiagnosisType BIDiagnosticType()
        {
            return new BIDiagnosisType(this.BIDiagnosticTypeId);
        }

        public BIEvaluation BIEvaluation()
        {
            return new BIEvaluation(this.BIEvaluationId);
        }

        public Vendor OrderedBy()
        {
            return new Vendor(this.OrderedById);
        }
        #endregion
    }
}

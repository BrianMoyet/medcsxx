﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Salvage : TransactionReferenceTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public Salvage() : base() { }
        public Salvage(int id) : base(id) { }
        public Salvage(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Salvage"; }
        }

        public Int64 CheckNo
        {
            get { return (Int64)row["Check_No"]; }
            set { this.setRowValue("Check_No", value); }
        }

        public bool isVoid
        {
            get { return (bool)row["Is_Void"]; }
            set { this.setRowValue("Is_Void", value); }
        }

        public int ReissuedFromSalvageId
        {
            get { return (int)row["Reissued_From_Salvage_Id"]; }
            set { this.setRowValue("Reissued_From_Salvage_Id", value); }
        }

        public int SalvageId
        {
            get { return (int)row["Salvage_Id"]; }
            set { this.setRowValue("Salvage_Id", value); }
        }

        public double SalvageYardCharges
        {
            get { return (double)((decimal)row["Salvage_Yard_Charges"]); }
            set { this.setRowValue("Salvage_Yard_Charges", value); }
        }

        public double StorageCharges
        {
            get { return (double)((decimal)row["Storage_Charges"]); }
            set { this.setRowValue("Storage_Charges", value); }
        }

        public int StorageDays
        {
            get { return (int)row["Storage_Days"]; }
            set { this.setRowValue("Storage_Days", value); }
        }

        public int SubroSalvageVoidReasonId
        {
            get { return (int)row["Subro_Salvage_Void_Reason_Id"]; }
            set { this.setRowValue("Subro_Salvage_Void_Reason_Id", value); }
        }
      
        public double Towing_Charges
        {
            get { return (double)((decimal)row["Towing_Charges"]); }
            set { this.setRowValue("Towing_Charges", value); }
        }
        
        public int VendorId
        {
            get { return (int)row["Vendor_Id"]; }
            set { this.setRowValue("Vendor_Id", value); }
        }
       


        public DateTime DateCompleted
        {
            get { return (DateTime?)row["Completed_Date"] == null ? ml.DEFAULT_DATE : (DateTime)row["Completed_Date"]; }
            set { this.setRowValue("Completed_Date", value); }
        }

        public bool IsLocked
        {
            get { return (bool?)row["Is_Locked"] == null ? false : (bool)row["Is_Locked"]; }
            set { this.setRowValue("Is_Locked", value); }
        }

        public DateTime LockDate
        {
            get { return (DateTime?)row["Lock_Date"] == null ? ml.DEFAULT_DATE : (DateTime)row["Lock_Date"]; }
            set { this.setRowValue("Lock_Date", value); }
        }

        public string SalvageYardDescription
        {
            get { return string.IsNullOrEmpty((string)row["Salvage_Yard_Description"]) ? "" : (string)row["Salvage_Yard_Description"]; }
            set { this.setRowValue("Salvage_Yard_Description", value); }
        }
        #endregion

        #region Misc Methods
        public Vendor Vendor()
        {
            return new Vendor(this.VendorId);
        }

        public int TransactionId
        {
            //returns transaction ID for salvage
            get { return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Salvage_Id", this.Id); }
        }

        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }

        public static int InsertSalvage(Hashtable salvage, int reserveId, double Amount)
        {
            /*inserts salvage and transaction rows and returns transaction ID of the newly inserted Transaction row.
             * subro - a Salvage row dictionary
             * reserveId - the Reserve_Id to add the salvage
            */

            Reserve res = new Reserve(reserveId);
            Transaction tr = new Transaction();
            int salvageId; //ID of newly inserted row
            int transactionId; //transaction ID for subro

            try
            {
                //insert Salvage row
                salvageId = ml.InsertRow(salvage, "Salvage");

                //insert Transaction row
                tr.ReserveId = reserveId;
                tr.SalvageId = salvageId;
                tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Salvage;
                tr.TransactionAmount = Amount;

                TransactionType trType = new TransactionType((int)modGeneratedEnums.TransactionType.Salvage);
                tr.isLoss = trType.isLoss;
                transactionId = tr.Update();

                //fire Salvage inserted event - has to happen manually - after transaction has been inserted
                tr.FireEvent((int)modGeneratedEnums.EventType.Salvage_Added);

                if (res.LossAmount < Amount)
                    tr.FireEvent((int)modGeneratedEnums.EventType.Salvage_Amount_Greater_Than_Reserve_Loss);
                //fire event warning about Salvage being greater than Loss

                //insert offsetting transaction reserve
                return res.InsertOffsettingTransaction(-1.0 * Amount, trType.isLoss);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool LockSalvage(int reserve_id)
        {
            Reserve res = new Reserve(reserve_id);
            TimeSpan days = DateTime.Today.Subtract(this.CreatedDate);
            TimeSpan DaysOver;
            int gracePeriod = 30;
            if (days.TotalDays > 120)
            {
                if (this.IsLocked)
                    return false;

                DaysOver = DateTime.Today.Subtract(this.LockDate);
                if ((DaysOver.TotalDays > gracePeriod) && (DaysOver.TotalDays < gracePeriod * 2))
                {
                    this.IsLocked = true;
                    this.LockDate = DateTime.Now;
                    res.FireEvent((int)modGeneratedEnums.EventType.Salvage_Wait_90);
                }
            }
            else if (days.TotalDays > 90) 
            {
                if (this.IsLocked)
                    return false;

                DaysOver = DateTime.Today.Subtract(this.LockDate);
                if (DaysOver.TotalDays > gracePeriod)
                {
                    this.IsLocked = true;
                    this.LockDate = DateTime.Now;
                    res.FireEvent((int)modGeneratedEnums.EventType.Salvage_Wait_90);
                }
            }
            else if (days.TotalDays > 60)
            {
                if (this.IsLocked)
                    return false;

                DaysOver = DateTime.Today.Subtract(this.LockDate);
                if (DaysOver.TotalDays > gracePeriod)
                {
                    this.IsLocked = true;
                    this.LockDate = DateTime.Now;
                    res.FireEvent((int)modGeneratedEnums.EventType.Salvage_Wait_60);
                }
            }
            else if ((days.TotalDays > 30) && (this.LockDate == ml.DEFAULT_DATE))
            {
                this.LockDate = DateTime.Now;
                res.FireEvent((int)modGeneratedEnums.EventType.Salvage_Wait_30);
            }

            return this.LockDate != ml.DEFAULT_DATE;
        }
        #endregion
    }
}

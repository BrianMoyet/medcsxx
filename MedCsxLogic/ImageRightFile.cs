﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public abstract class ImageRightFile:MedCsxObject
    {

        #region Class Variables
        private Claim m_Claim = null;
        private static clsDatabase db=new clsDatabase();
        #endregion

        #region Constructors
        public ImageRightFile() : base() { }
        public ImageRightFile(int claimId)
            : base()
        {
            this.Claim = new Claim(claimId);
        }
        #endregion

        #region Misc Methods
        public Claim Claim
        {
            get { return m_Claim; }
            set { m_Claim = value; }
        }

        public string DetermineClaimsImageRightDrawer(int companyId)
        {
            return ModConfig.ClaimsDrawer;
        }

        public string DeterminePolicyImageRightDrawer(int companyId, string policyNo)
        {
            return ModConfig.PolicyDrawer;
        }

        private bool isMJIBrokerageAccount(string policyNo)
        {
            string sql = "SELECT pp.provider_product_code FROM GAAS01.dbo.policy p ";
            sql += "INNER JOIN GAAS01.dbo.Provider_Product pp ON p.Product_Key = pp.Provider_Product_Key ";
            sql += "WHERE p.Policy_Number = '{0}'";

            string productCode = (string)db.ExecuteSelectReturnValue(string.Format(sql, policyNo));
            return productCode.Equals(ConfigurationManager.AppSettings["MJIBrokerageProductCode"]);
        }
        #endregion
    }
}

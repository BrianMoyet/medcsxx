﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public abstract class ClaimantTable:ClaimTable 
    {

        #region Class Variables
        private Claimant m_Claimant = null;
        #endregion

        #region Constructors
        public ClaimantTable() : base() { }
        public ClaimantTable(int id) : base(id) { }
        public ClaimantTable(Hashtable row) : base(row) { }
        public ClaimantTable(DataTable Table, DataRow Row) : base(Table, Row) { }
        #endregion

        #region Methods
        public virtual int ClaimantId
        {
            get { return (int)row["Claimant_Id"]; }
            set { this.setRowValue("Claimant_Id", value); }
        }

        public Claimant Claimant
        {
            get { return m_Claimant == null ? m_Claimant = new Claimant(this.ClaimantId) : m_Claimant; }
            set { m_Claimant = value; }
        }

        public string ClaimantName()
        {
            return this.Claimant.Name;
        }
        #endregion

        #region NonImplemented
        public virtual int ClaimId
        {
            get
            {
                return base.ClaimId;
            }
            set
            {
                base.ClaimId = value;
            }
        }
        #endregion

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class InjuryCause:TypeTable
    {

        #region Constructors
        public InjuryCause() : base() { }
        public InjuryCause(int id) : base(id) { }
        public InjuryCause(string description) : base(description) { }
        public InjuryCause(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Injury_Cause";
            }
        }
    }
}

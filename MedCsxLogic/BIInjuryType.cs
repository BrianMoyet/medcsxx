﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BIInjuryType:TypeTable
    {

        #region Constructors
        public BIInjuryType() : base() { }
        public BIInjuryType(int id) : base(id) { }
        public BIInjuryType(string description) : base(description) { }
        public BIInjuryType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "BI_Injury_Type";
            }
        }

        public int BIInjuryTypeId
        {
            get { return (int)row["BI_Injury_Type_Id"]; }
            set { this.setRowValue("BI_Injury_Type_Id", value); }
        }
        #endregion
    }
}

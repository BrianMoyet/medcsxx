﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ReserveType:TypeTable
    {

        #region Class Variables
        private static clsDatabase db = new clsDatabase();
        private PolicyCoverageType m_PolicyCoverageType = null;
        #endregion

        #region Constructors
        public ReserveType() : base() { }
        public ReserveType(int id) : base(id) { }
        public ReserveType(string description) : base(description) { }
        public ReserveType(Hashtable row) : base() { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Reserve_Type";
            }
        }

        public int DefaultAssignedUserGroupId
        {
            get { return (int)row["Default_Assigned_User_Group_Id"]; }
            set { this.setRowValue("Default_Assigned_User_Group_Id", value); }
        }

        public int ExperienceWeight
        {
            get { return (int)row["Experience_Weight"]; }
            set { this.setRowValue("Experience_Weight", value); }
        }

        public int LitigationGroup
        {
            get { return (int)row["Litigation_Group"]; }
            set { this.setRowValue("Litigation_Group", value); }
        }

        public bool isMedicalType
        {
            get { return (bool)row["Is_Medical_Type"]; }
            set { this.setRowValue("Is_Medical_Type", value); }
        }

        public bool isPip
        {
            get { return (bool)row["Is_Pip"]; }
            set { this.setRowValue("Is_Pip", value); }
        }

        public int PolicyCoverageTypeId
        {
            get { return (int)row["Policy_Coverage_Type_Id"]; }
            set { this.setRowValue("Policy_Coverage_Type_Id", value); }
        }

        public int ReceivedPerWeekWeight
        {
            get { return (int)row["Received_Per_Week_Weight"]; }
            set { this.setRowValue("Received_Per_Week_Weight", value); }
        }

        public int ReserveTypeId
        {
            get { return (int)row["Reserve_Type_Id"]; }
            set { this.setRowValue("Reserve_Type_Id", value); }
        }

        public int StaffingModelAnnualPerAdjuster
        {
            get { return (int)row["Staffing_Model_Annual_Per_Adjuster"]; }
            set { this.setRowValue("Staffing_Model_Annual_Per_Adjuster", value); }
        }

        [System.ComponentModel.Description("Sub type of claim.")]
        public int SubClaimTypeId
        {
            get { return (int)row["Sub_Claim_Type_Id"]; }
            set { this.setRowValue("Sub_Claim_Type_Id", value); }
        }

        public int TestAssignmentTypeId
        {
            get { return (int)row["Test_Assignment_Type_Id"]; }
            set { this.setRowValue("Test_Assignment_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public PolicyCoverageType PolicyCoverageType
        {
            get { return m_PolicyCoverageType == null ? m_PolicyCoverageType = new PolicyCoverageType(this.PolicyCoverageTypeId) : m_PolicyCoverageType; }
        }

        public UserGroup DefaultAssignedUserGroup()
        {
            return new UserGroup(this.DefaultAssignedUserGroupId);
        }

        public double UserAuthority()
        {
            //returns user's financial authority for reserve type id
            return db.GetDblFromStoredProcedure("usp_Get_User_Authority", Convert.ToInt32(HttpContext.Current.Session["userID"]), this.Id);
        }

        public bool isAbi
        {
            get { return ((this.Id == (int)modGeneratedEnums.ReserveType.ABI_Major) || (this.Id == (int)modGeneratedEnums.ReserveType.ABI_Minor)); }
        }

        public bool isPropertyDamage
        {
            get { return this.Id == (int)modGeneratedEnums.ReserveType.Property_Damage; }
        }
        #endregion
    }
}

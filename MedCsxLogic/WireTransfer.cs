﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class WireTransfer : TransactionReferenceTable
    {
        #region Constructors
        public WireTransfer() : base() { }
        public WireTransfer(int id) : base(id) { }
        public WireTransfer(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Wire_Transfer"; }
        }

        public DateTime DateWired
        {
            get { return (DateTime)row["Date_Wired"]; }
            set { this.setRowValue("Date_Wired", value); }
        }

        public string Notes
        {
            get { return (string)row["Notes"]; }
            set { this.setRowValue("Notes", value); }
        }
        
        public int TransactionId
        {
            get { return (int)row["Transaction_Id"]; }
            set { this.setRowValue("Transaction_Id", value); }
        }

        public int VoidTransactionId
        {
            get { return (int)row["Void_Transaction_Id"]; }
            set { this.setRowValue("Void_Transaction_Id", value); }
        }

        public int WireTransferId
        {
            get { return (int)row["Wire_Transfer_Id"]; }
            set { this.setRowValue("Wire_Transfer_Id", value); }
        }

        public string WiredTo
        {
            get { return (string)row["Wired_To"]; }
            set { this.setRowValue("Wired_To", value); }
        }
        #endregion

        #region Methods
        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }

        public string Description
        {
            get
            {
                string s = this.DateWired.ToShortDateString() + " - " + (this.Transaction().TransactionAmount * -1).ToString("c") + " to " + this.WiredTo;
                return s;
            }
        }
        #endregion
    }
}

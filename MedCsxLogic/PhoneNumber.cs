﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
   public  class PhoneNumber:ClaimTable
   {

       private static clsDatabase db = new clsDatabase();
       #region Constructors
       public PhoneNumber() : base() { }
       public PhoneNumber(int id) : base(id) { }
       public PhoneNumber(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get { return "Phone_Number"; }
       }

       public string CellPhone
       {
           get { return (string)row["Cell_Phone"]; }
           set { this.setRowValue("Cell_Phone", value); }
       }

       public string Email
       {
           get { return (string)row["Email"]; }
           set { this.setRowValue("Email", value); }
       }

       public string Fax
       {
           get { return (string)row["Fax"]; }
           set { this.setRowValue("Fax", value); }
       }

       public string HomePhone
       {
           get { return (string)row["Home_Phone"]; }
           set { this.setRowValue("Home_Phone", value); }
       }

       public string Name
       {
           get { return (string)row["Name"]; }
           set { this.setRowValue("Name", value); }
       }

       public int PartyId
       {
           get { return (int)row["Party_Id"]; }
           set { this.setRowValue("Party_Id", value); }
       }
       
       public int PersonId
       {
           get { return (int)row["Person_Id"]; }
           set { this.setRowValue("Person_Id", value); }
       }

       public string PersonType
       {
           get { return (string)row["Person_Type"]; }
           set { this.setRowValue("Person_Type", value); }
       }

       public int PhoneNumberId
       {
           get { return (int)row["Phone_Number_Id"]; }
           set { this.setRowValue("Phone_Number_Id", value); }
       }

       public bool RefreshedAutomatically
       {
           get { return (bool)row["Refreshed_Automatically"]; }
           set { this.setRowValue("Refreshed_Automatically", value); }
       }

       public int VendorId
       {
           get { return (int)row["Vendor_Id"]; }
           set { this.setRowValue("Vendor_Id", value); }
       }

       public string WorkPhone
       {
           get { return (string)row["Work_Phone"]; }
           set { this.setRowValue("Work_Phone", value); }
       }

       public string Note
       {
           get { return db.GetStringFromStoredProcedure("usp_Get_Phone_Number_Note", this.ClaimId, this.Name); }
           set { db.ExecuteStoredProcedure("usp_Update_Phone_Number_Note", this.ClaimId, this.Name, value); }
       }
       #endregion
   }
}

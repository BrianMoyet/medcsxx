﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public class PendingDraft:ReserveTable
   {

       #region Class Variables
       private static modLogic ml = new modLogic();
       #endregion

       #region Constructors
       public PendingDraft() : base() { }
       public PendingDraft(int id) : base(id) { }
       public PendingDraft(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get { return "Pending_Draft"; }
       }

       public override int ClaimantId
       {
           get
           {
               return this.Reserve.ClaimantId;
           }
           set
           {
               base.ClaimantId = value;
           }
       }

       public override int ClaimId
       {
           get
           {
               return this.Reserve.ClaimId;
           }
           set
           {
               base.ClaimId = value;
           }
       }

       public Draft Draft
       {
           get { return new Draft(DraftId); }
       }

       public int DraftId
       {
           get { return (int)row["Draft_Id"]; }
           set { this.setRowValue("Draft_Id", value); }
       }

       public DraftPrint DraftPrint
       {
           get { return new DraftPrint(DraftPrintId); }
       }

       public int DraftPrintId
       {
           get { return (int)row["Draft_Print_Id"]; }
           set { this.setRowValue("Draft_Print_Id", value); }
       }

       public int PendingDraftId
       {
           get { return (int)row["Pending_Draft_Id"]; }
           set { this.setRowValue("Pending_Draft_Id", value); }
       }

       public string PendingDraftSentTo
       {
           get { return (string)row["Pending_Draft_Sent_To"]; }
           set { this.setRowValue("Pending_Draft_Sent_To", value); }
       }

       public PendingDraftStatus PendingDraftStatus
       {
           get { return new PendingDraftStatus(PendingDraftStatusId); }
       }

       public int PendingDraftStatusId
       {
           get { return (int)row["Pending_Draft_Status_Id"]; }
           set { this.setRowValue("Pending_Draft_Status_Id", value); }
       }

       public Transaction Transaction
       {
           get { return new Transaction(TransactionId); }
       }

       public int TransactionId
       {
           get { return (int)row["Transaction_Id"]; }
           set { this.setRowValue("Transaction_Id", value); }
       }

       public TransactionType TransactionType
       {
           get { return new TransactionType(TransactionTypeId); }
       }

       public int TransactionTypeId
       {
           get { return (int)row["Transaction_Type_Id"]; }
           set { this.setRowValue("Transaction_Type_Id", value); }
       }
       #endregion
       
       #region Methods
       public void FinalizeDraft(double amount)
       {
           Draft d = this.Draft;
           d.ActualPrintDate = DateTime.Now;    //update the actual print date
           d.Update();

           //transaction
           bool isLoss = this.TransactionType.isLoss;
           Transaction tr = new Transaction();
           tr.ReserveId = this.ReserveId;
           tr.DraftId = this.DraftId;
           tr.TransactionAmount = amount * -1;
           tr.TransactionDate = ml.DBNow();
           tr.TransactionTypeId = this.TransactionTypeId;
           tr.isLoss = isLoss;
           tr.UserId = ml.CurrentUser.Id;
           this.TransactionId = tr.Update();

           //reserves
           if (isLoss)
           {
               this.Reserve.NetLossReserve -= amount;
               this.Reserve.PaidLoss += amount;
           }
           else
           {
               this.Reserve.NetExpenseReserve -= amount;
               this.Reserve.PaidExpense += amount;
           }
           this.Reserve.Update();

           //final loss payment - close reserve
           if (this.TransactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment)
               this.Reserve.Close();
           else if (this.Reserve.isClosed || this.Reserve.isClosedPendingSalvage || this.Reserve.isClosedPendingSubro)
           {    //supplemental payment - offset transaction and change reserve amounts
               this.Reserve.InsertOffsettingTransaction(amount, isLoss);
               this.Reserve.NoSupplementalPayments += 1;
               this.Reserve.NetLossReserve = 0;
               this.Reserve.GrossLossReserve = 0;
               this.Reserve.NetExpenseReserve = 0;
               this.Reserve.GrossExpenseReserve = 0;
               this.Reserve.Update();

               //claim closed - increment supplemental payments
               if (this.Claim.isClosed)
               {
                   this.Claim.NoSupplementalPayments += 1;
                   this.Claim.Update();
               }
           }
           this.PendingDraftStatusId = (int)modGeneratedEnums.PendingDraftStatus.Complete;
           this.Update();
       }
       #endregion



   }
}

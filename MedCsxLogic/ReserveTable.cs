﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public abstract class ReserveTable:ClaimantTable 
    {

        private Reserve m_Reserve = null;

        #region Constructors
        public ReserveTable() : base() { }
        public ReserveTable(int id) : base(id) { }
        public ReserveTable(DataTable Table, DataRow Row) : base(Table, Row) { }
        public ReserveTable(Hashtable row) : base(row) { }
        #endregion

        #region Methods
        public override int ClaimId
        {
            get { return Claimant.ClaimantId; }
            set
            {
                this.setRowValue("Claim_Id", value);
                Claimant.ClaimId = value;
            }
        }

        public virtual int ReserveId
        {
            get { return (int)row["Reserve_Id"]; }
            set
            {
                m_Reserve = null;
                this.setRowValue("Reserve_Id", value);
            }
        }

        public Reserve Reserve
        {
            get { return m_Reserve == null ? m_Reserve = new Reserve(ReserveId) : m_Reserve; }
            set { m_Reserve = value; }
        }
        #endregion

    }
}

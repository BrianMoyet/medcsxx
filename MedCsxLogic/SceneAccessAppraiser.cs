﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class SceneAccessAppraiser:MedCsXTable
    {

        #region Class Variables
        private Vendor m_Vendor = null;
        private static clsDatabase db=new clsDatabase();
        #endregion

        #region Constructors
        public SceneAccessAppraiser() : base() { }
        public SceneAccessAppraiser(int id) : base(id) { }
        public SceneAccessAppraiser(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Scene_Access_Appraiser"; }
        }

        public bool Active
        {
            get { return (bool)row["Active"]; }
            set { this.setRowValue("Active", value); }
        }

        public int SceneAccessUserId
        {
            get { return (int)row["Scene_Access_User_Id"]; }
            set { this.setRowValue("Scene_Access_User_Id", value); }
        }
        
        public bool UseEmail
        {
            get { return (bool)row["Use_Email"]; }
            set { this.setRowValue("Use_Email", value); }
        }

        public int VendorId
        {
            get { return (int)row["Vendor_Id"]; }
            set { this.setRowValue("Vendor_Id", value); }
        }
        #endregion
        #region Misc Methods
        public override void DeleteRow()
        {
            this.DeleteStates();
            base.DeleteRow();
        }

        public Vendor Vendor
        {
            get { return m_Vendor == null ? m_Vendor = new Vendor(this.VendorId) : m_Vendor; }
        }

        public void DeleteStates()
        {
            db.ExecuteStoredProcedure("usp_Delete_Scene_Access_Appraiser_States", this.Id);
        }

        public void AddState(string stateName)
        {
            SceneAccessAppraiserState st = new SceneAccessAppraiserState();
            st.SceneAccessAppraiserId = this.Id;
            st.StateId = State.idForName(stateName);
            st.Update();
        }
        #endregion
    }
}

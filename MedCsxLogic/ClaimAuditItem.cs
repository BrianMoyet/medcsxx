﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ClaimAuditItem:ClaimTable
    {

        #region Class Variables
        private ClaimAuditItemType m_ClaimAuditItemType = null;
        private ClaimAuditResult m_ClaimAuditResult = null;
        #endregion

        #region Constructors
        public ClaimAuditItem() : base() { }
        public ClaimAuditItem(int id) : base(id) { }
        public ClaimAuditItem(Hashtable row) : base(row) { }
        public ClaimAuditItem(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Claim_Audit_Item"; }
        }

        public int ClaimAuditItemTypeId
        {
            get { return (int)row["Claim_Audit_Item_Type_Id"]; }
            set 
            { 
                this.setRowValue("Claim_Audit_Item_Type_Id", value);
                this.m_ClaimAuditItemType = null;
            }
        }

        public int ClaimAuditResultId
        {
            get { return (int)row["Claim_Audit_Result_Id"]; }
            set 
            { 
                this.setRowValue("Claim_Audit_Result_Id", value);
                this.m_ClaimAuditResult = null;
            }
        }
        #endregion

        #region Misc Methods
        public ClaimAuditItemType ClaimAuditItemType
        {
            get { return this.m_ClaimAuditItemType == null ? this.m_ClaimAuditItemType = new ClaimAuditItemType(this.ClaimAuditItemTypeId) : this.m_ClaimAuditItemType; }
        }

        public ClaimAuditResult ClaimAuditResult
        {
            get { return this.m_ClaimAuditResult == null ? this.m_ClaimAuditResult = new ClaimAuditResult(this.ClaimAuditResultId) : this.m_ClaimAuditResult; }
        }

        public string ItemTypeDescription
        {
            //returns description of item type for policy state
            get
            {
                string state;
                try
                {
                    state = this.Claim.Policy().State.Abbreviation;
                }
                catch (Exception ex)
                {
                    //can't find policy state - use Kansas by default
                    state = "KS";
                }

                try
                {
                    ClaimAuditItemTypeState s = (ClaimAuditItemTypeState)this.ClaimAuditItemType.States[state];
                    return s.Description;
                }
                catch (Exception ex)
                {
                    //No definition has been entered for Policy State
                    return "Definition Not Found for " + this.ClaimAuditItemType.Description;
                }
                
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ChoicePointRequest : MedCsXTable
    {

        #region Class Variables
        private const string CHOICEPOINT_ACCOUNT_NBR = "5557";
        private readonly DateTime DEFAULT_DATE = new DateTime(1900, 1, 1); //Default date for date fields.  Nulls aren't allowed in DB.
        private Vehicle m_Vehicle = null;
        #endregion

        #region Constructors
        public ChoicePointRequest()
            : base()
        {
            SetDefaultValues();
        }

        public ChoicePointRequest(int id) : base(id) { }
        public ChoicePointRequest(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "ChoicePoint_Request"; }
        }

        public string AdjusterDivision
        {
            get { return (string)row["Adjuster_Division"]; }
            set { this.setRowValue("Adjuster_Division", value); }
        }

        public string AdjusterId
        {
            get { return (string)row["Adjuster_Id"]; }
            set { this.setRowValue("Adjuster_Id", value); }
        }

        public string ChoicePointAccountNumber
        {
            get { return (string)row["ChoicePoint_Account_Number"]; }
            set { this.setRowValue("ChoicePoint_Account_Number", value); }
        }

        public int ChoicePointReportTypeId
        {
            get { return (int)row["ChoicePoint_Report_Type_Id"]; }
            set { this.setRowValue("ChoicePoint_Report_Type_Id", value); }
        }

        public int ChoicePointRequestId
        {
            get { return (int)row["ChoicePoint_Request_Id"]; }
            set { this.setRowValue("ChoicePoint_Request_Id", value); }
        }

        public int ChoicePointRequestStatusId
        {
            get { return (int)row["ChoicePoint_Request_Status_Id"]; }
            set { this.setRowValue("ChoicePoint_Request_Status_Id", value); }
        }

        public string CommentIndicator
        {
            get { return (string)row["Comment_Indicator"]; }
            set { this.setRowValue("Comment_Indicator", value); }
        }

        public string DescriptionIfOther
        {
            get { return (string)row["Description_If_Other"]; }
            set { this.setRowValue("Description_If_Other", value); }
        }

        public string DisplayClaimId
        {
            get { return (string)row["Display_Claim_Id"]; }
            set { this.setRowValue("Display_Claim_Id", value); }
        }

        public string EndOfRecord
        {
            get { return (string)row["End_Of_Record"]; }
            set { this.setRowValue("End_Of_Record", value); }
        }

        public string InsuredDriversLicenseNo
        {
            get { return (string)row["Insured_Drivers_License_No"]; }
            set { this.setRowValue("Insured_Drivers_License_No", value); }
        }

        public string InsuredDriversLicenseState
        {
            get { return (string)row["Insured_Drivers_License_State"]; }
            set { this.setRowValue("Insured_Drivers_License_State", value); }
        }

        public string InsuredFirstName
        {
            get { return (string)row["Insured_First_Name"]; }
            set { this.setRowValue("Insured_First_Name", value); }
        }

        public string InsuredLastName
        {
            get { return (string)row["Insured_Last_Name"]; }
            set { this.setRowValue("Insured_Last_Name", value); }
        }

        public string InsuredMiddleName
        {
            get { return (string)row["Insured_Middle_Name"]; }
            set { this.setRowValue("Insured_Middle_Name", value); }
        }

        public string InsuredSSN
        {
            get { return (string)row["Insured_SSN"]; }
            set { this.setRowValue("Insured_SSN", value); }
        }

        public string InvestigatingAgencyName
        {
            get { return (string)row["Investigating_Agency_Name"]; }
            set { this.setRowValue("Investigating_Agency_Name", value); }
        }

        public string InvestigatingAgencyType
        {
            get { return (string)row["Investigating_Agency_Type"]; }
            set { this.setRowValue("Investigating_Agency_Type", value); }
        }

        public int InvestigatingAgencyTypeId
        {
            get { return (int)row["Investigating_Agency_Type_Id"]; }
            set { this.setRowValue("Investigating_Agency_Type_Id", value); }
        }

        public string LossCity
        {
            get { return (string)row["Loss_City"]; }
            set { this.setRowValue("Loss_City", value); }
        }

        public string LossCounty
        {
            get { return (string)row["Loss_County"]; }
            set { this.setRowValue("Loss_County", value); }
        }

        public string LossLocation
        {
            get { return (string)row["Loss_Location"]; }
            set { this.setRowValue("Loss_Location", value); }
        }

        public string LossState
        {
            get { return (string)row["Loss_State"]; }
            set { this.setRowValue("Loss_State", value); }
        }

        public string PoliceReportDateOfLoss
        {
            get { return (string)row["Police_Report_Date_Of_Loss"]; }
            set { this.setRowValue("Police_Report_Date_Of_Loss", value); }
        }

        public string PrecinctTroopNumber
        {
            get { return (string)row["Precinct_Troop_Number"]; }
            set { this.setRowValue("Precinct_Troop_Number", value); }
        }

        public string RecordType
        {
            get { return (string)row["Record_Type"]; }
            set { this.setRowValue("Record_Type", value); }
        }

        public string ReportNumber
        {
            get { return (string)row["Report_Number"]; }
            set { this.setRowValue("Report_Number", value); }
        }

        public string SecondPartyFirstName
        {
            get { return (string)row["Second_Party_First_Name"]; }
            set { this.setRowValue("Second_Party_First_Name", value); }
        }

        public string SecondPartyLastName
        {
            get { return (string)row["Second_Party_Last_Name"]; }
            set { this.setRowValue("Second_Party_Last_Name", value); }
        }

        public string SecondPartyMiddleName
        {
            get { return (string)row["Second_Party_Middle_Name"]; }
            set { this.setRowValue("Second_Party_Middle_Name", value); }
        }

        public string SequentialRecordCounter
        {
            get { return (string)row["Sequential_Record_Counter"]; }
            set { this.setRowValue("Sequential_Record_Counter", value); }
        }

        public string SignedReleaseConfirmation
        {
            get { return (string)row["Signed_Release_Confirmation"]; }
            set { this.setRowValue("Signed_Release_Confirmation", value); }
        }

        public string ThirdPartyFirstName
        {
            get { return (string)row["Third_Party_First_Name"]; }
            set { this.setRowValue("Third_Party_First_Name", value); }
        }

        public string ThirdPartyLastName
        {
            get { return (string)row["Third_Party_Last_Name"]; }
            set { this.setRowValue("Third_Party_Last_Name", value); }
        }

        public string ThirdPartyMiddleName
        {
            get { return (string)row["Third_Party_Middle_Name"]; }
            set { this.setRowValue("Third_Party_Middle_Name", value); }
        }

        public string TimeOfLoss
        {
            get { return (string)row["Time_Of_Loss"]; }
            set { this.setRowValue("Time_Of_Loss", value); }
        }

        public string TypeOfReportRequested
        {
            get { return (string)row["Type_Of_Report_Requested"]; }
            set { this.setRowValue("Type_Of_Report_Requested", value); }
        }

        public string VehicleLicensePlate
        {
            get { return (string)row["Vehicle_License_Plate"]; }
            set { this.setRowValue("Vehicle_License_Plate", value); }
        }

        public string VehicleLicensePlateState
        {
            get { return (string)row["Vehicle_License_Plate_State"]; }
            set { this.setRowValue("Vehicle_License_Plate_State", value); }
        }

        public string VehicleMake
        {
            get { return (string)row["Vehicle_Make"]; }
            set { this.setRowValue("Vehicle_Make", value); }
        }

        public string VehicleModel
        {
            get { return (string)row["Vehicle_Model"]; }
            set { this.setRowValue("Vehicle_Model", value); }
        }

        public string VehicleYear
        {
            get { return (string)row["Vehicle_Year"]; }
            set { this.setRowValue("Vehicle_Year", value); }
        }

        public string VIN
        {
            get { return (string)row["VIN"]; }
            set { this.setRowValue("VIN", value); }
        }
        #endregion

        #region Modified Column Names
        public string Comment
        {
            get { return (string)row["Comment"]; }
            set
            {
                this.setRowValue("Comment", value);
                if (value.Trim().Length > 0)
                    this.CommentIndicator = "C";
            }
        }

        public string DateOfLoss
        {
            get { return PoliceReportDateOfLoss; }
            set
            {
                //set date of loss to value in MMDDYYYY format
                string dateString;
                DateTime d = DateTime.Parse(value);
                int month = d.Month,
                    day = d.Day;

                if (month < 10)
                    dateString = "0" + month.ToString();
                else
                    dateString = month.ToString();

                if (day < 10)
                    dateString += "0" + day.ToString();
                else
                    dateString += day.ToString();

                dateString += d.Year.ToString();
                PoliceReportDateOfLoss = dateString;

                //set time of loss ot HH:MM am/pm format
                TimeOfLoss = d.ToShortTimeString();
            }
        }

        public string InsuredDateOfBirth
        {
            get { return (string)row["Insured_Date_Of_Birth"]; }
            set
            {
                //set date of loss to value in MMDDYYYY format
                string dateString;
                DateTime d = DateTime.Parse(value);
                int month = d.Month,
                    day = d.Day;

                if (d == DEFAULT_DATE)
                    return;

                if (month < 10)
                    dateString = "0" + month.ToString();
                else
                    dateString = month.ToString();

                if (day < 10)
                    dateString += "0" + day.ToString();
                else
                    dateString += day.ToString();

                dateString += d.Year.ToString();

                this.setRowValue("Insured_Date_Of_Birth", dateString);
            }
        }

        public int VehicleId
        {
            get { return (int)row["Vehicle_Id"]; }
            set
            {
                this.setRowValue("Vehicle_Id", value);
                this.m_Vehicle = null;
            }
        }

        public string InvestigatingAgencyTypeDesc
        {
            get
            {
                InvestigatingAgencyType i = new InvestigatingAgencyType(this.InvestigatingAgencyTypeId);
                return i.Description;
            }
            set
            {
                InvestigatingAgencyType i = new InvestigatingAgencyType(value);
                this.InvestigatingAgencyType = i.InvestigatingAgencyTypeCode;        //set investigating agency

                this.InvestigatingAgencyTypeId = i.Id;        //set investigating agency type id
            }
        }

        public string ReportTypeDesc
        {
            get
            {
                return (new ChoicePointReportType(this.ChoicePointReportTypeId)).Description;
            }
            set
            {
                //set type of report requested
                ChoicePointReportType rt = new ChoicePointReportType(value);
                this.TypeOfReportRequested = rt.ChoicePointReportTypeCode;

                //set police report type id
                this.ChoicePointReportTypeId = rt.Id;
            }
        }

        public Vehicle Vehicle
        {
            get { return m_Vehicle == null ? m_Vehicle = new Vehicle(this.VehicleId) : m_Vehicle; }
            set
            {
                m_Vehicle = value;

                PopulateVehicleColumns();
                PopulateClaimColumns();
            }
        }

        public string Description
        {
            get
            { //return police report description, suitable for file note generation
                string s;
                s = "Report Type: " + this.ReportTypeDesc + Environment.NewLine;

                if (this.DescriptionIfOther.Trim().Length > 0)
                    s += "Other Report Description: " + this.DescriptionIfOther + Environment.NewLine;

                s += "Investigating Agency: " + this.InvestigatingAgencyName + Environment.NewLine;
                s += "Investigating Agency Type: " + this.InvestigatingAgencyTypeDesc + Environment.NewLine;
                s += "Vehicle: " + this.Vehicle.Name + Environment.NewLine;
                s += "Requesting Adjuster: " + (new User(int.Parse(this.AdjusterId))).Name + Environment.NewLine;

                if (this.Comment.Trim().Length > 0)
                    s += "Comment: " + this.Comment + Environment.NewLine;

                s += "Claim: " + this.DisplayClaimId + Environment.NewLine;
                s += "Date of Loss: " + this.DateOfLoss + Environment.NewLine;
                s += "Insured: " + this.InsuredFirstName + " " + this.InsuredLastName + Environment.NewLine;

                if (this.InsuredSSN.Trim().Length > 0)
                    s += "Insured SSN: " + this.InsuredSSN + Environment.NewLine;

                if (this.InsuredDateOfBirth.Trim().Length > 0)
                    s += "Insured Date of Birth: " + this.InsuredDateOfBirth + Environment.NewLine;

                if (this.InsuredDriversLicenseNo.Trim().Length > 0)
                    s += "Insured Drivers License No: " + this.InsuredDriversLicenseNo + Environment.NewLine;

                if (this.LossLocation.Trim().Length > 0)
                    s += "Loss Location: " + this.LossLocation + Environment.NewLine;

                if (this.LossCity.Trim().Length > 0)
                    s += "Loss City: " + this.LossCity + Environment.NewLine;

                if (this.LossCounty.Trim().Length > 0)
                    s += "Loss County: " + this.LossCounty + Environment.NewLine;

                if (this.LossState.Trim().Length > 0)
                    s += "Loss State: " + this.LossState + Environment.NewLine;

                if (this.ReportNumber.Trim().Length > 0)
                    s += "Report Number: " + this.ReportNumber + Environment.NewLine;

                if (this.PrecinctTroopNumber.Trim().Length > 0)
                    s += "Precinct or Troop Number: " + this.PrecinctTroopNumber + Environment.NewLine;

                if ((this.SecondPartyFirstName.Trim().Length > 0) || (this.SecondPartyLastName.Trim().Length > 0))
                    s += "Second Party: " + this.SecondPartyFirstName + " " + this.SecondPartyLastName + Environment.NewLine;

                if ((this.ThirdPartyFirstName.Trim().Length > 0) || (this.ThirdPartyLastName.Trim().Length > 0))
                    s += "Second Party: " + this.ThirdPartyFirstName + " " + this.ThirdPartyLastName + Environment.NewLine;
                return s;
            }
        }

        public Claim Claim
        {
            get { return new Claim(modLogic.ExtractClaimId(this.DisplayClaimId)); }
        }

        public ChoicePointReportType ChoicePointReportType
        {
            get { return new ChoicePointReportType(this.ChoicePointReportTypeId); }
        }

        public string ShortDescription
        {
            get { return this.InvestigatingAgencyName + " " + this.ReportTypeDesc + " Report " + this.ReportNumber; }
        }
        #endregion

        #region Methods
        private void PopulateVehicleColumns()
        {
            this.VehicleId = Vehicle.Id;
            this.VIN = Vehicle.VIN;
            this.VehicleLicensePlate = Vehicle.LicensePlateNo;
            this.VehicleLicensePlateState = Vehicle.LicensePlateState.Abbreviation;
            this.VehicleMake = Vehicle.Manufacturer;
            this.VehicleModel = Vehicle.Model;
            this.VehicleYear = Vehicle.YearMade;
        }

        private void PopulateClaimColumns()
        {
            Claim cl = Vehicle.Claim;

            this.AdjusterId = cl.AdjusterId.ToString();
            this.DateOfLoss = cl.DateOfLoss.ToString();
            this.DisplayClaimId = cl.DisplayClaimId;
            this.InsuredLastName = cl.InsuredPerson.LastName;
            this.InsuredFirstName = cl.InsuredPerson.FirstName;
            this.InsuredMiddleName = cl.InsuredPerson.MiddleInitial;
            this.InsuredDateOfBirth = cl.InsuredPerson.DateOfBirth.ToString();

            //SSN
            string ssn = cl.InsuredPerson.SSN;
            if (ssn == null)
                ssn = "";
            this.InsuredSSN = ssn.Replace("-", "");

            string dl = cl.InsuredPerson.DriversLicenseNo;
            string[] dlPart = dl.Split(' ');

            if (dlPart.Length - 1 > 0)
            {
                this.InsuredDriversLicenseState = dlPart[0];
                this.InsuredDriversLicenseNo = dlPart[1];
            }
            else
                this.InsuredDriversLicenseNo = cl.InsuredPerson.DriversLicenseNo;
        }

        private void SetDefaultValues()
        {
            this.ChoicePointAccountNumber = CHOICEPOINT_ACCOUNT_NBR;
            this.AdjusterDivision = "";
            this.SignedReleaseConfirmation = "Y";
            this.EndOfRecord = "           " + Environment.NewLine;
        }
        #endregion
    }
}

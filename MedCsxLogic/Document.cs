﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class Document:ClaimTable
   {

       #region Constructors
       public Document()
           : base()
       {
           row = new Hashtable();
       }
       public Document(int id) : base(id) { }
       public Document(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get { return "Document"; }
       }

       public int ClaimantId
       {
           get { return (int)row["Claimant_Id"]; }
           set { this.setRowValue("Claimant_Id", value); }
       }

       public DateTime DatePrinted
       {
           get { return (DateTime)row["Date_Printed"]; }
           set { this.setRowValue("Date_Printed", value); }
       }

       public int DocumentId
       {
           get { return (int)row["Document_Id"]; }
           set { this.setRowValue("Document_Id", value); }
       }

       public int DocumentStatusId
       {
           get { return (int)row["Document_Status_Id"]; }
           set { this.setRowValue("Document_Status_Id", value); }
       }

       public int DocumentTypeId
       {
           get { return (int)row["Document_Type_Id"]; }
           set { this.setRowValue("Document_Type_Id", value); }
       }

       public int DraftId
       {
           get { return (int)row["Draft_Id"]; }
           set { this.setRowValue("Draft_Id", value); }
       }

       public int FileNoteId
       {
           get { return (int)row["File_Note_Id"]; }
           set { this.setRowValue("File_Note_Id", value); }
       }

       public bool ManuallyAdded
       {
           get { return (bool)row["Manually_Added"]; }
           set { this.setRowValue("Manually_Added", value); }
       }

       public int ReserveId
       {
           get { return (int)row["Reserve_Id"]; }
           set { this.setRowValue("Reserve_Id", value); }
       }

       public int TransactionId
       {
           get { return (int)row["Transaction_Id"]; }
           set { this.setRowValue("Transaction_Id", value); }
       }

       public int VehicleId
       {
           get { return (int)row["Vehicle_Id"]; }
           set { this.setRowValue("Vehicle_Id", value); }
       }
       #endregion

       #region Methods
       public FileNote FileNote
       {
           get { return new FileNote(this.FileNoteId); }
       }

       public DocumentType DocumentType
       {
           get { return new DocumentType(this.DocumentTypeId); }
       }

       public Claimant Claimant
       {
           get { return new Claimant(this.ClaimantId); }
       }

       public Reserve Reserve
       {
           get { return new Reserve(this.ReserveId); }
       }

       public Transaction Transaction
       {
           get { return new Transaction(this.TransactionId); }
       }

       public string TemplateFileName
       {
           get { return this.DocumentType.FileName; }
       }

       public Vehicle Vehicle
       {
           get { return new Vehicle(this.VehicleId); }
       }

       public Draft Draft
       {
           get { return new Draft(this.DraftId); }
       }
       #endregion
   }
}

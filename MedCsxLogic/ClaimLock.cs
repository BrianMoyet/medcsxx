﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ClaimLock:LockTable
    {


        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public ClaimLock() : base() { }
        public ClaimLock(int id) : base(id) { }
        public ClaimLock(Hashtable row) : base(row) { }
        public ClaimLock(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Claim_Lock"; }
        }

        public int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public int ClaimLockId
        {
            get { return (int)row["Claim_Lock_Id"]; }
            set { this.setRowValue("Claim_Lock_Id", value); }
        }
        #endregion

        #region Misc Methods
        public Claim Claim()
        {
            return new Claim(this.ClaimId);
        }

        public void SendUnlockAlerts()
        {
            //sends unlock alerts for a claim
            ArrayList userIds = new ArrayList(); //collection of user group IDs to send alerts to
            FileNote f = new FileNote(this.LockFileNoteId);
            EventType et = new EventType(f.EventTypeId);

            //add users to the userIds dictionary
            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Alert_Definition_Unlock_Users", et.Id).Rows)
                userIds.Add(row["User_Id"]);

            //send alerts
            et.InsertAlerts(this.ClaimId,0,this.UnlockFileNoteId,userIds, Convert.ToInt32(HttpContext.Current.Session["userID"]), ml.DEFAULT_DATE, false, true);
        }

        public void Unlock(string unlockFileNoteText)
        {
            //unlock claim - update claim lock row to turn lock off
            int unlockFileNoteId = this.Claim().InsertFileNote((int)modGeneratedEnums.FileNoteType.Unlock_Claim, unlockFileNoteText); //insert unlock file note

            //unlock claim
            this.isLocked = false;
            this.UnlockDate = ml.DBNow();
            this.UnlockedBy = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            this.UnlockFileNoteId = unlockFileNoteId;
            this.Update();

            //clear alerts issued because of lock
            FileNote f = new FileNote(this.LockFileNoteId);
            f.ClearAlerts();

            //file unlock claim event
            this.Claim().FireEvent((int)modGeneratedEnums.EventType.Unlock_Claim);

            //send unlock alerts
            this.SendUnlockAlerts();
        }
        #endregion
    }
}

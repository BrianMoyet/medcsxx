﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class State : TypeTable
    {

        #region Constructors
        public State() : base() { }
        public State(string description) : base(description) { }
        public State(int id) : base(id) { }
        public State(Hashtable row) : base(row) { }
        #endregion

        private static clsDatabase db = new clsDatabase();
        #region Methods
        public static int idForName(string stateName)
        {
            // Returns state ID given state name
            return (int)db.GetIntFromStoredProcedure("usp_Get_State_Id_For_Name", stateName);
        }

        public static int idForAbbr(string abbreviation)
        {
            // Returns state ID given state abbreviation
            return db.GetIntFromStoredProcedure("usp_Get_State_Id_For_Abbr", abbreviation);
        }

        public static string nameForId(int stateID)
        {
            // Returns state name given state ID 
            return db.GetStringFromStoredProcedure("usp_Get_State", stateID);
        }

        public override string TableName
        {
            get
            {
                return "State";
            }
        }

        public string Abbreviation
        {
            get { return (string)row["Abbreviation"]; }
            set { this.setRowValue("Abbreviation", value); }
        }

        public string stateDefaultText
        {
            get { return db.GetStringFromStoredProcedure("usp_Get_State_Text_For_State_Id", this.Id); }
        }
        #endregion
    }
}

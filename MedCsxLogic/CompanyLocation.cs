﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class CompanyLocation:TypeTable
    {

        #region Constructors
        public CompanyLocation() : base() { }
        public CompanyLocation(int id) : base(id) { }
        public CompanyLocation(string description) : base(description) { }
        public CompanyLocation(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Company_Location";
            }
        }

        public string Abbreviation
        {
            get { return (string)row["Abbreviation"]; }
            set { this.setRowValue("Abbreviation", value); }
        }

        public int CompanyId
        {
            get { return (int)row["Company_Id"]; }
            set { this.setRowValue("Company_Id", value); }
        }

        public int CompanyLocationId
        {
            get { return (int)row["Company_Location_Id"]; }
            set { this.setRowValue("Company_Location_Id", value); }
        }
        #endregion

        public Company Company
        {
            get { return new Company(this.CompanyId); }
        }
    }
}

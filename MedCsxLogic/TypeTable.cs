﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public abstract class TypeTable : MedCsXTable, IComparable
    {

        private static modLogic ml = new modLogic();

        #region Constructors
        public TypeTable() : base()
        { }

        public TypeTable(int id) : base(id)
        { }

        public TypeTable(string description) : base(description)
        {
            this.Id = ml.getId(this.TableName, description);
        }

        public TypeTable(Hashtable row) : base(row)
        { }

        public TypeTable(DataTable Table, DataRow Row) : base(Table,Row)
        { }
        #endregion

        #region Methods
        private static Hashtable _typeTableRows = new Hashtable();
        private static clsDatabase db = new clsDatabase();
       
        public static List<Hashtable> getTypeTableRows(string tableName, string whereClause = "1 = 1")
        {
            // Gets Type Table rows - for use in the frmPickTypeTablesRow form and MedCsXComboBox

            string sql = "select " + tableName + "_Id as Id, Description, Image_Index from "
                + tableName + " where " + whereClause + " order by ";
            bool hasSortOrder = db.GetBoolFromStoredProcedure("usp_Has_Sort_Order_Column", tableName);

            if (hasSortOrder)
                sql += "Sort_Order";
            else
                sql += "Description";

            if (_typeTableRows.ContainsKey(tableName))
                _typeTableRows[tableName] = db.ExecuteSelectReturnHashList(sql);
            else
                _typeTableRows.Add(tableName, db.ExecuteSelectReturnHashList(sql));

            return (List<Hashtable>)(_typeTableRows[tableName]);
        }

        public static List<Hashtable> getDefaultPrinterTypeTableRows(string tableName, string whereClause = "1 = 1")
        {
            // Gets Type Table rows - for use in the frmPickTypeTablesRow form and MedCsXComboBox

            string sql = "select " + tableName + "_Id as Id, Printer_location as Description, 0 as Image_Index from "
                + tableName + " where " + whereClause + " order by ";
            bool hasSortOrder = db.GetBoolFromStoredProcedure("usp_Has_Sort_Order_Column", tableName);

            if (hasSortOrder)
                sql += "Sort_Order";
            else
                sql += "Description";

            if (_typeTableRows.ContainsKey(tableName))
                _typeTableRows[tableName] = db.ExecuteSelectReturnHashList(sql);
            else
                _typeTableRows.Add(tableName, db.ExecuteSelectReturnHashList(sql));

            return (List<Hashtable>)(_typeTableRows[tableName]);
        }

        public static DataTable getTypeTableRowsDDL(string tableName, string whereClause = "1 = 1")
        {
            DataTable dt = new DataTable();

            string sql = "select " + tableName + "_Id as Id, Description, Image_Index from "
                + tableName + " where " + whereClause + " order by ";
            bool hasSortOrder = db.GetBoolFromStoredProcedure("usp_Has_Sort_Order_Column", tableName);

            if (hasSortOrder)
                sql += "Sort_Order";
            else
                sql += "Description";

            dt = db.ExecuteSelectReturnDataTable(sql);
           

            return dt;
        }

        public static DataTable getDDLTypeTableRows(string tableName, string whereClause = "1 = 1")
        {
            // Gets Type Table rows - for use in the frmPickTypeTablesRow form and MedCsXComboBox
            DataTable dt = new DataTable();
            string sql = "select " + tableName + "_Id as Id, Description, Image_Index from "
                + tableName + " where " + whereClause + " order by ";
            bool hasSortOrder = db.GetBoolFromStoredProcedure("usp_Has_Sort_Order_Column", tableName);

            if (hasSortOrder)
                sql += "Sort_Order";
            else
                sql += "Description";


            dt = db.ExecuteSelectReturnDataTable(sql);
            //if (_typeTableRows.ContainsKey(tableName))
            //    dt = db.ExecuteSelectReturnDataTable(sql);
            //else
            //    _typeTableRows.Add(tableName, db.ExecuteSelectReturnDataTable(sql));

            return dt;
        }

        public static void clearTypeTableCache(string tableName)
        {
            if (_typeTableRows.ContainsKey(tableName))
                _typeTableRows.Remove(tableName);

            MedCsXTable.ClearTableContents(tableName);
        }

        public bool descriptionExists(string desc)
        {
            return ml.getId(TableName, desc) == 0 ? false : true;
        }

        public int maxSortOrder
        {
            get
            {
                string sql = "select max(sort_order) as c from " + this.TableName;
                DataTable rows = db.ExecuteSelect(sql);

                if (rows.Rows.Count == 0)
                    return 0;

                DataRow row = rows.Rows[0];

                return (int)row["c"];
            }
        }

        public string Description
        {
            get { return (string)row["Description"]; }
            set { this.setRowValue("Description", value); }
        }

        public int ImageIndex
        {
            get { return (int)row["Image_Index"]; }
            set { this.setRowValue("Image_Index", value); }
        }

        public string EnumName
        {
            get { return (string)row["Enum_Name"]; }
            set { this.setRowValue("Enum_Name", value); }
        }

        public int sortOrder
        {
            get { return (int)row["Sort_Order"]; }
            set { this.setRowValue("Sort_Order", value); }
        }

        public int GetId(string description)
        {
            return ml.getId(TableName, description);
        }

        public override string TableName
        {
            get { return ""; }
        }
        
        public int CompareTo(object obj)
        {
            TypeTable tt = (TypeTable)obj;
            return this.Description.CompareTo(tt.Description);
        }
        #endregion

    }
}

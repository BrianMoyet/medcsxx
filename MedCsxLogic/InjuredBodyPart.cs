﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class InjuredBodyPart:ClaimTable
    {

        #region Constructors
        public InjuredBodyPart() : base() { }
        public InjuredBodyPart(int id) : base(id) { }
        public InjuredBodyPart(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Injured_Body_Part"; }
        }

        [System.ComponentModel.Description("ID of the injured body part.")]
        public int InjuredBodyPartId
        {
            get { return (int)row["Injured_Body_Part_Id"]; }
            set { this.setRowValue("Injured_Body_Part_Id", value); }
        }

        [System.ComponentModel.Description("Injured person.")]
        public int InjuredId
        {
            get { return (int)row["Injured_Id"]; }
            set { this.setRowValue("Injured_Id", value); }
        }

        [System.ComponentModel.Description("Type of body part.")]
        public int BodyPartId
        {
            get { return (int)row["Body_Part_Id"]; }
            set { this.setRowValue("Body_Part_Id", value); }
        }
        #endregion

        #region Methods
        public Injured InjuredPerson
        {
            get { return new Injured(this.InjuredId); }
        }

        public BodyPart BodyPart
        {
            get { return new BodyPart(this.BodyPartId); }
        }
        #endregion

    }
}

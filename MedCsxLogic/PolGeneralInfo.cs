﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class PolGeneralInfo:PersonalLinesTable
    {
        private static clsDatabase db = new clsDatabase();
        #region Class Variables
        private string m_policyNo;
        private string m_versionNo;
        private ActProducers m_agency;
        #endregion

        #region Constructors
        public PolGeneralInfo(string policyNumber) : base(policyNumber) { }
        public PolGeneralInfo(string policyNumber, string versionNumber)
            : base(policyNumber, versionNumber)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Pol_GeneralInfo", policyNumber, versionNumber);
            if (rows.Rows.Count > 0)
                row = db.DataTableRowToHashtable(rows, rows.Rows[0]);
            else
                row = new Hashtable();
        }
        public PolGeneralInfo(Hashtable row) : base(row) { }
        #endregion

        #region Methods
        public override string TableName
        {
            get { return "PersonalLines.dbo.Pol_GeneralIfo"; }
        }

        public DateTime dtEff
        {
            get { return (DateTime)row["Dt_Eff"]; }
        }

        public DateTime dtExp
        {
            get { return (DateTime)row["Dt_Exp"]; }
        }

        public string NameInsd
        {
            get { return (string)row["Name_Insd"]; }
        }

        public string Address1
        {
            get { return (string)row["Address1"]; }
        }

        public string Address2
        {
            get { return (string)row["Address2"]; }
        }

        public string NameCity
        {
            get { return (string)row["Name_City"]; }
        }

        public string NameState
        {
            get { return (string)row["Name_State"]; }
        }
        
        public string CdZip
        {
            get { return (string)row["Cd_Zip"]; }
        }

        public string NoProducer
        {
            get { return (string)row["No_Producer"]; }
        }

        public ActProducers Agency
        {
            get { return this.m_agency == null ? this.m_agency = new ActProducers(this.NoProducer) : this.m_agency; }
        }

        public string Address
        {
            get
            {
                string s = this.Address1;
                if (this.Address2 != "")
                    s += Environment.NewLine + this.Address2;

                if ((this.NameCity != "") || (this.NameState != "") || (this.CdZip != ""))
                    s += Environment.NewLine + this.NameCity + ", " + this.NameState + " " + this.CdZip;

                return s;
            }
        }
        #endregion
    }
}

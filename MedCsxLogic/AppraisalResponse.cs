﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class AppraisalResponse:MedCsXTable
    {
        private static clsDatabase db = new clsDatabase();

        private static modLogic ml = new modLogic();
        #region Constructors
        public AppraisalResponse() : base() { }
        public AppraisalResponse(int id) : base(id) { }
        public AppraisalResponse(Hashtable row) : base(row) { }
        public AppraisalResponse(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Appraisal_Response"; }
        }

        public string AdjCode
        {
            get { return (string)row["Adj_Code"]; }
            set { this.setRowValue("Adj_Code", value); }
        }

        public int AppraisalResponseId
        {
            get { return (int)row["Appraisal_Response_Id"]; }
            set { this.setRowValue("Appraisal_Response_Id", value); }
        }

        public int AppraisalTaskId
        {
            get { return (int)row["Appraisal_Task_Id"]; }
            set { this.setRowValue("Appraisal_Task_Id", value); }
        }

        public string ClaimNumber
        {
            get { return (string)row["Claim_Number"]; }
            set { this.setRowValue("Claim_Number", value); }
        }

        public DateTime DateTaken
        {
            get { return (DateTime)row["Date_Taken"]; }
            set { this.setRowValue("Date_Taken", value); }
        }

        public string DocumentExtension
        {
            get { return (string)row["Document_Extension"]; }
            set { this.setRowValue("Document_Extension", value); }
        }

        public string DocType
        {
            get { return (string)row["Doc_Type"]; }
            set { this.setRowValue("Doc_Type", value); }
        }

        public string Filename
        {
            get { return (string)row["Filename"]; }
            set { this.setRowValue("Filename", value); }
        }

        public int FolderId
        {
            get { return (int)row["Folder_Id"]; }
            set { this.setRowValue("Folder_Id", value); }
        }

        public int ItemId
        {
            get { return (int)row["Item_Id"]; }
            set { this.setRowValue("Item_Id", value); }
        }

        public string PolicyNumber
        {
            get { return (string)row["Policy_Number"]; }
            set { this.setRowValue("Policy_Number", value); }
        }
 
        public string Remark
        {
            get { return (string)row["Remark"]; }
            set { this.setRowValue("Remark", value); }
        }
        
        public string VIN
        {
            get { return (string)row["VIN"]; }
            set { this.setRowValue("VIN", value); }
        }

        public double InitialEstimate
        {
            get { return (double)((decimal)row["Initial_Estimate"]); }
            set { this.setRowValue("Initial_Estimate", value); }
        }

        public double FinalEstimate
        {
            get { return (double)((decimal)row["Final_Estimate"]); }
            set { this.setRowValue("Final_Estimate", value); }
        }

        public double TotalSupplemental
        {
            get { return (double)((decimal)row["Total_Supplemental"]); }
            set { this.setRowValue("Total_Supplemental", value); }
        }

        public DateTime DateInitialEstimate
        {
            get { return (DateTime)row["Date_Initial_Estimate"]; }
            set { this.setRowValue("Date_Initial_Estimate", value); }
        }
        #endregion

        #region Misc Methods
        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            this.FindMatchingAppraisalTaskId();
            return base.Update(parms, fireEvents);
        }

        public AppraisalTask AppraisalTask
        {
            get { return new AppraisalTask(this.AppraisalTaskId); }
        }

        public string FileNoteText
        {
            get
            {
                string s = "Appraisal Response Received from SceneAccess  Appraisal Document has been Imported Into ImageRight." + Environment.NewLine;
                if (this.VIN != "")
                    s += "Vehicle: " + this.AppraisalTask.VehicleDescription + Environment.NewLine;

                s += "Insured: " + this.AppraisalTask.InsuredDescription + Environment.NewLine;
                s += "Claimant: " + this.AppraisalTask.ClaimantDescription + Environment.NewLine;
                s += "Date Taken: " + this.DateTaken.ToLongDateString() + Environment.NewLine;
                s += "Remarks: " + this.Remark + Environment.NewLine;
                s += "Document Extension: " + this.DocumentExtension + Environment.NewLine;
                s += "Document Type: " + this.DocType + Environment.NewLine;
                s += "Adj. Code: " + this.AdjCode + Environment.NewLine;
                s += "Item Id: " + this.ItemId + Environment.NewLine;
                s += "Folder Id: " + this.FolderId + Environment.NewLine;
                s += "File Name: " + this.Filename + Environment.NewLine;
                s += "Claim Number: " + this.ClaimNumber + Environment.NewLine;
                s += "Policy Number: " + this.PolicyNumber + Environment.NewLine;

                return s;
            }
        }

        public string ThumbnailFileName
        {
            get { return this.Filename + "_Thumb.jpg"; }
        }

        public string DocumentFileName
        {
            get { return this.Filename + "." + this.DocumentExtension; }
        }

        public void FindMatchingAppraisalTaskId()
        {
            //finds matching appraisal task ID
            //TODO  what does this do??
            //try
            //{
            //    if (this.VIN.Trim().Length > 0)
            //        this.AppraisalTaskId = db.GetIntFromStoredProcedure("usp_Get_Appraisal_Task_For_VIN", this.VIN);

                
            //    if (this.AppraisalTaskId == 0)
            //        this.AppraisalTaskId = db.GetIntFromStoredProcedure("usp_Get_Appraisal_Task_For_Claim", modLogic.ExtractClaimId(this.ClaimNumber));
            //}
            //catch (Exception ex)
            //{
            //    System.Windows.MessageBox.Show(ex.ToString(), "MecCsX Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error, System.Windows.MessageBoxResult.OK);
            //}
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MedCsxLogic
{
    public class cboGrid
    {

        public int id { get; set; }
        public string description { get; set; }
        public int imageIdx { get; set; }
        public string type { get; set; }
        public BitmapImage imageIndex
        {
            get { return modLogic.ImageList(imageIdx); }
        }

    }
}

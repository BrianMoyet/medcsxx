﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ClaimAuditCategoryType:TypeTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public ClaimAuditCategoryType() : base() { }
        public ClaimAuditCategoryType(int id) : base(id) { }
        public ClaimAuditCategoryType(Hashtable row) : base(row) { }
        #endregion

        #region Misc Methods
        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            //if an insert, set sort order to be the current highest value in the table + 1
            if (isAdd)
            {
                if (!row.Contains("Sort_Order"))
                    row.Add("Sort_Order", this.maxSortOrder + 1);
            }
            return base.Update(parms, fireEvents);
        }

        public override string TableName
        {
            get
            {
                return "Claim_Audit_Category_Type";
            }
        }

        public List<Hashtable> ItemRows
        {
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Audit_Items_Types", this.Id); }
        }
        #endregion
    }
}

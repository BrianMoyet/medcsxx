﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class DraftVoidReason:TypeTable
    {
        private static modLogic ml=new modLogic();

        #region Constructors
        public DraftVoidReason() : base() { }
        public DraftVoidReason(int id) : base(id) { }
        public DraftVoidReason(string description)
            : base()
        {
            this.Id = ml.getId(this.TableName, description);
        }
        public DraftVoidReason(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Draft_Void_Reason";
            }
        }

        public int DraftVoidReasonId
        {
            get { return (int)row["Draft_Void_Reason_Id"]; }
            set { this.setRowValue("Draft_Void_Reason_Id", value); }
        }
        #endregion
    }
}

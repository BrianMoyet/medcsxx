﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class Diary:MedCsXTable
    {

        #region Constructors
        public Diary() : base() { }
        public Diary(int id) : base(id) { }
        public Diary(DataTable Table, DataRow Row) : base(Table, Row) { }
        public Diary(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public DateTime DateRespondedTo
        {
            get { return (DateTime)row["Date_Responded_To"]; }
            set { this.setRowValue("Date_Responded_To", value); }
        }

        public DateTime DiaryDueDate
        {
            get { return (DateTime)row["Diary_Due_Date"]; }
            set { this.setRowValue("Diary_Due_Date", value); }
        }

        public DateTime DiaryEntryDate
        {
            get { return (DateTime)row["Diary_Entry_Date"]; }
            set { this.setRowValue("Diary_Entry_Date", value); }
        }

        public int DiaryId
        {
            get { return (int)row["Diary_Id"]; }
            set { this.setRowValue("Diary_Id", value); }
        }

        public int FileNoteId
        {
            get { return (int)row["File_Note_Id"]; }
            set { this.setRowValue("File_Note_Id", value); }
        }

        public int NbrDays
        {
            get { return (int)row["Nbr_Days"]; }
            set { this.setRowValue("Nbr_Days", value); }
        }

        public int SentFromUserId
        {
            get { return (int)row["Sent_From_User_Id"]; }
            set { this.setRowValue("Sent_From_User_Id", value); }
        }

        public int SentToUserId
        {
            get { return (int)row["Sent_To_User_Id"]; }
            set { this.setRowValue("Sent_To_User_Id", value); }
        }

        public bool ShowDiary
        {
            get { return (bool)row["Show_Diary"]; }
            set { this.setRowValue("Show_Diary", value); }
        }
        #endregion

        #region Methods
        public override string TableName
        {
            get { return "Diary"; }
        }

        public FileNote FileNote()
        {
            return new FileNote(this.FileNoteId);
        }

        public User SentFromUser()
        {
            return new User(this.SentFromUserId);
        }

        public User SentToUser()
        {
            return new User(this.SentToUserId);
        }

        public int ClaimId()
        {
            this.setRowValue("Claim_Id", this.FileNote().ClaimId);
            return this.FileNote().ClaimId;
        }

        public int ReserveId()
        {
            this.setRowValue("Reserve_Id", this.FileNote().ReserveId);
            return this.FileNote().ReserveId;
        }

        public Claim Claim()
        {
            return new Claim(this.ClaimId());
        }

        public void Clear()
        {
            this.ShowDiary = false;
            this.Update();
        }

        public override void AssignTo(User user)
        {
            if (user == null)
                return;

            this.SentToUserId = user.Id;
            this.Update();
        }

        public override void AssignTo(UserGroup userGroup)
        {
            return; //assign to user group not supported for diaries
        }

        public override int Update(System.Collections.Hashtable parms = null, bool fireEvents = true)
        {
            int claimId = this.ClaimId();
            int reserveId = this.ReserveId();
            return base.Update(parms, fireEvents);
        }
        #endregion
    }
}

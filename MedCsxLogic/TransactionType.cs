﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class TransactionType:TypeTable
    {
        #region Construtors
        public TransactionType() : base() { }
        public TransactionType(int id) : base(id) { }
        public TransactionType(string description) : base(description) { }
        public TransactionType(Hashtable row) : base(row) { }
        #endregion


        public override string TableName
        {
            get
            {
                return "Transaction_Type";
            }
        }

        #region Column Names
        public bool isLoss 
        { 
            get { return (bool)row["Is_Loss"]; }
            set { this.setRowValue("Is_Loss", value); }
        }

        public bool isPaymentType
        {
            get { return (bool)row["Is_Payment_Type"]; }
            set { this.setRowValue("Is_Payment_Type", value); }
        }

        public bool isSupplementalPaymentType
        {
            get { return (bool)row["Is_Supplemental_Payment_Type"]; }
            set { this.setRowValue("Is_Supplemental_Payment_Type", value); }
        }

        public string PersonalLinesType
        {
            get { return (string)row["Personal_Lines_Type"]; }
            set { this.setRowValue("Personal_Lines_Type", value); }
        }

        public int TransactionTypeId
        {
            get { return (int)row["Transaction_Type_Id"]; }
            set { this.setRowValue("Transaction_Type_Id", value); }
        }
        #endregion
    }
}

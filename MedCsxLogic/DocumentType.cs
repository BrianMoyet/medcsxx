﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
   public  class DocumentType:TypeTable
   {
       private static clsDatabase db = new clsDatabase();

       #region Constructors
       public DocumentType() : base() { }
       public DocumentType(int id) : base(id) { }
       public DocumentType(string description) : base(description) { }
       public DocumentType(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get
           {
               return "Document_Type";
           }
       }

       [System.ComponentModel.Description("Type of claim.")]
       public int ClaimTypeId
       {
           get { return (int)row["Claim_Type_Id"]; }
           set { this.setRowValue("Claim_Type_Id", value); }
       }

       public int DocumentLevelId
       {
           get { return (int)row["Document_Level_Id"]; }
           set { this.setRowValue("Document_Level_Id", value); }
       }

       public int DocumentTypeId
       {
           get { return (int)row["Document_Type_Id"]; }
           set { this.setRowValue("Document_Type_Id", value); }
       }

       public int EventTypeId
       {
           get { return (int)row["Event_Type_Id"]; }
           set { this.setRowValue("Event_Type_Id", value); }
       }

       public string FileName
       {
           get { return (string)row["File_Name"]; }
           set { this.setRowValue("File_Name", value); }
       }

        public bool Active
        {
            get { return (bool)row["Active"]; }
            set { this.setRowValue("Active", value); }
        }

        public string Stored_Procedure_Name
       {
           get { return (string)row["Stored_Procedure_Name"]; }
           set { this.setRowValue("Stored_Procedure_Name", value); }
       }
       #endregion

       #region Methods

       #region Class Objects
       public ClaimType ClaimType
       {
           get { return new ClaimType(this.ClaimTypeId); }
       }

       public EventType EventType
       {
           get { return new EventType(this.EventTypeId); }
       }

       public FileNoteLevel DocumentLevel
       {
           get { return new FileNoteLevel(this.DocumentLevelId); }
       }
        #endregion

        public static List<Hashtable> AvailableDocs(int TypeId = -1)
        {
            if (TypeId == -1)
                return db.ExecuteStoredProcedureReturnHashList("usp_Get_Available_Documents");
            else
                return db.ExecuteStoredProcedureReturnHashList("usp_Get_Available_Documents", TypeId);
        }

        public static DataTable AvailableDocuments(int TypeId = -1)
       {
           if (TypeId == -1)
               return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Available_Documents");
           else
               return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Available_Documents", TypeId);
       }

       public static int AvailableDocumentsCount(int TypeId = -1)
       {
           if (TypeId == -1)
               return db.GetIntFromStoredProcedure("usp_Get_Available_Documents_Count");
           else
               return db.GetIntFromStoredProcedure("usp_Get_Available_Documents_Count", TypeId);
       }
       #endregion
   }
}

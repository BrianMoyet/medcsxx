﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ClaimAuditCategory:ClaimTable
    {

        #region Class Variables
        private ClaimAuditCategoryType m_ClaimAuditCategoryType = null;
        #endregion

        #region Constructors
        public ClaimAuditCategory() : base() { }
        public ClaimAuditCategory(int id) : base(id) { }
        public ClaimAuditCategory(Hashtable row) : base(row) { }
        public ClaimAuditCategory(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Claim_Audit_Category"; }
        }

        public int ClaimAuditCategoryTypeId
        {
            get { return (int)row["Claim_Audit_Category_Type_Id"]; }
            set 
            { 
                this.setRowValue("Claim_Audit_Category_Type_Id", value);
                this.m_ClaimAuditCategoryType = null;
            }
        }

        public int Comments
        {
            get { return (int)row["Comments"]; }
            set { this.setRowValue("Comments", value); }
        }
        #endregion

        #region Misc Methods
        public ClaimAuditCategoryType ClaimAuditCategoryType
        {
            get { return this.m_ClaimAuditCategoryType == null ? this.m_ClaimAuditCategoryType = new ClaimAuditCategoryType(this.ClaimAuditCategoryTypeId) : m_ClaimAuditCategoryType; }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BIDiagnosticType:TypeTable
    {

        #region Constructors
        public BIDiagnosticType() : base() { }
        public BIDiagnosticType(int id) : base(id) { }
        public BIDiagnosticType(string description) : base(description) { }
        public BIDiagnosticType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "BI_Diagnostic_Type";
            }
        }

        public int BIDiagnosticTypeId
        {
            get { return (int)row["BI_Diagnostic_Type_Id"]; }
            set { this.setRowValue("BI_Diagnostic_Type_Id", value); }
        }
        #endregion
    }
}

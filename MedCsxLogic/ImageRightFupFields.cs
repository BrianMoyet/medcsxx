﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ImageRightFupFields
    {

        #region Class Variables
        public string Drawer = "";
        public string FileNumber = "";
        public string FileName = "";
        public string UserId_8 = "";
        public string UserId_10 = "";
        public string UserKey1 = "";
        public string UserData1 = "";
        public string UserData2 = "";
        public string UserData3 = "";
        public string UserData4 = "";
        public string UserData5 = "";
        public string NewFileNumber = "";
        public string NewDrawer = "";
        public string Status = "";
        public string StatusDate = "";
        #endregion

        #region Misc Methods
        public string EntireRecord
        {
            get
            {
                //string record = this.Drawer.PadRight(4).Substring(0, 4) + this.FileNumber.PadRight(40).Substring(0, 40) + this.FileName.PadRight(30).Substring(0, 30) +
                //              this.UserId_8.PadRight(8).Substring(0, 8) + this.UserData1.PadRight(10).Substring(0, 10) + this.UserData2.PadRight(20).Substring(0, 20) +
                //              this.UserData3.PadRight(30).Substring(0, 30) + this.UserData4.PadRight(40).Substring(0, 40) + this.UserData5.PadRight(50).Substring(0, 50) +
                //              this.NewFileNumber.PadRight(40).Substring(0, 40) + this.NewDrawer.PadRight(4).Substring(0, 4) + this.Status.PadRight(1).Substring(0, 1) +
                //              this.StatusDate.PadRight(8).Substring(0, 8) + this.UserId_10.PadRight(335).Substring(0, 335);
                 string record = (this.Drawer.PadRight(4).Substring(0, 4) + this.FileNumber.PadRight(40).Substring(0, 40) + this.FileName.PadRight(30).Substring(0, 30) + this.UserId_8.PadRight(8).Substring(0, 8) + this.UserKey1.PadRight(40).Substring(0, 40) + this.UserData1.PadRight(10).Substring(0, 10)
                                + this.UserData2.PadRight(20).Substring(0, 20) + this.UserData3.PadRight(30).Substring(0, 30) + this.UserData4.PadRight(40).Substring(0, 40) + this.UserData5.PadRight(50).Substring(0, 50) + this.NewFileNumber.PadRight(40).Substring(0, 40) + this.NewDrawer.PadRight(4).Substring(0, 4)
                                + this.Status.PadRight(1).Substring(0, 1) + this.StatusDate.PadRight(8).Substring(0, 8) + this.UserId_10).PadRight(335).Substring(0, 335);

                return record;
            }
        }

        public string DeleteRecord
        {
            get
            {
                string record = (this.Drawer.PadRight(4).Substring(0, 4) + this.FileNumber.PadRight(40).Substring(0, 40) + "Y").PadRight(94).Substring(0, 94);
                return record;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public sealed class Person : ClaimTable
    {

        #region Class Variables
        private PersonType m_PersonType = null;
        private State m_State = null;
        private State m_OtherInsState = null;
        #endregion

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public Person() : base() { }
        public Person(int id) : base(id) { }
        public Person(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get { return "Person"; }
        }

        #region Column Names
        public string PersonSource
        {
            get
            {
                if (row["PersonSource"] == null)
                    row["PersonSource"] = "";
                return row["PersonSource"].ToString();
            }
            set { this.setRowValue("PersonSource", value); }
        }

        public string Address1
        {
            get
            {
                if (row["Address1"] == null)
                    row["Address1"] = "";
                return row["Address1"].ToString();
            }
            set { this.setRowValue("Address1", value); }
        }

        public string Address2
        {
            get
            {
                if (row["Address2"] == null)
                    row["Address2"] = "";
                return row["Address2"].ToString();
            }
            set { this.setRowValue("Address2", value); }
        }

        public string CellPhone
        {
            get
            {
                if (row["Cell_Phone"] == null)
                    row["Cell_Phone"] = "";
                return row["Cell_Phone"].ToString();
            }
            set { this.setRowValue("Cell_Phone", value); }
        }

        public string City
        {
            get
            {
                if (row["City"] == null)
                    row["City"] = "";
                return row["City"].ToString();
            }
            set { this.setRowValue("City", value); }
        }

        public DateTime DateOfBirth
        {
            get
            {
                if (row["Date_Of_Birth"] == null)
                    row["Date_Of_Birth"] = ml.DEFAULT_DATE;
                return (DateTime)row["Date_Of_Birth"];
            }
            set { this.setRowValue("Date_Of_Birth", value); }
        }

        public string DriversLicenseNo
        {
            get
            {
                if (row["Drivers_License_No"] == null)
                    row["Drivers_License_No"] = "";
                return row["Drivers_License_No"].ToString();
            }
            set { this.setRowValue("Drivers_License_No", value); }
        }

        public string DriversLicenseState
        {
            get
            {
                return row["Drivers_License_State"] == null ? "" : row["Drivers_License_State"].ToString();
            }
            set { this.setRowValue("Drivers_License_State", value); }
        }

        public string DriversLicenseClass
        {
            get
            {
                return row["Drivers_License_Class"] == null ? "" : row["Drivers_License_Class"].ToString();
            }
            set { this.setRowValue("Drivers_License_Class", value); }
        }

        public string Email
        {
            get
            {
                if (row["Email"] == null)
                    row["Email"] = "";
                return row["Email"].ToString();
            }
            set { this.setRowValue("Email", value); }
        }

        public string Employer
        {
            get
            {
                if (row["Employer"] == null)
                    row["Employer"] = "";
                return row["Employer"].ToString();
            }
            set { this.setRowValue("Employer", value); }
        }

        public string Fax
        {
            get
            {
                if (row["Fax"] == null)
                    row["Fax"] = "";
                return row["Fax"].ToString();
            }
            set { this.setRowValue("Fax", value); }
        }

        public string FirstName
        {
            get { return (string)row["First_Name"]; }
            set { this.setRowValue("First_Name", value); }
        }

        public string HomePhone
        {
            get
            {
                if (row["Home_Phone"] == null)
                    row["Home_Phone"] = "";
                return row["Home_Phone"].ToString();
            }
            set { this.setRowValue("Home_Phone", value); }
        }

        [System.ComponentModel.Description("This is a company.")]
        public bool isCompany
        {
           
            get { return (bool)row["Is_Company"]; }
            set { this.setRowValue("Is_Company", value); }
           
            
        }

        public bool IsFraudulent
        {
            get { return (bool)row["Flag_Fraud"]; }
            set { this.setRowValue("Flag_Fraud", value); }
        }

        public int LanguageId
        {
            get { return (int)row["Language_Id"]; }
            set { this.setRowValue("Language_Id", value); }
        }

        public string LastName
        {
            get
            {
                if (row["Last_Name"] == null)
                    row["Last_Name"] = "";
                return row["Last_Name"].ToString();
            }
            set { this.setRowValue("Last_Name", value); }
        }

        public string MiddleInitial
        {
            get
            {
                if (row["Middle_Intitial"] == null)
                    row["Middle_Intitial"] = "";
                return row["Middle_Intitial"].ToString();
            }
            set { this.setRowValue("Middle_Intitial", value); }
        }

        public string OtherContactPhone
        {
            get
            {
                if (row["Other_Contact_Phone"] == null)
                    row["Other_Contact_Phone"] = "";
                return row["Other_Contact_Phone"].ToString();
            }
            set { this.setRowValue("Other_Contact_Phone", value); }
        }

        public int PersonTypeId
        {
            get { return (int)row["Person_Type_Id"]; }
            set { this.setRowValue("Person_Type_Id", value); }
        }

        public int PolicyIndividualId
        {
            get { return (int)row["Policy_Individual_Id"]; }
            set { this.setRowValue("Policy_Individual_Id", value); }
        }

        public string Salutation
        {
            get
            {
                if (row["Salutation"] == null)
                    row["Salutation"] = "";
                return row["Salutation"].ToString();
            }
            set { this.setRowValue("Salutation", value); }
        }

        public string Sex
        {
            get
            {
                if (row["Sex"] == null)
                    row["Sex"] = "";
                return row["Sex"].ToString();
            }
            set { this.setRowValue("Sex", value); }
        }

        public string SSN
        {
            get
            {
                if (row["SSN"] == null)
                    row["SSN"] = "";
                return row["SSN"].ToString();
            }
            set { this.setRowValue("SSN", value); }
        }

        public string Suffix
        {
            get
            {
                if (row["Suffix"] == null)
                    row["Suffix"] = "";
                return row["Suffix"].ToString();
            }
            set { this.setRowValue("Suffix", value); }
        }

        public string WorkPhone
        {
            get
            {
                if (row["Work_Phone"] == null)
                    row["Work_Phone"] = "";
                return row["Work_Phone"].ToString();
            }
            set { this.setRowValue("Work_Phone", value); }
        }

        public string Zipcode
        {
            get
            {
                if (row["Zipcode"] == null)
                    row["Zipcode"] = "";
                return row["Zipcode"].ToString();
            }
            set { this.setRowValue("Zipcode", value); }
        }

        public string Comments
        {
            get { return (string)row["Comments"]; }
            set { this.setRowValue("Comments", value); }
        }

        public string OtherInsuranceCompany
        {
            get
            {
                if (row["Other_Insurance_Company"] == null)
                    row["Other_Insurance_Company"] = "";
                return (string)row["Other_Insurance_Company"];
            }
            set { this.setRowValue("Other_Insurance_Company", value); }
        }
        
        public string OtherInsurancePolicyNo
        {
            get
            {
                if (row["Other_Insurance_Policy_No"] == null)
                    row["Other_Insurance_Policy_No"] = "";
                return (string)row["Other_Insurance_Policy_No"];
            }
            set { this.setRowValue("Other_Insurance_Policy_No", value); }
        }

        public string OtherInsuranceAgentName
        {
            get
            {
                if (row["Other_Insurance_Agent_Name"] == null)
                    row["Other_Insurance_Agent_Name"] = "";
                return (string)row["Other_Insurance_Agent_Name"];
            }
            set { this.setRowValue("Other_Insurance_Agent_Name", value); }
        }

        public string OtherInsuranceAddress
        {
            get
            {
<<<<<<< HEAD
                if (row["Other_Insurance_Agent_Address"] == null)
                    row["Other_Insurance_Agent_Address"] = "";
                return (string)row["Other_Insurance_Agent_Address"];
            }
            set { this.setRowValue("Other_Insurance_Agent_Address", value); }
=======
                if (row["Other_Insurance_Address"] == null)
                    row["Other_Insurance_Address"] = "";
                return (string)row["Other_Insurance_Address"];
            }
            set { this.setRowValue("Other_Insurance_Address", value); }
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        public string OtherInsuranceCity
        {
            get
            {
<<<<<<< HEAD
                if (row["Other_Insurance_Agent_City"] == null)
                    row["Other_Insurance_Agent_City"] = "";
                return (string)row["Other_Insurance_Agent_City"];
            }
            set { this.setRowValue("Other_Insurance_Agent_City", value); }
=======
                if (row["Other_Insurance_City"] == null)
                    row["Other_Insurance_City"] = "";
                return (string)row["Other_Insurance_City"];
            }
            set { this.setRowValue("Other_Insurance_City", value); }
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        public string OtherInsuranceZipCode
        {
            get
            {
<<<<<<< HEAD
                if (row["Other_Insurance_Agent_ZipCode"] == null)
                    row["Other_Insurance_Agent_ZipCode"] = "";
                return (string)row["Other_Insurance_Agent_ZipCode"];
            }
            set { this.setRowValue("Other_Insurance_Agent_ZipCode", value); }
=======
                if (row["Other_Insurance_ZipCode"] == null)
                    row["Other_Insurance_ZipCode"] = "";
                return (string)row["Other_Insurance_ZipCode"];
            }
            set { this.setRowValue("Other_Insurance_ZipCode", value); }
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        public string OtherInsuranceAgentPhone
        {
            get
            {
                if (row["Other_Insurance_Agent_Phone"] == null)
                    row["Other_Insurance_Agent_Phone"] = "";
                return (string)row["Other_Insurance_Agent_Phone"];
            }
            set { this.setRowValue("Other_Insurance_Agent_Phone", value); }
        }

        public string OtherInsuranceAgentFax
        {
            get
            {
                if (row["Other_Insurance_Agent_Fax"] == null)
                    row["Other_Insurance_Agent_Fax"] = "";
                return (string)row["Other_Insurance_Agent_Fax"];
            }
            set { this.setRowValue("Other_Insurance_Agent_Fax", value); }
        }

        public string OtherInsuranceAgentEmail
        {
            get
            {
                if (row["Other_Insurance_Agent_Email"] == null)
                    row["Other_Insurance_Agent_Email"] = "";
                return (string)row["Other_Insurance_Agent_Email"];
            }
            set { this.setRowValue("Other_Insurance_Agent_Email", value); }
        }

        public string OtherInsuranceClaimNo
        {
            get
            {
                if (row["Other_Insurance_Claim_No"] == null)
                    row["Other_Insurance_Claim_No"] = "";
                return (string)row["Other_Insurance_Claim_No"];
            }
            set { this.setRowValue("Other_Insurance_Claim_No", value); }
        }

        public bool hasOtherInsurance
        {
            get
            {
                return (bool)row["Has_Other_Insurance"];
            }
            set { this.setRowValue("Has_Other_Insurance", value); }
        }
        #endregion

        #region Modified Column Names
        public int StateId
        {
            get { return (int)row["State_Id"]; }
            set 
            { 
                this.setRowValue("State_Id", value);
                m_State = null;
            }
        }

        //TODO
<<<<<<< HEAD
        public int OtherInsuranceStateId
        {
            get
            {
                if (row["Other_Insurance_Agent_State_Id"] == null)
                    row["Other_Insurance_Agent_State_Id"] =  0;
                return (int)row["Other_Insurance_Agent_State_Id"];
            }
            set { this.setRowValue("Other_Insurance_Agent_State_Id", value); }
        }
=======
        public int OtherInsuranceStateId { get; set; }
        //{
            
        //    get { return row["Other_Insurance_State_Id"].Equals(DBNull.Value) ? 0 : Convert.ToInt32(row["Other_Insurance_State_Id"]); }

        //    set
        //    {
        //        this.setRowValue("Other_Insurance_State_Id", value);
        //        m_OtherInsState  = null;
        //    }
        //}
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        #endregion

        #region Misc Methods
        public new int Update()
        {
            MedCsXSystem mySystem = new MedCsXSystem();
            // Updates the person object in db

            //Perform Insert/Update
            if (isAdd) //Insert
<<<<<<< HEAD
                this.Id = db.GetIntFromStoredProcedure("usp_Insert_Person", this.ClaimId, this.PersonTypeId, this.PolicyIndividualId,
                          this.Salutation, this.LastName, this.MiddleInitial, this.FirstName, this.Suffix, this.Address1,
                          this.Address2, this.City, this.StateId, this.Zipcode, this.HomePhone, this.WorkPhone, this.CellPhone,
                          this.OtherContactPhone, this.Fax, this.Email, this.DateOfBirth, this.SSN, this.DriversLicenseNo, this.Sex,
                          this.Employer, this.LanguageId, this.isCompany, Convert.ToInt32(HttpContext.Current.Session["userID"]), this.Comments, this.IsFraudulent, this.hasOtherInsurance, this.OtherInsuranceCompany,
                          this.OtherInsurancePolicyNo, this.OtherInsuranceClaimNo, this.OtherInsuranceAgentName, this.OtherInsuranceAddress,
                                                 this.OtherInsuranceCity, this.OtherInsuranceStateId, this.OtherInsuranceZipCode, this.OtherInsuranceAgentPhone, this.OtherInsuranceAgentFax,
                                                 this.OtherInsuranceAgentEmail);
=======
                this.Id = db.GetIntFromStoredProcedure("usp_Insert_Person", this.ClaimId, this.PersonTypeId, this.PolicyIndividualId, 
                          this.Salutation, this.LastName, this.MiddleInitial, this.FirstName, this.Suffix, this.Address1, 
                          this.Address2, this.City, this.StateId, this.Zipcode, this.HomePhone, this.WorkPhone, this.CellPhone, 
                          this.OtherContactPhone, this.Fax, this.Email, this.DateOfBirth, this.SSN, this.DriversLicenseNo, this.Sex, 
                          this.Employer, this.LanguageId, this.isCompany, Convert.ToInt32(HttpContext.Current.Session["userID"]), this.Comments, this.DriversLicenseState, this.DriversLicenseClass);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            else //Update
                db.ExecuteStoredProcedureNoReturnValue("usp_Update_Person", this.Id, this.ClaimId, this.PersonTypeId, this.PolicyIndividualId,
                          this.Salutation, this.LastName, this.MiddleInitial, this.FirstName, this.Suffix, this.Address1,
                          this.Address2, this.City, this.StateId, this.Zipcode, this.HomePhone, this.WorkPhone, this.CellPhone,
                          this.OtherContactPhone, this.Fax, this.Email, this.DateOfBirth, this.SSN, this.DriversLicenseNo, this.Sex,
<<<<<<< HEAD
                          this.Employer, this.LanguageId, this.isCompany, Convert.ToInt32(HttpContext.Current.Session["userID"]), this.Comments, this.IsFraudulent, this.hasOtherInsurance, this.OtherInsuranceCompany,
                          this.OtherInsurancePolicyNo, this.OtherInsuranceClaimNo, this.OtherInsuranceAgentName, this.OtherInsuranceAddress,
                                                 this.OtherInsuranceCity, this.OtherInsuranceStateId, this.OtherInsuranceZipCode, this.OtherInsuranceAgentPhone, this.OtherInsuranceAgentFax,
                                                 this.OtherInsuranceAgentEmail);
            //if (isAdd) //Insert
            //    this.Id = db.GetIntFromStoredProcedure("usp_Insert_Person", this.ClaimId, this.PersonTypeId, this.PolicyIndividualId, 
            //              this.Salutation, this.LastName, this.MiddleInitial, this.FirstName, this.Suffix, this.Address1, 
            //              this.Address2, this.City, this.StateId, this.Zipcode, this.HomePhone, this.WorkPhone, this.CellPhone, 
            //              this.OtherContactPhone, this.Fax, this.Email, this.DateOfBirth, this.SSN, this.DriversLicenseNo, this.Sex, 
            //              this.Employer, this.LanguageId, this.isCompany, Convert.ToInt32(HttpContext.Current.Session["userID"]), this.Comments, this.DriversLicenseState, this.DriversLicenseClass);
            //else //Update
            //    db.ExecuteStoredProcedureNoReturnValue("usp_Update_Person", this.Id, this.ClaimId, this.PersonTypeId, this.PolicyIndividualId,
            //              this.Salutation, this.LastName, this.MiddleInitial, this.FirstName, this.Suffix, this.Address1,
            //              this.Address2, this.City, this.StateId, this.Zipcode, this.HomePhone, this.WorkPhone, this.CellPhone,
            //              this.OtherContactPhone, this.Fax, this.Email, this.DateOfBirth, this.SSN, this.DriversLicenseNo, this.Sex,
            //              this.Employer, this.LanguageId, this.isCompany, mySystem.UserId, this.Comments, this.DriversLicenseState, this.DriversLicenseClass);
=======
                          this.Employer, this.LanguageId, this.isCompany, mySystem.UserId, this.Comments, this.DriversLicenseState, this.DriversLicenseClass);
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

            return this.Id; //returns person Id
        }

        public static string PersonName(int personId)
        {
            return db.GetStringFromStoredProcedure("usp_Get_Person_Name_For_Id", personId); 
        }

        public State State
        {
            get { return m_State == null ? m_State = new State(this.StateId) : m_State; }
        }

        public static Person Named(string name, int claimId)
        {
            return new Person(db.GetIntFromStoredProcedure("usp_Get_Person_Id_For_Name", name, claimId));
        }

        public static void SetToNamedInsured(int personId)
        {
            Person p = new Person(personId);
            p.SetToNamedInsured();
        }

        public string Name
        {
            //Returns the person's name
            get
            {
                string myName = this.Salutation + " " + this.FirstName + " " + this.MiddleInitial + " ";
                if (this.LastName != "_")
                    myName += this.LastName;
                myName += " " + this.Suffix;

                //Removes excess spaces
                myName = myName.Trim().Replace("  ", " ").Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");

                return myName;
            }
        }

        public string FormalName
        {
            //Returns the formal name of the person
            get
            {
                if (this.isCompany)
                    return this.FirstName;
                return this.LastName + ", " + this.FirstName;
            }
        }

        public string Type() 
        {
            /*More encompassing than the description field from the Person_Type table
             * Gets MedCsX person types, not just PersonalLines Person Types.
            */

            return db.GetStringFromStoredProcedure("usp_Get_Person_Type_Description", this.Id);
        }

        public PersonType PersonType
        {
            get { return m_PersonType == null ? m_PersonType = new PersonType(PersonTypeId) : m_PersonType; }
        }

        public void SetToNamedInsured()
        {
            // Sets PersonTypeId to Named Insured
            db.ExecuteStoredProcedure("usp_Update_Person_Type", this.Id, (int)modGeneratedEnums.PersonType.Named_Insured);
        }

        public void UpdateUnknownPersons()
        {
            //Adds additional unknow person if necessary

            string[] a;
            int result;

            if (this.Id == 0)
                return;

            a = this.FirstName.Split(' ');
            if (a[0].ToUpper()=="UNKNOWN")
            {
                if (a.Length-1>0)  //is there more than 1?
                {
                    if (int.TryParse(a[1], out result))
                        this.Claim.InsertUnknownPersonIfNecessary(int.Parse(a[1])+1);
                }
                else
                    this.Claim.InsertUnknownPersonIfNecessary(2); //personId is "Unknown" - make sure "Unknown 2" exists
            }
        }

        public WitnessPassenger WitnessPassenger()
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Witness_Passenger", this.Id);

            if (rows.Rows.Count == 0)
                return null;
            return new WitnessPassenger(rows, rows.Rows[0]);
        }

        public string Description
        {
            get
            {
                string s = this.Name;

                if (this.Address1 != "")
                    s += Environment.NewLine + this.Address1;

                if (this.Address2 != "")
                    s += Environment.NewLine + this.Address2;

                if (this.City != "" || this.State.Abbreviation != "" || this.Zipcode != "")
                    s += Environment.NewLine + this.City + ", " + this.State.Abbreviation + " " + this.Zipcode;

                return s;
            }
        }

        public string ExtendedDescription()
        {
            string s = this.Name;

            if (s == "" || s == "Parked and Unoccupied")
                return s;

            if (s.Split(' ')[0] == "Unknown")
                return s;

            WitnessPassenger wp = this.WitnessPassenger();

            if (wp != null)
                s += Environment.NewLine + wp.WitnessPassengerType().Description;

            if (this.Address1 != "")
                s += Environment.NewLine + this.Address1;

            if (this.Address2 != "")
                s += Environment.NewLine + this.Address2;

            if (this.City != "" || this.State.Abbreviation != "" || this.Zipcode != "")
                s += Environment.NewLine + this.City + ", " + this.State.Abbreviation + " " + this.Zipcode;

            if ((this.HomePhone != "") && (ml.CountDigits(this.HomePhone) > 0))
                s += Environment.NewLine + "Home Phone: " + this.HomePhone;

            if ((this.WorkPhone != "") && (ml.CountDigits(this.WorkPhone) > 0))
                s += Environment.NewLine + "Work Phone: " + this.WorkPhone;

            if ((this.CellPhone != "") && (ml.CountDigits(this.CellPhone) > 0))
                s += Environment.NewLine + "Cell Phone: " + this.CellPhone;

            if ((this.OtherContactPhone != "") && (ml.CountDigits(this.OtherContactPhone) > 0))
                s += Environment.NewLine + "Other Contact Phone: " + this.OtherContactPhone;

            if ((this.Fax != "") && (ml.CountDigits(this.Fax) > 0))
                s += Environment.NewLine + "Fax: " + this.Fax;

            if (this.Email != "")
                s += Environment.NewLine + "Email: " + this.Email;

            if (this.DateOfBirth != ml.DEFAULT_DATE)
                s += Environment.NewLine + "Date of Birth: " + this.DateOfBirth.ToShortDateString();

            if (this.SSN != "")
                s += Environment.NewLine + "SSN: " + this.SSN;

            if (this.DriversLicenseNo != "")
                s += Environment.NewLine + "Drivers License No: " + this.DriversLicenseNo;

            if (this.Sex != "")
                s += Environment.NewLine + "Sex: " + this.Sex;

            if (this.Employer != "")
                s += Environment.NewLine + "Employer: " + this.Employer;

            if (this.LanguageId != (int)modGeneratedEnums.Language.English )
                s += Environment.NewLine + "Language: " + new Language(this.LanguageId).Description;

            return s;
        }

        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_Person_Row";
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class LossLocation:TypeTable
    {

        #region Constructors
        public LossLocation() : base() { }
        public LossLocation(int id) : base(id) { }
        public LossLocation(string description) : base(description) { }
        public LossLocation(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Loss_Location";
            }
        }
    }
}

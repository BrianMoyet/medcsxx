﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Configuration;

using MedCsxDatabase;
using System.Web;

namespace MedCsxLogic
{

    public class modLogic
    {

        public readonly DateTime DEFAULT_DATE = new DateTime(1900, 1, 1); //Default date for date fields.  Nulls aren't allowed in DB.

        //Script function class to be used when executing file note scripts
        public ScriptFunctions myScriptFunctions = new ScriptFunctions();

        #region Class Variables
        private clsDatabase m_db;
        private TimeSpan _timeOffset;
        private Hashtable descriptions = new Hashtable();
        private Hashtable _dataDictionaryTables = new Hashtable();
        private Hashtable _changeEventsForTable = new Hashtable();
        private Dictionary<string, int> _tableUpdateEvents = new Dictionary<string, int>();
        #endregion

        public modLogic() 
        {
            Initialize();
        }

        #region Misc Methods
        public clsDatabase db
        {
            get { return m_db; }
            set { m_db = value; }
        }

       
        public void Initialize()
        {
            //Connect to the data source
            try
            {
                db = CreateNewDataConnection();
            }
            catch (System.Exception ex)
            {
                throw ex;
                //System.Windows.MessageBox.Show("Problem Connecting to Database - " + ex.ToString());
            }
        }

        public clsDatabase CreateNewDataConnection()
        {
            //Returns a new db connection
            //string connection = "";
            //connection = System.Configuration.ConfigurationManager.ConnectionStrings["MedCsxDatabase.Properties.Settings.QAClaimsConnectionString"].ConnectionString;
            return new clsDatabase();
        }

        public User CurrentUser
        {
            get {
                MedCsXSystem mySystem = new MedCsXSystem();
                return mySystem.CurrentUser; }
        }

        public int CountDigits(string s)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (isNumeric(s.Substring(i, 1)))
                    count++;
            }
            return count;
        }

        public static Int32 ExtractClaimId(string s)
        {
            //Extracts claim ID from full claim number, i.e. KAL123456
            string s2 = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (isNumeric(s.Substring(i, 1)))
                    s2 += s.Substring(i, 1);
            }
            if (s2 == "")
                s2 = "0";
            int maxClaimIdLength=System.Configuration.ConfigurationManager.AppSettings["ClaimIdMax"].Length;
            if (s2.Length > maxClaimIdLength)
                s2 = s2.Substring(s2.Length - maxClaimIdLength, maxClaimIdLength);
            return Convert.ToInt32(s2);
        }

        public string FormatCoverageAmount(double amount)
        {
            //Format coverage amount: 15000.00 becomes 15k, 500.00 becomes 500, etc.
            string s;
            if (amount < 1000)
                s = Math.Truncate(amount).ToString();
            else
                s = Math.Truncate(amount / 1000).ToString() + "k";
            return s;
        }

        public string PolicySystemName()
        {
            return "Personal Lines";
        }

        public void setTimeOffset()
        {
            /* Set time offset - the difference between the DB time that the local time on the PC.
             * Getting the date from the server is time consuming.
            */

            _timeOffset = db.CurrentDate().Subtract(DateTime.Now);
        }

        public DateTime DBNow(bool estimated = true)
        {
            try
            {
                if (estimated)
                    return DateTime.Now.Add(_timeOffset); //db.CurrentDate()
                else
                    return db.CurrentDate();
            }
            catch
            {
                return DateTime.Now;
            }
        }

        public object firstItem(List<Hashtable> c)
        {
            //returns first item in a collection.  Null if empty.
            foreach (object v in c)
                return v;
            return null;
        }

        public string getDescriptions(string table, int id)
        {
            //returns description from TypeTable row - used by the FileNote scripts
            Hashtable tableDict = null;
            string sql = "";

            if (!descriptions.Contains(table))
                descriptions.Add(table, new Hashtable());

            tableDict = (Hashtable)descriptions[table];
            if (!tableDict.Contains(id))
            {
                sql = "select description from [" + table + "] where " + table + "_Id = " + id;
                tableDict.Add(id, db.GetString(sql));
            }
            return (string)tableDict[id];
        }

        public int InsertPerson(Hashtable PolicyHolder, int claimId)
        {
            //create insured person
            Person ip = new Person();
            ip.ClaimId = claimId;
            ip.FirstName = (string)PolicyHolder["Name"];
            ip.LastName = "";
            ip.PersonTypeId = (int)modGeneratedEnums.PersonType.Named_Insured;
            ip.Address1 = (string)PolicyHolder["Address1"];
            ip.Address2 = (string)PolicyHolder["Address2"];
            ip.City = (string)PolicyHolder["City"];
            ip.StateId = State.idForAbbr((string)PolicyHolder["State"]);
            ip.Zipcode = (string)PolicyHolder["Zip"];
            ip.isCompany = true;
            ip.Update();

            //set the insured person for this claim
            return ip.Id;
        }

        public static BitmapImage ImageList(int image_index)
        {
            switch (image_index)
            {
                case 1:
                case 2:
                    return new BitmapImage(new Uri("../../Forms/Images/mnuEditUser.png", UriKind.Relative));
                case 4:
                    return new BitmapImage(new Uri("../../Forms/Images/mnuMoney.png", UriKind.Relative));
                case 5:
                    return new BitmapImage(new Uri("../../Forms/Images/icoMoney.png", UriKind.Relative));
                case 28:
                    return new BitmapImage(new Uri("../../Forms/Images/icoHandicap.png", UriKind.Relative));
                case 30:
                    return new BitmapImage(new Uri("../../Forms/Images/icoMan.png", UriKind.Relative));
                case 33:
                    return new BitmapImage(new Uri("../../Forms/Images/icoRedBook.png", UriKind.Relative));
                case 42:
                    return new BitmapImage(new Uri("../../Forms/Images/icoOrangeCone.png", UriKind.Relative));
                case 40:
                    return new BitmapImage(new Uri("../../Forms/Images/icoPaperClipped.png", UriKind.Relative));
                case 44:
                    return new BitmapImage(new Uri("../../Forms/Images/icoDiary.png", UriKind.Relative));
                case 48:
                    return new BitmapImage(new Uri("../../Forms/Images/mnuFileNew.png", UriKind.Relative));
                case 64:
                    return new BitmapImage(new Uri("../../Forms/Images/flagEngland.png", UriKind.Relative));
                case 65:
                    return new BitmapImage(new Uri("../../Forms/Images/flagSpain.png", UriKind.Relative));
                case 67:
                    return new BitmapImage(new Uri("../../Forms/Images/icoPhoneGreen.png", UriKind.Relative));
                case 72:
                    return new BitmapImage(new Uri("../../Forms/Images/icoDilbertBoss.jpg", UriKind.Relative));
                case 75:
                    return new BitmapImage(new Uri("../../Forms/Images/tbRedCar.png", UriKind.Relative));
                case 79:
                    return new BitmapImage(new Uri("../../Forms/Images/icoCheckMarkBW.png", UriKind.Relative));
                case 91:
                    return new BitmapImage(new Uri("../../Forms/Images/icoGavelBW.png", UriKind.Relative));
                case 97:
                    return new BitmapImage(new Uri("../../Forms/Images/flagFrance.png", UriKind.Relative));
                case 98:
                    return new BitmapImage(new Uri("../../Forms/Images/flagGermany.png", UriKind.Relative));
                case 100:
                    return new BitmapImage(new Uri("../../Forms/Images/flagItaly.png", UriKind.Relative));
                case 101:
                    return new BitmapImage(new Uri("../../Forms/Images/flagJapan.png", UriKind.Relative));
                case 105:
                    return new BitmapImage(new Uri("../../Forms/Images/flagSwitzerland.png", UriKind.Relative));
                default:
                    return new BitmapImage(new Uri("../../Forms/Images/tbOpenClaim.png", UriKind.Relative));
            }
        }
        #endregion

        #region Find Policies Methods
        public List<Hashtable> FindPolicies(bool findActivePolicies, string dateOfLoss, string policyNumber
            , string firstName, string lastName, string SSN, string autoYear, string make, string model
            , string VIN, string address, string city, string state, string zip, ArrayList excludedPolicies = null)
        {
            /*Find policies which match entered criteria.  If findActivePolicies = true, return active policies
             * for the date of loss entered.  If findactivePolicies = false, return recently expired policies
             * (last 60 days) for the date of loss.  excludedPolicies contains policy numbers of active policies
             * to exclude from the recently expired list - we don't want the recently expired list to also 
             * contain active policies.
            */
            try
            {
                //use a new data connection - this will prevent the main data connection from being killed if the query is cancelled
                clsDatabase dbConnection = CreateNewDataConnection();
                List<Hashtable> Policies = dbConnection.ExecuteStoredProcedureReturnHashList("usp_FindPolicies", findActivePolicies
                    , dateOfLoss, policyNumber, firstName, lastName, SSN, autoYear, make, model, VIN, address, city, state, zip);

                if (excludedPolicies != null)
                {
                    if ((excludedPolicies.Count == 0) && (Policies.Count == 0))
                    {
                        DateTime dtOfLoss = DateTime.Parse(dateOfLoss);
                        DateTime newDOL = dtOfLoss.AddDays(7);
                        Policies = dbConnection.ExecuteStoredProcedureReturnHashList("usp_FindPolicies", true,
                                 dateOfLoss, policyNumber, firstName, lastName, SSN, autoYear, make, model, VIN, address, city, state, zip);
                    }
                }
                dbConnection = null;

                //return rows
                return Policies;
            }
            catch (Exception ex)
            {
                //ran into an error - return an empty list
                return new List<Hashtable>();
            }
        }
        #endregion
       
        #region Data Dictionary Functions
        public void GenerateTableInsertEvent(string tableName, int id)
        {
            //generates table and inserts event - if an insert event is registered for the table in the data dictionary
            Hashtable d = getDataDictionaryTable(tableName);

            //if no insert event is defined - exit
            if (d == null)
                return;

            if (!d.Contains("Insert_Event_Type_Id"))
                return;

            if ((int)d["Insert_Event_Type_Id"] == 0)
                return;

            //fire table insert event
            ScriptParms sp = new ScriptParms();
            sp.UpdateParm("New_Value", id);

            Hashtable row = db.GetRow(tableName, id);
            sp.GenerateParms(tableName, row);
            
            EventType et = new EventType((int)d["Insert_Event_Type_Id"]);
            et.FireEvent(sp.Parms);
        }

        //public void GenerateTableInsertEventAppraisalTask(string tableName, int id)
        //{
        //    tableName = "Appraisal_Task";
        //    //generates table and inserts event - if an insert event is registered for the table in the data dictionary
        //    Hashtable d = getDataDictionaryTable(tableName);

        //    //if no insert event is defined - exit
        //    if (d == null)
        //        return;

        //    if (!d.Contains("Insert_Event_Type_Id"))
        //        return;

        //    if ((int)d["Insert_Event_Type_Id"] == 0)
        //        return;

        //    //fire table insert event
        //    ScriptParms sp = new ScriptParms();
        //    sp.UpdateParm("New_Value", id);
           

        //    Hashtable row = db.GetRow(tableName, id);
        //    sp.GenerateParms(tableName, row);

        //    EventType et = new EventType((int)d["Insert_Event_Type_Id"]);
        //    et.FireEvent(sp.Parms);
        //}

        public void FireTableUpdateEvent(string tableName, Hashtable oldRow, Hashtable newRow, Hashtable parms)
        {
            /*The data dictionary is used to find which event type is related to a table.
             * oldRow and parms are passed as variants by they are really dictionaries.
             * This is necessary because a calling function has them defined as variants
             * because they are optional parameters and have to be defined as variants
             * so isMissing() can be used against them.
            */

            //if update event is not registered for the table - exit
            int updateEventId = getTableUpdateEvent(tableName);
            if (updateEventId == 0)
                return;

            //set the event parms
            ScriptParms sp = new ScriptParms();
            sp.GenerateParms(tableName, newRow);
            sp.UpdateParm("Old_Row", oldRow);
            sp.UpdateParm("New_Row", newRow);

            //fire event
            EventType et = new EventType(updateEventId);
            et.FireEvent(sp.Parms);
        }

        public void FireColumnChangeEvents(string tableName, Hashtable oldRow, Hashtable newRow, Hashtable parms)
        {
            /*The data dictionary is used to find which event type is related to a database column.
             * oldRow and parms are passed as variants by they are really dictionaries.
             * This is necessary because a calling function has them defined as variants
             * because they are optional parameters and have to be defined as variants
             * so isMissing() can be used against them.
            */

            string colName;
            int eventTypeId;
            ScriptParms sp = new ScriptParms();

            sp.GenerateParms(tableName, newRow);

            foreach (Hashtable row in getChangeEventsForTable(tableName))
            {
                colName = (string)row["Column_Name"];
                eventTypeId = (int)row["Event_Type_Id"];
                if (eventTypeId == 458)   //draft created claim_id wasn't getting set.
                {
                    int tmpreserveid = Convert.ToInt32(sp.ReserveId().ToString());
                    Reserve myReserve = new Reserve(tmpreserveid);
                    sp.Parms.Add("Claim_Id", myReserve.ClaimId);
                }

                EventType et = new EventType(eventTypeId);

                if (oldRow.Contains(colName) && newRow.Contains(colName))
                    et.FireEventIfNecessary(oldRow[colName], newRow[colName], sp.Parms);
            }
        }

        public void setDataDictionaryDisplayNamesForTable(string table)
        {
            //sets data dictionary display names to default values for a table
            string column, displayName;
            int id;
            DataTable c = db.ExecuteStoredProcedure("usp_Get_Data_Dictionary_Columns", "[" + table + "]");

            foreach(DataRow d in c.Rows)
            {
                column = (string)d["Column Name"];
                id = (int)d["Id"];
                displayName = makeDisplayName(column);
                db.ExecuteStoredProcedure("usp_Update_Data_Dictionary_Column_Display_Name", id, displayName, Convert.ToInt32(HttpContext.Current.Session["userID"]));
            }
        }

        public void updateDataDictionaryColumn(ref string tableName, string columnName, bool isRequired
            , string description, string displayName, string validationType, string eventType)
        {
            db.ExecuteStoredProcedure("usp_Update_Data_Dictionary_Column", "[" + tableName + "]", columnName, isRequired, description, displayName, validationType, eventType);
        }

        private string makeDisplayName(string column)
        {
            //Make a default data dictionary column displayName from a column name - Replace "_" with spaces
            string s;

            s = column.Trim();
            s = s.Replace("_", " ");
            return s;
        }

        public void generateTableEventDefinitions(string tableName)
        {
            //Generate table insert event and column change event definitions for table
            //Associate events with their data dictionary table and column entries.

            //Generate table insert event and associate with data dictionary table row
            db.ExecuteStoredProcedure("usp_Generate_Table_Insert_Event_Definition", "[" + tableName + "]", Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //Generate table update event and associate with data dictionary table row
            db.ExecuteStoredProcedure("usp_Generate_Table_Update_Event_Definition", "[" + tableName + "]", Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //Generate column change event and associate with data dictionary column row
            db.ExecuteStoredProcedure("usp_Generate_Column_Change_Event_Definition", "[" + tableName + "]", Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public Hashtable getDataDictionaryTable(string tableName)
        {
            if (!_dataDictionaryTables.ContainsKey(tableName))
            {
                List<Hashtable> rows = db.ExecuteStoredProcedureReturnHashList("usp_Get_Data_Dictionary_Table", "[" + tableName + "]");
                if (rows.Count == 0)
                    return null;
                _dataDictionaryTables.Add(tableName, rows[0]);
            }
            return (Hashtable)_dataDictionaryTables[tableName];
        }

        public List<Hashtable> getChangeEventsForTable(string tableName)
        {
            if (!_changeEventsForTable.ContainsKey(tableName))
                _changeEventsForTable.Add(tableName, db.ExecuteStoredProcedureReturnHashList("usp_Get_Change_Events_For_Table", "[" + tableName + "]"));
            return (List<Hashtable>)_changeEventsForTable[tableName];
        }

        public int getTableUpdateEvent(string tableName)
        {
            if (!_tableUpdateEvents.ContainsKey(tableName))
                _tableUpdateEvents.Add(tableName, db.GetIntFromStoredProcedure("usp_Get_Table_Update_Event", "[" + tableName + "]"));
            return (int)_tableUpdateEvents[tableName];
        }
        #endregion

        #region Database
        public bool SqlLogging
        {
            get { return db.SqlLogging; }
            set { db.SqlLogging = value; }
        }

        public string getStrFieldData(string columnName, string tableName, string colWhere, string colData)
        {
            //gets string field from a table
            if (colData == "")
                return "";
            string sql = "select " + columnName + " from [" + tableName + "] (nolock) where " + colWhere + " = '" + colData.Replace("'", "''") + "'";
            return db.GetString(sql);
        }

        public int getId(string tableName, string description, string columnName = "Description", string whereClause = "1=1")
        {
            //gets ID for table.  Default search column to "Description" because TypeTable searches will be most common
            if (description == "")
                return 0;
            string sql = "select " + tableName + "_Id as id from [" + tableName + "] (nolock) where " + columnName + " = '" + description.Replace("'", "''") + "' and " + whereClause;
            return db.GetInteger(sql);
        
        }

        public int getId(string tableName, int entirestring, string whereClause = "1=1")
        {
            //gets ID for table.  Default search column to "Description" because TypeTable searches will be most common
           
            string sql = "select " + tableName + "_Id as id from [" + tableName + "] (nolock) where " +  whereClause;
            return db.GetInteger(sql);

        }

        public int InsertRow(Hashtable row, string table)
        {
            /*Insert row into a table.  row is a dictionary of field names/values.
             * Return id of newly inserted row.  If there is an insert event 
             * registered for the table in the data dictionary, an insert event
             * will be raised
            */

            int id = db.InsertRow(row, table, Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //raise insert event if registered for the table
            GenerateTableInsertEvent(table, id);

            return id;
        }

        public Hashtable getRow(string table, int id)
        {
            //returns row from table as dictionary keyed by field names
            return db.GetRow(table, id);
        }
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        public Hashtable GetPasswordENCR(string table, int id)
        {
            //returns row from table as dictionary keyed by field names
            return db.GetRow(table, id);
        }



<<<<<<< HEAD
=======
=======

        
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e

        public void deleteRow(string table, int id)
        {
            db.DeleteRow(table, id);
        }

        public void updateRow(string table, Hashtable newRow, Hashtable oldRow = null, Hashtable parms = null, bool fireEvents = true)
        {
            /*Update row in table and fire column change events
             * oldRow and newRow are dictionaries whose keys are column names and values are column values.
             * parms is a dictionary of parameters to be passed to the event handlers.
             * If the oldrow isn't passed, no events are raised.  oldRow and parms are passed as variants so
             * the isMissing() function can be used against them.
            */

            db.UpdateRow(table, newRow, oldRow, parms, Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //fire table and column update events
            if (fireEvents)
            {
                if (oldRow == null)
                    return;
                if (parms == null)
                    parms = new Hashtable();

                //fire column change events
                FireColumnChangeEvents(table, oldRow, newRow, parms);

                //generate table update event
                FireTableUpdateEvent(table, oldRow, newRow, parms);
            }
        }

     

        public void updateDateColumn(string tableName, int tableId, string columnName, DateTime columnValue)
        {
            //Update a column in a table to a new value.  Use the updateRow() function so that the proper change events can be triggered
            Hashtable row, newRow;

            row = db.GetRow(tableName, tableId);
            newRow = copyDictionary(row);
            newRow[columnName] = columnValue;
            updateRow(tableName, newRow, row);
        }

        public Hashtable copyDictionary(Hashtable source)
        {
            /*Returns copy of source dictionary.  source is defined as variant because some
             * calling routines have it defined as a variant because it is an optional parameter.
            */

            Hashtable newDict = new Hashtable();
            foreach (object key in source.Keys)
                newDict.Add(key, source[key]);
            return newDict;
        }

        public int insertTypeRow(ref string table, string description)
        {
            //insert typeTable row and return ID
            TypeTable.clearTypeTableCache(table);  //clear type table cache so it will reload on next use and contain new value

            Hashtable row = new Hashtable();//row to insert
            row["Description"] = description;
            row["Enum_Name"] = makeEnum(description);
            return InsertRow(row, table);
        }

        public string makeEnum(string desc)
        {
            string s = desc.Trim();

            s = s.Replace("'", "");
            s = s.Replace(".", "");
            s = s.Replace("%", "");
            s = s.Replace("#", "");
            s = s.Replace(",", "_");
            s = s.Replace("-", "_");
            s = s.Replace(" ", "_");
            s = s.Replace("(", "_");
            s = s.Replace(")", "_");
            s = s.Replace("&", "_");
            s = s.Replace("/", "_");
            s = s.Replace("__", "_");
            s = s.Replace("__", "_");

            return s;
        }
        #endregion
       
        #region Dates/Times
        public int getQuarter(int Month)
        {
            //returns quarter from the given month
            switch (Month)
            {
                case 1:
                case 2:
                case 3:
                    //First quarter
                    return 1;
                case 4:
                case 5:
                case 6:
                    //Second quarter
                    return 2;
                case 7:
                case 8:
                case 9:
                    //Third quarter
                    return 3;
                case 10:
                case 11:
                case 12:
                    //Fourth quarter
                    return 4;
                default:
                    //Return first quarter by default
                    return 1;
            }
        }
        #endregion
        
        #region Validation
        public bool isDate(object inputDate, DateTime outputDate = default(DateTime))
        {
            //is string date?
            if (inputDate.GetType() == typeof(DateTime))
            {
                outputDate = DateTime.Parse((string)inputDate);
                return true;
            }
            return DateTime.TryParse((string)inputDate, out outputDate);
        }

        public static bool isNumeric(object number, decimal outputNumber = 0)
        {
            //is the object a number?
            if (number == null)
                return false;

            if ((number.GetType() == typeof(string)) || (number.GetType() == typeof(char)))
            {
                //number is a string - test if numeric
                return decimal.TryParse(number.ToString().Replace("$", ""), out outputNumber);
            }
            else if ((number.GetType() == typeof(Int16)) || (number.GetType() == typeof(Int32)) || (number.GetType() == typeof(Int64)))
            {
                //number is numeric
                outputNumber = (decimal)number;
                return true;
            }
            return false;
        }

        public bool VIN_Checksum(string VIN)
        {
            char[] vin = VIN.ToUpper().ToCharArray();

            if (vin.Length != 17)
            {
                //System.Windows.MessageBox.Show("VIN is not valid.", "Invalid VIN", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                return false;
            }
            int vValue = 0;
            int remainder;
            int sumProduct = 0;
            string ckSum;
            char pos9 = ' ';
            for (int pos = 0; pos < vin.Length; pos++)
            {
                int product = 0;

                if (pos == 8)
                    pos9 = vin[pos];
                else
                {
                    if (!int.TryParse(vin[pos].ToString(), out vValue))
                        vValue = Transliterate(vin[pos]);

                    product = vValue * VIN_Weight(pos + 1);
                }
                sumProduct += product;
            }
            remainder = sumProduct % 11;

            ckSum = remainder == 10 ? "X" : remainder.ToString();
            return ckSum == pos9.ToString();
        }

        private int VIN_Weight(int p)
        {
            switch (p)
            {
                case 1:
                case 11:
                    return 8;
                case 2:
                case 12:
                    return 7;
                case 3:
                case 13:
                    return 6;
                case 4:
                case 14:
                    return 5;
                case 5:
                case 15:
                    return 4;
                case 6:
                case 16:
                    return 3;
                case 7:
                case 17:
                    return 2;
                case 8:
                    return 10;
                case 10:
                    return 9;
                default:
                    return 0;
            }
        }

        private int Transliterate(char p)
        {
            switch (p)
            {
                case 'A':
                case 'J':
                    return 1;
                case 'B':
                case 'K':
                case 'S':
                    return 2;
                case 'C':
                case 'L':
                case 'T':
                    return 3;
                case 'D':
                case 'M':
                case 'U':
                    return 4;
                case 'E':
                case 'N':
                case 'V':
                    return 5;
                case 'F':
                case 'W':
                    return 6;
                case 'G':
                case 'P':
                case 'X':
                    return 7;
                case 'H':
                case 'Y':
                    return 8;
                case 'R':
                case 'Z':
                    return 9;
                default:
                    //System.Windows.MessageBox.Show("The VIN has invalid character " + p.ToString() + ".", "Invalid VIN", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);
                    return 0;
            }
        }
        #endregion
        #region
        #endregion
    }
}

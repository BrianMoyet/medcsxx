﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Claimant:ClaimantTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Class Variables
        private Person m_Person;
        #endregion


        #region Constructors
        public Claimant() : base() { }
        public Claimant(int id) : base(id) { }
        public Claimant(Hashtable row) : base(row) { }
        public Claimant(DataTable table, DataRow row) : base(table, row) { }
        public Claimant(Claim claim, string name)
            : base()
        {
            foreach (Claimant cl in claim.Claimants)
            {
                if (cl.Name.Trim() == name.Trim())
                {
                    this.row = cl.row;
                    return;
                }
            }
        }

        public Claimant(Claim claim, int personId)
            : base()
        {
            foreach (Claimant cl in claim.Claimants)
            {
                if (cl.PersonId == personId)
                {
                    this.row = cl.row;
                    return;
                }
            }
        }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Claimant"; }
        }

        public int ClaimantNo
        {
            get { return (int)row["Claimant_No"]; }
            set { this.setRowValue("Claimant_No", value); }
        }

        public DateTime DateAttemptContactClaimant
        {
            get { return (DateTime)row["Date_Attempt_Contact_Claimant"]; }
            set { this.setRowValue("Date_Attempt_Contact_Claimant", value); }
        }

        public DateTime DateFirstContactClaimant
        {
            get { return (DateTime)row["Date_First_Contact_Claimant"]; }
            set { this.setRowValue("Date_First_Contact_Claimant", value); }
        }

        public int PersonId
        {
            get { return (int)row["Person_Id"]; }
            set { this.setRowValue("Person_Id", value); }
        }
        #endregion

        #region Functions that return class objects
        public Person Person
        {
            get { return m_Person == null ? m_Person = new Person(PersonId) : m_Person; }
            set { m_Person = value; }
        }
        #endregion

        #region Misc Methods
        public int ReserveId(int reserveTypeId)
        {
            //returns reserve ID for a claimant ID and reserve type ID
            return db.GetIntFromStoredProcedure("usp_Get_Reserve_Id", this.Id, reserveTypeId);
        }

        public Reserve Reserve(int reserveTypeId)
        {
            //returns reserve for reserve type ID
            return new Reserve(this.ReserveId(reserveTypeId));
        }

        public bool ReserveExists(int reserveTypeId)
        {
            DataTable a = db.ExecuteStoredProcedure("usp_Reserve_Exists", this.Id, reserveTypeId);
            if (a.Rows.Count == 0)
                return false;

            if ((int)a.Rows[0]["c"] == 0)
                return false;
            return true;
        }

        public Reserve InsertReserve(int reserveTypeId)
        {
            Reserve res = new Reserve();

            res.ClaimantId = this.Id;
            res.ReserveTypeId = reserveTypeId;
            res.Update();
            return res;
        }

        public int CreateReserveIfNecessary(int reserveTypeId)
        {
            /*if a reserve line corresponding to the passed claimantId and reserveTypeId doesn't exist,
             * create one and return the Reserve ID.  If a Reserve Line does exist, return its Reserve ID.
            */

            return this.ReserveExists(reserveTypeId)?this.ReserveId(reserveTypeId):this.InsertReserve(reserveTypeId).Id;
        }

        public string ReserveTypesDescription()
        {
            //returns reserve types for claimant as a description
            return db.GetStringFromStoredProcedure("usp_Get_Reserve_Types_Description", this.Id);
        }

        public string Name
        {
            get
            {
                Person p = this.Person;
                if (p == null)
                    return "";
                return p.Name;
            }
        }

        public List<int> AssignedToUserIds()
        {
            //returns all user ids this claimant is assigned to
            IntegerSet userIds = new IntegerSet();
            foreach (Reserve res in this.Reserves())
                userIds.Add(res.AssignedToUserIds());
            return userIds.ToList();
        }

        public override ArrayList Reserves()
        {
            ArrayList a = new ArrayList();
            DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claimant", this.Id);
            foreach (DataRow row in myReserves.Rows)
                a.Add(new Reserve(myReserves, row));
            return a;
        }

        public ArrayList ReserveIds()
        {
            ArrayList a = new ArrayList();
            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claimant", this.Id).Rows)
                a.Add((int)row["Reserve_Id"]);
            return a;
        }

        public Injured Injured()
        {
            foreach (Injured i in this.Claim.Injured())
            {
                if (i.PersonId == this.PersonId)
                    return i;
            }
            return null;
        }

        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_Claimant_Row";
        }
        #endregion

        #region Create PD Reserve
        public Reserve CreatePropertyDamageReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Property_Damage))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Property_Damage);
            else
            {
                Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.Property_Damage);
                res.FireEvent((int)modGeneratedEnums.EventType.Property_Damage_Reserve_Created, parms);
                return res;
            }
        }

        public Reserve CreateCGLPropertyReserve(int reserveTypeId, int propertyTypeId, Hashtable parms = null)
        {
            /*create the reserve for the Commercial General Liability Property
             * the unique key for CGL properties is (Reserve Type, Property Type, Claimant)
            */

            //create a reserve object
            Reserve myReserve = null;

            if (this.ReserveExists(reserveTypeId))
            {
                //get the reserve
                myReserve = this.Reserve(reserveTypeId);

                if (PropertyDamage.ReserveExistsOfPropertyType(myReserve.Id, propertyTypeId))
                    return myReserve;                //a reserve of this type and property type exists for this claimant already
            }

            //the reserve type or property type are different - create a new reserve

            myReserve = this.InsertReserve(reserveTypeId);
            myReserve.FireEvent((int)modGeneratedEnums.EventType.Commercial_General_Liability_PD_Reserve_Created, parms);

            return myReserve;
        }
        #endregion

        #region Create BI Reserve
        public Reserve CreateCGLInjuryReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Commercial_General_Liability_BI))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Commercial_General_Liability_BI);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.Commercial_General_Liability_BI);
            res.FireEvent((int)modGeneratedEnums.EventType.Commercial_General_Liability_BI_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateABIMajorReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.ABI_Major))
                return this.Reserve((int)modGeneratedEnums.ReserveType.ABI_Major);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.ABI_Major);
            res.FireEvent((int)modGeneratedEnums.EventType.ABI_Major_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateABIMinorReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.ABI_Minor))
                return this.Reserve((int)modGeneratedEnums.ReserveType.ABI_Minor);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.ABI_Minor);
            res.FireEvent((int)modGeneratedEnums.EventType.ABI_Minor_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateMedicalPaymentsReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Medical_Payments))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Medical_Payments);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.Medical_Payments);
            res.FireEvent((int)modGeneratedEnums.EventType.Med_Pay_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateUnderinsuredMotoristReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Underinsured_Motorist))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Underinsured_Motorist);

            Reserve res=this.InsertReserve((int)modGeneratedEnums.ReserveType.Underinsured_Motorist);
            res.FireEvent((int)modGeneratedEnums.EventType.UIM_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateUninsuredMotoristReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Uninsured_Motorist))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Uninsured_Motorist);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.Uninsured_Motorist);
            res.FireEvent((int)modGeneratedEnums.EventType.UM_Reserve_Created, parms);
            return res;
        }

        public Reserve CreatePIPMedicalReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.PIP_Medical))
                return this.Reserve((int)modGeneratedEnums.ReserveType.PIP_Medical);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.PIP_Medical);
            res.FireEvent((int)modGeneratedEnums.EventType.PIP_Medical_Reserve_Created, parms);
            return res;
        }

        public Reserve CreatePIPRehabReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.PIP_Rehab))
                return this.Reserve((int)modGeneratedEnums.ReserveType.PIP_Rehab);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.PIP_Rehab);
            res.FireEvent((int)modGeneratedEnums.EventType.PIP_Rehab_Reserve_Created, parms);
            return res;
        }

        public Reserve CreatePIPWagesReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.PIP_Wages))
                return this.Reserve((int)modGeneratedEnums.ReserveType.PIP_Wages);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.PIP_Wages);
            res.FireEvent((int)modGeneratedEnums.EventType.PIP_Wages_Reserve_Created, parms);
            return res;
        }

        public Reserve CreatePIPEssentialServicesReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.PIP_Essential_Services))
                return this.Reserve((int)modGeneratedEnums.ReserveType.PIP_Essential_Services);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.PIP_Essential_Services);
            res.FireEvent((int)modGeneratedEnums.EventType.PIP_Essential_Services_Reserve_Created, parms);
            return res;
        }

        public Reserve CreatePIPFuneralReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.PIP_Funeral))
                return this.Reserve((int)modGeneratedEnums.ReserveType.PIP_Funeral);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.PIP_Funeral);
            res.FireEvent((int)modGeneratedEnums.EventType.PIP_Funeral_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateCollisionReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Collision))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Collision);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.Collision);
            res.FireEvent((int)modGeneratedEnums.EventType.Collision_Reserve_Created, parms);
            return res;
        }

        public Reserve CreateComprehensiveReserve(Hashtable parms = null)
        {
            if (this.ReserveExists((int)modGeneratedEnums.ReserveType.Comprehensive))
                return this.Reserve((int)modGeneratedEnums.ReserveType.Comprehensive);

            Reserve res = this.InsertReserve((int)modGeneratedEnums.ReserveType.Comprehensive);
            res.FireEvent((int)modGeneratedEnums.EventType.Comprehensive_Reserve_Created, parms);
            return res;
        }
        #endregion

        #region Nonimplemented
        public int ClaimantId
        {
            get
            {
                return 0;
            }
            set
            {
                base.ClaimantId = value;
            }
        }

        public int ClaimId
        {
            get
            {
                return 0;
            }
            set
            {
                base.ClaimId = value;
            }
        }
        #endregion
    }
}

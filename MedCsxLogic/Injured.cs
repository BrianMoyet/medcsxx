﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Injured:ClaimTable
    {

        private static clsDatabase db = new clsDatabase();
        private ArrayList ICDCodeList = new ArrayList();
        private ArrayList VCodeList = new ArrayList();
        #region Constructors
        public Injured() : base() { }
        public Injured(int id) : base(id) { }
        public Injured(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Injured"; }
        }

        public int AttorneyVendorId
        {
            get { return (int)row["Attorney_Vendor_Id"]; }
            set { this.setRowValue("Attorney_Vendor_Id", value); }
        }

        public string ExtentOfInjury
        {
            get { return (string)row["Extent_Of_Injury"]; }
            set { this.setRowValue("Extent_Of_Injury", value); }
        }

        public int InjuredId
        {
            get { return (int)row["Injured_Id"]; }
            set { this.setRowValue("Injured_Id", value); }
        }
        
        [System.ComponentModel.Description("Cause of the injury.")]
        public int InjuryCauseId
        {
            get { return (int)row["Injury_Cause_Id"]; }
            set { this.setRowValue("Injury_Cause_Id", value); }
        }

        [System.ComponentModel.Description("Description of injury.")]
        public string InjuryDescription
        {
            get { return (string)row["Injury_Description"]; }
            set { this.setRowValue("Injury_Description", value); }
        }

        [System.ComponentModel.Description("Type of injury.")]
        public int InjuryTypeId
        {
            get { return (int)row["Injury_Type_Id"]; }
            set { this.setRowValue("Injury_Type_Id", value); }
        }

        public double MedicalLeinAmount
        {
            get { return (double)((decimal)row["Medical_Lein_Amount"]); }
            set { this.setRowValue("Medical_Lein_Amount", value); }
        }

        public int PersonId
        {
            get { return (int)row["Person_Id"]; }
            set { this.setRowValue("Person_Id", value); }
        }
        #endregion

        #region Methods that return class objects
        public InjuryCause CauseOfInjury()
        {
            return new InjuryCause(this.InjuryCauseId);
        }

        public InjuryType TypeOfInjury()
        {
            return new InjuryType(this.InjuryTypeId);
        }

        public Person Person()
        {
            return new Person(this.PersonId);
        }

        #endregion
        #region Misc Methods
        public List<Hashtable> InjuredBodyParts()
        {
            //returns the injured body parts of this person
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Injured_Body_Parts", this.Id);
        }

        public DataTable ISO_InjuredBodyParts()
        {
            //returns datatable of ISO code and description of injured body parts
            return db.ExecuteStoredProcedure("usp_Get_ISO_Injured_Body_Part_From_Injured_Id", this.Id);
        }

        public List<Hashtable> Liens()
        {
            //returns all the liens for the injured person
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Injured_Liens", this.Id);
        }

        public void DeleteLien(int injuredLienId)
        {
            //delete the injured lien
            db.ExecuteStoredProcedure("usp_Delete_Injured_Lien", injuredLienId);
        }

        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            //if injured person is changing - change claimant on reserve lines
            if (!this.isAdd)
            {
                if (this.PersonId != (int)this.oldRow["Person_Id"])
                {
                    //change claimants on reserve line
                    int claimantId = this.Claim.CreateClaimantIfNecessary(this.PersonId);
                    foreach (InjuredCoverage ic in this.InjuredCoverages())
                    {
                        if (ic.ReserveId != 0)
                        {
                            Reserve res = ic.Reserve;
                            res.ClaimantId = claimantId;
                            res.Update();
                        }
                    }
                }
            }
            return base.Update(parms, fireEvents);
        }

        public ArrayList InjuredCoverages()
        {
            //returns arraylist of corresponding InjuredCoverage objects
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Injured_Coverage_Rows", this.Id);
            ArrayList a = new ArrayList();
            foreach (DataRow row in rows.Rows)
                a.Add(new InjuredCoverage(rows, row));
            return a;
        }

        public InjuredCoverage InjuredCoverageForReserveType(int reserveTypeId)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Injured_Coverage_For_Reserve_Type", this.Id, reserveTypeId);
            if (rows.Rows.Count == 0)
            {
                //insert if not present
                InjuredCoverage ic = new InjuredCoverage();
                ic.ReserveTypeId = reserveTypeId;
                ic.InjuredId = this.Id;
                ic.CoverageSpecified = true;
                ic.ReserveId = 0;
                ic.Update();
                return ic;
            }
            return new InjuredCoverage(rows, rows.Rows[0]);
        }

        public string CoveragesDescription
        {
            get { return db.GetStringFromStoredProcedure("usp_Injured_Coverages_Description", this.Id); }
        }

        public string Description
        {
            get
            {
                StringBuilder s = new StringBuilder();
                s.Append("Injured: ");
                s.Append(this.Person().Description);
                s.Append(Environment.NewLine);

                s.Append("Extent of Injury: ");
                s.Append(this.ExtentOfInjury);
                s.Append(Environment.NewLine);

                //add the liens description
                s.Append(this.LiensDescription);

                return s.ToString();
            }
        }

        public string LiensDescription
        {
            //description of the liens for the injured person
            get
            {
                //get the injured liens
                List<Hashtable> myLiens = this.Liens();

                //create the description
                StringBuilder s = new StringBuilder();

                foreach (Hashtable myLien in myLiens)
                {
                    Vendor myVendor = new Vendor((int)myLien["Vendor_Id"]);
                    s.Append("Vendor: ");
                    s.Append(myVendor.Description);
                    s.Append(Environment.NewLine);

                    s.Append("Vendor Type: ");
                    s.Append(myVendor.VendorSubType().Description);
                    s.Append(Environment.NewLine);

                    s.Append("Lien Amount: ");
                    //s.Append(((double)myLien["Amount"]).ToString());
                    s.Append((Convert.ToDouble(myLien["Amount"]).ToString()));
                    s.Append(Environment.NewLine);
                    s.Append(Environment.NewLine);
                }

                //add the old lien to the description
                if (this.AttorneyVendorId != 0)
                {
                    Vendor myVendor = new Vendor(this.AttorneyVendorId);
                    s.Append("Vendor: ");
                    s.Append(myVendor.Description);
                    s.Append(Environment.NewLine);

                    s.Append("Vendor Type: ");
                    s.Append(myVendor.VendorSubType().Description);
                    s.Append(Environment.NewLine);

                    s.Append("Lien Amount: ");
                    s.Append(this.MedicalLeinAmount.ToString("c"));
                    s.Append(Environment.NewLine);
                    s.Append(Environment.NewLine);
                }

                if (s.Length > 0)
                    return "Liens" + Environment.NewLine + s.ToString();
                else
                    return "";
            }
        }

        public int ClaimantId
        {
            get { return this.Claim.CreateClaimantIfNecessary(this.PersonId); }
        }

        public Claimant Claimant()
        {
            return new Claimant(this.ClaimantId);
        }

        public BIEvaluation BIEvaluation()
        {
            //returns related BIEvaluation objects - max should be 1
            ArrayList a = this.ChildObjects(typeof(BIEvaluation));

            if (a.Count > 0)
                return (BIEvaluation)a[0];
            else
            {
                BIEvaluation b = new BIEvaluation();
                b.InjuredId = this.Id;
                b.Update();
                return b;
            }
        }

        public ArrayList ICDCodes()
        {
            if (ICDCodeList.Count > 0)
                return ICDCodeList;

            DataTable rows = db.ExecuteStoredProcedure("usp_Get_ICD_Code_For_Injured_Id", this.Id);
            int position = 0;
            foreach (DataRow dr in rows.Rows)
            {
                //insert if not present
                ICDCode ic = new ICDCode();
                ic.ICDCodeId = (int)dr.ItemArray[0];
                ic.InjuredId = this.Id;
                ic.isICDCode = true;
                ic.isVCode = false;
                ic.Slot = position;
                ic.Code = (string)dr.ItemArray[2];

                ICDCodeList.Add(ic);
                if (position++ > 2)
                    break;
            }
            return ICDCodeList;
        }

        public ArrayList VCodes()
        {
            if (VCodeList.Count > 0)
                return VCodeList;

            DataTable rows = db.ExecuteStoredProcedure("usp_Get_V_Code_For_Injured_Id", this.Id);
            int position = 0;
            foreach (DataRow dr in rows.Rows)
            {
                //insert if not present
                ICDCode ic = new ICDCode();
                ic.ICDCodeId = (int)dr.ItemArray[0];
                ic.InjuredId = this.Id;
                ic.isICDCode = true;
                ic.isVCode = false;
                ic.Slot = position;
                ic.Code = (string)dr.ItemArray[2];

                VCodeList.Add(ic);
                if (position++ > 2)
                    break;
            }
            return VCodeList;
        }

        public void Update_ICDCodes(int slot, string codeUpdated, bool isICDcode = true)
        {
            ICDCode ic;
            if (isICDcode)
            {
                if (ICDCodeList.Count >= slot + 1)
                {
                    ic = (ICDCode)ICDCodeList[slot];
                    if (string.IsNullOrWhiteSpace(codeUpdated))
                    {
                        ic.DeleteRow();
                        ICDCodeList.Remove(ic);
                        return;
                    }

                }
                else
                {
                    if (string.IsNullOrWhiteSpace(codeUpdated))
                        return;
                    ic = new ICDCode();
                }
            }
            else
            {
                if (VCodeList.Count >= slot + 1)
                {
                    ic = (ICDCode)VCodeList[slot];
                    if (string.IsNullOrWhiteSpace(codeUpdated))
                    {
                        ic.DeleteRow();
                        VCodeList.Remove(ic);
                        return;
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(codeUpdated))
                        return;
                    ic = new ICDCode();
                }
            }
            ic.InjuredId = this.Id;
            ic.Code = codeUpdated;
            ic.isICDCode = isICDcode;
            ic.isVCode = !isICDcode;
            ic.Update();
        }
        #endregion
    }
}

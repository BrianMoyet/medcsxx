﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Reserve:ReserveTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Class Variables
        private ReserveType m_ReserveType = null;
        private ReserveStatus m_ReserveStatus = null;
        private ReserveAudit m_ReserveAudit = null;
        private static Hashtable reserveClaimIds = new Hashtable();  //Cache reserve claimIds - a claim_Id for the reserve never changes
        private ReserveHistory m_ReserveHistory = null;
        #endregion

        #region Constructors
        public Reserve() : base() { }
        public Reserve(int id) : base() 
        {
            string sql = "SELECT * FROM [" + this.TableName + "] WHERE " + this.TableName + "_Id='" + id + "'";
            if (this.row.Count == 0)
            {
                List<Hashtable> Rows = db.ExecuteSelectReturnHashList(sql);
                foreach (Hashtable thisRow in Rows)
                {
                    row = thisRow;
                    break;
                }

            }
        }
        public Reserve(DataTable Table, DataRow Row) : base(Table, Row) { }
        public Reserve(Hashtable row) : base(row) { }
        public Reserve(Claim claim, string reserveDescription) : this()
        {
            foreach (Reserve r in claim.Reserves())
            {
                if (reserveDescription.Trim() == r.Description().Trim())
                {
                    this.row = r.row;
                    return;
                }

            }

        }
        #endregion

        #region Column Names
        public bool AdjusterEnteredFirstFileNote
        {
            get { return (bool)row["Adjuster_Entered_First_File_Note"]; }
            set { this.setRowValue("Adjuster_Entered_First_File_Note", value); }
        }

        public int AdjusterId
        {
            get { return (int)row["Adjuster_Id"]; }
            set { this.setRowValue("Adjuster_Id", value); }
        }

        public DateTime DateFirstLossPayment
        {
            get { return (DateTime)row["Date_First_Loss_Payment"]; }
            set { this.setRowValue("Date_First_Loss_Payment", value); }
        }

        public int AssignedToUserGroupId
        {
            get { return (int)row["Assigned_To_User_Group_Id"]; }
            set { this.setRowValue("Assigned_To_User_Group_Id", value); }
        }

        public DateTime DateClosed
        {
            get { return (DateTime)row["Date_Closed"]; }
            set { this.setRowValue("Date_Closed", value); }
        }

        public DateTime DateClosedSalvage
        {
            get { return (DateTime)row["Date_Closed_Salvage"]; }
            set { this.setRowValue("Date_Closed_Salvage", value); }
        }

        public DateTime DateClosedSubro
        {
            get { return (DateTime)row["Date_Closed_Subro"]; }
            set { this.setRowValue("Date_Closed_Subro", value); }
        }

        public DateTime DateClosedDeductibles
        {
            get { return (DateTime)row["Date_Closed_Deductibles"]; }
            set { this.setRowValue("Date_Closed_Deductibles", value); }
        }

        public DateTime DateClosedSIRRecovery
        {
            get { return (DateTime)row["Date_Closed_SIR_Recovery"]; }
            set { this.setRowValue("Date_Closed_SIR_Recovery", value); }
        }

        public DateTime DateIndexed
        {
            get { return (DateTime)row["Date_Indexed"]; }
            set { this.setRowValue("Date_Indexed", value); }
        }

        public DateTime DateOpen
        {
            get { return (DateTime)row["Date_Open"]; }
            set { this.setRowValue("Date_Open", value); }
        }

        public int DenialReasonTypeId
        {
            get { return (int)row["Denial_Reason_Type_Id"]; }
            set { this.setRowValue("Denial_Reason_Type_Id", value); }
        }

        public double GrossExpenseReserve
        {
            get { return Convert.ToDouble(row["Gross_Expense_Reserve"]); }
            set { this.setRowValue("Gross_Expense_Reserve", value); }
        }

        public double GrossLossReserve
        {
            //get { return (double)((decimal)row["Gross_Loss_Reserve"]); }
            get { return Convert.ToDouble(row["Gross_Loss_Reserve"]); }
            set { this.setRowValue("Gross_Loss_Reserve", value); }
        }

        public bool isCaseReserve
        {
            get { return (bool)row["Is_Case_Reserve"]; }
            set { this.setRowValue("Is_Case_Reserve", value); }
        }

        [System.ComponentModel.Description("Is the reserve in litigation?")]
        public bool inLitigation
        {
            get { return (bool)row["In_Litigation"]; }
            set { this.setRowValue("In_Litigation", value); }
        }

        public bool isTotalLoss
        {
            get { return (bool)row["Is_Total_Loss"]; }
            set { this.setRowValue("Is_Total_Loss", value); }
        }

        public double NetExpenseReserve
        {
            get { return Convert.ToDouble(row["Net_Expense_Reserve"]); }
            set { this.setRowValue("Net_Expense_Reserve", value); }
        }

        public double NetLossReserve
        {
            get { return Convert.ToDouble(row["Net_Loss_Reserve"]); }
            set { this.setRowValue("Net_Loss_Reserve", value); }
        }

        public int NoSupplementalPayments
        {
            get { return (int)row["No_Supplemental_Payments"]; }
            set { this.setRowValue("No_Supplemental_Payments", value); }
        }

        public double PaidExpense
        {
            get { return (double)((decimal)row["Paid_Expense"]); }
            set { this.setRowValue("Paid_Expense", value); }
        }

        public double PaidLoss
        {
            get { return (double)((decimal)row["Paid_Loss"]); }
            set { this.setRowValue("Paid_Loss", value); }
        }

        public DateTime ReopenDate
        {
            get { return (DateTime)row["Reopen_Date"]; }
            set { this.setRowValue("Reopen_Date", value); }
        }

        public int ReserveSequenceNo
        {
            get { return (int)row["Reserve_Sequence_No"]; }
            set { this.setRowValue("Reserve_Sequence_No", value); }
        }

        public int ReserveStatusId
        {
            get { return (int)row["Reserve_Status_Id"]; }
            set { this.setRowValue("Reserve_Status_Id", value); }
        }
        
        public int ReserveTypeId
        {
            get { return (int)row["Reserve_Type_Id"]; }
            set { this.setRowValue("Reserve_Type_Id", value); }
        }

        public int ReserveHistoryId
        {
            get { return db.GetIntFromStoredProcedure("usp_Get_Reserve_History_Id_For_Reserve_Id", this.ReserveId); }
        }
        #endregion
       
        #region Methods
        public override string TableName
        {
            get { return "Reserve"; }
        }

        public static int GetClaimantIdForReserveId(ref int reserveId)
        {
            //Returns ClaimantId for the given ReserveId
            return db.GetIntFromStoredProcedure("usp_Get_Claimant_Id_For_Reserve_Id", reserveId);
        }

        public static int GetClaimIdForReserveId(ref int reserveId)
        {
            //Returns ClaimId for the given ReserveId
            if (reserveClaimIds.Contains(reserveId))
                return (int)reserveClaimIds[reserveId];

            int claimId = db.GetIntFromStoredProcedure("usp_Get_Claim_Id_For_Reserve_Id", reserveId);
            reserveClaimIds.Add(reserveId, claimId);
            return claimId;
        }

        public static bool isReserveInLitigation(int reserveId)
        {
            //Is reserve in litigation?
            return db.GetBoolFromStoredProcedure("usp_Is_Reserve_In_Litigation", reserveId);
        }

        public static void setReserveInLitigation(int reserveId, bool inLitigation)
        {
            //Sets the inLitigation status of the reserve
            db.ExecuteStoredProcedure("usp_Update_Reserve_Litigation", reserveId, inLitigation);
        }

        public override int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {

            if (this.isAdd)
            {
                //Get average loss reserve amount
                //double loss = 0; // 
                double loss = db.GetDblFromStoredProcedure("usp_Get_Average_Reserve", this.ClaimantId, this.ReserveTypeId);
                
                //Insert reserve row
                this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Open_Reserve;
                this.DateOpen = ml.DBNow();
                this.GrossLossReserve = loss;
                this.NetLossReserve = loss;
                this.GrossExpenseReserve = 0;
                this.NetExpenseReserve = 0;
                this.ReserveSequenceNo = 1;
                this.isCaseReserve = false;
                int reserveId = base.Update(parms, fireEvents);

                //Insert open reserve transactions
                this.InsertTransaction(0.0, (int)modGeneratedEnums.TransactionType.Open_Reserve, true); //Loss
                this.InsertTransaction(0.0, (int)modGeneratedEnums.TransactionType.Open_Reserve, false); //Expense

                //insert history row
                this.UpdateHistory();

                //Insert Loss reserve transaction row
                if (loss > 0.0)
                    this.InsertTransaction(loss, (int)modGeneratedEnums.TransactionType.Reserve, true);
           }

           // Set adjuster id on claim to this reserve's new adjuster
            Claim c = this.Claim;
            if (this.AdjusterId != 0)
            {
                c.AdjusterId = this.AdjusterId;
                c.Update();
            }
            return base.Update(parms, fireEvents);
        }

        public ReserveType ReserveType
        {
            get { return m_ReserveType == null ? m_ReserveType = new ReserveType(this.ReserveTypeId) : m_ReserveType; }
        }

        public ReserveStatus ReserveStatus
        {
            get { return m_ReserveStatus == null ? m_ReserveStatus = new ReserveStatus(this.ReserveStatusId) : m_ReserveStatus; }
        }

        public ReserveHistory ReserveHistory
        {
            get
            {
                if (m_ReserveHistory == null)
                    m_ReserveHistory = new ReserveHistory(this.ReserveHistoryId);

                return m_ReserveHistory;
            }
        }
        public int FirstLockId
        {
            //Returns first active reserve lock fro the reserve
            get { return db.GetIntFromStoredProcedure("usp_Get_First_Reserve_Lock", this.Id); }
        }

        public bool isLocked
        {
            get { return db.GetBoolFromStoredProcedure("usp_IsReserveLocked", this.Id); }
        }

        public bool canVoid
        {
            get { return db.GetBoolFromStoredProcedure("usp_Can_Void_Reserve", this.Id); }
        }

        public int NoTransactionLocks 
        {
            //Returns the number of transaction locks for reserve
            get { return db.GetIntFromStoredProcedure("usp_Get_No_Transaction_Locks_For_Reserve", this.Id); }
        }

        public ReserveAudit ReserveAudit 
        {
            get
            {
                if (this.m_ReserveAudit == null)
                {
                    DataTable rows = db.ExecuteStoredProcedure("usp_Get_Reserve_Audit_For_Reserve", this.Id);
                    if (rows.Rows.Count == 0)
                    {
                        this.m_ReserveAudit = new ReserveAudit();
                        this.m_ReserveAudit.ReserveId = this.Id;
                    }
                    else
                        this.m_ReserveAudit = new ReserveAudit(rows, rows.Rows[0]);
                }
                return this.m_ReserveAudit;
            }
        }

        public string Description()
        {
            string s = this.ReserveType.Description + " for " + this.Claimant.Person.Name;
            return s;
        }

        public static string ReserveDescription(ref int reserveId)
        {
            //Returns a reserve description
            return new Reserve(reserveId).Description();
        }

        public User AssignedToUser()
        {
            return new User(this.AdjusterId);
        }

        public UserGroup AssignedToUserGroup()
        {
            return new UserGroup(this.AssignedToUserGroupId);
        }

        public string AssignedToDescription()
        {
            return this.AdjusterId == 0 ? this.AssignedToUserGroup().Description : this.AssignedToUser().Name;
        }

        public override void AssignTo(User user)
        {
            if (user == null)
                return;
            this.AssignTo(user.Id, 0);
        }

        public override void AssignTo(UserGroup userGroup)
        {
            if (userGroup == null)
                return;
            this.AssignTo(0, userGroup.Id);
        }

        public void AssignTo(int userId, int userGroupId)
        {
            //don't assign the reserve if the reserveId is zero
            if (this.Id == 0)
                return;

            bool unassigned = false;
            if (this.AdjusterId == 0)
                unassigned = true;

            //no change in assignment
            if ((this.AssignedToUserGroupId == userGroupId) && (this.AdjusterId == userId))
                return;

            //userId and userGroupId are both invalid
            if ((userId == 0) && (userGroupId == 0))
                return;

            //Update reserve row
            this.AssignedToUserGroupId = userGroupId;
            this.AdjusterId = userId;
            this.Update();

            //Add reserve assignment
            ReserveAssignment.InsertReserveAssignment(this.Id, userId, userGroupId, ml.DBNow());

            /*Fire reserve assignment event if the claim setup is complete.
             * Before claim setup is complete, all reserve assignments are grouped into 
             * an Initial Reserve Assignment file note.
            */
            //if (unassigned)
            //    this.FireEvent((int)modGeneratedEnums.EventType.Initial_Reserve_Assignment);
            //else

<<<<<<< HEAD
                this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Assignment);
=======
<<<<<<< HEAD
                this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Assignment);
=======
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Assignment);
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        }

        public int InsertFileNote(int fileNoteTypeId, string fileNoteText)
        {
            //inserts file note for reserve and return new file note Id

            FileNote f = new FileNote();
            f.FileNoteTypeId = fileNoteTypeId;
            f.FileNoteText = fileNoteText;
            f.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            f.ClaimId = this.ClaimId;
            f.ClaimantId = this.ClaimantId;
            f.ReserveId = this.Id;
            return f.Update();
        }

        public ReserveAssignment CurrentReserveAssignment()
        {
            int reserveAssignmentId = db.GetIntFromStoredProcedure("usp_Get_Current_Reserve_Assignment", this.Id);

            if (reserveAssignmentId == 0)
                return null;
            return new ReserveAssignment(reserveAssignmentId);
        }

        public ReserveLock LatestLockForEventType(int eventTypeId)
        {
            int reserveLockId = db.GetIntFromStoredProcedure("usp_Get_Latest_Reserve_Lock_For_Event_Type", this.Id, eventTypeId);

            if (reserveLockId == 0)
                return null;
            return new ReserveLock(reserveLockId);
        }

        public bool SyncEventExists(ref int eventTypeId)
        {
            //Does Reserve_Sync_Event row exist for event type?

            return db.GetBoolFromStoredProcedure("usp_Reserve_Sync_Event_Exists", this.Id, eventTypeId);
        }

        public void Lock(int fileNoteId)
        {
            //Locks reserve line

            ReserveLock reserveLock = new ReserveLock();
            reserveLock.ReserveId = ReserveId;
            reserveLock.isLocked = true;
            reserveLock.LockFileNoteId = fileNoteId;
            reserveLock.LockDate = ml.DBNow();
            reserveLock.ConditionStillExists = true;
            reserveLock.Update();
        }

        public bool FireEvent(int eventTypeId, Hashtable parms = null)
        {
            /* Fire event which is relevent to a reserve line (Subro greater than loss, Salvage greater than loss, etc)
             * eventTypeId is the event type (Subr greater than loss, etc.).
            */

            EventType et = new EventType(eventTypeId);
            ScriptParms sp = new ScriptParms(parms);
            sp.GenerateParms(this);

            return et.FireEvent(sp.Parms) ? true : false;
        }

        public bool FireEventIfSyncExists(int checkEventTypeId, int eventTypeId)
        {
            //Fire reserve event if synce exists for other event. 
            //Was event fired?

            return this.SyncEventExists(ref checkEventTypeId) ? this.FireEvent(eventTypeId) : false;
        }

        public void ClearEvent(int eventTypeId)
        {
            /*Clears event for a reserve.  
             * Delete any alerts associated with this event.
             * Inactivate any locks associated with event.
             * Set Condition_Exists column to false.
            */

            int fileNoteId;
            EventType et = new EventType(eventTypeId);

            db.ExecuteStoredProcedure("usp_Delete_Reserve_Alerts_For_Event", this.Id, eventTypeId);
            db.ExecuteStoredProcedure("usp_Delete_Reserve_Sync_Event", this.Id, eventTypeId);

            DataTable myReserveLocks = db.ExecuteStoredProcedure("usp_Get_Reserve_Locks_For_Event_Type", this.Id, eventTypeId);
            foreach (DataRow row in myReserveLocks.Rows)
            {
                ReserveLock reserveLock = new ReserveLock(myReserveLocks, row);

                if (reserveLock.isLocked)
                {
                    //Unlock locked reserve
                    fileNoteId = reserveLock.Reserve().InsertFileNote((int)modGeneratedEnums.FileNoteType.Unlock_Reserve,
                                                                 "System Unlocked " + reserveLock.Reserve().Description() + " Because Condition '" + et.Description + "' No Longer Exists");

                    reserveLock.isLocked = false;
                    reserveLock.UnlockFileNoteId = fileNoteId;
                    reserveLock.UnlockDate = ml.DBNow();
                    reserveLock.ConditionStillExists = false;
                    reserveLock.Update();
                }
                else
                {
                    //Reserve is not locked - set condition still exists to false

                    reserveLock.ConditionStillExists = false;
                    reserveLock.Update();
                }
            }
        }

        public int InsertTransaction(double amount, int transactionTypeId, bool isLoss)
        {
            //Insert transaction row and return new Transaction_Id

            Transaction t = new Transaction();
            t.ReserveId = this.Id;
            t.TransactionTypeId= transactionTypeId;
            t.isLoss= isLoss;
            t.TransactionAmount = amount;
            return t.Update();
        }

        public int InsertTransaction(double amount, int transactionTypeId, bool isLoss, int reserveid)
        {
            //Insert transaction row and return new Transaction_Id

            Transaction t = new Transaction();
            t.ReserveId =reserveid;
            t.TransactionTypeId = transactionTypeId;
            t.isLoss = isLoss;
            t.TransactionAmount = amount;
            return t.Update();
        }

        public void UpdateHistory()
        {
            //TODO
            //if (ReserveHistory == null)
            //{
            //    ReserveHistory rh = new ReserveHistory();
            //    rh.StatusOpenDate = ml.DBNow();
            //    rh.ReserveId = this.ReserveId;
            //    rh.ReserveStatusId = this.ReserveStatusId;
            //    rh.IsCurrentStatus = true;
            //    rh.Update();
            //}
            //else if (ReserveHistory.ReserveStatusId != this.ReserveHistoryId)
            //{
            //    ReserveHistory.StatusCloseDate = ml.DBNow();
            //    ReserveHistory.IsCurrentStatus = false;
            //    ReserveHistory.Update();

            //    ReserveHistory rh = new ReserveHistory();
            //    rh.StatusOpenDate = ml.DBNow();
            //    rh.ReserveId = this.ReserveId;
            //    rh.ReserveStatusId = this.ReserveStatusId;
            //    rh.IsCurrentStatus = true;
            //    rh.Update();
            //}
        }
        public int InsertOffsettingTransaction(double amount, bool isLoss)
        {
            //Insert offsetting transaction and return Id of newly inserted transaction row

            int transactionTypeId;

            if (this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                transactionTypeId = (int)modGeneratedEnums.TransactionType.Reserve;
            else
                transactionTypeId = (int)modGeneratedEnums.TransactionType.Supplemental_Reserve;

            return this.InsertTransaction(amount, transactionTypeId, isLoss);
        }

        public void ChangeNetExpenseReserve(double newAmount, Hashtable eventTypeIds = null)
        {
            //Changes net expense reserve amount

            double oldAmount; //old net reserve amount
            Hashtable parms;  //parms for firing event
            int claimantId;
            int claimId;

            oldAmount = this.NetExpenseReserve;

            //Issue offsetting transaction
            this.InsertOffsettingTransaction(newAmount - oldAmount, false);

            //Save new gross and net expense reserve
            this.NetExpenseReserve = newAmount;
            this.GrossExpenseReserve += newAmount - oldAmount;
            this.Update();

            //Fire events
            if (eventTypeIds == null)
                return;

            if (eventTypeIds.Count > 0)
            {
                claimantId = this.ClaimantId;
                claimId = this.ClaimId;
                foreach(int myKey in eventTypeIds.Keys)
                {
                    //Fire event to lock reserve
                    parms = (Hashtable)eventTypeIds[myKey];

                    if (parms == null)
                        parms = new Hashtable();

                    parms.Add("New_Value", newAmount);
                    parms.Add("Old_Value", oldAmount);
                    parms.Add("Reserve_Id", ReserveId);
                    parms.Add("Claimant_Id", claimantId);
                    parms.Add("Claim_Id", claimId);

                    Reserve.FireEvent(myKey, parms);
                }
            }
        }

        public int NoNonReserveTransactions
        {
            /* Returns the number of non-reserve transactions for a reserve line.
             * This is used to determine if a reserve can be Voided.
            */
            get { return db.GetIntFromStoredProcedure("usp_Get_No_Non_Reserve_Transactions", this.Id); }
        }

        public void Reopen(double loss, double expense, List<Tuple<int, Hashtable>> eventTypeIds, int reserveid)
        {
            //Re-open reserve line
            int refzero = 0;

            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Open_Reserve;
            this.ReopenDate = ml.DBNow();
            this.GrossLossReserve = loss;
            this.NetLossReserve = loss;
            this.GrossExpenseReserve = expense;
            this.NetExpenseReserve = expense;
            this.ReserveSequenceNo += 1;
            this.isCaseReserve = true;
            this.Update();

            //issue reopen transactions
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Reopen_Reserve, true, reserveid); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Reopen_Reserve, false); //expense

            //insert history row
            this.UpdateHistory();

            //insert loss reserve transaction row
            if (loss > 0)
                this.InsertTransaction(loss, (int)modGeneratedEnums.TransactionType.Reserve, true);
                
            //insert expense reserve transaction row
            if (expense > 0)
                this.InsertTransaction(expense, (int)modGeneratedEnums.TransactionType.Reserve, false);

            //fire events to set locks
            if (eventTypeIds != null)
            {
                foreach (Tuple<int, Hashtable> EventIdsTuple in eventTypeIds)
                {
                    this.FireEvent(EventIdsTuple.Item1, EventIdsTuple.Item2 );
                }
            }

            //fire event notifying that the reserve is reopened
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Reopened);
        }

        public void Close(int denialReasonTypeId = 0)
        {
            //issue loss reserve offset
            if (this.NetLossReserve != 0)
                this.InsertOffsettingTransaction(Reserve.NetLossReserve * -1, true);

            //issue expense reseve offset
            if (this.NetExpenseReserve != 0)
                this.InsertOffsettingTransaction(Reserve.NetExpenseReserve * -1, false);

            //close reserve line
            this.NetLossReserve = 0;
            this.NetExpenseReserve = 0;
            this.GrossLossReserve = 0;
            this.GrossExpenseReserve = 0;
            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Closed;
            this.DateClosed = ml.DBNow();
            this.DenialReasonTypeId = denialReasonTypeId;
            this.Update();

            //issue closed transaction
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Reserve, true); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Reserve, false); //expense

            //insert history row
            this.UpdateHistory();

            //fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Closed);

            //sum transactions on closed reserve line.
            //if they don't equal zeron, something is wrong - send alert
            if (db.GetDblFromStoredProcedure("usp_Sum_Transactions_For_Reserve", this.Id) != 0)
                this.FireEvent((int)modGeneratedEnums.EventType.Transactions_Dont_Sum_to_Zero_on_Reserve);
        }

        public string CoverageDescription
        {
            //returns coverage description for a reserve
            get { return this.Claim.CoverageDescriptions(this.ReserveType.PolicyCoverageTypeId); }
        }

        public void ClosePendingSalvage()
        {
            //close reserve line pending salvage

            double netLoss, netExpense;

            if (this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
            {
                netLoss = Reserve.NetLossReserve;
                netExpense = Reserve.NetExpenseReserve;

                //issue loss reserve offset
                if (netLoss != 0)
                    this.InsertOffsettingTransaction(netLoss * -1, true);

                //issue expense reserve offset
                if (netExpense != 0)
                    this.InsertOffsettingTransaction(netExpense * -1, false);
            }

            //close reserve line pending salvage
            this.NetLossReserve = 0;
            this.NetExpenseReserve = 0;
            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage;
            this.DateClosedSalvage = ml.DBNow();
            this.Update();

            //insert history row
            this.UpdateHistory();

            //issue closed pending salvage transaction
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Salvage, true); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Salvage, false); //expense

            //fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Closed_Pending_Salvage);
        }

        public void ClosePendingSubro()
        {
            //close reserve line pending subrogation

            double netLoss, netExpense;

            if (this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
            {
                netLoss = Reserve.NetLossReserve;
                netExpense = Reserve.NetExpenseReserve;

                //issue loss reserve offset
                if (netLoss != 0)
                    this.InsertOffsettingTransaction(netLoss * -1, true);

                //issue expense reserve offset
                if (netExpense != 0)
                    this.InsertOffsettingTransaction(netExpense * -1, false);
            }

            //close reserve line pending subro
            this.NetLossReserve = 0;
            this.NetExpenseReserve = 0;
            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro;
            this.DateClosedSalvage = ml.DBNow();
            this.Update();

            //insert history row
            this.UpdateHistory();

            //issue closed pending subro transaction
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Subro, true); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Subro, false); //expense

            //fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Closed_Pending_Subro);
        }

        public void ClosePendingDeductibles()
        {
            //close reserve line pending deductibles

            double netLoss, netExpense;

            if (this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
            {
                netLoss = Reserve.NetLossReserve;
                netExpense = Reserve.NetExpenseReserve;

                //issue loss reserve offset
                if (netLoss != 0)
                    this.InsertOffsettingTransaction(netLoss * -1, true);

                //issue expense reserve offset
                if (netExpense != 0)
                    this.InsertOffsettingTransaction(netExpense * -1, false);
            }

            //close reserve line pending deductibles
            this.NetLossReserve = 0;
            this.NetExpenseReserve = 0;
            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Deductibles;
            this.DateClosedSalvage = ml.DBNow();
            this.Update();

            //insert history row
            this.UpdateHistory();

            //issue closed pending deductibles transaction
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Deductibles, true); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_Deductibles, false); //expense

            //fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Closed_Pending_Deductibles);
        }

        public void ClosePendingSIRRecovery()
        {
            //close reserve line pending SIR Recovery

            double netLoss, netExpense;

            if (this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
            {
                netLoss = Reserve.NetLossReserve;
                netExpense = Reserve.NetExpenseReserve;

                //issue loss reserve offset
                if (netLoss != 0)
                    this.InsertOffsettingTransaction(netLoss * -1, true);

                //issue expense reserve offset
                if (netExpense != 0)
                    this.InsertOffsettingTransaction(netExpense * -1, false);
            }

            //close reserve line pending SIR recovery
            this.NetLossReserve = 0;
            this.NetExpenseReserve = 0;
            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Closed_Pending_SIR_Recovery;
            this.DateClosedSalvage = ml.DBNow();
            this.Update();

            //insert history row
            this.UpdateHistory();

            //issue closed pending SIR recovery transaction
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_SIR_Recovery, true); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Close_SIR_Recovery, false); //expense

            //fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Closed_Pending_SIR_Recovery);
        }

        public void Void()
        {
            //voids reserve line

            double netLoss, netExpense;

            if (this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
            {
                netLoss = Reserve.NetLossReserve;
                netExpense = Reserve.NetExpenseReserve;

                //issue loss reserve offset
                if (netLoss != 0)
                    this.InsertOffsettingTransaction(netLoss * -1, true);

                //issue expense reserve offset
                if (netExpense != 0)
                    this.InsertOffsettingTransaction(netExpense * -1, false);
            }

            //void reserve line
            this.NetLossReserve = 0;
            this.NetExpenseReserve = 0;
            this.ReserveStatusId = (int)modGeneratedEnums.ReserveStatus.Void;
            this.DateClosedSalvage = ml.DBNow();
            this.Update();

            //issue void reserve transaction
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Void_Reserve, true); //loss
            this.InsertTransaction(0, (int)modGeneratedEnums.TransactionType.Void_Reserve, false); //expense

            //insert history row
            this.UpdateHistory();

            //fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Reserve_Voided);
        }

        public bool CoverageExists
        {
            //Does coverage exist for reserve line?
            get
            {
                return db.GetIntFromStoredProcedure("usp_Coverage_Exists", this.Id) > 0 ?
                true : false;
            }
        }

        public bool hasCoverage
        {
            //Does reserve line have corresponding Policy Coverage?
            get
            {
                DataTable a = db.ExecuteStoredProcedure("usp_Get_Policy_Coverage_For_Reserve", this.Id);
                return a.Rows.Count == 0 ? false : true;
            }
        }

        public void getPIPDateRange(ref DateTime minDate, ref DateTime maxDate)
        {
            DataTable c = db.ExecuteStoredProcedure("usp_get_Pip_Date_Range", this.Id);
            if (c.Rows.Count == 0)
                return;

            DataRow row = c.Rows[0];
            minDate = (DateTime)row["Min_Date"];
            maxDate = (DateTime)row["Max_Date"];
        }

        public double PerPersonCoverage
        {
            //returns per-person coverage amount for a reserve ID
            get
            {
                if (this.PolicyCoverage() == null)
                    return 0;

                return this.PolicyCoverage().PerPersonCoverage;
            }
        }

        public double PerAccidentCoverage
        {
            //returns per-accident coverage amount for a reserve ID

            get
            {
                if (this.PolicyCoverage() == null)
                    return 0;

                return this.PolicyCoverage().PerAccidentCoverage;
            }
        }

        public PolicyCoverage PolicyCoverage()
        {
            //returns Policy_Coverage row corresponding to reserveID

            DataTable c = db.ExecuteStoredProcedure("usp_Get_Policy_Coverage_For_Reserve", this.Id);

            return c.Rows.Count == 0 ? null : new PolicyCoverage(c, c.Rows[0]);
        }

        public bool hasDeductible
        {
            //Does reserve have a deductible?
            get { return this.PolicyCoverage() == null ? false : this.PolicyCoverage().hasDeductible; }
        }

        public int PerPersonNoPeriods
        {
            get { return this.PolicyCoverage() == null ? 0 : this.PolicyCoverage().PerPersonNoPeriods; }
        }

        public bool isPerPeriod
        {
            //Should reserve have per perid coverage (per-month, perday, etc.)?

            get
            {
                return db.GetBoolFromStoredProcedure("usp_Reserve_Is_Per_Period", this.Id) ?
                false : true;
            }
        }

        public string getPeriodName()
        {
            //if reserve has per-period coverage, returns period name (day, month, etc.)

            return this.PerPersonNoPeriods > 0 ? this.PolicyCoverage().PerPersonPeriodType().PeriodName : "Undefined";
        }

        public double UserAuthority()
        {
            return this.ReserveType.UserAuthority();
        }

        public double TotalLossPayments
        {
            get { return (double)db.GetIntFromStoredProcedure("usp_Get_Total_Loss_Payments_For_Reserve", this.Id); }
        }

        public double TotalExpensePayments
        {
            get { return (double)db.GetIntFromStoredProcedure("usp_Get_Total_Expense_Payments_For_Reserve", this.Id); }
        }

        public bool AuthorityExceeded(double amount, bool isLoss)
        {
            //Does reserve + amount exceed user authority?

            if (isLoss)
            {
                if (this.TotalLossPayments + amount > this.UserAuthority())
                    return true;
            }
            else
            {
                if (this.TotalExpensePayments + amount > this.UserAuthority())
                    return true;
            }
            return false;
        }

        public double LossAmount
        {
            //Returns total loss amount for a reserve line
            get { return db.GetDblFromStoredProcedure("usp_Get_Reserve_Loss_Amount", this.Id); }
        }

        public void ChangeNetLossReserve(ref double newAmount, Hashtable eventTypeIds = null)
        {
            //Changes net loss reserve amount

            double oldAmount;  //old net reserve amount
            Hashtable parms = new Hashtable(); //parms for firing event

            oldAmount = this.NetLossReserve;

            //issue Offsetting transaction
            this.InsertOffsettingTransaction(newAmount - oldAmount, true);

            //save new gross and net loss reserve
            this.NetLossReserve = newAmount;
            this.GrossLossReserve += newAmount - oldAmount;
            this.isCaseReserve = true;
            this.Update();

            //fire events
            if (eventTypeIds == null)
                return;

            if (eventTypeIds.Count > 0)
            {
                foreach(int eventTypeId in eventTypeIds.Keys)
                {
                    parms = (Hashtable)eventTypeIds[eventTypeId];
                    if (parms == null)
                        parms = new Hashtable();

                    parms.Add("New_Value", newAmount);
                    parms.Add("Old_Value", oldAmount);
                    parms.Add("Reserve_Id", ReserveId);
                    parms.Add("Claimant_Id", ClaimantId);
                    parms.Add("Claim_Id", ClaimId);

                    //fire event to lock reserve
                    this.FireEvent(eventTypeId, parms);
                }
            }
        }

        public int getNoUnprintedDrafts()
        {
            return this.getUnprintedDrafts().Count;
        }

        public List<Hashtable> getUnprintedDrafts()
        {
            //returns unprinted drafts for a reserve line
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Unprinted_Drafts_For_Reserve", this.Id);
        }

        public DataTable getReconRows(int ReserveId, int ClaimId)
        {
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            DateTime rightNow = DateTime.Now;

            if (ml.getQuarter(rightNow.Month) == 1)
            {
                startDate = new DateTime(rightNow.Year, 1, 1);
                endDate = new DateTime(rightNow.Year, 3, 31);
            }
            else if (ml.getQuarter(rightNow.Month) == 2)
            {
                startDate = new DateTime(rightNow.Year, 4, 1);
                endDate = new DateTime(rightNow.Year, 6, 30);
            }
            else if (ml.getQuarter(rightNow.Month) == 3)
            {
                startDate = new DateTime(rightNow.Year, 7, 1);
                endDate = new DateTime(rightNow.Year, 9, 30);
            }
            else
            {
                startDate = new DateTime(rightNow.Year, 10, 1);
                endDate = new DateTime(rightNow.Year, 12, 31);
            }
            return db.ExecuteStoredProcedure("usp_Get_ReserveRecon", ReserveId, ClaimId, startDate, endDate);
        }

        public InjuredCoverage InjuredCoverage()
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Injured_Coverage_For_Reserve", this.Id);
            if (rows.Rows.Count == 0)
                return null;
            return new InjuredCoverage(rows, rows.Rows[0]);
        }

        public Injured Injured()
        {
            InjuredCoverage ic = this.InjuredCoverage();
            if (ic == null)
                return null;
            return ic.Injured();
        }

        public ArrayList AssignedToUsers()
        {
            //returns all users to which this reserve is assigned
            ArrayList a = new ArrayList();
            foreach (int i in this.AssignedToUserIds())
            {
                a.Add(new User(i));
            }
            return a;
        }

        public bool isAssignedToUser(User user)
        {
            //Is this reserve assigned to user?
            foreach (User u in this.AssignedToUsers())
            {
                if (u.Id == user.Id)
                    return true;
            }
            return false;
        }

        public List<int> AssignedToUserIds()
        {
            //returns all user ids to which this reserve is assigned
            List<int> a = new List<int>();
            if (this.AdjusterId!=0)
                a.Add(this.AdjusterId);

            if(this.AssignedToUserGroupId!=0)
                a.AddRange(this.AssignedToUserGroup().UserIds());

            return a;
        }

        public override ArrayList UserTasks()
        {
            //returns user tasks for this reserve
            ArrayList a = new ArrayList();
            DataTable myUserTasks = db.ExecuteStoredProcedure("usp_Get_User_Tasks_For_Reserve", this.Id);

            foreach (DataRow row in myUserTasks.Rows)
            {
                a.Add(new UserTask(myUserTasks, row));
            }
            return a;
        }

        public PolicyCoverageType PolicyCoverageType()
        {
            return this.ReserveType.PolicyCoverageType;
        }

        public bool maxSupplementalPaymentsReached
        {
           get
           {
            return ((this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed) &&
                (MedCsXSystem.Settings.maxSupplementalPayments <= this.NoSupplementalPayments)) ?
                true : false;
           }
        }

        public bool isClosedPendingSubro
        {
            get { return this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro; }
        }

        public bool isClosedPendingSalvage
        {
            get { return this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage; }
        }

        public bool isClosed
        {
            get { return this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed; }
        }

        public bool isOpen
        {
            get { return this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve; }
        }

        public bool isVoid
        {
            get { return this.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Void; }
        }

        public double AveragePaidLoss(int claimCount = 0)
        {
            /* Returns the average paid loss and claim count for this type of reserve 
             * (as defined by Reserve_Type.Litigation_Group)
             * Use the past 12 months dataa on closed reserves within the same state.
            */

            DataTable rows = db.ExecuteStoredProcedure("usp_Avg_Loss_For_Reserve", this.Id);
            if (rows.Rows.Count == 0)
                return 0;

            DataRow row = rows.Rows[0];
            claimCount = (int)row["Nbr_Claims"];
            return (double)row["Avg_Paid"];
        }

        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_Reserve_Row";
        }

        public int InsertDeductibles(string RecoverySource, Int64 CheckNo, double Amount)
        {
            //Recovering deductibles
            //inserts a deductibles table row and according transaction.

            //Create new transaction
            Transaction tr = new Transaction();
            int dedId = 0;          //ID of newly inserted deductibles row
            int transactionId = 0;  //Transaction ID for deductibles

            //Insert deductibles row
            Deductibles dedRecovery = new Deductibles();
            dedRecovery.RecoverySource = RecoverySource;
            dedRecovery.CheckNo = CheckNo;
            dedId = dedRecovery.Update();

            //Insert transaction row
            tr.ReserveId = this.Id;
            tr.DeductiblesId = dedId;
            tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Deductibles;
            tr.TransactionAmount = Amount;

            TransactionType transactionType = new TransactionType(tr.TransactionTypeId);
            tr.isLoss = transactionType.isLoss;
            transactionId = tr.Update();

            //Fire deductibles inserted event - has to happen manually - after transaction has been inserted
            tr.FireEvent((int)modGeneratedEnums.EventType.Deductibles_Added);

            if (this.LossAmount < Amount)
                tr.FireEvent((int)modGeneratedEnums.EventType.Deductibles_Amount_Greater_Than_Reserve_Loss);
        
            //Insert offsetting transaction reserve
            return this.InsertOffsettingTransaction(Amount * -1, transactionType.isLoss);
        }

        public int InsertSIR(Int64 CheckNo, double Amount, bool isRecovery, double SIRAmount,
            double SIRAmountUsed, bool RecoveryExceedsPaymentsAmount)
        {
            //inserts a SIR table row and according transactions

            //Create new transaction
            Transaction tr = new Transaction();
            int SIRId = 0;
            int transactionId = 0;
            
            //Has SIR limit been reached?
            if (SIRAmount > 0)
            {
                if (((SIRAmountUsed + Amount) > (SIRAmount * MedCsXSystem.Settings.maxSIRBeforeAlert / 100)) &&
                    ((SIRAmountUsed + Amount) <= SIRAmount))
                {
                    //the new SIR amount used will be above a certatin percentage of the policy SIR amount

                    Hashtable parms = new Hashtable();
                    parms.Add("SIR_Amount_Used", ((SIRAmountUsed + Amount) / SIRAmount) * 100); //shown as percentage
                    parms.Add("SIR_Amount", SIRAmount);
                    parms.Add("Policy_no", this.Claim.PolicyNo);
                    parms.Add("Effective_Date", this.Claim.PolicyExpirationDate());

                    this.FireEvent((int)modGeneratedEnums.EventType.SIR_Used_Near_Policy_Limit, parms);
                }
                else if ((SIRAmountUsed + Amount) > SIRAmount)
                {
                    //the new SIR amount used will be over the policy SIR amount

                    Hashtable parms = new Hashtable();
                    parms.Add("Amount_Over_Limit", (SIRAmountUsed + Amount - SIRAmount)); 
                    parms.Add("SIR_Amount", SIRAmount);
                    parms.Add("Policy_no", this.Claim.PolicyNo);
                    parms.Add("Effective_Date", this.Claim.PolicyExpirationDate());
                    parms.Add("Expiration_Date", this.Claim.PolicyExpirationDate());

                    this.FireEvent((int)modGeneratedEnums.EventType.SIR_Used_Near_Policy_Limit, parms);
                }
            }
            else
            {
                //Policy SIR amount has not been set

                //System.Windows.MessageBox.Show("Policy SIR amount has not been set.", "No SIR Amount", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning, System.Windows.MessageBoxResult.OK);

                return 0;
            }

            //Insert Deductibles Row
            SIR mySIR = new SIR();
            mySIR.CheckNo = CheckNo;
            mySIR.isRecovery = isRecovery;
            SIRId = mySIR.Update();

            //Insert Transaction Row
            tr.ReserveId = this.Id;
            tr.TransactionAmount = Amount;
            if (isRecovery)
            {
                tr.SIRRecoveryId = SIRId;
                tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.SIR_Recovery;
            }
            else
            {
                tr.SIRCreditId = SIRId;
                tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.SIR_Credit;
            }

            TransactionType transactionType = new TransactionType();
            tr.isLoss = transactionType.isLoss;
            transactionId = tr.Update();

            //Fire SIR inserted event - after transaction has been inserted
            if (isRecovery)
                tr.FireEvent((int)modGeneratedEnums.EventType.SIR_Recovery_Added);
            else
                tr.FireEvent((int)modGeneratedEnums.EventType.SIR_Credit_Added);

            //Insert offsetting transaction reserve
            this.InsertOffsettingTransaction(Amount * -1, transactionType.isLoss);

            if (RecoveryExceedsPaymentsAmount)
            {
                if (isRecovery)
                {
                    //total recovered SIR plus the new recovered amount is greater than the amount paid on teh reserve.
                    tr.FireEvent((int)modGeneratedEnums.EventType.SIR_Recovery_Amount_Greater_Than_Reserve_Payments);
                }
            }
            return 1;
        }
        #endregion

        #region Nonimplemented
        //public override int ClaimantId
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}
        #endregion
    }
}

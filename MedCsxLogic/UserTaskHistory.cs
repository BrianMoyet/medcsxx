﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UserTaskHistory:MedCsXTable
    {

        #region Constructors
        public UserTaskHistory() : base() { }
        public UserTaskHistory(int id) : base(id) { }
        public UserTaskHistory(Hashtable row) : base(row) { }
        public UserTaskHistory(UserTask task)
            : base()
        {
            this.LockedByUserId = task.LockedByUserId;
            this.UserTaskId = task.Id;
        }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "User_Task_History"; }
        }

        public int AssignedToUserGroupId
        {
            get { return (int)row["Assigned_To_User_Group_Id"]; }
            set { this.setRowValue("Assigned_To_User_Group_Id", value); }
        }

        public int AssignedToUserId
        {
            get { return (int)row["Assigned_To_User_Id"]; }
            set { this.setRowValue("Assigned_To_User_Id", value); }
        }

        public int LockedByUserId
        {
            get { return (int)row["Locked_By_User_Id"]; }
            set { this.setRowValue("Locked_By_User_Id", value); }
        }

        public int UserTaskActionTypeId
        {
            get { return (int)row["User_Task_Action_Type_Id"]; }
            set { this.setRowValue("User_Task_Action_Type_Id", value); }
        }

        public int UserTaskHistoryId
        {
            get { return (int)row["User_Task_History_Id"]; }
            set { this.setRowValue("User_Task_History_Id", value); }
        }

        public int UserTaskId
        {
            get { return (int)row["User_Task_Id"]; }
            set { this.setRowValue("User_Task_Id", value); }
        }
        #endregion

        #region Misc Methods
        public static void ClearTask(UserTask task)
        {
            UserTaskHistory h = new UserTaskHistory(task);
            h.UserTaskActionTypeId = (int)modGeneratedEnums.UserTaskActionType.Task_Cleared;
            h.Update();
        }

        public static void CreateTask(UserTask task)
        {
            UserTaskHistory h = new UserTaskHistory(task);
            h.UserTaskActionTypeId = (int)modGeneratedEnums.UserTaskActionType.Task_Created;
            h.Update();
        }

        public static void LockTask(UserTask task)
        {
            UserTaskHistory h = new UserTaskHistory(task);
            h.UserTaskActionTypeId = (int)modGeneratedEnums.UserTaskActionType.Task_Locked;
            h.Update();
        }
        
        public static void ReassignTask(UserTask task)
        {
            UserTaskHistory h = new UserTaskHistory(task);
            h.UserTaskActionTypeId = (int)modGeneratedEnums.UserTaskActionType.Task_Reassigned;
            h.Update();
        }
        #endregion
    }
}

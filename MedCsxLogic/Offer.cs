﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class Offer:Demand
    {

        #region Constructors
        public Offer() : base() { }
        public Offer(int id) : base(id) { }
        public Offer(Hashtable row) : base(row) { }
        #endregion

        #region Class Variables
        private OfferSentType m_OfferSentType = null;
        private Demand m_Demand = null;
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Offer";
            }
        }

        public int OfferSentTypeId
        {
            get { return (int)row["Offer_Sent_Type_Id"]; }
            set { this.setRowValue("Offer_Sent_Type_Id", value); }
        }

        public double OfferAmount
        {
            get { return (double)((decimal)row["Offer_Amount"]); }
            set { this.setRowValue("Offer_Amount", value); }
        }

        public DateTime OfferDate
        {
            get { return (DateTime)row["Offer_Date"]; }
            set { this.setRowValue("Offer_Date", value); }
        }
        #endregion

        #region Methods
        public OfferSentType OfferSentType
        {
            get { return this.m_OfferSentType == null ? this.m_OfferSentType = new OfferSentType(this.OfferSentTypeId) : this.m_OfferSentType; }
        }

        public Demand Demand
        {
            get { return this.m_Demand == null ? this.m_Demand = new Demand(this.DemandId) : this.m_Demand; }
        }
        #endregion
    }
}

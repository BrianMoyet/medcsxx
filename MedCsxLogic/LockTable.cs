﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public abstract class LockTable:MedCsXTable
    {

        #region Constructors
        public LockTable() : base() { }
        public LockTable(int id) : base(id) { }
        public LockTable(Hashtable row) : base(row) { }
        public LockTable(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public bool ConditionStillExists
        {
            get { return (bool)row["Condition_Still_Exists"]; }
            set { this.setRowValue("Condition_Still_Exists", value); }
        }
       
        public bool isLocked
        {
            get { return (bool)row["Is_Locked"]; }
            set { this.setRowValue("Is_Locked", value); }
        }

        public DateTime LockDate
        {
            get { return (DateTime)row["Lock_Date"]; }
            set { this.setRowValue("Lock_Date", value); }
        }

        public int LockFileNoteId
        {
            get { return (int)row["Lock_File_Note_Id"]; }
            set { this.setRowValue("Lock_File_Note_Id", value); }
        }

        public int UnlockFileNoteId
        {
            get { return (int)row["Unlock_File_Note_Id"]; }
            set { this.setRowValue("Unlock_File_Note_Id", value); }
        }

        public DateTime UnlockDate
        {
            get { return (DateTime)row["Unlock_Date"]; }
            set { this.setRowValue("Unlock_Date", value); }
        }

        public int UnlockedBy
        {
            get { return (int)row["Unlocked_By"]; }
            set { this.setRowValue("Unlocked_By", value); }
        }
        #endregion

        #region Misc Methods
        public FileNote LockFileNote()
        {
            return new FileNote(this.LockFileNoteId);
        }

        public FileNote UnlockFileNote()
        {
            return new FileNote(this.UnlockFileNoteId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BodyPart:TypeTable
    {

        #region Constructors
        public BodyPart() : base() { }
        public BodyPart(int id) : base(id) { }
        public BodyPart(string description) : base(description) { }
        public BodyPart(Hashtable row) : base(row) { }
        #endregion
        
        #region Table Properties
        public override string TableName
        {
            get
            {
                return "Body_Part";
            }
        }
        #endregion
    }
}

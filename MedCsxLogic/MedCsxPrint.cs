﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Printing;
using System.IO;
using System.Drawing;

namespace MedCsxLogic
{


    public class MedCsxPrint : System.Drawing.Printing.PrintDocument
    {
        //private Font m_font;
        private TextReader m_streamReader;
        private readonly string m_fileName;
        private StreamReader m_stream;
        private StreamReader streamToPrint;
        private Font printFont;


        protected override void OnBeginPrint(System.Drawing.Printing.PrintEventArgs ev)
        {

            base.OnBeginPrint(ev);


            //string m_fileName = "C:\\GitCodeRepos\\medcsxx Integration\\MedCSX\\Documents\\Completed DocumentsKEY Contact Letter to Claimant-140644-12012020 014219-389415.docx";
            //if (m_fileName != null)
            //{

            //    StreamReader m_streamReader = new StreamReader(m_fileName);

            //}

            streamToPrint = new StreamReader
            ("C:\\GitCodeRepos\\medcsxx Integration\\MedCSX\\Documents\\Completed Documents\\KEY Contact Letter to Claimant-140644-12012020 014219-389415.docx");


            this.PrinterSettings.PrinterName = "PLAuto-M3560";
            

          

        }
        public MedCsxPrint()
        {

        }

        public MedCsxPrint(StreamReader stream)
        {
            m_stream = stream;

            streamToPrint = new StreamReader
              ("C:\\GitCodeRepos\\medcsxx Integration\\MedCSX\\Documents\\Completed Documents\\KEY Contact Letter to Claimant-140644-12012020 014219-389415.docx");
            //try
            //{
            //    // printFont = new Font("Arial", 10);
            //    PrintDocument pd = new PrintDocument();
            //    pd.PrintPage += new PrintPageEventHandler
            //       (this.pd_PrintPage);
                
            //    pd.Print();
            //}
            //finally
            //{
            //    streamToPrint.Close();
            //}
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }
    }
}
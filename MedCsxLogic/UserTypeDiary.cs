﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class UserTypeDiary:MedCsXTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public UserTypeDiary() : base() { }
        public UserTypeDiary(int id) : base(id) { }
        public UserTypeDiary(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "User_Type_Diary"; }
        }

        public int fromReserveDay
        {
            get { return (int)row["From_Reserve_Day"]; }
            set { this.setRowValue("From_Reserve_Day", value); }
        }

        public int maxDiaryDays
        {
            get { return (int)row["Max_Diary_Days"]; }
            set { this.setRowValue("Max_Diary_Days", value); }
        }

        public int toReserveDay
        {
            get { return (int)row["To_Reserve_Day"]; }
            set { this.setRowValue("To_Reserve_Day", value); }
        }

        public int UserTypeDiaryId
        {
            get { return (int)row["User_Type_Diary_Id"]; }
            set { this.setRowValue("User_Type_Diary_Id", value); }
        }

        public int UserTypeId
        {
            get { return (int)row["User_Type_Id"]; }
            set { this.setRowValue("User_Type_Id", value); }
        }
        #endregion

        public UserType UserType()
        {
            return new UserType(this.UserTypeId);
        }

        public static int MaxDiaryDaysFor(UserType userType, int daysReserveOpen)
        {
            //returns max diary days for user type and number of days reserve has been open
            return db.GetIntFromStoredProcedure("usp_Max_Diary_Days_For_User_Type_And_Nbr_Days", userType.Id, daysReserveOpen);
        }

        #region
        #endregion
        #region
        #endregion


    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Subro:TransactionReferenceTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public Subro() : base() { }
        public Subro(int id) : base(id) { }
        public Subro(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Subro"; }
        }

        public Int64 CheckNo
        {
            get { return (Int64)row["Check_No"]; }
            set { this.setRowValue("Check_No", value); }
        }

        public bool isVoid
        {
            get { return (bool)row["Is_Void"]; }
            set { this.setRowValue("Is_Void", value); }
        }

        public string RecoverySource
        {
            get { return (string)row["Recovery_Source"]; }
            set { this.setRowValue("Recovery_Source", value); }
        }

        public int ReissueFromSubroId
        {
            get { return (int)row["Reissued_From_Subro_Id"]; }
            set { this.setRowValue("Reissued_From_Subro_Id", value); }
        }

        public int SubroId
        {
            get { return (int)row["Subro_Id"]; }
            set { this.setRowValue("Subro_Id", value); }
        }

        public int SubroSalvageVoidReasonId
        {
            get { return (int)row["Subro_Salvage_Void_Reason_Id"]; }
            set { this.setRowValue("Subro_Salvage_Void_Reason_Id", value); }
        }
        #endregion

        #region Misc Methods
        public int TransactionId
        {
            //returns transaction ID for salvage
            get { return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Subro_Id", this.Id); }
        }

        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }

        public static int InsertSubro(Hashtable subro, int reserveId, double Amount)
        {
            /*inserts subro and transaction rows and returns transaction ID of the newly inserted Transaction row.
             * subro - a Subro row dictionary
             * reserveId - the Reserve_Id to add the subro
            */

            Reserve res = new Reserve(reserveId);
            Transaction tr = new Transaction();
            int subroId; //ID of newly inserted row
            int transactionId; //transaction ID for subro

            try
            {
                //insert Subro row
                subroId = ml.InsertRow(subro, "Subro");

                //insert Transaction row
                tr.ReserveId = reserveId;
                tr.SubroId = subroId;
                tr.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Subro;
                tr.TransactionAmount = Amount;

                TransactionType trType = new TransactionType((int)modGeneratedEnums.TransactionType.Subro);
                tr.isLoss = trType.isLoss;
                transactionId = tr.Update();

                //fire Subro inserted event - has to happen manually - after transaction has been inserted
                tr.FireEvent((int)modGeneratedEnums.EventType.Subro_Added);

                if (res.LossAmount < Amount)
                    tr.FireEvent((int)modGeneratedEnums.EventType.Subro_Amount_Greater_Than_Reserve_Loss);
                //fire event warning about Subro being greater than Loss
                
                //insert offsetting transaction reserve
                return res.InsertOffsettingTransaction(-1.0 * Amount, trType.isLoss);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ReserveHistory : TypeTable
    {

        private static modLogic ml = new modLogic();
        #region Contructors
        public ReserveHistory()
            : base()
        {
            this.StatusCloseDate = ml.DEFAULT_DATE;
        }
        public ReserveHistory(int id) : base(id) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Reserve_History";
            }
        }

        public int ReserveHistoryId
        {
            get { return (int)row["Reserve_History_Id"]; }
            set { this.setRowValue("Reserve_History_Id", value); }
        }

        public int ReserveId
        {
            get { return (int)row["Reserve_Id"]; }
            set { this.setRowValue("Reserve_Id", value); }
        }

        public int ReserveStatusId
        {
            get { return (int)row["Reserve_Status_Id"]; }
            set { this.setRowValue("Reserve_Status_Id", value); }
        }

        public bool IsCurrentStatus
        {
            get { return (bool)row["Is_Current_Status"]; }
            set { this.setRowValue("Is_Current_Status", value); }
        }

        public DateTime StatusOpenDate
        {
            get { return (DateTime)row["Status_Open_Date"]; }
            set { this.setRowValue("Status_Open_Date", value); }
        }

        public DateTime StatusCloseDate
        {
            get { return (DateTime)row["Status_Close_Date"]; }
            set { this.setRowValue("Status_Close_Date", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class DiaryType:TypeTable
    {

        #region Constructors
        public DiaryType() : base() { }
        public DiaryType(int id) : base(id) { }
        public DiaryType(string description) : base(description) { }
        public DiaryType(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Diary_Type";
            }
        }

        #region Column Names
        public bool AllowManualClear
        {
            get { return (bool)row["Allow_Manual_Clear"]; }
            set { this.setRowValue("Allow_Manual_Clear", value); }
        }

        public int DiaryTypeId
        {
            get { return (int)row["Diary_Type_Id"]; }
            set { this.setRowValue("Diary_Type_Id", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using MedCsxDatabase;

namespace MedCsxLogic
{
    /// <summary>
    /// Returns City and State for valid zip code
    /// </summary>
   public class ZipCodeLookUp:MedCsXTable 
   {

       #region Class Variables
       private string m_zip5;
       private string m_zip4;

       public string zCity = "";
       public string zState = "";

       private static clsDatabase db = new clsDatabase();
       #endregion

       #region Class Initialization
       public ZipCodeLookUp(string postalCode, string frmState="")
       {
           ValidateZip(postalCode, frmState);
       }
       #endregion
       
       #region Column Names
       public override string TableName
       {
           get { return "ZipCode"; }
       }

       public string City
       {
           get { return (string)row["City"]; }
           set { this.setRowValue("City", value); }
       }

       public string State
       {
           get { return (string)row["State"]; }
           set { this.setRowValue("State", value); }
       }
       
       public string Zip
       {
           get { return (string)row["Zip_Code"]; }
           set { this.setRowValue("Zip_Code", value); }
       }
       #endregion

       #region Methods
       private void ValidateZip(string postalCode, string thisState = "")
       {
           try
           {
               if (thisState == "FA")
                   return;

               //valid US Zip code is 99999-9999
               if (postalCode.Length > 5)
               {    //if valid will be of type Zip+4
                   string[] zip = postalCode.Split('-');
                   if (zip.Length <= 2)
                   {
                       if ((postalCode.Length == 9) && (zip.Length == 1))
                       {
                           postalCode = postalCode.Substring(0, 5) + "-" + postalCode.Substring(5, 4);
                           ValidateZip(postalCode);
                           return;
                       }

                       if (zip[0].Length == 5)
                       {
                           int zip4;
                           if ((int.TryParse(zip[1], out zip4)) || (zip[1] == ""))
                           {
                               m_zip4 = zip[1];
                               ValidateZip(zip[0]);
                               return;
                           }
                       }
                   }
               }
               else if (postalCode.Length == 5)
               {
                   int zip;
                   if (int.TryParse(postalCode, out zip))
                   {
                       m_zip5 = postalCode.ToString();
                       if (GetZipCode(Convert.ToInt32(postalCode)) == 0)
                           PostalCall(postalCode);
                       return;
                   }
               }

               //MessageBox.Show("The Zip Code entered is not valid.", "Invalid Zip Code", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       private void PostalCall(string zip5)
       {
           try
           {
               XDocument doc = new XDocument(
                   new XElement("CityStateLookupRequest",
                       new XAttribute("USERID", "092KEYIN5882"),
                       new XElement("ZipCode",
                           new XAttribute("ID", "0"),
                           new XElement("Zip5", zip5))
                           )
                   );
               string strUrl = System.Configuration.ConfigurationManager.AppSettings["PostalServicePath"];
               strUrl += doc;
               PstMaster(strUrl);
               UpdateZipCode();
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       private void PstMaster(string strUrl)
       {
           try
           {
               HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(strUrl));
               request.Method = "POST";
               request.ContentType = "application/xml";
               request.Accept = "application/xml";

               HttpWebResponse response = (HttpWebResponse)request.GetResponse();
               Stream dataStream = response.GetResponseStream();
               XDocument xResponse = XDocument.Load(dataStream);
               var xCity = xResponse.Descendants("City");
               var xState = xResponse.Descendants("State");

               foreach (var sCity in xCity)
               {
                   zCity = sCity.Value;
                   break;
               }
               foreach (var sState in xState)
               {
                   zState = sState.Value;
                   break;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       private int GetZipCode(int postalCode)
       {
           string sql = "SELECT * FROM ZipCode WHERE Zip_Code=" + postalCode.ToString();
           List<Hashtable> list = db.ExecuteSelectReturnHashList(sql);
           foreach (Hashtable zData in list)
           {
               this.zCity = (string)zData["City"];
               this.zState = (string)zData["State"];
           }
           return list.Count;
       }

       private void  UpdateZipCode(){
           this.City = this.zCity;
           this.State = this.zState;
           this.Zip = this.m_zip5;
           this.Update();
       }
       #endregion
   }
}

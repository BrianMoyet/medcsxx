﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Transaction:ReserveTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Class Variables
        private int m_ClaimantId = 0;
        private Draft m_Draft = null;
        #endregion

        #region Constructors
        public Transaction() : base() { }
        public Transaction(int id) : base(id) { }
        public Transaction(Hashtable row) : base(row) { }
        public Transaction(DataTable Table, DataRow Row) : base(Table, Row) { }
        #endregion

        #region Methods
        public static int getReserveIdForTransactionId(int transactionId)
        {
            //Returns reserveID for given transactionID
            Transaction tr = new Transaction(transactionId);
            return tr.ReserveId;
        }

        public static int getClaimIdForTransactionId(int transactionId)
        {
            //Returns claimID for given transactionID
            Transaction tr = new Transaction(transactionId);
            return tr.ClaimId;
        }

        public static string TransactionDescription(int transactionId)
        {
            Transaction tr = new Transaction(transactionId);
            return tr.Description();
        }

        public static int VoidDraft(int transactionId, int voidReasonId)
        {
            //Voids draft for passed transactionID and void reason ID
            //Returns transaction ID of voided transaction

            Transaction tr = new Transaction(transactionId);
            Draft dr = tr.Draft;
            Reserve res = tr.Reserve;
            int voidDR = 0;

            //Exit if draft is already voided
            if (dr.isVoid)
                return 0;

            //Update draft row
            dr.isVoid = true;
            dr.DraftVoidReasonId = voidReasonId;
            dr.Update();

            double amount = (double)tr.TransactionAmount;
            int vdr = (int)tr.DraftId;

            ////Issue Void transaction
            //Transaction voidTR = new Transaction();
            //voidTR.TransactionDate = ml.DBNow();
            //voidTR.TransactionTypeId = (int)modGeneratedEnums.TransactionType.Void_Draft;
            //voidTR.TransactionAmount = amount * -1; //double.Parse((tr.TransactionAmount * -1.00).ToString());
            //voidTR.UserId = MedCsXSystem.UserId;
            //voidTR.VoidDraftId = vdr;
            //voidTR.ReserveId = tr.ReserveId;
            //voidTR.isLoss = tr.isLoss;
            //voidDR = voidTR.Update();

            Hashtable voidTR = new Hashtable();
            //Issue Void transaction
            voidTR["Transaction_Date"] = ml.DBNow();
            voidTR["Transaction_Type_Id"] = (int)modGeneratedEnums.TransactionType.Void_Draft;
            voidTR["Transaction_Amount"] = tr.TransactionAmount * -1;
            voidTR["User_Id"] = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            voidTR["Void_draft_Id"] = tr.DraftId;
            voidTR["Reserve_Id"] = tr.ReserveId;
            voidTR["Is_Loss"] = tr.isLoss;
            voidDR = ml.InsertRow(voidTR, "Transaction");



            //Fire event for draft voided
            tr.FireEvent((int)modGeneratedEnums.EventType.Draft_Voided);

            //Change paid loss/expense
            if (tr.isLoss)
                res.PaidLoss += tr.TransactionAmount;
            else
                res.PaidExpense += tr.TransactionAmount;

            //if reserve line is open and the same line as voided draft, change net reserve balance
            if ((res.isOpen) && (tr.ReserveSequenceNo == res.ReserveSequenceNo))
            {
                if (tr.isLoss)
                    res.NetLossReserve -= tr.TransactionAmount;
                else
                    res.NetExpenseReserve -= tr.TransactionAmount;
            }
            else
            {
                //issue offsetting reserve transaction
                voidDR = res.InsertOffsettingTransaction(tr.TransactionAmount, tr.isLoss);
            }
            res.Update();

            return voidDR;
        }

      

        public static int VoidSubro(int transactionId, int voidReasonId)
        {
            //Voids Subro for passed transactionID and void reason ID
            //Returns transaction ID of voided transaction

            Transaction tr = new Transaction(transactionId);
            Hashtable subro = new Hashtable();
            Hashtable newSubro = new Hashtable();
            Hashtable voidTR = new Hashtable();
            Reserve res = tr.Reserve;
        
            //get row dictionaries
            subro = db.GetRow("Subro", tr.SubroId);
            newSubro = ml.copyDictionary(subro);

            //Exit if subro is already voided
            if ((bool)subro["Is_Void"])
                return 0;

            //Update Subro row
            newSubro["Is_Void"] = true;
            newSubro["Subro_Salvage_Void_Reason_Id"] = voidReasonId;
            ml.updateRow("Subro", newSubro, subro);

            //Issue Void transaction
            voidTR["Transaction_Date"] = ml.DBNow();
            voidTR["Transaction_Type_Id"] = (int)modGeneratedEnums.TransactionType.Void_Subro;
            voidTR["Transaction_Amount"] = tr.TransactionAmount * -1;
            voidTR["User_Id"] = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            voidTR["Void_Subro_Id"] = tr.SubroId;
            voidTR["Reserve_Id"] = tr.ReserveId;
            voidTR["Is_Loss"] = tr.isLoss;
            ml.InsertRow(voidTR, "Transaction");
            
            //Fire event for subro voided
            tr.FireEvent((int)modGeneratedEnums.EventType.Subro_Voided);

           
            //issue offsetting reserve transaction
            return res.InsertOffsettingTransaction(tr.TransactionAmount, tr.isLoss);
        }

        public static int VoidSalvage(int transactionId, int voidReasonId)
        {
            //Voids Salvage for passed transactionID and void reason ID
            //Returns transaction ID of voided transaction

            Transaction tr = new Transaction(transactionId);
            Hashtable salv = new Hashtable();
            Hashtable newSalv = new Hashtable();
            Hashtable voidTR = new Hashtable();
            Reserve res = tr.Reserve;

            //get row dictionaries
            salv = db.GetRow("Salvage", tr.SalvageId);
            newSalv = ml.copyDictionary(salv);

            //Exit if salvage is already voided
            if ((bool)salv["Is_Void"])
                return 0;

            //Update Salvage row
            newSalv["Is_Void"] = true;
            newSalv["Subro_Salvage_Void_Reason_Id"] = voidReasonId;
            ml.updateRow("Salvage", newSalv, salv);

            //Issue Void transaction
            voidTR["Transaction_Date"] = ml.DBNow();
            voidTR["Transaction_Type_Id"] = (int)modGeneratedEnums.TransactionType.Void_Salvage;
            voidTR["Transaction_Amount"] = tr.TransactionAmount * -1;
            voidTR["User_Id"] = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            voidTR["Void_Salvage_Id"] = tr.SalvageId;
            voidTR["Reserve_Id"] = tr.ReserveId;
            voidTR["Is_Loss"] = tr.isLoss;
            ml.InsertRow(voidTR, "Transaction");

            //Fire event for salvage voided
            tr.FireEvent((int)modGeneratedEnums.EventType.Salvage_Voided);


            //issue offsetting reserve transaction
            return res.InsertOffsettingTransaction(tr.TransactionAmount, tr.isLoss);
        }

        public static int VoidDeductibles(int transactionId, int voidReasonId)
        {
            //Voids Deductibles for passed transactionID and void reason ID
            //Returns transaction ID of voided transaction

            Transaction tr = new Transaction(transactionId);
            Hashtable ded = new Hashtable();
            Hashtable newDed = new Hashtable();
            Hashtable voidTR = new Hashtable();
            Reserve res = tr.Reserve;
        
            //get row dictionaries
            ded = db.GetRow("Deductibles", tr.DeductiblesId);
            newDed = ml.copyDictionary(ded);

            //Exit if Deductibles is already voided
            if ((bool)ded["Is_Void"])
                return 0;

            //Update Deductibles row
            newDed["Is_Void"] = true;
            newDed["Subro_Salvage_Void_Reason_Id"] = voidReasonId;
            ml.updateRow("Deductibles", newDed, ded);

            //Issue Void transaction
            voidTR["Transaction_Date"] = ml.DBNow();
            voidTR["Transaction_Type_Id"] = (int)modGeneratedEnums.TransactionType.Void_Deductibles;
            voidTR["Transaction_Amount"] = tr.TransactionAmount * -1;
            voidTR["User_Id"] = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            voidTR["Void_Deducibles_Id"] = tr.DeductiblesId;
            voidTR["Reserve_Id"] = tr.ReserveId;
            voidTR["Is_Loss"] = tr.isLoss;
            ml.InsertRow(voidTR, "Transaction");

            //Fire event for deductibles voided
            tr.FireEvent((int)modGeneratedEnums.EventType.Deductibles_Voided);


            //issue offsetting reserve transaction
            return res.InsertOffsettingTransaction(tr.TransactionAmount, tr.isLoss);
        }       
        
        public static int VoidSIRRecovery(int transactionId, int voidReasonId)
        {
            //Voids SIR recovery for passed transactionID and void reason ID
            //Returns transaction ID of voided transaction

            Transaction tr = new Transaction(transactionId);
            Hashtable SIR = new Hashtable();
            Hashtable newSIR = new Hashtable();
            Hashtable voidTR = new Hashtable();
            Reserve res = tr.Reserve;
        
            //get row dictionaries
            SIR = db.GetRow("SIR", tr.SIRRecoveryId);
            newSIR = ml.copyDictionary(SIR);

            //Exit if SIR is already voided
            if ((bool)SIR["Is_Void"])
                return 0;

            //Update SIR row
            newSIR["Is_Void"] = true;
            newSIR["Subro_Salvage_Void_Reason_Id"] = voidReasonId;
            ml.updateRow("SIR", newSIR, SIR);

            //Issue Void transaction
            voidTR["Transaction_Date"] = ml.DBNow();
            voidTR["Transaction_Type_Id"] = (int)modGeneratedEnums.TransactionType.Void_SIR_Recovery;
            voidTR["Transaction_Amount"] = tr.TransactionAmount * -1;
            voidTR["User_Id"] = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            voidTR["Void_SIR_Recovery_Id"] = tr.SIRRecoveryId;
            voidTR["Reserve_Id"] = tr.ReserveId;
            voidTR["Is_Loss"] = tr.isLoss;
            ml.InsertRow(voidTR, "Transaction");
            
            //Fire event for SIR Recovery voided
            tr.FireEvent((int)modGeneratedEnums.EventType.SIR_Recovery_Voided);

           
            //issue offsetting reserve transaction
            return res.InsertOffsettingTransaction(tr.TransactionAmount, tr.isLoss);
        }

        public static int VoidSIRCredit(int transactionId, int voidReasonId)
        {
            //Voids SIR Credit for passed transactionID and void reason ID
            //Returns transaction ID of voided transaction

            Transaction tr = new Transaction(transactionId);
            Hashtable SIR = new Hashtable();
            Hashtable newSIR = new Hashtable();
            Hashtable voidTR = new Hashtable();
            Reserve res = tr.Reserve;
            int voidSub = 0;

            //get row dictionaries
            SIR = db.GetRow("SIR", tr.SIRCreditId);
            newSIR = ml.copyDictionary(SIR);

            //Exit if SIR is already voided
            if ((bool)SIR["Is_Void"])
                return 0;

            //Update SIR row
            newSIR["Is_Void"] = true;
            newSIR["Subro_Salvage_Void_Reason_Id"] = voidReasonId;
            ml.updateRow("SIR", newSIR, SIR);

            //Issue Void transaction
            voidTR["Transaction_Date"] = ml.DBNow();
            voidTR["Transaction_Type_Id"] = (int)modGeneratedEnums.TransactionType.Void_SIR_Credit;
            voidTR["Transaction_Amount"] = tr.TransactionAmount * -1;
            voidTR["User_Id"] = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            voidTR["Void_SIR_Credit_Id"] = tr.SIRCreditId;
            voidTR["Reserve_Id"] = tr.ReserveId;
            voidTR["Is_Loss"] = tr.isLoss;
            ml.InsertRow(voidTR, "Transaction");

            //Fire event for SIR Credit voided
            tr.FireEvent((int)modGeneratedEnums.EventType.SIR_Credit_Voided);


            //issue offsetting reserve transaction
            return res.InsertOffsettingTransaction(tr.TransactionAmount, tr.isLoss);
        }

        public static List<Hashtable> getTransactions(int reserveId, bool includeLoss = true, bool includeExpense = true)
        {
            /*Returns collection of transaction rows for a reserveID.  
             * includeLoss = true - include loss transactions
             * includeExpense = true - include expense transactions
            */

            List<Hashtable> c = new List<Hashtable>();
            List<Hashtable> rows = db.ExecuteStoredProcedureReturnHashList("usp_Get_Transactions_For_Reserve",reserveId);

            foreach(Hashtable transaction in rows)
            {
                if (((string)transaction["expense_loss"] == "Loss") && (includeLoss))
                    c.Add(transaction);
                if (((string)transaction["expense_loss"] == "Expense") && (includeExpense))
                    c.Add(transaction);
            }
            return c;
        }

        public override string TableName
        {
            get { return "Transaction"; }
        }

        public int DraftId
        {
            get { return (int)row["Draft_Id"]; }
            set { 
                this.setRowValue("Draft_Id", value);
                m_Draft = null;
            }
        }

        public int SubroId
        {
            get { return (int)row["Subro_Id"]; }
            set { this.setRowValue("Subro_Id", value); }
        }
        
        public int SalvageId
        {
            get { return (int)row["Salvage_Id"]; }
            set { this.setRowValue("Salvage_Id", value); }
        }
        
        public int DeductiblesId
        {
            get { return (int)row["Deductibles_Id"]; }
            set { this.setRowValue("Deductibles_Id", value); }
        }

        public int SIRRecoveryId
        {
            get { return (int)row["SIR_Recovery_Id"]; }
            set { this.setRowValue("SIR_Recovery_Id", value); }
        }

        public int SIRCreditId
        {
            get { return (int)row["SIR_Credit_Id"]; }
            set { this.setRowValue("SIR_Credit_Id", value); }
        }

        public int VoidDraftId
        {
            get { return (int)row["Void_Draft_Id"]; }
            set { this.setRowValue("Void_DraftId", value); }
        }

        public int VoidSubroId
        {
            get { return (int)row["Void_Subro_Id"]; }
            set { this.setRowValue("Void_Subro_Id", value); }
        }

        public int VoidSalvageId
        {
            get { return (int)row["Void_Salvage_Id"]; }
            set { this.setRowValue("Void_Salvage_Id", value); }
        }

        public int VoidDeductiblesId
        {
            get { return (int)row["Void_Deductibles_Id"]; }
            set { this.setRowValue("Void_Deductibles_Id", value); }
        }

        public int VoidSIRRecoveryId
        {
            get { return (int)row["Void_SIR_Recovery_Id"]; }
            set { this.setRowValue("Void_SIR_Recovery_Id", value); }
        }

        public int VoidSIRCreditId
        {
            get { return (int)row["Void_SIR_Credit_Id"]; }
            set { this.setRowValue("Void_SIR_Credit_Id", value); }
        }

        public DateTime TransactionDate
        {
            get { return (DateTime)row["Transaction_Date"]; }
            set { this.setRowValue("Transaction_Date", value); }
        }

        public int TransactionTypeId
        {
            get { return (int)row["Transaction_Type_Id"]; }
            set { this.setRowValue("Transaction_Type_Id", value); }
        }

        public double TransactionAmount
        {
            get { return (double)((decimal)row["Transaction_Amount"]); }
            set { this.setRowValue("Transaction_Amount", value); }
        }
        
        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }
        
        public bool isLoss
        {
            get { return (bool)row["Is_Loss"]; }
            set { this.setRowValue("Is_Loss", value); }
        }

        public bool isSupplemental
        {
            get { return (bool)row["Is_Supplemental"]; }
            set { this.setRowValue("Is_Supplemental", value); }
        }

        public int ReserveSequenceNo
        {
            get { return (int)row["Reserve_Sequence_No"]; }
            set { this.setRowValue("Reserve_Sequence_No", value); }
        }

        public override int ClaimantId
        {
            get { return (int)row["Claimant_Id"]; }
            set { this.setRowValue("Claimant_Id", value); }
        }

        public override int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public Draft Draft
        {
            get {
                if (m_Draft == null)
                    m_Draft = new Draft(DraftId);
                return m_Draft;
            }
        }

        public TransactionType TransactionType()
        {
            return new TransactionType(this.TransactionTypeId);
        }

        public Subro Subro()
        {
            return new Subro(this.SubroId);
        }

        public Salvage Salvage()
        {
            return new Salvage(this.SalvageId);
        }

        public int FirstLockId
        {
            //returns first active transaction lock for a transaction
            get { return db.GetIntFromStoredProcedure("usp_Get_First_Transaction_Lock", this.Id); }
        }

        public int InsertFileNote(int fileNoteTypeId, string fileNoteText)
        {
            //Inserts file note for transaction and returns new file note id

            FileNote f = new FileNote();
            f.FileNoteTypeId = fileNoteTypeId;
            f.FileNoteText = fileNoteText;
            f.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            f.ClaimId = this.ClaimId;
            f.ClaimantId = this.ClaimantId;
            f.ReserveId = this.ReserveId;
            f.TransactionId = this.Id;
            return f.Update();
        }

        public TransactionLock LatestLockForEventType(int eventTypeId)
        {
            int transactionLockId = db.GetIntFromStoredProcedure("usp_Get_Latest_Transaction_Lock_For_Event_Type", this.Id, eventTypeId);
            return transactionLockId == 0 ? null : new TransactionLock(transactionLockId);
        }

        public bool SyncEventExists(ref int eventTypeId)
        {
            //Does Transaction_Sync_Event row exist for event type?
            return db.GetBoolFromStoredProcedure("usp_Transaction_Sync_Event_Exists", this.Id, eventTypeId);
        }

        public void Lock(int fileNoteId)
        {
            //Locks transaction
            TransactionLock tl = new TransactionLock();

            tl.TransactionId = this.Id;
            tl.isLocked = true;
            tl.LockFileNoteId = fileNoteId;
            tl.LockDate = ml.DBNow();
            tl.ConditionStillExists = true;
            tl.Update();
        }

        public bool FireEvent(int eventTypeId, Hashtable parms = null)
        {
            //Fires event which is relevant to a transaction (Draft voided, Subro voided, etc.)
            //eventTypeId is the event type

<<<<<<< HEAD
            EventType et = new EventType(eventTypeId);
=======
<<<<<<< HEAD
            EventType et = new EventType(eventTypeId);
=======
            EventType et = new EventType();
>>>>>>> 5ff95f612c3e333f551085e8166258a3f3978899
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
            ScriptParms sp = new ScriptParms(parms);
            sp.GenerateParms(this);

            return et.FireEvent(sp.Parms) ? true : false;
        }

        public bool FireEventIfSyncExists(int checkEventTypeId, int eventTypeId)
        {
            //Fires transaction event if sync exists for other event

            return this.SyncEventExists(ref checkEventTypeId) ? this.FireEvent(eventTypeId) : false;
        }

        public void ClearEvent(int eventTypeId)
        {
            /*Clears event for a transaction.  
             * Delete any Alerts associated with this event.
             * Inactivate any Locks associated with event.
             * Set their Condition_Exists column to false
            */

            int fileNoteId;

            db.ExecuteStoredProcedure("usp_Delete_Transaction_Alerts_For_Event", this.Id, eventTypeId);
            EventType et = new EventType();
            DataTable myTransactionLocks = db.ExecuteStoredProcedure("usp_Get_Transaction_Locks_For_Event_Type", this.Id, eventTypeId);

            foreach (DataRow row in myTransactionLocks.Rows)
            {
                TransactionLock tl = new TransactionLock(myTransactionLocks, row);

                if (tl.isLocked)
                {
                    //transaction is locked - unlock it
                    fileNoteId = tl.Transaction().InsertFileNote((int)modGeneratedEnums.FileNoteType.Unlock_Transaction, "System Unlocked " + tl.Transaction().Description() + " Because Condition '" + et.Description + "' No Longer Exists.");

                    tl.isLocked = false;
                    tl.UnlockFileNoteId = fileNoteId;
                    tl.UnlockDate = ml.DBNow();
                    tl.ConditionStillExists = false;
                    tl.Update();
                }
                else
                {
                    //transaction is not locked
                    tl.ConditionStillExists = false;
                    tl.Update();
                }
            }
        }

        public string Description()
        {
            //returns a transaction description
            string msg;
            msg = this.TransactionType().Description + " Of ";

            if (this.DraftId != 0)
                msg += this.Draft.DraftAmount.ToString("c") + " To " + this.Draft.Payee;

            if (this.SubroId != 0)
                msg += this.TransactionAmount.ToString("c") + " From " + this.Subro().RecoverySource;

            if (this.SalvageId != 0)
                msg += this.TransactionAmount.ToString("c") + " From " + this.Salvage().Vendor().Name;

            if ((this.DraftId == 0) && (this.SubroId == 0) && (this.SalvageId == 0))
                msg += this.TransactionAmount.ToString("c");

            return msg;
        }

        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            this.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            this.TransactionDate = ml.DBNow();
            return base.Update(parms, fireEvents);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class Recon:MedCsXTable
   {


       #region Constructors
       public Recon() : base() { }
       public Recon(int id) : base(id) { }
       public Recon(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get { return "Recon"; }
       }

       public int ClaimId
       {
           get { return (int)row["Claim_Id"]; }
           set { this.setRowValue("Claim_Id", value); }
       }

       public Claim Claim
       {
           get { return new Claim(ClaimId); }
       }

       public int ReconId
       {
           get { return (int)row["Recon_Id"]; }
           set { this.setRowValue("Recon_Id", value); }
       }
      
       public double ReserveAfterRecon
       {
           get { return (double)((decimal)row["Reserve_After_Recon"]); }
           set { this.setRowValue("Reserve_After_Recon", value); }
       }

       public double ReserveAtRecon
       {
           get { return (double)((decimal)row["Reserve_At_Recon"]); }
           set { this.setRowValue("Reserve_At_Recon", value); }
       }

       public double ReserveDifference
       {
           get { return (double)((decimal)row["Reserve_Difference"]); }
           set { this.setRowValue("Reserve_Difference", value); }
       }
      
       public int ReserveId
       {
           get { return (int)row["Reserve_Id"]; }
           set { this.setRowValue("Reserve_Id", value); }
       }

       public Reserve Reserve
       {
           get { return new Reserve(ReserveId); }
       }

       public int UserId
       {
           get { return (int)row["User_Id"]; }
           set { this.setRowValue("User_Id", value); }
       }

       public User User
       {
           get { return new User(UserId); }
       }
       #endregion
   }
}

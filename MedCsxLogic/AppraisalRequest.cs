﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class AppraisalRequest:ClaimTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Class Variables
        private ArrayList m_AppraisalTasks = null;
        #endregion


        #region Constructors
        public AppraisalRequest() : base() { }
        public AppraisalRequest(int id)
            : base(id)
        {
            this.m_AppraisalTasks = new ArrayList();
            //reads in appraisal tasks corresponding to appraisal request ID
            DataTable tasks = db.ExecuteStoredProcedure("usp_Get_Appraisal_Tasks_For_Request", this.Id);
            foreach (DataRow row in tasks.Rows)
                this.m_AppraisalTasks.Add(new AppraisalTask(tasks, row));
        }
        public AppraisalRequest(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int AppraisalRequestId
        {
            get { return (int)row["Appraisal_Request_Id"]; }
            set { this.setRowValue("Appraisal_Request_Id", value); }
        }

        public int AppraisalRequestStatusId
        {
            get { return (int)row["Appraisal_Request_Status_Id"]; }
            set { this.setRowValue("Appraisal_Request_Status_Id", value); }
        }

        public int RequestingUserId
        {
            get { return (int)row["Requesting_User_Id"]; }
            set { this.setRowValue("Requesting_User_Id", value); }
        }
        #endregion

        #region Misc Methods
        public ArrayList AppraisalTasks
        {
            get { return m_AppraisalTasks == null ? m_AppraisalTasks = new ArrayList() : m_AppraisalTasks; }
            set { m_AppraisalTasks = value; }
        }

        public void InsertAll()
        {
            //saves all task objects and appraisal requests
            this.Update();
            foreach (AppraisalTask t in this.AppraisalTasks)
            {
                t.AppraisalRequestId = this.Id;
                t.Update();
            }
        }

        public string AppraisalTasksXml
        {
            get
            {
                string xml = "<Appraisal_Request>";
                foreach (AppraisalTask t in this.AppraisalTasks)
                    xml += t.XML;
                xml += "</Appraisal_Request>";

                return xml;
            }
        }

        public void CreateAppraisalTask(int vehicleId, int claimantId = 0)
        {
            //creates appraisalTask object, but doesn't update DB
            int clmtId = claimantId;

            if (vehicleId != 0)
            {
                if (clmtId == 0)
                {
                    Vehicle car = new Vehicle(vehicleId);
                    Claimant cl = car.Claimant;
                    if (car.Claimant != null)
                        clmtId = car.Claimant.Id;
                }
            }

            AppraisalTask at = new AppraisalTask(this, vehicleId, clmtId);
            //at.Update();
            this.AppraisalTasks.Add(at);
           
        }
        #endregion

        #region Nonimplemented
        public override string TableName
        {
            get
            {
                return "Appraisal_Request";
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Draft:TransactionReferenceTable
    {

        private static modLogic ml = new modLogic();
        private static clsDatabase db = new clsDatabase();

        #region Constructors
        public Draft() : base() { }
        public Draft(int id) : base(id) { }
        public Draft(Hashtable row) : base(row) { }
        public Draft(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Draft"; }
        }

        public DateTime ActualPrintDate
        {
            get { return (DateTime)row["Actual_Print_Date"]; }
            set { this.setRowValue("Actual_Print_Date", value); }
        }

        public string Address1
        {
            get { return (string)row["Address1"]; }
            set { this.setRowValue("Address1", value); }
        }

        public string Address2
        {
            get { return (string)row["Address2"]; }
            set { this.setRowValue("Address2", value); }
        }

        public string BankAccountNo
        {
            get { return (string)row["Bank_Account_No"]; }
            set { this.setRowValue("Bank_Account_No", value); }
        }

        public string BankRoutingNo
        {
            get { return (string)row["Bank_Routing_No"]; }
            set { this.setRowValue("Bank_Routing_No", value); }
        }

        public string City
        {
            get { return (string)row["City"]; }
            set { this.setRowValue("City", value); }
        }

        public double DraftAmount
        {
            get { return (double)((decimal)row["Draft_Amount"]); }
            set { this.setRowValue("Draft_Amount", value); }
        }

        public int DraftId
        {
            get { return (int)row["Draft_Id"]; }
            set { this.setRowValue("Draft_Id", value); }
        }

        public int DraftNo
        {
            get { return (int)row["Draft_No"]; }
            set { this.setRowValue("Draft_No", value); }
        }

        public int DraftVoidReasonId
        {
            get { return (int)row["Draft_Void_Reason_Id"]; }
            set { this.setRowValue("Draft_Void_Reason_Id", value); }
        }

        public DraftVoidReason DraftVoidReason()
        {
            return new DraftVoidReason(this.DraftVoidReasonId);
        }

        public string ExplanationOfProceeds
        {
            get { return (string)row["Explanation_Of_Proceeds"]; }
            set { this.setRowValue("Explanation_Of_Proceeds", value); }
        }

        public int InDraftQueue
        {
            get { return (int)row["In_Draft_Queue"]; }
            set { this.setRowValue("In_Draft_Queue", value); }
        }

        public bool isPrinted
        {
            get { return (bool)row["Is_Printed"]; }
            set { this.setRowValue("Is_Printed", value); }
        }

        public bool isVoid
        {
            get { return (int)row["Is_Void"] == 1; }
            set { this.setRowValue("Is_Void", value ? 1 : 0); }
        }

        public string MailToName
        {
            get { return (string)row["Mail_To_Name"]; }
            set { this.setRowValue("Mail_To_Name", value); }
        }
        
        public int NoPeriods
        {
            get { return (int)row["No_Periods"]; }
            set { this.setRowValue("No_Periods", value); }
        }

        public string Payee
        {
            get { return (string)row["Payee"]; }
            set { this.setRowValue("Payee", value); }
        }

        public DateTime PipDateFrom
        {
            get { return (DateTime)row["Pip_Date_From"]; }
            set { this.setRowValue("Pip_Date_From", value); }
        }

        public DateTime PipDateTo
        {
            get { return (DateTime)row["Pip_Date_To"]; }
            set { this.setRowValue("Pip_Date_To", value); }
        }
        
        public DateTime PrintDate
        {
            get { return (DateTime)row["Print_Date"]; }
            set { this.setRowValue("Print_Date", value); }
        }
        
        public int ReissuedFromDraftId
        {
            get { return (int)row["Reissued_From_Draft_Id"]; }
            set { this.setRowValue("Reissued_From_Draft_Id", value); }
        }

        public int ReleasedFromQueueBy
        {
            get { return (int)row["Released_From_Queue_By"]; }
            set { this.setRowValue("Released_From_Queue_By", value); }
        }

        public User ReleasedFromQueue()
        {
            return new User(this.ReleasedFromQueueBy);
        }

        public int StateId
        {
            get { return (int)row["State_Id"]; }
            set { this.setRowValue("State_Id", value); }
        }

        public State State()
        {
            return new State(this.StateId);
        }

        public int VendorId
        {
            get { return (int)row["Vendor_Id"]; }
            set { this.setRowValue("Vendor_Id", value); }
        }

        public Vendor Vendor()
        {
            return new Vendor(this.VendorId);
        }

        public string Zipcode
        {
            get { return (string)row["Zipcode"]; }
            set { this.setRowValue("Zipcode", value); }
        }
        #endregion

        #region Misc Methods
        public static int InsertDraft(Hashtable draft, int reserveId, int transactionTypeId
            , Hashtable eventTypeIds, int voidReasonId, int reissueFromTransactionId
            , bool isTotalLoss = false, bool isUnvoid = false, int transDraft_ID = 0)
        {
            /*saves draft and updates transaction and reserve line.
             * issue any offsetting transactions necessary.
             * returns the transaction ID of the transaction inserted corresponding to the draft
             * (enhancement) add a transaction scope so an implicit rollback occurs - bcp 03/2012
             * 
             * ***************************************************************************************
             * draft = draft row
             * reserveId = reserve ID of draft
             * transactionId = transaction ID of draft
             * transactionTypeId = transaction type selected
             * eventTypeIds = eventType IDs to fire, if applicable: Above User's Authority Limt, Above Per Person Coverage, etc.
             * voidReasonId = void reason ID if this is a reissue draft - a value of 0 indicates not a reissue draft
             * reissueFomTransactionId = transaction ID this draft is reissued from - a value of 0 indicates not a reissue draft
             * isTotalLoss = this is a total loss - close in pending salvage (if true)
             * isUnvoid - don't auto increment draft number
            */

            double amount; //draft amount
            Hashtable parms; //parms for event firing
            bool isLoss; //is draft loss - vs expense
            Hashtable company; //company row
            int draftId; //Draft_Id for newly inserted draft row
            int transactionId; //ID of transaction row inserted - corresponding to draft row
            Reserve res = new Reserve(reserveId);
            Claim claim = new Claim(res.ClaimId);

          

            /*create a transaction scope for all table insert/updates.  we also want to include any fired events which are table inserts.  this is all or nothing
             * if this is a reissue, enter void transaction.  this needs to happen before the reserve var is created because it updates the reserve. */
            if (voidReasonId != 0)
            {
                
                MedCsxLogic.Transaction.VoidDraft(reissueFromTransactionId, voidReasonId);
            }
            //keep original draft_No is this is an unvoid
            if (!isUnvoid)
                draft["Draft_No"] = claim.NextDraftNo;

            company = ml.getRow("Company", db.GetIntFromStoredProcedure("usp_Get_Company_Id_For_Claim_Id", claim.Id));

            /*H.T. 11/19/2008
             * Leave the Draft Bank stuff alone if it is an Unvoid.  It has the correct banking information from the original Draft
             * what you see below is not necessary to do
             * draft["Bank_Routing_No"] = GetStrFieldData("Bank_Routing_No", "Draft", "Draft_ID", transDraft_ID) //get the data associated with the original Draft
             * draft["Bank_Account_No"] = GetStrFieldData("Bank_Account_No", "Draft", "Draft_ID", transDraft_ID) //Accounting likes it this way
            */
            if (!isUnvoid)
            {
                draft["Bank_Routing_No"] = company["Bank_Routing_No"]; //get current bank numbers
                draft["Bank_Account_No"] = company["Bank_Account_No"];
            }

            //insert draft
            draftId = ml.InsertRow(draft, "Draft");
            amount = (double)draft["Draft_Amount"];

            //insert transaction now corresponding to draft row
            Transaction tr = new Transaction();
            TransactionType trType = new TransactionType(transactionTypeId);
            tr.ReserveId = reserveId;
            tr.DraftId = draftId;
            isLoss = trType.isLoss;
            tr.TransactionAmount = amount * -1.0;
            tr.TransactionDate = ml.DBNow();
            tr.TransactionTypeId = transactionTypeId;
            tr.isLoss = isLoss;
            tr.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            transactionId = tr.Update();

            //update net reserve and paid amounts
            if (isLoss)
            {
                res.NetLossReserve -= amount;
                res.PaidLoss += amount;
            }
            else //expense
            {
                res.NetExpenseReserve -= amount;
                res.PaidExpense += amount;
            }
            res.Update();

            //if final loss payment, close reserve
            if (transactionTypeId == (int)modGeneratedEnums.TransactionType.Final_Loss_Payment)
            {
                if (isTotalLoss)
                    res.ClosePendingSalvage();
                else
                    res.Close();
            }

            //if supplemental payment - issue offsetting transaction and change amounts on reserve row
            if (((res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed) ||
                (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Salvage) ||
                (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Closed_Pending_Subro)) &&
                (transactionTypeId != (int)modGeneratedEnums.TransactionType.Final_Loss_Payment))
            {
                //issue offsetting transaction
                res.InsertOffsettingTransaction(amount, isLoss);

                //increment number of supplementat payments counter on reserve line
                res.NoSupplementalPayments++;

                //change reserves and paid columns
                res.NetLossReserve = 0;
                res.GrossLossReserve = 0;
                res.NetExpenseReserve = 0;
                res.GrossExpenseReserve = 0;
                res.Update();

                //if claim closed, increment supplementatl payments counter on claim
                if ((claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed) && (reissueFromTransactionId != 0))
                {
                    claim.NoSupplementalPayments++;
                    claim.Update();
                }
            }

            //fire events
            parms = new Hashtable();
            parms.Add("Draft_Id", draftId);
            foreach (int eventTypeId in eventTypeIds.Keys)
            {
                parms = (Hashtable)eventTypeIds[eventTypeId];
                if (parms == null)
                    parms = new Hashtable();
                parms.Add("Draft_Id", draftId);
                tr.FireEvent(eventTypeId, parms);
            }

            return transactionId;
        }

        public static int getTransactionIdForDraftId(int draftId)
        {
            //returns transaction ID for draft ID - if multiple transactions per draft, return first
            return new Draft(draftId).TransactionId;
        }

        public static bool hasDraftReissue(int draftId)
        {
            return new Draft(draftId).hasReissue;
        }

        public static void MakeDraftNonNegotiable(int draftId)
        {
            //makes draft non-negotiable
            new Draft(draftId).MakeNonNegotiable();
        }

        public static Draft DraftForDraftNo(int draftNo)
        {
            return new Draft(db.GetIntFromStoredProcedure("usp_Get_Draft_Id_For_Draft_No", draftNo));
        }

        public static Draft DraftForDescription(string description)
        {
            string[] a = description.Split(' ');
            if (a.Length < 3)
                return null;

            string draftNo = a[2];
            if (!modLogic.isNumeric(draftNo))
                return null;

            return DraftForDraftNo(int.Parse(draftNo));
        }

        public int TransactionId
        {
            get { return db.GetIntFromStoredProcedure("usp_Get_Transaction_Id_For_Draft_Id", this.Id); }
        }

        public bool hasReissue
        {
            //does this draft has a valid reissue?
            get { return db.GetIntFromStoredProcedure("usp_Get_No_Draft_Reissues", this.Id) != 0; }
        }

        public void MakeNonNegotiable()
        {
            //make draft non-negotiable
            db.ExecuteStoredProcedure("usp_Make_Draft_Non_Negotiable", this.Id);
        }

        public string Description
        {
            //returns draft description from draft ID
            get { return "Draft No " + this.DraftNo + " in the amount of " + this.DraftAmount.ToString("c") + " to " + this.Payee; }
        }

        public bool isDraftNoVoid
        {
            //is draft number currently voided?
            get { return db.GetBoolFromStoredProcedure("usp_Is_Draft_No_Void", this.DraftNo); }
        }

        public override Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }

        public DraftPrint InsertDraftPrintForPending(Reserve res)
        {
            DraftPrint dp = new DraftPrint();
            int draftPrintId;
            Claim claim = res.Claim;

            dp.DraftId = this.DraftId;
            dp.MailToLine1 = this.MailToName;
            dp.MailToLine2 = this.Address1;

            if (this.Address2.Trim() != "")
            {
                dp.MailToLine3 = this.Address2;
                dp.MailToLine4 = this.City + ", " + this.State().Abbreviation + " " + this.Zipcode;
            }
            else
            {
                dp.MailToLine3 = this.City + ", " + this.State().Abbreviation + " " + this.Zipcode;
                dp.MailToLine4 = "";
            }

            dp.Insured = claim.InsuredPerson.Name;
            dp.ClaimNo = claim.DisplayClaimId;
            dp.PolicyNo = claim.PolicyNo;

            if (this.VendorId != 0)
                dp.TaxId = this.Vendor().TaxId;

            dp.Employee = ml.CurrentUser.Name;

            dp.LossDate = claim.DateOfLoss.ToString("MM/dd/yyyy");
            dp.DraftNo = this.DraftNo.ToString().PadLeft(6, '0');
            dp.DraftAmount = "";

            string cov = res.ReserveType.Description;
            cov = cov.Replace("ABI Major", "ABI");
            cov = cov.Replace("ABI Minor", "ABI");

            dp.DraftLongDate = DateTime.Now.ToString("MM/dd/yyy");
            dp.DraftAmountWithStars = "$____________";
            dp.DraftAmountDescription = "__________________________________________________________ Dollars";
            dp.Payee = this.Payee;
            dp.ExplanationOfProceeds = this.ExplanationOfProceeds;
            dp.RoutingSymbols = "<" + this.DraftNo.ToString().PadLeft(6, '0') + "< :" + this.BankRoutingNo + ": <" + this.BankAccountNo + "<";
            dp.NonNegotiable = "";
            draftPrintId = dp.Update();

            return dp;
        }

        public static int InsertDraftPrint(int draftId)
        {
            //inserts draft_print row from draft ID and return ID

            //draftID = Draft_Id of draft to be printed
            int transactionId; //transaction ID for draft
            int reserveId; //reserve ID for draft
            Reserve res; //reserve row
            Claim claim; //claim row
            Hashtable dp; //draft_print row
            double draftAmount; //draft amount to be printed
            Vendor vendor;
            int vendorId;
            string cov;
            
            //get transaction, reserve, and claim data from draft ID
            Draft d = new Draft(draftId);

            transactionId = d.TransactionId;
            reserveId = d.ReserveId;
            res = d.Reserve();
            claim = d.Claim();

            //get draft row
            draftAmount = d.DraftAmount;

            //get vendor row
            vendorId = d.VendorId;
            vendor = d.Vendor();

            //insert draft print row
            dp = new Hashtable();
            dp.Add("Draft_Id", draftId);
            dp.Add("Draft_Date", DateTime.Today.ToString("MM/dd/yyyy"));  //get mm/dd/yy portion of date
            dp.Add("Mail_To_Line_1", d.MailToName);
            dp.Add("Mail_To_Line_2", d.Address1);

            if (d.Address2.Trim() != "")
            {
                dp.Add("Mail_To_Line_3", d.Address2);
                dp.Add("Mail_To_Line_4", d.City + ", " + d.State().Abbreviation + " " + d.Zipcode);
            }
            else
            {
                dp.Add("Mail_To_Line_3", d.City + ", " + d.State().Abbreviation + " " + d.Zipcode);
                dp.Add("Mail_To_Line_4", "");
            }

            dp.Add("Insured", claim.InsuredPerson.Name);
            dp.Add("Claim_No", claim.DisplayClaimId);
            dp.Add("Policy_No", claim.PolicyNo);
            dp.Add("Tax_Id", vendor.TaxId);

            dp.Add("Employee", ml.CurrentUser.Name);

            dp.Add("Loss_Date", claim.DateOfLoss.ToString("MM/dd/yyyy"));
            dp.Add("Draft_No", d.DraftNo.ToString().PadLeft(6, '0'));
            dp.Add("Draft_Amount", draftAmount.ToString("c"));

            cov = res.ReserveType.Description;
            cov = cov.Replace("ABI Major", "ABI");
            cov = cov.Replace("ABI Minor", "ABI");
            dp.Add("Coverage", cov);

            dp.Add("Draft_Long_Date", DateTime.Today.ToString("MM/dd/yyy"));
            dp.Add("Draft_Amount_With_Stars", "*****"+draftAmount.ToString("c")+"***");
            dp.Add("Draft_Amount_Description", AmountDescription(draftAmount));
            dp.Add("Payee", d.Payee);
            dp.Add("Explanation_Of_Proceeds", d.ExplanationOfProceeds);
            dp.Add("Routing_Symbols", "<" + d.DraftNo.ToString().PadLeft(6, '0') + "< :" + d.BankRoutingNo.PadLeft(9,'0') + ": <" + d.BankAccountNo + "<");
            dp.Add("NonNegotiable", "");

            return ml.InsertRow(dp, "Draft_Print");
        }

        public static string AmountDescription(double amount)
        {
            //returns a description for an amount, such as would be printed on a draft.
            //e.g., an amount of 1100.00 would return "One Thousand One Hundred Dollars and 00/100".

            string s = "";
            string result = "";
            string[] a;
            string millions = "";
            string thousands = "";
            string dollars = "";
            string cents = "";

            s = amount.ToString("000,000,000.00");
            a = s.Split(',');

            millions = a[0];
            thousands = a[1];
            dollars = a[2].Split('.')[0];
            cents = a[2].Split('.')[1];

            if (millions != "000")
                result = ThreeDigitAmountDescription(millions) + " Million ";

            if (thousands != "000")
                result += ThreeDigitAmountDescription(thousands) + " Thousand ";

            if (dollars != "000")
                result += ThreeDigitAmountDescription(dollars);
            else
            {
                if (result == "")
                    result = "Zero";
            }

            if (result == "One")
                result += " Dollar and " + cents + "/100";
            else
                result += " Dollars and " + cents + "/100";

            return result.Replace("  ", " ").Trim();
        }

        private static string ThreeDigitAmountDescription(string s)
        {
            //returns a draft-style description of a three-digit number (s).
            //e.g., s = 532 would return "Five Hundred Thirty-Two"

            string hundreds; //hundreds digit
            string tensOnes; //tens and ones digit
            string result = "";

            hundreds = s.Substring(0, 1);
            tensOnes = s.Substring(s.Length - 2, 2);

            if (hundreds == "0")
                result = TwoDigitAmountDescription(tensOnes);
            else
            {
                result = OneDigitAmountDescription(hundreds) + " Hundred ";
                if (tensOnes != "00")
                    result += TwoDigitAmountDescription(tensOnes);
            }
            return result;
        }

        private static string TwoDigitAmountDescription(string s)
        {
            //returns a draft-style description of a two-digit number (s).
            //e.g., s = 32 would return "Thirty-Two"

            string tens; //tens digit
            string ones; //ones digit
            string result = "";

            tens = s.Substring(0, 1);
            ones = s.Substring(s.Length - 1, 1);

            if (tens == "0")
            {
                result = OneDigitAmountDescription(ones);
                return result;
            }

            if (tens == "1")
            {
                switch (ones)
                {
                    case "1":
                        result = "Eleven";
                        break;
                    case "2":
                        result = "Twelve";
                        break;
                    case "3":
                        result = "Thirteen";
                        break;
                    case "4":
                        result = "Fourteen";
                        break;
                    case "5":
                        result = "Fifteen";
                        break;
                    case "6":
                        result = "Sixteen";
                        break;
                    case "7":
                        result = "Seventeen";
                        break;
                    case "8":
                        result = "Eighteen";
                        break;
                    case "9":
                        result = "Nineteen";
                        break;
                    default:// case "0":
                        result = "Ten";
                        break;
                }
            }
            else
            {
                //Tens is 2 - 9
                switch (tens)
                {
                    case "2":
                        result = "Twenty";
                        break;
                    case "3":
                        result = "Thirty";
                        break;
                    case "4":
                        result = "Forty";
                        break;
                    case "5":
                        result = "Fifty";
                        break;
                    case "6":
                        result = "Sixty";
                        break;
                    case "7":
                        result = "Seventy";
                        break;
                    case "8":
                        result = "Eighty";
                        break;
                    case "9":
                        result = "Ninety";
                        break;
                }
            }

            if (ones != "0")//the amount is not Ten, Twenty, Thirty,...
                result += "-" + OneDigitAmountDescription(ones);
            return result;
        }

        private static string OneDigitAmountDescription(string s)
        {
            //returns a draft-style description of a one-digit number (s).
            //e.g., s = 2 would return "Two"

            string result = "";

            switch (s)
            {
                case "0":
                    result = "Zero";
                    break;
                case "1":
                    result = "One";
                    break;
                case "2":
                    result = "Two";
                    break;
                case "3":
                    result = "Three";
                    break;
                case "4":
                    result = "Four";
                    break;
                case "5":
                    result = "Five";
                    break;
                case "6":
                    result = "Six";
                    break;
                case "7":
                    result = "Seven";
                    break;
                case "8":
                    result = "Eight";
                    break;
                case "9":
                    result = "Nine";
                    break;
            }
            return result;
        }
        #endregion
    }
}

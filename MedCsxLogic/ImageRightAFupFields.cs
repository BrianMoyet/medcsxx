﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ImageRightAFupFields
    {

        #region Class Variables
        public string Drawer = "";
        public string Drawer2 = "";
        public string FileNumber = "";
        public string FileNumber2 = "";
        public string Delete = "";
        #endregion

        #region Misc Methods
        public string EntireRecord
        {
            get
            {
                string record = (this.Drawer.PadRight(4).Substring(0, 4) + this.FileNumber.PadRight(40).Substring(0, 40) + this.Delete.PadRight(6).Substring(0, 6) +
                              this.Drawer2.PadRight(4).Substring(0, 4) + this.FileNumber2).PadRight(94).Substring(0, 94);
                return record;
            }
        }

        public string DeleteRecord
        {
            get
            {
                string record = (this.Drawer.PadRight(4).Substring(0, 4) + this.FileNumber.PadRight(40).Substring(0, 40) + "Y").PadRight(94).Substring(0, 94);
                return record;
            }
        }
        #endregion
    }
}

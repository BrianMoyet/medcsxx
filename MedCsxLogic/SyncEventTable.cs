﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public abstract class SyncEventTable:MedCsXTable
    {

        #region Constructors
        public SyncEventTable() : base() { }
        public SyncEventTable(int id) : base(id) { }
        public SyncEventTable(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int EventTypeId
        {
            get { return (int)row["Event_Type_Id"]; }
            set { this.setRowValue("Event_Type_Id", value); }
        }

        public EventType EventType()
        {
            return new EventType(this.EventTypeId);
        }
        #endregion

        #region Nonimplementation
        public override string TableName
        {
            get { throw new NotImplementedException(); }
        }
        #endregion

    }
}

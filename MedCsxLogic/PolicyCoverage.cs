﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PolicyCoverage:ClaimTable
    {

        #region Class Variables
        private PolicyCoverageType m_PolicyCoverageType = null;
        #endregion

        #region Constructors
        public PolicyCoverage() : base() { }
        public PolicyCoverage(int id) : base(id) { }
        public PolicyCoverage(Hashtable row) : base(row) { }
        public PolicyCoverage(DataTable table, DataRow row) : base(table, row) { }
        public PolicyCoverage(int claimId, double versionNo, int policyCoverageTypeId)
            : base()
        {
            this.ClaimId = claimId;
            this.VersionNo = versionNo;
            this.PolicyCoverageTypeId = policyCoverageTypeId;
        }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Policy_Coverage"; }
        }

        public double VersionNo
        {
            get { return (double)((decimal)row["Version_No"]); }
            set { this.setRowValue("Version_No", value); }
        }

        public int PolicyCoverageTypeId
        {
            get { return (int)row["Policy_Coverage_Type_Id"]; }
            set { this.setRowValue("Policy_Coverage_Type_Id", value); }
        }

        public bool hasPerPersonCoverage
        {
            get { return (bool)row["Has_Per_Person_Coverage"]; }
            set { this.setRowValue("Has_Per_Person_Coverage", value); }
        }

        public double PerPersonCoverage
        {
            get { return (double)((decimal)row["Per_Person_Coverage"]); }
            set { this.setRowValue("Per_Person_Coverage", value); }
        }

        public int PerPersonPeriodTypeId
        {
            get { return (int)row["Per_Person_Period_Type_Id"]; }
            set { this.setRowValue("Per_Person_Period_Type_Id", value); }
        }

        public int PerPersonNoPeriods
        {
            get { return (int)row["Per_Person_No_Periods"]; }
            set { this.setRowValue("Per_Person_No_Periods", value); }
        }

        public bool PerPersonArePeriodsContiguous
        {
            get { return (bool)row["Per_Person_Are_Periods_Contiguous"]; }
            set { this.setRowValue("Per_Person_Are_Periods_Contiguous", value); }
        }

        public bool hasPerAccidentCoverage
        {
            get { return (bool)row["Has_Per_Accident_Coverage"]; }
            set { this.setRowValue("Has_Per_Accident_Coverage", value); }
        }

        public double PerAccidentCoverage
        {
            get { return (double)((decimal)row["Per_Accident_Coverage"]); }
            set { this.setRowValue("Per_Accident_Coverage", value); }
        }
        
        public bool hasDeductible
        {
            get { return (bool)row["Has_Deductible"]; }
            set { this.setRowValue("Has_Deductible", value); }
        }

        public double DeductibleAmount
        {
            get { return (double)((decimal)row["Deductible_Amount"]); }
            set { this.setRowValue("Deductible_Amount", value); }
        }
        #endregion
        #region Misc Methods
        public PolicyCoverageType PolicyCoverageType
        {
            get { return m_PolicyCoverageType == null ? m_PolicyCoverageType = new PolicyCoverageType(this.PolicyCoverageTypeId) : m_PolicyCoverageType; }
        }

        public bool hasCoverage
        {
            get { return (this.hasPerPersonCoverage || this.hasPerAccidentCoverage || hasDeductible); }
        }

        public PeriodType PerPersonPeriodType()
        {
            return new PeriodType(this.PerPersonPeriodTypeId);
        }

        public bool isAutoBodilyInjury
        {
            get { return this.PolicyCoverageType.isAutoBodilyInjury; }
        }

        public bool isCollision
        {
            get { return this.PolicyCoverageType.isCollision; }
        }

        public bool isComprehensive
        {
            get { return this.PolicyCoverageType.isComprehensive; }
        }

        public bool isMedicalPayments
        {
            get { return this.PolicyCoverageType.isMedicalPayments; }
        }

        public bool isPipEssentialServices
        {
            get { return this.PolicyCoverageType.isPipEssentialServices; }
        }

        public bool isPipFuneral
        {
            get { return this.PolicyCoverageType.isPipFuneral; }
        }

        public bool isPipMedical
        {
            get { return this.PolicyCoverageType.isPipMedical; }
        }

        public bool isPipRehab
        {
            get { return this.PolicyCoverageType.isPipRehab; }
        }

        public bool isPipWages
        {
            get { return this.PolicyCoverageType.isPipWages; }
        }

        public bool isPropertyDamage
        {
            get { return this.PolicyCoverageType.isPropertyDamage; }
        }

        public bool isUnderinsuredMotorist
        {
            get { return this.PolicyCoverageType.isUnderinsuredMotorist; }
        }

        public bool isUninsuredMotorist
        {
            get { return this.PolicyCoverageType.isUninsuredMotorist; }
        }
        #endregion
    }
}

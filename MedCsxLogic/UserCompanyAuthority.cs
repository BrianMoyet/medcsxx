﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class UserCompanyAuthority:MedCsXTable
   {

       #region Contructors
       public UserCompanyAuthority() : base() { }
       public UserCompanyAuthority(int id) : base(id) { }
       public UserCompanyAuthority(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get { return "User_Company_Authority"; }
       }

       public int UserId
       {
           get { return (int)row["User_Id"]; }
           set { this.setRowValue("User_Id", value); }
       }

       public int CompanyId
       {
           get { return (int)row["Company_Id"]; }
           set { this.setRowValue("Company_Id", value); }
       }

       public bool OpenAllowed
       {
           get { return (bool)row["Open_Allowed"]; }
           set { this.setRowValue("Open_Allowed", value); }
       }
       #endregion
   }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PendingDraftStatus : TypeTable
    {

        #region Constructors
        public PendingDraftStatus() : base() { }
        public PendingDraftStatus(int id) : base(id) { }
        public PendingDraftStatus(string description)
            : base(description)
        {
            this.Id = new modLogic().getId(this.TableName, description);
        }
        public PendingDraftStatus(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Pending_Draft_Status";
            }
        }

        public int PendingDraftStatusId
        {
            get { return (int)row["Pending_Draft_Status_Id"]; }
            set { this.setRowValue("Pending_Draft_Status_Id", value); }
        }
        #endregion

        #region Methods
        public bool IsCancelled
        {
            get { return this.Id == (int)modGeneratedEnums.PendingDraftStatus.Cancelled; }
        }

        public bool IsComplete
        {
            get { return this.Id == (int)modGeneratedEnums.PendingDraftStatus.Complete; }
        }

        public bool IsPending
        {
            get { return this.Id == (int)modGeneratedEnums.PendingDraftStatus.Pending; }
        }
        #endregion

    }
}

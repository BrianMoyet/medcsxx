﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class Task:TypeTable
    {

        #region Constructors
        public Task() : base() { }
        public Task(int id) : base(id) { }
        public Task(string description) : base(description) { }
        public Task(Hashtable row) : base(row) { }
        public Task(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Task";
            }
        }

        public int TaskId
        {
            get { return (int)row["Task_Id"]; }
            set { this.setRowValue("Task_Id", value); }
        }
        #endregion
    }
}

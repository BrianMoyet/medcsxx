﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Forms;
using MedCsxDatabase;

namespace MedCsxLogic
{

    public enum PossibleDuplicateClaimMode
    {
        Policy_Changed, 
        Date_Of_Loss_Changed,
        Claim_Created,
        Add_Update_Person,
        Add_Update_Vehicle
    };
    public class Claim:ClaimTable
    {
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
       

        #region Class Variables
        private Vehicle m_InsuredVehicle = null;
        private ArrayList m_Persons = null;
        private User m_Adjuster = null;
        private State m_LossState = null;
        private PolGeneralInfo _policy = null;
        private string myPolicyDates = "";
        private Hashtable _policyCoverages = new Hashtable();
        private Hashtable _coverageDescriptions = new Hashtable();
        private ClaimHistory m_ClaimHistory = null;
        private int m_ClaimHistoryId = 0;
        private int m_FraudTypeId = 0;
        #endregion

        #region Constructors
        public Claim() : base() { }
        public Claim(int id) : base(id) { }
        public Claim(Hashtable row) : base(row) { }
        #endregion

        #region Table Properties
        public override string TableName
        {
            get { return "Claim"; }
        }
        #endregion

        #region Column Names
        public int AccidentTypeId
        {
            get { return (int)row["Accident_Type_Id"]; }
            set { this.setRowValue("Accident_Type_Id", value); }
        }

        public DateTime AdjusterAssignmentDate
        {
            get { return (DateTime)row["Adjuster_Assignment_Date"]; }
            set { this.setRowValue("Adjuster_Assignment_Date", value); }
        }

        public bool AdjusterEnteredFirstFileNote
        {
            get { return (bool)row["Adjuster_Entered_First_File_Note"]; }
            set { this.setRowValue("Adjuster_Entered_First_File_Note", value); }
        }

        public int AdjusterId
        {
            get { return (int)row["Adjuster_Id"]; }
            set 
            { 
                this.setRowValue("Adjuster_Id", value);
                RefreshAdjuster();
            }
        }

        public DateTime AtFaultDateSet
        {
            get { return (DateTime)row["At_Fault_Date_Set"]; }
            set { this.setRowValue("At_Fault_Date_Set", value); }
        }

        public int AtFaultTypeId
        {
            get { return (int)row["At_Fault_Type_Id"]; }
            set 
            {
                if ((int)row["At_Fault_Type_Id"] != value)
                    this.AtFaultDateSet = ml.DBNow();
                this.setRowValue("At_Fault_Type_Id", value); 
            }
        }

        public string AuthorityContacted
        {
            get { return (string)row["Authority_Contacted"]; }
            set { this.setRowValue("Authority_Contacted", value); }
        }

        public int ClaimStatusId
        {
            get { return (int)row["Claim_Status_Id"]; }
            set { this.setRowValue("Claim_Status_Id", value); }
        }

        public DateTime CloseDate
        {
            get { return (DateTime)row["Close_Date"]; }
            set { this.setRowValue("Close_Date", value); }
        }

        public string Comments
        {
            get { return (string)row["Comments"]; }
            set { this.setRowValue("Comments", value); }
        }

        public int CompanyLocationId
        {
            get { return (int)row["Company_Location_Id"]; }
            set { this.setRowValue("Company_Location_Id", value); }
        }

        public DateTime DateAttemptContactInsured
        {
            get { return (DateTime)row["Date_Attempt_Contact_Insured"]; }
            set { this.setRowValue("Date_Attempt_Contact_Insured", value); }
        }

        public DateTime DateFirstContactInsured
        {
            get { return (DateTime)row["Date_First_Contact_Insured"]; }
            set { this.setRowValue("Date_First_Contact_Insured", value); }
        }

        public DateTime DateOfLoss
        {
            get { return (DateTime)row["Date_Of_Loss"]; }
            set { this.setRowValue("Date_Of_Loss", value); }
        }

        public DateTime NoLossSent
        {
            get { return (DateTime)row["No_Loss_Sent"]; }
            set { this.setRowValue("No_Loss_Sent", value); }
        }

        public DateTime DateReported
        {
            get { return (DateTime)row["Date_Reported"]; }
            set { this.setRowValue("Date_Reported", value); }
        }

        public string DisplayClaimId
        {
            get { return (string)row["Display_Claim_Id"]; }
            set { this.setRowValue("Display_Claim_Id", value); }
        }

        public int DriverPersonId
        {
            get { return (int)row["Driver_Person_Id"]; }
            set { this.setRowValue("Driver_Person_Id", value); }
        }

        public string FoxproClaimNo
        {
            get { return (string)row["Foxpro_Claim_No"]; }
            set { this.setRowValue("Foxpro_Claim_No", value); }
        }

        public bool hasCoverageIssue
        {
            get { return (bool)row["Has_Coverage_Issue"]; }
            set { this.setRowValue("Has_Coverage_Issue", value); }
        }

        public bool hasDisputedLiability
        {
            get { return (bool)row["Has_Disputed_Liability"]; }
            set { this.setRowValue("Has_Disputed_Liability", value); }
        }

        public bool? hasSR22
        {
            get { return (bool?)row["HasSR22"]; }
            set { this.setRowValue("HasSR22", value); }
        }

        public int InsuredPersonId
        {
            get { return (int)row["Insured_Person_Id"]; }
            set 
            { 
                this.setRowValue("Insured_Person_Id", value);
                RefreshInsuredPerson();
            }
        }

        public int InsuredVehicleId
        {
            get { return (int)row["Insured_Vehicle_Id"]; }
            set 
            { 
                this.setRowValue("Insured_Vehicle_Id", value);
                m_InsuredVehicle = null;
            }
        }

        public bool isCollision
        {
            get { return (bool)row["Is_Collision"]; }
            set { this.setRowValue("Is_Collision", value); }
        }

        public bool isComp
        {
            get { return (bool)row["Is_Comp"]; }
            set { this.setRowValue("Is_Comp", value); }
        }

        public bool isExpiredPolicy
        {
            get { return (bool)row["Is_Expired_Policy"]; }
            set { this.setRowValue("Is_Expired_Policy", value); }
        }

        public bool isFoxproConversion
        {
            get { return (bool)row["Is_Foxpro_Conversion"]; }
            set { this.setRowValue("Is_Foxpro_Conversion", value); }
        }

        public bool LargeLoss
        {
            get { return (bool)row["Large_Loss"]; }
            set { this.setRowValue("Large_Loss", value); }
        }

        public DateTime LastActivityDate
        {
            get { return (DateTime)row["Last_Activity_Date"]; }
            set { this.setRowValue("Last_Activity_Date", value); }
        }

        public string LossCity
        {
            get { return (string)row["Loss_City"]; }
            set { this.setRowValue("Loss_City", value); }
        }

        public string LossDescription
        {
            get { return (string)row["Loss_Description"]; }
            set { this.setRowValue("Loss_Description", value); }
        }

        public string LossLocation
        {
            get { return (string)row["Loss_Location"]; }
            set { this.setRowValue("Loss_Location", value); }
        }

        public int LossLocationId
        {
            get { return (int)row["Loss_Location_Id"]; }
            set { this.setRowValue("Loss_Location_Id", value); }
        }

        public int LossLocationNbr
        {
            get { return (int)row["Loss_Location_Nbr"]; }
            set { this.setRowValue("Loss_Location_Nbr", value); }
        }

        public int LossState_Id
        {
            get { return (int)row["Loss_State_Id"]; }
            set 
            { 
                this.setRowValue("Loss_State_Id", value);
                this.m_LossState = null; 
            }
        }

        public int NoSupplementalPayments
        {
            get { return (int)row["No_Supplemental_Payments"]; }
            set { this.setRowValue("No_Supplemental_Payments", value); }
        }

        public DateTime NotificationDate
        {
            get { return (DateTime)row["Notification_Date"]; }
            set { this.setRowValue("Notification_Date", value); }
        }

        public int OwnerPersonId
        {
            get { return (int)row["Owner_Person_Id"]; }
            set { this.setRowValue("Owner_Person_Id", value); }
        }

        public bool PermissionToUse
        {
            get { return (bool)row["Permission_To_Use"]; }
            set { this.setRowValue("Permission_To_Use", value); }
        }
        
        public string PoliceReportNumber
        {
            get { return (string)row["Police_Report_Number"]; }
            set { this.setRowValue("Police_Report_Number", value); }
        }

        public string PolicyNo
        {
            get { return (string)row["Policy_No"]; }
            set 
            { 
                this.setRowValue("Policy_No", value);
                this._policy = null;
            }
        }

        public DateTime ReopenDate
        {
            get { return (DateTime)row["Reopen_Date"]; }
            set { this.setRowValue("Reopen_Date", value); }
        }

        public string ReportedBy
        {
            get { return (string)row["Reported_By"]; }
            set { this.setRowValue("Reported_By", value); }
        }

        public int ReportedByTypeId
        {
            get { return (int)row["Reported_By_Type_Id"]; }
            set { this.setRowValue("Reported_By_Type_Id", value); }
        }

        public bool SetupComplete
        {
            get { return (bool)row["Setup_Complete"]; }
            set { this.setRowValue("Setup_Complete", value); }
        }

        public int SubClaimTypeId
        {
            get { return (int)row["Sub_Claim_Type_Id"]; }
            set 
            { 
                this.setRowValue("Sub_Claim_Type_Id", value);
                this.SubClaimTypeObject = null;
            }
        }
        
        public string VehicleDamage
        {
            get { return (string)row["Vehicle_Damage"]; }
            set { this.setRowValue("Vehicle_Damage", value); }
        }

        public string Limit
        {
            get { return (string)row["Limit"]; }
            set { this.setRowValue("Limit", value); }
        }

        public string Deductible
        {
            get { return (string)row["Deductible"]; }
            set { this.setRowValue("Deductible", value); }
        }

        public double VersionNo
        {
            get { return (double)((decimal)row["Version_No"]); }
            set 
            { 
                this.setRowValue("Version_No", value);
                this._policy = null;
            }
        }

        public string WhereSeen
        {
            get { return (string)row["Where_Seen"]; }
            set { this.setRowValue("Where_Seen", value); }
        }
        #endregion

        #region Class Objects
        //Object that will be returned by method
        private SubClaimType SubClaimTypeObject = null;
        private ClaimType ClaimTypeObject = null;
        private Person InsuredPersonObject = null;
        #endregion

        #region Misc Properties
        [System.ComponentModel.Description("Return the claim type ID from the sub claim type id of this claim")]
        public int ClaimTypeId
        {
            get { return this.SubClaimType.ClaimTypeId; }
        }

        public ClaimType ClaimType
        {
            get { return this.ClaimTypeObject == null ? 
                this.ClaimTypeObject = new ClaimType(this.ClaimTypeId) : this.ClaimTypeObject; }
        }

        [System.ComponentModel.Description("Sub claim type object.")]
        public SubClaimType SubClaimType
        {
            get { return this.SubClaimTypeObject == null ? this.SubClaimTypeObject = new SubClaimType(this.SubClaimTypeId) : this.SubClaimTypeObject; }
        }

        [System.ComponentModel.Description("Insured person object.")]
        public Person InsuredPerson
        {
            get { return this.InsuredPersonObject == null ? 
                this.InsuredPersonObject = new Person(this.InsuredPersonId) : this.InsuredPersonObject; }
        }

        public int ClaimHistoryId
        {
            get { return m_ClaimHistoryId == 0 ? m_ClaimHistoryId = db.GetIntFromStoredProcedure("usp_Get_Claim_History_Id_For_Claim_Id", this.ClaimId) : m_ClaimHistoryId; }
        }
        #endregion

        #region Return New Object Functions
        public LossLocation LocationOfLoss
        {
            get { return new LossLocation(this.LossLocationId); }
        }

        public ReportedByType ReportedByType
        {
            get { return new ReportedByType(this.ReportedByTypeId); }
        }

        public Person OwnerPerson
        {
            get { return new Person(this.OwnerPersonId); }
        }

        public Person DriverPerson
        {
            get { return new Person(this.DriverPersonId); }
        }

        public ClaimHistory ClaimHistory
        {
            get
            {
                if (m_ClaimHistory == null)
                    m_ClaimHistory = new ClaimHistory(this.ClaimHistoryId);

                return m_ClaimHistory;
            }
        }
        #endregion

        #region Fuctions/Methods
        public DataTable GetFileNotes(int fileNoteTypeId, int claimantId, int reserveId)
        {
            //Get claim file notes
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_File_Notes_With_Images", this.Id, fileNoteTypeId, claimantId, reserveId, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            //Update the db row
            int rowId = base.Update(parms, fireEvents);

            //IMAGERIGHT
            //create ImageRight FUP file with new claim data
            ImageRightFupFile fup = new ImageRightFupFile(rowId, "");
            fup.WriteFile();
            ImageRightAFupFile afup = new ImageRightAFupFile(rowId);
            afup.WriteFile();

            UpdateHistory();

            return rowId;
        }

        public bool isInLitigation()
        {
            //Is this claim in litigation?
            return db.GetBoolFromStoredProcedure("usp_Is_Claim_In_Litigation", this.Id);
        }

        public List<Hashtable> OpenAppraisals()
        {
            //Get all the open appraisalf for this claim
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Open_Appraisals_For_Claim", this.Id);
        }

        public string LossLocationDescription()
        {
            //Description of the loss location
            return db.GetStringFromStoredProcedure("usp_Get_Commercial_Policy_Location_Description", this.PolicyNo);
        }

        public List<Hashtable> Demands()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Demands", this.Id);
        }

        public List<Hashtable> LitigationReserves()
        {
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Litigation_Reserves_For_Claim", this.Id);
        }

        public User Adjuster
        {
            get { return m_Adjuster == null ? m_Adjuster = new User(AdjusterId) : m_Adjuster; }
        }

        public void RefreshInsuredPerson()
        {
            //InsuredPerson will be refreshed on next call
        }

        public void RefreshAdjuster()
        {
            //Adjuster will be refreshed on next call
            m_Adjuster = null;
        }

        public Vehicle InsuredVehicle
        {
            get { return m_InsuredVehicle == null ? m_InsuredVehicle = new Vehicle(InsuredVehicleId) : m_InsuredVehicle; }
        }

        public State LossState
        {
            get { return m_LossState == null ? m_LossState = new State(LossState_Id) : m_LossState; }
        }

        public ArrayList Persons
        {
            get
            {
                if (m_Persons == null)
                {
                    m_Persons = new ArrayList();
                    DataTable rows = db.ExecuteStoredProcedure("usp_Get_Claim_Person_Ids", this.Id);
                    foreach (DataRow row in rows.Rows)
                        m_Persons.Add(new Person((int)row["Person_Id"]));
                }
                return m_Persons;
            }
            set { m_Persons = value; }
        }

        public ArrayList PersonsNotOnPolicy()
        {
            ArrayList a = this.Persons;
            foreach (Person p in a)
            {
                if (!p.PersonType.isOnPolicy())
                    a.Add(p);
            }
            return a;
        }

        public bool SyncEventExists(int eventTypeId)
        {
            //Does Claim_Sync_Event row exist for event type?
            return db.GetBoolFromStoredProcedure("usp_Claim_Sync_Event_Exists", this.Id, eventTypeId);
        }

        public int FirstLockId()
        {
            //get first active claim lock for this claim
            return db.GetIntFromStoredProcedure("usp_Get_First_Claim_Lock", this.Id);
        }

        public List<Hashtable> PersonNamesAndTypes
        {
            //Returns an arraylist of person names and type descriptions for display in a list view or combobox.
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Persons", this.Id); }
        }

        public List<Hashtable> PersonNamesAndTypesNotOnPolicy()
        {
            //Returns an arraylist of person names and type descriptions for all people not listed on policy.
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Persons_Not_On_Policy", this.Id); 
        }
        //public DataTable PartyNamesAndTypes
        //{
        //    //Returns an arraylist of party names and type descriptions for display in a list view or combobox.
        //    get { return db.ExecuteStoredProcedure("usp_Get_Claim_Parties", this.Id); }
        // }

        public List<Hashtable> ClaimantNamesAndTypes
        {
            //Returns an arraylist of Claimant person names and type descriptions for display in a list view or combobox.
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Claimant_Persons", this.Id); }
        }

        public ArrayList ClaimantNames()
        {
            ArrayList a = new ArrayList();
            foreach (Claimant c in this.Claimants)
                a.Add(c.Name);
            return a;
        }

        public Claimant ClaimantNamed(string name)
        {
            foreach (Claimant c in this.Claimants)
            {
                if (c.Name.ToUpper() == name.ToUpper())
                    return c;
            }
            return null;
        }

        public string ClaimantNamesSeparatedByComma()
        {
            string s = "";
            foreach (string c in this.ClaimantNames())
            {
                if (s == "")
                    s = c;
                else
                    s += ", " + c;
            }
            return s;
        }

        public string AgentDescription
        {
            //returns agent description for claim
            get {
                Hashtable agent = null;
                string s = "";
                List<Hashtable> rows = db.ExecuteStoredProcedureReturnHashList("usp_Get_Agent_For_Claim", this.Id);

                if (rows.Count == 0)
                    return "";

                agent = rows[0];
                s = (string)agent["Name_Firm1"];

                if ((string)agent["Name_Firm2"] != "")
                    s += Environment.NewLine + (string)agent["Name_Firm2"];
                if ((string)agent["Address1"] != "")
                    s += Environment.NewLine + (string)agent["Address1"];
                if ((string)agent["Address2"] != "")
                    s += Environment.NewLine + (string)agent["Address2"];
                if (((string)agent["Name_City"] != "") && ((string)agent["Name_State"] != "") && ((string)agent["Cd_zip"] != ""))
                    s += Environment.NewLine + (string)agent["Name_City"] + ", " + (string)agent["Name_State"] + " " + (string)agent["Cd_zip"];
                if ((string)agent["Phone_Bus"] != "")
                    s += Environment.NewLine + "Phone: " + (string)agent["Phone_Bus"];
                if ((string)agent["Phone_Fax"] != "")
                    s += Environment.NewLine + "Fax: " + (string)agent["Phone_Fax"];
                return s;
            }
        }

        public string AdjusterDescription
        {
            get
            {
                string s = "";
                User adjuster = this.Adjuster;

                s = adjuster.Name;
                if (!string.IsNullOrWhiteSpace(adjuster.PhoneExt))
                    s += Environment.NewLine + "Ph: x " + adjuster.PhoneExt;
                if (!string.IsNullOrWhiteSpace(adjuster.DirectFax))
                    s += Environment.NewLine + "Fax: " + adjuster.DirectFax;
                if (!string.IsNullOrWhiteSpace(adjuster.Email))
                    s += Environment.NewLine + "Email: " + adjuster.Email;

                return s;
            }
        }


        public bool HasUnassignedReserves()
        { 
            foreach (Reserve r in this.Reserves())
            { 
                if (r.AdjusterId == 0)
                    return true;

            }

            return false;
        }

        public ArrayList Claimants
        {
            //returns collection of claimant objects for claim
            get
            {
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Claimant_Rows", this.Id);
                ArrayList claimantObj = new ArrayList();

                foreach (DataRow row in rows.Rows)
                    claimantObj.Add(new Claimant(rows, row));
                return claimantObj;
            }
            set { this.setRowValue("DateOfLoss", value); }
        }

        public DataTable AlertList
        {
            //Get alerts for claim and current user
            get { return db.ExecuteStoredProcedure("usp_Get_Alerts_For_Claim_And_User", this.Id, Convert.ToInt32(HttpContext.Current.Session["userID"])); }
        }

        public override ArrayList Alerts()
        {
            //returns alert objects for claim and current user
            ArrayList a = new ArrayList();
            DataTable myAlerts = this.AlertList;
            foreach (DataRow row in myAlerts.Rows)
                a.Add(new Alert(myAlerts, row));
            return a;
        }

        public Claimant Claimaint(int personId)
        {
            return new Claimant(ClaimantId(personId));
        }

        public int ClaimantId(int personId)
        {
            //returns claimant ID for person ID
            return db.GetIntFromStoredProcedure("usp_Get_Claimant_Id", this.Id, personId);
        }

        public int ClaimantId(string personName)
        {
            //returns claimant ID for person name
            return ClaimantId(this.PersonNamed(personName).Id);
        }

        public bool ExistingPropertyDamageForOwner(int ownerId)
        {
            //Does owner already have an existing APD reserve line?
            Claimant claimant = this.Claimaint(ownerId);
            if (!claimant.isValid)
                return false;

            //check for reserve line
            return claimant.ReserveExists((int)modGeneratedEnums.ReserveType.Property_Damage) ? true : false;
        }

        public bool ExistingPropertyDamageForOwnerOfReserveTypeAndPropertyType(int claimantID, int reserveTypeID, int propertyTypeID, int ownerID)
        {
            //Does a PD reserve exist for the specified owner, reserve type and property type?
            return db.GetIntFromStoredProcedure("usp_Property_Damage_Of_Type_Exists_For_Claim", this.Id, reserveTypeID, propertyTypeID, ownerID) == 1 ? 
                true : false;
        }

        public bool ExistingReserveOfReserveType(int reserveTypeID)
        {
            //Does reserve exist for this claim that is of the specified reserve type?
            return db.GetIntFromStoredProcedure("usp_Reserve_Exists_For_Claim_Of_Reserve_Type", this.Id, reserveTypeID) == 1 ? true : false;
        }

        public Person PersonNamed(string personName)
        {
            return Person.Named(personName, this.Id);
        }

        public Vehicle VehicleNamed(string vehicleName)
        {
            return Vehicle.Named(vehicleName, this.Id);
        }

        public AppraisalRequest CreateAppraisalRequest()
        {
            //Creates AppraisalRequest object - not sent to DB
            AppraisalRequest ar = new AppraisalRequest();
            ar.ClaimId = this.Id;
            return ar;
        }

        public int InsertClaimant(int personId)
        {
            //insert claimant
            Claimant cl = new Claimant();

            cl.ClaimId = this.Id;
            cl.PersonId = personId;
            int claimantId = cl.Update();

            //Update unknown persons
            Person p = new Person(personId);
            p.UpdateUnknownPersons();

            //return new claimant ID
            return claimantId;
        }

        private void InsertDeductibleCoverage(int policyCoverageTypeId, double deductibleAmt)
        {
            db.ExecuteStoredProcedure("usp_Insert_Deductable_Coverage", this.Id, this.VersionNo, policyCoverageTypeId, deductibleAmt);
        }

        private void InsertPerAccidentCoverage(int policyCoverageTypeId, double perAccidentCoverage)
        {
            db.ExecuteStoredProcedure("usp_Insert_Per_Accident_Coverage", this.Id, this.VersionNo, policyCoverageTypeId, perAccidentCoverage);
        }

        private void InsertPerPersonAccidentCoverage(int policyCoverageTypeId, double perPersonCoverage, double perAccidentCoverage)
        {
            db.ExecuteStoredProcedure("usp_Insert_Per_Person_Accident_Coverage", this.Id, this.VersionNo, policyCoverageTypeId, perPersonCoverage, perAccidentCoverage);
        }

        private void InsertPerPersonCoverage(int policyCoverageTypeId, double perPersonCoverage)
        {
            db.ExecuteStoredProcedure("usp_Insert_Per_Person_Coverage", this.Id, this.VersionNo, policyCoverageTypeId, perPersonCoverage);
        }

        public void InsertUnknownPersonIfNecessary(int unknownNo)
        {
            //inserts unknown person, if they don't exist already
            if (!this.UnknownPersonExists(unknownNo))
            {
                Person p = new Person();
                p.FirstName = "Unknown " + unknownNo;
                p.PolicyIndividualId = 0;
                p.PersonTypeId = 0;
                p.Address1 = "";
                p.Address2 = "";
                p.City = "";
                p.StateId = 0;
                p.Zipcode = "";
                p.isCompany = false;
                p.ClaimId = this.Id;
                p.LanguageId = (int)modGeneratedEnums.Language.English;
                p.Update();
            }
        }

        public double MaxDeductible
        {
            get
            {
                double max = 0;

                foreach (PolicyCoverage p in this.PolicyCoverages)
                {
                    if (p.DeductibleAmount > max)
                        max = p.DeductibleAmount;
                }
                return max;
            }
        }

        public DataTable InjuredRows()
        {
            //returns injured row for this claim - for display in a list view.
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Injured_With_Images", this.Id);
        }

        public ArrayList Injured()
        {
            ArrayList a = new ArrayList();
            foreach (DataRow row in this.InjuredRows().Rows)
                a.Add(new Injured((int)row["Injured_Id"]));
            return a;
        }

        public PolGeneralInfo Policy()
        {
            return this._policy == null ? this._policy = new PolGeneralInfo(this.PolicyNo, this.VersionNo.ToString()) : this._policy;
        }

        public List<PolicyCoverage> PolicyCoverages
        {
            get
            {
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Policy_Coverages_For_Claim", this.Id);
                List<PolicyCoverage> a = new List<PolicyCoverage>();

                foreach (DataRow row in rows.Rows)
                    a.Add(new PolicyCoverage(rows, row));
                return a;
            }
        }

        public string PolicyDates
        {
            //returns policy effective and expiration dates for a claim as a string
            get
            {
                return this.myPolicyDates == "" ? this.myPolicyDates = db.GetStringFromStoredProcedure("usp_PolicyDates", this.Id) : this.myPolicyDates;
            }
        }

        public DateTime PolicyEffectiveDate()
        {
            //returns the policy effective date
            string[] Dates = this.PolicyDates.Split('-');  //get the policy dates
            string myDate = Dates[0].Trim(); //get the correct date

            //return date only if it is correct
            return ml.isDate(myDate) ? DateTime.Parse(myDate) : ml.DEFAULT_DATE;
        }

        public DateTime PolicyExpirationDate()
        {
            //returns the policy expiration date
            string[] Dates = this.PolicyDates.Split('-');  //get the policy dates
            string myDate = Dates[1].Trim(); //get the correct date

            //return date only if it is correct
            return ml.isDate(myDate) ? DateTime.Parse(myDate) : ml.DEFAULT_DATE;
        }

        public DataTable PropertyDamageRows
        {
            get
            {
                DataTable a = db.ExecuteStoredProcedureReturnDataTable("usp_Get_Property_Damages", this.Id);

                //foreach (Hashtable row in a)
                //    row["Estimated_Damage_Amount"] = ((double)((decimal)row["Estimated_Damage_Amount"])).ToString("C");
                return a;
            }
        }

        public ArrayList PropertyDamages()
        {
            //returns arraylist of property damage objects for a claim
            ArrayList a = new ArrayList();
            foreach (Hashtable row in this.PropertyDamageRows.Rows)
                a.Add(new PropertyDamage((int)row["Property_Damage_Id"]));
            return a;
        }

        private void RefreshPolicyCoverages()
        {
            //insert policy coverages from Personal Lines into Policy_Coverage table
            Hashtable _policyCoverages = new Hashtable();
            DataTable c = null;
            bool hasPipCoverage = false;

            //delete existing policy coverage rows for claim
            db.ExecuteStoredProcedure("usp_Delete_Policy_Coverage", this.Id);

            if (!this.isExpiredPolicy) //only get coverage amounts if the policy is not expired 
            {
                c = db.ExecuteStoredProcedure("usp_Get_Coverage", this.Id);
                hasPipCoverage = false;

                foreach (DataRow d in c.Rows)
                {
                    switch (((string)d["cd_cov"]).ToUpper().Trim())
                    {
                        case "APD":
                            InsertPerAccidentCoverage((int)modGeneratedEnums.PolicyCoverageType.Property_Damage, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()));
                            break;
                        case "ABI":
                            InsertPerPersonAccidentCoverage((int)modGeneratedEnums.PolicyCoverageType.Auto_Bodily_Injury, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()), double.Parse(d["cd_limit2"].ToString().Replace("$", "").Trim()));
                            break;
                        case "AMP":
                            InsertPerPersonCoverage((int)modGeneratedEnums.PolicyCoverageType.Medical_Payments, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()));
                            break;
                        case "AUM":
                            InsertPerPersonAccidentCoverage((int)modGeneratedEnums.PolicyCoverageType.Uninsured_Motorist, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()), double.Parse(d["cd_limit2"].ToString().Replace("$", "").Trim()));
                            break;
                        case "AUN":
                            InsertPerPersonAccidentCoverage((int)modGeneratedEnums.PolicyCoverageType.Underinsured_Motorist, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()), double.Parse(d["cd_limit2"].ToString().Replace("$", "").Trim()));
                            break;
                        case "AUUM":
                            InsertPerPersonAccidentCoverage((int)modGeneratedEnums.PolicyCoverageType.UM_UIM, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()), double.Parse(d["cd_limit2"].ToString().Replace("$", "").Trim()));
                            break;
                        case "ACOL":
                            InsertDeductibleCoverage((int)modGeneratedEnums.PolicyCoverageType.Collision, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()));
                            break;
                        case "ACMP":
                            InsertDeductibleCoverage((int)modGeneratedEnums.PolicyCoverageType.Comprehensive, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()));
                            break;
                        case "APHY":
                            InsertDeductibleCoverage((int)modGeneratedEnums.PolicyCoverageType.Comp_Coll, double.Parse(d["cd_limit1"].ToString().Replace("$", "").Trim()));
                            break;
                        case "APIP": //for PIP: insert PIP coverages for state from state PIP table
                            hasPipCoverage = true;
                            db.ExecuteStoredProcedure("usp_Insert_Policy_Coverage_From_State_Pip", this.Id, this.VersionNo, d["cd_state"], Convert.ToInt32(HttpContext.Current.Session["userID"]));
                            break;
                        case "AGLS":
                            InsertDeductibleCoverage((int)modGeneratedEnums.PolicyCoverageType.Glass, 0);
                            break;
                        default:
                            //System.Windows.MessageBox.Show("Error - Unknown Personal Lines Coverage Type -" + (string)d["cd_cov"]);
                            break;
                    }
                }
                if (!hasPipCoverage && (c.Rows.Count > 0))
                {
                    //inserts PIP coverage no periods and period type, but leaves per-person/per-accident coverage amounts as 0.
                    foreach (DataRow d in c.Rows)
                        db.ExecuteStoredProcedure("usp_Insert_No_Policy_Coverage_From_State_Pip", this.Id, d["cd_state"], Convert.ToInt32(HttpContext.Current.Session["userID"]));
                    //seems we are adding coverage, then if not PIP, we are removing it
                }
            }
        }

        public void RefreshPolicyData()
        {
            RefreshPolicyIndividuals();
            RefreshPolicyVehicles();
            RefreshPolicyLossPayees();
            RefreshPolicyCoverages();
        }

        private void RefreshPolicyIndividuals()
        {
            /* Refresh policy individuals from PLA.  If there is no named insured in the Pol_individuals
             * table, determine named insured by the Name_Insd column in teh Pol_GeneralInfo table.
            */
            string[] names;
            string firstName = "";
            string lastName = "";
            DataTable persons = null;

            db.ExecuteStoredProcedure("usp_Refresh_Policy_Individuals", this.Id, this.PolicyNo, this.VersionNo, Convert.ToInt32(HttpContext.Current.Session["userID"]));
            if (db.GetBoolFromStoredProcedure("usp_Is_Garaging_Address", this.Id))
                this.FireEvent((int)modGeneratedEnums.EventType.Garaging_Address_Different_from_Mailing_Address);
            else
                this.ClearEvent((int)modGeneratedEnums.EventType.Garaging_Address_Different_from_Mailing_Address);

            if (this.NamedInsuredExists)
                return;

            //get named insured from Pol_GeneralInfo table
            names = this.Policy().NameInsd == null ? "".Split(' ') : this.Policy().NameInsd.Split(' ');

            if (names.Length-1<1)
                return;
                //there is no first and last name, so use first person

            firstName = names[0];
            lastName = names[names.Length - 1];

            //get PLA persons on claim
            persons = db.ExecuteStoredProcedure("usp_Get_Claim_Policy_Persons", this.Id);
            if (persons.Rows.Count == 0)
                return;
                //nothing we can do - nobody is on the policy

            //match on first/last name
            foreach (DataRow personRow in persons.Rows)
            {
                if (personRow["Last_Name"].ToString().Trim().ToUpper() == lastName.Trim().ToUpper())
                {
                    Person.SetToNamedInsured((int)personRow["Person_Id"]);
                    return; //we have matched by last name and set named insured for claim
                }
            }

            //have not matched on first/last name - pick first person in the list
            if (persons != null)
            {
                foreach (DataRow personRow in persons.Rows)
                {
                    Person.SetToNamedInsured((int)personRow["Person_Id"]);
                    return; //set the first person in the list to the named insured
                }
            }
        }

        public bool NamedInsuredExists
        {
            //Does named insured exist on this claim
            get { return db.GetBoolFromStoredProcedure("usp_Named_Insured_Exists", this.Id); }
        }

        private void RefreshPolicyLossPayees()
        {
            db.ExecuteStoredProcedure("usp_Refresh_Policy_Loss_Payees", this.Id, this.PolicyNo, this.VersionNo, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        private void RefreshPolicyVehicles()
        {
            db.ExecuteStoredProcedure("usp_Refresh_Policy_Vehicles", this.Id, this.PolicyNo, this.VersionNo, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        }

        public bool ExistingWitnessPassenger(int personId)
        {
            //Is there an existing witness/passenger on claim? - prevents duplicates
            return db.GetIntFromStoredProcedure("usp_Existing_Witness_Passenger", this.Id, personId) > 0 ? true : false;
        }

        public bool ExistingClaimant(int personId)
        {
            //Is there an existing claimant? - prevents duplicates
            return db.GetIntFromStoredProcedure("usp_Existing_Claimant", this.Id, personId) > 0 ? true : false;
        }

        public bool ExistingInjured(int personId)
        {
            //Is there and existing injured? - prevents duplicates
            return db.GetIntFromStoredProcedure("usp_Existing_Injured", this.Id, personId) > 0 ? true : false;
        }

        public double LossForCoverageType(int reserveTypeId)
        {
            //Total loss amount for this claim ID and coverage type.  Compare new loss amount against per-accident limits.
            return db.GetDblFromStoredProcedure("usp_Get_Loss_For_Claim_And_Coverage_Type", this.Id, reserveTypeId);
        }

        public double NetLossReserveForCoverageType(int reserveTypeId)
        {
            //Total net loss reserve amount for the claim ID and coverage type.  Compare new loss amount against per-accident limits.
            return db.GetDblFromStoredProcedure("usp_Get_Net_Loss_Reserve_For_Claim_And_Coverage_Type", this.Id, reserveTypeId);
        }

        public List<Hashtable> ReserveRows
        {
            //returns collection of Reserve rows for claim ID
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Reserves_For_Claim", this.Id); }
        }

        public override ArrayList Reserves()
        {
            //returns collection of reserve objects for Claim_Id
            List<Hashtable> rows = this.ReserveRows;
            ArrayList a = new ArrayList();
            foreach (Hashtable row in rows)
                a.Add(new Reserve(row));
            return a;
        }

        public List<Hashtable> ReservesWithImages()
        {
            //returns collection of Reserve objects for Claim_Id
            return db.ExecuteStoredProcedureReturnHashList("usp_Get_Reserves_With_Images", this.Id);
        }

        public ArrayList ReservesForReserveType(int reserveTypeId)
        {
            ArrayList a = new ArrayList();
            DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claim_And_Reserve_Type", this.Id, reserveTypeId);
            foreach (DataRow row in myReserves.Rows)
                a.Add(new Reserve(myReserves, row));
            return a;
        }

        public DataTable TransactionsRows
        {
            //returns collection of Transaction rows for Claim_Id
            get { return db.ExecuteStoredProcedure("usp_Get_Transactions_For_Claim", this.ClaimId); }
        }

        public ArrayList Transactions
        {
            get
            {
                ArrayList a = new ArrayList();
                DataTable myTransactions = this.TransactionsRows;
                foreach (DataRow row in myTransactions.Rows)
                    a.Add(new Transaction(myTransactions, row));
                return a;
            }
        }

        public bool UnknowVehicleExists(int unknownNo)
        {
            return db.GetBoolFromStoredProcedure("usp_Unknown_Vehicle_Exists", this.Id, unknownNo);
        }

        public bool UnknownPersonExists(int unknownNo)
        {
            return db.GetBoolFromStoredProcedure("usp_Unknown_Person_Exists", this.Id, unknownNo);
        }

        public List<Hashtable> VehicleNames
        {
            //returns a collection of vehicle names for this claim
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Vehicle_Names", this.Id); }
        }

        public List<Hashtable> Vehicles
        {
            //returns a collection of vehicles associated with this claim
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Claim_Vehicles", this.Id); }
        }

        public int NextDraftNo
        {
            //returns next draft number for claim
            get { return db.GetIntFromStoredProcedure("usp_Get_Next_Draft_No", this.Id); }
        }

        public bool InsuredPhoneNumberExists
        {
            get { return db.GetBoolFromStoredProcedure("usp_Insured_Phone_Number_Exists", this.Id); }
        }

        public bool ClaimantPhoneNumberExists
        {
            get { return db.GetBoolFromStoredProcedure("usp_Claimant_Phone_Number_Exists", this.Id); }
        }

        public int NoUnprintedDraftsForClaim
        {
            get { return this.UnprintedDraftsForClaim.Rows.Count; }
        }

        public DataTable UnprintedDraftsForClaim
        {
            //returns unprinted drafts for claim
            get { return db.ExecuteStoredProcedure("usp_Get_Unprinted_Drafts_For_Claim", this.Id); }
        }

        public bool PolicyManuallyInForce
        {
            //Is claim locked with "Policy Not in Force for Date of Loss" and has been manually unlocked with no verifiable coverage?
            get { return db.GetBoolFromStoredProcedure("usp_Is_Policy_Manually_In_Force", this.Id); }
        }

        public List<Hashtable> PersonDescriptions
        {
            //returns a collection of persons descriptions for a claim
            //(insured, owner, driver, PD owners and drivers, claimants, etc.)
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Person_Descriptions_For_Claim", this.Id); }
        }

        public List<Hashtable> InsuredDescription
        {
            //returns a collection of persons descriptions for a claim
            //(insured, owner, driver, PD owners and drivers, claimants, etc.)
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Insured_Description_For_Claim", this.Id); }
        }

        public bool UnclearedDiariesExist
        {
            //Is there any uncleared diary entries for a claim?
            get { return UnclearedDiaryList.Count == 0 ? false : true; }
        }

        public List<Hashtable> UnclearedDiaryList
        {
            //returns uncleared diary entries for this claim - for display in a list view.
            get { return db.ExecuteStoredProcedureReturnHashList("usp_Get_Uncleared_Diary_Entries", this.Id); }
        }

        public bool hasLanguageIssueForAdjuster(int adjusterId)
        {
            //Are there any persons on the claim speaking a language the adjuster doesn't understand?
            return db.GetIntFromStoredProcedure("usp_No_Claim_Persons_With_Language_Issues", this.Id, adjusterId) == 0 ? 
                false : true;
        }

        public bool RequiresLanguageSkillsFromAdjuster(int adjusterId)
        {
            //Are there any persons on the claim speaking a language the adjuster doesn't understand?
            return db.GetBoolFromStoredProcedure("usp_Claim_Requires_Language_Skills_From_Adjuster", this.ClaimId, adjusterId);
        }

        public void Close()
        {
            //Clear diary entries for claim
            db.ExecuteStoredProcedure("usp_Clear_Diary_Entries_For_Claim", this.Id);

            //Update claim row
            this.CloseDate = ml.DBNow();
            this.ClaimStatusId = (int)modGeneratedEnums.ClaimStatus.Claim_Closed;
            this.NoSupplementalPayments = 0;
            this.Update();

            //fire claim closed event
            this.FireEvent((int)modGeneratedEnums.EventType.Claim_Closed);
        }

        public string LossPayeeDescription
        {
            //returns the loss payee description (name, address, phone, etc.) as one string with embedded crlfs
            get { return this.InsuredVehicle.LossPayeeDescription(); }
        }

        public ArrayList AuditItems
        {
            //returns array list of ClaimAuditItems for this claim
            get
            {
                ArrayList items = new ArrayList();
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Claim_Audit_Items", this.Id);
                foreach (DataRow row in rows.Rows)
                    items.Add(new ClaimAuditItem(rows, row));
                return items;
            }
        }

        public ArrayList AuditCategories
        {
            //returns arraylist of ClaimAuditCategories for this claim
            get 
            { 
                ArrayList categories = new ArrayList();
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Claim_Audit_Categories", this.Id);
                foreach (DataRow row in rows.Rows)
                    categories.Add(new ClaimAuditCategory(rows, row));
                return categories;
            }
        }

        public bool setFoxproClaimNumber(string lfn)
        {
            return db.GetBoolFromStoredProcedure("usp_Set_Foxpro_Claim_Number", this.Id, lfn);
        }

        public void InsertRecentClaim(int userId)
        {
            db.ExecuteStoredProcedure("usp_Insert_Recent_Claim", this.Claim.Id, userId);
        }

        public int InsertFileNote(int fileNoteTypeId, string fileNoteText)
        {
            //inserts file note for claim and returns new file note ID
            FileNote f = new FileNote();
            f.FileNoteTypeId = fileNoteTypeId;
            f.FileNoteText = fileNoteText;
            f.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            f.ClaimId = this.ClaimId;
            return f.Update();
        }

        public ClaimLock LatestLockForEventType(int eventTypeId)
        {
            int claimLockId = db.GetIntFromStoredProcedure("usp_Get_Latest_Claim_Lock_For_Event_Type", this.Id, eventTypeId);
            if (claimLockId == 0)
                return null;

            return new ClaimLock(claimLockId);
        }

        public void UpdateNamedInsured()
        {
            //Updates the named insured on the claim.  This will be called if the Policy changed.  
            //Also updates any named insured claimants (comp or collision).
            int newInsuredPersonId = db.GetIntFromStoredProcedure("usp_Get_Named_Insured", this.Id);
            db.ExecuteStoredProcedure("usp_Update_Claimant_Persons", this.Id, this.InsuredPersonId, newInsuredPersonId);
            this.InsuredPersonId = newInsuredPersonId;
            this.Update();
        }

        public void UpdateCompanyLocationId(string companyName, bool associateWithNewPolicy)
        {
            int newCompanyLocationId = db.GetIntFromStoredProcedure("usp_Get_Company_Locations", companyName);
            db.ExecuteStoredProcedure("usp_Update_Claim_Company_Location_Id", this.ClaimId, this.CompanyLocationId, newCompanyLocationId);
            this.CompanyLocationId = newCompanyLocationId;
            this.Update();
            if (!associateWithNewPolicy)
                this.UpdateDisplayClaimId(newCompanyLocationId);

            this.FireEvent((int)modGeneratedEnums.EventType.Claim_Updated);
        }

        public void UpdateDisplayClaimId(int iCompanyLocationId)
        {
            int newCompanyId = db.GetIntFromStoredProcedure("usp_Get_Company_Id_For_Claim_Id", this.ClaimId);
            string newCompanyLocationAbbreviation=db.GetStringFromStoredProcedure("usp_Get_Company_Location_Abbreviation", iCompanyLocationId);
            string newCompanyAbbreviation = db.GetStringFromStoredProcedure("usp_Get_Company_Abbreviation_For_Company_Id", newCompanyId);
            string newDisplayClaimId = newCompanyAbbreviation + newCompanyLocationAbbreviation + this.ClaimId;
            this.DisplayClaimId = newDisplayClaimId;
            this.Update();
        }

        public void Lock(int fileNoteId)
        {
            //Locks claim file
            ClaimLock cl = new ClaimLock();
            cl.ClaimId = this.Id;
            cl.isLocked = true;
            cl.LockFileNoteId = fileNoteId;
            cl.LockDate = ml.DBNow();
            cl.ConditionStillExists = true;
            cl.Update();
        }

        public void ClearEvent(int eventTypeId)
        {
            /*Clears event for a claim.
             * Delete any alerts associated with this event.
             * Inactivate any locks associated with this event.
             * Set lock Condition_Exists column to false
            */
            int fileNoteId;
            EventType et = new EventType(eventTypeId);
            DataTable myLockRows = db.ExecuteStoredProcedure("usp_Delete_Claim_Alerts_And_Get_Claim_Locks", this.Id, eventTypeId);

            foreach (DataRow claimLockRow in myLockRows.Rows)
            {
                ClaimLock cl = new ClaimLock(myLockRows, claimLockRow);

                if (cl.isLocked)  //claim is locked - unlock it
                {
                    fileNoteId = Claim.InsertFileNote((int)modGeneratedEnums.FileNoteType.Unlock_Claim, 
                        "System Unlocked Claim " + Claim.DisplayClaimId + " Because Condition '" + et.Description + "' No Longer Exists.");

                    cl.isLocked = false;
                    cl.UnlockFileNoteId = fileNoteId;
                    cl.UnlockDate = ml.DBNow();
                    cl.ConditionStillExists = false;
                    cl.Update();
                }
                else  //claim is not locked - set Condition Still Exists to false
                {
                    cl.ConditionStillExists = false;
                    cl.Update();
                }
            }
        }

        public bool FireEvent(int eventTypeId, Hashtable parms = null)
        {
            /*Fire event which is relevant to a claim (Close Claim, Create Claim, etc.)
             * eventTypeId is the event type (Close Claim, Create Claim, etc.)
            */

            EventType et = new EventType(eventTypeId);
            ScriptParms sp = new ScriptParms(parms);
            sp.GenerateParms(this);

            if (et.FireEvent(sp.Parms))
                return true;
            return false;
        }

        public bool FireEventIfSyncExists(int checkEventTypeId, int eventTypeId)
        {
            //Fire claim event if sync exists for other event
            return this.SyncEventExists(checkEventTypeId) ? this.FireEvent(eventTypeId) : false;
        }

        public void ClearSyncEvents()
        {
            /*Clears all sync events for claim and its reserve lines and transactions.
             * This is necessary before changing the policy or version number on a claim
             * for th sync'ing to work right.
            */

            //Clear Claim Sync events
            this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver);
            this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore);
            this.ClearEvent((int)modGeneratedEnums.EventType.Flat_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.Flat_Cancellation_No_Longer_Applies);
            this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Not_On_Policy);
            this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Now_On_Policy);
            this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy);
            this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy);
            this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_No_Longer_Applies);
            this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_Found);
            this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Now_Found);
            this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);
            this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss);
            this.ClearEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation_No_Longer_Applies);
            this.ClearEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation_No_Longer_Applies);

            //Clear Reserve Sync events
            foreach(Reserve r in this.Reserves())
            {
                r.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Doesnt_Exceed_Per_Accident_Coverage_Anymore);
                r.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Doesnt_Exceed_Per_Person_Coverage_Anymore);
                r.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage);
                r.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage);
            }

            //Clear Transaction Sync events
            foreach (Transaction t in this.Transactions)
            {
                t.ClearEvent((int)modGeneratedEnums.EventType.Draft_Amount_Doesnt_Exceed_Per_Accident_Coverage_Anymore);
                t.ClearEvent((int)modGeneratedEnums.EventType.Draft_Amount_Doesnt_Exceed_Per_Person_Coverage_Anymore);
                t.ClearEvent((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Accident_Coverage);
                t.ClearEvent((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage);
            }
        }

        public void AssociateWithNewPolicy(string policyNo, string versionNo, bool isExpired)
        {
            //Associate claim with new policy
            this.ClearSyncEvents(); //removes old sync data a locks

            this.PolicyNo = policyNo;
            this.VersionNo = Convert.ToDouble(versionNo);
            this.isExpiredPolicy = isExpired;
            this.UpdateNamedInsured();
            this.FireEvent((int)modGeneratedEnums.EventType.Associate_Claim_With_New_Policy);

            //If now assocated with a KS policy, fix any injured coverages to now include PIP
            if ((policyNo.ToUpper().Substring(0, 3) == "BFP") || (policyNo.ToUpper().Substring(0, 3) == "AKS"))
                db.ExecuteStoredProcedure("usp_Fix_Pip_Coverage_For_Claim_Id", this.Id);
        }

        public void Reopen()
        {
            //Reopen claim.  Update the claim status to reopened and the reopen date to now.

            //Update Claim row
            this.ClaimStatusId = (int)modGeneratedEnums.ClaimStatus.Claim_Reopened;
            this.ReopenDate = ml.DBNow();
            this.Update();

            //insert history row
            this.UpdateHistory();

            //Fire event
            this.FireEvent((int)modGeneratedEnums.EventType.Claim_Reopened);
        }

        public bool ClaimantExists(int personId)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Claimant_Exists", this.Id, personId);
            if (rows.Rows.Count == 0)
                return false;

            DataRow row = rows.Rows[0];
            if ((int)row["c"] == 0)
                return false;
            return true;
        }

        public int CreateClaimantIfNecessary(int personId)
        {
            /*If a claimant corresponding to the passed personId/PartyId doesn't exist, create one
             * and return the claimant ID.  If a claimant does exist for the person/party, 
             * return its claimant ID.
            */

            return this.ClaimantExists(personId) ? this.ClaimantId(personId) : this.InsertClaimant(personId);
        }

        public PolicyCoverage getPolicyCoverage(int policyCoverageTypeId)
        {
            //returns policy coverage for coverage type
            if (!_policyCoverages.ContainsKey(policyCoverageTypeId))
            {
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Policy_Coverage", this.Id, policyCoverageTypeId);
                PolicyCoverage pc = null;

                if (rows.Rows.Count > 0)
                {
                    DataRow row = rows.Rows[0];
                    pc = new PolicyCoverage(rows, row);
                }
                _policyCoverages.Add(policyCoverageTypeId, pc);
            }
            return (PolicyCoverage)_policyCoverages[policyCoverageTypeId];
        }

        public string ClearCancellationEvents()
        {
            //Clear Claim cancellation events (NSF, Pro-Rate, Short-Rate, etc.) - returns message to display
            string msg = "";

            if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Short_Rate_Cancellation, (int)modGeneratedEnums.EventType.Short_Rate_Cancellation_No_Longer_Applies))
                msg += "Short Rate Cancellation No Longer Applies to Policy" + Environment.NewLine;
            if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation, (int)modGeneratedEnums.EventType.Pro_Rate_Cancellation_No_Longer_Applies))
                msg += "Pro-Rate Cancellation No Longer Applies to Policy" + Environment.NewLine;
            if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Flat_Cancellation, (int)modGeneratedEnums.EventType.Flat_Cancellation_No_Longer_Applies))
                msg += "Flat Cancellation No Longer Applies to Policy" + Environment.NewLine;
            if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.NSF_Cancellation, (int)modGeneratedEnums.EventType.NSF_Cancellation_No_Longer_Applies))
                msg += "NSF Cancellation No Longer Applies to Policy" + Environment.NewLine;

            this.ClearEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.Flat_Cancellation);
            this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation);

            return msg;
        }

        public string ClearWorkInProgressEvents(string policyNo, string versionNo, int excludeEventTypeId)
        {
            //Clears all WIP events and fires corresponding clear events (except for the one excluded) - returns message to display
            string msg = "";

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel, (int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel_No_Longer_Applies))
                    msg += "Holding Bad Data See Mel No Longer Applies: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress, (int)modGeneratedEnums.EventType.Change_To_Policy_In_Progress_No_Longer_Applies))
                    msg += "Change to Policy in Progress No Longer Applies: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Payment_Posting_Error)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Payment_Posting_Error, (int)modGeneratedEnums.EventType.Payment_Posting_Error_No_Longer_Applies))
                    msg += "Payment Posting Error No Longer Applies: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Payment_Posting_Error);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Policy_Has_Expired)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Has_Expired, (int)modGeneratedEnums.EventType.Policy_Has_Expired_No_Longer_Applies))
                    msg += "Policy is No Longer Expired: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Has_Expired);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Policy_Is_Cancelled)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Is_Cancelled, (int)modGeneratedEnums.EventType.Policy_Is_Cancelled_No_Longer_Applies))
                    msg += "Policy is No Longer Cancelled: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Is_Cancelled);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Policy_Is_Inactive)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Is_Inactive, (int)modGeneratedEnums.EventType.Policy_Is_Inactive_No_Longer_Applies))
                    msg += "Policy is No Longer Inactive: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Is_Inactive);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Waiting_Final_Rating)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Waiting_Final_Rating, (int)modGeneratedEnums.EventType.Waiting_Final_Rating_No_Longer_Applies))
                    msg += "Policy is No Longer Waiting Final Rating: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Waiting_Final_Rating);
            }

            if (excludeEventTypeId != (int)modGeneratedEnums.EventType.Waiting_For_New_Business)
            {
                if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Waiting_For_New_Business, (int)modGeneratedEnums.EventType.Waiting_For_New_Business_No_Longer_Applies))
                    msg += "Policy is No Longer Waiting For New Business: Policy " + policyNo + " Version " + versionNo + Environment.NewLine;
                this.ClearEvent((int)modGeneratedEnums.EventType.Waiting_For_New_Business);
            }

            return msg;
        }

        public void InsertUnknownVehicleIfNecessary(int unknownNo)
        {
            if (this.UnknowVehicleExists(unknownNo))
            {
                Vehicle v = new Vehicle();
                v.Manufacturer = "Unknown " + unknownNo;
                v.ClaimId = ClaimId;
                v.Update();
            }
        }

        public bool hasDeductible(int policyCoverageTypeId)
        {
            PolicyCoverage pc = this.getPolicyCoverage(policyCoverageTypeId);
            if (pc == null)
                return false;
            return pc.hasDeductible;
        }

        public double CollisionDeductible()
        {
            PolicyCoverage pc = this.getPolicyCoverage((int)modGeneratedEnums.PolicyCoverageType.Collision);

            if (pc == null)
            {
                pc = this.getPolicyCoverage((int)modGeneratedEnums.PolicyCoverageType.Comp_Coll);
                if (pc == null)
                    return 0;
            }
            return pc.DeductibleAmount;
        }

        public double ComprehensiveDeductible()
        {
            PolicyCoverage pc = this.getPolicyCoverage((int)modGeneratedEnums.PolicyCoverageType.Comprehensive);

            if (pc == null)
            {
                pc = this.getPolicyCoverage((int)modGeneratedEnums.PolicyCoverageType.Comp_Coll);
                if (pc == null)
                    return 0;
            }
            return pc.DeductibleAmount;
        }

        public string CoverageDescriptions(int policyCoverageTypeId)
        {
            //get covrage description for a policy coverage type (ABI, APD, etc.).  if no coverage - return empty string
            return db.GetStringFromStoredProcedure("usp_Coverage_Description", this.Id, policyCoverageTypeId);
        }

        public ClaimReport ClaimReport()
        {
            return new ClaimReport(db.GetIntFromStoredProcedure("usp_Get_Claim_Report_Id", this.Id));
        }

        public void populateClaimReport()
        {
            db.ExecuteStoredProcedure("usp_Populate_Claim_Report", this.Id, Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //populate the rest of the columns
            this.ClaimReport().PopulateFromClaim(this);
        }

        public string SyncPolicy(bool associatingWithNewPolicy = false)
        {
            //synchronize the claim with the policy

            //switch (this.ClaimId)
            //{
            //    case (int)modGeneratedEnums.ClaimType.Auto:
                    //sync claim with PLA.  Return any problems as an error message string.
                    _coverageDescriptions = null;
                    string msg = "";
                    int insPersonId = 0;
                    int drivPersonId = 0;
                    Hashtable coverages = null;
                    Hashtable coverage = null;
                    string versionNo = "";
                    DateTime dateOfLoss;

                    //get insured vehicle, person and drive IDs
                    Vehicle insVehicle = this.InsuredVehicle;
                    insPersonId = this.InsuredPersonId;
                    drivPersonId = this.DriverPersonId;
                    Person drivPerson = this.DriverPerson;
                    Person insPerson = this.InsuredPerson;

                    if (this.PolicyNo.Trim() == "")
                        return msg;

                    if(!this.PolicyExists)
                    {
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Not_Found))
                            msg += "Policy " + this.PolicyNo + " Not found in Personal Lines" + Environment.NewLine;

                        //if policy isn't found in PLA, we don't need the other PLA related locks to be set (NSF, Date of Loss not covered,etc.).
                        //they can be unlocked
                        this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);
                        this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Now_Found);
                        msg += this.ClearCancellationEvents();
                        return msg;
                    }
                    else
                    {
                        if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Not_Found, (int)modGeneratedEnums.EventType.Policy_Now_Found))
                            msg += "Policy " + this.PolicyNo + " is Now Found in Personal Lines" + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_Found);
                        this.ClearEvent((int)modGeneratedEnums.EventType.Claim_Created_Without_Policy);
                    }

                    //check version number
                    dateOfLoss = this.DateOfLoss;
                    versionNo = db.GetStringFromStoredProcedure("usp_Get_Personal_Lines_Version_No", this.PolicyNo, dateOfLoss);

                    if (Convert.ToDouble(versionNo) == -1)
                    {
                        //Did not receive good version number from PLA and the policy is not supposed to be expired
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
                            msg += "Policy " + this.PolicyNo + " Not in Force For Date of Loss " + dateOfLoss + Environment.NewLine;

                        this.isExpiredPolicy = true;
                        this.UpdateNamedInsured();

                        //if policy is not in force for date of claim - NSF Cancellation, Pro-Rate, etc. do not apply and can be unlocked
                        msg += this.ClearCancellationEvents();
                        this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss);
                    }
                    else
                    {
                        //Received a good version number from PLA
                        if (this.isExpiredPolicy)
                        {
                            //if the claim used to belong to an expired policy, it is now in force
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
                                msg += "Policy " + this.PolicyNo + " is Now in Force for the Date of Loss " + this.DateOfLoss + Environment.NewLine;
                            this.isExpiredPolicy = false;
                            this.Update();
                        }
                        else
                        {
                            //See if the PLA version number changed
                            if ((Convert.ToDouble(versionNo) != this.VersionNo) && (Convert.ToDouble(versionNo) > 0))
                            {
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Personal_Lines_Version_No_Changed))
                                    msg += "Personal Lines Version Number Changed from " + this.VersionNo + " To " + versionNo + Environment.NewLine;

                                this.VersionNo = Convert.ToDouble(versionNo);
                                this.Update();
                            }
                            else if (Convert.ToDouble(versionNo) == 0)
                                versionNo = this.VersionNo.ToString();
                        }
                        //check date of loss and transaction code
                        PolGeneralInfo genInfo = new PolGeneralInfo(this.PolicyNo, versionNo);

                        if (genInfo.dtExp <= dateOfLoss)
                        {
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss))
                                msg += "Policy " + this.PolicyNo + " Not in Force For Date of Loss " + dateOfLoss + Environment.NewLine;

                            msg += this.ClearCancellationEvents();
                        }
                        else
                        {
                            //Policy is in force for the date of claim
                            if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss, (int)modGeneratedEnums.EventType.Policy_Now_In_Force_For_Date_Of_Loss))
                                msg += "Policy " + this.PolicyNo + " Now in Force for Date of Loss " + dateOfLoss + Environment.NewLine;

                            this.ClearEvent((int)modGeneratedEnums.EventType.Policy_Not_In_Force_For_Date_Of_Loss);

                    //check transaction codes
                    //***trying old policy   moved to ClearCancellationEvents()
                    //switch (genInfo.cdTrx)
                    //{
                    //    case 30:
                    //        if (this.FireEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation))
                    //            msg += "Short Rate Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                    //        this.ClearEvent((int)modGeneratedEnums.EventType.Short_Rate_Cancellation_No_Longer_Applies);
                    //        break;
                    //    case 32:
                    //        if (this.FireEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation))
                    //            msg += "Pro-Rate Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                    //        this.ClearEvent((int)modGeneratedEnums.EventType.Pro_Rate_Cancellation_No_Longer_Applies);
                    //        break;
                    //    case 34:
                    //        if (this.FireEvent((int)modGeneratedEnums.EventType.Flat_Cancellation))
                    //            msg += "Flat Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                    //        this.ClearEvent((int)modGeneratedEnums.EventType.Flat_Cancellation_No_Longer_Applies);
                    //        break;
                    //    case 35:
                    //        if (this.FireEvent((int)modGeneratedEnums.EventType.NSF_Cancellation))
                    //            msg += "NSF Cancellation on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                    //        this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_No_Longer_Applies);
                    //        break;
                    //    default:
                    //        //Policy is good - remove short-rate, flat, NSF locks and alerts
                    //                msg += this.ClearCancellationEvents();
                    //        break;
                    //}
                    //**
                }
            }
                    //check NSF check register in PLA for NSF.  This is a different check than looking for a Transaction code of 35 in Pol_GeneralInfo

                    if (db.GetBoolFromStoredProcedure("usp_Is_Policy_Nsf", this.PolicyNo))
                    {
                        if (this.FireEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_NSF_Check_Register_))
                            msg += "NSF Cancellation (NSF Check Register) on Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.NSF_Cancellation_NSF_Check_Register_No_Longer_Applies);
                    }

                    //check Pol_Control Work in Progress Code
                    versionNo = this.VersionNo.ToString();
                    PolControl polCtrl = new PolControl(this.PolicyNo, versionNo);

                    if (polCtrl.row.Count > 0)
                    {
                        switch (polCtrl.CdWorkInprog.Trim().ToUpper())
                        {
                            case "CHNIP":  //Change to Policy in Progress
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress))
                                    msg += "Change To Policy in Progress: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                                this.ClearEvent((int)modGeneratedEnums.EventType.Change_To_Policy_In_Progress_No_Longer_Applies);
                                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Change_To_Policy_in_Progress);
                                break;
                            case "HOLD":  //Holding Bad Data See Mel
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel))
                                    msg += "Holding Bad Data See Mel: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                                this.ClearEvent((int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel_No_Longer_Applies);
                                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Holding_Bad_Data_See_Mel);
                                break;
                            case "PYERR":  //Payment Posting Error
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Payment_Posting_Error))
                                    msg += "Payment Posting Error: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                                this.ClearEvent((int)modGeneratedEnums.EventType.Payment_Posting_Error_No_Longer_Applies);
                                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Payment_Posting_Error);
                                break;
                            case "WATFR":  //Waiting Final Rating
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Waiting_Final_Rating))
                                    msg += "Waiting Final Rating: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                                this.ClearEvent((int)modGeneratedEnums.EventType.Waiting_Final_Rating_No_Longer_Applies);
                                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Waiting_Final_Rating);
                                break;
                            case "WATNB":  //Waiting for New Business
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Waiting_For_New_Business))
                                    msg += "Waiting For New Business: Policy " + this.PolicyNo + " Version " + versionNo + Environment.NewLine;
                                this.ClearEvent((int)modGeneratedEnums.EventType.Waiting_For_New_Business_No_Longer_Applies);
                                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, (int)modGeneratedEnums.EventType.Waiting_For_New_Business);
                                break;
                            default:
                                //Clear all WIP related events
                                msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, 0);
                                break;
                        }
                    }
                    else
                    {
                        //There is no corresponding row in Pol_Control - Clear all WIP related events
                        msg += this.ClearWorkInProgressEvents(this.PolicyNo, versionNo, 0);
                    }

                    //read current policy coverages into dictionary of Policy_Coverage rows (indexed by Policy_Coverage_Type_Id)
                    coverages = new Hashtable();
                    foreach (Hashtable row in db.ExecuteStoredProcedureReturnHashList("usp_Get_Policy_Coverages_For_Claim", this.Id))
                        coverages.Add((int)row["Policy_Coverage_Type_Id"], row);

                    //refresh policy data
                    this.RefreshPolicyData();

                    //Check for problems

                    //check if insured vehicle is currently on policy
                    if (insVehicle.PolicyVehicleId == 0)
                    {
                        //insured vehicle is no longer on policy
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy))
                            msg += "Insured Vehicle " + insVehicle.Name + "is Not on the Policy." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy);
                    }
                    else
                    {
                        //insured vehicle is on policy
                        if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy, (int)modGeneratedEnums.EventType.Insured_Vehicle_Now_on_Policy))
                            msg += "Insured Vehicle " + insVehicle.Name + " is Now on the Policy." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Vehicle_Not_On_Policy);
                    }

                    //check if insured person is currently on policy
                    if (insPerson.PersonTypeId != 1)
                    {
                        //insured persion is no longer on policy
                        if (!associatingWithNewPolicy)
                        {
                            if (this.FireEvent((int)modGeneratedEnums.EventType.Insured_Not_On_Policy))
                                msg += "Insured " + insPerson.Name + "is Not on the Policy." + Environment.NewLine;
                            this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Now_On_Policy);
                        }
                    }
                    else
                    {
                        //insured person is on policy
                        if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Insured_Not_On_Policy, (int)modGeneratedEnums.EventType.Insured_Now_On_Policy))
                            msg += "Insured " + insPerson.Name + " is Now on the Policy." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Insured_Not_On_Policy);
                    }

                    //check if driver is currently excluded on policy
                    if (drivPerson.PersonTypeId == (int)modGeneratedEnums.PersonType.Excluded_Driver)
                    {
                        //driver has changed to be an excluded driver
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver))
                            msg += "Driver " + drivPerson.Name + "is an Excluded Driver." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore);
                    }
                    else
                    {
                        //driver is not an excluded driver
                        if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Driver_Is_Not_An_Excluded_Driver_Anymore, (int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver))
                            msg += "Driver " + drivPerson.Name + " is not an Excluded Driver Anymore." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Is_An_Excluded_Driver);
                    }

                    //check if driver is not listed on policy
                    if (((drivPerson.PersonTypeId < 1) || (drivPerson.PersonTypeId > 6)) && (drivPerson.FirstName != "Parked and Unoccupied"))
                    {
                        //driver is not on policy
                        if (this.FireEvent((int)modGeneratedEnums.EventType.Driver_Not_On_Policy))
                            msg += "Driver " + drivPerson.Name + "is Not on the Policy." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Now_On_Policy);
                    }
                    else
                    {
                        //driver is on policy
                        if (this.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Driver_Not_On_Policy, (int)modGeneratedEnums.EventType.Driver_Now_On_Policy))
                            msg += "Driver " + drivPerson.Name + " is Now on the Policy." + Environment.NewLine;
                        this.ClearEvent((int)modGeneratedEnums.EventType.Driver_Not_On_Policy);
                    }

                    //check policy coverages
                    int policyCoverageTypeId;

                    foreach (PolicyCoverage newCoverage in this.PolicyCoverages)
                    {
                        policyCoverageTypeId = newCoverage.PolicyCoverageTypeId;
                        if (!coverages.Contains(policyCoverageTypeId))
                        {
                            //check for collision coverage now on policy
                            if (newCoverage.isCollision && newCoverage.hasCoverage)
                            {
                                foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Collision))
                                {
                                    if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Collision_Coverage_Now_Exists_on_Policy))
                                        msg += "Collision Coverage Now Exists on Policy." + Environment.NewLine;
                                    res.ClearEvent((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
                                }

                                if (!this.ValidateVIN(this.InsuredVehicle))
                                {
                                    if(this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                                }
                            }

                            //check for comprehensive coverage now on the policy
                            if (newCoverage.isComprehensive && newCoverage.hasCoverage)
                            {
                                foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Comprehensive))
                                {
                                    if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Comp_Coverage_Now_Exists_on_Policy))
                                        msg += "Comprehensive Coverage Now Exists on Policy." + Environment.NewLine;
                                    res.ClearEvent((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
                                }
                                if (!this.ValidateVIN(this.InsuredVehicle))
                                {
                                    if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            //see if coverage has increased/decreased
                            coverage = (Hashtable)coverages[policyCoverageTypeId];

                            //check per person coverage
                            if ((bool)coverage["Has_Per_Person_Coverage"])
                            {
                                if (!newCoverage.hasPerPersonCoverage)
                                    msg += this.PerPersonCoverageDecreased(policyCoverageTypeId);
                                else
                                {
                                    if (Convert.ToDouble(coverage["Per_Person_Coverage"]) > newCoverage.PerPersonCoverage)
                                        msg += this.PerPersonCoverageDecreased(policyCoverageTypeId);
                                    else
                                    {
                                        if (Convert.ToDouble(coverage["Per_Person_Coverage"]) < newCoverage.PerPersonCoverage)
                                            msg += this.PerPersonCoverageIncreased(policyCoverageTypeId);
                                    }
                                }
                            }
                            else
                            {
                                if (newCoverage.hasPerPersonCoverage)
                                    msg += this.PerPersonCoverageIncreased(policyCoverageTypeId);
                            }

                            //check per accident coverage
                            if ((bool)coverage["Has_Per_Accident_Coverage"])
                            {
                                if (!newCoverage.hasPerAccidentCoverage)
                                    msg += this.PerAccidentCoverageDecreased(policyCoverageTypeId);
                                else
                                {
                                    if (Convert.ToDouble(coverage["Per_Accident_Coverage"]) > newCoverage.PerAccidentCoverage)
                                        msg += this.PerAccidentCoverageDecreased(policyCoverageTypeId);
                                    else
                                    {
                                        if (Convert.ToDouble(coverage["Per_Accident_Coverage"]) < newCoverage.PerAccidentCoverage)
                                            msg += this.PerAccidentCoverageIncreased(policyCoverageTypeId);
                                    }
                                }
                            }
                            else
                            {
                                if (newCoverage.hasPerAccidentCoverage)
                                    msg += this.PerAccidentCoverageIncreased(policyCoverageTypeId);
                            }

                            //check for collision coverage now on policy
                            if (newCoverage.isCollision && newCoverage.hasCoverage)
                            {
                                foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Collision))
                                {
                                    if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Collision_Coverage_Now_Exists_on_Policy))
                                        msg += "Collision Coverage Now Exists on Policy." + Environment.NewLine;
                                    res.ClearEvent((int)modGeneratedEnums.EventType.Collision_Reserve_Created_Without_Coverage);
                                }

                                if (!this.ValidateVIN(this.InsuredVehicle))
                                {
                                    if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                                }
                            }

                            //check for comprehensive coverage now on the policy
                            if (newCoverage.isComprehensive && newCoverage.hasCoverage)
                            {
                                foreach (Reserve res in this.ReservesForReserveType((int)modGeneratedEnums.ReserveType.Comprehensive))
                                {
                                    if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage, (int)modGeneratedEnums.EventType.Comp_Coverage_Now_Exists_on_Policy))
                                        msg += "Comprehensive Coverage Now Exists on Policy." + Environment.NewLine;
                                    res.ClearEvent((int)modGeneratedEnums.EventType.Comp_Reserve_Created_Without_Coverage);
                                }

                                if (!this.ValidateVIN(this.InsuredVehicle))
                                {
                                    if (this.FireEvent((int)modGeneratedEnums.EventType.Manual_Claim_Lock))
                                        msg += "Insured Vehicle has Invalid VIN." + Environment.NewLine;
                                }
                            }
                        }
                    }

                    Hashtable parms = new Hashtable();
                    int thisFraudId = CheckFraud(ref msg);
                    if (thisFraudId > 0)
                    {
                        parms.Add("Claim_Id", this.ClaimId);
                        switch (m_FraudTypeId)
                        {
                            case 1:     //vendor
                                parms.Add("Vendor_Id", thisFraudId);
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Vendor_On_Fraud_List))
                                    msg += "Vendor " + new Vendor(thisFraudId).Name + " On Fraud List" + Environment.NewLine;
                                break;
                            case 2:     //person
                                parms.Add("Person_Id", thisFraudId);
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Person_On_Fraud_List))
                                    msg += new Person(thisFraudId).Name + " On Fraud List" + Environment.NewLine;
                                break;
                            case 3:     //vehicle
                                parms.Add("Vehicle_Id", thisFraudId);
                                if (this.FireEvent((int)modGeneratedEnums.EventType.Vehicle_On_Fraud_List))
                                    msg += "Vehicle " + new Vehicle(thisFraudId).ShortName + " On Fraud List" + Environment.NewLine;
                                break;
                        }
                    }

                    return msg;
            //    default:
            //        return "";
            //}
        }


        public void SetNoLossTask(int mode = 0)
        {
            //'mode is 0 if orginal send, nonzero if date of loss changes
            FileNote fn = new FileNote();
            fn.ClaimId = this.ClaimId;
            fn.FileNoteTypeId = (int)modGeneratedEnums.FileNoteType.Memo_to_File;
            fn.FileNoteText = "Claim sent to Underwriting for verification of No Loss Statement.";
            fn.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            fn.Update();

           
            db.ExecuteStoredProcedure("usp_Set_No_Loss_Task", this.PolicyNo, this.DateOfLoss, this.ClaimId, mode);
            
            NoLossSent = ml.DBNow();
            this.Update();

        }
        
    

        private int CheckFraud(ref string msg)
        {
            try
            {
                List<Hashtable> search = db.ExecuteStoredProcedureReturnHashList("usp_Get_Fraudulent", this.ClaimId);
                Person p;
                string ph;
                if (search.Count > 0)
                {
                    if (this.OwnerPersonId != 0)
                    {
                        //p = this.OwnerPerson;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_id"].ToString()))
                            {
                                if (this.OwnerPersonId == (int)item["Person_id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return OwnerPersonId;
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //{
                            //    this.m_FraudTypeId = 2;
                            //    return (int)item["Person_Id"];
                            //}
                        }
                        //    }
                        //}
                    }
                    msg += "Owner Person " + System.DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

                    if (this.DriverPersonId != 0)
                    {
                        //p = this.DriverPerson;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_id"].ToString()))
                            {
                                if (this.DriverPersonId == (int)item["Person_id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return DriverPersonId;
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //  {
                            //      this.m_FraudTypeId = 2;
                            //      return (int)item["Person_Id"];
                            //  }
                        }
                        //    }
                        //}
                    }
                    msg += "Driver Person " + System.DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

                    if (this.InsuredPersonId != 0)
                    {
                        //p = this.InsuredPerson;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_id"].ToString()))
                            {
                                if (this.InsuredPersonId == (int)item["Person_id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return InsuredPersonId;
                                }
                            }
                            //if ((bool)item["Flag_Fraud"])
                            //{
                            //    this.m_FraudTypeId = 2;
                            //    return (int)item["Person_Id"];
                            //}
                        }
                        //    }
                        //}
                    }
                    msg += "Insured Person " + System.DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

                    foreach (Claimant c in this.Claimants)
                    {
                        //p = c.Person;
                        //ph = !string.IsNullOrWhiteSpace(phStrip(p.CellPhone)) ? phStrip(p.CellPhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.HomePhone)) ? phStrip(p.HomePhone)
                        //    : !string.IsNullOrWhiteSpace(phStrip(p.WorkPhone)) ? phStrip(p.WorkPhone) : "";
                        //if (ph != "")
                        //{
                        //    if (p.FirstName.ToUpper() != "UNKNOWN")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", p.FirstName, p.LastName, "", ph, "", "", p.DateOfBirth.ToShortDateString(), "", p.DriversLicenseNo);
                        foreach (Hashtable item in search)
                        {
                            if (!string.IsNullOrWhiteSpace(item["Person_id"].ToString()))
                            {
                                if (c.PersonId == (int)item["Person_id"])
                                {
                                    this.m_FraudTypeId = 2;
                                    return c.PersonId;
                                }
                            }
                            // if ((bool)item["Flag_Fraud"])
                            //{
                            //    this.m_FraudTypeId = 2;
                            //    return (int)item["Person_Id"];
                            //}
                        }
                        //    }
                        //}
                    }
                    msg += "Claimants " + System.DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

                    foreach (Hashtable h in this.Vehicles)
                    {
                        //Vehicle v = new Vehicle((int)h["Vehicle_Id"]);
                        //if (v.Manufacturer.ToUpper() != "UNKNOWN")
                        //{
                        //    if (v.VIN != "")
                        //    {
                        //        search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Vehicles_Fraud", v.VIN);
                        foreach (Hashtable item in search)
                        {
                            try
                            {
                                if (!string.IsNullOrWhiteSpace(item["Vehicle_Id"].ToString()))
                                {
                                    if ((int)h["Vehicle_Id"] == (int)item["Vehicle_Id"])
                                    {
                                        this.m_FraudTypeId = 3;
                                        return (int)item["Vehicle_Id"];
                                    }
                                }
                                //if ((bool)item["Flag_Fraud"])
                                //          {
                                //              this.m_FraudTypeId = 3;
                                //              return (int)item["Vehicle_Id"];
                                //          }
                            }
                            catch (Exception ex)
                            {
                                //System.Windows.MessageBox.Show(msg, "Error in checkFraud line 2267", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                            }
                        }
                        //    }
                        //}
                    }

                    msg += "Vehicles " + System.DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;

                    foreach (Transaction trx in this.Transactions)
                    {
                        if (trx.Draft != null)
                        {
                            Vendor v = trx.Draft.Vendor();
                            //if (!string.IsNullOrWhiteSpace(v.TaxId))
                            //{
                            //    search = db.ExecuteStoredProcedureReturnHashList("usp_Search_Vendors_Fraud", v.TaxId);
                            foreach (Hashtable item in search)
                            {
                                try
                                {
                                    if (!string.IsNullOrWhiteSpace(item["Vendor_Id"].ToString()))
                                    {
                                        if (v.Id == (int)item["Vendor_Id"])
                                        {
                                            this.m_FraudTypeId = 1;
                                            return (int)item["Vendor_Id"];
                                        }
                                    }
                                    //if ((bool)item["Flag_Fraud"])
                                    //      {
                                    //          this.m_FraudTypeId = 1;
                                    //          return (int)item["Vendor_Id"];
                                    //      }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            //}
                        }
                    }

                    msg += "Transactions " + System.DateTime.Now.ToString("hh:mm:ss.fff") + Environment.NewLine;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        private string phStrip(string p)
        {
            string phNum = p.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("_", "").Replace("x", "").Replace("X", "").Replace(".", "").Trim();
            double testInt = 0;

            if (string.IsNullOrWhiteSpace(phNum))
                return phNum;

            if (!double.TryParse(phNum, out testInt))
                return "";

            string ph = "";
            if (phNum.Length > 10)
                ph = string.Format("({0}) {1}-{2}x{3}", phNum.Substring(0, 3), phNum.Substring(3, 3), phNum.Substring(6, 4), phNum.Substring(10, phNum.Length - 10));
            else if (phNum.Length == 10)
                ph = string.Format("({0}) {1}-{2}", phNum.Substring(0, 3), phNum.Substring(3, 3), phNum.Substring(6, 4));
            else
                ph = phNum;
            return ph;
        }

        public bool ValidateVIN(Vehicle vehicle)
        {
            string vin = vehicle.VIN;
            if (string.IsNullOrWhiteSpace(vin))
                return true;

            return ml.VIN_Checksum(vin);
        }

        public bool PolicyExists
        {
            get { return db.GetBoolFromStoredProcedure("usp_Policy_Exists", this.PolicyNo); }
        }

        public string PerPersonCoverageDecreased(int coverageTypeId)
        {
            //Per person coverage decreased during refresh.  return description of problems to be displayed to user after syncing

            double amount = 0;
            string msg = "";

            //lock any open reserve lines whose paid + net reserve > the new coverage amount
            PolicyCoverage coverage = this.getPolicyCoverage(coverageTypeId); //Policy_Coverage row, if none available - an empty dictionary
            if (coverage == null)
                amount = 0.0;
            else
            {
                amount = coverage.PerPersonCoverage;
                if (coverage.PerPersonPeriodTypeId != (int)modGeneratedEnums.PeriodType.Flat)
                    amount *= coverage.PerPersonNoPeriods;
            }

            DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claim_And_Coverage_Type", this.Id, coverageTypeId);
            foreach (DataRow row in myReserves.Rows)
            {
                Reserve res = new Reserve(myReserves, row);
                if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                {
                    if (res.TotalLossPayments + res.NetLossReserve > amount)
                    {
                        //lock reserve
                        if (res.FireEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage))
                            msg += "Per-Person Coverage Exceeded for " + res.Description() + Environment.NewLine;
                        //add to message

                        res.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Doesnt_Exceed_Per_Person_Coverage_Anymore);
                    }
                }
            }
            return msg;
        }

        public string PerPersonCoverageIncreased(int coverageTypeId)
        {
            /*Per person coverage increased during refresh.  
             * Unlock any drafts or reserve lines which are now under the coverage limit which weren't before.
             * Delet any alerts which are now no longer necessary.
            */

            double amount;
            string msg = "";
            PolicyCoverage coverage = this.getPolicyCoverage(coverageTypeId); //Policy_Coverage row, if none available - an empty dictionary

            if (coverage == null)
                amount = 0;
            else
            {
                amount = coverage.PerPersonCoverage;
                if (coverage.PerPersonPeriodTypeId != (int)modGeneratedEnums.PeriodType.Flat)
                    amount *= coverage.PerPersonNoPeriods;
            }
         
            DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claim_And_Coverage_Type", this.Id, coverageTypeId);
            foreach (DataRow row in myReserves.Rows)
            {
                Reserve res = new Reserve(myReserves, row);
                if (res.TotalLossPayments + res.NetLossReserve < amount)
                {
                    //clear reserve line if it exceeds per person coverage
                    if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage, (int)modGeneratedEnums.EventType.Loss_Reserve_Doesnt_Exceed_Per_Person_Coverage_Anymore))
                        msg += res.Description() + "Doesn't Exceed Per-Person Coverage Anymore." + Environment.NewLine;
                    //add to message

                    res.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Person_Coverage);

                    //clear any drafts locked for exceeding per-person coverage
                    DataTable myTransactionLocks=db.ExecuteStoredProcedure("usp_Get_Transaction_Locks_For_Reserve_And_Event_Type", res.Id,(int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage);
                    foreach (DataRow tlRow in myTransactionLocks.Rows)
                    {
                        TransactionLock tl = new TransactionLock(myTransactionLocks, tlRow);
                        if (tl.Transaction().FireEventIfSyncExists((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage, (int)modGeneratedEnums.EventType.Draft_Amount_Doesnt_Exceed_Per_Person_Coverage_Anymore))
                            msg += tl.DraftDescription + "Doesn't Exceed Per-Person Coverage Anymore." + Environment.NewLine;

                        tl.Transaction().ClearEvent((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage);
                    }
                }
            }
            return msg;
        }

        public string PerAccidentCoverageDecreased(int coverageTypeId)
        {
            //Per accident coverage decreased during refresh.  return description of problems to be displayed to user after syncing

            double amount = 0;
            string msg = "";

            //lock any open reserve lines whose paid + net reserve > the new coverage amount
            PolicyCoverage coverage = this.getPolicyCoverage(coverageTypeId); //Policy_Coverage row, if none available - an empty dictionary
            if (coverage == null)
                amount = 0.0;
            else
                amount = coverage.PerAccidentCoverage;

            if (this.LossForCoverageType(coverageTypeId) + this.NetLossReserveForCoverageType(coverageTypeId) > amount)
            {
                //Per-accident limit exceeded
                DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claim_And_Coverage_Type", this.Id, coverageTypeId);
                foreach (DataRow row in myReserves.Rows)
                {
                    Reserve res = new Reserve(myReserves, row);
                    if (res.ReserveStatusId == (int)modGeneratedEnums.ReserveStatus.Open_Reserve)
                    {
                        if (res.TotalLossPayments + res.NetLossReserve > amount)
                        {
                            //lock reserve
                            if (res.FireEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage))
                                msg += "Per-Accident Coverage Exceeded for " + res.Description() + Environment.NewLine;
                            //add to message

                            res.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Doesnt_Exceed_Per_Accident_Coverage_Anymore);
                        }
                    }
                }
            }
            return msg;
        }

        public string PerAccidentCoverageIncreased(int coverageTypeId)
        {
            /*Per accident coverage increased during refresh.  
             * Unlock any drafts or reserve lines which are now under the coverage limit which weren't before.
             * Delet any alerts which are now no longer necessary.
            */

            double amount, lossPayments;
            string msg = "";
            PolicyCoverage coverage = this.getPolicyCoverage(coverageTypeId); //Policy_Coverage row, if none available - an empty dictionary

            if (coverage == null)
                amount = 0;
            else
                amount = coverage.PerAccidentCoverage;

            if (this.LossForCoverageType(coverageTypeId) + this.NetLossReserveForCoverageType(coverageTypeId) < amount)
            {
                //per-accident coverage is greater than net reserves + loss -- unlock any reserve lines locked for exceeding per-accident coverage
                DataTable myReserves = db.ExecuteStoredProcedure("usp_Get_Reserves_For_Claim_And_Coverage_Type", this.Id, coverageTypeId);
                foreach (DataRow row in myReserves.Rows)
                {
                    Reserve res = new Reserve(myReserves, row);
                    //clear reserve line if it exceeds per person coverage
                    if (res.FireEventIfSyncExists((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage, (int)modGeneratedEnums.EventType.Loss_Reserve_Doesnt_Exceed_Per_Accident_Coverage_Anymore))
                        msg += res.Description() + "Doesn't Exceed Per-Accident Coverage Anymore." + Environment.NewLine;
                    //add to message

                    res.ClearEvent((int)modGeneratedEnums.EventType.Loss_Reserve_Exceeds_Per_Accident_Coverage);

                    //clear any drafts locked for exceeding per-person coverage
                    DataTable myTransactionLocks = db.ExecuteStoredProcedure("usp_Get_Transaction_Locks_For_Reserve_And_Event_Type", res.Id, (int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Person_Coverage);
                    foreach (DataRow tlRow in myTransactionLocks.Rows)
                    {
                        TransactionLock tl = new TransactionLock(myTransactionLocks, tlRow);
                        if (tl.Transaction().FireEventIfSyncExists((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Accident_Coverage, (int)modGeneratedEnums.EventType.Draft_Amount_Doesnt_Exceed_Per_Accident_Coverage_Anymore))
                            msg += tl.DraftDescription + "Doesn't Exceed Per-Accident Coverage Anymore." + Environment.NewLine;

                        tl.Transaction().ClearEvent((int)modGeneratedEnums.EventType.Draft_Amount_Exceeds_Per_Accident_Coverage);
                    }
                }
            }
            return msg;
        }

        public ArrayList WitnessPassengers
        {
            //returns arraylist of witnessPassenger objects for this claim
            get
            {
                ArrayList a = new ArrayList();
                foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Witness_Passengers", this.Id).Rows)
                    a.Add(new WitnessPassenger((int)row["Id"]));
                return a;
            }
        }


        public void CompleteSetup()
        {
            //issue filenote and alert for each user group and user reserves have been assigned to
            if (this.SetupComplete)
                return;

            db.ExecuteStoredProcedure("usp_Claim_Setup_Complete", this.Id);  //don't want file note to be generated for setup_complete=true

            //issue filenote and alert for each user group assigned a reserve
            foreach (int userGroupId in this.AssignedToUserGroupIds())
            {
                Hashtable parms = new Hashtable();
                parms.Add("User_Group_Id", userGroupId);
                parms.Add("Adjuster_Id", 0);
                this.FireEvent((int)modGeneratedEnums.EventType.Initial_Reserve_Assignment, parms);
            }

            //issue filenote and alert for each user assigned a reserve
            IntegerSet s = new IntegerSet();
            foreach (Reserve res in this.Reserves())
            {
                if (res.AdjusterId != 0)
                    s.Add(res.AdjusterId);
            }

            foreach (int userId in s.ToList())
            {
                Hashtable parms = new Hashtable();
                parms.Add("User_Group_Id", 0);
                parms.Add("Adjuster_Id", userId);
                this.FireEvent((int)modGeneratedEnums.EventType.Initial_Reserve_Assignment, parms);
            }
            this.SetupComplete = true;
        }

        public ArrayList ReservesAssignedToUserId(int userId)
        {
            ArrayList a = new ArrayList();
            foreach(Reserve res in this.Reserves())
            {
                if (res.AdjusterId==userId)
                    a.Add(res);
            }
            return a;
        }

        public ArrayList ReservesAssignedToUserGroupId(int userGroupId)
        {
            ArrayList a = new ArrayList();
            foreach(Reserve res in this.Reserves())
            {
                if (res.AssignedToUserGroupId==userGroupId)
                    a.Add(res);
            }
            return a;
        }

        public List<int> AssignedToUserGroupIds()
        {
            //returns list of user group IDs which are assigned reserves
            IntegerSet a = new IntegerSet();
            foreach(Reserve res in this.Reserves())
            {
                if (res.AssignedToUserGroupId!=0)
                    a.Add(res.AssignedToUserGroupId);
            }
            return a.ToList();
        }

        public ArrayList AssignedToUsers()
        {
            //returns all users this claim is assigned to
            ArrayList a = new ArrayList();
            foreach(int i in this.AssignedToUserIds())
                a.Add(new User(i));
            return a;
        }

        public List<int> AssignedToUserIds()
        {
            //returns all user IDs this claim is assigned to
            IntegerSet a = new IntegerSet();
            foreach(Claimant cl in this.Claimants)
                a.Add(cl.AssignedToUserIds());
            return a.ToList();
        }

        public void UpdateHistory()
        {
            if (m_ClaimHistoryId <= 0)
            {
                Hashtable ch = new Hashtable();
                ch.Add("Status_Open_Date", ml.DBNow());
                if (this.Claim.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
                    ch.Add("Status_Close_Date", ml.DBNow());
                ch.Add("Claim_Id", this.ClaimId);
                ch.Add("Claim_Status_Id", this.ClaimStatusId);
                ch.Add("Is_Current_Status", true);
                this.m_ClaimHistoryId = ml.InsertRow(ch, "Claim_History");
            }
            else if (ClaimHistory.ClaimStatusId != this.ClaimStatusId)
            {
                ClaimHistory.StatusCloseDate = ml.DBNow();
                ClaimHistory.IsCurrentStatus = false;
                this.m_ClaimHistoryId = ClaimHistory.Update();

                if (this.ClaimHistory.ClaimStatusId != (int)modGeneratedEnums.ClaimStatus.Claim_Closed)
                {
                    ClaimHistory ch = new ClaimHistory();
                    ch.StatusOpenDate = ml.DBNow();
                    ch.ClaimId = this.ClaimId;
                    ch.ClaimStatusId = this.ClaimStatusId;
                    ch.IsCurrentStatus = true;
                    this.m_ClaimHistoryId = ch.Update();
                }
            }
        }

        public ArrayList OutstandingTasks()
        {
            //returns list of non-cleared UserTasks for Claim
            ArrayList a = new ArrayList();
            DataTable myTasks=db.ExecuteStoredProcedure("usp_Get_Outstanding_Tasks_For_Claim",this.Id);
            foreach (DataRow row in myTasks.Rows)
            {
                a.Add(new UserTask(myTasks,row));
            }
            return a;
        }

        public bool isClosed
        {
            get { return this.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed; }
        }

        public bool isNotClosed
        {
            get { return !this.isClosed; }
        }

        public ClaimStatus ClaimStatus()
        {
            return new ClaimStatus(this.ClaimStatusId);
        }

        public int maxDiaryDaysAllowed
        {
            /*the max number of days that current user may use in setting up a diary.
             * if there is no need for a forced diary entry, return zero.
            */

            get
            {
                //is user enabled for forced diaries?
                if (!ml.CurrentUser.shouldForceDiary)
                    return 0;

                //is user not assigned to this claim?
                if ((this.AdjusterId != ml.CurrentUser.Id) && !ml.CurrentUser.isAssignedToClaim(this))
                    return 0;

                if (this.isClosed)
                    return 0;

                int maxDays;
                bool hasOpen = false, 
                    hasSubro = false, 
                    hasSalvage = false;

                foreach (Reserve res in this.Reserves())
                {
                    if (res.isOpen)
                        hasOpen = true;
                    else if (res.isClosedPendingSubro)
                        hasSubro = true;
                    else if (res.isClosedPendingSalvage)
                        hasSalvage = true;
                }

                if (hasOpen)
                    maxDays = UserTypeDiary.MaxDiaryDaysFor(ml.CurrentUser.UserType(), this.DaysSinceReported);
                else if (hasSubro && !hasSalvage)
                    maxDays = ml.CurrentUser.UserType().SubroMaxDiaryDays;
                else if (!hasSubro && hasSalvage)
                    maxDays = ml.CurrentUser.UserType().SubroMaxDiaryDays;
                else if (hasSubro && hasSalvage)
                    maxDays = Math.Min(ml.CurrentUser.UserType().SubroMaxDiaryDays, ml.CurrentUser.UserType().SalvageMaxDiaryDays);
                else
                    maxDays = UserTypeDiary.MaxDiaryDaysFor(ml.CurrentUser.UserType(), this.DaysSinceReported);

                //does user already have active diaries on this claim that fall within the acceptable date range for the number of days open on the reserve?
                TimeSpan tsMaxDays = new TimeSpan(maxDays, 0, 0, 0);
                foreach (Diary d in this.DiariesForCurrentUser())
                {
                    if (d.DiaryDueDate < ml.DBNow().Add(tsMaxDays))
                        return 0;
                }

                return maxDays;
            }
        }

        public Reserve MostRecentReserveForCurrentUser()
        {
            DateTime d = ml.DEFAULT_DATE;
            Reserve recentRes = null;

            foreach (Reserve res in this.Reserves())
            {
                if (res.isAssignedToUser(ml.CurrentUser))
                {
                    if (res.DateOpen > d)
                    {
                        d = res.DateOpen;
                        recentRes = res;
                    }
                }
            }
            return recentRes;
        }

        public ArrayList DiariesForCurrentUser()
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_My_Diaries_For_Claim", ml.CurrentUser.Id, this.Id);
            ArrayList a = new ArrayList();
            foreach (DataRow row in rows.Rows)
                a.Add(new Diary(rows, row));
            return a;
        }

        public int DaysSinceReported
        {
            get { return ml.DBNow().Subtract(this.DateReported).Days; }
        }

        public bool maxSupplementalPaymentReached
        {
            get { return ((this.ClaimStatusId == (int)modGeneratedEnums.ClaimStatus.Claim_Closed) && 
                (MedCsXSystem.Settings.maxSupplementalPayments <= this.NoSupplementalPayments)) ? true : false; }
        }

        public bool hasUnassignedReserves
        {
            get
            {
                foreach (Reserve res in this.Reserves())
                {
                    if ((res.AdjusterId == 0) && (res.AssignedToUserGroupId == 0))
                        return true;
                }
                return false;
            }
        }

        public AtFaultType AtFaultType()
        {
            return new AtFaultType(this.AtFaultTypeId);
        }

        public AccidentType AccidentType()
        {
            return new AccidentType(this.AccidentTypeId);
        }

        public CompanyLocation CompanyLocation()
        {
            return new CompanyLocation(this.CompanyLocationId);
        }

        public string CompanyName
        {
            get
            {
                try
                {
                    return this.CompanyLocation().Company.Description;
                }
                catch
                {
                    return "";
                }
            }
        }


        public double PaidLoss
        {
            get
            {
                double total = 0;
                foreach (Reserve res in this.Reserves())
                    total += res.PaidLoss;
                return total;
            }
        }

        public override string ReadRowStoredProcedureName()
        {
            return "usp_Get_Claim_Row";
        }

        public ArrayList ReserveDescriptions()
        {
            ArrayList a = new ArrayList();
            foreach (Reserve res in this.Reserves())
                a.Add(res.Description());
            return a;
        }
        #endregion

        #region Duplicate Claims
        public ArrayList PossibleDuplicateClaims()
        {
            return this.ChildObjects(typeof(PossibleDuplicateClaim));
        }

        public int checkForPossibleDuplicateClaims(PossibleDuplicateClaimMode mode)
        {
            int dupCount = db.GetIntFromStoredProcedure("usp_Insert_Possible_Duplicates", this.PolicyNo, this.VersionNo, this.Id, this.DateOfLoss.ToShortDateString(), (int)mode);
            if (dupCount > 0)
            {
                Hashtable parms = new Hashtable();
                parms.Add("FileNoteText", this.DuplicateClaimFileNoteText);
                this.FireEvent((int)modGeneratedEnums.EventType.Possible_Duplicate_Claims_Found, parms);
            }
            return dupCount;
        }

        public string DuplicateClaimFileNoteText
        {
            get
            {
                string s = "The following claims are possible duplicates of this one and should be investigated:" + Environment.NewLine + Environment.NewLine;
                foreach (PossibleDuplicateClaim c in this.PossibleDuplicateClaims())
                    s += c.DuplicateClaim().DisplayClaimId + " - " + c.Description + Environment.NewLine;
                return s;
            }
        }
        #endregion

        #region Companion Claims
        public List<Hashtable> CompanionClaims()
        {
            //these are claims with the same policy number and date of loss but different claimants
            if (this.PolicyNo.Trim() != "")
                return db.ExecuteStoredProcedureReturnHashList("usp_Get_Companion_Claims", this.Id, this.PolicyNo, this.DateOfLoss);
            else
                return new List<Hashtable>();
            //not associated with a policy - can't have companion claims
        }
        #endregion
    }
}

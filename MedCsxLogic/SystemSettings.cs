﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class SystemSettings:MedCsXTable
    {
        private static modLogic ml=new modLogic();
        #region Constructors
        public SystemSettings() : base() { }
        public SystemSettings(int id) : base(id) { }
        public SystemSettings(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "System_Settings"; }
        }

        public string AppraisalEmailAddress
        {
            get { return (string)row["Appraisal_Email_Address"]; }
            set { this.setRowValue("Appraisal_Email_Address", value); }
        }

        public string AppraisalRequestFileName
        {
            get { return (string)row["Appraisal_Request_File_Name"]; }
            set { this.setRowValue("Appraisal_Request_File_Name", value); }
        }

        public string AppraisalSMTPServer
        {
            get { return (string)row["Appraisal_SMTP_Server"]; }
            set { this.setRowValue("Appraisal_SMTP_Server", value); }
        }

        public string AppraisalSubjectLine
        {
            get { return (string)row["Appraisal_Subject_Line"]; }
            set { this.setRowValue("Appraisal_Subject_Line", value); }
        }

        public double CoverageIssuesPoints
        {
            get { return (double)row["Coverage_Issues_Points"]; }
            set { this.setRowValue("Coverage_Issues_Points", value); }
        }

        public int DaysToAlertPasswordChange
        {
            get { return (int)row["Days_To_Alert_Password_Change"]; }
            set { this.setRowValue("Days_To_Alert_Password_Change", value); }
        }

        public double DisputedLiabilityPoints
        {
            get { return (double)row["Disputed_Liability_Points"]; }
            set { this.setRowValue("Disputed_Liability_Points", value); }
        }

        public string DraftCopyPrinter
        {
            get { return (string)row["Draft_Copy_Printer"]; }
            set { this.setRowValue("Draft_Copy_Printer", value); }
        }

        public string DraftCopyPrinterWin98
        {
            get { return (string)row["Draft_Copy_Printer_Win98"]; }
            set { this.setRowValue("Draft_Copy_Printer_Win98", value); }
        }

        public string DraftPrinter
        {
            get { return (string)row["Draft_Printer"]; }
            set { this.setRowValue("Draft_Printer", value); }
        }

        public string DraftPrinterKC
        {
            get { return (string)row["Draft_PrinterKC"]; }
            set { this.setRowValue("Draft_PrinterKC", value); }
        }

       

        public string DraftPrinterLV
        {
            get { return (string)row["Draft_PrinterLV"]; }
            set { this.setRowValue("Draft_PrinterLV", value); }
        }

        public string DraftPrinterWin98
        {
            get { return (string)row["Draft_Printer_Win98"]; }
            set { this.setRowValue("Draft_Printer_Win98", value); }
        }

        public bool DraftPrintingFriday
        {
            get { return (bool)row["Draft_Printing_Friday"]; }
            set { this.setRowValue("Draft_Printing_Friday", value); }
        }

        public int DraftPrintingFromTime
        {
            get { return (int)row["Draft_Printing_From_Time"]; }
            set { this.setRowValue("Draft_Printing_From_Time", value); }
        }

        public bool DraftPrintingMonday
        {
            get { return (bool)row["Draft_Printing_Monday"]; }
            set { this.setRowValue("Draft_Printing_Monday", value); }
        }

        public int DraftPrintingPermissionTypeId
        {
            get { return (int)row["Draft_Printing_Permission_Type_Id"]; }
            set { this.setRowValue("Draft_Printing_Permission_Type_Id", value); }
        }

        public bool DraftPrintingSaturday
        {
            get { return (bool)row["Draft_Printing_Saturday"]; }
            set { this.setRowValue("Draft_Printing_Saturday", value); }
        }

        public bool DraftPrintingSunday
        {
            get { return (bool)row["Draft_Printing_Sunday"]; }
            set { this.setRowValue("Draft_Printing_Sunday", value); }
        }

        public bool DraftPrintingThursday
        {
            get { return (bool)row["Draft_Printing_Thursday"]; }
            set { this.setRowValue("Draft_Printing_Thursday", value); }
        }

        public int DraftPrintingToTime
        {
            get { return (int)row["Draft_Printing_To_Time"]; }
            set { this.setRowValue("Draft_Printing_To_Time", value); }
        }

        public bool DraftPrintingTuesday
        {
            get { return (bool)row["Draft_Printing_Tuesday"]; }
            set { this.setRowValue("Draft_Printing_Tuesday", value); }
        }

        public bool DraftPrintingWednesday
        {
            get { return (bool)row["Draft_Printing_Wednesday"]; }
            set { this.setRowValue("Draft_Printing_Wednesday", value); }
        }

        public double ExperienceWeight
        {
            get { return (double)row["Experience_Weight"]; }
            set { this.setRowValue("Experience_Weight", value); }
        }

        public double FilesPerDayWeight
        {
            get { return (double)row["Files_Per_Day_Weight"]; }
            set { this.setRowValue("Files_Per_Day_Weight", value); }
        }

        public double LanguageIssuePoints
        {
            get { return (double)row["Language_Issue_Points"]; }
            set { this.setRowValue("Language_Issue_Points", value); }
        }

        public int maxSupplementalPayments
        {
            get { return (int)row["Max_Supplemental_Payments"]; }
            set { this.setRowValue("Max_Supplemental_Payments", value); }
        }

        public int MaximumPasswordCharacters
        {
            get { return (int)row["Maximum_Password_Characters"]; }
            set { this.setRowValue("Maximum_Password_Characters", value); }
        }

        public int MinimumPasswordCharacters
        {
            get { return (int)row["Minimum_Password_Characters"]; }
            set { this.setRowValue("Minimum_Password_Characters", value); }
        }

        public int MinimumNumericCharacters
        {
            get { return (int)row["Minimum_Numeric_Characters"]; }
            set { this.setRowValue("Minimum_Numeric_Characters", value); }
        }

        public int Minimum_Special_Characters
        {
            get { return (int)row["Minimum_Special_Characters"]; }
            set { this.setRowValue("Minimum_Special_Characters", value); }
        }

        public int MinimumUpperCaseCharacters
        {
            get { return (int)row["Minimum_UpperCase_Characters"]; }
            set { this.setRowValue("Minimum_UpperCase_Characters", value); }
        }

        public int MinutesLockedUntilShutdown
        {
            get { return (int)row["Minutes_Locked_Until_Shutdown"]; }
            set { this.setRowValue("Minutes_Locked_Until_Shutdown", value); }
        }

        public int DraftPrintingLocation
        {
            get { return (int)row["Draft_Printing_Location_Id"]; }
            set { this.setRowValue("Draft_Printing_Location_Id", value); }
        }

        public int MinutesUntilLocked
        {
            get { return (int)row["Minutes_Until_Locked"]; }
            set { this.setRowValue("Minutes_Until_Locked", value); }
        }

        public int PasswordChangesBeforeReuse
        {
            get { return (int)row["Password_Changes_Before_Reuse"]; }
            set { this.setRowValue("Password_Changes_Before_Reuse", value); }
        }

        public double PendingWeight
        {
            get { return (double)row["Pending_Weight"]; }
            set { this.setRowValue("Pending_Weight", value); }
        }

        public int PollingIntervalMinutes
        {
            get { return (int)row["Polling_Interval_Minutes"]; }
            set { this.setRowValue("Polling_Interval_Minutes", value); }
        }

        public double ReceivedPerWeekWeight
        {
            get { return (double)row["Received_Per_Week_Weight"]; }
            set { this.setRowValue("Received_Per_Week_Weight", value); }
        }

        public int ReserveRequirementDays
        {
            get { return (int)row["Reserve_Requirement_Days"]; }
            set { this.setRowValue("Reserve_Requirement_Days", value); }
        }

        public int ReserveWarningDays
        {
            get { return (int)row["Reserve_Warning_Days"]; }
            set { this.setRowValue("Reserve_Warning_Days", value); }
        }

        public bool ReservesRequiredOnLockedFiles
        {
            get { return (bool)row["Reserves_Required_On_Locked_Files"]; }
            set { this.setRowValue("Reserves_Required_On_Locked_Files", value); }
        }

        public int SalvageRequirementDays
        {
            get { return (int)row["Salvage_Requirement_Days"]; }
            set { this.setRowValue("Salvage_Requirement_Days", value); }
        }

        public int SalvageWarningDays
        {
            get { return (int)row["Salvage_Warning_Days"]; }
            set { this.setRowValue("Salvage_Warning_Days", value); }
        }

        public string SceneAccessDefaultInsuranceCoAddress1
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_Address1"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_Address1", value); }
        }

        public string SceneAccessDefaultInsuranceCoAddress2
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_Address2"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_Address2", value); }
        }

        public string SceneAccessDefaultInsuranceCoCity
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_City"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_City", value); }
        }

        public string SceneAccessDefaultInsuranceCoFax
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_Fax"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_Fax", value); }
        }

        public string SceneAccessDefaultInsuranceCoName
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_Name"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_Name", value); }
        }

        public string SceneAccessDefaultInsuranceCoPhone
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_Phone"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_Phone", value); }
        }

        public string SceneAccessDefaultInsuranceCoState
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_State"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_State", value); }
        }

        public string SceneAccessDefaultInsuranceCoZip
        {
            get { return (string)row["Scene_Access_Default_Insurance_Co_Zip"]; }
            set { this.setRowValue("Scene_Access_Default_Insurance_Co_Zip", value); }
        }

        public string SceneAccessDefaultSpecialInstructions
        {
            get { return (string)row["Scene_Access_Default_Special_Instructions"]; }
            set { this.setRowValue("Scene_Access_Default_Special_Instructions", value); }
        }

        public string SceneAccessDefaultTaskDescription
        {
            get { return (string)row["Scene_Access_Default_Task_Description"]; }
            set { this.setRowValue("Scene_Access_Default_Task_Description", value); }
        }

        public string SceneAccessDefaultTypeOfClaim
        {
            get { return (string)row["Scene_Access_Default_Type_Of_Claim"]; }
            set { this.setRowValue("Scene_Access_Default_Type_Of_Claim", value); }
        }

        public string SceneAccessDefaultTypeOfInvestigation
        {
            get { return (string)row["Scene_Access_Default_Type_Of_Investigation"]; }
            set { this.setRowValue("Scene_Access_Default_Type_Of_Investigation", value); }
        }

        public bool SceneAccessEMSForm
        {
            get { return (bool)row["Scene_Access_EMS_Form"]; }
            set { this.setRowValue("Scene_Access_EMS_Form", value); }
        }

        public bool SceneAccessRequiresApproval
        {
            get { return (bool)row["Scene_Access_Requires_Approval"]; }
            set { this.setRowValue("Scene_Access_Requires_Approval", value); }
        }

        public int SystemSettingsId
        {
            get { return (int)row["System_Settings_Id"]; }
            set { this.setRowValue("System_Settings_Id", value); }
        }

        public string VersionNo
        {
            get { return (string)row["Version_No"]; }
            set { this.setRowValue("Version_No", value); }
        }

        public int WeeksBetweenPasswordChanges
        {
            get { return (int)row["Weeks_Between_Password_Changes"]; }
            set { this.setRowValue("Weeks_Between_Password_Changes", value); }
        }

        public Single maxAggregateBeforeAlert
        {
            get { return (Single)row["Max_Aggregate_Before_Alert"]; }
            set { this.setRowValue("Max_Aggregate_Before_Alert", value); }
        }

        public Single maxSIRBeforeAlert
        {
            get { return (Single)row["Max_SIR_Before_Alert"]; }
            set { this.setRowValue("Max_SIR_Before_Alert", value); }
        }
        #endregion


        public bool DraftPrintingCurrentlyOn
        {
            //is draft printing on?
            get
            {
                switch (this.DraftPrintingPermissionTypeId)
                {
                    case (int)modGeneratedEnums.DraftPrintingPermissionType.Off:
                        return false;
                    case (int)modGeneratedEnums.DraftPrintingPermissionType.On:
                        return true;
                    case (int)modGeneratedEnums.DraftPrintingPermissionType.Use_Set_Times:
                        switch (ml.DBNow().DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                if (!this.DraftPrintingMonday)
                                    return false;
                                break;
                            case DayOfWeek.Tuesday:
                                if (!this.DraftPrintingTuesday)
                                    return false;
                                break;
                            case DayOfWeek.Wednesday:
                                if (!this.DraftPrintingWednesday)
                                    return false;
                                break;
                            case DayOfWeek.Thursday:
                                if (!this.DraftPrintingThursday)
                                    return false;
                                break;
                            case DayOfWeek.Friday:
                                if (!this.DraftPrintingFriday)
                                    return false;
                                break;
                            case DayOfWeek.Saturday:
                                if (!this.DraftPrintingSaturday)
                                    return false;
                                break;
                            case DayOfWeek.Sunday:
                                if (!this.DraftPrintingSunday)
                                    return false;
                                break;
                        }

                        //the day is ok - check time
                        int currentTime = ml.DBNow().Hour * 100 + ml.DBNow().Minute;
                        if ((currentTime < this.DraftPrintingFromTime) || (currentTime > this.DraftPrintingToTime))
                            return false; //outside of time range
                        return true;
                    default:
                        return false;
                }
            }
            
        }
    }
}

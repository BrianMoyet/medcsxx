﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class Company:TypeTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public Company() : base() { }
        public Company(int id) : base(id) { }
        public Company(string description) : base(description) { }
        public Company(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Company";
            }
        }

        public string Abbreviation
        {
            get { return (string)row["Abbreviation"]; }
            set { this.setRowValue("Abbreviation", value); }
        }

        public string BankAccountNo
        {
            get { return (string)row["Bank_Account_No"]; }
            set { this.setRowValue("Bank_Account_No", value); }
        }

        public string BankRoutingNo
        {
            get { return (string)row["Bank_Routing_No"]; }
            set { this.setRowValue("Bank_Routing_No", value); }
        }

        public int CompanyId
        {
            get { return (int)row["Company_Id"]; }
            set { this.setRowValue("Company_Id", value); }
        }

        public int DraftNo
        {
            get { return (int)row["Draft_No"]; }
            set { this.setRowValue("Draft_No", value); }
        }

        public int PersonalLinesBankAcctId
        {
            get { return (int)row["Personal_Lines_BankAcctId"]; }
            set { this.setRowValue("Personal_Lines_BankAcctId", value); }
        }
        #endregion

        #region Misc Methods
        public static DataTable getCompanies()
        {
            //returns all the companies
            return db.ExecuteStoredProcedureReturnDataTable("usp_Get_Companies");
        }

        public static string getCompanyForPolicy(string policyNo)
        {
            //returns company for given policy 
            return policyNo == null ? "" : db.GetStringFromStoredProcedure("usp_Get_Company_For_Policy", policyNo);
        }
        #endregion
    }
}

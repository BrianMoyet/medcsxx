﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class WitnessPassengerType:TypeTable
    {

        #region Constructors
        public WitnessPassengerType() : base() { }
        public WitnessPassengerType(int id) : base(id) { }
        public WitnessPassengerType(string description) : base(description) { }
        public WitnessPassengerType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Witness_Passenger_Type";
            }
        }

        public int WitnessPassengerTypeId
        {
            get { return (int)row["Witness_Passenger_Type_Id"]; }
            set { this.setRowValue("Witness_Passenger_Type_Id", value); }
        }

        [System.ComponentModel.Description("Type of Claim")]
        public int ClaimTypeId
        {
            get { return (int)row["Claim_Type_Id"]; }
            set { this.setRowValue("Claim_Type_Id", value); }
        }
        #endregion
    }
}

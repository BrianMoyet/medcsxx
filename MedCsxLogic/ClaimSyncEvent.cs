﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ClaimSyncEvent:SyncEventTable
    {

        #region Constructors
        public ClaimSyncEvent() : base() { }
        public ClaimSyncEvent(int id) : base(id) { }
        public ClaimSyncEvent(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Claim_Sync_Event";
            }
        }

        public int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }
      
        public int ClaimSyncEventId
        {
            get { return (int)row["Claim_Sync_Event_Id"]; }
            set { this.setRowValue("Claim_Sync_Event_Id", value); }
        }

        public Claim Claim()
        {
            return new Claim(this.ClaimId);
        }
        #endregion
    }
}

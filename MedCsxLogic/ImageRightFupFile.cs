﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MedCsxLogic
{

    public class ImageRightFupFile:ImageRightFile
    {
        #region Class Variables
        private ImageRightFupFields m_Fields;
        #endregion

        #region Constructors
        public ImageRightFupFile() : base() { }
        public ImageRightFupFile(int claimId, string oldDisplayClaimId)
            : base(claimId)
        {
            m_Fields = new ImageRightFupFields();
            m_Fields.Drawer = DetermineClaimsImageRightDrawer(this.Claim.CompanyLocation().CompanyId);
            if (oldDisplayClaimId == "")
                m_Fields.FileNumber = this.Claim.DisplayClaimId;
            else
                m_Fields.FileNumber = oldDisplayClaimId;
            m_Fields.FileName = this.Claim.InsuredPerson.FormalName;
            m_Fields.UserId_8 = "";
            m_Fields.UserKey1 = "";
            m_Fields.UserData1 = this.Claim.PolicyNo;
            m_Fields.UserData2 = this.Claim.Adjuster.LoginId.ToUpper();
            if (m_Fields.UserData2.Length > 10)
                m_Fields.UserData2 = m_Fields.UserData2.Substring(0, 10);
            m_Fields.UserData3 = this.Claim.DateOfLoss.ToShortDateString();
            m_Fields.UserData4 = "";
            m_Fields.UserData5 = "";
            m_Fields.NewFileNumber = this.Claim.DisplayClaimId;
            m_Fields.NewDrawer = "";
            m_Fields.Status = "";
            m_Fields.StatusDate = "";
            m_Fields.UserId_10 = "";
        }
        #endregion

        #region Misc Methods
        public string EntireRecord
        {
            get { return m_Fields.EntireRecord; }
        }

        public void WriteFile()
        {
            if (this.Claim.CompanyLocation().CompanyId == (int)modGeneratedEnums.Company.Companion)
                return;

            string path = HttpContext.Current.Server.MapPath(ModConfig.FUP_PATH + Claim.DisplayClaimId + ".FUP");
            StreamWriter sw = new StreamWriter(path);
            sw.WriteLine(m_Fields.EntireRecord);
            sw.Close();

            Debug.WriteLine("ImageRight FUP File Written.  Claim: " + Claim.DisplayClaimId + ", Policy: " + Claim.PolicyNo + ", Insured: " + this.Claim.InsuredPerson.LastName + ", " + this.Claim.InsuredPerson.FirstName);            
        }
        #endregion
    }
}

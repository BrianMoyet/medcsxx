﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UnsavedFileNoteAlert : MedCsXTable
    {


        #region Constructors
        public UnsavedFileNoteAlert() : base() { }
        public UnsavedFileNoteAlert(int id) : base(id) { }
        public UnsavedFileNoteAlert(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Unsaved_File_Note_Alert"; }
        }

        public int UnsavedFileNoteAlertId
        {
            get { return (int)row["Unsaved_File_Note_Alert_Id"]; }
            set { this.setRowValue("Unsaved_File_Note_Alert_Id", value); }
        }

        public int UnsavedFileNoteId
        {
            get { return (int)row["Unsaved_File_Note_Id"]; }
            set { this.setRowValue("Unsaved_File_Note_Id", value); }
        }

        public string UserName
        {
            get { return (string)row["User_Name"]; }
            set { this.setRowValue("User_Name", value); }
        }
        #endregion
    }
}

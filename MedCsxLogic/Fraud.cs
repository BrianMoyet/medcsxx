﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
   public  class Fraud : MedCsXTable 
    {

       public Fraud() : base() { }
       public Fraud(int id) : base(id) { }
       public Fraud(string description) : base(description) { }
       public Fraud(Hashtable row) : base(row) { }

       public override string TableName
       {
           get { return "NICB"; }
       }

       public int NICBId
       {
           get { return (int)row["NICB_Id"]; }
           set { this.setRowValue("NICB_Id", value); }
       }

       public int VendorId
       {
           get { return (int)row["Vendor_Id"]; }
           set { this.setRowValue("Vendor_Id", value); }
       }

       public int PersonId
       {
           get { return (int)row["Person_Id"]; }
           set { this.setRowValue("Person_Id", value); }
       }

       public int VehicleId
       {
           get { return (int)row["Vehicle_Id"]; }
           set { this.setRowValue("Vehicle_Id", value); }
       }
       
       public int ClaimId
       {
           get { return (int)row["Claim_Id"]; }
           set { this.setRowValue("Claim_Id", value); }
       }

       public int NICBTypeId
       {
           get { return (short)row["NICB_Type_Id"]; }
           set { this.setRowValue("NICB_Type_Id", value); }
       }
      
       public short PersonTypeId
       {
           get { return (short)row["Person_Type_Id"]; }
           set { this.setRowValue("Person_Type_Id", value); }
       }

       public string Description
       {
           get { return (string)row["Description"]; }
           set { this.setRowValue("Description", value); }
       }

       public Claim Claim
       {
           get { return new Claim(this.ClaimId); }
       }

       public Vendor Vendor
       {
           get { return new Vendor(this.VendorId); }
       }

       public Person Person
       {
           get { return new Person(this.PersonId); }
       }

       public Vehicle Vehicle
       {
           get { return new Vehicle(this.VehicleId); }
       }

       public string PolicyNo
       {
           get { return this.Claim.PolicyNo; }
       }
    }
}

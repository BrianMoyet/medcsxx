﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BIDiagnosisType:TypeTable
    {

        #region Constructors
        public BIDiagnosisType() : base() { }
        public BIDiagnosisType(int id) : base(id) { }
        public BIDiagnosisType(string description) : base(description) { }
        public BIDiagnosisType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "BI_Diagnosis_Type";
            }
        }

        public int BIDiagnosisTypeId
        {
            get { return (int)row["BI_Diagnosis_Type_Id"]; }
            set { this.setRowValue("BI_Diagnosis_Type_Id", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MedCsxLogic
{
    public class ReserveGrid
    {

        public int reserveImage { get; set; }
        public int id { get; set; }
        public string claimId { get; set; }
        public string claimant { get; set; }
        public int reserveId { get; set; }
        public string reserveType { get; set; }
        public string reserveStatus { get; set; }
        public string reservingInd { get; set; }
        public string adjusterName { get; set; }
        public DateTime dtReported { get; set; }
        public string policyNo { get; set; }
        public string insName { get; set; }
        public DateTime dtOfLoss { get; set; }
        public int rowColor { get; set; }
        public double netLossReserve { get; set; }
        public double netLossPaid { get; set; }
        public double netExpenseReserve { get; set; }
        public double netExpensePaid { get; set; }
        public double grossLossReserve { get; set; }
        public double grossExpenseReserve { get; set; }
        public string description { get; set; }
        public string instructions { get; set; }
        public string adjCode { get; set; }
        public DateTime dtDue { get; set; }
        public bool isSelected { get; set; }
        public BitmapImage imageIdx
        {
            get
            {
                return modLogic.ImageList(reserveImage);
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PersonType:TypeTable
    {

        #region Constructors
        public PersonType() : base() { }
        public PersonType(int id) : base(id) { }
        public PersonType(string description) : base(description) { }
        public PersonType(Hashtable row) : base(row) { }
        #endregion

        #region Methods
        public override string TableName
        {
            get
            {
                return "Person_Type";
            }
        }

        public int PhoneCallFileNoteTypeId
        {
            get { return (int)row["Phone_Call_File_Note_Type_Id"]; }
            set { this.setRowValue("Phone_Call_File_Note_Type_Id", value); }
        }

        public bool isNamedInsured()
        {
            return this.Id == (int)modGeneratedEnums.PersonType.Named_Insured;
        }

        public bool isExcludedDriver()
        {
            return this.Id == (int)modGeneratedEnums.PersonType.Excluded_Driver;
        }

        public bool isAdditionalDriver()
        {
            return this.Id == (int)modGeneratedEnums.PersonType.Additional_Driver;
        }

        public bool isOnPolicy()
        {
            return (this.isAdditionalDriver() || this.isExcludedDriver() || this.isNamedInsured());
        }
        #endregion
    }
}

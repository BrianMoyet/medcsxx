﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BIEvaluation:MedCsXTable
    {

        #region Constructors
        public BIEvaluation() : base() { }
        public BIEvaluation(int id) : base(id) { }
        public BIEvaluation(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "BI_Evaluation"; }
        }

        public double AdjustedExpectedValue
        {
            get { return (double)((decimal)row["Adjusted_Expected_Value"]); }
            set { this.setRowValue("Adjusted_Expected_Value", value); }
        }

        public double AdjustedLossHigh
        {
            get { return (double)((decimal)row["Adjusted_Loss_High"]); }
            set { this.setRowValue("Adjusted_Loss_High", value); }
        }

        public double AdjustedLossLow
        {
            get { return (double)((decimal)row["Adjusted_Loss_Low"]); }
            set { this.setRowValue("Adjusted_Loss_Low", value); }
        }

        public double AmbulanceBestCase
        {
            get { return (double)((decimal)row["Ambulance_Best_Case"]); }
            set { this.setRowValue("Ambulance_Best_Case", value); }
        }

        public double AmbulanceWorstCase
        {
            get { return (double)((decimal)row["Ambulance_Worst_Case"]); }
            set { this.setRowValue("Ambulance_Worst_Case", value); }
        }

        public int BIEvaluationId
        {
            get { return (int)row["BI_Evaluation_Id"]; }
            set { this.setRowValue("BI_Evaluation_Id", value); }
        }

        public int BIInjuryTypeId
        {
            get { return (int)row["BI_Injury_Type_Id"]; }
            set { this.setRowValue("BI_Injury_Type_Id", value); }
        }
        
        public double ComparativeBestCase
        {
            get { return (double)((decimal)row["Comparative_Best_Case"]); }
            set { this.setRowValue("Comparative_Best_Case", value); }
        }

        public double ComparativeWorstCase
        {
            get { return (double)((decimal)row["Comparative_Worst_Case"]); }
            set { this.setRowValue("Comparative_Worst_Case", value); }
        }

        public double FutureMedicalBestCase
        {
            get { return (double)((decimal)row["Future_Medical_Best_Case"]); }
            set { this.setRowValue("Future_Medical_Best_Case", value); }
        }

        public double FutureMedicalWorstCase
        {
            get { return (double)((decimal)row["Future_Medical_Worst_Case"]); }
            set { this.setRowValue("Future_Medical_Worst_Case", value); }
        }

        public double GeneralDamagesBestCase
        {
            get { return (double)((decimal)row["General_Damages_Best_Case"]); }
            set { this.setRowValue("General_Damages_Best_Case", value); }
        }

        public double GeneralDamagesWorstCase
        {
            get { return (double)((decimal)row["General_Damages_Worst_Case"]); }
            set { this.setRowValue("General_Damages_Worst_Case", value); }
        }

        public int InjuredId
        {
            get { return (int)row["Injured_Id"]; }
            set { this.setRowValue("Injured_Id", value); }
        }

        public string Notes
        {
            get { return (string)row["Notes"]; }
            set { this.setRowValue("Notes", value); }
        }
        
        public double ProbabilityDamage
        {
            get { return (double)row["Probability_Damage"]; }
            set { this.setRowValue("Probability_Damage", value); }
        }

        public double ProbabilityLiability
        {
            get { return (double)row["Probability_Liability"]; }
            set { this.setRowValue("Probability_Liability", value); }
        }

        public double UnadjustedExpectedValue
        {
            get { return (double)((decimal)row["Unadjusted_Expected_Value"]); }
            set { this.setRowValue("Unadjusted_Expected_Value", value); }
        }

        public double UnadjustedLossHigh
        {
            get { return (double)((decimal)row["Unadjusted_Loss_High"]); }
            set { this.setRowValue("Unadjusted_Loss_High", value); }
        }

        public double UnadjustedLossLow
        {
            get { return (double)((decimal)row["Unadjusted_Loss_Low"]); }
            set { this.setRowValue("Unadjusted_Loss_Low", value); }
        }
        public double WageLossBestCase
        {
            get { return (double)((decimal)row["Wage_Loss_Best_Case"]); }
            set { this.setRowValue("Wage_Loss_Best_Case", value); }
        }

        public double WageLossWorstCase
        {
            get { return (double)((decimal)row["Wage_Loss_Worst_Case"]); }
            set { this.setRowValue("Wage_Loss_Worst_Case", value); }
        }
        #endregion

        #region Misc Methods
        public BIInjuryType BIInjuryType()
        {
            return new BIInjuryType(this.BIInjuryTypeId);
        }

        public Injured Injured()
        {
            return new Injured(this.InjuredId);
        }

        public Claimant Claimant()
        {
            return this.Injured().Claimant();
        }

        public Claim Claim()
        {
            return this.Injured().Claim;
        }

        public ArrayList BIDiagnostics()
        {
            return this.ChildObjects(typeof(BIDiagnostic));
        }

        public ArrayList BITreatments()
        {
            return this.ChildObjects(typeof(BITreatment));
        }

        public void DeleteBIDiagnostics()
        {
            this.DeleteChildObjects(typeof(BIDiagnostic));
        }

        public void DeleteBITreatments()
        {
            this.DeleteChildObjects(typeof(BITreatment));
        }

        public State State
        {
            get
            {
                try
                {
                    return this.Claim().Policy().State;
                }
                catch
                {
                    return this.Claim().LossState;
                }
            }
        }

        public double TotalBestCaseDiagnostics
        {
            get
            {
                double total = 0;
                foreach (BIDiagnostic d in this.BIDiagnostics())
                    total += d.BestCaseCost;
                return total;
            }
        }

        public double TotalWorstCaseDiagnostics
        {
            get
            {
                double total = 0;
                foreach (BIDiagnostic d in this.BIDiagnostics())
                    total += d.WorstCaseCost;
                return total;
            }
        }

        public decimal TotalBestCaseTreatments
        {
            get
            {
                decimal total = 0;
                foreach (BITreatment t in this.BITreatments())
                    total += t.CostBestCase;
                return total;
            }
        }

        public decimal TotalWorstCaseTreatments
        {
            get
            {
                decimal total = 0;
                foreach (BITreatment t in this.BITreatments())
                    total += t.CostWorstCase;
                return total;
            }
        }
        #endregion
    }
}

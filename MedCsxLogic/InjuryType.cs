﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class InjuryType:TypeTable
    {

        #region Constructors
        public InjuryType() : base() { }
        public InjuryType(int id) : base(id) { }
        public InjuryType(string description) : base(description) { }
        public InjuryType(Hashtable row) : base(row) { }
        #endregion

        public override string TableName
        {
            get
            {
                return "Injury_Type";
            }
        }
    }
}

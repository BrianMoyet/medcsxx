﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class FileNoteType:TypeTable
    {
        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public FileNoteType() : base() { }
        public FileNoteType(int id) : base(id) { }
        public FileNoteType(string description) : base(description) { }
        public FileNoteType(Hashtable row) : base(row) { }
        #endregion

        #region Class Variables
        private static List<Hashtable> m_UserFileNoteTypes = null;
        #endregion

        #region Column Names
        public int FileNoteLevelId
        {
            get { return (int)row["File_Note_Level_Id"]; }
            set { this.setRowValue("File_Note_Level_Id", value); }
        }

        public int FileNoteTypeId
        {
            get { return (int)row["File_Note_Type_Id"]; }
            set { this.setRowValue("File_Note_Type_Id", value); }
        }

        public int FiresEventTypeId
        {
            get { return (int)row["Fires_Event_Type_Id"]; }
            set { this.setRowValue("Fires_Event_Type_Id", value); }
        }

        public bool isPhoneCallType
        {
            get { return (bool)row["Is_Phone_Call_Type"]; }
            set { this.setRowValue("Is_Phone_Call_Type", value); }
        }

        public bool isSystemType
        {
            get { return (bool)row["Is_System_Type"]; }
            set { this.setRowValue("Is_System_Type", value); }
        }
        #endregion

        #region Methods
        public static List<Hashtable> UserFileNoteTypes
        {
            get
            {
                return m_UserFileNoteTypes == null ? 
                    m_UserFileNoteTypes = db.ExecuteSelectReturnHashList("usp_Get_User_File_Note_Types") : m_UserFileNoteTypes;
            }
        }

        public override string TableName
        {
            get
            {
                return "File_Note_Type";
            }
        }

        public bool isPhoneCallClaimant()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.Phone_Call_Claimant ? 
                true : false;
        }

        public bool isPhoneCallInsured()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.Phone_Call_Insured ?
                true : false;
        }

        public bool isPhoneCallOther()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.Phone_Call_Other ?
                true : false;
        }

        public bool isAttemptCallClaimant()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.Attempt_Contact_Claimant ?
                true : false;
        }

        public bool isFirstContactClaimant()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.First_Contact_Claimant ?
                true : false;
        }

        public bool isFirstContactNamedInsured()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.First_Contact_Named_Insured ?
                true : false;
        }

        public bool isAttemptContactInsured()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.Attempt_Contact_Insured ?
                true : false;
        }

        public bool isDraftPrinted()
        {
            return this.Id == (int)modGeneratedEnums.FileNoteType.Draft_Printed ?
                true : false;
        }

        public FileNoteLevel FileNoteLevel()
        {
            return new FileNoteLevel(this.FileNoteLevelId);
        }
        #endregion
    }
}

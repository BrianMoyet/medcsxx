﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UserType:TypeTable
    {

        #region Constructors
        public UserType() : base() { }
        public UserType(int id) : base(id) { }
        public UserType(string description) : base(description) { }
        public UserType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int DiaryGracePeriod
        {
            get { return (int)row["Diary_Grace_Period"]; }
            set { this.setRowValue("Diary_Grace_Period", value); }
        }

        public bool ForceDiary
        {
            get { return (bool)row["Force_Diary"]; }
            set { this.setRowValue("Force_Diary", value); }
        }

        public int UserTypeId
        {
            get { return (int)row["User_Type_Id"]; }
            set { this.setRowValue("User_Type_Id", value); }
        }

        public int SubroMaxDiaryDays
        {
            get { return (int)row["Subro_Max_Diary_Days"]; }
            set { this.setRowValue("Subro_Max_Diary_Days", value); }
        }

        public int SalvageMaxDiaryDays
        {
            get { return (int)row["Salvage_Max_Diary_Days"]; }
            set { this.setRowValue("Salvage_Max_Diary_Days", value); }
        }
        #endregion


        public override string TableName
        {
            get
            {
                return "User_Type";
            }
        }
    }
}

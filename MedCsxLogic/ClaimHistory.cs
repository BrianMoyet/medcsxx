﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ClaimHistory:TypeTable
    {

        private static modLogic ml = new modLogic();
        #region Contructors
        public ClaimHistory()
            : base()
        {
            this.StatusCloseDate = ml.DEFAULT_DATE;
        }
        public ClaimHistory(int id) : base(id) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Claim_History";
            }
        }

        public int ClaimHistoryId
        {
            get { return (int)row["Claim_History_Id"]; }
            set { this.setRowValue("Claim_History_Id", value); }
        }

        public int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public int ClaimStatusId
        {
            get { return (int)row["Claim_Status_Id"]; }
            set { this.setRowValue("Claim_Status_Id", value); }
        }

        public bool IsCurrentStatus
        {
            get { return (bool)row["Is_Current_Status"]; }
            set { this.setRowValue("Is_Current_Status", value); }
        }

        public DateTime StatusOpenDate
        {
            get { return (DateTime)row["Status_Open_Date"]; }
            set { this.setRowValue("Status_Open_Date", value); }
        }

        public DateTime StatusCloseDate
        {
            get { return (DateTime)row["Status_Close_Date"]; }
            set { this.setRowValue("Status_Close_Date", value); }
        }
        #endregion
    }
}

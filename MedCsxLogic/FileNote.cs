﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class FileNote:ClaimTable
    {

        private static modLogic ml = new modLogic();
        private static clsDatabase db = new clsDatabase();
     
        #region Constructors
        public FileNote() : base() { }
        public FileNote(int id) : base(id) { }
        public FileNote(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "File_Note"; }
        }
        public double AuthorityNeededToUnlock
        {
            get { return (double)((decimal)row["Authority_Needed_To_Unlock"]); }
            set { this.setRowValue("Authority_Needed_To_Unlock", value); }
        }

        public int ClaimantId
        {
            get { return (int)row["Claimant_Id"]; }
            set { this.setRowValue("Claimant_Id", value); }
        }

        public int DiaryTypeId
        {
            get { return (int)row["Diary_Type_Id"]; }
            set { this.setRowValue("Diary_Type_Id", value); }
        }

        public int DocumentId
        {
            get { return (int)row["Document_Id"]; }
            set { this.setRowValue("Document_Id", value); }
        }

        public int EventTypeId
        {
            get { return (int)row["Event_Type_Id"]; }
            set { this.setRowValue("Event_Type_Id", value); }
        }

        public int FileNoteId
        {
            get { return (int)row["File_Note_Id"]; }
            set { this.setRowValue("File_Note_Id", value); }
        }

        public string FileNoteText
        {
            get { return (string)row["File_Note_Text"]; }
            set { this.setRowValue("File_Note_Text", value); }
        }

        public int FileNoteTypeId
        {
            get { return (int)row["File_Note_Type_Id"]; }
            set { this.setRowValue("File_Note_Type_Id", value); }
        }

        public bool isSummary
        {
            get { return (bool)row["Is_Summary"]; }
            set { this.setRowValue("Is_Summary", value); }
        }

        public int ReserveId
        {
            get { return (int)row["Reserve_Id"]; }
            set { this.setRowValue("Reserve_Id", value); }
        }
        
        public int TransactionId
        {
            get { return (int)row["Transaction_Id"]; }
            set { this.setRowValue("Transaction_Id", value); }
        }

        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }
        #endregion

        #region Misc Methods
        public override int Update(Hashtable parms = null, bool fireEvents = true)
        {
            int fileNoteId;
            try
            {
                fileNoteId = base.Update(parms, fireEvents);
            }
            finally
            {
            }

            //update reserve assignment table on update and insert of file note
            //update adjuster entered first file note column on reserve table
            if (this.ReserveId != 0)
            {
                this.UpdateReserveAssignment();
                this.UpdateAdjusterEnteredFirstFileNote();
            }
            this.UpdateContactInsured();

            //if eventType is specified to be fired after file not is entered, fire it
            int eventTypeId = this.FileNoteType().FiresEventTypeId;

            if (eventTypeId != 0)
            {
                if (this.TransactionId != 0)
                    this.Transaction().FireEvent(eventTypeId, parms);
                else if (this.ReserveId != 0)
                    this.Reserve().FireEvent(eventTypeId, parms);
                else if (this.ClaimId != 0)
                    this.Claim.FireEvent(eventTypeId, parms);
            }
            return fileNoteId;
        }

        public static int InsertManualFileNote(int claimId, string fileNoteText, string fileNoteType, 
            bool alertAdjuster, bool alertSupervisor, bool alertTechnicalSupervisor, ArrayList alertUsers, 
            bool diaryAdjuster, bool diarySuperviser, bool diaryTechnicalSupervisor, ArrayList diaryUsers, 
            short diaryNbrDays, string claimant, string reserve, string diaryType, 
            bool deferAlerts, bool isSummary, DateTime deferredDate, bool isUrgent)
        {
            /*insert manual file note and send corresponding alerts or diary entries.
             * returns id of newly inserted file note.
            */

            //alerts
            EventType et = new EventType();
            et.SendAlertToAdjuster = alertAdjuster;
            et.SendAlertToSupervisor = alertSupervisor;
            et.SendAlertToTechnicalSupervisor = alertTechnicalSupervisor;
            et.areAlertsDeferred = deferAlerts;

            //diaries
            EventType det = new EventType();
            det.SendAlertToAdjuster = diaryAdjuster;
            det.SendAlertToSupervisor = diarySuperviser;
            det.SendAlertToTechnicalSupervisor = diaryTechnicalSupervisor;

            //insert file note row
            FileNote f = new FileNote();
            Claim claim = new Claim(claimId);
            int claimantId = claim.ClaimantId(claimant);
            int reserveTypeId = ml.getId("Reserve_Type", reserve);

            f.ClaimantId = claimantId;
            f.ClaimId = claimId;
            f.FileNoteText = fileNoteText;
            f.FileNoteTypeId = ml.getId("File_Note_Type", fileNoteType);
            f.DiaryTypeId = ml.getId("Diary_Type", diaryType);
            //f.UserId = MedCsXSystem.UserId;
            f.UserId = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            f.ReserveId = new Claimant(claimantId).ReserveId(reserveTypeId);
            f.isSummary = isSummary;
            

            int fileNoteId = f.Update();

            //send alerts
            et.InsertAlerts(claimId, f.ReserveId, fileNoteId, User.UserIdsForNames(ref alertUsers), Convert.ToInt32(HttpContext.Current.Session["userID"]), deferredDate, isUrgent);

            //set diary entries
            et.InsertDiaries(claimId, f.ReserveId, fileNoteId, User.UserIdsForNames(ref diaryUsers), diaryNbrDays, Convert.ToInt32(HttpContext.Current.Session["userID"]));

            return fileNoteId;
        }

        public FileNoteType FileNoteType()
        {
            return new FileNoteType(this.FileNoteTypeId);
        }

        public Transaction Transaction()
        {
            return this.TransactionId == 0 ? null : new Transaction(this.TransactionId);
        }

        public bool isLossPayment
        {
            get
            {
                if (this.FileNoteType().isDraftPrinted())
                {
                    Transaction t = this.Transaction();
                    if (t != null)
                    {
                        if (t.isLoss)
                            return true;
                    }
                }
                return false;
            }
        }

        public void UpdateReserveAssignment()
        {
            //update reserve assignment history table if certain types of file notes are added
            if (!this.FileNoteType().isSystemType)
            {
                if (this.ReserveId == 0)
                    return;

                DateTime currentTime = ml.DBNow();
                Reserve res = new Reserve(this.ReserveId);

                if ((res.DateFirstLossPayment == ml.DEFAULT_DATE) && this.isLossPayment)
                {
                    res.DateFirstLossPayment = currentTime;
                    res.Update();
                }

                Claimant cl = res.Claimant;
                bool claimantUpdated = false;

                if ((cl.DateAttemptContactClaimant == ml.DEFAULT_DATE) && this.FileNoteType().isAttemptCallClaimant())
                {
                    cl.DateAttemptContactClaimant = currentTime;
                    claimantUpdated = true;
                }

                if ((cl.DateFirstContactClaimant == ml.DEFAULT_DATE) && this.FileNoteType().isFirstContactClaimant())
                {
                    cl.DateFirstContactClaimant = currentTime;
                    claimantUpdated = true;
                }

                if (claimantUpdated)
                    cl.Update();

                ReserveAssignment ra = res.CurrentReserveAssignment();

                if (ra != null)
                {
                    if (ra.DateEnteredFirstFileNote == ml.DEFAULT_DATE)
                        ra.DateEnteredFirstFileNote = currentTime;
                    ra.Update();
                }
            }
        }

        public void UpdateAdjusterEnteredFirstFileNote()
        {
            if (this.ReserveId == 0)
                return;

            if (this.FileNoteType().isSystemType)
                return;

            Reserve res = new Reserve(this.ReserveId);
            if (!res.AdjusterEnteredFirstFileNote)
            {
                res.AdjusterEnteredFirstFileNote = true;
                res.Update();
            }
        }

        public void ClearAlerts()
        {
            //clears all alerts issued for a file note
            DataTable myAlerts = db.ExecuteStoredProcedure("usp_Get_Alerts_For_File_Note", this.Id);
            foreach (DataRow row in myAlerts.Rows)
            {
                Alert alert = new Alert(myAlerts, row);
                alert.ShowAlert = false;
                alert.Update();
            }
        }

        public void ClearDiaries()
        {
            foreach (Diary d in this.ChildObjects(typeof(Diary)))
                d.Clear();
        }

        public void ClearDiaryEntry()
        {
            //clears the diary entry for a given file note ID and current user
            db.ExecuteStoredProcedure("usp_Clear_Diary_Entry", this.FileNoteId, ml.CurrentUser.Id);
        }

        public bool isForLock
        {
            //does file note correspond to a claim, reserve, or transaction lock?
            get { return db.GetBoolFromStoredProcedure("usp_Is_File_Note_For_Lock", this.Id); }
        }

        public bool isForClaimLock
        {
            //does file note correspond to a claim lock?
            get { return db.GetBoolFromStoredProcedure("usp_Is_File_Note_For_Claim_Lock", this.Id); }
        }
        
        public bool isForReserveLock
        {
            //does file note correspond to a reserve lock?
            get { return db.GetBoolFromStoredProcedure("usp_Is_File_Note_For_Reserve_Lock", this.Id); }
        }

        public bool isForTransactionLock
        {
            //does file note correspond to a transaction lock?
            get { return db.GetBoolFromStoredProcedure("usp_Is_File_Note_For_Transaction_Lock", this.Id); }
        }

        public Alert Alert()
        {
            //get alert for file note and current user
            DataTable a = db.ExecuteStoredProcedure("usp_Get_Alert_For_File_Note_And_User", this.Id, Convert.ToInt32(HttpContext.Current.Session["userID"]));
            return a.Rows.Count == 0 ? null : new Alert(a, a.Rows[0]);
        }

        public EventType EventType()
        {
            return new EventType(this.EventTypeId);
        }

        private void UpdateContactInsured()
        {
            if (this.FileNoteType().isAttemptContactInsured())
            {
                Claim c = this.Claim;
                if (c.DateAttemptContactInsured == ml.DEFAULT_DATE)
                {
                    c.DateAttemptContactInsured = ml.DBNow();
                    c.Update();
                }
            }

            if (this.FileNoteType().isFirstContactNamedInsured())
            {
                Claim c = this.Claim;
                if (c.DateFirstContactInsured == ml.DEFAULT_DATE)
                {
                    c.DateFirstContactInsured = ml.DBNow();
                    c.Update();
                }
            }
        }

        public Diary DiaryForUser(User v)
        {
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Diary_For_File_Note_And_User", this.Id, v.Id);
            return rows.Rows.Count == 0 ? null : new Diary(rows, rows.Rows[0]);
        }

        public Reserve Reserve()
        {
            return new Reserve(this.ReserveId);
        }

        public Claimant Claimant()
        {
            return new Claimant(this.ClaimantId);
        }

        public DiaryType DiaryType()
        {
            return new DiaryType(this.DiaryTypeId);
        }










































        #endregion



    }
}

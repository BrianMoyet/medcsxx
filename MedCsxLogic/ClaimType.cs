﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ClaimType:TypeTable
    {

        #region Constructors
        public ClaimType() : base() { }
        public ClaimType(int id) : base(id) { }
        public ClaimType(string description) : base(description) { }
        public ClaimType(Hashtable row) : base(row) { }
        #endregion

        #region Table Properties
        public override string TableName
        {
            get
            {
                return "Claim_Type";
            }
        }
        #endregion
    }
}

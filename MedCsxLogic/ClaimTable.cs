﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{

    public abstract class ClaimTable : MedCsXTable
    {
        #region Class Variables
        private Claim m_Claim = null;
        #endregion

        #region Constructors
        public ClaimTable() : base() { }
        public ClaimTable(int id) : base(id) { }
        public ClaimTable(Hashtable row) : base(row) { }
        public ClaimTable(DataTable Table, DataRow Row) : base(Table,Row) { }
        #endregion

        #region Methods
        public virtual int ClaimId
        {
            get { return (int)row["Claim_Id"]; }
            set { this.setRowValue("Claim_Id", value); }
        }

        public Claim Claim
        {
            get { return m_Claim == null ? m_Claim = new Claim(ClaimId) : m_Claim; }
            set { m_Claim = value; }
        }
        #endregion



    }
}

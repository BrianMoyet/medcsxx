﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class ICDCode : MedCsXTable
    {

        private int m_Slot;     //position on Injured screen

        public ICDCode() : base() { }
        public ICDCode(int id) : base(id) { }

        public override string TableName
        {
            get { return "ICD_Code"; }
        }

        public int ICDCodeId
        {
            get { return (int)row["ICD_Code_Id"]; }
            set { this.setRowValue("ICD_Code_Id", value); }
        }

        public int InjuredId
        {
            get { return (int)row["Injured_Id"]; }
            set { this.setRowValue("Injured_Id", value); }
        }

        public string Code
        {
            get { return (string)row["ICD_Code"]; }
            set { this.setRowValue("ICD_Code", value); }
        }

        public bool isICDCode
        {
            get { return (bool)row["Is_ICD_Code"]; }
            set { this.setRowValue("Is_ICD_Code", value); }
        }

        public bool isVCode
        {
            get { return (bool)row["Is_V_Code"]; }
            set { this.setRowValue("Is_V_Code", value); }
        }

        public int Slot
        {
            get { return m_Slot; }
            set { m_Slot = value; }
        }
    }
}

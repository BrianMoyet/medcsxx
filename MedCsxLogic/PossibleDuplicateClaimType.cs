﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PossibleDuplicateClaimType:TypeTable
    {

        #region Constructors
        public PossibleDuplicateClaimType() : base() { }
        public PossibleDuplicateClaimType(int id) : base(id) { }
        public PossibleDuplicateClaimType(string desc) : base(desc) { }
        public PossibleDuplicateClaimType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Possible_Duplicate_Claim_Type";
            }
        }

        public int PossibleDuplicatClaimTypeId
        {
            get { return (int)row["Possible_Duplicate_Claim_Type_Id"]; }
            set { this.setRowValue("Possible_Duplicate_Claim_Type_Id", value); }
        }
        #endregion
    }
}

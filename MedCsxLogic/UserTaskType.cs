﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UserTaskType:TypeTable
    {

        #region Constructors
        public UserTaskType() : base() { }
        public UserTaskType(int id) : base(id) { }
        public UserTaskType(string description) : base(description) { }
        public UserTaskType(Hashtable row) : base(row) { }
        public UserTaskType(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "User_Task_Type";
            }
        }

        public int ClearedFileNoteTypeId
        {
            get { return (int)row["Cleared_File_Note_Type_Id"]; }
            set { this.setRowValue("Cleared_File_Note_Type_Id", value); }
        }

        public int ClearUserTaskLevelId
        {
            get { return (int)row["Clear_User_Task_Level_Id"]; }
            set { this.setRowValue("Clear_User_Task_Level_Id", value); }
        }

        public int CreateUserTaskLevelId
        {
            get { return (int)row["Create_User_Task_Level_Id"]; }
            set { this.setRowValue("Create_User_Task_Level_Id", value); }
        }

        public int UserTaskTypeId
        {
            get { return (int)row["User_Task_Type_Id"]; }
            set { this.setRowValue("User_Task_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public FileNoteType ClearedFileNoteType()
        {
            return new FileNoteType(this.ClearedFileNoteTypeId);
        }

        public UserTaskLevel ClearUserTaskLevel()
        {
            return new UserTaskLevel(this.ClearUserTaskLevelId);
        }

        public UserTaskLevel CreateUserTaskLevel()
        {
            return new UserTaskLevel(this.CreateUserTaskLevelId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class UserTaskLevel:TypeTable
    {

        #region Constructors
        public UserTaskLevel() : base() { }
        public UserTaskLevel(int id) : base(id) { }
        public UserTaskLevel(string descrip) : base(descrip) { }
        public UserTaskLevel(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "User_Task_Level";
            }
        }

        public int UserTaskLevelId
        {
            get { return (int)row["User_Task_Level_Id"]; }
            set { this.setRowValue("User_Task_Level_Id", value); }
        }
        #endregion

        #region Misc Methods
        public bool isClaimLevel
        {
            get { return this.Id==(int)modGeneratedEnums.UserTaskLevel.Claim;}
        }

        public bool isClaimantLevel
        {
            get { return this.Id == (int)modGeneratedEnums.UserTaskLevel.Claimant; }
        }

        public bool isReserveLevel
        {
            get { return this.Id == (int)modGeneratedEnums.UserTaskLevel.Reserve; }
        }

        public bool isTransactionLevel
        {
            get { return this.Id == (int)modGeneratedEnums.UserTaskLevel.Transaction; }
        }

        public bool isNoneLevel
        {
            get { return this.Id == (int)modGeneratedEnums.UserTaskLevel.None; }
        }
        #endregion
    }
}

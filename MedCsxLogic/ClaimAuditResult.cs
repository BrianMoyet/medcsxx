﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public  class ClaimAuditResult:TypeTable
    {

        #region Constructors
        public ClaimAuditResult() : base() { }
        public ClaimAuditResult(int id) : base(id) { }
        public ClaimAuditResult(Hashtable row) : base(row) { }
        #endregion

        #region Misc Methods
        public override string TableName
        {
            get
            {
                return "Claim_Audit_Result";
            }
        }

        public bool isYes
        {
            get { return this.Id == (int)modGeneratedEnums.ClaimAuditResult.Yes; }
        }

        public bool isNo
        {
            get { return this.Id == (int)modGeneratedEnums.ClaimAuditResult.No; }
        }
        public bool isNA
        {
            get { return this.Id == (int)modGeneratedEnums.ClaimAuditResult.N_A; }
        }
        #endregion
    }
}

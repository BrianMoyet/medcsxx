﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MedCsxLogic
{
    public class ClaimantGrid
    {

        public int claimantId { get; set; }
        public int witnessId { get; set; }
        public int imageIdx { get; set; }
        public string personType { get; set; }
        public string fullName { get; set; }
        public string streetAddress { get; set; }
        public string Address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string cDOB { get; set; }
        public int cAge { get; set; }
        public string cSex { get; set; }
        public string MaritalStatus { get; set; }
        public string taxId { get; set; }
        public string payee { get; set; }
        public int draftNo { get; set; }
        public double draftAmount { get; set; }
        public int clCounter { get; set; }
        public bool fraud_select { get; set; }
        public string driversLic { get; set; }
        public string nameAddressType
        {
            get
            {
                if (!string.IsNullOrEmpty(fullName))
                {
                    string s = fullName + ", " + streetAddress + ", " + city + ", " + state + " " + postalCode + " " + personType;
                    return s.Replace(", , , ", string.Empty).Replace(", , ", string.Empty).Replace("  ", " ").Trim();
                }
                else
                    return "";
            }
        }
        public BitmapImage imageIndex
        {
            get { return modLogic.ImageList(imageIdx); }
        }
    }
}

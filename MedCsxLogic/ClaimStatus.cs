﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ClaimStatus:TypeTable
    {

        #region Constructors
        public ClaimStatus() : base() { }
        public ClaimStatus(int id) : base() { }
        public ClaimStatus(string descrip) : base(descrip) { }
        public ClaimStatus(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Claim_Status";
            }
        }

        public int ClaimStatusId
        {
            get { return (int)row["Claim_Status_Id"]; }
            set { this.setRowValue("Claim_Status_Id", value); }
        }
        #endregion
    }
}

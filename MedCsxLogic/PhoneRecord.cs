﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
   public class PhoneRecord:MedCsXTable
    {

       private static clsDatabase db = new clsDatabase();
       
       public PhoneRecord() : base() { }

       public List<Hashtable> CallRecord = new List<Hashtable>();
       private List<User> ActiveUsers = new List<User>();

       public override string TableName
       {
           get { return "Phone_Record"; }
       }

       public void PopulatePhoneRecord()
       {
           PopulateUsers();
           foreach (Hashtable pr in CallRecord)
           {
               if (((string)pr["File_Name"]).ToUpper().Substring(0, 11) != "CALL_RECORD")
               {
                   User v = null;
                   int testInt = 0;
                   int counter = 0;
                   bool inbound = false,
                       outboud = false;
                   DateTime callDate = default(DateTime);
                   string fileName = (string)pr["File_Name"];
                   string baseFileName = fileName;
                   string baseFileLocation = (string)pr["File_Location"];
                   string dateString = "";
                   string claimantPh = "";
                   string fileName2 = "";
                   string callLength = "0";
                   while (fileName.Length > 0)
                   {
                       int ptr = fileName.IndexOf('(') + 1;
                       int endPtr = fileName.IndexOf(')');
                       string item = fileName.Substring(ptr, endPtr - ptr);
                       switch (counter)
                       {
                           case 0:
                               if (!int.TryParse(item.Substring(0, 1), out testInt))
                               {

                                   string[] usr = item.Split('_');
                                   v = User.Named(usr[0] + " " + usr[1]);
                               }
                               else
                               {
                                   string userPh = item;
                                   if (userPh == "8555")
                                       break;
                                   foreach (User u in ActiveUsers)
                                   {
                                       if (userPh == u.PhoneExt)
                                       {
                                           v = u;
                                           break;
                                       }
                                   }
                               }
                               break;
                           case 1:
                               if (item.ToUpper() == "IN")
                                   inbound = true;
                               else
                                   outboud = true;
                               break;
                           case 2:
                           case 3:
                               if (counter == 2)
                                   dateString = item + "-";
                               else
                               {
                                   dateString += item;
                                   string[] dtParts = dateString.Split('-');
                                   string dtWhole = string.Format("{0}-{1}-{2} {3}:{4}:{5}", dtParts[0], dtParts[1], dtParts[2], dtParts[3], dtParts[4], dtParts[5]);
                                   callDate = DateTime.Parse(dtWhole);
                               }
                               break;
                           case 4:
                               if (ptr != endPtr)
                                   claimantPh = phoneStrip(item);
                               break;
                           default:
                               if (ptr != endPtr)
                                   callLength = item;
                               break;
                       }
                       if (fileName.Length > endPtr)
                       {
                           fileName2 = fileName.Substring(endPtr + 1, fileName.Length - (endPtr + 1));
                           fileName = fileName2.Replace(".wav","");
                       }
                       else
                           fileName = "";
                       counter++;
                   }
                   List<Hashtable> ExistingCalls = db.ExecuteStoredProcedureReturnHashList("usp_Search_Phone_Records_By_PhoneNumber", claimantPh);
                   bool isExisting = false;
                   foreach (Hashtable a in ExistingCalls)
                   {
                       if (baseFileName == (string)a["File_Name"])
                       {
                           isExisting = true;
                           break;
                       }
                   }
                   if (!isExisting)
                       PopulateCallRecord(claimantPh, callDate, v, callLength, baseFileLocation, baseFileName, inbound, outboud);
               }
           }
       }

       private void PopulateCallRecord(string phoneNum, DateTime callDate, User adjuster, string callLength, string fileLocation, string fileName, bool call_in, bool call_out)
       {
           PhoneCall pc = new PhoneCall();
           pc.UserId = adjuster.Id;
           pc.PhoneNumber = phoneNum;
           pc.CallStartTime = callDate;
           pc.CallLengthTicks = int.Parse(callLength);
           pc.IsVoiceMail = false;
           pc.PersonId = GetIdByPhoneDate(phoneNum, callDate);
           pc.ClaimId = pc.Person.ClaimId;
           pc.FileLocation = fileLocation;
           pc.IsInbound = call_in;
           pc.IsOutbound = call_out;
           pc.FileName = fileName;
           pc.Update();
       }

       private int GetIdByPhoneDate(string phoneNum, DateTime callDate)
       {
           List<Hashtable> possPersons = db.ExecuteStoredProcedureReturnHashList("usp_Search_Persons_Fraud", "-", "-", "-", phoneNum);
           if (possPersons.Count <= 0)      //nothing found
               return 0;
           else if (possPersons.Count == 1) //only 1 record
               return (int)possPersons[0]["Person_Id"];
           else                             //multiple records
           {
               DateTime lastDate = DateTime.Parse("01-01-1900");
               int bestPerson = 0;
               foreach (Hashtable p in possPersons)
               {
                   DateTime thisDate =(DateTime)p["Created_Date"];
                   if((thisDate > lastDate)&& (thisDate.Date <=callDate.Date) )
                   {
                       lastDate = thisDate;
                       bestPerson = (int)p["Person_Id"];
                   }
               }
               return bestPerson;
           }
       }

       private string phoneStrip(string p)
       {
           string phNum = p.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "").Replace("_", "").Replace("x", "").Replace(" ", "").Replace("+","");
           string p2;

           if ((phNum.ToUpper() == "UNKNOWN") || (phNum.ToUpper() == "ANONYMOUS"))
               return phNum;
           else if (phNum.Substring(0, 1) == "1")
               phNum = phNum.Substring(1, phNum.Length - 1);

           if (phNum.Length > 10)
               p2 = "(" + phNum.Substring(0, 3) + ") " + phNum.Substring(3, 3) + "-" + phNum.Substring(6, 4) + "x" + phNum.Substring(10, phNum.Length - 10);
           else if (phNum.Length == 10)
               p2 = "(" + phNum.Substring(0, 3) + ") " + phNum.Substring(3, 3) + "-" + phNum.Substring(6, 4);
           else
               p2 = phNum;

           return p2;
       }

       private void PopulateUsers()
       {
           foreach (Hashtable a in db.ExecuteStoredProcedureReturnHashList("usp_Get_All_Users"))
           {
               if ((string)a["Last_Name"] != "")
                   ActiveUsers.Add(new User((int)a["User_Id"]));
           }
       }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class AtFaultType:TypeTable
    {

        #region Constructors
        public AtFaultType() : base() { }
        public AtFaultType(int id) : base(id) { }
        public AtFaultType(string desc) : base(desc) { }
        public AtFaultType(Hashtable row) : base(row) { }
        #endregion

        #region Misc Methods
        public override string TableName
        {
            get
            {
                return "At_Fault_Type";
            }
        }

        public bool isUndetermined
        {
            get { return this.Id == (int)modGeneratedEnums.AtFaultType.Undetermined; }
        }

        public bool isYes
        {
            get { return this.Id == (int)modGeneratedEnums.AtFaultType.Yes; }
        }

        public bool isNo
        {
            get { return this.Id == (int)modGeneratedEnums.AtFaultType.No; }
        }
        #endregion
    }
}

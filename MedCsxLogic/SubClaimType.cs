﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class SubClaimType:TypeTable
    {

        #region Constructors
        public SubClaimType() : base() { }
        public SubClaimType(int id) : base(id) { }
        public SubClaimType(string description) : base(description) { }
        public SubClaimType(Hashtable row) : base(row) { }
        #endregion

        #region Table Properties
        public override string TableName
        {
            get
            {
                return "Sub_Claim_Type";
            }
        }
        #endregion
       
        #region Column Names
        [System.ComponentModel.Description("Type of claim.")]
        public int ClaimTypeId
        {
            get { return (int)row["Claim_Type_Id"]; }
            set { this.setRowValue("Claim_Type_Id", value); }
        }
        #endregion

        #region Return New Object Functions
        public ClaimType GetClaimType()
        {
            return new ClaimType(this.ClaimTypeId);
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ReportedByType:TypeTable
    {

        #region Constructors
        public ReportedByType() : base() { }
        public ReportedByType(int id) : base(id) { }
        public ReportedByType(string description) : base(description) { }
        public ReportedByType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Reported_By_Type";
            }
        }

        [System.ComponentModel.Description("Type of claim.")]
        public int ClaimTypeId
        {
            get { return (int)row["Claim_Type_Id"]; }
            set { this.setRowValue("Claim_Type_Id", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MedCsxLogic
{
    public class ScriptParms:MedCsxObject
    {

        #region Class Variables
        private Hashtable m_Parms;
        #endregion

        #region Constructors
        public ScriptParms() : base() { }
        public ScriptParms(Hashtable row)
        {
            this.Parms = row;
        }
        #endregion

        #region Methods
        public Hashtable Parms
        {
            get { return this.m_Parms == null ? this.m_Parms = new Hashtable() : this.m_Parms; }
            set { this.m_Parms = value; }
        }

        public void UpdateParm(string key, object value)
        {
            if (this.Parms.Contains(key))
                this.Parms[key] = value;
            else
                this.Parms.Add(key, value);
        }

        public int ClaimId()
        {
            if (this.Parms.Contains("Claim_Id"))
                return (int)this.Parms["Claim_Id"];

            if (this.ClaimantId() != 0)
                return new Claimant(this.ClaimantId()).ClaimId;

            if (this.ReserveId() != 0)
                return new Reserve(this.ReserveId()).ClaimId;

            if (this.TransactionId() != 0)
                return new Transaction(this.TransactionId()).ClaimId;

            return 0;
        }

        public int ClaimantId()
        {
            if (this.Parms.Contains("Claimant_Id"))
                return (int)this.Parms["Claimant_Id"];

            if (this.ReserveId() != 0)
                return new Reserve(this.ReserveId()).ClaimantId;

            if (this.TransactionId() != 0)
                return new Transaction(this.TransactionId()).ClaimantId;

            return 0;
        }

        public int ReserveId()
        {
            if (this.Parms.Contains("Reserve_Id"))
                return (int)this.Parms["Reserve_Id"];

            if (this.TransactionId() != 0)
                return new Transaction(this.TransactionId()).ReserveId;

            return 0;
        }

        public int TransactionId()
        {
            return this.Parms.Contains("Transaction_Id") ? (int)this.Parms["Transaction_Id"] : 0;
        }

        public int DocumentId()
        {
            return this.Parms.Contains("Document_Id") ? (int)this.Parms["Document_Id"] : 0;
        }

        public int AppraisalTaskId()
        {
            return this.Parms.Contains("Appraisal_Task_Id") ? (int)this.Parms["Appraisal_Task_Id"] : 0;
        }

        public AppraisalTask AppraisalTask()
        {
            return this.AppraisalTaskId() == 0 ? null : new AppraisalTask(this.AppraisalTaskId());
        }

        public Claim Claim()
        {
            return this.ClaimId() == 0 ? null : new Claim(this.ClaimId());
        }

        public Claimant Claimant()
        {
            return this.ClaimantId() == 0 ? null : new Claimant(this.ClaimantId());
        }

        public Reserve Reserve()
        {
            return this.ReserveId() == 0 ? null : new Reserve(this.ReserveId());
        }

        public Transaction Transaction()
        {
            return this.TransactionId() == 0 ? null : new Transaction(this.TransactionId());
        }

        public bool isClaimEvent()
        {
            //Does parms denote a Claim event?
            if(Parms.Contains("Claim_Id"))
            {
                if (!Parms.Contains("Reserve_Id"))
                    return true;
            }
            return false;
        }

        public bool isReserveEvent()
        {
            //Does parms denote a Reserve event?
            if (Parms.Contains("Reserve_Id"))
            {
                if (!Parms.Contains("Transaction_Id"))
                    return true;
            }
            return false;
        }

        public bool isTransactionEvent()
        {
            //Does parms denote a Transaction event?
            return Parms.Contains("Transaction_Id") ? true : false;
        }

        public bool SyncEventExists(int eventTypeId)
        {
            //Does sync event already exist for the given event type - claim, reserve, transaction?
            if (this.isClaimEvent())
                return this.Claim().SyncEventExists(eventTypeId);

            if (this.isReserveEvent())
                return this.Reserve().SyncEventExists(ref eventTypeId);

            if (this.isTransactionEvent())
                return this.Transaction().SyncEventExists(ref eventTypeId);

            return false;
        }

        public void InsertSyncEvent(int eventTypeId)
        {
            if (this.isClaimEvent())
            {
                ClaimSyncEvent cs = new ClaimSyncEvent();
                cs.ClaimId = this.ClaimId();
                cs.EventTypeId = eventTypeId;
                cs.Update();
                return;
            }

            if (this.isReserveEvent())
            {
                ReserveSyncEvent rs = new ReserveSyncEvent();
                rs.ReserveId = this.ReserveId();
                rs.EventTypeId = eventTypeId;
                rs.Update();
                return;
            }

            if (this.isTransactionEvent())
            {
                TransactionSyncEvent ts = new TransactionSyncEvent();
                ts.TransactionId = this.TransactionId();
                ts.EventTypeId = eventTypeId;
                ts.Update();
                return;
            }
        }

        public void GenerateParms(MedCsXTable table)
        {
            this.GenerateParms(table.TableName, table.row);
        }

        public void GenerateParms(string tableName, Hashtable row)
        {
            //Generates parameters
            int draftId, 
                salvageId, 
                subroId, 
                deductiblesId, 
                transactionId, 
                reserveId, 
                claimantId, 
                claimId, 
                SIRrecoveryId, 
                SIRcreditId = 0;

            //create User_Id parameter, if possible
            if (!this.Parms.Contains("UserId"))
                this.Parms.Add("UserId", Convert.ToInt32(HttpContext.Current.Session["userID"]));

            //create Id parameter (primary key value of updated table)
            if (!this.Parms.Contains(tableName + "_Id"))
                this.Parms.Add(tableName + "_Id", row[tableName + "_Id"]);

            //create Draft_Id and Transaction_Id parameter, if possible
            if (row.Contains("Draft_Id"))
            {
                draftId = (int)row["Draft_Id"];
                if (!this.Parms.Contains("Draft_Id"))
                    this.Parms.Add("Draft_Id", draftId);

                if (!this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Transaction_Id", new Draft(draftId).TransactionId);
            }

            //create Salvage_Id and Transaction_Id parameter, if possible
            if (row.Contains("Salvage_Id"))
            {
                salvageId = (int)row["Salvage_Id"];
                if (!this.Parms.Contains("Salvage_Id"))
                    this.Parms.Add("Salvage_Id", salvageId);

                if (!this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Transaction_Id", new Salvage(salvageId).TransactionId);
            }

            //create Subro_Id and Transaction_Id parameter, if possible
            if (row.Contains("Subro_Id"))
            {
                subroId = (int)row["Subro_Id"];
                if (!this.Parms.Contains("Subro_Id"))
                    this.Parms.Add("Subro_Id", subroId);

                if (!this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Transaction_Id", new Subro(subroId).TransactionId);
            }

            //create Deductibles_Id and Transaction_Id parameter, if possible
            if (row.Contains("Deductibles_Id"))
            {
                deductiblesId = (int)row["Deductibles_Id"];
                if (!this.Parms.Contains("Deductibles_Id"))
                    this.Parms.Add("Deductibles_Id", deductiblesId);

                if (!this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Transaction_Id", new Deductibles(deductiblesId).TransactionId);
            }

            //create SIR_Recovery_Id and Transaction_Id parameter, if possible
            if (row.Contains("SIR_Recovery_Id"))
            {
                SIRrecoveryId = (int)row["SIR_Recovery_Id"];
                if (!this.Parms.Contains("SIR_Recovery_Id"))
                    this.Parms.Add("SIR_Recovery_Id", SIRrecoveryId);

                if (!this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Transaction_Id", new SIR(SIRrecoveryId).TransactionId());
            }

            //create SIR_Credit_Id and Transaction_Id parameter, if possible
            if (row.Contains("SIR_Credit_Id"))
            {
                SIRcreditId = (int)row["SIR_Credit_Id"];
                if (!this.Parms.Contains("SIR_Credit_Id"))
                    this.Parms.Add("SIR_Credit_Id", SIRcreditId);

                if (!this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Transaction_Id", new SIR(SIRcreditId).TransactionId());
            }

            //create Transaction_Id parameter, if possible
            if (!this.Parms.Contains("Transaction_Id"))
            {
                if (row.Contains("Transaction_Id"))
                {
                    transactionId = (int)row["Transaction_Id"];
                    this.Parms.Add("Transaction_Id", transactionId);
                }
            }

            //create Reserve_Id parameter, if possible
            if (!this.Parms.Contains("Reserve_Id"))
            {
                if (row.Contains("Reserve_Id"))
                {
                    reserveId = (int)row["Reserve_Id"];
                    this.Parms.Add("Reserve_Id", reserveId);
                }
                else if (this.Parms.Contains("Transaction_Id"))
                    this.Parms.Add("Reserve_Id", this.ReserveId());
            }

            //create Claimant_Id parameter, if possible
            if (!this.Parms.Contains("Claimant_Id"))
            {
                if (row.Contains("Claimant_Id"))
                {
                    claimantId = (int)row["Claimant_Id"];
                    this.Parms.Add("Claimant_Id", ClaimantId());
                }
                else if (!this.Parms.Contains("Reserve_Id"))
                    this.Parms.Add("Claimant_Id", this.ClaimantId());
            }

            //create Claim_Id and Transaction_Id parameter, if possible
            if (!this.Parms.Contains("Claim_Id"))
            {

                if (row.Contains("Claim_Id"))
                {
                    claimId = (int)row["Claim_Id"];
                    this.Parms.Add("Claim_Id", claimId);
                }
                else if (this.Parms.Contains("Claimant_Id"))
                {
                    if (!row.Contains("Appraisal_Task_Id"))
                        this.Parms.Add("Claim_Id", this.ClaimId());
                    else
                    {
                        AppraisalRequest ar = new AppraisalRequest((int)row["Appraisal_Request_Id"]);
                        this.Parms.Add("Claim_Id", ar.ClaimId);
                    }
                }
                else if (row.Contains("Appraisal_Task_Id"))
                {
                    AppraisalRequest ar = new AppraisalRequest((int)row["Appraisal_Request_Id"]);
                    this.Parms.Add("Claim_Id", ar.ClaimId);
                }
                else if (row.Contains("Vehicle_Id"))
                {
                    Vehicle v = new Vehicle((int)row["Vehicle_Id"]);
                    this.Parms.Add("Claim_Id", v.ClaimId);
                }

                //else if(row.Contains("ChoicePoint_Request_Id")){
                //    ChoicePointRequest c=new ChoicePointRequest((int)row["ChoicePoint_Request_Id"]);
                //    this.Parms.Add("Claim_Id", c.ClaimId);
                //}
            }
            

           

        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PropertyType:TypeTable
    {

        #region Constructors
        public PropertyType() : base() { }
        public PropertyType(int id) : base(id) { }
        public PropertyType(string description) : base(description) { }
        public PropertyType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Property_Type";
            }
        }
        public int PropertyTypeId
        {
            get { return (int)row["Property_Type_Id"]; }
            set { this.setRowValue("Property_Type_Id", value); }
        }

        [System.ComponentModel.Description("Type of claim.")]
        public int ClaimTypeId
        {
            get { return (int)row["Claim_Type_Id"]; }
            set { this.setRowValue("Claim_Type_Id", value); }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class UnsavedFileNote : ReserveTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public UnsavedFileNote() : base() { }
        public UnsavedFileNote(int id) : base(id) { }
        public UnsavedFileNote(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Unsaved_File_Note"; }
        }

        public DateTime DeferredDate
        {
            get { return (DateTime)row["Deferred_Date"]; }
            set { this.setRowValue("Deferred_Date", value); }
        }

        public int DiaryNbrDays
        {
            get { return (int)row["Diary_Nbr_Days"]; }
            set { this.setRowValue("Diary_Nbr_Days", value); }
        }

        public int DiaryTypeId
        {
            get { return (int)row["Diary_Type_Id"]; }
            set { this.setRowValue("Diary_Type_Id", value); }
        }

        public string FileNoteText
        {
            get { return (string)row["File_Note_Text"]; }
            set { this.setRowValue("File_Note_Text", value); }
        }

        public int FileNoteTypeId
        {
            get { return (int)row["File_Note_Type_Id"]; }
            set { this.setRowValue("File_Note_Type_Id", value); }
        }

        public bool IsDeferred
        {
            get { return (bool)row["Is_Deferred"]; }
            set { this.setRowValue("Is_Deferred", value); }
        }

        public bool IsSummary
        {
            get { return (bool)row["Is_Summary"]; }
            set { this.setRowValue("Is_Summary", value); }
        }

        public bool IsUrgent
        {
            get { return (bool)row["Is_Urgent"]; }
            set { this.setRowValue("Is_Urgent", value); }
        }

        public int UnsavedFileNoteId
        {
            get { return (int)row["Unsaved_File_Note_Id"]; }
            set { this.setRowValue("Unsaved_File_Note_Id", value); }
        }

        public int UserId
        {
            get { return (int)row["User_Id"]; }
            set { this.setRowValue("User_Id", value); }
        }

        public override int ClaimId
        {
            get
            {
                return (int)row["Claim_Id"];
            }
            set
            {
                this.setRowValue("Claim_Id", value);
            }
        }
        #endregion     
   
        #region Misc Methods
        public void DeleteAlertNames()
        {
            if (this.isAdd)
                return; //there are no alert names because parent row hasn't been added in Unsaved_File_Note

            db.ExecuteStoredProcedure("usp_Delete_Unsaved_File_Note_Alerts", this.Id);
        }

        public void DeleteDiaryNames()
        {
            if (this.isAdd)
                return; //there are no diary names because parent row hasn't been added in Unsaved_File_Note

            db.ExecuteStoredProcedure("usp_Delete_Unsaved_File_Note_Diaries", this.Id);
        }

        public void AddAlertName(string name)
        {
            //called after row is inserted - ID is not populated until then
            UnsavedFileNoteAlert a = new UnsavedFileNoteAlert();
            a.UnsavedFileNoteId = this.Id;
            a.UserName = name;
            a.Update();
        }
        public void AddDiaryName(string name)
        {
            //called after row is inserted - ID is not populated until then
            UnsavedFileNoteDiary a = new UnsavedFileNoteDiary();
            a.UnsavedFileNoteId = this.Id;
            a.UserName = name;
            a.Update();
        }

        public ArrayList AlertNames()
        {
            ArrayList a = new ArrayList();
            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Unsaved_File_Note_Alerts", this.Id).Rows)
                a.Add((string)row["User_Name"]);

            return a;
        }

        public ArrayList DiaryNames()
        {
            ArrayList a = new ArrayList();
            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Unsaved_File_Note_Diaries", this.Id).Rows)
                a.Add((string)row["User_Name"]);

            return a;
        }
        #endregion        
    }
}

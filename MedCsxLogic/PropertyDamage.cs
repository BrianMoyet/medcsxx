﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class PropertyDamage:ReserveTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public PropertyDamage(int id) : base(id) { }
        public PropertyDamage(Hashtable row) : base(row) { }
        public PropertyDamage(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Property_Damage"; }
        }

        public string DamageDescription
        {
            get { return (string)row["Damage_Description"]; }
            set { this.setRowValue("Damage_Description", value); }
        }

        public DateTime DateTitleReceived
        {
            get { return (DateTime)row["Date_Title_Received"]; }
            set { this.setRowValue("Date_Title_Received", value); }
        }

        public int DriverPersonId
        {
            get { return (int)row["Driver_Person_Id"]; }
            set { this.setRowValue("Driver_Person_Id", value); }
        }

        public double EstimatedDamageAmount
        {
            get { return (double)((decimal)row["Estimated_Damage_Amount"]); }
            set { this.setRowValue("Estimated_Damage_Amount", value); }
        }

        public double GrossStorage
        {
            get { return (double)((decimal)row["Gross_Storage"]); }
            set { this.setRowValue("Gross_Storage", value); }
        }

        public string OtherInsuranceAddress
        {
            get { return (string)row["Other_Insurance_Address"]; }
            set { this.setRowValue("Other_Insurance_Address", value); }
        }

        public string OtherInsuranceAdjuster
        {
            get { return (string)row["Other_Insurance_Adjuster"]; }
            set { this.setRowValue("Other_Insurance_Adjuster", value); }
        }

        public string OtherInsuranceCity
        {
            get { return (string)row["Other_Insurance_City"]; }
            set { this.setRowValue("Other_Insurance_City", value); }
        }

        public string OtherInsuranceClaimNo
        {
            get { return (string)row["Other_Insurance_Claim_No"]; }
            set { this.setRowValue("Other_Insurance_Claim_No", value); }
        }

        public string OtherInsuranceCompany
        {
            get { return (string)row["Other_Insurance_Company"]; }
            set { this.setRowValue("Other_Insurance_Company", value); }
        }

        public string OtherInsuranceFax
        {
            get { return (string)row["Other_Insurance_Fax"]; }
            set { this.setRowValue("Other_Insurance_Fax", value); }
        }

        public string OtherInsurancePhone
        {
            get { return (string)row["Other_Insurance_Phone"]; }
            set { this.setRowValue("Other_Insurance_Phone", value); }
        }

        public string OtherInsurancePolicyNo
        {
            get { return (string)row["Other_Insurance_Policy_No"]; }
            set { this.setRowValue("Other_Insurance_Policy_No", value); }
        }

        public int OtherInsuranceStateId
        {
            get { return (int)row["Other_Insurance_State_Id"]; }
            set { this.setRowValue("Other_Insurance_State_Id", value); }
        }

        public string OtherInsuranceZipcode
        {
            get { return (string)row["Other_Insurance_Zipcode"]; }
            set { this.setRowValue("Other_Insurance_Zipcode", value); }
        }

        public int OwnerPersonId
        {
            get { return (int)row["Owner_Person_Id"]; }
            set { this.setRowValue("Owner_Person_Id", value); }
        }

        public int PropertyDamageId
        {
            get { return (int)row["Property_Damage_Id"]; }
            set { this.setRowValue("Property_Damage_Id", value); }
        }

        public string PropertyDescription
        {
            get { return (string)row["Property_Description"]; }
            set { this.setRowValue("Property_Description", value); }
        }

        public int PropertyTypeId
        {
            get { return (int)row["Property_Type_Id"]; }
            set { this.setRowValue("Property_Type_Id", value); }
        }

        public int RentalDays
        {
            get { return (int)row["Rental_Days"]; }
            set { this.setRowValue("Rental_Days", value); }
        }

        public int StorageDays
        {
            get { return (int)row["Storage_Days"]; }
            set { this.setRowValue("Storage_Days", value); }
        }

        public double Towing
        {
            get { return (double)((decimal)row["Towing"]); }
            set { this.setRowValue("Towing", value); }
        }

        public int VehicleId
        {
            get { return (int)row["Vehicle_Id"]; }
            set { this.setRowValue("Vehicle_Id", value); }
        }

        public string WhereSeen
        {
            get { return (string)row["Where_Seen"]; }
            set { this.setRowValue("Where_Seen", value); }
        }
        #endregion

        #region Misc Methods
        public Person DriverPerson()
        {
            return new Person(this.DriverPersonId);
        }

        public Person OwnerPerson()
        {
            return new Person(this.OwnerPersonId);
        }

        public Vehicle Vehicle()
        {
            return new Vehicle(this.VehicleId);
        }
        public PropertyType PropertyType()
        {
            return new PropertyType(this.PropertyTypeId);
        }

        public int Update(int claimId, int reserveTypeId = 0)
        {
            //update property damage ang generate column change event
            Claim claim = new Claim(claimId);

            //create claimant if necessary - claimant may have changed since last save
            int claimantId = claim.CreateClaimantIfNecessary(this.OwnerPersonId);

            //change claimant on reserve row if necessary
            if (!this.isAdd)
            {
                if ((int)this.oldRow["Owner_Person_Id"] != OwnerPersonId)
                {
                    this.Reserve.ClaimantId = claimantId;
                    this.Reserve.Update();
                }
            }

            bool createReserve = this.isAdd;

            

            //update property damage row
            if (this.PropertyTypeId != (int)modGeneratedEnums.PropertyType.Auto_Property)
                this.VehicleId = 0;

            this.Update();

            //create reserve line
            if (createReserve)
            {
                Claimant claimant = new Claimant(claimantId);
                Hashtable parms = new Hashtable();

                parms.Add("Property_Damage_Id", this.Id);

                if (reserveTypeId == 0)
                    this.ReserveId=claimant.CreatePropertyDamageReserve(parms).Id;
                    //create property damage reserve
                this.Update();
            }

            //update unknown persons/vehicles
            Person p = new Person(this.OwnerPersonId);
            p.UpdateUnknownPersons();

            if (this.PropertyTypeId == (int)modGeneratedEnums.PropertyType.Auto_Property)
            { 
                Person p2 = new Person(this.DriverPersonId);
                p2.UpdateUnknownPersons();

          
                this.Vehicle().UpdateUnknownVehicles();
            }
            return this.Id;
        }

        public string OwnerName
        {
            get
            {
                try
                {
                    return this.OwnerPerson().Name;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string DriverName
        {
            get
            {
                try
                {
                    return this.DriverPerson().Name;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string VehicleName
        {
            get
            {
                try
                {
                    return this.Vehicle().Name;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public State OtherInsuranceState()
        {
            return this.OtherInsuranceStateId == 0 ? null : new State(this.OtherInsuranceStateId);
        }

        public string OtherInsuranceStateAbbreviation
        {
            get
            {
                try
                {
                    return this.OtherInsuranceState().Abbreviation;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string Description
        {
            get
            {
                StringBuilder s = new StringBuilder();
                s.Append("Property Type: ");
                s.Append(this.PropertyType().Description);
                s.Append(Environment.NewLine);

                if (this.PropertyDescription != "")
                {
                    s.Append("Property Description: ");
                    s.Append(this.PropertyDescription);
                    s.Append(Environment.NewLine);
                }

                if (this.VehicleName != "")
                {
                    s.Append("Vehicle: ");
                    s.Append(this.VehicleName);
                    s.Append(Environment.NewLine);
                }

                if (this.OwnerName != "")
                {
                    s.Append("Owner: ");
                    s.Append(this.OwnerName);
                    s.Append(Environment.NewLine);
                }

                if (this.DriverName != "")
                {
                    s.Append("Driver: ");
                    s.Append(this.DriverName);
                    s.Append(Environment.NewLine);
                }

                if (this.DamageDescription != "")
                {
                    s.Append("Damage Description: ");
                    s.Append(this.DamageDescription);
                    s.Append(Environment.NewLine);
                }

                if (this.EstimatedDamageAmount != 0)
                {
                    s.Append("Estimated Damage Amount: ");
                    s.Append(this.EstimatedDamageAmount);
                    s.Append(Environment.NewLine);
                }

                if (this.WhereSeen != "")
                {
                    s.Append("Where Seen: ");
                    s.Append(this.WhereSeen);
                    s.Append(Environment.NewLine);
                }
                s.Append(this.OtherInsuranceDescription);

                if (this.DateTitleReceived.Year > 1901)
                {
                    s.Append("Date Title Received: ");
                    s.Append(this.DateTitleReceived.ToShortDateString());
                    s.Append(Environment.NewLine);
                }

                if (this.Towing != 0)
                {
                    s.Append("Towing: ");
                    s.Append(this.Towing.ToString("c"));
                    s.Append(Environment.NewLine);
                }

                if (this.RentalDays != 0)
                {
                    s.Append("Rental Days: ");
                    s.Append(this.RentalDays.ToString());
                    s.Append(Environment.NewLine);
                }

                if (this.StorageDays != 0)
                {
                    s.Append("Storage Days: ");
                    s.Append(this.StorageDays.ToString());
                    s.Append(Environment.NewLine);
                }

                if (this.GrossStorage != 0)
                {
                    s.Append("Gross Storage: ");
                    s.Append(this.GrossStorage.ToString());
                    s.Append(Environment.NewLine);
                }

                return s.ToString();
            }
        }

        public string OtherInsuranceDescription
        {
            get
            {
                StringBuilder s = new StringBuilder();
                if (this.OtherInsuranceCompany != "")
                {
                    s.Append("     ");
                    s.Append(this.OtherInsuranceCompany);
                    s.Append(Environment.NewLine);
                }

                if (this.OtherInsuranceAddress != "")
                {
                    s.Append("     ");
                    s.Append(this.OtherInsuranceAddress);
                    s.Append(Environment.NewLine);
                }

                if ((this.OtherInsuranceCity != "") || (this.OtherInsuranceStateAbbreviation != "") || (this.OtherInsuranceZipcode != ""))
                {
                    s.Append("     ");

                    if (this.OtherInsuranceCity != "")
                    {
                        s.Append(this.OtherInsuranceCity);
                        s.Append(", ");
                    }

                    if (this.OtherInsuranceStateAbbreviation != "")
                    {
                        s.Append(this.OtherInsuranceStateAbbreviation);
                        s.Append(" ");
                    }

                    s.Append(this.OtherInsuranceZipcode);
                    s.Append(Environment.NewLine);
                }

                if (this.OtherInsurancePhone != "")
                {
                    s.Append("     Phone: ");
                    s.Append(this.OtherInsurancePhone);
                    s.Append(Environment.NewLine);
                }

                if (this.OtherInsuranceFax != "")
                {
                    s.Append("     Fax: ");
                    s.Append(this.OtherInsuranceFax);
                    s.Append(Environment.NewLine);
                }

                if (this.OtherInsuranceAdjuster != "")
                {
                    s.Append("     Adjuster: ");
                    s.Append(this.OtherInsuranceAdjuster);
                    s.Append(Environment.NewLine);
                }

                if (this.OtherInsurancePolicyNo != "")
                {
                    s.Append("     Policy No: ");
                    s.Append(this.OtherInsurancePolicyNo);
                    s.Append(Environment.NewLine);
                }

                if (this.OtherInsuranceClaimNo != "")
                {
                    s.Append("     Claim No: ");
                    s.Append(this.OtherInsuranceClaimNo);
                    s.Append(Environment.NewLine);
                }

                if (s.Length > 0)
                    return "Other Insurance" + Environment.NewLine + s.ToString();
                else
                    return "";
            }
        }

        public static bool ReserveExistsOfPropertyType(int reserveId, int propertyTypeId)
        {
            //does property damage reserve exist with given property type?
            return db.GetIntFromStoredProcedure("usp_Property_Damage_Reserve_Exists_Of_Property_Type", reserveId, propertyTypeId) != 0;
        }
        #endregion

        #region Nonimplemented
        public override int ClaimantId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}

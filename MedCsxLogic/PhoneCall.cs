﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
   public  class PhoneCall:ClaimTable
   {

       #region Class Variables
       private static clsDatabase db = new clsDatabase(); 

       private FileNoteType m_FileNoteType = null;
       private Person m_Person = null;
       private User m_User = null;
       #endregion

       #region Constructors
       public PhoneCall() : base() { }
       public PhoneCall(int id) : base(id) { }
       public PhoneCall(Hashtable row) : base(row) { }
       #endregion

       #region Column Names
       public override string TableName
       {
           get { return "Phone_Call"; }
       }

       public int PhoneCallId
       {
           get { return (int)row["Phone_Call_Id"]; }
           set { this.setRowValue("Phone_Call_Id", value); }
       }

       public int UserId
       {
           get { return (int)row["User_Id"]; }
           set { this.setRowValue("User_Id", value); }
       }

       public string PhoneNumber
       {
           get { return (string)row["Phone_Number"]; }
           set { this.setRowValue("Phone_Number", value); }
       }

       public int PersonId
       {
           get { return (int)row["Person_Id"]; }
           set { this.setRowValue("Person_Id", value); }
       }

       public int FileNoteTypeId
       {
           get { return (int)row["File_Note_Type_Id"]; }
           set { this.setRowValue("File_Note_Type_Id", value); }
       }

       public string CallNotes
       {
           get { return (string)row["Call_Notes"]; }
           set { this.setRowValue("Call_Notes", value); }
       }

       public DateTime CallStartTime
       {
           get { return (DateTime)row["Call_Start_Time"]; }
           set { this.setRowValue("Call_Start_Time", value); }
       }

       public int CallLengthTicks
       {
           get { return (int)row["Call_Length_Ticks"]; }
           set { this.setRowValue("Call_Length_Ticks", value); }
       }

       public bool IsVoiceMail
       {
           get { return (bool)row["Is_Voicemail"]; }
           set
           {
               this.setRowValue("Is_Voicemail", value);
               this.m_FileNoteType = null;
           }
       }

       public bool IsInbound
       {
           get { return (bool)row["Is_Inbound"]; }
           set { this.setRowValue("Is_Inbound", value); }
       }

       public bool IsOutbound
       {
           get { return (bool)row["Is_Outbound"]; }
           set { this.setRowValue("Is_Outbound", value); }
       }

       public string FileName
       {
           get { return (string)row["File_Name"]; }
           set { this.setRowValue("File_Name", value); }
       }

       public string FileLocation
       {
           get { return (string)row["File_Location"]; }
           set { this.setRowValue("File_Location", value); }
       }
       #endregion
       
       #region Properties
       public FileNoteType FileNoteType
       {
           get { return m_FileNoteType == null ? m_FileNoteType = new FileNoteType(FileNoteTypeId) : m_FileNoteType; }
       }

       public override int ClaimId
       {
           get
           {
               return base.ClaimId;
           }
           set
           {
               base.ClaimId = value;
               if (this.Person.ClaimId != value)
                   this.Person.ClaimId = value;
           }
       }

       public Person Person
       {
           get { return m_Person == null ? m_Person = new Person(PersonId) : m_Person; }
           set
           {
               m_Person = value;
               this.PersonId = value.Id;

               //set ClaimId on Person if not set
               if (m_Person.ClaimId != this.ClaimId)
               {
                   if (this.ClaimId == 0)
                       this.ClaimId = m_Person.ClaimId;
                   else
                   {
                       m_Person.ClaimId = this.ClaimId;
                       m_Person.Update();
                   }
               }
           }
       }

       public string PersonName
       {
           get { return this.Person.Name; }
           set { this.Person = Person.Named(value, this.ClaimId); }
       }

       public User User
       {
           get { return m_User == null ? m_User = new User(this.UserId) : m_User; }
           set
           {
               m_User = value;
               this.UserId = m_User.Id;
           }
       }

       public string UserName
       {
           get { return this.User.Name; }
           set { this.User = User.Named(value); }
       }

       public string CallLength
       {
           get
           {
               TimeSpan ts = new TimeSpan(this.CallLengthTicks);
               string s = ts.ToString();
               if (s.Length >= 10)
                   return s.Substring(0, 10);
               else
                   return s;
           }
       }

       public string Description
       {
           get
           {
               string s;
               s = "A phone call was made between " + this.UserName + " and " +
                   this.PersonName + " on " + this.CallStartTime.ToString("MM/dd/yyyy hh:mm:ss tt") + ".";
               s += "  Call length was " + this.CallLength + ".";

               if (CallNotes != null)
               {
                   if (this.CallNotes.Length > 0)
                       s += Environment.NewLine + "Notes: " + this.CallNotes;
               }
               return s;
           }
       }

       public string ShortDescription
       {
           get
           {
               string s = "Phone call from " + this.User.Name + " to " + this.Person.Name + " on " + this.CallStartTime.ToShortDateString();
               return s;
           }
       }
       #endregion

       #region Methods
       public void  SetFilNoteTypeIdFromPersonType(string personTypeDescription)
       {
           if(personTypeDescription=="")
           {
               this.FileNoteTypeId = (int)modGeneratedEnums.FileNoteType.Phone_Call_Other;
               return;
           }

           this.FileNoteTypeId = new PersonType(personTypeDescription).PhoneCallFileNoteTypeId;
       }

       public static void PopulatePhoneCall()
       {
           Thread UpdatePhoneRecord = new Thread(() =>
           {
               PhoneRecord pr = new PhoneRecord();
               string sql = "SELECT [File_Location], [File_Name] FROM Phone_Record";
               pr.CallRecord = db.ExecuteSelectReturnHashList(sql);
               pr.PopulatePhoneRecord();

               System.Windows.Threading.Dispatcher.Run();
           });

           UpdatePhoneRecord.SetApartmentState(ApartmentState.STA);
           UpdatePhoneRecord.IsBackground = true;
           UpdatePhoneRecord.Start();
       }
       #endregion

    }
}

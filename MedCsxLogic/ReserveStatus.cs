﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ReserveStatus : TypeTable
    {

        #region Constructors
        public ReserveStatus() : base() { }
        public ReserveStatus(int id) : base(id) { }
        public ReserveStatus(string description) : base(description) { }
        public ReserveStatus(Hashtable row) : base(row) { }
        #endregion

        #region Misc Methods
        public override string TableName
        {
            get
            {
                return "Reserve_Status";
            }
        }

        public bool isOpen
        {
            get { return this.Id == (int)modGeneratedEnums.ReserveStatus.Open_Reserve; }
        }
        #endregion
    }
}

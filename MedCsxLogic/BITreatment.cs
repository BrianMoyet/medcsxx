﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class BITreatment:MedCsXTable
    {

        #region Constructors
        public BITreatment() : base() { }
        public BITreatment(int id) : base(id) { }
        public BITreatment(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "BI_Treatment"; }
        }

        public int BIDiagnosisTypeId
        {
            get { return (int)row["BI_Diagnosis_Type_Id"]; }
            set { this.setRowValue("BI_Diagnosis_Type_Id", value); }
        }

        public int BIEvaluationId
        {
            get { return (int)row["BI_Evaluation_Id"]; }
            set { this.setRowValue("BI_Evaluation_Id", value); }
        }

        public int BITreatmentId
        {
            get { return (int)row["BI_Treatment_Id"]; }
            set { this.setRowValue("BI_Treatment_Id", value); }
        }
       
        public bool Chronic
        {
            get { return (bool)row["Chronic"]; }
            set { this.setRowValue("Chronic", value); }
        }

        public decimal CostBestCase
        {
            get { return (decimal)row["Cost_Best_Case"]; }
            set { this.setRowValue("Cost_Best_Case", value); }
        }

        public decimal CostWorstCase
        {
            get { return (decimal)row["Cost_Worst_Case"]; }
            set { this.setRowValue("Cost_Worst_Case", value); }
        }

        public int NbrTreatments
        {
            get { return (int)row["Nbr_Treatments"]; }
            set { this.setRowValue("Nbr_Treatments", value); }
        }

        public decimal ReportFees
        {
            get { return (decimal)row["Report_Fees"]; }
            set { this.setRowValue("Report_Fees", value); }
        }

        public int TreatingDoctorId
        {
            get { return (int)row["Treating_Doctor_Id"]; }
            set { this.setRowValue("Treating_Doctor_Id", value); }
        }

        public DateTime TreatmentEndDate
        {
            get { return (DateTime)row["Treatment_End_Date"]; }
            set { this.setRowValue("Treatment_End_Date", value); }
        }

        public DateTime TreatmentStartDate
        {
            get { return (DateTime)row["Treatment_Start_Date"]; }
            set { this.setRowValue("Treatment_Start_Date", value); }
        }
        #endregion

        #region Misc Methods
        public BIDiagnosisType BIDiagnosisType()
        {
            return new BIDiagnosisType(this.BIDiagnosisTypeId);
        }

        public BIEvaluation BIEvaluation()
        {
            return new BIEvaluation(this.BIEvaluationId);
        }

        public Vendor TreatingDoctor()
        {
            return new Vendor(this.TreatingDoctorId);
        }
        #endregion
    }
}

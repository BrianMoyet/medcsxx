﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class AlertPreference:TypeTable
    {

        #region Constructors
        public AlertPreference() : base() { }
        public AlertPreference(int id) : base(id) { }
        public AlertPreference(string description) : base(description) { }
        public AlertPreference(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int AlertPreferenceId
        {
            get { return (int)row["Alert_Preference_Id"]; }
            set { this.setRowValue("Alert_Preference_Id", value); }
        }

        public string BackgroundBitmap
        {
            get { return (string)row["Background_Bitmap"]; }
            set { this.setRowValue("Background_Bitmap", value); }
        }

        public string CloseBitmap
        {
            get { return (string)row["Close_Bitmap"]; }
            set { this.setRowValue("Close_Bitmap", value); }
        }

        public int ClosePositionX
        {
            get { return (int)row["Close_Position_X"]; }
            set { this.setRowValue("Close_Position_X", value); }
        }

        public int ClosePositionY
        {
            get { return (int)row["Close_Position_Y"]; }
            set { this.setRowValue("Close_Position_Y", value); }
        }

        public bool isNonIntrusive
        {
            get { return (bool)row["Is_Non_Intrusive"]; }
            set { this.setRowValue("Is_Non_Intrusive", value); }
        }

        public string SoundFile
        {
            get { return (string)row["Sound_File"]; }
            set { this.setRowValue("Sound_File", value); }
        }

        public int TextHeight
        {
            get { return (int)row["Text_Height"]; }
            set { this.setRowValue("Text_Height", value); }
        }

        public int TextPositionX
        {
            get { return (int)row["Text_Position_X"]; }
            set { this.setRowValue("Text_Position_X", value); }
        }

        public int TextPositionY
        {
            get { return (int)row["Text_Position_Y"]; }
            set { this.setRowValue("Text_Position_Y", value); }
        }

        public int TextWidth
        {
            get { return (int)row["Text_Width"]; }
            set { this.setRowValue("Text_Width", value); }
        }

        public int TitleHeight
        {
            get { return (int)row["Title_Height"]; }
            set { this.setRowValue("Title_Height", value); }
        }

        public int TitlePositionX
        {
            get { return (int)row["Title_Position_X"]; }
            set { this.setRowValue("Title_Position_X", value); }
        }

        public int TitlePositionY
        {
            get { return (int)row["Title_Position_Y"]; }
            set { this.setRowValue("Title_Position_Y", value); }
        }

        public int TitleWidth
        {
            get { return (int)row["Title_Width"]; }
            set { this.setRowValue("Title_Width", value); }
        }
        #endregion

        public override string TableName
        {
            get
            {
                return "Alert_Preference";
            }
        }

        public bool isNone()
        {
            return this.Id == (int)modGeneratedEnums.AlertPreference.None;
        }
    }
}

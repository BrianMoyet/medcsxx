﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class PeriodType:TypeTable
    {

        #region Constructors
        public PeriodType() : base() { }
        public PeriodType(int id) : base(id) { }
        public PeriodType(string description) : base(description) { }
        public PeriodType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "Period_Type";
            }
        }

        public string PeriodName
        {
            get { return (string)row["Period_Name"]; }
            set { this.setRowValue("Period_Name", value); }
        }

        public int PeriodTypeId
        {
            get { return (int)row["Period_Type_Id"]; }
            set { this.setRowValue("Period_Type_Id", value); }
        }
        #endregion

        #region Misc Methods
        public string getSuffix(int nbrPeriods)
        {
            //returs suffix for period type and number periods, e.g., "/mo, 12 mos"
            string s = "";
            switch (this.Id)
            {
                case (int)modGeneratedEnums.PeriodType.Flat:
                case (int)modGeneratedEnums.PeriodType.Undefined:
                    //no modifications necessary
                    break;
                case (int)modGeneratedEnums.PeriodType.Per_Day:
                    s = "/day, " + nbrPeriods + " days";
                    break;
                case (int)modGeneratedEnums.PeriodType.Per_Month:
                    s = "/mo, " + nbrPeriods + " mos";
                    break;
                case (int)modGeneratedEnums.PeriodType.Per_Year:
                    s = "/yr, " + nbrPeriods + " yrs";
                    break;
                default:
                    break;
            }
            return s;
        }
        #endregion
        #region
        #endregion
        #region
        #endregion
        #region
        #endregion

    }
}

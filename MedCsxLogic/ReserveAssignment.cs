﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class ReserveAssignment:ReserveTable
    {

        private static clsDatabase db = new clsDatabase();
        #region Constructors
        public ReserveAssignment() : base() { }
        public ReserveAssignment(int id) : base(id) { }
        public ReserveAssignment(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Reserve_Assignment"; }
        }

        public int AssignedToUserGroupId
        {
            get { return (int)row["Assigned_To_User_Group_Id"]; }
            set { this.setRowValue("Assigned_To_User_Group_Id", value); }
        }

        public int AssignedToUserId
        {
            get { return (int)row["Assigned_To_User_Id"]; }
            set { this.setRowValue("Assigned_To_User_Id", value); }
        }

        public DateTime AssignmentDate
        {
            get { return (DateTime)row["Assignment_Date"]; }
            set { this.setRowValue("Assignment_Date", value); }
        }

        public DateTime DateEnteredFirstFileNote
        {
            get { return (DateTime)row["Date_Entered_First_File_Note"]; }
            set { this.setRowValue("Date_Entered_First_File_Note", value); }
        }

        public int ReserveAssignmentId
        {
            get { return (int)row["Reserve_Assignment_Id"]; }
            set { this.setRowValue("Reserve_Assignment_Id", value); }
        }
        #endregion

        #region Misc Methods
        public static void InsertReserveAssignment(int reserveId, int assignedToUserId, int assignedToUserGroupId, DateTime assignmentDate)
        {
            //inserts new reserve assignment
            db.ExecuteStoredProcedure("usp_Insert_Reserve_Assignment", reserveId, assignedToUserId, assignedToUserGroupId, assignmentDate);
        }
        #endregion

        #region Nonimplemented
        public override int ClaimantId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;
using System.Web;

namespace MedCsxLogic
{

    public class MedCsXSystem : MedCsxObject
    {
        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Class Variables
        private  int m_userId;
        private  User m_user;
        private static SystemSettings m_settings = null;

        private const int SYSTEM_SETTINGS_ID = 1;
        #endregion

        #region Methods
        public  User CurrentUser
        {
            get
            {
                if (m_user == null)
                {
                    UserId = 78;  //Alexandra Soto
                }
                //m_user = new User(6);
                m_user = new User(Convert.ToInt32(HttpContext.Current.Session["userID"]));
                return m_user;
            }
            set {
                //m_user = new User(6);
                //m_user = value;
                m_user = new User(Convert.ToInt32(HttpContext.Current.Session["userID"]));
            }
        }

        public int UserId
        {
            get { return m_userId; }
            set
            {
                
                if (m_userId != value)  //change user id iff it is assigned a different value
                {
                   
                    m_userId = value;
                    CurrentUser = new User(m_userId);
                }
            }
        }

        public static SystemSettings Settings
        {
            get
            {
                if (m_settings == null)
                    m_settings = new SystemSettings(SYSTEM_SETTINGS_ID);
                return m_settings;
            }
        }

        public static void RefreshDataDictionary()
        {
            MedCsXSystem mySystem = new MedCsXSystem();
            db.ExecuteStoredProcedure("usp_Get_My_Claims", mySystem.UserId);
        }

        public static bool CheckLogonId(string logonId, ref string msg)
        {
            //verfies logon is completed and provides message
            if (logonId.Trim().Length == 0)
            {
                msg = "Logon Id is Required.";
                return false;
            }
            MedCsXSystem mySystem = new MedCsXSystem();
            mySystem.UserId = db.GetIntFromStoredProcedure("usp_Get_User_Id_From_Logon_Id", logonId);

             HttpContext.Current.Session["userID"] = mySystem.UserId;
            //is Logon ID found?
            if (mySystem.UserId == 0)
            {
                msg = "Invalid Logon Id";
                return false;
            }

            //is session dead - not polled active for 5 minutes
            if (ml.DBNow().Subtract(mySystem.CurrentUser.LastPolledActiveDate).Minutes > 5)
                return true;
            //assume dead session

            return true;
        }

        public static int UserIDFromUserName(string logonId)
        {

            MedCsXSystem mySystem = new MedCsXSystem();
            mySystem.UserId = db.GetIntFromStoredProcedure("usp_Get_User_Id_From_Logon_Id", logonId);

            return mySystem.UserId;
        }


        public static ArrayList Users()
        {
            ArrayList a = new ArrayList();
            DataTable myUsers = db.ExecuteStoredProcedure("usp_Get_All_Users");
            foreach (DataRow row in myUsers.Rows)
                a.Add(new User(myUsers, row));
            return a;
        }

        public static ArrayList UserNames()
        {
            ArrayList a = new ArrayList();
            foreach (User v in Users())
                a.Add(v.Name);
            return a;
        }

        public static ArrayList AllAttorneys()
        {
            ArrayList a = new ArrayList();
            foreach (User v in Users())
            {
                if (v.isAttorney())
                    a.Add(v);
            }
            return a;
        }

        public static ArrayList UserGroups()
        {
            ArrayList a = new ArrayList();
            DataTable myUserGroups = db.ExecuteStoredProcedure("usp_Get_User_Groups");
            foreach (DataRow row in myUserGroups.Rows)
                a.Add(new UserGroup(myUserGroups, row));
            return a;
        }

        public static ArrayList DraftsInQueue()
        {
            ArrayList a = new ArrayList();
            DataTable myDrafts = db.ExecuteStoredProcedure("usp_Get_Drafts_In_Queue");
            foreach (DataRow row in myDrafts.Rows)
                a.Add(new Draft(myDrafts, row));
            return a;
        }

       
        #endregion
    }
}

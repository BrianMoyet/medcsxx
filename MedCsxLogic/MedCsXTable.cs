﻿using System;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using MedCsxDatabase;
using System.Web;

namespace MedCsxLogic
{
    public abstract class MedCsXTable : MedCsxObject
    {

        #region Class Variables
        private Hashtable m_row;
        private Hashtable m_oldRow;
        private bool m_isChanged = false;
        #endregion

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public MedCsXTable()
            : base()
        {
            row = new Hashtable();
        }

        public MedCsXTable(int id)
            : base()
        {
            ReadRow(id);
        }

        public MedCsXTable(Hashtable row)
            : base()
        {
            this.row = row;

        }

        public MedCsXTable(DataTable Table, DataRow Row)
            : base()
        {
            Hashtable thisRow = new Hashtable();
            thisRow = db.DataTableRowToHashtable(Table, Row);
            this.row = thisRow;
        }

        public MedCsXTable(string description)
            : base()
        {

        }
        #endregion

        public static ArrayList GetObjects(Type tableType)
        {
            //Creates array list of MedCsx table objects

            MedCsXTable m = (MedCsXTable)System.Activator.CreateInstance(tableType);
            string tableName = m.TableName;
            ArrayList objects = new ArrayList();

            foreach (Hashtable row in MedCsXTable.GetTableContents(tableName))
            {
                int id = (int)row[tableName + "_Id"];
                if (id != 0)
                {
                    object[] args = new object[0];
                    args[0] = id;
                    m = (MedCsXTable)System.Activator.CreateInstance(tableType, args);
                    objects.Add(m);
                }
            }
            return objects;
        }

        public abstract string TableName { get; }

        private static Hashtable _tableContents = new Hashtable();

        public static List<Hashtable> GetTableContents(string tableName)
        {
            if (!_tableContents.ContainsKey(tableName))
            {
                string sql = "select * from [" + tableName + "]";
                _tableContents.Add(tableName, db.ExecuteSelectReturnHashList(sql));
            }
            return (List<Hashtable>)_tableContents[tableName];
        }

        public static void ClearTableContents(string tableName)
        {
            if (_tableContents.ContainsKey(tableName))
                _tableContents.Remove(tableName);
        }

        public bool isChanged
        {
            get { return (m_isChanged); }
            set { m_isChanged = value; }
        }

        public bool isValid
        {
            get
            {
                if (this.Id == 0)
                    return false;
                return true;
            }
        }

        public void ReadRow(int id)
        {
            if ((this.ReadRowStoredProcedureName() != "") && (this.ReadRowStoredProcedureName()!=null))
            {
                DataTable Table = db.ExecuteStoredProcedure(this.ReadRowStoredProcedureName(), id);
                if (Table.Rows.Count == 0)
                {
                    row = new Hashtable();
                }
                else
                {
                    row = db.DataTableRowToHashtable(Table, Table.Rows[0]);
                }
            }
            else
                row = db.GetRow(TableName, id);
        }

        public Hashtable row
        {
            get { return (m_row); }
            set { m_row = value; }
        }

        public Hashtable oldRow
        {
            get { return (m_oldRow); }
            set { m_oldRow = value; }
        }

        public string PrimaryKeyName
        {
            get { return (TableName + "_Id"); }
        }

        public bool isAdd
        {
            get { return (row.Contains(PrimaryKeyName) ? false : true); }
        }

        public bool isEdit
        {
            get { return (!isAdd); }
        }

        public int Id
        {
            /*Returns Id (primary key of row).  If row is bing added 
             * and doesn't have a primary key value, return zero.
            */

            get { return (isAdd ? 0 : (int)row[TableName + "_Id"]); }
            set { ReadRow(value); }
        }

        public DateTime CreatedDate
        {
            get { return ((DateTime)row["Created_Date"]); }
        }

        public DateTime ModifiedDate
        {
            get { return ((DateTime)row["Modified_Date"]); }
        }

        protected void setRowValue(string key, object value)
        {
            if (oldRow == null)
                oldRow = (Hashtable)row.Clone();

            if (row.Contains(key))
            {
                if (row[key] != value)
                {
                    row[key] = value;
                    isChanged = true;
                }
            }
            else
            {
                row.Add(key, value);
                isChanged = true;
            }
        }

        public virtual void DeleteRow()
        {
            db.DeleteRow(TableName, Id);
        }

        public void DeleteAllRows()
        {
            db.DeleteAllRows(TableName);
        }

        public void DeleteRows(ArrayList ids)
        {
            db.DeleteAllRows(TableName);
        }

        public ArrayList ChildObjects(Type medCsX_Table, string whereClause = "")
        {
            /* Finds all rows in medCsX_Table which matches the primary key in this table.
             * claim.ChildObjects(gettype(Claimant)) would return claimants which match on claim id
            */

            MedCsXTable t = (MedCsXTable)Activator.CreateInstance(medCsX_Table);

            string sql = "SELECT * FROM [" + t.TableName + "] WHERE " +
                            this.PrimaryKeyName + " = " + this.Id;

            if (whereClause != "")
                sql += " and " + whereClause;

            List<Hashtable> rows = db.ExecuteSelectReturnHashList(sql);
            ArrayList a = new ArrayList();

            foreach (Hashtable row in rows)
            {
                t = (MedCsXTable)Activator.CreateInstance(medCsX_Table, new object[] { row });
                a.Add(t);
            }
            return a;
        }

        public ArrayList DupicateChildObjects(Type medCsX_Table, string whereClause = "")
        {
            /* Finds all rows in medCsX_Table which matches the primary key in this table.
             * claim.ChildObjects(gettype(Claimant)) would return claimants which match on claim id
            */

            MedCsXTable t = (MedCsXTable)Activator.CreateInstance(medCsX_Table);

            string sql = "SELECT * FROM [" + t.TableName + "] WHERE ";

            if (whereClause != "")
                sql +=  whereClause;

            List<Hashtable> rows = db.ExecuteSelectReturnHashList(sql);
            ArrayList a = new ArrayList();

            foreach (Hashtable row in rows)
            {
                t = (MedCsXTable)Activator.CreateInstance(medCsX_Table, new object[] { row });
                a.Add(t);
            }
            return a;
        }

        public void DeleteChildObjects(Type medCsX_Table)
        {
            /* Finds all rows in medCsX_Table which matches the primary key in this table - and deletes them.
             * claim.DeleteChildObjects(gettype(Claimant)) would delete claimants which match on claim id
            */

            foreach (MedCsXTable row in this.ChildObjects(medCsX_Table))
            {
                row.DeleteRow();
            }
        }

        public string XML
        {
            get
            {
                string x = "";

                x = "<" + this.TableName + ">";
                foreach (string field in row.Keys)
                {
                    x += "<" + field + ">";
                    switch (row[field].ToString().ToUpper())
                    {
                        case "FALSE":
                            x += "0";
                            break;
                        case "TRUE":
                            x += "1";
                            break;
                        default:
                            x += FixXmlString(row[field].ToString());
                            break;
                    }
                    x += "</" + field + ">";
                }
                x += "</" + this.TableName + ">";

                return x;
            }
        }

        public string FixXmlString(string s)
        {
            //Replaces < > and & with appropriate HTML symbols
            if (s == "")
                return "";

            string s2 = s.Replace("<", "&lt;");
            if (s2 == null)
                return "";

            s2 = s2.Replace(">", "&gt;");
            if (s2 == null)
                return "";

            s2 = s2.Replace("&", "&amp;");
            if (s2 == null)
                return "";

            return s2;
        }

        public virtual int Update(Hashtable parms = null, bool fireEvents = true)
        {
            int rowID;

            //Performs Insert/Update
            if (isAdd)
            {
                rowID = db.InsertRow(row, TableName, Convert.ToInt32(HttpContext.Current.Session["userID"]));
                ReadRow(rowID);

                //Raise insert event if registered for the table
                ml.GenerateTableInsertEvent(TableName, rowID);
            }
            else
            {
                if (oldRow == null)
                    return Id;
                
                rowID = Id;
                db.UpdateRow(TableName, row, oldRow, parms, Convert.ToInt32(HttpContext.Current.Session["userID"]));

                //Fire Table and Column update events
                if (fireEvents)
                {
                    if (oldRow != null)
                    {
                        if (parms == null)
                        {
                            parms = new Hashtable();

                            //Fire Column change events
                            ml.FireColumnChangeEvents(TableName, oldRow, row, parms);

                            //Generate Table update event
                            ml.FireTableUpdateEvent(TableName, oldRow, row, parms);
                        }
                    }
                }
                ReadRow(rowID);
                oldRow = null;
                this.isChanged = false;
            }
            return rowID;
        }

        //public virtual int UpdateAppraisal(Hashtable parms = null, bool fireEvents = true)
        //{
        //    int rowID;

        //    //Performs Insert/Update
        //    if (isAdd)
        //    {
        //        rowID = db.InsertRow(row, TableName, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        //        ReadRow(rowID);

        //        //Raise insert event if registered for the table
        //        ml.GenerateTableInsertEvent(TableName, rowID);
        //    }
        //    else
        //    {
        //        if (oldRow == null)
        //            return Id;

        //        rowID = Id;
        //        db.UpdateRow(TableName, row, oldRow, parms, Convert.ToInt32(HttpContext.Current.Session["userID"]));
        //        ml.GenerateTableInsertEventAppraisalTask("Appraisal_Task", rowID);
        //        //Fire Table and Column update events
        //        //if (fireEvents)
        //        //{
        //        //    if (oldRow != null)
        //        //    {
        //        //        if (parms == null)
        //        //        {
        //        //            parms = new Hashtable();

        //        //            //Fire Column change events
        //        //            ml.FireColumnChangeEvents(TableName, oldRow, row, parms);

        //        //            //Generate Table update event
        //        //            ml.FireTableUpdateEvent(TableName, oldRow, row, parms);
        //        //        }
        //        //    }
        //        // }
        //        ReadRow(rowID);
        //        oldRow = null;
        //        this.isChanged = false;
        //    }
        //    return rowID;
        //}

        public Hashtable DuplicateRow()
        {
            return (Hashtable)row.Clone();
        }

        public bool Exists(int id)
        {
            //Returns whether table row with id exists

            Hashtable h = db.GetRow(this.TableName, id);
            return h.Count == 0 ? false : true;
        }

        public bool Exists()
        {
            //Returns whether current table row exists - it was instantiated with an id that exists

            return this.Id == 0 ? false : true;
        }

        public virtual void AssignTo(User user)
        {
            return;
        }

        public virtual void AssignTo(UserGroup userGroup)
        {
            return;
        }

        public virtual ArrayList Reserves()
        {
            return new ArrayList();
        }

        public virtual ArrayList Diaries()
        {
            return new ArrayList();
        }

        public virtual ArrayList Alerts()
        {
            return new ArrayList();
        }

        public virtual ArrayList UserTasks()
        {
            return new ArrayList();
        }

        public virtual string ReadRowStoredProcedureName()
        {
            return "";
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedCsxLogic
{
    public class ChoicePointReportType : TypeTable
    {

        #region Constructors
        public ChoicePointReportType() : base() { }
        public ChoicePointReportType(int id) : base(id) { }
        public ChoicePointReportType(string description) : base(description) { }
        public ChoicePointReportType(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get
            {
                return "ChoicePoint_Report_Type";
            }
        }

        public string ChoicePointReportTypeCode
        {
            get { return (string)row["ChoicePoint_Report_Type_Code"]; }
            set { this.setRowValue("ChoicePoint_Report_Type_Code", value); }
        }

        public int ChoicePointReportTypeId
        {
            get { return (int)row["ChoicePoint_Report_Type_Id"]; }
            set { this.setRowValue("ChoicePoint_Report_Type_Id", value); }
        }
        #endregion
    }
}

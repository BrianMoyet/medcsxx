﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class UserGroup:TypeTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public UserGroup() : base() { }
        public UserGroup(int id) : base(id) { }
        public UserGroup(string description)
            : base(description)
        {
            this.Id = ml.getId(this.TableName, description);
        }
        public UserGroup(DataTable Table, DataRow Row) : base(Table, Row) { }
        public UserGroup(Hashtable row) : base(row) { }
        #endregion

        #region Column Names
        public int UserGroupId
        {
            get { return (int)row["User_Group_Id"]; }
            set { this.setRowValue("User_Group_Id", value); }
        }

        public bool CanBeAssignedClaims
        {
            get { return (bool)row["Can_Be_Assigned_Claims"]; }
            set { this.setRowValue("Can_Be_Assigned_Claims", value); }
        }
        #endregion

        #region Methods
        public override string TableName
        {
            get
            {
                return "User_Group";
            }
        }

        public static int IdForDescription(string desc)
        {
            UserGroup a = new UserGroup(desc);
            return a.Id;
        }

        public ArrayList Users()
        {
            //Returns users for usergroup
            ArrayList a = new ArrayList();
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Users_for_User_Group", this.Id);

            foreach (DataRow row in rows.Rows)
                a.Add(new User(rows, row));
            return a;
        }

        public List<int> UserIds()
        {
            //Returns user ids for usergroup
            List<int> a = new List<int>();
            DataTable rows = db.ExecuteStoredProcedure("usp_Get_Users_for_User_Group", this.Id);

            foreach (DataRow row in rows.Rows)
                a.Add((int)row["User_Id"]);
            return a;
        }

        public ArrayList Tasks()
        {
            //Returns tasks for usergroup
            ArrayList a = new ArrayList();
            DataTable myTasks = db.ExecuteStoredProcedure("usp_Get_Tasks_for_User_Group", this.Id);

            foreach (DataRow task in myTasks.Rows)
                a.Add(new Task(myTasks, task));
            return a;
        }

        public override ArrayList Reserves()
        {
            //Returns pending reserves for usergroup
            ArrayList a = new ArrayList();
            DataTable myReserves= db.ExecuteStoredProcedure("usp_Get_Reserves_for_User_Group", this.Id);

            foreach (DataRow res in myReserves.Rows)
                a.Add(new Reserve(myReserves, res));
            return a;
        }

        public override ArrayList UserTasks()
        {
            //Returns non-cleared UserTasks for usergroup
            ArrayList a = new ArrayList();
            DataTable myTasks = db.ExecuteStoredProcedure("usp_Get_User_Tasks_for_User_Group", this.Id);

            foreach (DataRow task in myTasks.Rows)
                a.Add(new UserTask(myTasks, task));
            return a;
        }
        #endregion
    }
}

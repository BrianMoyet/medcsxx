﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;

namespace MedCsxLogic
{
    public class TransactionLock:LockTable
    {

        private static clsDatabase db = new clsDatabase();
        private static modLogic ml = new modLogic();
        #region Constructors
        public TransactionLock() : base() { }
        public TransactionLock(int id) : base(id) { }
        public TransactionLock(Hashtable row) : base(row) { }
        public TransactionLock(DataTable table, DataRow row) : base(table, row) { }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Transaction_Lock"; }
        }

        public int TransactionId
        {
            get { return (int)row["Transaction_Id"]; }
            set { this.setRowValue("Transaction_Id", value); }
        }

        public int TransactionLockId
        {
            get { return (int)row["Transaction_Lock_Id"]; }
            set { this.setRowValue("Transaction_Lock_Id", value); }
        }
        #endregion

        #region Misc Methods
        public Transaction Transaction()
        {
            return new Transaction(this.TransactionId);
        }

        public Reserve Reserve()
        {
            return this.Transaction().Reserve;
        }

        public int ReserveId
        {
            get { return this.Transaction().ReserveId; }
        }

        public Claimant Claimant()
        {
            return this.Reserve().Claimant;
        }

        public int ClaimantId
        {
            get { return this.Reserve().ClaimantId; }
        }

        public Claim Claim()
        {
            return this.Reserve().Claim;
        }

        public int ClaimId
        {
            get { return this.Reserve().ClaimId; }
        }

        public void SendUnlockAlerts()
        {
            //sends unlock alerts for a transaction
            ArrayList userIds = new ArrayList();
            TransactionLock tl = new TransactionLock(TransactionLockId);
            FileNote f = new FileNote(tl.LockFileNoteId);
            EventType et = new EventType(f.EventTypeId);

            //add users to the userIds dictionary
            foreach (DataRow row in db.ExecuteStoredProcedure("usp_Get_Alert_Definition_Unlock_Users", et.Id).Rows)
                userIds.Add(row["User_Id"]);

            //send unlock alerts for transaction
            et.InsertAlerts(tl.ClaimId,tl.ReserveId,tl.UnlockFileNoteId,userIds,0, ml.DEFAULT_DATE, false,true);
        }

        public string DraftDescription
        {
            //draft description from transaction lock row
            get { return this.Transaction().Draft.Description; }
        }

        public void Unlock(string unlockFileNoteText)
        {
            //unlock transaction - update transaction lock row to turn lock off
            int unlockFileNoteId = this.Transaction().InsertFileNote((int)modGeneratedEnums.FileNoteType.Unlock_Transaction, unlockFileNoteText);  //insert unlock file note

            //unlock transaction
            this.isLocked = false;
            this.UnlockDate = ml.DBNow();
            this.UnlockedBy = Convert.ToInt32(HttpContext.Current.Session["userID"]);
            this.UnlockFileNoteId = UnlockFileNoteId;
            this.Update();

            //clear alerts issued because of lock
            FileNote f = new FileNote(this.LockFileNoteId);
            f.ClearAlerts();

            //file unlock transaction event
            this.Transaction().FireEvent((int)modGeneratedEnums.EventType.Unlock_Transaction);

            //send unlock alerts
            this.SendUnlockAlerts();
        }
        #endregion
    }
}

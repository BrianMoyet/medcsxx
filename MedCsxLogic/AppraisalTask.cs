﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using MedCsxDatabase;


namespace MedCsxLogic
{
    public class AppraisalTask : MedCsXTable
    {

        private static modLogic ml = new modLogic();
        private static clsDatabase db = new clsDatabase();
        #region Class Variables
        private AppraisalRequest m_AppraisalRequest;
        private Vehicle m_Vehicle = null;
        private SceneAccessAppraiser m_SceneAccessAppraiser = null;
        private int m_ClaimantId;
        private int m_PersonId;
        private AppraisalInvestigationType m_AppraisalInvestigationType = null;
        #endregion

        #region Constructors
        public AppraisalTask() : base() { }
        public AppraisalTask(int id) : base(id) { }
        public AppraisalTask(Hashtable row) : base(row) { }
        public AppraisalTask(DataTable table, DataRow row) : base(table, row) { }
        public AppraisalTask(AppraisalRequest appraisalRequest, int vehicleId, int claimantId = 0)
            : base()
        {
            this.AppraisalRequest = appraisalRequest;
            this.VehicleId = vehicleId;
            this.ClaimantId = claimantId;
            this.AppraisalTaskStatusId = (int)modGeneratedEnums.AppraisalTaskStatus.Appraisal_Requested;

            PopulateDefaultFields();
            PopulateClaimFields();
            PopulateClaimantFields();
            PopulateVehicleFields();
            PopulateUserFields();
        }
        #endregion

        #region Column Names
        public override string TableName
        {
            get { return "Appraisal_Task"; }
        }

        public string AdjCode
        {
            get { return (string)row["Adj_Code"]; }
            set { this.setRowValue("Adj_Code", this.fixTextField(value)); }
        }

        public int AppraisalInvestigationTypeId
        {
            get { return (int)row["Appraisal_Investigation_Type_Id"]; }
            set { this.setRowValue("Appraisal_Investigation_Type_Id", value); }
        }

        public int AppraisalRequestId
        {
            get { return (int)row["Appraisal_Request_Id"]; }
            set { this.setRowValue("Appraisal_Request_Id", value); }
        }

        public int AppraisalTaskId
        {
            get { return (int)row["Appraisal_Task_Id"]; }
            set { this.setRowValue("Appraisal_Task_Id", value); }
        }

        public int AppraisalTaskStatusId
        {
            get { return (int)row["Appraisal_Task_Status_Id"]; }
            set { this.setRowValue("Appraisal_Task_Status_Id", value); }
        }

        public string Appraiser
        {
            get { return (string)row["Appraiser"]; }
            set { this.setRowValue("Appraiser", this.fixTextField(value)); }
        }

        public DateTime AppraiserResponseDate
        {
            get { return (DateTime)row["Appraiser_Response_Date"]; }
            set { this.setRowValue("Appraiser_Response_Date", value); }
        }
        
        public string AssignedToUserId
        {
            get { return (string)row["Assigned_To_User_Id"]; }
            set { this.setRowValue("Assigned_To_User_Id", this.fixTextField(value)); }
        }

        public string ClaimNumber
        {
            get { return (string)row["Claim_Number"]; }
            set { this.setRowValue("Claim_Number", value); }
        }

        public string ClaimantAddress1
        {
            get { return (string)row["Claimant_Address1"]; }
            set { this.setRowValue("Claimant_Address1", this.fixTextField(value)); }
        }

        public string ClaimantAddress2
        {
            get { return (string)row["Claimant_Address2"]; }
            set { this.setRowValue("Claimant_Address2", this.fixTextField(value)); }
        }

        public string ClaimantCellPhone
        {
            get { return (string)row["Claimant_Cell_Phone"]; }
            set { this.setRowValue("Claimant_Cell_Phone", this.fixTextField(value)); }
        }

        public string ClaimantCity
        {
            get { return (string)row["Claimant_City"]; }
            set { this.setRowValue("Claimant_City", this.fixTextField(value)); }
        }

        public string ClaimantFirstName
        {
            get { return (string)row["Claimant_First_Name"]; }
            set { this.setRowValue("Claimant_First_Name", this.fixTextField(value)); }
        }

        public string ClaimantHomePhone
        {
            get { return (string)row["Claimant_Home_Phone"]; }
            set { this.setRowValue("Claimant_Home_Phone", this.fixTextField(value)); }
        }

        public string ClaimantLastName
        {
            get { return (string)row["Claimant_Last_Name"]; }
            set { this.setRowValue("Claimant_Last_Name", this.fixTextField(value)); }
        }


        public string ClaimantState
        {
            get { return (string)row["Claimant_State"]; }
            set { this.setRowValue("Claimant_State", this.fixTextField(value)); }
        }

        public string ClaimantWorkPhone
        {
            get { return (string)row["Claimant_Work_Phone"]; }
            set { this.setRowValue("Claimant_Work_Phone", this.fixTextField(value)); }
        }

        public string ClaimantZip
        {
            get { return (string)row["Claimant_Zip"]; }
            set { this.setRowValue("Claimant_Zip", this.fixTextField(value)); }
        }

        public string Color
        {
            get { return (string)row["Color"]; }
            set { this.setRowValue("Color", this.fixTextField(value)); }
        }

        public string DateOfLoss
        {
            get { return (string)row["Date_Of_Loss"]; }
            set { this.setRowValue("Date_Of_Loss", this.fixTextField(value)); }
        }

        public decimal DeductibleAmount
        {
            get { return (decimal)row["Deductible_Amount"]; }
            set { this.setRowValue("Deductible_Amount", value); }
        }

        public bool EMSForm
        {
            get { return (bool)row["EMS_Form"]; }
            set { this.setRowValue("EMS_Form", value); }
        }

        public string InsuranceCoAddress1
        {
            get { return (string)row["Insurance_Co_Address1"]; }
            set { this.setRowValue("Insurance_Co_Address1", this.fixTextField(value)); }
        }

        public string InsuranceCoAddress2
        {
            get { return (string)row["Insurance_Co_Address2"]; }
            set { this.setRowValue("Insurance_Co_Address2", this.fixTextField(value)); }
        }

        public string InsuranceCoCity
        {
            get { return (string)row["Insurance_Co_City"]; }
            set { this.setRowValue("Insurance_Co_City", this.fixTextField(value)); }
        }

        public string InsuranceCoContactEmailAddress
        {
            get { return (string)row["Insurance_Co_Contact_Email_Address"]; }
            set { this.setRowValue("Insurance_Co_Contact_Email_Address", this.fixTextField(value)); }
        }

        public string InsuranceCoContactFirstName
        {
            get { return (string)row["Insurance_Co_Contact_First_Name"]; }
            set { this.setRowValue("Insurance_Co_Contact_First_Name", this.fixTextField(value)); }
        }

        public string InsuranceCoContactLastName
        {
            get { return (string)row["Insurance_Co_Contact_Last_Name"]; }
            set { this.setRowValue("Insurance_Co_Contact_Last_Name", this.fixTextField(value)); }
        }

        public string InsuranceCoFax
        {
            get { return (string)row["Insurance_Co_Fax"]; }
            set { this.setRowValue("Insurance_Co_Fax", this.fixTextField(value)); }
        }

        public string InsuranceCoName
        {
            get { return (string)row["Insurance_Co_Name"]; }
            set { this.setRowValue("Insurance_Co_Name", this.fixTextField(value)); }
        }

        public string InsuranceCoPhone
        {
            get { return (string)row["Insurance_Co_Phone"]; }
            set { this.setRowValue("Insurance_Co_Phone", this.fixTextField(value)); }
        }

        public string InsuranceCoState
        {
            get { return (string)row["Insurance_Co_State"]; }
            set { this.setRowValue("Insurance_Co_State", this.fixTextField(value)); }
        }

        public string InsuranceCoZip
        {
            get { return (string)row["Insurance_Co_Zip"]; }
            set { this.setRowValue("Insurance_Co_Zip", this.fixTextField(value)); }
        }

        public string InsuredAddress1
        {
            get { return (string)row["Insured_Address1"]; }
            set { this.setRowValue("Insured_Address1", this.fixTextField(value)); }
        }

        public string InsuredAddress2
        {
            get { return (string)row["Insured_Address2"]; }
            set { this.setRowValue("Insured_Address2", this.fixTextField(value)); }
        }

        public string InsuredBusinessName
        {
            get { return (string)row["Insured_Business_Name"]; }
            set { this.setRowValue("Insured_Business_Name", value); }
        }

        public string InsuredCellPhone
        {
            get { return (string)row["Insured_Cell_Phone"]; }
            set { this.setRowValue("Insured_Cell_Phone", this.fixTextField(value)); }
        }

        public string InsuredCity
        {
            get { return (string)row["Insured_City"]; }
            set { this.setRowValue("Insured_City", this.fixTextField(value)); }
        }

        public string InsuredFirstName
        {
            get { return (string)row["Insured_First_Name"]; }
            set { this.setRowValue("Insured_First_Name", this.fixTextField(value)); }
        }

        public string InsuredHomePhone
        {
            get { return (string)row["Insured_Home_Phone"]; }
            set { this.setRowValue("Insured_Home_Phone", this.fixTextField(value)); }
        }

        public string InsuredLastName
        {
            get { return (string)row["Insured_Last_Name"]; }
            set { this.setRowValue("Insured_Last_Name", this.fixTextField(value)); }
        }

        public string InsuredOrClaimant
        {
            get { return (string)row["Insured_Or_Claimant"]; }
            set { this.setRowValue("Insured_Or_Claimant", this.fixTextField(value)); }
        }

        public string InsuredState
        {
            get { return (string)row["Insured_State"]; }
            set { this.setRowValue("Insured_State", this.fixTextField(value)); }
        }

        public string InsuredWorkPhone
        {
            get { return (string)row["Insured_Work_Phone"]; }
            set { this.setRowValue("Insured_Work_Phone", this.fixTextField(value)); }
        }

        public string InsuredZip
        {
            get { return (string)row["Insured_Zip"]; }
            set { this.setRowValue("Insured_Zip", this.fixTextField(value)); }
        }

        public bool isVehicleDrivable
        {
            get { return (bool)row["Is_Vehicle_Drivable"]; }
            set { this.setRowValue("Is_Vehicle_Drivable", value); }
        }

        public string LicensePlateNumber
        {
            get { return (string)row["License_Plate_Number"]; }
            set { this.setRowValue("License_Plate_Number", this.fixTextField(value)); }
        }

        public string LicensePlateState
        {
            get { return (string)row["License_Plate_State"]; }
            set { this.setRowValue("License_Plate_State", this.fixTextField(value)); }
        }

        public string LocationOfVehicleAddress1
        {
            get { return (string)row["Location_Of_Vehicle_Address1"]; }
            set { this.setRowValue("Location_Of_Vehicle_Address1", this.fixTextField(value)); }
        }

        public string LocationOfVehicleAddress2
        {
            get { return (string)row["Location_Of_Vehicle_Address2"]; }
            set { this.setRowValue("Location_Of_Vehicle_Address2", this.fixTextField(value)); }
        }

        public string LocationOfVehicleCity
        {
            get { return (string)row["Location_Of_Vehicle_City"]; }
            set { this.setRowValue("Location_Of_Vehicle_City", this.fixTextField(value)); }
        }

        public string LocationOfVehicleContactEmailAddress
        {
            get { return (string)row["Location_Of_Vehicle_Contact_Email_Address"]; }
            set { this.setRowValue("Location_Of_Vehicle_Contact_Email_Address", this.fixTextField(value)); }
        }

        public string LocationOfVehicleContactFirstName
        {
            get { return (string)row["Location_Of_Vehicle_Contact_First_Name"]; }
            set { this.setRowValue("Location_Of_Vehicle_Contact_First_Name", this.fixTextField(value)); }
        }

        public string LocationOfVehicleContactLastName
        {
            get { return (string)row["Location_Of_Vehicle_Contact_Last_Name"]; }
            set { this.setRowValue("Location_Of_Vehicle_Contact_Last_Name", this.fixTextField(value)); }
        }

        public string LocationOfVehiclePhone1
        {
            get { return (string)row["Location_Of_Vehicle_Phone1"]; }
            set { this.setRowValue("Location_Of_Vehicle_Phone1", this.fixTextField(value)); }
        }

        public string LocationOfVehiclePhone2
        {
            get { return (string)row["Location_Of_Vehicle_Phone2"]; }
            set { this.setRowValue("Location_Of_Vehicle_Phone2", this.fixTextField(value)); }
        }

        public string LocationOfVehicleState
        {
            get { return (string)row["Location_Of_Vehicle_State"]; }
            set { this.setRowValue("Location_Of_Vehicle_State", this.fixTextField(value)); }
        }

        public string LocationOfVehicleZip
        {
            get { return (string)row["Location_Of_Vehicle_Zip"]; }
            set { this.setRowValue("Location_Of_Vehicle_Zip", this.fixTextField(value)); }
        }

        public string LossDescription
        {
            get { return (string)row["Loss_Description"]; }
            set { this.setRowValue("Loss_Description", this.fixTextField(value)); }
        }

        public string MakeCode
        {
            get { return (string)row["Make_Code"]; }
            set { this.setRowValue("Make_Code", this.fixTextField(value)); }
        }

        public string Model
        {
            get { return (string)row["Model"]; }
            set { this.setRowValue("Model", this.fixTextField(value)); }
        }

        public string PolicyNumber
        {
            get { return (string)row["Policy_Number"]; }
            set { this.setRowValue("Policy_Number", this.fixTextField(value)); }
        }

        public bool RequiresApproval
        {
            get { return (bool)row["Requires_Approval"]; }
            set { this.setRowValue("Requires_Approval", value); }
        }

        public int SceneAccessAppraiserId
        {
            get { return (int)row["Scene_Access_Appraiser_Id"]; }
            set
            {
                this.setRowValue("Scene_Access_Appraiser_Id", value);
                this.m_SceneAccessAppraiser = null; //refresh on next use
                this.AssignedToUserId = this.SceneAccessAppraiser.SceneAccessUserId.ToString();
            }
        }

        public string SpecialInstructions
        {
            get { return (string)row["Special_Instructions"]; }
            set { this.setRowValue("Special_Instructions", this.fixTextField(value)); }
        }

        public string TaskDescription
        {
            get { return (string)row["Task_Description"]; }
            set { this.setRowValue("Task_Description", this.fixTextField(value)); }
        }

        public string TypeOfClaim
        {
            get { return (string)row["Type_Of_Claim"]; }
            set { this.setRowValue("Type_Of_Claim", this.fixTextField(value)); }
        }

        public string TypeOfInvestigationNeeded
        {
            get { return (string)row["Type_Of_Investigation_Needed"]; }
            set { this.setRowValue("Type_Of_Investigation_Needed", this.fixTextField(value)); }
        }

        public string UploadNotificationEmailAddress
        {
            get { return (string)row["Upload_Notification_Email_Address"]; }
            set { this.setRowValue("Upload_Notification_Email_Address", this.fixTextField(value)); }
        }

        public string VIN
        {
            get { return (string)row["VIN"]; }
            set { this.setRowValue("VIN", this.fixTextField(value)); }
        }

        public int VehicleId
        {
            get { return (int)row["Vehicle_Id"]; }
            set
            {
                this.setRowValue("Vehicle_Id", value);
                m_Vehicle = null;
            }
        }

        public string YearMade
        {
            get { return (string)row["Year_Made"]; }
            set
            {
                //Scene Access wants a 2-digit year
                if (value.Length > 2)
                    this.setRowValue("Year_Made", value.Substring(value.Length - 2, 2));
                else
                    this.setRowValue("Year_Made", value);
            }
        }
        #endregion

        #region Misc Methods
        public AppraisalInvestigationType AppraisalInvestigationType()
        {
            return this.m_AppraisalInvestigationType == null ?
                this.m_AppraisalInvestigationType = new AppraisalInvestigationType(this.AppraisalInvestigationTypeId) :
                this.m_AppraisalInvestigationType;
        }

        public AppraisalRequest AppraisalRequest
        {
            get
            {
                return this.m_AppraisalRequest == null ?
                    this.m_AppraisalRequest = new AppraisalRequest(this.AppraisalRequestId) : this.m_AppraisalRequest;
            }
            set { m_AppraisalRequest = value; }
        }

        public string AppraisalRequestedFileNoteText
        {
            get
            {
                MedCsXSystem mySystem = new MedCsXSystem();
                string s = "";
                if (this.Claim() != null)
                    s = "Claim: " + this.Claim().DisplayClaimId + Environment.NewLine + Environment.NewLine;

                if (this.VehicleId == 0)
                    s += "Appraisal Requested ";
                else
                    s += "Appraisal Requested for " + this.Vehicle().Name;

                s += " By " + mySystem.CurrentUser.Name + "." + Environment.NewLine + Environment.NewLine;
                s += "Vehicle: " + this.VehicleDescription + Environment.NewLine;

                try
                {
                    s += "Appraiser: " + this.SceneAccessAppraiser.Vendor.Name + Environment.NewLine;
                }
                catch { }

                try
                {
                    if (this.Claim() != null)
                    {
                        s += "Date of Loss: " + this.Claim().DateOfLoss.ToShortDateString() + Environment.NewLine;
                        if (this.Claim().isCollision)
                            s += "Collision Deductible: " + this.Claim().CollisionDeductible().ToString("c") + Environment.NewLine;

                        if (this.Claim().isComp)
                            s += "Comprehensive Deductible: " + this.Claim().ComprehensiveDeductible().ToString("c") + Environment.NewLine;
                    }
                }
                catch { }

                s += "Type of Claim: " + this.TypeOfClaim + Environment.NewLine;
                s += "Investigation Type: " + this.AppraisalInvestigationType().Description + Environment.NewLine + Environment.NewLine;
                s += "Task Description: " + this.TaskDescription + Environment.NewLine + Environment.NewLine;
                s += "Special Instructions: " + this.SpecialInstructions + Environment.NewLine + Environment.NewLine;
                s += "Location of Vehicle: " + this.VehicleLocationDescription + Environment.NewLine + Environment.NewLine;
                s += "Insured Details: " + this.InsuredDescription + Environment.NewLine + Environment.NewLine;
                s += "Claimant Details: " + this.ClaimantDescription + Environment.NewLine + Environment.NewLine;

                return s;
            }
        }

        public Claim Claim()
        {
            return this.AppraisalRequest.Claim;
        }

        public string ClaimantDescription
        {
            get
            {
                string s = this.ClaimantFirstName + " " + this.ClaimantLastName;
                if (this.ClaimantAddress1 != "")
                    s += Environment.NewLine + this.ClaimantAddress1;

                if (this.ClaimantAddress2 != "")
                    s += Environment.NewLine + this.ClaimantAddress2;

                if ((this.ClaimantCity != "") || (this.ClaimantState != "") || (this.ClaimantZip != ""))
                    s += Environment.NewLine + this.ClaimantCity + ", " + this.ClaimantState + " " + this.ClaimantZip;

                if (this.ClaimantHomePhone != "")
                    s += Environment.NewLine + "Home: " + this.ClaimantHomePhone;

                if (this.ClaimantWorkPhone != "")
                    s += Environment.NewLine + "Work: " + this.ClaimantWorkPhone;

                if (this.ClaimantCellPhone != "")
                    s += Environment.NewLine + "Cell: " + this.ClaimantCellPhone;
                return s;
            }
        }

        public int ClaimantId
        {
            get { return this.m_ClaimantId; }
            set { this.m_ClaimantId = value; }
        }

        public int ClaimId
        {
            get { return this.AppraisalRequest.ClaimId; }
            set { this.AppraisalRequest.ClaimId = value; }
        }

        private string fixTextField(string strValue)
        {
            try
            {
                string s = strValue.Replace("&", " and ");
                s = s.Replace("  and", " and");
                s = s.Replace("and  ", "and ");
                return s;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return strValue;
            }
        }

        public string InsuranceCoDescription
        {
            //returns a description of the insurance company (name, address, phone, etc.)
            get
            {
                string s = this.InsuranceCoName + Environment.NewLine + this.InsuranceCoAddress1;
                if (this.InsuranceCoAddress2 != "")
                    s += Environment.NewLine + this.InsuranceCoAddress2;

                if ((this.InsuranceCoCity != "") || (this.InsuranceCoState != "") || (this.InsuranceCoZip != ""))
                    s += Environment.NewLine + this.InsuranceCoCity + ", " + this.InsuranceCoState + " " + this.InsuranceCoZip;

                if (this.InsuranceCoPhone != "")
                    s += Environment.NewLine + "Phone: " + this.InsuranceCoPhone;

                if (this.InsuranceCoFax != "")
                    s += Environment.NewLine + "Fax: " + this.InsuranceCoFax;

                s += Environment.NewLine + Environment.NewLine + "Contact: " + this.InsuranceCoContactFirstName +
                    " " + this.InsuranceCoContactLastName + Environment.NewLine + this.InsuranceCoContactEmailAddress;
                return s;
            }
        }

        public string InsuredVehicleDescription
        {
            get
            {
                try
                {
                    Vehicle car = this.Claim().InsuredVehicle;
                    string s = car.LongName;

                    if (car.LossPayeeDescription() != "")
                        s += Environment.NewLine + Environment.NewLine + "Loss Payee: " + Environment.NewLine + car.LossPayeeDescription();
                    return s;
                }
                catch
                {
                    return "";
                }
            }
        }

        public string InsuredDescription
        {
            get
            {
                string s = this.InsuredFirstName + " " + this.InsuredLastName;
                if (this.InsuredAddress1 != "")
                    s += Environment.NewLine + this.InsuredAddress1;

                if (this.InsuredAddress2 != "")
                    s += Environment.NewLine + this.InsuredAddress2;

                if ((this.InsuredCity != "") || (this.InsuredState != "") || (this.InsuredZip != ""))
                    s += Environment.NewLine + this.InsuredCity + ", " + this.InsuredState + " " + this.InsuredZip;

                if (this.InsuredHomePhone != "")
                    s += Environment.NewLine + "Home: " + this.InsuredHomePhone;

                if (this.InsuredWorkPhone != "")
                    s += Environment.NewLine + "Work: " + this.InsuredWorkPhone;

                if (this.InsuredCellPhone != "")
                    s += Environment.NewLine + "Cell: " + this.InsuredCellPhone;
                return s;
            }
        }

        public int PersonId
        {
            get { return this.m_PersonId; }
            set { this.m_PersonId = value; }
        }

        public SceneAccessAppraiser SceneAccessAppraiser
        {
            get
            {
                return this.m_SceneAccessAppraiser == null ?
                    this.m_SceneAccessAppraiser = new SceneAccessAppraiser(this.SceneAccessAppraiserId) :
                    this.m_SceneAccessAppraiser;
            }
            set { this.m_SceneAccessAppraiser = value; }
        }
        
        public Vehicle Vehicle()
        {
            return m_Vehicle == null ? m_Vehicle = new Vehicle(this.VehicleId) : m_Vehicle;
        }

        public string VehicleDescription
        {
            get
            {
                string s = this.Vehicle().YearMade + " " + this.MakeCode + " " + this.Model;
                if (this.VIN != "")
                    s += Environment.NewLine + "VIN: " + this.VIN;

                if (this.Color != "")
                    s += Environment.NewLine + "Color: " + this.Color;

                if (this.LicensePlateNumber != "")
                    s += Environment.NewLine + "License Plate: " + this.LicensePlateNumber;

                if (this.LicensePlateState != "")
                    s += Environment.NewLine + "License Plate State: " + this.LicensePlateState;
                return s;
            }
        }

        public string VehicleLocationDescription
        {
            get
            {
                string s = "";
                if (this.LocationOfVehicleAddress1 != "")
                    s += this.LocationOfVehicleAddress1 + Environment.NewLine;

                if (this.LocationOfVehicleAddress2 != "")
                    s += this.LocationOfVehicleAddress2 + Environment.NewLine;

                if ((this.LocationOfVehicleCity != "") || (this.LocationOfVehicleState != "") || (this.LocationOfVehicleZip != ""))
                    s += this.LocationOfVehicleCity + ", " + this.LocationOfVehicleState + " " + this.LocationOfVehicleZip + Environment.NewLine;

                if (this.LocationOfVehiclePhone1 != "")
                    s += "Phone 1: " + this.LocationOfVehiclePhone1 + Environment.NewLine;

                if (this.LocationOfVehiclePhone2 != "")
                    s += "Phone 2: " + this.LocationOfVehiclePhone2 + Environment.NewLine;

                if (s != "")
                    s += Environment.NewLine;

                if ((this.LocationOfVehicleContactFirstName != "") || (this.LocationOfVehicleContactLastName != ""))
                    s += "Contact: " + this.LocationOfVehicleContactFirstName + " " + this.LocationOfVehicleContactLastName + Environment.NewLine;

                if (this.LocationOfVehicleContactEmailAddress != "")
                    s += this.LocationOfVehicleContactEmailAddress;
                return s;
            }
        }

        public void PopulateDefaultFields()
        {
            this.SpecialInstructions = MedCsXSystem.Settings.SceneAccessDefaultSpecialInstructions;
            this.InsuranceCoName = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoName;
            this.InsuranceCoAddress1 = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoAddress1;
            this.InsuranceCoAddress2 = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoAddress2;
            this.InsuranceCoCity = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoCity;
            this.InsuranceCoState = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoState;
            this.InsuranceCoZip = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoZip;
            this.InsuranceCoFax = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoFax;
            this.InsuranceCoPhone = MedCsXSystem.Settings.SceneAccessDefaultInsuranceCoPhone;
            this.EMSForm = MedCsXSystem.Settings.SceneAccessEMSForm;
            this.RequiresApproval = MedCsXSystem.Settings.SceneAccessRequiresApproval;
            this.TypeOfClaim = MedCsXSystem.Settings.SceneAccessDefaultTypeOfClaim;
            this.TypeOfInvestigationNeeded = MedCsXSystem.Settings.SceneAccessDefaultTypeOfInvestigation;
            this.TaskDescription = MedCsXSystem.Settings.SceneAccessDefaultTaskDescription;
        }

        public void PopulateClaimFields()
        {
            this.ClaimNumber = this.Claim().DisplayClaimId;
            this.PolicyNumber = this.Claim().PolicyNo;
            this.DateOfLoss = FormatDateForSceneAccess(this.Claim().DateOfLoss);
            this.LossDescription = this.Claim().LossDescription;
            this.DeductibleAmount = Convert.ToDecimal(this.Claim().MaxDeductible);
            this.InsuredFirstName = this.Claim().InsuredPerson.FirstName;
            this.InsuredLastName = this.Claim().InsuredPerson.LastName;
            this.InsuredAddress1 = this.Claim().InsuredPerson.Address1;
            this.InsuredAddress2 = this.Claim().InsuredPerson.Address2;
            this.InsuredCellPhone = FormatPhoneNumberForSceneAccess(this.Claim().InsuredPerson.CellPhone);
            this.InsuredCity = this.Claim().InsuredPerson.City;
            this.InsuredState = this.Claim().InsuredPerson.State.Abbreviation;
            this.InsuredZip = this.Claim().InsuredPerson.Zipcode;
            this.InsuredHomePhone = FormatPhoneNumberForSceneAccess(this.Claim().InsuredPerson.HomePhone);
            this.InsuredWorkPhone = FormatPhoneNumberForSceneAccess(this.Claim().InsuredPerson.WorkPhone);

            if (this.Claim().InsuredPerson.CellPhone != "")
            {
                if (this.InsuredHomePhone == "")
                    this.InsuredHomePhone = FormatPhoneNumberForSceneAccess(this.Claim().InsuredPerson.CellPhone);
                else
                {
                    if (this.InsuredWorkPhone == "")
                        this.InsuredWorkPhone = FormatPhoneNumberForSceneAccess(this.Claim().InsuredPerson.CellPhone);
                }
            }

            if (this.VehicleId == this.Claim().InsuredVehicleId)
                this.InsuredOrClaimant = "Insured";
            else
                this.InsuredOrClaimant = "Claimant";
        }

        private string FormatDateForSceneAccess(DateTime d)
        {
            return d.ToString("MM") + "/" + d.ToString("dd") + "/" + d.ToString("yyyy");
        }

        private string FormatPhoneNumberForSceneAccess(string phNo)
        {
            //MedCsX phone number format is too long for Scene Access.  Change (999) 999-9999 to 999-999-9999.
            if (ml.CountDigits(phNo) == 0)
                return "";
            return phNo.Replace("(", "").Replace(") ", "-");
        }

        public void PopulateClaimantFields()
        {
            Person p;
            if (this.ClaimantId != 0)
            {
                this.PopulateClaimantFields(new Claimant(ClaimantId));
                return;
            }
            else
                p = this.Vehicle().OwnerPerson;

            if (p != null)
            {
                this.ClaimantAddress1 = p.Address1;
                this.ClaimantAddress2 = p.Address2;
                this.ClaimantCellPhone = FormatPhoneNumberForSceneAccess(p.CellPhone);
                this.ClaimantCity = p.City;
                this.ClaimantFirstName = p.FirstName;
                this.ClaimantLastName = p.LastName;
                this.ClaimantState = p.State.Abbreviation;
                this.ClaimantWorkPhone = FormatPhoneNumberForSceneAccess(p.WorkPhone);
                this.ClaimantZip = p.Zipcode;
                this.ClaimantHomePhone = FormatPhoneNumberForSceneAccess(p.HomePhone);

                if (p.CellPhone != "")
                {
                    if (this.ClaimantHomePhone == "")
                        this.ClaimantHomePhone = FormatPhoneNumberForSceneAccess(p.CellPhone);
                    else
                    {
                        if (this.ClaimantWorkPhone == "")
                            this.ClaimantWorkPhone = FormatPhoneNumberForSceneAccess(p.CellPhone);
                    }
                }
            }
        }

        public void PopulateClaimantFields(Claimant claimant)
        {
            Person p = claimant.Person;
            if (p != null)
            {
                this.ClaimantAddress1 = p.Address1;
                this.ClaimantAddress2 = p.Address2;
                this.ClaimantCellPhone = FormatPhoneNumberForSceneAccess(p.CellPhone);
                this.ClaimantCity = p.City;
                this.ClaimantFirstName = p.FirstName;
                this.ClaimantLastName = p.LastName;
                this.ClaimantState = p.State.Abbreviation;
                this.ClaimantWorkPhone = FormatPhoneNumberForSceneAccess(p.WorkPhone);
                this.ClaimantZip = p.Zipcode;
                this.ClaimantHomePhone = FormatPhoneNumberForSceneAccess(p.HomePhone);
                this.PersonId = p.Id;

                if (p.CellPhone != "")
                {
                    if (this.ClaimantHomePhone == "")
                        this.ClaimantHomePhone = FormatPhoneNumberForSceneAccess(p.CellPhone);
                    else
                    {
                        if (this.ClaimantWorkPhone == "")
                            this.ClaimantWorkPhone = FormatPhoneNumberForSceneAccess(p.CellPhone);
                    }
                }
            }
        }

        public void PopulateVehicleFields()
        {
            if (this.VehicleId == 0)
                return;

            Vehicle car = new Vehicle(this.VehicleId);

            this.MakeCode = car.Manufacturer;
            this.Model = car.Model;
            this.YearMade = car.YearMade;
            this.VIN = car.VIN;
            this.Color = car.Color;
            this.LicensePlateNumber = car.LicensePlateNo;
            this.LicensePlateState = car.LicensePlateState.Abbreviation;
            this.LocationOfVehicleAddress1 = car.LocationAddress1;
            this.LocationOfVehicleAddress2 = car.LocationAddress2;
            this.LocationOfVehicleCity = car.LocationCity;
            this.LocationOfVehicleContactEmailAddress = car.LocationContactEmailAddress;
            this.LocationOfVehicleContactFirstName = car.LocationContactFirstName;
            this.LocationOfVehicleContactLastName = car.LocationContactLastName;

            this.LocationOfVehiclePhone1 = FormatPhoneNumberForSceneAccess(car.LocationPhone1);
            this.LocationOfVehiclePhone2 = FormatPhoneNumberForSceneAccess(car.LocationPhone2);
            this.LocationOfVehicleState = car.LocationState.Abbreviation;
            this.LocationOfVehicleZip = car.LocationZipcode;
        }

        public void PopulateUserFields()
        {
            MedCsXSystem mySystem = new MedCsXSystem();
            this.InsuranceCoContactFirstName = mySystem.CurrentUser.FirstName;
            this.InsuranceCoContactLastName = mySystem.CurrentUser.LastName;
            this.InsuranceCoContactEmailAddress = mySystem.CurrentUser.EmailAddress;
            this.AdjCode = mySystem.CurrentUser.SceneAccessAdjCode;
            if (this.AdjCode == "")
                this.AdjCode = mySystem.CurrentUser.SceneAccessAdjCode;
        }

        public ArrayList AppraisalResponses
        {
            //returns appraisal responses for this task
            get
            {
                ArrayList a = new ArrayList();
                DataTable rows = db.ExecuteStoredProcedure("usp_Get_Appraisal_Responses_For_Task", this.Id);
                foreach (DataRow row in rows.Rows)
                    a.Add(new AppraisalResponse(rows, row));
                return a;
            }
        }

        public void SendAppraisalCancellationEmail(string body)
        {
            //sends an appraisal email to the vendor
            SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, MedCsXSystem.Settings.AppraisalEmailAddress,
                this.SceneAccessAppraiser.Vendor.Email,
                "Cancel Appraisal on " + this.Claim().DisplayClaimId + ", " + this.Vehicle().Name, body, false);
        }

        public void SendAppraisalEmail(string appraisalBody)
        {
            //sends appraisal email to vendor
            //SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, MedCsXSystem.Settings.AppraisalEmailAddress, this.SceneAccessAppraiser.Vendor.Email,
            //    this.ReplacePlaceholders(MedCsXSystem.Settings.AppraisalSubjectLine), appraisalBody, false);
            
            SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, MedCsXSystem.Settings.AppraisalEmailAddress, this.SceneAccessAppraiser.Vendor.Email,
               this.ReplacePlaceholders(MedCsXSystem.Settings.AppraisalSubjectLine), appraisalBody, false);
        }

<<<<<<< HEAD
        public void SendAppraisalEmail()
        {
            //sends appraisal email to vendor
            SendEmail(MedCsXSystem.Settings.AppraisalSMTPServer, MedCsXSystem.Settings.AppraisalEmailAddress, this.SceneAccessAppraiser.Vendor.Email,
                this.ReplacePlaceholders(MedCsXSystem.Settings.AppraisalSubjectLine), this.AppraisalEmailBody(), false);
        }

        public string AppraisalEmailBody()
        {
            //Production
            //string vd = HttpContext.Current.Server.MapPath("~/AppraisalTemplates/");

            //QA and Local
            string vd = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["AppraisalTemplate"].ToString() + "appraisal_request_template_new.txt");
            //string mappedPath = HttpContext.Current.Server.MapPath(MedCsXSystem.Settings.AppraisalRequestFileName);
            StreamReader sr = new StreamReader(vd);
            string s = sr.ReadToEnd();
            sr.Close();

            return this.ReplacePlaceholders(s);
            //return s;
        }

=======
>>>>>>> 599fc40daa8032485eafabcd5e0176f600dadc8e
        public string AppraisalEmailBody(string dir)
        {
            //Production
            //string vd = HttpContext.Current.Server.MapPath("~/AppraisalTemplates/");
            
            //QA and Local
            string vd = HttpContext.Current.Server.MapPath(dir + "appraisal_request_template_new.txt");
            //string mappedPath = HttpContext.Current.Server.MapPath(MedCsXSystem.Settings.AppraisalRequestFileName);
            StreamReader sr = new StreamReader(vd);
            string s = sr.ReadToEnd();
            sr.Close();

            //return this.ReplacePlaceholders(s);
            return s;
        }

        //public static string VirtualToPhysicalPath(string vPath)
        //{
        //    // Remove query string:
        //    vPath = Regex.Replace(vPath, @"\?.+", "").ToLower();

        //    // Check if file is in standard folder:
        //    var pPath = System.Web.Hosting.HostingEnvironment.MapPath("~" + vPath);
        //    if (System.IO.File.Exists(pPath)) return pPath;

        //    // Else check for IIS virtual directory:
        //    var siteName = System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();
        //    var sm = new Administration.ServerManager();
        //    var vDirs = sm.Sites[siteName].Applications[0].VirtualDirectories;
        //    foreach (var vd in vDirs)
        //    {
        //        if (vd.Path != "/" && vPath.Contains(vd.Path.ToLower())) pPath = vPath.Replace(vd.Path.ToLower(), vd.PhysicalPath).Replace("/", "\\");
        //    }
        //    return pPath;
        //}

        private string ReplacePlaceholders(string s)
        {
            /*replaces all the placeholders in string s with their actual values.
             * e.g., #CLAIM_NUMBER# is replaced with BMKC11234.
             * returns all replacements made.
            */

            string s2 = s.Replace("#DATE#", ml.DBNow().ToLongDateString());

            //get appraiser full name an first name
            string appraiserName = this.SceneAccessAppraiser.Vendor.Name;
            string[] a = appraiserName.Split(' ');
            string appraiserFirstName;
            if (a.Length - 1 == 1)
                appraiserFirstName = a[0];
            else
                appraiserFirstName = appraiserName;

            s2 = s2.Replace("#APPRAISER_FIRST_NAME#", appraiserFirstName);
            s2 = s2.Replace("#ADJUSTER_NAME#", ml.CurrentUser.Name);
            s2 = s2.Replace("#ADJUSTER_EXT#", ml.CurrentUser.PhoneExt);
            s2 = s2.Replace("#APPRAISER_NAME#", appraiserName);
            s2 = s2.Replace("#TASK_DESCRIPTION#", this.TaskDescription);
            s2 = s2.Replace("#INSURED_OR_CLAIMANT#", this.InsuredOrClaimant);
            s2 = s2.Replace("#VEHICLE_NAME#", this.Vehicle().Name);
            s2 = s2.Replace("#VIN#", this.VIN);
            s2 = s2.Replace("#VEHICLE_COLOR#", this.Vehicle().Color);
            s2 = s2.Replace("#LICENSE_PLATE_STATE#", this.Vehicle().LicensePlateState.Description.Replace("Undefined", ""));
            s2 = s2.Replace("#LICENSE_PLATE#", this.Vehicle().LicensePlateNo);
            s2 = s2.Replace("#VEHICLE_LOCATION_CONTACT#", this.VehicleLocationDescription);
            s2 = s2.Replace("#VEHICLE_LOCATION#", this.VehicleLocationDescription);
            s2 = s2.Replace("#POLICY_NUMBER#", this.PolicyNumber);
            s2 = s2.Replace("#CLAIM_NUMBER#", this.Claim().DisplayClaimId);
            s2 = s2.Replace("#DATE_OF_LOSS#", this.DateOfLoss);
            s2 = s2.Replace("#DEDUCTIBLE#", this.DeductibleAmount.ToString("c"));
            s2 = s2.Replace("#TYPE_OF_CLAIM#", this.TypeOfClaim);
            s2 = s2.Replace("#LOSS_DESCRIPTION#", this.LossDescription);
            s2 = s2.Replace("#INSURED_INFO#", this.InsuredDescription);
            s2 = s2.Replace("#INSURED_VEHICLE#", this.InsuredVehicleDescription);
            s2 = s2.Replace("#CLAIMANT_INFO#", this.ClaimantDescription);
            s2 = s2.Replace("#WHERE_IV_SEEN#", this.Claim().WhereSeen);  //this will go on the appraisal request if the claim has comp/collision
            s2 = s2.Replace("#APPRAISAL_REQUEST_ID#", AppraisalRequestId.ToString());
            //add property damage fields
            if (this.Vehicle().PropertyDamage != null)
            {
                s2 = s2.Replace("#DAMAGE_DESCRIPTION#", this.Vehicle().PropertyDamage.DamageDescription);
                s2 = s2.Replace("#WHERE_SEEN#", this.Vehicle().PropertyDamage.WhereSeen);
                s2 = s2.Replace("#ESTIMATED_DAMAGE_AMOUNT#", this.Vehicle().PropertyDamage.EstimatedDamageAmount.ToString("c"));
            }
            return s2;
        }
        #endregion

    }
}

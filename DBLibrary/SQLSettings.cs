﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedJames.Database.SQL
{
    [Serializable]
    public class SQLSettings
    {

        #region Private Members
        private bool bUseTrustedConnection;
        private bool bUseConnectionPooling;
        private string strSQLServer;
        private string strSQLDB;
        private string strSQLUser;
        private string strSQLPassword;
        private int intSQLTimeOut;
        private int intSQLConMin;
        private int intSQLConMax;
        #endregion

        #region Public Properties
        public string SQLServer
        {
            get { return (strSQLServer); }
            set { strSQLServer = value; }
        }

        public string SQLDB
        {
            get { return (strSQLDB); }
            set { strSQLDB = value; }
        }

        public string SqlUserID
        {
            get { return (strSQLUser); }
            set { strSQLUser = value; }
        }

        public string SqlUserPassword
        {
            get { return (strSQLPassword); }
            set { strSQLPassword = value; }
        }

        public string SQLConnectionString
        {
            get { return (GenerateConnectionString()); }
        }

        public int SQLConnectionTimeout
        {
            get { return (intSQLTimeOut); }
            set { intSQLTimeOut = value; }
        }

        public int SQLPoolMinConnections
        {
            get { return (intSQLConMin); }
            set { intSQLConMin = value; }
        }

        public int SQLPoolMaxConnections
        {
            get { return (intSQLConMax); }
            set { intSQLConMax = value; }
        }

        public bool UseTrustedConnection
        {
            get { return (bUseTrustedConnection); }
            set { bUseTrustedConnection = value; }
        }

        public bool UseConnectionPooling
        {
            get { return (bUseConnectionPooling); }
            set { bUseConnectionPooling = value; }
        }
        #endregion

        #region Helper Methods
        private string GenerateConnectionString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Server={0}; Database={1}; Network Library=dbmssocn; TimeOut={2}", SQLServer, SQLDB, SQLConnectionTimeout);

            if (UseTrustedConnection && UseConnectionPooling)
            {
                builder.AppendFormat("; Min Pool Size={0}; Max Pool Size={1}; Trusted_Connection=Yes", SQLPoolMinConnections, SQLPoolMaxConnections);
            }
            else if (UseTrustedConnection && !UseConnectionPooling)
            {
                builder.AppendFormat("; Trusted_Connection=Yes");
            }
            else if (!UseTrustedConnection && UseConnectionPooling)
            {
                builder.AppendFormat("; Min Pool Size={0}; Max Pool Size={1}; UID={2}; PWD={3}", SQLPoolMinConnections, SQLPoolMaxConnections, SqlUserID, SqlUserPassword);
            }
            else if (!UseTrustedConnection && !UseConnectionPooling)
            {
                builder.AppendFormat("; UID={0}; PWD={1}", SqlUserID, SqlUserPassword);
            }
            return builder.ToString();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace MedJames.Database.SQL
{
    [Serializable]
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    public class DBRoutines
    {
        private readonly string constring = string.Empty;
        private readonly SQLSettings sqlSettings;
       

        #region Constructors
        public DBRoutines(SQLSettings sqlSettings)
        {
            this.sqlSettings = sqlSettings;
        }

        public DBRoutines(string sqlConnection)
        {
            constring = sqlConnection;
        }
        #endregion

        #region Public Methods
        public object ExecSPReturnValue(string procedureName, SqlParameter[] procedureParams, string returnColumn)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            int rowsAffected;
            object returnValue = null;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(procedureName, conSql);
                cmd.CommandType = CommandType.StoredProcedure;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                cmd.ExecuteNonQuery();

                if (returnColumn != "")
                {
                    returnValue = cmd.Parameters[returnColumn].Value;
                }
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
            return returnValue;
        }

        public object ExecSPReturnScalar(string procedureName, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            object returnValue = null;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(procedureName, conSql);
                cmd.CommandType = CommandType.StoredProcedure;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                returnValue = cmd.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
            return returnValue;
        }

        public object ExecSQLReturnValue(string sqlstring)
        {
            return ExecSQLReturnValue(sqlstring, null);
        }

        public object ExecSQLReturnValue(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            object returnValue = null;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
                cmd.CommandType = CommandType.Text;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                //TODO Duplicates
                //returnValue = cmd.ExecuteScalar() == null ? 0 : cmd.ExecuteScalar();
                returnValue = cmd.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                 throw ex;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
            return returnValue;
        }

        public SqlDataReader ExecSQLReturnRS(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            SqlDataReader rd;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
                cmd.CommandType = CommandType.StoredProcedure;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            return rd;
        }
        
        public DataSet ExecSQLReturnDS(string sqlstring, int commandtimeout, string dsTableName)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            DataSet dSet;
            SqlDataAdapter sAdapter = null;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = commandtimeout;

                sAdapter = new SqlDataAdapter();
                sAdapter.SelectCommand = cmd;
                dSet = new DataSet();

                if (dsTableName.Length > 0)
                {
                    sAdapter.Fill(dSet, dsTableName);
                }
                else 
                { 
                    sAdapter.Fill(dSet);
                }
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
                if (sAdapter != null)
                    sAdapter.Dispose();
            }
            return (dSet);
        }

        public SqlDataReader ExecSQLRS(string sqlstring)
        {
            return ExecSQLRS(sqlstring, null);
        }

        public SqlDataReader ExecSQLRS(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd;
            SqlDataReader sqlReader;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                sqlReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return (sqlReader);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
        }

        public SqlCommand ExecSQLRSCmd(string strSqlQuery)
        {
            return ExecSQLRSCmd(strSqlQuery, null);
        }

        public SqlCommand ExecSQLRSCmd(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
            
                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                return (cmd);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
        }

        public bool ExecSQLUpdateStmt(string strSqlQuery)
        {
            return ExecSQLUpdateStmt(strSqlQuery, null);
        }

        public bool ExecSQLUpdateStmt(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            SqlDataReader dr;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
                cmd.CommandType = CommandType.Text;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr != null)
                    dr.Close();
                return (true);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
        }

        public bool ExecSQLInsertStmt(string strSqlQuery)
        {
            return ExecSQLInsertStmt(strSqlQuery, null);
        }

        public bool ExecSQLInsertStmt(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            SqlDataReader dr;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
                cmd.CommandType = CommandType.Text;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr != null)
                    dr.Close();
                return (true);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
        }

        public bool ExecSQLDeleteStmt(string strSqlQuery)
        {
            return ExecSQLDeleteStmt(strSqlQuery, null);
        }

        public bool ExecSQLDeleteStmt(string sqlstring, SqlParameter[] procedureParams)
        {
            SqlConnection conSql;
            SqlCommand cmd = null;
            SqlDataReader dr;

            try
            {
                conSql = OpenSQLConnection();
                cmd = new SqlCommand(sqlstring, conSql);
                cmd.CommandType = CommandType.Text;

                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr != null)
                    dr.Close();
                return (true);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
        }

        public bool ExecSP(string sqlProcedure, SqlParameter[] procedureParams, int commandtimeout)
        {
            //Executes the give stored procedure with the passed parameters
            SqlCommand cmd = null;
            
            try
            {
                cmd = new SqlCommand();
                cmd = ExecSQLRSCmd(sqlProcedure);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = commandtimeout;

                foreach (SqlParameter sqlParm in procedureParams)
                {
                    cmd.Parameters.Add(sqlParm);
                }
                cmd.ExecuteNonQuery();
                return (true);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
            }
        }

        public DataSet ExecSPReturnDS(string sqlProcedure, SqlParameter[] procedureParams, 
                                        int commandtimeout, string dsTableName)
        {
            //Executes the give stored procedure with the passed parameters returning dataset
            DataSet ds;
            SqlCommand cmd = null;
            SqlDataAdapter sAdapter = null;

            try
            {
                cmd = new SqlCommand();
                cmd = ExecSQLRSCmd(sqlProcedure);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = commandtimeout;
                if (procedureParams != null)
                {
                    foreach (SqlParameter sqlParm in procedureParams)
                    {
                        cmd.Parameters.Add(sqlParm);
                    }
                }
                sAdapter = new SqlDataAdapter();
                sAdapter.SelectCommand = cmd;
                ds = new DataSet();

                if (dsTableName.Length > 0)
                {
                    sAdapter.Fill(ds, dsTableName);
                }
                else
                {
                    sAdapter.Fill(ds);
                }
                return (ds);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
                if (sAdapter != null)
                    sAdapter.Dispose();
            }
        }

        public DataSet ExecSPReturnDS(string sqlProcedure, SqlParameter[] Parameters, int commandtimeout)
        {
            return (ExecSPReturnDS(sqlProcedure, Parameters, commandtimeout, ""));
        }

        public DataSet ExecSPReturnDS(string sqlProcedure, int commandtimeout, string dsTableName)
        {
            return (ExecSPReturnDS(sqlProcedure, commandtimeout, dsTableName));
        }

        public DataSet ExecSPReturnDS(string sqlProcedure, int commandtimeout)
        {
            return (ExecSPReturnDS(sqlProcedure, commandtimeout, ""));
        }

        public DataSet ExecSPReturnDS(string sqlProcedure, SqlParameter Parameters, int commandtimeout, string dsTableName)
        {
            var sqlParms = new SqlParameter[1];
            sqlParms[0] = Parameters;
            return (ExecSPReturnDS(sqlProcedure, Parameters, commandtimeout, dsTableName));
        }

        public DataSet ExecSPReturnDS(string sqlProcedure, SqlParameter Parameters, int commandtimeout)
        {
            var sqlParms = new SqlParameter[1];
            sqlParms[0] = Parameters;
            return (ExecSPReturnDS(sqlProcedure, Parameters, commandtimeout, ""));
        }

        public SqlParameter BuildSQLParm(string name, SqlDbType datatype,
                                            ParameterDirection Direction, int size, object value)
        {
            //Builds a sql parameter object using values that are passed to this function
            SqlParameter sqlParm;

            sqlParm = new SqlParameter(name, datatype, size);
            sqlParm.Direction = Direction;
            sqlParm.Value = value;

            return (sqlParm);
        }
        #endregion

        #region Helper Methods
        private SqlConnection OpenSQLConnection()
        {
            var connection = new SqlConnection();
            try
            {
                connection.ConnectionString = (string.IsNullOrEmpty(constring)) ? sqlSettings.SQLConnectionString : constring;
                connection.Open();
                return connection;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    
}
